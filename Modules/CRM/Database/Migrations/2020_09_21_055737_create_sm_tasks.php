<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_of_task');
            $table->string('priority');
            $table->string('status');
            $table->string('type_id')->nullable();
            $table->string('start_date');
            $table->string('end_date');
            $table->string('complete_date');
            $table->string('contact_type')->nullable();
            $table->string('contact_id')->nullable();
            $table->string('description')->nullable();
            $table->string('created_by');
            $table->string('modified_by');
            $table->string('assigned_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_tasks');
    }
}
