<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmTasksPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_task_plan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('planning')->nullable();
            $table->string('target')->nullable();
            $table->string('expected_date');
            $table->string('task_id');
            $table->string('created_by');
            $table->string('complete_level');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
