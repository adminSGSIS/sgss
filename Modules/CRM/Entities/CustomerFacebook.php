<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;

class CustomerFacebook extends Model
{
    protected $fillable = [];
    public $table = "customer_facebook";
}
