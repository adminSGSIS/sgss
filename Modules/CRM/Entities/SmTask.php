<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;

class SmTask extends Model
{
    protected $fillable = [];
    protected $casts = [
        'assigned_users' => 'array',
    ];
    public $table = "sm_tasks";
    public $timestamps = false;
}
