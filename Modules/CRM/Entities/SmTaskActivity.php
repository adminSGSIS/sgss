<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;

class SmTaskActivity extends Model
{
    protected $fillable = [];
    public $table = "sm_task_activity";
}
