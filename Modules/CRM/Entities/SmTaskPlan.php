<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;

class SmTaskPlan extends Model
{
    protected $fillable = [];
    public $table = "sm_task_plan";
    public $timestamps = false;
}
