<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;

class SmTaskStatus extends Model
{
    protected $fillable = [];
    public $table = "sm_tasks_status";
}
