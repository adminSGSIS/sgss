<?php

namespace Modules\CRM\Entities;

use Illuminate\Database\Eloquent\Model;

class SmTaskType extends Model
{
    protected $fillable = [];
    public $table = "sm_tasks_type";
}
