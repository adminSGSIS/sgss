<?php

namespace Modules\CRM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CRM\Entities\CustomerFacebook;
use App\SmStaff;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Carbon;

class CustomerFacebookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function searchCustomer()
    {
        return view('crm::CustomerFacebook.searchCustomer');
    }

    public function listsCustomer(Request $request)
    {
        try{
            $year = $request->year ? $request->year : '';
            $customer_name = $request->customer_name ? $request->customer_name : '';
            $creator_name = $request->creator_name ? $request->creator_name : '';
            if(!$year && !$customer_name && !$creator_name){
                $customers = [[]];
                return view('crm::CustomerFacebook.searchCustomer', compact('customers'));
            }
            //handling search by staff name (full name or not)
            $staffs = SmStaff::where('full_name', 'like', '%' . $creator_name . '%')->get();
            if($creator_name != ''){
                $customers = [[]];
                foreach($staffs as $staff){
                    $count = CustomerFacebook::where('year', 'like', '%' . $year . '%')
                    ->where('customer_name', 'like', '%' . $customer_name . '%')
                    ->where('staff_id', $staff->id)->count();
                    if($count > 0){
                        $customers[] = CustomerFacebook::where('year', 'like', '%' . $year . '%')
                        ->where('customer_name', 'like', '%' . $customer_name . '%')
                        ->where('staff_id', $staff->id)->get();
                    }
                }
            }
            //if request not have staff name
            else{
                $customers[] = CustomerFacebook::where('year', 'like', '%' . $year . '%')
                ->where('customer_name', 'like', '%' . $customer_name . '%')->get();
            }
            return view('crm::CustomerFacebook.searchCustomer', compact('customers'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function createCustomer()
    {
        return view('crm::CustomerFacebook.createCustomer');
    }

    public function storeCustomer(Request $request)
    {
        try{
            if(CustomerFacebook::where('email', $request->email)->first() || CustomerFacebook::where('phone_number', $request->phone_number)->first()){
                Toastr::error('Duplicate information', 'Failed');
                return redirect()->back();
            }
            $customer = new CustomerFacebook();
            $customer->staff_id = SmStaff::where('email', Auth::user()->email)->first()->id;
            $customer->customer_name = $request->customer_name;
            $customer->email = $request->email;
            $customer->phone_number = $request->phone_number;
            $customer->expected_class = $request->expected_class;
            $customer->notes = $request->notes;
            $customer->year = date('Y');
            $customer->school_id = Auth::user()->school_id;
            $result = $customer->save();
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function viewCustomer($id)
    {
        try{
            $customer = CustomerFacebook::find($id);
            $staffDetails = SmStaff::find($customer->staff_id);
            return view('crm::CustomerFacebook.viewCustomer', compact('customer', 'staffDetails'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function editCustomer($id)
    {
        try{
            $editData = CustomerFacebook::find($id);
            if(SmStaff::where('email', Auth::user()->email)->first()->id != $editData->staff_id && Auth::user()->role_id != 1){
                Toastr::error('You don\'t have permission to edit', 'Failed');
                return redirect()->back();
            }
            return view('crm::CustomerFacebook.createCustomer', compact('editData'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function updateCustomer(Request $request, $id)
    {
        try{
            if(CustomerFacebook::where('email', $request->email)->first() || CustomerFacebook::where('phone_number', $request->phone_number)->first()){
                Toastr::error('Duplicate information', 'Failed');
                return redirect()->back();
            }
            $customer = CustomerFacebook::find($id);
            $customer->customer_name = $request->customer_name;
            $customer->email = $request->email;
            $customer->phone_number = $request->phone_number;
            $customer->expected_class = $request->expected_class;
            $customer->notes = $request->notes;
            $customer->year = date('Y');
            $result = $customer->update();
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('/customer-facebook/' . $id);
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function deleteView(Request $request, $id)
    {
        try{
            $title = "Are you sure to detete ?";
            $url = url('customer-facebook/' . $id . '/delete');
            return view('backEnd.modal.delete', compact('id', 'title', 'url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function deleteCustomer($id)
    {
        try{
            $result = CustomerFacebook::destroy($id);
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }
}
