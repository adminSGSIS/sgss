<?php

namespace Modules\CRM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\SmHumanDepartment;
use App\SmStaff;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use App\ApiBaseMethod;
use Modules\CRM\Entities\SmTask;
use Modules\CRM\Entities\SmTaskStatus;
use Modules\CRM\Entities\SmTaskType;
use Modules\CRM\Entities\SmTaskPlan;
use Modules\CRM\Entities\SmTaskActivity;
use App\Mail\DuesFeesMail;
use App\SmNotification;
Use App\User;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function newTaskType(Request $request)
    {
        try{
            if(SmTaskType::where('title', $request->title)->where('school_id', Auth::user()->school_id)->first()){
                Toastr::error('Duplicate title', 'Failed');
                return redirect()->back();
            }
            $taskType = new SmTaskType();
            $taskType->title = $request->title;
            $taskType->color = $request->color ? $request->color : 'btn';
            $taskType->school_id = Auth::user()->school_id;
            $taskType->created_by = SmStaff::where('email', Auth::user()->email)->first()->id;
            $taskType->save();
            Toastr::success('Create tags success', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function taskTypeView()
    {
        try{
            return view('crm::TasksManagement.newTaskType');
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function chooseTags()
    {
        try{
            return view('crm::TasksManagement.chooseTags');
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function editTags($id)
    {
        try{
            $editData = SmTaskType::find($id);
            return view('crm::TasksManagement.newTaskType', compact('editData'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function updateTags(Request $request, $id)
    {
        try{
            if(!(Auth::user()->role_id == 1) && !(SmStaff::where('email', Auth::user()->email)->first()->id == SmTaskType::find($id)->created_by)){
                Toastr::error('You don\'t have permission to edit', 'Failed');
                return redirect('/tasks/add');
            }
            if(SmTaskType::where('title', $request->title)->where('school_id', Auth::user()->school_id)->where('id', '!=', $id)->first()){
                Toastr::error('Duplicate title', 'Failed');
                return redirect()->back();
            }
            $taskType = SmTaskType::find($id);
            $taskType->title = $request->title;
            $taskType->color = $request->color ? $request->color : $taskType->color;
            $taskType->school_id = Auth::user()->school_id;
            $taskType->save();
            Toastr::success('Update tags success', 'Success');
            return redirect('/tasks/add');
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function deleteTags($id)
    {
        try{
            if(!(Auth::user()->role_id == 1) && !(SmStaff::where('email', Auth::user()->email)->first()->id == SmTaskType::find($id)->created_by)){
                Toastr::error('You don\'t have permission to delete', 'Failed');
                return redirect('/tasks/add');
            }
            if(SmTask::where('type_id', $id)->first()){
                Toastr::error('Please delete task first', 'Failed');
                return redirect('/tasks/add');
            }
            $taskType = SmTaskType::find($id);
            $taskType->delete();
            Toastr::success('Delete tags success', 'Success');
            return redirect('/tasks/add');
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function index()
    {
        try{
            $priority = ['high', 'mid', 'low'];
            $departments = SmHumanDepartment::where('active_status', '=', '1')->where('school_id', Auth::user()->school_id)->get();
            $staffs = SmStaff::where('school_id', Auth::user()->school_id)->get();
            if(Auth::user()->role_id == 1){
                $filterTask = SmTask::where('school_id', Auth::user()->school_id)->where('active_status', 1)->get();
            }
            else{
                $filterTask = [];
                $tasks = SmTask::where('school_id', Auth::user()->school_id)->where('active_status', 1)->get();
                $staff = SmStaff::where('email', Auth::user()->email)->first()->id;
                foreach($tasks as $task){
                    if(in_array($staff, $task->assigned_users) || $task->created_by == $staff){
                        $filterTask[] = $task;
                    }
                }
            }
            return view('crm::TasksManagement.newTask', compact('priority', 'departments', 'staffs', 'filterTask'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function disabledTask()
    {
        try{
            $priority = ['high', 'mid', 'low'];
            $departments = SmHumanDepartment::where('active_status', '=', '1')->where('school_id', Auth::user()->school_id)->get();
            $staffs = SmStaff::where('school_id', Auth::user()->school_id)->get();
            if(Auth::user()->role_id == 1){
                $filterTask = SmTask::where('school_id', Auth::user()->school_id)->where('active_status', 0)->get();
            }
            else{
                $filterTask = [];
                $tasks = SmTask::where('school_id', Auth::user()->school_id)->where('active_status', 0)->get();
                $staff = SmStaff::where('email', Auth::user()->email)->first()->id;
                foreach($tasks as $task){
                    if(in_array($staff, $task->assigned_users) || $task->created_by == $staff){
                        $filterTask[] = $task;
                    }
                }
            }
            return view('crm::TasksManagement.disableTask', compact('priority', 'departments', 'staffs', 'filterTask'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $input = $request->all();
        $validator = Validator::make($input, [
            'priority' => "required",
            'start_date' => "after:yesterday",
            'tag' => 'required',
        ]);

        if ($validator->fails()) {
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                return ApiBaseMethod::sendError('Validation Error.', $validator->errors());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        if(strtotime($request->start_date) > strtotime($request->end_date)){
            Toastr::error('End date must be greather than start date', 'Failed');
            return redirect()->back();
        }
        if(!$request->has('staffs')){
            Toastr::error('Please choose staffs', 'Failed');
            return redirect()->back();
        }
        try{
            $taskStatus = new SmTaskStatus();
            $taskStatus->status = "0%";
            $taskStatus->save();

            $taskType = SmTaskType::where('title', $request->tag)->where('school_id', Auth::user()->school_id)->first();

            $task = new SmTask();
            $task->name_of_task = $request->name_of_task;
            $task->priority = $request->priority;
            $task->status = $taskStatus->id;
            $task->type_id = $taskType->id;
            $task->start_date = $request->start_date;
            $task->end_date = $request->end_date;
            $task->description = $request->description;
            $task->created_by = SmStaff::where('email', Auth::user()->email)->first()->id;
            $task->modified_by = SmStaff::where('email', Auth::user()->email)->first()->id;
            $task->assigned_users = $request->staffs;
            $task->school_id = Auth::user()->school_id;
            $task->active_status = 1;
            $task->save();

            foreach($request->staffs as $staff){
                $user = SmStaff::find($staff);

                $notification = new SmNotification;
                $notification->user_id = User::where('email', $user->email)->first()->id;
                $notification->role_id = $user->role_id;
                $notification->date = date('Y-m-d');
                $notification->message = 'You has a new task, please check !';
                $notification->school_id = $user->school_id;
                $notification->url = '/tasks/'.$task->id;
                $notification->save();
            }

            // if($request->has('sendNoti')){
            //     foreach($request->staffs as $staff){
            //         // Mail::raw('You has a new task from sgstar', function ($message) {
            //         //     $message->to($staff->email);
            //         // });
            //     }
            // }

            Toastr::success('Create task success', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function viewTask($id)
    {
        try{
            $task = SmTask::find($id);
            if(!in_array(SmStaff::where('email', Auth::user()->email)->first()->id, $task->assigned_users) && !(Auth::user()->role_id == 1) && !(SmTask::find($id)->created_by == SmStaff::where('email', Auth::user()->email)->first()->id)){
                Toastr::error('Don\'t have permission', 'Failed');
                return redirect()->back();
            }
            $staffDetails = SmStaff::find($task->created_by);
            $activities = SmTaskActivity::where('task_id', $id)->get();
            return view('crm::TasksManagement.viewTask', compact('task', 'staffDetails', 'activities'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function editTask($id)
    {
        if(!(Auth::user()->role_id == 1) && !(SmTask::find($id)->created_by == SmStaff::where('email', Auth::user()->email)->first()->id)){
            Toastr::error('You don\'t have permission to update', 'Failed');
            return redirect('/tasks/add');
        }
        if(SmTask::find($id)->active_status == 0){
            Toastr::error('Can\'t edit ended task', 'Failed');
            return redirect()->back();
        }
        try{
            $priority = ['high', 'mid', 'low'];
            $editData = SmTask::find($id);
            $editTaskPlan = SmTaskPlan::where('task_id', $editData->id);
            $departments = SmHumanDepartment::where('active_status', '=', '1')->where('school_id', Auth::user()->school_id)->get();
            $staffs = SmStaff::where('school_id', Auth::user()->school_id)->get();
            if(Auth::user()->role_id == 1 || Auth::user()->role_id == 5){
                $filterTask = SmTask::where('school_id', Auth::user()->school_id)->get();
            }
            else{
                $tasks = SmTask::where('school_id', Auth::user()->school_id)->get();
                $staff = SmStaff::where('email', Auth::user()->email)->first()->id;
                foreach($tasks as $task){
                    if(in_array($staff, $task->assigned_users) || $task->created_by == $staff){
                        $filterTask[] = $task;
                    }
                }
            }
            return view('crm::TasksManagement.newTask', compact('editData', 'editTaskPlan', 'priority', 'departments', 'staffs', 'filterTask'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function updateTask(Request $request, $id)
    {       
        if(!$request->has('staffs')){
            Toastr::error('Please choose staffs', 'Failed');
            return redirect('/tasks/'.$id.'/edit');
        }
        try{
            $task = SmTask::find($id);
            if(strtotime($task->start_date) > strtotime($request->end_date)){
                Toastr::error('End date must be greather than start date', 'Failed');
                return redirect()->back();
            }

            $staffCount = count($task->assigned_users);

            $task->name_of_task = $request->name_of_task;
            $task->priority = $request->priority;
            $task->end_date = $request->end_date;
            $task->type_id = SmTaskType::where('title', $request->tag)->where('school_id', Auth::user()->school_id)->first()->id;
            $task->description = $request->description;
            $task->assigned_users = $request->staffs;
            $task->save();

            $taskPlan = SmTaskPlan::where('task_id', $id)->get(); 
            foreach($taskPlan as $item){
                if(!in_array($item->created_by, $task->assigned_users)){
                    SmTaskPlan::destroy($item->id);
                }
            }
            if(count($request->staffs) != $staffCount){
                $total = 0;
                foreach($taskPlan as $item){
                    $level = (rtrim($item->complete_level, "%") * 0.01)*(100/count($request->staffs));
                    $total += $level;
                }
                $taskStatus = SmTaskStatus::find($task->status);
                $taskStatus->status = $total.'%';
                $taskStatus->save(); 
            } 

            foreach($request->staffs as $staff){
                $user = SmStaff::find($staff);

                $notification = new SmNotification;
                $notification->user_id = User::where('email', $user->email)->first()->id;
                $notification->role_id = $user->role_id;
                $notification->date = date('Y-m-d');
                $notification->message = 'Task updated, please check !';
                $notification->school_id = $user->school_id;
                $notification->url = '/tasks/'.$id;
                $notification->save();
            }
            
            Toastr::success('Update task success', 'Success');
            return redirect('/tasks/add');
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function disableView($id)
    {
        try{
            $title = "Are you sure to disable ?";
            $url = url('tasks/' . $id . '/disable');
            return view('crm::TasksManagement.onoffTask', compact('id', 'title', 'url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function disableTask($id)
    {
        try{
            $task = SmTask::find($id);
            $task->active_status = 0;
            $task->save();

            // $taskStatus = SmTaskStatus::find($task->status);
            // $taskStatus->delete();

            // $taskPlan = SmTaskPlan::where('task_id', $id)->get(); 
            // foreach($taskPlan as $item){
            //     if($item->file != ''){
            //         unlink($item->file);
            //     }
            //     $item->delete();
            // }
            // $task->delete();

            // $activities = SmTaskActivity::where('task_id', $id)->get();
            // foreach($activities as $item){
            //     $item->delete();
            // }

            Toastr::success('Disable task success', 'Success');
           return redirect()->back();
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function unableView($id)
    {
        try{
            $title = "Are you sure to unable ?";
            $url = url('tasks/' . $id . '/unable');
            return view('crm::TasksManagement.onoffTask', compact('id', 'title', 'url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function unableTask($id)
    {
        try{
            $task = SmTask::find($id);
            $task->active_status = 1;
            $task->save();

            Toastr::success('Unable task success', 'Success');
           return redirect('/tasks/add');
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }
    
    public function deleteView($id)
    {
        try{
            $title = "Are you sure to delete ?";
            $url = url('tasks/' . $id . '/delete');
            return view('backEnd.modal.delete', compact('id', 'title', 'url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function deleteTask($id)
    {
        try{
            $task = SmTask::find($id);

            $taskStatus = SmTaskStatus::find($task->status);
            $taskStatus->delete();

            $taskPlan = SmTaskPlan::where('task_id', $id)->get(); 
            foreach($taskPlan as $item){
                if($item->file != ''){
                    unlink($item->file);
                }
                $item->delete();
            }
            $task->delete();

            $activities = SmTaskActivity::where('task_id', $id)->get();
            foreach($activities as $item){
                $item->delete();
            }

            Toastr::success('Disable task success', 'Success');
           return redirect()->back();
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function planningTask($id)
    {
        try{
            $task = SmTask::find($id);
            if(!in_array(SmStaff::where('email', Auth::user()->email)->first()->id, $task->assigned_users)){
                Toastr::error('Only assigned users can create plan', 'Failed');
                return redirect()->back();
            }
            if($task->active_status == 0){
                Toastr::error('Can\'t planning ended task', 'Failed');
                return redirect()->back();
            }
            $taskPlan = SmTaskPlan::where('task_id', $id)->where('created_by', SmStaff::where('email', Auth::user()->email)->first()->id)->first(); 
            if($taskPlan){
                Toastr::error('You already has a plan', 'Failed');
                return redirect('/tasks/'.$id.'/planning/edit');
            }
            return view('crm::TasksManagement.planning', compact('id'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function storePlanningTask(Request $request, $id)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(strtotime($request->expected_date) < strtotime(SmTask::find($id)->start_date)){
            Toastr::error('Expected complete date must be greather than start date', 'Failed');
            return redirect()->back();
        }
        try{
            $plan = new SmTaskPlan();
            $plan->planning = $request->plan;
            $plan->target = $request->target;
            $plan->expected_date = $request->expected_date;
            $plan->task_id = $id;
            $plan->school_id = Auth::user()->school_id;
            $plan->created_by = SmStaff::where('email', Auth::user()->email)->first()->id;
            $plan->complete_level = "0%";
            $document_file = "";
            if ($request->file('file') != "") {
                $file = $request->file('file');
                $plan->file_name = strtoupper($file->getClientOriginalName());
                $document_file = 'plan-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/task-planning/', $document_file);
                $document_file =  'public/uploads/task-planning/' . $document_file;
            }
            $plan->file = $document_file;
            $plan->save();

            $user = SmStaff::find(SmTask::find($id)->created_by);

            $notification = new SmNotification;
            $notification->user_id = User::where('email', $user->email)->first()->id;
            $notification->role_id = $user->role_id;
            $notification->date = date('Y-m-d');
            $notification->message = SmStaff::where('email', Auth::user()->email)->first()->full_name . ' created a new plan, please check !';
            $notification->school_id = $user->school_id;
            $notification->url = '/tasks/' . $id;
            
            $notification->save();

            Toastr::success('Create plan success', 'Success');
            return redirect('/tasks/add');
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function editPlanningTask($id)
    {
        try{
            $editData = SmTaskPlan::where('task_id', $id)->where('created_by', SmStaff::where('email', Auth::user()->email)->first()->id)->first(); 
            return view('crm::TasksManagement.planning', compact('id', 'editData'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function updatePlanningTask(Request $request, $id)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        if(strtotime($request->expected_date) < strtotime(SmTask::find($id)->start_date)){
            Toastr::error('Expected complete date must be greather than start date', 'Failed');
            return redirect()->back();
        }
        try{
            $plan = SmTaskPlan::where('task_id', $id)->where('created_by', SmStaff::where('email', Auth::user()->email)->first()->id)->first();
            $plan->planning = $request->plan;
            $plan->target = $request->target;
            $plan->expected_date = $request->expected_date;
            $document_file = "";
            if ($request->file('file') != "") {
                if($plan->file != ''){
                    unlink($plan->file);
                }

                $file = $request->file('file');
                $plan->file_name = strtoupper($file->getClientOriginalName());
                $document_file = 'plan-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/task-planning/', $document_file);
                $document_file =  'public/uploads/task-plannings/' . $document_file;
                $plan->file = $document_file;
            }
            $plan->save();

            $user = SmStaff::find(SmTask::find($id)->created_by);

            $notification = new SmNotification;
            $notification->user_id = User::where('email', $user->email)->first()->id;
            $notification->role_id = $user->role_id;
            $notification->date = date('Y-m-d');
            $notification->message = SmStaff::where('email', Auth::user()->email)->first()->full_name . ' updated a plan, please check !';
            $notification->school_id = $user->school_id;
            $notification->url = '/tasks/' . $id;
            $notification->save();

            Toastr::success('Update plan success', 'Success');
            return redirect('/tasks/add');
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function changeTaskLevel(Request $request, $id)
    {
        try{
            $title = "Want to update your task level ?";
            $status = SmTaskPlan::where('task_id', $id)->where('created_by', SmStaff::where('email', Auth::user()->email)->first()->id)->first();
            $level =  rtrim($status->complete_level, "%");
            return view('crm::TasksManagement.changeTaskLevel', compact('id', 'title', 'level'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function updateActual(Request $request, $id)
    {
        try{
            $task = SmTask::find($id);
            if($task->active_status == 0){
                Toastr::error('Can\'t edit ended task', 'Failed');
                return redirect()->back();
            }
            $taskStatus = SmTaskStatus::find($task->status);
            if(Auth::user()->role_id == 1 || $task->created_by == SmStaff::where('email', Auth::user()->email)->first()->id){
                Toastr::error('Only assigned users can update', 'Failed');
                return redirect()->back();
            }
            $plan = SmTaskPlan::where('task_id', $id)->where('created_by', SmStaff::where('email', Auth::user()->email)->first()->id)->first();
            $plan->complete_level = $request->level ? $request->level.'%' : $plan->complete_level;
            $plan->save();

            $allPlan = SmTaskPlan::where('task_id', $id)->get();
            $total = 0;
            foreach($allPlan as $item){
                $status =  rtrim($item->complete_level, "%");
                $level = ($status * 0.01)*(100/count($task->assigned_users));
                $total += $level;
            }
            $taskStatus->status = $total.'%';
            $taskStatus->save(); 

            $user = SmStaff::find(SmTask::find($id)->created_by);

            $notification = new SmNotification;
            $notification->user_id = User::where('email', $user->email)->first()->id;
            $notification->role_id = $user->role_id;
            $notification->date = date('Y-m-d');
            $notification->message = 'Task status has updated by ' . SmStaff::where('email', Auth::user()->email)->first()->full_name . ', please check !';
            $notification->school_id = $user->school_id;
            $notification->url = '/tasks/' . $id;
            $notification->save();
    
            Toastr::success('Update actual success', 'Success');
            return redirect('/tasks/add');
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function storeActivity(Request $request)
    {
        try{
            $task = SmTask::find($request->task_id);
            if($task->active_status == 0){
                Toastr::error('Can\'t edit ended task', 'Failed');
                return redirect()->back();
            }
            $activity = new SmTaskActivity();
            $activity->activity = $request->comment;
            $activity->task_id = $request->task_id;
            $activity->created_by = SmStaff::where('email', Auth::user()->email)->first()->id; 
            $activity->save();

            $staffs = SmTask::find($request->task_id)->assigned_users;
            $staffs[] = SmTask::find($request->task_id)->created_by;
            foreach($staffs as $staff){
                $user = SmStaff::find($staff);
                if($staff != SmStaff::where('email', Auth::user()->email)->first()->id){
                    $notification = new SmNotification;
                    $notification->user_id = User::where('email', $user->email)->first()->id;
                    $notification->role_id = $user->role_id;
                    $notification->date = date('Y-m-d');
                    $notification->message = SmStaff::where('email', Auth::user()->email)->first()->full_name.' update new activity';
                    $notification->school_id = $user->school_id;
                    $notification->url = '/tasks/'.$request->task_id;
                    $notification->save();
                }
            }

            Toastr::success('Activity created', 'Success');
            return redirect('/tasks/'.$request->task_id);
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
}
