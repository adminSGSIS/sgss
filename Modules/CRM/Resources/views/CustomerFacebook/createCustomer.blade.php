@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Customer Facebook</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-8 col-md-6">
                <div class="main-title">
                    <h3 class="mb-30">Customer @lang('lang.information') </h3>
                </div>
            </div>
            <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg">
                <a href="/customer-facebook/search-customer" class="primary-btn small fix-gr-bg">
                    @lang('lang.all') Customer List
                </a>
            </div>
        </div>
        @if(isset($editData))
            @if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'customer-facebook/'. $editData->id . '/edit', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
            @endif
        @elseif(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'customer-facebook/add', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        @endif
        <div class="row">
            <div class="col-lg-12">
                @if(session()->has('message-success'))
                <div class="alert alert-success">
                  {{ session()->get('message-success') }}
              </div>
              @elseif(session()->has('message-danger'))
              <div class="alert alert-danger">
                  {{ session()->get('message-danger') }}
              </div>
              @endif
              <div class="white-box">
                <div class="">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-title">
                                <h4>@lang('lang.basic_info')</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-20">
                        <div class="col-lg-12">
                            <hr>
                        </div>
                    </div>

                    <input type="hidden" name="url" id="url" value="{{URL::to('/')}}"> 

                    <div class="row mb-30">
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="text" required name="customer_name" value="{{isset($editData) ? $editData->customer_name : ''}}">
                                <span class="focus-border"></span>
                                <label>Customer Name <span>*</span> </label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="email" required name="email" value="{{isset($editData) ? $editData->email : ''}}">
                                <span class="focus-border"></span>
                                <label>@lang('lang.email') <span>*</span> </label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="number" min="7" required name="phone_number" value="{{isset($editData) ? $editData->phone_number : ''}}">
                                <span class="focus-border"></span>
                                <label>@lang('lang.phone_number')</label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="text" required name="expected_class" value="{{isset($editData) ? $editData->expected_class : ''}}">
                                <span class="focus-border"></span>
                                <label>Expected Class</label>
                            </div>
                        </div>

                        <div class="col-lg-12 mt-3">
                            <div class="input-effect">
                                <textarea class="primary-input form-control" cols="0" rows="4" name="notes">{{isset($editData) ? $editData->notes : ''}}</textarea>
                                <label>notes <span>*</span> </label>
                                <span class="focus-border textarea"></span>
                            </div>
                        </div>
                        
                    </div>
            <div class="row mt-40">
                <div class="col-lg-12 text-center">
                    <button class="primary-btn fix-gr-bg">
                        <span class="ti-check"></span>
                        @if(isset($editData))
                            @lang('lang.update') customer
                        @else
                            @lang('lang.save') customer
                        @endif
                    </button>
                </div>
            </div>
    </div>
    {{ Form::close() }}
    </div>
</section>


@endsection
