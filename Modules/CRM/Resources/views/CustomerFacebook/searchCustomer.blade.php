@extends('backEnd.master')
@section('css')
 <style>
    .centerTd td, .centerTh th{
        text-align: center;
    }
 </style>
 @endsection
@section('mainContent')
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('lang.list') Customers</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-8 col-md-6 col-sm-6">
                    <div class="main-title mt_0_sm mt_0_md">
                        <h3 class="mb-30  ">@lang('lang.select_criteria')</h3>
                    </div>
                </div>

                @if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                 <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg col-sm-6 text_sm_right">
                    <a href="/customer-facebook/add" class="primary-btn small fix-gr-bg">
                        <span class="ti-plus pr-2"></span>
                        @lang('lang.add') customer
                    </a>
                </div>
            @endif
            </div>
            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/customer-facebook/search-customer', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'infix_form']) }}
            <div class="row">
                <div class="col-lg-12">
                <div class="white-box">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <input class="primary-input" type="text" name="year">
                                <label>search by year</label>
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        
                        
                        <div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <input class="primary-input" type="text" name="customer_name">
                                <label>search by customers name</label>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                       
                        <div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <input class="primary-input" type="text" name="creator_name">
                                <label>search by creator name</label>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 mt-20 text-right">
                            <button type="submit" class="primary-btn small fix-gr-bg" id="btnsubmit">
                                <span class="ti-search pr-2"></span>
                                @lang('lang.search')
                            </button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            {{ Form::close() }}

            @if (@$customers)
            <div class="row mt-40 full_wide_table">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 no-gutters">
                            <div class="main-title">
                                <h3 class="mb-0">Customers List </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row  ">
                        <div class="col-lg-12">
                            <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                                <thead>
                                    @if(session()->has('message-success') != "" ||
                                    session()->get('message-danger') != "")
                                    <tr>
                                        <td colspan="10">
                                            @if(session()->has('message-success'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success') }}
                                            </div>
                                            @elseif(session()->has('message-danger'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger') }}
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    <tr class="centerTh">
                                        <th>@lang('lang.staff') @lang('lang.name')</th>
                                        <th>name</th>
                                        <th>phone number</th>
                                        <th>email</th>
                                        <th>child age</th>
                                        <th>action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach(@$customers as $customer)
                                    @foreach ($customer as $item)
                                    
                                    <tr class="centerTd">
                                        @if($item->staff_id == 0)
                                            <td>Bot</td>
                                        @else
                                            <td>{{App\SmStaff::find($item->staff_id)->full_name}}</td>
                                        @endif
                                        <td>{{$item->customer_name}}</td>
                                        <td>{{$item->phone_number}}</td> 
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->child_age}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    @if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                                        <a class="dropdown-item" href="{{url('customer-facebook/'.$item->id)}}">@lang('lang.view')</a>
                                                    @endif
                                                    @if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                                        <a class="dropdown-item" href="{{url('customer-facebook/'.$item->id.'/edit')}}">@lang('lang.edit')</a>
                                                    @endif
                                                    @if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                                        <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Item" href="{{url('customer-facebook/'.$item->id.'/delete-view')}}">@lang('lang.delete')</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            
    </div>
</section>
@endsection
