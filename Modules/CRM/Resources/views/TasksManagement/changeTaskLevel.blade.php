<div class="text-center">
    <h4 id="title">{{ $title }}</h4>
 </div>
 
 <div class="mt-40 d-flex justify-content-between">
        <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
        <input oninput="changeLevel()" type="range" min="0" max="100" value="{{$level}}" id="status1">
        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/'. $id . '/actual', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
            <input type="hidden" id="status2" name="level">
            <button class="primary-btn fix-gr-bg" type="submit">Apply</button>  
        {{ Form::close() }}
 </div>
 <script>
    function changeLevel(){
        document.getElementById('title').innerHTML = "Change task level to " + document.getElementById('status1').value + "% ?";
        document.getElementById('status2').value = document.getElementById('status1').value;
    }
 </script>