 {{-- Author: Tran Thanh Phu --}}
 @extends('backEnd.master')
 @section('css')
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 <style>
    .centerTd td, .centerTh th{
        text-align: center;
    }
    .removeOutline:focus {
        box-shadow: none;
    }
    .removeHover:hover{
        background: none;
    }
    .removeHover{
        display: inline;
        margin-left: -25px;
    }
 </style>
 @endsection
 @section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
     <div class="container-fluid">
         <div class="row justify-content-between">
             <h1>Tasks Management</h1>
             <div class="bc-pages">
                 <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
             </div>
         </div>
     </div>
 </section>
 <section class="admin-visitor-area up_st_admin_visitor">
     <div class="container-fluid p-0">
         <div class="row">
             <div class="col-lg-12">
                 <table id="table_id" class="display school-table" cellspacing="0" width="100%">
 
                     <thead>
                         @if(session()->has('message-success-delete') != "" ||
                                 session()->get('message-danger-delete') != "")
                                 <tr>
                                     <td colspan="6">
                                          @if(session()->has('message-success-delete'))
                                           <div class="alert alert-success">
                                               {{ session()->get('message-success-delete') }}
                                           </div>
                                         @elseif(session()->has('message-danger-delete'))
                                           <div class="alert alert-danger">
                                               {{ session()->get('message-danger-delete') }}
                                           </div>
                                         @endif
                                     </td>
                                 </tr>
                              @endif
                         <tr class="centerTh">
                             <th>Tasks Name</th>
                             <th>Priority</th>
                             <th>Tasks Type</th>
                             <th>Created By</th>
                             <th>@lang('lang.action')</th>
                         </tr>
                     </thead>
 
                     <tbody>
                         @if(isset($filterTask))
                         @foreach($filterTask as $value)
                         <tr class="centerTd">
                             <td>{{$value->name_of_task}}</td>
                             <td @if($value->priority == "high") style="color: red" @endif>{{$value->priority}}</td>
                             <td>
                                <button class="{{Modules\CRM\Entities\SmTaskType::find($value->type_id)->color}} removeOutline" style="font-size: 11px;">
                                    {{Modules\CRM\Entities\SmTaskType::find($value->type_id)->title}}
                                </button>  
                            </td>                              
                             <td>{{App\SmStaff::find($value->created_by)->full_name}}</td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                         @lang('lang.select')
                                     </button>
                                     <div class="dropdown-menu dropdown-menu-right">

                                        <a class="dropdown-item" href="{{url('tasks/'.$value->id)}}">@lang('lang.view')</a>
                                    
                                        @if(Auth::user()->role_id == 1 || $value->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id)
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Unable Task" href="{{url('tasks/'.$value->id.'/unable-view')}}">unable</a>
                                        @endif
                                        
                                        @if(Auth::user()->role_id == 1 || $value->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id)
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Task" href="{{url('tasks/'.$value->id.'/delete-view')}}">delete</a>
                                        @endif
                                     </div>
                                 </div>
                             </td>
                         @endforeach
                         @endif
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>
 </div>
 </section>
 
 @endsection