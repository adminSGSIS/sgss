 {{-- Author: Tran Thanh Phu --}}
 @extends('backEnd.master')
 @section('css')
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 <style>
    .centerTd td{
        text-align: center;
    }
    .removeOutline:focus {
        box-shadow: none;
    }
    .removeHover:hover{
        background: none;
    }
    .removeHover{
        display: inline;
        margin-left: -25px;
    }
 </style>
 @endsection
 @section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
     <div class="container-fluid">
         <div class="row justify-content-between">
             <h1>Tasks Management</h1>
             <div class="bc-pages">
                 <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
             </div>
         </div>
     </div>
 </section>
 <section class="admin-visitor-area up_st_admin_visitor">
     <div class="container-fluid p-0">
         @if(isset($editData))
         @if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
            
         <div class="row">
             <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                 <a href="{{url('tasks/add')}}" class="primary-btn small fix-gr-bg">
                     <span class="ti-plus pr-2"></span>
                     @lang('lang.add') Task
                 </a>
             </div>
         </div>
         @endif
         @endif
       <div class="row">
             <div class="col-lg-3">
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="main-title">
                             <h3 class="mb-30">@if(isset($editData))
                                     @lang('lang.edit')
                                 @else
                                     @lang('lang.add')
                                 @endif
                                 Tasks
                             </h3>
                         </div>
                         @if(isset($editData))
                         {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/'.$editData->id . '/edit', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                         @else
                         @if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
            
                         {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/add', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                         @endif
                         @endif
                         <div class="white-box">
                             <div class="add-visitor">
                                 <div class="row">
                                     @if(session()->has('message-success'))
                                     <div class="alert alert-success mb-20">
                                         {{ session()->get('message-success') }}
                                     </div>
                                     @elseif(session()->has('message-danger'))
                                     <div class="alert alert-danger">
                                         {{ session()->get('message-danger') }}
                                     </div>
                                     @endif
                                     <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <input class="primary-input form-control"
                                            type="text" name="name_of_task" autocomplete="off" value="{{isset($editData)? $editData->name_of_task : '' }}{{old('name_of_task')}}" required>
                                            <label>Name of task<span></span> </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <label>Choose Tags<span></span></label>
                                            <a class="deleteUrl removeHover dropdown-item" data-modal-size="modal-md" title="Choose Tags" href="/tasks/choose-tags" >
                                                <button id="tags" type="button" class="btn removeOutline ml-2" style="font-size: 11px; color: #828bb2;">
                                                    +
                                                </button>
                                            </a>
                                            
                                        </div>
                                     </div>

                                     <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <input class="primary-input form-control{{ $errors->has('tag') ? ' is-invalid' : '' }}"
                                            type="text" name="tag" id="tag" autocomplete="off" value=" {{isset($editData)? Modules\CRM\Entities\SmTaskType::find($editData->type_id)->title : '' }}{{old('tag')}}" readonly>
                                            <label>Tags<span></span> </label>
                                            <span class="focus-border textarea"></span>
                                            @if ($errors->has('tag'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('tag') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                     <div class="col-lg-12 mb-20">
                                         <div class="input-effect">
                                             <select class="niceSelect w-100 bb form-control{{ $errors->has('priority') ? ' is-invalid' : '' }}" name="priority">
                                                 <option data-display="Priority" value="">Priority</option>
                                                 @foreach($priority as $key=>$value)
                                                 <option  value="{{$value}}"
                                                 {{ old('priority')==$value ? 'selected' : '' }}
                                                 @if(isset($editData))
                                                 @if($editData->priority == $value)
                                                     @lang('lang.selected')
                                                 @endif
                                                 @endif
                                                 >{{$value}}</option>
                                                 @endforeach
                                             </select>
                                             <span class="focus-border"></span>
                                             @if ($errors->has('priority'))
                                             <span class="invalid-feedback invalid-select" role="alert">
                                                 <strong>{{ $errors->first('priority') }}</strong>
                                             </span>
                                             @endif
                                         </div>
                                     </div>

                                     @if (!isset($editData))
                                     <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <div class="no-gutters input-right-icon">
                                                <div class="col">
                                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                                        <input class="primary-input date form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }}" value="{{isset($editData)? $editData->start_date : '' }}{{old('start_date')}}" type="text"
                                                            name="start_date" autocomplete="off" required="">
                                                        <label>Start Date</label>
                                                        <span class="focus-border"></span>
                                                        @if ($errors->has('start_date'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('start_date') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="start-date-icon"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     @endif 

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <div class="no-gutters input-right-icon">
                                                <div class="col">
                                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                                        <input class="primary-input date form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }}" value="{{isset($editData)? $editData->end_date : '' }}{{old('end_date')}}" type="text"
                                                            name="end_date" autocomplete="off" required="">
                                                        <label>End Date</label>
                                                        <span class="focus-border"></span>
                                                        @if ($errors->has('end_date'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('end_date') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="start-date-icon"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    
 
                                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 5)
                                        
                                        <div class="col-lg-12">
                                            <select class="niceSelect w-100 bb form-control mb-3" onchange="addStaffFunc()" name="role_id" id="role_id">
                                                <option data-display="Role" value=""> @lang('lang.select') </option>
                                                @foreach($departments as $key=>$value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                            <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="addStaff" onclick="addStaffButton()" >
                                                <img width="20px" src="https://static.thenounproject.com/png/261370-200.png" alt="">
                                                <label style="position: absolute; top: 53px; width: 100px">ADD STAFF</label>
                                            </button>
                                            <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="closeAddStaff" onclick="closeAddStaffButton()">
                                                <img style="margin-top: -3px" width="20px" src="https://static.thenounproject.com/png/261368-200.png" alt="">
                                                <label style="position: absolute; top: 50px; width: 100px">ADD STAFF</label> 
                                            </button>
                                            @foreach ($departments as $department)
                                            <div style="display: none" id="{{$department->id}}">
                                                @foreach ($staffs as $staff)
                                                    @if ($staff->department_id == $department->id && $staff->id != App\SmStaff::where('email', Auth::user()->email)->first()->id)
                                                        <div class="mt-2 {{ $errors->has('staffs') ? ' is-invalid' : '' }}" style="color: #828bb2;">
                                                            <input onclick="getStaffName()" @if(isset($editData)) @if(in_array($staff->id, $editData->assigned_users)) checked @endif @endif
                                                            @if(old('staffs')) @if(in_array($staff->id, old('staffs'))) checked @endif @endif 
                                                            type="checkbox" class="uncheck common-checkbox" name="staffs[]" value="{{$staff->id}}" id="{{$staff->email}}">
                                                            <label id="id{{$staff->id}}" for="{{$staff->email}}">{{$staff->full_name}}</label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="col-lg-12">
                                            <input type="hidden" value="{{App\SmStaff::where('email', Auth::user()->email)->first()->department_id}}" name="role_id" id="role_id_2">
                                            <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="addStaff" onclick="addStaffButton()" >
                                                <img width="20px" src="https://static.thenounproject.com/png/261370-200.png" alt="">
                                                <label style="position: absolute; top: 0px; width: 100px">ADD STAFF</label>
                                            </button>
                                            <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="closeAddStaff" onclick="closeAddStaffButton()">
                                                <img style="margin-top: -3px" width="20px" src="https://static.thenounproject.com/png/261368-200.png" alt="">
                                                <label style="position: absolute; top: -3px; width: 100px">ADD STAFF</label> 
                                            </button>
                                            @foreach ($departments as $department)
                                            <div style="display: none" id="{{$department->id}}">
                                                @foreach ($staffs as $staff)
                                                    @if ($staff->department_id == $department->id && $staff->id != App\SmStaff::where('email', Auth::user()->email)->first()->id)
                                                        <div class="mt-2 {{ $errors->has('staffs') ? ' is-invalid' : '' }}" style="color: #828bb2;">
                                                            <input onclick="getStaffName()" @if(isset($editData)) @if(in_array($staff->id, $editData->assigned_users)) checked @endif @endif 
                                                            @if(old('staffs')) @if(in_array($staff->id, old('staffs'))) checked @endif @endif
                                                            type="checkbox" class="uncheck common-checkbox" name="staffs[]" value="{{$staff->id}}" id="{{$staff->email}}">
                                                            <label id="id{{$staff->id}}" for="{{$staff->email}}">{{$staff->full_name}}</label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            @endforeach
                                        </div>
                                    @endif

                                    <div class="col-lg-12 mb-20">
                                        <div id="getStaff" class="input-effect">
                                            <label>Assigned Staffs<span></span></label> </br>
                                        </div>
                                     </div>

                                    {{-- <div class="col-lg-12">
                                        <div class="input-effect">
                                            <label for="cbx">Send notification to staffs ?</label>
                                            <input name="sendNoti" id="cbx" style="margin-left: 7px; position: absolute; top: 5px" type="checkbox"/>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                     </div> --}}
 
                                     <div class="col-lg-12 mb-20 mt-2">
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control" cols="0" rows="5" name="description" id="description">{{isset($editData) ? $editData->description : ''}}{{old('description')}}</textarea>
                                            <label>@lang('lang.description') <span></span> </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                     </div>

                              </div>
                                   @php 
                                   $tooltip = "";
                                   if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                                         $tooltip = "";
                                     }else{
                                         $tooltip = "You have no permission to add";
                                     }
                                 @endphp
                                 <div class="row mt-40">
                                     <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">
 
                                             <span class="ti-check"></span>
                                             @if(isset($editData))
                                                 @lang('lang.update')
                                             @else
                                                 @lang('lang.save')
                                             @endif
                                         </button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         {{ Form::close() }}
                     </div>
                 </div>
             </div>
 
             <div class="col-lg-9">
                
           <div class="row">
             <div class="col-lg-4 no-gutters">
                 <div class="main-title">
                     <h3 class="mb-0">Tasks List</h3>
                 </div>
             </div>
         </div>
 
         <div class="row">
 
             <div class="col-lg-12">
                 <table id="table_id" class="display school-table" cellspacing="0" width="100%">
 
                     <thead>
                         @if(session()->has('message-success-delete') != "" ||
                                 session()->get('message-danger-delete') != "")
                                 <tr>
                                     <td colspan="6">
                                          @if(session()->has('message-success-delete'))
                                           <div class="alert alert-success">
                                               {{ session()->get('message-success-delete') }}
                                           </div>
                                         @elseif(session()->has('message-danger-delete'))
                                           <div class="alert alert-danger">
                                               {{ session()->get('message-danger-delete') }}
                                           </div>
                                         @endif
                                     </td>
                                 </tr>
                              @endif
                         <tr>
                             <th>Tasks Name</th>
                             <th>Priority</th>
                             <th>Tasks Type</th>
                             <th>Created By</th>
                             <th>@lang('lang.action')</th>
                         </tr>
                     </thead>
 
                     <tbody>
                         @if(isset($filterTask))
                         @foreach($filterTask as $value)
                         <tr class="centerTd">
                             <td>{{$value->name_of_task}}</td>
                             <td @if($value->priority == "high") style="color: red" @endif>{{$value->priority}}</td>
                             <td>
                                <button class="{{Modules\CRM\Entities\SmTaskType::find($value->type_id)->color}} removeOutline" style="font-size: 11px;">
                                    {{Modules\CRM\Entities\SmTaskType::find($value->type_id)->title}}
                                </button>  
                            </td>                              
                             <td>{{App\SmStaff::find($value->created_by)->full_name}}</td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                         @lang('lang.select')
                                     </button>
                                     <div class="dropdown-menu dropdown-menu-right">

                                        @if(Auth::user()->role_id != 1 && $value->created_by != App\SmStaff::where('email', Auth::user()->email)->first()->id)
                                            <a class="dropdown-item" href="{{url('tasks/'.$value->id.'/planning')}}">Planning</a>
                                        @endif
                                        @if(Modules\CRM\Entities\SmTaskPlan::where('task_id', $value->id)->where('created_by', App\SmStaff::where('email', Auth::user()->email)->first()->id)->first())
                                            <a class="deleteUrl dropdown-item" href="{{url('tasks/'.$value->id.'/actual')}}" title="Task Level">Actual</a>
                                        @endif

                                        <a class="dropdown-item" href="{{url('tasks/'.$value->id)}}">@lang('lang.view')</a>
                                        
                                        @if(Auth::user()->role_id == 1 || $value->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id)
                                            <a class="dropdown-item" href="{{url('tasks/'.$value->id.'/edit')}}">@lang('lang.edit')</a> 
                                        @endif
                                        @if(Auth::user()->role_id == 1 || $value->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id)
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Disable Task" href="{{url('tasks/'.$value->id.'/disable-view')}}">disable</a>
                                        @endif
                                     </div>
                                 </div>
                             </td>
                         @endforeach
                         @endif
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>
 </div>
 </section>
 @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 5)
 <script>
    var id, oldId;
    var staffsName = [];
    var inputs = document.getElementsByClassName('uncheck'); 
    for(var i = 0; i < inputs.length; i++){
        if(inputs[i].checked){
            if(document.getElementById('getStaff').children.length >= 2){
                if(staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML) == -1){
                    var node = document.createElement("span");
                    node.className = "badge badge-dark";
                    var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                    node.appendChild(textnode);
                    document.getElementById("getStaff").appendChild(node);
                    staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                }
            }
            else{
                staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                var node = document.createElement("span");
                node.className = "badge badge-dark";
                var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                node.appendChild(textnode);
                document.getElementById("getStaff").appendChild(node);
            }
        }
        else{
            var index = staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML);
            if(index != -1){
                staffsName.splice(index, 1);
                document.getElementById('getStaff').children[index + 2].parentNode.removeChild(document.getElementById('getStaff').children[index + 2]); 
            }
        }
    }
    function addStaffFunc(){
        var x = document.getElementById("role_id").value;
        id = x;
        document.getElementById("addStaff").style.display = 'block';
        document.getElementById(oldId || x).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
    }
    function addStaffButton(){
        oldId = id;
        document.getElementById(id).style.display = 'block';
        document.getElementById('closeAddStaff').style.display = 'block';
        document.getElementById('addStaff').style.display = 'none';
    }
    function closeAddStaffButton(){
        document.getElementById(id).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
        document.getElementById('addStaff').style.display = 'block';
    }
    function getStaffName(){
        var inputs = document.getElementsByClassName('uncheck'); 
        for(var i = 0; i < inputs.length; i++){
            if(inputs[i].checked){
                if(document.getElementById('getStaff').children.length >= 2){
                    if(staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML) == -1){
                        var node = document.createElement("span");
                        node.className = "badge badge-dark";
                        var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                        node.appendChild(textnode);
                        document.getElementById("getStaff").appendChild(node);
                        staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                    }
                }
                else{
                    staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                    var node = document.createElement("span");
                    node.className = "badge badge-dark";
                    var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                    node.appendChild(textnode);
                    document.getElementById("getStaff").appendChild(node);
                }
            }
            else{
                var index = staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML);
                if(index != -1){
                    staffsName.splice(index, 1);
                    document.getElementById('getStaff').children[index + 2].parentNode.removeChild(document.getElementById('getStaff').children[index + 2]); 
                }
            }
        }
    }
</script>
@else
<script>
    var x = document.getElementById("role_id_2").value;
    id = x;
    var staffsName = [];
    document.getElementById("addStaff").style.display = 'block';
    document.getElementById(x).style.display = 'none';
    document.getElementById('closeAddStaff').style.display = 'none';
    function addStaffButton(){
        document.getElementById(id).style.display = 'block';
        document.getElementById('closeAddStaff').style.display = 'block';
        document.getElementById('addStaff').style.display = 'none';
    }
    function closeAddStaffButton(){
        document.getElementById(id).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
        document.getElementById('addStaff').style.display = 'block';
    }
    function getStaffName(){
        var inputs = document.getElementsByClassName('uncheck'); 
        for(var i = 0; i < inputs.length; i++){
            if(inputs[i].checked){
                if(document.getElementById('getStaff').children.length >= 2){
                    if(staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML) == -1){
                        var node = document.createElement("span");
                        node.className = "badge badge-dark";
                        var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                        node.appendChild(textnode);
                        document.getElementById("getStaff").appendChild(node);
                        staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                    }
                }
                else{
                    staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                    var node = document.createElement("span");
                    node.className = "badge badge-dark";
                    var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                    node.appendChild(textnode);
                    document.getElementById("getStaff").appendChild(node);
                }
            }
            else{
                var index = staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML);
                if(index != -1){
                    staffsName.splice(index, 1);
                    document.getElementById('getStaff').children[index + 2].parentNode.removeChild(document.getElementById('getStaff').children[index + 2]); 
                }
            }
        }
    }
</script>
@endif
 @endsection