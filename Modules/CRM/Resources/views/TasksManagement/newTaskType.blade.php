
@if(isset($editData))
    {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/edit-tags/'.$editData->id, 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
@else
    {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/task-type', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
@endif
    <div class="col-lg-12 mb-5">
        <div class="input-effect">
            <input class="primary-input form-control has-content"
            type="text" name="title" autocomplete="off" required value="@if(isset($editData)) {{$editData->title}} @endif">
            <label>Tags Title<span></span> </label>
            <span class="focus-border textarea"></span>
        </div>
    </div>
    <input type="hidden" id="color" name="color">
    <p id="choose" style="margin-left: 15px">Choose color : </p>
    <div style="margin: auto; width: 60%">
        <button onclick="getColor(this)" value="btn" type="button" class="btn"></button>
        <button onclick="getColor(this)" value="btn btn-dark" type="button" class="btn btn-dark"></button>
        <button onclick="getColor(this)" value="btn btn-primary" type="button" class="btn btn-primary"></button>
        <button onclick="getColor(this)" value="btn btn-success" type="button" class="btn btn-success"></button>
        <button onclick="getColor(this)" value="btn btn-info" type="button" class="btn btn-info"></button>
        <button onclick="getColor(this)" value="btn btn-warning" type="button" class="btn btn-warning"></button>
        <button onclick="getColor(this)" value="btn btn-danger" type="button" class="btn btn-danger"></button>
    </div>

    @if(isset($editData)) 
        <button class="primary-btn fix-gr-bg" type="submit">apply</button>
{{ Form::close() }}
        <a href="/tasks/delete-tags/{{$editData->id}}" >
            <button class="primary-btn fix-gr-bg" style="position: absolute; right: 10%; bottom: 19%">
                delete
            </button>
        </a>
        
    @else
        <button class="primary-btn fix-gr-bg mt-3" style="margin-left: 150px" type="submit">Apply</button>  
    @endif
{{ Form::close() }}
<script>
    function getColor(className){
        document.getElementById('color').value = className.value;
        var classes = ["btn", "btn btn-dark", "btn btn-primary", "btn btn-success", "btn btn-info", "btn btn-warning", "btn btn-danger"];
        var colors = ["", "#343a40", "#007bff", "#28a745", "#17a2b8", "#ffc107", "#dc3545"];
        var index = classes.indexOf(className.value);
        document.getElementById('choose').style.color = colors[index];
    }
</script>

