@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Planning task</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-8 col-md-6">
                <div class="main-title">
                    <h3 class="mb-30">Plan Information</h3>
                </div>
            </div>
            <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg">
                <a href="/tasks/add" class="primary-btn small fix-gr-bg">
                    @lang('lang.all') tasks
                </a>
            </div>
        </div>
        @if(isset($editData))
        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/'. $id . '/planning/edit', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        @else
        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/' . $id . '/planning', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        @endif
        <div class="row">
            <div class="col-lg-12">
                @if(session()->has('message-success'))
                <div class="alert alert-success">
                  {{ session()->get('message-success') }}
              </div>
              @elseif(session()->has('message-danger'))
              <div class="alert alert-danger">
                  {{ session()->get('message-danger') }}
              </div>
              @endif
              <div class="white-box">
                <div class="">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-title">
                                <h4>@lang('lang.basic_info')</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-20">
                        <div class="col-lg-12">
                            <hr>
                        </div>
                    </div>

                    <input type="hidden" name="url" id="url" value="{{URL::to('/')}}"> 

                    <div class="row mb-30">
                        <div class="col-lg-12">
                            <div class="input-effect">
                                <textarea class="primary-input form-control" cols="0" rows="3" name="plan" required>{{isset($editData) ? $editData->planning : ''}}</textarea>
                                <label>What's your plan ?<span>*</span> </label>
                                <span class="focus-border textarea"></span>
                            </div>
                        </div>

                        <div class="col-lg-12 mt-4">
                            <div class="input-effect">
                                <textarea class="primary-input form-control" cols="0" rows="3" name="target" required>{{isset($editData) ? $editData->target : ''}}</textarea>
                                <label>Target<span>*</span> </label>
                                <span class="focus-border textarea"></span>
                            </div>
                        </div>

                        <div class="col-lg-12 mt-4">
                            <div class="input-effect">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control{{ $errors->has('expected_date') ? ' is-invalid' : '' }}" type="text"
                                                name="expected_date" autocomplete="off" required="" value="{{isset($editData) ? $editData->expected_date : ''}}">
                                            <label>Expected complete date</label>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('expected_date'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('expected_date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="" type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 mt-5">
                            <div class="row no-gutters input-right-icon">
                                <div class="col">
                                    <div class="input-effect">
                                        <input class="primary-input" type="text" id="placeholderFileOneName" placeholder="Report File" readonly="">
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button class="primary-btn-small-input" type="button">
                                        <label class="primary-btn small fix-gr-bg"
                                               for="document_file_1">@lang('lang.browse')</label>
                                        <input type="file" class="d-none" name="file" id="document_file_1">
                                    </button>
                                </div>
                            </div>
                        </div>
                    
                    </div>
            <div class="row mt-40">
                <div class="col-lg-12 text-center">
                    <button class="primary-btn fix-gr-bg">
                        <span class="ti-check"></span>
                        @if(isset($editData))
                            @lang('lang.update') 
                        @else
                            @lang('lang.save') 
                        @endif
                    </button>
                </div>
            </div>
    </div>
    {{ Form::close() }}
    </div>
</section>


@endsection
