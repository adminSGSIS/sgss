@extends('backEnd.master')
@section('css')
<style>
   .removeOutline:focus {
        box-shadow: none;
   }
   .editInline p{
        display: inline;
   }
</style>
@endsection
@section('mainContent')
@php  $setting = App\SmGeneralSettings::find(1); if(!empty($setting->currency_symbol)){ $currency = $setting->currency_symbol; }else{ $currency = '$'; } @endphp
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Task Details</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="{{url('tasks/add')}}">All Tasks</a>
            </div>
            
        </div>
    </div>
</section>
<section class="mb-40 student-details">
    @if(session()->has('message-success'))
    <div class="alert alert-success">
        {{ session()->get('message-success') }}
    </div>
    @elseif(session()->has('message-danger'))
    <div class="alert alert-danger">
        {{ session()->get('message-danger') }}
    </div>
    @endif
    <div class="container-fluid p-0">
        <div class="row">
         <div class="col-lg-3">
            <div class="main-title">
                <h3 class="mb-20">Creator Details</h3>
            </div>
            <div class="student-meta-box">
                <div class="student-meta-top"></div>

                <img class="student-meta-img img-100" src="{{ file_exists(@$staffDetails->staff_photo) ? asset($staffDetails->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"  alt="">
                <div class="white-box">
                    <div class="single-meta mt-10">
                        <div class="d-flex justify-content-between">
                            <div class="name">
                                @lang('lang.staff_name')
                            </div>
                            <div class="value">

                                @if(isset($staffDetails)){{$staffDetails->full_name}}@endif

                            </div>
                        </div>
                    </div>
                    <div class="single-meta">
                        <div class="d-flex justify-content-between">
                            <div class="name">
                                @lang('lang.role') 
                            </div>
                            <div class="value">
                               @if(isset($staffDetails)){{$staffDetails->roles->name}}@endif
                           </div>
                       </div>
                   </div>
                   <div class="single-meta">
                    <div class="d-flex justify-content-between">
                        <div class="name">
                            @lang('lang.designation')
                        </div>
                        <div class="value">
                           @if(isset($staffDetails)){{ !empty($staffDetails->designations)?$staffDetails->designations->title:''}}@endif
                            
                       </div>
                   </div>
               </div>
               <div class="single-meta">
                <div class="d-flex justify-content-between">
                    <div class="name">
                        @lang('lang.department')
                    </div>
                    <div class="value">
                           @if(isset($staffDetails)){{ !empty($staffDetails->departments)?$staffDetails->departments->name:''}}@endif 
                    </div>
                </div>
            </div>
            <div class="single-meta">
                <div class="d-flex justify-content-between">
                    <div class="name">
                        @lang('lang.epf_no')
                    </div>
                    <div class="value">
                       @if(isset($staffDetails)){{$staffDetails->epf_no}}@endif
                   </div>
               </div>
           </div>
           <div class="single-meta">
            <div class="d-flex justify-content-between">
                <div class="name">
                    @lang('lang.basic_salary')
                </div>
                <div class="value">
                    ({{$currency}}) @if(isset($staffDetails)){{$staffDetails->basic_salary}}@endif
                </div>
            </div>
        </div>
        <div class="single-meta">
            <div class="d-flex justify-content-between">
                <div class="name">
                    @lang('lang.contract_type')
                </div>
                <div class="value">
                   @if(isset($staffDetails)){{$staffDetails->contract_type}}@endif
               </div>
           </div>
       </div>
       <div class="single-meta">
        <div class="d-flex justify-content-between">
            <div class="name">
                @lang('lang.date_of_joining')
            </div>
            <div class="value">
                @if(isset($staffDetails))
               
{{$staffDetails->date_of_joining != ""? App\SmGeneralSettings::DateConvater($staffDetails->date_of_joining):''}}


                @endif
            </div>
        </div>
    </div>
</div>
</div>
</div>

<div class="col-lg-9 staff-details">
    <ul class="nav nav-tabs tabs_scroll_nav"  role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="#studentProfile" role="tab" data-toggle="tab">Information</a>
        </li>
        @if (Auth::user()->role_id == 1 || $task->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id)
        @if(isset($task))
            @foreach ($task->assigned_users as $index=>$item)
                <li class="nav-item">
                    <a class="nav-link" href="#id{{$item}}" role="tab" data-toggle="tab">Staff {{$index + 1}}</a>
                </li>
            @endforeach
        @endif
        @endif
        <li class="nav-item">
            <a class="nav-link" href="#activity" role="tab" data-toggle="tab">Activity</a>
        </li>
        
    </ul>

<div class="tab-content">
    <!-- Start Profile Tab -->
    <div role="tabpanel" class="tab-pane fade show active" id="studentProfile">
        <div class="white-box">
            <h4 class="stu-sub-head">Task @lang('lang.info')</h4>
            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-5">
                        <div class="">
                            Task Title 
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-6">
                        <div class="">
                            @if(isset($task)){{$task->name_of_task}}@endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="">
                           Priority
                       </div>
                   </div>

                   <div class="col-lg-7 col-md-7">
                        <div @if($task->priority == "high") style="color: red" @endif class="">
                                @if(isset($task)){{$task->priority}}@endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="">
                            Task Type
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-7">
                        <div class="">
                            @if(isset($task))
                            <button class="{{Modules\CRM\Entities\SmTaskType::find($task->type_id)->color}} removeOutline" style="font-size: 11px;">
                                {{Modules\CRM\Entities\SmTaskType::find($task->type_id)->title}}
                            </button>
                            
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="">
                            Task Status
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-7">
                        <div class="">
                            @if(isset($task))  
                                @if ($task->active_status == 0)
                                    Finished
                                @else
                                    {{floor(rtrim(Modules\CRM\Entities\SmTaskStatus::find($task->status)->status, "%") * 100) / 100}}%  
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="">
                            Start Date
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-7">
                        <div class="">
                            @if(isset($task)) {{$task->start_date}} @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="">
                            End Date
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-7">
                        @if (strtotime($task->end_date) < strtotime(date('Y-m-d')))
                            <div style="color: red" class="">
                                @if(isset($task)) {{$task->end_date}} - TASK EXPIRED @endif
                            </div>
                        @else
                            <div class="">
                                @if(isset($task)) {{$task->end_date}} @endif
                            </div>
                        @endif
                        
                    </div>
                </div>
            </div>
            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="">
                            Assigned Staffs
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-7">
                        @foreach ($task->assigned_users as $item)
                            <div class="">
                                {{App\SmStaff::find($item)->full_name}}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (Auth::user()->role_id == 1 || $task->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id)
    @if(isset($task))
        @foreach ($task->assigned_users as $item)
        <div role="tabpanel" class="tab-pane fade" id="id{{$item}}">
            <div class="white-box">
                <h4 class="stu-sub-head">@lang('lang.personal') Staff @lang('lang.info')</h4>
                <div class="single-info">
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <div class="">
                                Full Name
                            </div>
                        </div>
    
                        <div class="col-lg-7 col-md-6">
                            <div class="">
                                {{App\SmStaff::find($item)->full_name}}
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="single-info">
                    <div class="row">
                        <div class="col-lg-5 col-md-6">
                            <div class="">
                               Role
                           </div>
                       </div>
    
                       <div class="col-lg-7 col-md-7">
                            <div class="">
                                {{App\SmStaff::find($item)->roles->name}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-info">
                    <div class="row">
                        <div class="col-lg-5 col-md-6">
                            <div class="">
                                Designation
                           </div>
                       </div>
    
                       <div class="col-lg-7 col-md-7">
                            <div class="">
                                {{App\SmStaff::find($item)->designations->title}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-info">
                    <div class="row">
                        <div class="col-lg-5 col-md-6">
                            <div class="">
                                Department
                           </div>
                       </div>
    
                       <div class="col-lg-7 col-md-7">
                            <div class="">
                                {{App\SmStaff::find($item)->departments->name}}
                            </div>
                        </div>
                    </div>
                </div>
            
                @if (Modules\CRM\Entities\SmTaskPlan::where('task_id', $task->id)->where('created_by', $item)->first())
                    <h4 class="stu-sub-head mt-4">task planning</h4>
                    <div class="single-info">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <div class="">
                                    Planned
                            </div>
                        </div>
        
                        <div class="col-lg-7 col-md-7">
                                <div class="">
                                    {{Modules\CRM\Entities\SmTaskPlan::where('task_id', $task->id)->where('created_by', $item)->first()->planning}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="single-info">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <div class="">
                                    Target
                            </div>
                        </div>
        
                        <div class="col-lg-7 col-md-7">
                                <div class="">
                                    {{Modules\CRM\Entities\SmTaskPlan::where('task_id', $task->id)->where('created_by', $item)->first()->target}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="single-info">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <div class="">
                                    Expected Complete Date
                            </div>
                        </div>
        
                        <div class="col-lg-7 col-md-7">
                                <div class="">
                                    {{Modules\CRM\Entities\SmTaskPlan::where('task_id', $task->id)->where('created_by', $item)->first()->expected_date}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="single-info">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <div class="">
                                    Actual
                            </div>
                        </div>
        
                        <div class="col-lg-7 col-md-7">
                                <div class="">
                                    {{Modules\CRM\Entities\SmTaskPlan::where('task_id', $task->id)->where('created_by', $item)->first()->complete_level}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="single-info">
                        <div class="row">
                            <div class="col-lg-5 col-md-6">
                                <div class="">
                                    Reported File
                            </div>
                        </div>
        
                        <div class="col-lg-7 col-md-7">
                                <div class="">
                                    <a href="{{Modules\CRM\Entities\SmTaskPlan::where('task_id', $task->id)->where('created_by', $item)->first()->file}}" download>
                                        {{Modules\CRM\Entities\SmTaskPlan::where('task_id', $task->id)->where('created_by', $item)->first()->file_name}}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                
            </div>
        </div>
        @endforeach
    @endif
    @endif

    <div role="tabpanel" class="tab-pane fade" id="activity">
        <div class="white-box">
            <h4 class="stu-sub-head">Comment</h4>
            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/activity',
                            'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                <input type="hidden" name="task_id" value="{{$task->id}}">
                <div style="border: none" class="single-info">
                    <div class="col-12">
                        <div class="row mt-2">
                            <div class="col-lg-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control summernote" cols="0" rows="1" name="comment" required></textarea>
                                    <span class="focus-border textarea"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-lg-12 text-center">
                        <button type="submit" class="btn btn-success removeOutline">
                            <span class="ti-check"></span>
                            @lang('lang.add')
                        </button>
                    </div>
                </div>
            {{ Form::close() }}
            <h4 class="stu-sub-head mt-3">Task Activities</h4>
            <div class="single-info">
                <div class="row">
                    @foreach ($activities as $item)
                        <div class="col-12" >
                            <img style="border-radius: 50%; width: 40px" src="{{ file_exists(@$staffDetails->staff_photo) ? asset($staffDetails->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"  alt="">
                            <div style="display: inline">
                                <label style="font-weight: 700; color: #000000a1" class="ml-2">{{App\SmStaff::find($item->created_by)->full_name}}</label>
                                <label class="ml-2">{{$item->created_at}}</label>
                            </div>
                            <div style="width: 80%; background-color: #f9f9f5; border-radius: 10px" class="ml-5 p-2 editInline">
                                {!!$item->activity!!}
                            </div>
                        </div>

                        
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
    
</div>

</section>
@endsection
@section('script')
<script type="text/javascript">
    $('.summernote').summernote({

       toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['insert', ['link', 'picture', 'video']],
],
});

</script>


@endsection