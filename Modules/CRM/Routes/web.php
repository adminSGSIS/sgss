<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//customer facebook
Route::prefix('customer-facebook')->group(function() {
    Route::get('/search-customer', 'CustomerFacebookController@searchCustomer');
    Route::post('/search-customer', 'CustomerFacebookController@listsCustomer');
    Route::get('/add', 'CustomerFacebookController@createCustomer');
    Route::post('/add', 'CustomerFacebookController@storeCustomer');
    Route::get('/{id}', 'CustomerFacebookController@viewCustomer');
    Route::get('/{id}/edit', 'CustomerFacebookController@editCustomer');
    Route::post('/{id}/edit', 'CustomerFacebookController@updateCustomer');
    Route::get('/{id}/delete-view', 'CustomerFacebookController@deleteView');
    Route::get('/{id}/delete', 'CustomerFacebookController@deleteCustomer');
});

//task management
Route::prefix('tasks')->group(function() {
    Route::get('/task-type/view', 'TasksController@taskTypeView');
    Route::post('/task-type', 'TasksController@newTaskType');
    Route::get('/choose-tags', 'TasksController@chooseTags');
    Route::get('/edit-tags/{id}', 'TasksController@editTags');
    Route::post('/edit-tags/{id}', 'TasksController@updateTags');
    Route::get('/delete-tags/{id}', 'TasksController@deleteTags');
    Route::get('/add', 'TasksController@index');
    Route::post('/add', 'TasksController@store');
    Route::get('/disabled', 'TasksController@disabledTask');
    Route::get('/{id}/planning', 'TasksController@planningTask');
    Route::post('/{id}/planning', 'TasksController@storePlanningTask');
    Route::get('/{id}/planning/edit', 'TasksController@editPlanningTask');
    Route::post('/{id}/planning/edit', 'TasksController@updatePlanningTask');
    Route::get('/{id}/actual', 'TasksController@changeTaskLevel');
    Route::post('/{id}/actual', 'TasksController@updateActual');
    Route::get('/{id}/edit', 'TasksController@editTask');
    Route::post('/{id}/edit', 'TasksController@updateTask');
    Route::get('/{id}/disable-view', 'TasksController@disableView');
    Route::get('/{id}/disable', 'TasksController@disableTask');
    Route::get('/{id}/unable-view', 'TasksController@unableView');
    Route::get('/{id}/unable', 'TasksController@unableTask');
    Route::get('/{id}/delete', 'TasksController@deleteTask');
    Route::get('/{id}/delete-view', 'TasksController@deleteView');
    Route::post('/activity', 'TasksController@storeActivity');
    Route::get('/{id}', 'TasksController@viewTask');
});
