<?php

namespace Modules\ChatBox\Entities;

use Illuminate\Database\Eloquent\Model;

class converImages extends Model
{
    protected $table ="conversation_images";

    public function Detail()
    {
    	return $this->BelongsTo('Modules\ChatBox\Entities\conversationDetail','message_id','id');
    }
}
