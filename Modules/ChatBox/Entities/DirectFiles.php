<?php

namespace Modules\ChatBox\Entities;

use Illuminate\Database\Eloquent\Model;

class DirectFiles extends Model
{
    protected $table ="direct_files";
}
