<?php

namespace Modules\ChatBox\Entities;

use Illuminate\Database\Eloquent\Model;

class MessageOneOne extends Model
{
    protected $table ="message_one_one";

    public function details()
    {
    	return $this->hasmany('Modules\ChatBox\Entities\MessageOneOneDetail','id_message','id');
    }
}
