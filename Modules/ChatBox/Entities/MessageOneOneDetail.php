<?php

namespace Modules\ChatBox\Entities;

use Illuminate\Database\Eloquent\Model;

class MessageOneOneDetail extends Model
{
    protected $table ="message_one_one_detail";
    public function images()
    {
    	return $this->hasMany('Modules\ChatBox\Entities\MessageOneOneImage','message_detail_id','id');
    }
    public function files()
    {
    	return $this->hasOne('Modules\ChatBox\Entities\DirectFiles','direct_detail_id','id');
    }
}
