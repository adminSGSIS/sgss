<?php

namespace Modules\ChatBox\Entities;

use Illuminate\Database\Eloquent\Model;

class MessageOneOneImage extends Model
{
    protected $table ="message_one_one_image";
}
