<?php

namespace Modules\ChatBox\Entities;

use Illuminate\Database\Eloquent\Model;

class conversation extends Model
{
    public function User()
    {
    	return $this->BelongsToMany('App\User','conversation_users','conversation_id','user_id');
    }

    public function details()
    {
    	return $this->hasmany('Modules\ChatBox\Entities\conversationDetails','conversation_id','id');
    }
}
