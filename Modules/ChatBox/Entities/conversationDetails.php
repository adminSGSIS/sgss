<?php

namespace Modules\ChatBox\Entities;

use Illuminate\Database\Eloquent\Model;

class conversationDetails extends Model
{
    protected $table ="conversation_details";

    public function User()
    {
    	return $this->BelongsTo('App\User','posted_by','id');
    }

    public function images()
    {
    	return $this->hasMany('Modules\ChatBox\Entities\ConverImages','conversation_id','id');
    }

    public function files()
    {
    	return $this->hasOne('Modules\ChatBox\Entities\GroupFiles','group_detail_id','id');
    }
}
