<?php

namespace Modules\ChatBox\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddToGroup implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $ids;
    public $group_id;
    public $name;
    public $subject;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ids,$group_id,$name,$subject)
    {
        $this->ids = $ids;
        $this->group_id = $group_id;
        $this->name = $name;
        $this->subject = $subject;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return new Channel('add-to-group');
    }
}
