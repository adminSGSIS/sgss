<?php

namespace Modules\ChatBox\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CreateConversation implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $id;
    public $name;
    public $conv_name;
    public $conv_subject;
    public $conv_id;
    public $id_from;
    public $name_from;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($id,$name,$conv_id,$id_from,$name_from,$conv_name,$conv_subject)
    {
        $this->id = $id;
        $this->name = $name;
        $this->conv_name = $conv_name;
        $this->conv_subject = $conv_subject;
        $this->conv_id = $conv_id;
        $this->id_from = $id_from;
        $this->name_from = $name_from;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('create-conversation');
    }
}
