<?php

namespace Modules\ChatBox\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\response;
use Modules\ChatBox\Events\NewMessage;
use Modules\ChatBox\Events\NewMessage2;
use Modules\ChatBox\Events\CreateConversation;
use Modules\ChatBox\Events\AddToGroup;
use Modules\ChatBox\Events\DeleteFromGroup;
use Modules\ChatBox\Events\DeleteGroup;
use App\User;
use Modules\ChatBox\Entities\conversation;
use Modules\ChatBox\Entities\conversation_users;
use Modules\ChatBox\Entities\conversationDetails;
use Modules\ChatBox\Entities\ConverImages;
use Auth;
use Pusher;
use App\Role;
use Illuminate\Routing\Controller;
use App\ApiBaseMethod;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use DB;
use App\SmStaff;
use Modules\ChatBox\Entities\MessageOneOne;
use Modules\ChatBox\Entities\MessageOneOneDetail;
use Modules\ChatBox\Entities\MessageOneOneImage;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function index()
    {

        $cons = User::find(Auth::user()->id)->Conversation;    
        $users = User::all()->where('id','!=',Auth::user()->id);
        foreach ($cons as $con) {
            $lastest = conversationDetails::where('conversation_id',$con->id)->orderBy('created_at','desc')->first();
            if($lastest != null )
            {
                $con->lastest_message = $lastest->detail;
            }
            
        }
        $cons = $cons->sortByDesc('updated_at');

        return view('chatbox::mainpage',compact('cons','users'));
    }

    public function submit(Request $request)
    {
        $check = conversation_users::where('conversation_id',$request->conv_id)->where('user_id',Auth::user()->id)->first();
        if(isset($check))
        {
        $conDetail = new conversationDetails();
        $conDetail->conversation_id = $request->conv_id;
        $conDetail->posted_by = $request->auth_id;
        $conDetail->detail = $request->message;
        $conDetail->save();
        // if($request->img_source != 'null')
        // {

        //     $image = new ConverImages();
        //     $image->url = $request->img_source;
        //     $image->message_id = $conDetail->id;
        //     $image->conversation_id = $request->conv_id;
        //     $image->posted_by = Auth::user()->id;
        //     $image->save();
            
        // }
        $changestatus = conversation::find($request->conv_id);
        $changestatus->status = 0;
        $changestatus->save();
        $id_receivers = conversation_users::all()->where('conversation_id',$request->conv_id)->where('user_id','!=',Auth::user()->id);
        foreach ($id_receivers as $id_receiver) {
            $ids [] = $id_receiver->user_id;
        }
        $staff_photo = SmStaff::where('user_id',$request->auth_id)->first();
        if(isset($staff_photo) && $staff_photo->staff_photo != null)
            {
                $image = $staff_photo->staff_photo;
            }
        else
            {
                $image = 'public/sg_star_logo.png';
            }
        $data = [
        'mes_id' => $conDetail->id,
        'message' => $request->message,
        'conv_id' => $request->conv_id,
        'posted_by' => Auth::user()->id,
        'name' => Auth::user()->full_name,
        'ids' =>$ids,
        'staff_photo' => $image,
        'img_source' =>'null',
        ];
        broadcast(new NewMessage($data))->toOthers();
        
        return response()->json(['message' => $data], 200);
        }
        else
        {
            return "You cannot listen on this channle!!";
        }
    }

    public function add()
    {        
        $users = User::all()->where('id','!=',Auth::user()->id);
        return view('chatbox::create',compact('users'));
    }

    public function createGroup(Request $request)
    {
        $input = $request->all();       
            $validator = Validator::make($input, [
                'user_ids'=>"required",
            ]);
            $conversation = new conversation();
            $conversation->created_by = Auth::user()->id;
            $conversation->name = $request->conv_name;
            //$conversation->subject = $request->subject;
            $conversation->save();

            $con_user = new conversation_users();
            $con_user->user_id = Auth::user()->id;
            $con_user->conversation_id = $conversation->id;
            $con_user->save();

            foreach ($request->user_ids as $value) {
                $con_user = new conversation_users();
                $con_user->user_id = $value;
                $con_user->conversation_id = $conversation->id;
                $con_user->save();
            }
        
            Toastr::success('Operation Success', 'Success');
            return redirect()->back();
    }

    public function checkAuth(Request $request)
    {
        $user_id = Auth::user()->id;
        $check = conversation_users::where('user_id','=',$user_id)->where('conversation_id','=',$request->request->get('conver_iv'))->first();
        if($check != null)
        {
            $pusher = new Pusher\Pusher(
            'd269c80a507e99df75c3',
            'a0b9c8c82353064a5647', 
            '1077907'
            );
            return $pusher->socket_auth($request->request->get('channel_name'),$request->request->get('socket_id')); 
            
        }
        else
        {
            //return new response('forbidden',403);
        }      
    }



    public function addMultiple()
    {
        $users = User::all()->where('id','!=',Auth::user()->id);
        return view('chatbox::create_multiple',compact('users'));
    }

    public function createMultiple(Request $request)
    {
        
        foreach ($request->user_ids as $value) 
        {   
            $find = MessageOneOne::where('id_from',Auth::user()->id)->where('id_to',$value)->first();
            if(isset($find))
            {
                $message_detail = new MessageOneOneDetail();
                $message_detail->posted_by = Auth::user()->id;
                $message_detail->id_message = $find->id;
                $message_detail->detail = $request->detail;
                $message_detail->save();

                $changestatus = MessageOneOne::find($find->id);
                $changestatus->status = 1;
                $changestatus->save();
            }
            else
            {
                $find2 = MessageOneOne::where('id_from',$value)->where('id_to',Auth::user()->id)->first();
                if(isset($find2))
                {
                    $message_detail = new MessageOneOneDetail();
                    $message_detail->posted_by = Auth::user()->id;
                    $message_detail->id_message = $find2->id;
                    $message_detail->detail = $request->detail;
                    $message_detail->save();

                    $changestatus = MessageOneOne::find($find2->id);
                    $changestatus->status = 1;
                    $changestatus->save();
                }
                else
                {
                    $message = new MessageOneOne();
                    $message->id_from = Auth::user()->id;
                    $message->id_to = $value;
                    $message->lastest_sender = Auth::user()->id;
                    $message->status = 1;
                    $message->save(); 
                    $message_detail = new MessageOneOneDetail();
                    $message_detail->posted_by = Auth::user()->id;
                    $message_detail->id_message = $message->id;
                    $message_detail->detail = $request->detail;
                    $message_detail->save();
                }
            }
                            
        }       
        Toastr::success('Operation Success', 'Success');
        return redirect()->back();
    }


    public function leave($conv_id,$user_id)
    {

        try{
            $leave = conversation_users::where('conversation_id',$conv_id)->where('user_id',$user_id)->delete();
            Toastr::success('Operation Success', 'Success');
            return redirect()->back();
        }
        catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function delete($conv_id)
    {
         $con = conversation::find($conv_id);
        if(Auth::user()->id == $con->created_by)
        {
            try{
                $con->delete();
                $con_user = conversation_users::where('conversation_id',$conv_id)->delete();                
                DB::table('conversation_details')->where('conversation_id', $conv_id)->delete(); 
                // Toastr::success('Operation Successful', 'Deleted');
                // return redirect('chatbox');  
                broadcast(new DeleteGroup($conv_id))->toOthers();
            }
            catch (\Exception $e) {
               Toastr::error('Operation Failed', 'Failed');
               return redirect()->back(); 
            }
        }
        else
        {
            return "You are not the author of this conversation";
        }
                 
    }

    public function refresh_chat_list()
    {
        $cons = User::find(Auth::user()->id)->Conversation;          
        foreach ($cons as $con) {
            foreach ($con->User as  $user) {
                $full_name = $user->full_name;
                $participant[] = "<label class='btn btn-success btn-sm'>".$full_name."</lable>"; 
               
                
            } 
            $con->participants=$participant;
            $participant =[];         
        }  
        $cons = $cons->sortByDesc('updated_at');
        return response()->json([
            'data' => $cons,
        ]);
    }

    public function getAddtogroup($id)
    {
        $members = conversation_users::where('conversation_id',$id)->get();
        foreach ($members as $member) {
            $member_ids [] = $member->user_id;
        }
        $users = User::all()->where('role_id','!=',2)->whereNotIn('id',$member_ids);
        return $users;
    }

    public function Addtogroup(Request $request)
    {
        foreach ($request->user_ids as $value) {
            $add = new conversation_users();
            $add->user_id = $value;
            $add->conversation_id = $request->group_id;
            $add->save();
            $names [] = User::find($value);
        }
        $ids = $request->user_ids;
        $group_id = $request->group_id;
        $name = conversation::find($request->group_id)->name;
        $subject = conversation::find($request->group_id)->subject;
        broadcast(new AddToGroup($ids,$group_id,$name,$subject))->toOthers();
        return response()->json([
            'names' => $names,
        ]);
    }

    public function kickgroup ($group_id,$user_id)
    {
        $kick = conversation_users::where('conversation_id',$group_id)->where('user_id',$user_id)->delete();

        broadcast(new DeleteFromGroup($group_id,$user_id))->toOthers();
        return "delete user ".$user_id;
    }

    public function loadmore($group_id,$node_id)
    {
        $first_id = conversationDetails::where('conversation_id',$group_id)->orderBy('id','asc')->first()->id;
        if($first_id != $node_id){
            $details = conversationDetails::where('conversation_id',$group_id)->where('id','<',$node_id)->orderBy('id','desc')->take(10)->get();
                $node_id = $details->sortBy('id')->first();
                //$details = $details1->sortBy('created_at');
                //$details = conversationDetails::where('conversation_id',$id)->oldest('created_at')->limit(10)->get();
                // $users = conversation::find($id)->User;
                // $users = $users->where('id','!=',Auth::user()->id);
                foreach ($details as  $detail) {
                    $detail->user_create = User::find($detail->posted_by)->full_name;
                    $staff_photo = SmStaff::where('user_id',$detail->posted_by)->first();
                    if(isset($staff_photo) && $staff_photo->staff_photo != null)
                    {
                        $detail->image = $staff_photo->staff_photo;
                    }
                    else
                    {
                        $detail->image = 'public/sg_star_logo.png';
                    }
                    if($detail->Images)
                    {
                        $detail->img_source = $detail->Images->url;
                    }
                    else
                    {
                        $detail->img_source = 'null';
                    }
                }

                $created_by = conversation::find($group_id)->created_by;
                //return view('chatbox::mainpage',compact('details','created_by','cons','id','users'));
                return response()->json([
                        'details' => $details,
                        'node_id' => $node_id,
                        'first_id' => $first_id,
                        

                ]);
        }
        else
        {
            return "top reached";
        }
    }
}