<?php

namespace Modules\ChatBox\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\response;
use Modules\ChatBox\Events\NewMessage;
use Modules\ChatBox\Events\NewMessage2;
use Modules\ChatBox\Events\CreateConversation;
use Modules\ChatBox\Events\MessageAlert;
use App\User;
use Modules\ChatBox\Entities\conversation;
use Modules\ChatBox\Entities\conversation_users;
use Modules\ChatBox\Entities\conversationDetails;
use Modules\ChatBox\Entities\ConverImages;
use Modules\ChatBox\Entities\MessageOneOne;
use Modules\ChatBox\Entities\MessageOneOneDetail;
use Modules\ChatBox\Entities\MessageOneOneImage;
use Modules\ChatBox\Entities\DirectFiles;
use Modules\ChatBox\Entities\GroupFiles;
use Auth;
use Pusher;
use Carbon\Carbon;
use App\Role;
use Illuminate\Routing\Controller;
use App\ApiBaseMethod;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use DB;
use App\SmStaff;
use App\SmParent;
use Cache;
use Image;

class ChatController extends Controller
{
    public function submit(Request $request)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    	$staff_photo = SmStaff::where('user_id',Auth::user()->id)->first();
        if(isset($staff_photo) && $staff_photo->staff_photo != null)
            {
                $image = $staff_photo->staff_photo;
            }
        else
            {
                $image = 'public/sg_star_logo.png';
            }
        
        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
        
        if($request->mes_type == "direct")
        {
            $last_mess = MessageOneOneDetail::where('id_message', $request->cur_mes_id)->orderBy('updated_at', 'desc')->first();
        	$newMes = new MessageOneOneDetail();
        	if($last_mess){
                $diff = Carbon::create($last_mess->created_at->year, $last_mess->created_at->month, $last_mess->created_at->day, 
                        $last_mess->created_at->hour, $last_mess->created_at->minute, $last_mess->created_at->second)->diffInMinutes(Carbon::now());
                    
            	if($diff > 30){
            	    $newMes->difference_time = "y";
            	}
        	}else{
        	    $newMes->difference_time = "y";
        	}
        	$newMes->posted_by = Auth::user()->id;
        	$newMes->id_message = $request->cur_mes_id;
        	$newMes->detail = $request->cur_mes_detail;
        	$newMes->save();
            
            $update = MessageOneOne::find($request->cur_mes_id);
            $update->status = 1;
            $update->lastest_sender = Auth::user()->id;
            $update->updated_day = date("Y-m-d H-i-s");
            $update->save();
            
            $incomUserId = $update->id_from != Auth::user()->id ? $update->id_from : $update->id_to;
            $checkOnline = Cache::has('user-is-online-' . $incomUserId);
            
        	$data = [
	        'detail_id' => $newMes->id,
	        'message' => $request->cur_mes_detail,
	        'mes_id' => $request->cur_mes_id,
	        // 'posted_by' => Auth::user()->id,
	        'name' => Auth::user()->full_name,
	        'id_sender' => Auth::user()->id,
	        'staff_photo' => $image,
	        'img_source' =>[],
	        'files_source' =>[],
	        ];
        	broadcast(new NewMessage2($data))->toOthers();
        	if(!$checkOnline){
        	    return response()->json([
            	    'check'=>$checkOnline
            	]);
        	}
        }
        elseif ($request->mes_type == "group") 
        {
            $last_mess = conversationDetails::where('conversation_id', $request->cur_mes_id)->orderBy('updated_at', 'desc')->first();
            
        	$newGrMes = new conversationDetails();
        	if($last_mess){
        	    $diff = Carbon::create($last_mess->created_at->year, $last_mess->created_at->month, $last_mess->created_at->day, 
                    $last_mess->created_at->hour, $last_mess->created_at->minute, $last_mess->created_at->second)->diffInMinutes(Carbon::now());
                
            	if($diff > 30){
            	    $newGrMes->difference_time = "y";
            	}
        	}else{
        	    $newGrMes->difference_time = "y";
        	}
        	
        	$newGrMes->posted_by = Auth::user()->id;
        	$newGrMes->conversation_id = $request->cur_mes_id;
        	$newGrMes->detail = $request->cur_mes_detail;
        	$newGrMes->save();
        	
         	$gr_cons = conversation::find($newGrMes->conversation_id);
        	$gr_cons->updated_at = date('Y-m-d G:i:s');
        	$gr_cons->save();
        	
        	$conversation_users = conversation_users::where('conversation_id', $newGrMes->conversation_id)->get();
        	foreach($conversation_users as $item){
        	    $item->updated_at = date('Y-m-d G:i:s');
        	    $item->save();
        	}
        	
        	$data = [
	        'gr_detail_id' => $newGrMes->id,
	        'message' => $request->cur_mes_detail,
	        'mes_id' => $request->cur_mes_id,
	        // 'posted_by' => Auth::user()->id,
	        'name' => Auth::user()->full_name,
	        'id_sender' => Auth::user()->id,
	        'staff_photo' => $image,
	        'img_source' =>[],
	        'files_source' =>[],
	        ];
        	broadcast(new NewMessage($data))->toOthers();
        }
        
    }

    public function getroles($id)
    {
    	$users = User::all()->where('role_id',$id);
    	return response()->json([
    		'users' => $users,
    	]);
    }

    public function imgUpload(Request $request)
    {
        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
            
        $last_mess = MessageOneOneDetail::where('id_message', $request->cur_mes_id)->orderBy('updated_at', 'desc')->first();
    	$newMes = new MessageOneOneDetail();
    	
    	if($last_mess){
    	    $diff = Carbon::create($last_mess->created_at->year, $last_mess->created_at->month, $last_mess->created_at->day, 
                $last_mess->created_at->hour, $last_mess->created_at->minute, $last_mess->created_at->second)->diffInMinutes(Carbon::now());
            
        	if($diff > 30){
        	    $newMes->difference_time = "y";
        	}
    	}else{
    	    $newMes->difference_time = "y";
    	}
    	
    	$newMes->posted_by = Auth::user()->id;
    	$newMes->id_message = $request->cur_mes_id;
    	$newMes->save();
    	$location = "Modules/ChatBox/public/images/";
    	
    	$update = MessageOneOne::find($request->cur_mes_id);
        $update->status = 1;
        $update->lastest_sender = Auth::user()->id;
        $update->updated_day = date("Y-m-d H-i-s");
        $update->save();
        
        $incomUserId = $update->id_from != Auth::user()->id ? $update->id_from : $update->id_to;
        $checkOnline = Cache::has('user-is-online-' . $incomUserId);
    	
    	foreach ($request->images as $image) {
            $img = Image::make($image);
            $h = $img->height();
            $w = $img->width();

            if($h > 600)
            {
                $height = 600;
            }
            else{
                $height = $h;
            }


            if($w > 1000)
            {
                $width = 1000;
            }
            else{
                $width = $w;
            }

            $wt [] = $width;
            $ht [] = $height;

    		$filename = md5($image->getClientOriginalName()).'.jpg';
    		//$image->move($location,$filename);
            $img->fit($width,$height)->save($location.$filename);
    		$data []= $filename; 
    		$newImg = new MessageOneOneImage();
    		$newImg->message_id = $request->cur_mes_id;
    		$newImg->message_detail_id = $newMes->id;
    		$newImg->url = $location.$filename;
    		$newImg->save();

    	}
    	$staff_photo = SmStaff::where('user_id',Auth::user()->id)->first();
        if(isset($staff_photo) && $staff_photo->staff_photo != null)
            {
                $image = $staff_photo->staff_photo;
            }
        else
            {
                $image = 'public/sg_star_logo.png';
            }
    	$message = [
        'mes_id' => $request->cur_mes_id,
        'message' => '',
        'detail_id' => $newMes->id,
        // 'posted_by' => Auth::user()->id,
        'name' => Auth::user()->full_name,
        'id_sender' => Auth::user()->id,
        'staff_photo' => $image,
        'img_source' =>$data,
        'files_source' =>[],
        ];
        broadcast(new NewMessage2($message))->toOthers();

    	if(!$checkOnline){
    	    return response()->json([
    	        'images'=>$data,
        		'cur_mes_id' => $request->cur_mes_id,
        		'detail_id' =>$newMes->id,
        	    'check'=>$checkOnline,
                'width' => $wt,
                'height' => $ht
                
        	]);
    	}else{
    	    return response()->json([
        		'images'=>$data,
        		'cur_mes_id' => $request->cur_mes_id,
        		'detail_id' =>$newMes->id,
                'width' => $wt,
                'height' => $ht
            
        	]);
    	}
    }

    public function groupimgUpload(Request $request)
    {
        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
            
        $last_mess = conversationDetails::where('conversation_id', $request->cur_mes_id)->orderBy('updated_at', 'desc')->first();
        
    	$newGrMes = new conversationDetails();
    	if($last_mess){
    	    $diff = Carbon::create($last_mess->created_at->year, $last_mess->created_at->month, $last_mess->created_at->day, 
                $last_mess->created_at->hour, $last_mess->created_at->minute, $last_mess->created_at->second)->diffInMinutes(Carbon::now());
            
        	if($diff > 30){
        	    $newGrMes->difference_time = "y";
        	}
    	}else{
    	    $newGrMes->difference_time = "y";
    	}
    	$newGrMes->conversation_id = $request->cur_mes_id;
    	$newGrMes->posted_by = Auth::user()->id;
    	$newGrMes->save();
    	$location = "Modules/ChatBox/public/images/";
    	foreach ($request->images as $image) {
            $img = Image::make($image);
            $h = $img->height();
            $w = $img->width();

            if($h > 600)
            {
                $height = 600;
            }
            else{
                $height = $h;
            }


            if($w > 1000)
            {
                $width = 1000;
            }
            else{
                $width = $w;
            }
    		$filename = md5($image->getClientOriginalName()).'.jpg';
    		//$image->move($location,$filename);
            $img->fit($width,$height)->save($location.$filename);
    		$data []= $filename; 

    		$newGrImg = new ConverImages();
    		$newGrImg->conversation_id = $newGrMes->id;
    		$newGrImg->message_id = $request->cur_mes_id;
    		$newGrImg->url = $location.$filename;
    		$newGrImg->save();
    	}
    	$staff_photo = SmStaff::where('user_id',Auth::user()->id)->first();
        if(isset($staff_photo) && $staff_photo->staff_photo != null)
            {
                $image = $staff_photo->staff_photo;
            }
        else
            {
                $image = 'public/sg_star_logo.png';
            }
    	$message = [
        'mes_id' => $request->cur_mes_id,
        'message' => '',
        'gr_detail_id' => $newGrMes->id,
        // 'posted_by' => Auth::user()->id,
        'name' => Auth::user()->full_name,
        'id_sender' => Auth::user()->id,
        'staff_photo' => $image,
        'img_source' =>$data,
        'files_source' =>[],
        ];
        broadcast(new NewMessage($message))->toOthers();

    	return response()->json([
    		'images'=>$data,
    		'cur_mes_id' => $request->cur_mes_id,
    		'gr_detail_id' =>$newGrMes->id,
    	]);
    }

    public function fileUpload(Request $request)
    {
        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
            
        $update = MessageOneOne::find($request->cur_mes_id);
        $update->status = 1;
        $update->lastest_sender = Auth::user()->id;
        $update->updated_day = date("Y-m-d H-i-s");
        $update->save();
        
        $incomUserId = $update->id_from != Auth::user()->id ? $update->id_from : $update->id_to;
        $checkOnline = Cache::has('user-is-online-' . $incomUserId);
        
    	$location = "Modules/ChatBox/public/files/";
    	foreach ($request->files_send as $file) {
    		
    		$filename = $file->getClientOriginalName();
    		$file->move($location,$filename);
    		$data []= $filename; 

            $last_mess = MessageOneOneDetail::where('id_message', $request->cur_mes_id)->orderBy('updated_at', 'desc')->first();
        	
    		$newMes = new MessageOneOneDetail();
    		if($last_mess){
        	    $diff = Carbon::create($last_mess->created_at->year, $last_mess->created_at->month, $last_mess->created_at->day, 
                    $last_mess->created_at->hour, $last_mess->created_at->minute, $last_mess->created_at->second)->diffInMinutes(Carbon::now());
                
            	if($diff > 30){
            	    $newMes->difference_time = "y";
            	}
        	}else{
        	    $newMes->difference_time = "y";
        	}
        	
	    	$newMes->id_message = $request->cur_mes_id;
	    	$newMes->posted_by = Auth::user()->id;
	    	$newMes->save();
	    	$detail_ids []=$newMes->id;

	    	$newFile = new DirectFiles();
	    	$newFile->direct_id = $request->cur_mes_id;
	    	$newFile->direct_detail_id = $newMes->id;
	    	$newFile->url=$location.$filename;
	    	$newFile->save();
    	}

    	$staff_photo = SmStaff::where('user_id',Auth::user()->id)->first();
        if(isset($staff_photo) && $staff_photo->staff_photo != null)
            {
                $image = $staff_photo->staff_photo;
            }
        else
            {
                $image = 'public/sg_star_logo.png';
            }
        $message = [
        'mes_id' => $request->cur_mes_id,
        'message' => '',
        'detail_id' => $detail_ids,
        // 'posted_by' => Auth::user()->id,
        'name' => Auth::user()->full_name,
        'id_sender' => Auth::user()->id,
        'staff_photo' => $image,
        'img_source' =>[],
        'files_source' =>$data,
        ];
        broadcast(new NewMessage2($message))->toOthers();
    	
    	if(!$checkOnline){
    	    return response()->json([
        		'files'=>$data,
        		'cur_mes_id' => $request->cur_mes_id,
        		'check' => $checkOnline
        	]);
    	}else{
    	    return response()->json([
        		'files'=>$data,
        		'cur_mes_id' => $request->cur_mes_id,
        	]);
    	}
    }

     public function groupfileUpload(Request $request)
    {
        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
            
    	$location = "Modules/ChatBox/public/files/";
    	foreach ($request->gr_files_send as $file) {
    		
    		$filename = $file->getClientOriginalName();
    		$file->move($location,$filename);
    		$data []= $filename; 

            $last_mess = conversationDetails::where('conversation_id', $request->cur_mes_id)->orderBy('updated_at', 'desc')->first();
    		$newGrMes = new conversationDetails();
    		if($last_mess){
        	    $diff = Carbon::create($last_mess->created_at->year, $last_mess->created_at->month, $last_mess->created_at->day, 
                    $last_mess->created_at->hour, $last_mess->created_at->minute, $last_mess->created_at->second)->diffInMinutes(Carbon::now());
                
            	if($diff > 30){
            	    $newGrMes->difference_time = "y";
            	}
        	}else{
        	    $newGrMes->difference_time = "y";
        	}
	    	$newGrMes->conversation_id = $request->cur_mes_id;
	    	$newGrMes->posted_by = Auth::user()->id;
	    	$newGrMes->save();
	    	$detail_ids []=$newGrMes->id;

	    	$newGrFile = new GroupFiles();
	    	$newGrFile->group_id = $request->cur_mes_id;
	    	$newGrFile->group_detail_id = $newGrMes->id;
	    	$newGrFile->url=$location.$filename;
	    	$newGrFile->save();
    	}

    	$staff_photo = SmStaff::where('user_id',Auth::user()->id)->first();
        if(isset($staff_photo) && $staff_photo->staff_photo != null)
            {
                $image = $staff_photo->staff_photo;
            }
        else
            {
                $image = 'public/sg_star_logo.png';
            }
        $message = [
        'mes_id' => $request->cur_mes_id,
        'message' => '',
        'detail_id' => $detail_ids,
        // 'posted_by' => Auth::user()->id,
        'name' => Auth::user()->full_name,
        'id_sender' => Auth::user()->id,
        'staff_photo' => $image,
        'img_source' =>[],
        'files_source' =>$data,
        ];
        broadcast(new NewMessage($message))->toOthers();
    	return response()->json([
    		'files'=>$data,
    		'cur_mes_id' => $request->cur_mes_id,
    	]);
    }
    
    public function updateStatus($id)
    {
        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
            
        $update = MessageOneOne::find($id);
        $lastest_mess = MessageOneOneDetail::where('posted_by', $update->lastest_sender)->where('id_message', $update->id)->orderBy('updated_at', 'desc')->first();
        if($update->status == 1 && isset($lastest_mess)){
            if($lastest_mess->posted_by != Auth::user()->id){
                $update->status = 0;
                $update->save();
                $message = [
                    'changeStatus' => true,
                    'mes_id' => $id,
                    'id_sender' => Auth::user()->id
                ];
                broadcast(new NewMessage2($message))->toOthers();
            }
        }
    }
    
    public function getNamesImages($id)
    {
        foreach(conversation_users::where('conversation_id', $id)->get() as $item){
            $names[] = substr(User::find($item->user_id)->full_name, 0, 27);
            $user = User::find($item->user_id);
            if ($user->role_id == 2) {
                $LoginUser = SmStudent::where('user_id', $user->id)->first();
                $profile = $LoginUser ? $LoginUser->student_photo : '';
            } elseif ($user->role_id == 3) {
                $LoginUser = SmParent::where('user_id', $user->id)->first();
                $profile = $LoginUser ? $LoginUser->fathers_photo : '';
            } else {
                $LoginUser = SmStaff::where('user_id', $user->id)->first();
                $profile = $LoginUser ? $LoginUser->staff_photo : '';
            }
            $avts[] = file_exists($profile) ? $profile : 'public/uploads/staff/demo/staff.jpg';
        }
        return response()->json([
            'names' => $names,
            'avt' => $avts
        ]);
    }
    
    public function getImgList($id)
    {
        $images = ConverImages::where('message_id',$id)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'images' => $images
        ]);
    }


    public function getFileList($id)
    {
        $files = GroupFiles::where('group_id',$id)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'files' => $files
        ]);
    }
    
    public function getInfoImgFile($id)
    {
        $images = MessageOneOneImage::where('message_id', $id)->orderBy('created_at', 'desc')->get();
        $files = DirectFiles::where('direct_id', $id)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'images' => $images,
            'files' => $files
        ]);
    }
}
