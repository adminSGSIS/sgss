<?php

namespace Modules\ChatBox\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\Admin\Chat;
use App\User;
use Modules\ChatBox\Entities\conversation;
use Modules\ChatBox\Entities\conversation_users;
use Modules\ChatBox\Entities\conversationDetails;
use Modules\ChatBox\Entities\ConverImages;
use Auth;
use Illuminate\Routing\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use DB;
use Modules\ChatBox\Events\NewMessage;
use Modules\ChatBox\Events\CreateConversation;
use App\SmStaff;

class MessageController extends Controller
{
    public function getDetails($id)
    {
        try{
        $user_id = Auth::user()->id;
        $users = User::all()->where('id','!=',$user_id);
        $changestatus = conversation::find($id);
        $changestatus->status = 1;
        $changestatus->save();       
        $check = conversation_users::where('user_id',$user_id)->where('conversation_id',$id)->get();
            if($check->count()>0)
            {
                $cons = User::find(Auth::user()->id)->Conversation; 
                $cons = $cons->sortByDesc('updated_at');      
                foreach ($cons as $con) {
                    $lastest = conversationDetails::where('conversation_id',$con->id)->orderBy('created_at','desc')->first();
                    $con->lastest_message = 'asdsadasd';
                }           
                $details = conversationDetails::where('conversation_id',$id)->orderBy('created_at','asc')->get();
                foreach ($details as  $detail) {
                    $detail->user_create = User::find($detail->posted_by)->full_name;
                    $staff_photo = SmStaff::where('user_id',$detail->posted_by)->first();
                    if(isset($staff_photo) && $staff_photo->staff_photo != null)
                    {
                        $detail->image = $staff_photo->staff_photo;
                    }
                    else
                    {
                        $detail->image = '/public/sg_star_logo.png';
                    }
                }

                $created_by = conversation::find($id)->created_by;
                return view('chatbox::mainpage',compact('details','created_by','cons','id','users'));
               
            }
            else{
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
        
    }

    public function getDetails2($id)
    {
       // try{
        $user_id = Auth::user()->id;
        $users = User::all()->where('id','!=',$user_id);
        $changestatus = conversation::find($id);
        $changestatus->status = 1;
        $changestatus->save();       
        $check = conversation_users::where('user_id',$user_id)->where('conversation_id',$id)->get();
            if($check->count()>0)
            {
                $auth = Auth::user()->id;  
                $details = conversationDetails::where('conversation_id',$id)->orderBy('id','desc')->take(10)->get();
                $node_id = $details->sortBy('id')->first()->id;
                //$details = $details1->sortBy('created_at');
                //$details = conversationDetails::where('conversation_id',$id)->oldest('created_at')->limit(10)->get();
                $users = conversation::find($id)->User;
                $users = $users->where('id','!=',Auth::user()->id);
                foreach ($details as  $detail) {
                    $detail->user_create = User::find($detail->posted_by)->full_name;
                    $staff_photo = SmStaff::where('user_id',$detail->posted_by)->first();
                    if(isset($staff_photo) && $staff_photo->staff_photo != null)
                    {
                        $detail->image = $staff_photo->staff_photo;
                    }
                    else
                    {
                        $detail->image = 'public/sg_star_logo.png';
                    }
                    if($detail->Images)
                    {
                        $detail->img_source = $detail->Images->url;
                    }
                    else
                    {
                        $detail->img_source = 'null';
                    }
                }

                $created_by = conversation::find($id)->created_by;
                //return view('chatbox::mainpage',compact('details','created_by','cons','id','users'));
                return response()->json([
                        'details' => $details,
                        'created_by' =>$created_by,
                        'auth' =>$auth,
                        'node_id' =>$node_id,
                        'id' => $id,
                        'users' =>$users,

                ]);
            }
            else{
                echo "This channel does not exists or you don't have authorization to view or listen on this channel !!";
            }
        // }catch (\Exception $e) {
        //    Toastr::error('Operation Failed', 'Failed');
        //    return redirect()->back(); 
        // }
        
    }

    public function edit($id)
    {
        try{
            $conv = conversation::find($id);
            $edit = $conv->User;
            $users = User::all()->where('id','!=',Auth::user()->id);
            return view('chatbox::edit',compact('users','edit','conv'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
       
    }

    public function update(Request $request)
    {
        try{
            if(count($request->user_ids)>=1)
            {
                $con = conversation::find($request->id_conv);
                $con->name = $request->conv_name ? $request->conv_name : $con->name;
                $con->subject = $request->subject ? $request->subject : $con->subject;        
                conversation_users::where('conversation_id',$request->id_conv)->delete();
                foreach ($request->user_ids as $user_id) {
                    $update = new conversation_users();
                    $update->conversation_id = $request->id_conv;
                    $update->user_id = $user_id;
                    $update->save();
                    $id[] = $user_id;
                    $name [] = User::find($user_id)->full_name;
                    
                }
                $update1 = new conversation_users();
                $update1->conversation_id = $request->id_conv;
                $update1->user_id = Auth::user()->id;
                $update1->save();

                $con->save();
                $conv_id [] = $request->id_conv;
                $id_from = Auth::user()->id;
                $name_from = Auth::user()->full_name;
                broadcast(new CreateConversation($id,$name,$conv_id,$id_from,$name_from,$request->conv_name,$request->subject))->toOthers();
            }
            else{
                $con = conversation::find($request->id_conv)->delete();
                $con_user = conversation_users::where('conversation_id',$request->id_conv)->delete();                
                DB::table('conversation_details')->where('conversation_id', $request->id_conv)->delete(); 
                Toastr::success('Operation Successful', 'Deleted');
                return redirect()->back(); 
            }              
            Toastr::success('Operation Success', 'Success');
            return redirect()->back();   
        }
        catch (\Exception $e)
        {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function imgUpload(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file;
            $location = "Modules/ChatBox/public/image_message";
            $filename = md5($file->getClientOriginalName());
            $file->move($location,$filename);

            $message = new conversationDetails();
            $message->conversation_id = $request->conv_id;
            $message->posted_by = Auth::user()->id;
            $message->detail = '';
            $message->save();

            $img = new ConverImages();
            $img->url = $location."/".$filename;
            $img->conversation_id = $request->conv_id;
            $img->message_id = $message->id;
            $img->posted_by = Auth::user()->id;
            $img->save();

            $id_receivers = conversation_users::all()->where('conversation_id',$request->conv_id)->where('user_id','!=',Auth::user()->id);
            foreach ($id_receivers as $id_receiver) {
                $ids [] = $id_receiver->user_id;
            }
            $staff_photo = SmStaff::where('user_id',$request->auth_id)->first();
            if(isset($staff_photo) && $staff_photo->staff_photo != null)
                {
                    $image = $staff_photo->staff_photo;
                }
            else
                {
                    $image = 'public/sg_star_logo.png';
                }
            $data = [
            'mes_id' => $message->id,
            'message' => '',
            'conv_id' => $request->conv_id,
            'posted_by' => Auth::user()->id,
            'name' => Auth::user()->full_name,
            'ids' =>$ids,
            'staff_photo' => $image,
            'img_source' => $location."/".$filename,
            ];
            broadcast(new NewMessage($data))->toOthers();
            //echo $location."/".$filename;
        }
    }

    public function postImage(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file;
            $location = "Modules/ChatBox/public/image_message";
            $filename = md5($file->getClientOriginalName());
            $file->move($location,$filename);

            $message = new conversationDetails();
            $message->conversation_id = $request->message_id;
            $message->posted_by = Auth::user()->id;
            $message->detail = '';
            $message->save();

            $img = new ConverImages();
            $img->url = $location."/".$filename;
            $img->conversation_id = $request->message_id;
            $img->message_id = $message->id;
            $img->posted_by = Auth::user()->id;
            $img->save();

            $id_receivers = conversation_users::all()->where('conversation_id',$request->message_id)->where('user_id','!=',Auth::user()->id);
            foreach ($id_receivers as $id_receiver) {
                $ids [] = $id_receiver->user_id;
            }
            $staff_photo = SmStaff::where('user_id',$request->auth_id)->first();
            if(isset($staff_photo) && $staff_photo->staff_photo != null)
                {
                    $image = $staff_photo->staff_photo;
                }
            else
                {
                    $image = 'public/sg_star_logo.png';
                }
            $data = [
            'mes_id' => $message->id,
            'message' => '',
            'conv_id' => $request->message_id,
            'posted_by' => Auth::user()->id,
            'name' => Auth::user()->full_name,
            'ids' =>$ids,
            'staff_photo' => $image,
            'img_source' =>$location."/".$filename,
            ];
            broadcast(new NewMessage($data))->toOthers();

            return response()->json([
                'detail' =>$data,
            ]);
            
        
        }
    }
}
