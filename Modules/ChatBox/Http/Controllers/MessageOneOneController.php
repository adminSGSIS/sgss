<?php

namespace Modules\ChatBox\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\response;
use Modules\ChatBox\Events\NewMessage;
use Modules\ChatBox\Events\NewMessage2;
use Modules\ChatBox\Events\CreateConversation;
use Modules\ChatBox\Events\MessageAlert;
use App\User;
use Modules\ChatBox\Entities\conversation;
use Modules\ChatBox\Entities\conversation_users;
use Modules\ChatBox\Entities\conversationDetails;
use Modules\ChatBox\Entities\ConverImages;
use Modules\ChatBox\Entities\MessageOneOne;
use Modules\ChatBox\Entities\MessageOneOneDetail;
use Modules\ChatBox\Entities\MessageOneOneImage;
use Modules\ChatBox\Events\AddToGroup;
use Auth;
use Pusher;
use Carbon\Carbon;
use App\Role;
use Illuminate\Routing\Controller;
use App\ApiBaseMethod;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use DB;
use App\SmStaff;
use App\SmParent;
use App\SmStudent;
use App\SmGeneralSettings;
use App\SmDateFormat;
use Spatie\GoogleCalendar\Event;
use Cache;

class MessageOneOneController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function sendMessage(Request $request)
    {
        $find = MessageOneOne::where('id_from',Auth::user()->id)->where('id_to',$request->id_user)->first();
        if(isset($find))
        {
            $message_details = MessageOneOneDetail::where('id_message',$find->id)->orderBy('id','desc')->take(10)->get();
            $node_id2 = $message_details->sortBy('id')->first();
            foreach ($message_details as $message_detail) {
                    $staff_photo = SmStaff::where('user_id',$message_detail->posted_by)->first();
                    $message_detail->staff_name = User::find($message_detail->posted_by)->full_name;
                    $img = $message_detail->image;
                    if(isset($img))
                    {
                        $message_detail->img_source = $img->url;
                    }
                    else
                    {
                        $message_detail->img_source = "null";
                    }
                    if(isset($staff_photo) && $staff_photo->staff_photo != null)
                        {
                            $message_detail->staff_photo = $staff_photo->staff_photo;
                        }
                    else
                        {
                            $message_detail->staff_photo = 'public/sg_star_logo.png';
                        }

                }
            $changeStatus = MessageOneOne::find($find->id);
           
                $changeStatus->status = 0  ;
                $changeStatus->save();
            
            
            return response()->json([
                'details' => $message_details,
                'id_one_one' => $find->id,
                'node_id2' => $node_id2,
                
            ]);
        }
        else{
            $find2 = MessageOneOne::where('id_from',$request->id_user)->where('id_to',Auth::user()->id)->first();
            if(isset($find2))
            {
                $message_details = MessageOneOneDetail::where('id_message',$find2->id)->orderBy('id','desc')->take(10)->get();
                $node_id2 = $message_details->sortBy('id')->first();
                foreach ($message_details as $message_detail) {
                    $staff_photo = SmStaff::where('user_id',$message_detail->posted_by)->first();
                    $message_detail->staff_name = User::find($message_detail->posted_by)->full_name;
                    $img = $message_detail->image;
                    if(isset($img))
                    {
                        $message_detail->img_source = $img->url;
                    }
                    else
                    {
                        $message_detail->img_source = "null";
                    }
                    if(isset($staff_photo) && $staff_photo->staff_photo != null)
                        {
                            $message_detail->staff_photo = $staff_photo->staff_photo;
                        }
                    else
                        {
                            $message_detail->staff_photo = 'public/sg_star_logo.png';
                        }
                    
                }
                $changeStatus = MessageOneOne::find($find2->id);
                
                    $changeStatus->status = 0  ;
                    $changeStatus->save();
                
                return response()->json([
                    'details' => $message_details,
                    'id_one_one' =>$find2->id,
                    'node_id2' => $node_id2,
                    
                ]);
            }
            else{
                $message = new MessageOneOne();
                $message->id_from = Auth::user()->id;
                $message->id_to = $request->id_user;
                $message->save();                
                // $message_detail = new MessageOneOneDetail();
                // $message_detail->posted_by = Auth::user()->id;
                // $message_detail->id_message = $message->id;
                // $message_detail->detail = 'Hello!';
                // $message_detail->save();

                $message_details = MessageOneOneDetail::where('id_message',$message->id)->get();
                $node_id2 = $message_details->sortBy('id')->first();
                foreach ($message_details as $message_detail) {
                    $staff_photo = SmStaff::where('user_id',$message_detail->posted_by)->first();
                    $message_detail->staff_name = User::find($message_detail->posted_by)->full_name;
                    if(isset($staff_photo) && $staff_photo->staff_photo != null)
                        {
                            $message_detail->staff_photo = $staff_photo->staff_photo;
                        }
                    else
                        {
                            $message_detail->staff_photo = 'public/sg_star_logo.png';
                        }
                    $message_detail->img_source ="null";
                }
                return response()->json([
                    'details' => $message_details,
                    'id_one_one' =>$message->id,
                    'node_id2' => $node_id2,
                    
                ]);
            }
        }
    }

    public function submitOneOne(Request $request)
    {
        $message_detail = new MessageOneOneDetail();
        $message_detail->posted_by = Auth::user()->id;
        $message_detail->id_message = $request->id_one_one;
        $message_detail->detail = $request->message;
        $message_detail->save();

        $id_user = Auth::user()->id;
        $ids = $request->receive_user;

        $changeStatus = MessageOneOne::find($request->id_one_one);
        $changeStatus->lastest_sender = Auth::user()->id;
        $changeStatus->status =1 ;
        $changeStatus->save();
        
        $staff_photo = SmStaff::where('user_id',$id_user)->first();
        if(isset($staff_photo) && $staff_photo->staff_photo != null)
            {
                $image = $staff_photo->staff_photo;
            }
        else
            {
                $image = 'public/sg_star_logo.png';
            }
        $data = [
        'mes_id' => $message_detail->id,
        'message' => $request->message,
        'conv_id' => $request->id_one_one,
        'posted_by' => Auth::user()->id,
        'name' => Auth::user()->full_name,
        'ids' =>$ids,
        'id_sender' => $id_user,
        'staff_photo' => $image,
        'img_source' =>'null',
        ];
        $data2 =[
            'conv_id' => $request->id_one_one,
            'receiver' => $request->receive_user,
            'sender' => Auth::user()->id,
            'sender_name' =>Auth::user()->full_name,
            'photo' => $image,
        ];
        broadcast(new NewMessage2($data))->toOthers();
        broadcast(new MessageAlert($data2))->toOthers();
        return $data;
    }

    public function postImage2(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file;
            $location = "Modules/ChatBox/public/image_message";
            $filename = md5($file->getClientOriginalName());
            $file->move($location,$filename);

            $message = new MessageOneOneDetail();
            $message->id_message = $request->id_one_to_one;
            $message->posted_by = Auth::user()->id;
            $message->detail = '';
            $message->save();

            $img = new MessageOneOneImage();
            $img->url = $location."/".$filename;
            $img->message_detail_id = $message->id;
            $img->message_id = $request->id_one_to_one;
            
            $img->save();

            $ids = $request->receive_user;
            $id_sender = Auth::user()->id;
            
            $changeStatus = MessageOneOne::find($request->id_one_to_one);
            $changeStatus->status =1 ;
            $changeStatus->save();
            
            $staff_photo = SmStaff::where('user_id',Auth::user()->id)->first();
            if(isset($staff_photo) && $staff_photo->staff_photo != null)
                {
                    $image = $staff_photo->staff_photo;
                }
            else
                {
                    $image = 'public/sg_star_logo.png';
                }
            $data = [
            'mes_id' => $message->id,
            'message' => '',
            'conv_id' => $request->id_one_to_one,
            'posted_by' => Auth::user()->id,
            'name' => Auth::user()->full_name,
            'ids' =>$ids,
            'staff_photo' => $image,
            'id_sender' => $id_sender,
            'img_source' =>$location."/".$filename,
            ];
            $data2 =[
            'conv_id' => $request->id_one_one,
            'receiver' => $request->receive_user,
            'sender' => Auth::user()->id,
            'sender_name' =>Auth::user()->full_name,
            'photo' => $image,
            ];
            broadcast(new MessageAlert($data2))->toOthers();
            broadcast(new NewMessage2($data))->toOthers();

            return response()->json([
                'detail' =>$data,
            ]);
        }
    }


    public function loadmore2($talk_id,$node_id2)
    {
        $first_id = MessageOneOneDetail::where('id_message',$talk_id)->orderBy('id','asc')->first()->id;
        if(isset($first_id))
        {
            if($first_id != $node_id2){
                $details = MessageOneOneDetail::where('id_message',$talk_id)->where('id','<',$node_id2)->orderBy('id','desc')->take(10)->get();
                    $node_id2 = $details->sortBy('id')->first();
                    
                    foreach ($details as  $detail) {
                        $detail->user_create = User::find($detail->posted_by)->full_name;
                        $staff_photo = SmStaff::where('user_id',$detail->posted_by)->first();
                        if(isset($staff_photo) && $staff_photo->staff_photo != null)
                        {
                            $detail->image = $staff_photo->staff_photo;
                        }
                        else
                        {
                            $detail->image = 'public/sg_star_logo.png';
                        }
                        if($detail->Images)
                        {
                            $detail->img_source = $detail->Images->url;
                        }
                        else
                        {
                            $detail->img_source = 'null';
                        }
                    }

                    //$created_by = conversation::find($talk_id)->created_by;
                    //return view('chatbox::mainpage',compact('details','created_by','cons','id','users'));
                    return response()->json([
                            'details' => $details,
                            'node_id2' => $node_id2,
                            'first_id' => $first_id,
                            

                    ]);
            }
            else
            {
                return "top reached";
            }
        }
    }
    
    public function chats()
    {   
        $expiresAt = Carbon::now()->addMinutes(5);
        Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
        
		$teachers = User::all()->where('role_id',4);
        $parents = User::all()->where('role_id',3);
        $users = User::all()->where('role_id','!=','2')->where('id','!=',Auth::user()->id)->where('role_id','!=','4')->where('role_id','!=','3');
        $recentOneOne = MessageOneOne::orderBy('updated_day', 'desc')->get();
        $cons = $recentOneOne->filter(function ($user) {
            return $user->id_to == Auth::user()->id || $user->id_from == Auth::user()->id;
        });
        $recentGroup = conversation_users::where('user_id', Auth::user()->id)->orderBy('updated_at', 'desc')->get();
        return view('chatbox::chats.chat',compact('cons', 'recentGroup','users','teachers','parents'));
    }
    
    public function chatsContact()
    {
        $users = User::where('role_id', '!=', 2)->where('id', '!=', Auth::user()->id)->orderBy('full_name', 'asc')->get();
        $role = [];
        foreach($users as $user){
            if(!in_array($user->role_id, $role)){
                $role[] = $user->role_id;
            }
        }
        return view('chatbox::chats.list_users',compact('role', 'users'));
    }
    
    public function chatsCons()
    {
        try{
            $users = User::where('role_id', '!=', 2)->orderBy('full_name', 'asc')->get();
            $role = [];
            foreach($users as $user){
                if(!in_array($user->role_id, $role)){
                    $role[] = $user->role_id;
                }
            }
            return view('chatbox::chats.cons', compact('role', 'users'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function chatsGroup()
    {
        try{
            $users = User::where('role_id', '!=', 2)->orderBy('full_name', 'asc')->get();
            $role = [];
            foreach($users as $user){
                if(!in_array($user->role_id, $role)){
                    $role[] = $user->role_id;
                }
            }
            return view('chatbox::chats.groupchat', compact('role', 'users'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function chatsMultiple()
    {
        try{
            $users = User::where('role_id', '!=', 2)->orderBy('full_name', 'asc')->get();
            $role = [];
            foreach($users as $user){
                if(!in_array($user->role_id, $role)){
                    $role[] = $user->role_id;
                }
            }
            return view('chatbox::chats.multiple', compact('role', 'users'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function chatsOption()
    {
        try{
            return view('chatbox::chats.option');
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function grchatsLoadMore($id, $take_number)
    {
        try{
            $mes = conversationDetails::where('conversation_id', $id)->orderBy('updated_at', 'desc')->take($take_number + 10)->get();
            for($i = $take_number; $i < $mes->count() ; $i++){
                $filterMes[] = $mes[$i];
            }
            $avt = [];
            $time = [];
            $images = [];
            $incomFullName = [];
            foreach($filterMes as $index=>$item){
                $generalSetting = SmGeneralSettings::find(1);
                $system_date_foramt = SmDateFormat::find($generalSetting->date_format_id);
                $DATE_FORMAT =  $system_date_foramt->format;
                $formatted = date_format(date_create($item->created_at), $DATE_FORMAT);
                $time[] = str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT) .' : '. str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT) . ' | ' . $formatted;
                if($item->posted_by != Auth::user()->id){
                    $item->images;
                    $item->files;
                    $user = User::find($item->posted_by);
                    $incomFullName[$index] = $user->full_name;
                    if ($user->role_id == 2) {
                        $LoginUser = SmStudent::where('user_id', $user->id)->first();
                        $profile = $LoginUser ? $LoginUser->student_photo : '';
                    } elseif ($user->role_id == 3) {
                        $LoginUser = SmParent::where('user_id', $user->id)->first();
                        $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                    } else {
                        $LoginUser = SmStaff::where('user_id', $user->id)->first();
                        $profile = $LoginUser ? $LoginUser->staff_photo : '';
                    }
                    $avt[$index] = file_exists($profile) ? $profile : 'public/uploads/staff/demo/staff.jpg';
                }
                else{
                    $item->images;
                    $item->files;
                }
            }
            return response()->json([
                'details' => $filterMes,
                'avt' => $avt,
                'time' => $time,
                'images' => $images,
                'incomFullName' => $incomFullName
            ]);
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function chatsLoadMore($id, $take_number)
    {
        try{
            $mes = MessageOneOneDetail::where('id_message', $id)->orderBy('updated_at', 'desc')->take($take_number + 10)->get();
            for($i = $take_number; $i < $mes->count() ; $i++){
                $filterMes[] = $mes[$i];
            }
            $time = [];
            $avt = '';
            $incomFullName = '';
            foreach($filterMes as $index=>$item){
                $generalSetting = SmGeneralSettings::find(1);
                $system_date_foramt = SmDateFormat::find($generalSetting->date_format_id);
                $DATE_FORMAT =  $system_date_foramt->format;
                $formatted = date_format(date_create($item->created_at), $DATE_FORMAT);
                $time[] = str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT) .' : '. str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT) . ' | ' . $formatted;
                if($item->posted_by != Auth::user()->id){
                    $item->images;
                    $item->files;
                    $user = User::find($item->posted_by);
                    $incomFullName = $user->full_name;
                    if ($user->role_id == 2) {
                        $LoginUser = SmStudent::where('user_id', $user->id)->first();
                        $profile = $LoginUser ? $LoginUser->student_photo : '';
                    } elseif ($user->role_id == 3) {
                        $LoginUser = SmParent::where('user_id', $user->id)->first();
                        $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                    } else {
                        $LoginUser = SmStaff::where('user_id', $user->id)->first();
                        $profile = $LoginUser ? $LoginUser->staff_photo : '';
                    }
                    $avt = file_exists($profile) ? $profile : 'public/uploads/staff/demo/staff.jpg';
                }
                else{
                    $item->images;
                    $item->files;
                }
            }
            return response()->json([
                'details' => $filterMes,
                'avt' => $avt,
                'time' => $time,
                'incomFullName' => $incomFullName
            ]);
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function chatsGroupEditPeople($id)
    {
        try{
            $participants = conversation_users::where('conversation_id', $id)->where('user_id', '!=', Auth::user()->id)->get();
            return view('chatbox::chats.gr_editPeople', compact('participants', 'id'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function chatsGroupAddPeople($id)
    {
        try{
            $participants = conversation_users::where('conversation_id', $id)->get();
            foreach($participants as $participant){
                $participantId[] = $participant->user_id;
            }
            $users = User::where('role_id', '!=', 2)->orderBy('full_name', 'asc')->get();
            $role = [];
            $userFiltered = [];
            foreach($users as $user){
                if(!in_array($user->id, $participantId)){
                    if(!in_array($user->role_id, $role)){
                        $role[] = $user->role_id;
                    }
                    $userFiltered[] = $user;
                }
            }
            return view('chatbox::chats.gr_addPeople', compact('userFiltered', 'role', 'id'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function chatsGroupUpdateName($id, $value)
    {
        try{
            $con = conversation::find($id);
            $con->name = $value;
            $con->save();
            return response()->json([$con]);
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function chatsGroupUpdatePeople(Request $request)
    {
        foreach ($request->user_ids as $value) {
            $add = new conversation_users();
            $add->user_id = $value;
            $add->conversation_id = $request->group_id;
            $add->save();
            $names [] = User::find($value);
        }
        $ids = $request->user_ids;
        $group_id = $request->group_id;
        $name = conversation::find($request->group_id)->name;
        $subject = conversation::find($request->group_id)->subject;
        broadcast(new AddToGroup($ids,$group_id,$name,$subject))->toOthers();
        return redirect()->back();
    }
    
    public function newCons(Request $request)
    {
        $find = MessageOneOne::where('id_from',Auth::user()->id)->where('id_to',$request->user_id)->first();
        if(isset($find))
        {
            $message_detail = new MessageOneOneDetail();
            $message_detail->posted_by = Auth::user()->id;
            $message_detail->id_message = $find->id;
            $message_detail->detail = $request->detail;
            $message_detail->save();

            $changestatus = MessageOneOne::find($find->id);
            $changestatus->lastest_sender = Auth::user()->id;
            $changestatus->status = 1;
            $changestatus->save();
        }
        else
        {
            $find2 = MessageOneOne::where('id_from',$request->user_id)->where('id_to',Auth::user()->id)->first();
            if(isset($find2))
            {
                $message_detail = new MessageOneOneDetail();
                $message_detail->posted_by = Auth::user()->id;
                $message_detail->id_message = $find2->id;
                $message_detail->detail = $request->detail;
                $message_detail->save();

                $changestatus = MessageOneOne::find($find2->id);
                $changestatus->lastest_sender = Auth::user()->id;
                $changestatus->status = 1;
                $changestatus->save();
            }
            else
            {
                $message = new MessageOneOne();
                $message->id_from = Auth::user()->id;
                $message->id_to = $request->user_id;
                $message->status = 1;
                $message->lastest_sender = Auth::user()->id;
                $message->save(); 
                if($request->detail){
                    $message_detail = new MessageOneOneDetail();
                    $message_detail->posted_by = Auth::user()->id;
                    $message_detail->id_message = $message->id;
                    $message_detail->detail = $request->detail;
                    $message_detail->save();
                }
            }
        }   
        Toastr::success('Operation Success', 'Success');
        return redirect()->back();
    }
    public static function subName($fullName){
        if(strlen($fullName) > 15){
            return substr($fullName, 0, 15)."...";
        }
        return $fullName;
    }
}
