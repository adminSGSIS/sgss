@extends('chatbox::chats.layout')
@section('css')
 <style>
    ::-webkit-scrollbar-track{
        display: none;
    }
    .online{
        width: 8px;
        height: 8px;
        position: absolute;
        background: #08d408;
        border-radius: 50%;
        bottom: 0;
        left: 75%;
        box-shadow: 0px 0px 0px 4px #fff;
    }
    .othersCons{
        display: none;
    }
    .avtDiv{
        width: 6%;
        height: 100%;
        position: relative;
        background: rgb(242 253 253 / 85%);
    }
    .avtDiv img{
        width: 50px;
        border-radius: 50%;
        height: 50px;
    }
    .avatarImg:hover .infor{
        display: block !important;
    }   
    .container{max-width:100%; margin-left: 0; padding-left: 0}
    img{ max-width:100%;}
    .inbox_people {
        background: white;
        float: left;
        overflow: hidden;
        width: 30%; border-right:1px solid #c4c4c447;
        max-width: 1000px;
    }
    .inbox_msg {
        border: 1px solid #dcdcdcad;
        clear: both;
        overflow: hidden;
    }
    .top_spac{ margin: 20px 0 0;}


    .recent_heading {float: left; width:40%;}
    .srch_bar {
        width: 95%;
        margin: auto;
        margin-bottom: 10px;
    }
    .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; }

    .recent_heading h4 {
        color: #05728f;
        font-size: 21px;
        margin: auto;
    }
    .chat_ib h5{ font-size:13px; color:#464646; margin:0 0 8px 0; text-transform: uppercase;}
    .chat_ib h5 span{ font-size:13px; float:right; }
    .chat_ib p{ font-size:12px; color:#989898; margin:auto; position: relative;}
    .chat_img {
        float: left;
        width: 11%;
        position: relative;
    }
    .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
    }

    .chat_people{ overflow:hidden; clear:both;}
    .chat_list, .gr_chat_list {
        border-radius: 17px;
        margin: 0;
        padding: 18px 16px 10px;
    }
    .inbox_chat { overflow-y: scroll; padding: 10px;}

    .active_chat{ background:#ebebeb; }

    .incoming_msg{
        position: relative;
    }
    .incoming_msg_img {
        display: inline-block;
        width: 6%;
        box-shadow: 0px 0px 0px 0.5rem #fff;
        position: absolute;
        bottom: 10%;
        left: 0%;
        border-radius: 50%;
    }
    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
        margin-left: 3%;
        max-width: 1000px;
    }
    .received_withd_msg p {
        word-break: break-all;
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 20px;
        width: 100%;
        border-radius: 20px;
        padding-left: 24px;
        padding-right: 24px;
    }
    .received_withd_msg span{
        margin-left: 20px;
    }
    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }
    .received_withd_msg { width: 57%;}
    .mesgs {
        float: left;
        width: 70%;
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0; color:#fff;
        padding: 5px 10px 5px 12px;
        width:100%;
        border-radius: 20px;
    }
    .outgoing_msg{ overflow:hidden; margin:26px 0 26px; padding-right: 15px; word-break: break-all;}
    .incoming_msg{margin:26px 0 26px;}
    .sent_msg {
        float: right;
        width: 46%;
    }
    .input_msg_write{
        max-width: 1000px;
    }
    .input_msg_write textarea{
        outline: none !important;
        border: 1px solid #c4c4c4 !important;
    }
    .input_msg_write textarea:focus{
        outline: none !important;
        border: 1px solid  #8f8f8f !important;
    }

    .type_msg {border-top: 1px solid #c4c4c447; position: relative;}
    .msg_send_btn {
        border: medium none;
        border-radius: 50%;
        cursor: pointer;
        font-size: 17px;
        height: 33px;
        position: absolute;
        right: 15%;
        top: 11px;
        width: 33px;
        background: none;
    }
    .msg_history {
        overflow-y: auto;
    }
    .list-unstyled{
        margin-left: 30px;
    }
    .list-unstyled li a{
        color: #828bb2;
    }
    .emoji-menu{
        top: -235px !important;
    }
    .emoji-items-wrap{
        overflow-x: hidden !important;
    }
    .emoji-wysiwyg-editor{
        width: 77% !important;
        height: 52px !important;
        border: 1px solid #fff;
        border-right: 0;
    }
    .emoji-wysiwyg-editor:focus{
        outline: none !important;
        border: 1px solid #fff;
        border-right: 0;
    }
    .emoji-picker-icon{
        font-size: 25px !important;
        top: 28% !important;
        right: 8% !important;
    }
    .likeBtn{
        width: auto;
        background: none;
        border: none;
        height: 24px;
        position: absolute;
        top: 30%;
        right: 1%;
        font-size: 23px;
    }
    .w3-button{
        display: none;
        color: #4e4e4e !important;
        background: none !important;
    } 
    .w3-button:hover{
        background: rgb(204 241 239 / 40%) !important;
    }
   .infor{
        background: rgb(255, 255, 255); 
        display: none; 
        z-index: 1000; 
        position: absolute; 
        left: 0;
        margin-top: 50px;
        border-radius: 20px;
    }
    .container{
        padding-right: 0; 
    }
    .chatIcon:hover, .activeTab{
        background: rgb(204 241 239 / 40%) !important;
    }
    .chat_header{
        border-bottom: 1px solid #c4c4c447;
        height: 59px;
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        background: white;
        margin-left: -25px;
        padding: 10px;
        z-index: 1000;
    }
    .chat_header img{
        border-radius: 50%;
        width: 40px;
    }
    .chat_header span{
        text-transform: uppercase;
        margin-left: 10px;   
    }
    .infoGroup{
        background: none;
        float: right;
        border: 1px solid #05728f4d !important;
    }
    .infoGroup:hover{
        background: aliceblue;
    }
    .infoGroup:focus{
        background: none !important;
        box-shadow: none !important;
    }
    .infoGroup:active{
        background: none !important;
    }
    .infoGroup img{
        width: 20px;
    }
    .w3-sidebar{
        z-index: 1001;
    }
    .w3-sidebar-right{
        width: 320px;
    }
    .gr_avt_sidebar{
        margin: auto;
        width: 30%;
        margin-top: 15px;
    }
    .closeBtnSideBar{
        border-bottom: 1px solid #c4c4c46b !important;
        height: 59px !important;
    }
    .w3-sidebar-right div h2, .w3-sidebar-right h3{
        text-align: center;
        text-transform: capitalize;
    }
    .closeBtnSideBar:hover{
        background: #c4c4c452 !important;
    }
    #group_participants_name_sidebar div{
        padding: 10px;
    }
    #group_participants_name_sidebar div:hover{
        background: #c4c4c445;
    }
    #group_participants_name_sidebar div img{
        border-radius: 50%;
        margin-right: 10px;
    }  
    #group_name_sidebar {
        margin: 10px 0px 0px 13px;
        width: 90%;
        text-align: center;
        border: none;
    }
    #group_name_sidebar:hover{
        border: 1px solid #e0dfdf;
    }
    #group_name_sidebar:focus{
        outline: none;
    }
    #saveGroupName, #cancelEditGroupName{
        height: 30px !important;
        background: none;
        border: none !important;
        display: none;
        color: #007bff;
        margin: auto;
    }
    #saveGroupName:focus, #cancelEditGroupName:focus, .more-people button:focus{
        box-shadow: none;
    }
    .more-people{
        display: none;
        padding: 15px;
    }
   
    .more-people img{
        width: 30px;
        margin-right: 12px;
    }
    .leaveGr p:hover{
        background: #cecece52;
    }
    .leaveGr p{
        text-align: center;
        color: indianred;
    }
    .multiImages{
        background: whitesmoke !important;
    }
    .multiImages a img{
        object-fit:cover;
        margin-bottom: 3px;
        width: 100%;
    }
    .text-muted {
        color: #05728f!important;
    }
    #lightbox{
        top: 26% !important;
    }
    #moreAction, #moreAction2 {
      -webkit-transform: none !important;
      transform: none !important;
      left: auto !important;
      right: 0 !important;
      top: -200% !important;
    }
    .modal-content .modal-header {
        background: linear-gradient(to right, #085078, #57c3b6) !important;
    }
    .optionBtn{
        background: linear-gradient(to right, #47d0b6, #7587da) !important;
    }
    .oneImg img{
        border-radius: 15px;
        border: 1px solid #a7a7a7c7;
    }
	.task{
        border-left: 5px solid blue;
        padding-left: 10px;
	}
	.event_title{
	    font-size : 20px;
	}
	.diffTime{
        background: aliceblue;
        width: fit-content;
        margin: auto;
        margin-top: -32px;
        border-radius: 10px;
        padding: 0px 16px;
	}
    .loadImage{
        display: none;
	    padding-left: 6px;
	}
	.loadImage img{
	    margin-bottom: 6px;
	    margin-right: 5px;
	    width: 97px;
	    height: 100px;
	    object-fit:cover;
	}
	.imageShared, .fileShared{
	    padding: 10px; 
	    display: block;
	    margin-bottom: 10px;
	}
	.imageShared:hover, .fileShared:hover{
	    background: #eee;
	    color: #828bb2;
	}
	.imageShared img, .fileShared img{
	    float: right;
	    margin-top: 7px;
	}
	div#wave .dot {
        display: inline-block;
        width: 6px;
        height: 6px;
        border-radius: 50%;
        margin-right: 3px;
        background: #b5b6b7;
        animation: wave 1.3s linear infinite;
    }
    div#wave .dot:nth-child(2) {
        animation-delay: -1.1s;
    }
    div#wave .dot:nth-child(3) {
        animation-delay: -0.9s;
    }
    
    @keyframes wave {
        0%, 60%, 100% {
            transform: initial;
        }
        30% {
            transform: translateY(-15px);
        }
    }
    @media(max-width: 768px){
        .chat_ib h5 span{
            display: none;
        }
        .chat_ib h5{
            font-size: 13px;
        }
        .chat_ib p{
            font-size: 12px;
        }
        #loadingMessage{
            right: 2% !important;
        }
        .avtDiv{
            width: 8%;
        }
        .emoji-wysiwyg-editor{
            width: 70% !important;
            height: 52px !important;
        }
        .msg_send_btn {
            right: 20%;
        }
        .emoji-picker-icon{
            right: 11% !important;
        }
        .received_withd_msg {
            width: 80%;
        }
        .sent_msg {
            width: 80%;
        }
        .incoming_msg_img {
            width: 9%;
        }
		.task{
        border-left: 5px solid blue;
        padding-left: 10px;
    }

    @media(max-width: 600px){
        #loadingMessage{
            right: 3% !important;
        }
        .avtDiv{
            width: 13%;
        }
        .inbox_people{
            width: 0;
        }
        .mesgs{
            width: 99%;
        }
        .avatarImg img{
            width: 40px;
            height: 40px;
        }
        .received_withd_msg {
            width: 85%;
        }
        .sent_msg {
            width: 85%;
        }
        .w3-button{
            display: inline;
        }
        .chatframes{
            display: none;
        }
        .incoming_msg_img {
            width: 8%;
        }
		.task{
        border-left: 5px solid blue;
        padding-left: 10px;
    }
    }
    @media(max-width: 500px){
        .incoming_msg_img {
            width: 9%;
        }
    }
    @media(max-width: 450px){
        .incoming_msg_img {
            width: 10%;
        }
        .likeBtn{
            right: 2%;
        }
        .msg_send_btn{
            width: 60px;
        }
        .emoji-picker-icon{
            display: none;
        }
    }
    @media(max-width: 400px){
        .incoming_msg_img {
            width: 11%;
        }
    }
 </style>
@endsection
@section('mainContent')

<div class="w3-sidebar w3-sidebar-right w3-bar-block w3-card w3-animate-right" style="display:none;right:0;" id="rightMenuOneOne">
    <div style="overflow-y: scroll; height: 95%">
        <input type="hidden" id="group_name_sidebar_temp">
        <input type="hidden" id="groupId">
        <button onclick="closeRightMenuOneOne()" style="" class="w3-bar-item w3-button w3-large closeBtnSideBar">Profile Details &times;</button>
        <div class="gr_avt_sidebar">
          <img src="" id="avtOneOne" style="border-radius: 50%" alt="">
        </div>
        <p id="name_sidebar" style="text-align: center; margin-top: 10px;"></p>
        <p class="mt-4" style="border-bottom: 1px solid #d0d0d07a;"></p>
       
        <a class="imageShared" href="#">
            <span>IMAGES SHARED</span>
            <img id="img_f" src="{{asset('public/svg/back.svg')}}" width="10px">
            <img style="display: none" id="img_s" src="{{asset('public/svg/next.svg')}}" width="10px">
        </a>
        <div class="loadImage" id="loadImage"></div>
        
         <a class="fileShared" href="#">
            <span>FILES SHARED</span>
            <img id="file_f" src="{{asset('public/svg/back.svg')}}" width="10px">
            <img style="display: none" id="file_s" src="{{asset('public/svg/next.svg')}}" width="10px">
        </a>
        <div class="loadFile" id="loadFile" style="display: none"></div>
    </div>
</div>

<div class="w3-sidebar w3-sidebar-right w3-bar-block w3-card w3-animate-right" style="display:none;right:0;" id="rightMenu">
    <div style="overflow-y: scroll; height: 95%">
        <input type="hidden" id="group_name_sidebar_temp">
        <input type="hidden" id="groupId">
        <button onclick="closeRightMenu()" style="" class="w3-bar-item w3-button w3-large closeBtnSideBar">Profile Details &times;</button>
        <div class="gr_avt_sidebar">
          <img src="{{asset('public/sg_star_logo.png')}}" alt="">
        </div>
        <input type="text" id="group_name_sidebar" autocomplete="off" onkeyup="editGroupName(this.value)">
        <div style="display: flex">
            <button type="button" onclick="cancelEdit(this)" class="btn" style="color: black;" id="cancelEditGroupName">Cancel</button>
            <button type="button" class="btn" id="saveGroupName">Save</button>
        </div>
        
        <h3 id="group_participants_sidebar"></h3>
        <p class="mt-4" style="border-bottom: 2px solid #a29f9f7a;"></p>
        <div class="more-people">
            <a href="#" class="deleteUrl" data-modal-size="modal-md" title="More people">
                <img src="{{asset('public/svg/add.svg')}}" alt="">
                <span>More People</span>
            </a>
        </div>
        <div class="more-people">
            <a href="#" class="deleteUrl" data-modal-size="modal-md" title="Edit people">
                <img src="{{asset('public/svg/edit.svg')}}" alt="">
                <span>Edit People</span>
            </a>
        </div>
        <a href="#" class="leaveGr">
            <p>Leave Group</p>
        </a>
        
        <a id="ListNamesImages" class="imageShared" href="#">
            <span>PARTICIPANTS</span>
            <img id="gr_names_img_f" src="{{asset('public/svg/back.svg')}}" width="10px">
            <img style="display: none" id="gr_names_img_s" src="{{asset('public/svg/next.svg')}}" width="10px">
        </a>
        <div id="group_participants_name_sidebar" style="overflow-y: scroll; height: 30%; display: none"></div>
        
        <a id="ListImages" class="imageShared" href="#">
            <span>IMAGES SHARED</span>
            <img id="gr_img_f" src="{{asset('public/svg/back.svg')}}" width="10px">
            <img style="display: none" id="gr_img_s" src="{{asset('public/svg/next.svg')}}" width="10px">
        </a>
        <div id="ImgsList" class="loadImage" style="display: none; padding-left: 6px;"></div>
        
        <a id="ListFiles" class="fileShared" href="#">
            <span>FILES SHARED</span>
            <img id="gr_file_f" src="{{asset('public/svg/back.svg')}}" width="10px">
            <img style="display: none" id="gr_file_s" src="{{asset('public/svg/next.svg')}}" width="10px">
        </a>
        <div id="FilesList" style="display: none;"></div>
    </div>
</div>

<div class="w3-sidebar w3-bar-block w3-card w3-animate-left nav nav-tabs" style="display:none" id="leftMenu" role="tablist">
    <button onclick="closeLeftMenu()" class="w3-bar-item w3-button w3-large closeBtnSideBar">Close &times;</button>

    
    <div style="overflow-y: scroll; height: 90%">
    @foreach($cons as $con)
        @if($con->id_to != Auth::user()->id)
            @php
                $user = App\User::find($con->id_to);
                if ($user->role_id == 2) {
                    $LoginUser = App\SmStudent::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->student_photo : '';
                } elseif ($user->role_id == 3) {
                    $LoginUser = App\SmParent::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                } else {
                    $LoginUser = App\SmStaff::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->staff_photo : '';
                }
            @endphp
            <li class="nav-item">
                <a class="nav-link chatIcon chat_list_mobile recent @if($loop->index == 0) active show @endif" href="#cons{{$con->id}}" onclick="removeActive(this)" role="tab" data-toggle="tab" @if($loop->index == 0) style="border: none; background: rgb(204 241 239 / 40%)" @else style="border: none;" @endif  id="acmo{{$con->id}}">
                    <div style="display: flex">
                        <img style="width: 40px; height: 40px; border-radius: 50%" src="{{ file_exists($profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="">
                        <h5 style="font-size: 12px; margin-left: 6px;">{{$user->full_name}}</h5>
                    </div>
                </a>
            </li>
        @elseif($con->id_from != Auth::user()->id)
            @php
                $user = App\User::find($con->id_from);
                if ($user->role_id == 2) {
                    $LoginUser = App\SmStudent::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->student_photo : '';
                } elseif ($user->role_id == 3) {
                    $LoginUser = App\SmParent::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                } else {
                    $LoginUser = App\SmStaff::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->staff_photo : '';
                }
            @endphp
            <li class="nav-item">
                <a class="nav-link chat_list_mobile chatIcon recent @if($loop->index == 0) active show @endif" href="#cons{{$con->id}}" onclick="removeActive(this)" role="tab" data-toggle="tab" @if($loop->index == 0) style="border: none; background: rgb(204 241 239 / 40%)" @else style="border: none;" @endif id="acmo{{$con->id}}">
                    <div style="display: flex">
                        <img style="width: 40px; height: 40px; border-radius: 50%" src="{{ file_exists($profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="">&nbsp;
                        <h5 style="font-size: 12px; margin-left: 7px;">{{$user->full_name}}</h5>
                    </div>
                </a>
            </li>
        @endif
    @endforeach
    </div>
</div>

<div class="w3-sidebar w3-bar-block w3-card w3-animate-left nav nav-tabs" style="display:none" id="gr_leftMenu" role="tablist">
    <button onclick="gr_closeLeftMenu()" class="w3-bar-item w3-button w3-large closeBtnSideBar">Close &times;</button>
    <div style="overflow-y: scroll; height: 90%">
    @foreach($recentGroup as $con)
    @php
        $groupInfo = Modules\ChatBox\Entities\conversation::find($con->conversation_id);
    @endphp
    <li class="nav-item">
        <a class="nav-link chat_list_mobile chatIcon gr_recent @if($loop->index == 0) active show @endif" href="#gr_cons{{$con->conversation_id}}" onclick="gr_removeActive(this)" role="tab" data-toggle="tab" @if($loop->index == 0) style="border: none; background: rgb(204 241 239 / 40%)" @else style="border: none;" @endif id="gr_acmo{{$con->conversation_id}}">
            <div style="display: flex">
                <img style="width: 40px; height: 40px; border-radius: 50%" src="{{asset('public/sg_star_logo.png')}}" alt="">&nbsp;
                <h5 style="font-size: 12px; margin-left: 7px;">{{$groupInfo->name}}</h5>
            </div>
        </a>
    </li>
    @endforeach
    </div>
</div>

<div style="overflow: hidden;">
    <div style="display: flex; height: 100%">
        <div class="avtDiv">
            @php
                $user = Auth::user();
                if ($user->role_id == 2) {
                    $LoginUser = App\SmStudent::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->student_photo : '';
                } elseif ($user->role_id == 3) {
                    $LoginUser = App\SmParent::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                } else {
                    $LoginUser = App\SmStaff::where('user_id', $user->id)->first();
                    $profile = $LoginUser ? $LoginUser->staff_photo : '';
                }
            @endphp
            <div class="d-flex justify-content-center avatarImg mt-3">
                <img src="{{ file_exists(@$profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="">

                <div style="" class="shadow-lg infor">
                    <div class="white-box">
                        <a class="dropdown-item" href="view-staff/{{Auth::user()->staff->id}}">
                            <div class="">
                                <div class="d-flex">

                                    <img class="client_img"
                                         src="{{ file_exists(@$profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }} "
                                         alt="">
                                    <div class="d-flex ml-10">
                                        <div class="">
                                            <h5 class="name text-uppercase">{{Auth::user()->full_name}}</h5>
                                            <p class="message">{{Auth::user()->email}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <ul class="list-unstyled">
                            <li>
                                @if(Auth::user()->role_id == "2")
                                    <a href="{{ url('student-profile')}}">
                                        <span class="ti-user"></span>
                                        @lang('lang.view_profile')
                                    </a>
                                {{-- @elseif(Auth::user()->role_id == "10")
                                    <a href="#">
                                        <span class="ti-user"></span>
                                        @lang('lang.view_profile')
                                    </a> --}}

                                @elseif(Auth::user()->role_id != "3")
                                    <a href="{{route('viewStaff', Auth::user()->staff->id)}}">
                                        <span class="ti-user"></span>
                                        VIEW PROFILE
                                    </a>
                                @endif
                            </li>

                            @if(App\SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes" && Auth::user()->role_id==1)

                                <li>
                                    <a href="{{url('view-as-superadmin')}}">
                                        <span class="ti-key"></span>
                                        @if(Session::get('isSchoolAdmin')==TRUE)
                                            @lang('lang.view') as  @lang('lang.saas') @lang('lang.admin')
                                        @else
                                            @lang('lang.view') as  @lang('lang.school') @lang('lang.admin')
                                        @endif
                                    </a>
                                </li>
                            @endif

                            <li>
                                <a href="{{url('change-password')}}">
                                    <span class="ti-key"></span>
                                    PASSWORD
                                </a>
                            </li>
                            <li>

                                <a href="{{ Auth::user()->role_id == 2? route('student-logout'): route('logout')}}"
                                   onclick="event.preventDefault();

                                     document.getElementById('logout-form').submit();">
                                    <span class="ti-unlock"></span>
                                    LOGOUT
                                </a>

                                <form id="logout-form"
                                      action="{{ Auth::user()->role_id == 2? route('student-logout'): route('logout') }}"
                                      method="POST" style="display: none;">

                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            

            <ul class="nav nav-tabs mt-5 d-flex justify-content-center" role="tablist" style="border: none">
                <li class="nav-item">
                    <a class="nav-link" style="border: none;" href="#chatframes" role="tab" data-toggle="tab">
                        <button class="w3-button w3-teal w3-xlarge w3-left activeTab" onclick="openLeftMenu(this)"  style="width: 60px">
                            <img style="border-radius: 0;" src="{{asset('public/svg/messenger.svg')}}" alt="">
                        </button> 
                    </a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" style="border: none;" href="#chatframes2" role="tab" data-toggle="tab">
                        <button class="w3-button w3-teal w3-xlarge w3-left" onclick="gr_openLeftMenu(this)"  style="width: 60px">
                            <img style="border-radius: 0;" src="{{asset('public/svg/group.svg')}}" alt="">
                        </button> 
                    </a>
                </li>
                
  
				
                <li class="nav-item direct-chat">
                    <a class="nav-link chatIcon changeActive activeTab chatframes" onclick="changeActiveTab(this)" href="#chatframes" role="tab" data-toggle="tab" title="Individual " style="border: none;">
                        <img style="border-radius: 0; width: 41px" src="{{asset('public/svg/messenger.svg')}}" alt="">
                    </a>
                </li>
                
                <li class="nav-item group-chat">
                    <a class="nav-link chatIcon changeActive chatframes" href="#chatframes2" onclick="changeActiveTab(this)" role="tab" data-toggle="tab" title="Group" style="border: none;">
                        <img style="border-radius: 0; width: 41px" src="{{asset('public/svg/group.svg')}}" alt="">
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link chatIcon deleteUrl" data-modal-size="modal-md" title="Chats Option" href="/chat/option" style="border: none;">
                        <img style="border-radius: 0; width: 41px" src="{{asset('public/svg/menu.svg')}}" alt="">
                    </a>
                </li>  
                
                <li class="nav-item">
                    <a class="nav-link chatIcon deleteUrl" data-modal-size="modal-md" title="Contact" href="/chat/contact" style="border: none;">
                        <img style="border-radius: 0; width: 41px" src="{{asset('public/svg/user.svg')}}" alt="">
                    </a>
                </li>       
                
                <li class="nav-item">
                    <a class="nav-link chatIcon" target="_blank" href="/send-email-sms-view" style="border: none;">
                        <img style="border-radius: 0; width: 41px" src="{{asset('public/svg/mail.svg')}}" alt="">
                    </a>
                </li>
            </ul>
            
        </div>
        <div style="width: 100%" class="tab-content">
			
            <div style="width: 100%" class="container tab-pane fade active show" role="tabpanel" id="chatframes">
                
                <div class="messaging">
                    <div class="inbox_msg">
                        <div class="inbox_people">
                            <div class="headind_srch">
                                <div class="recent_heading">
                                    <h4>Recent</h4>
                                </div>
                            </div>
                            <div class="inbox_chat" role="tablist">
                                <div class="srch_bar">
                                    <ul class="nav navbar-nav mr-auto search-bar" style="z-index:0">
                                        <li class="">
                                            <div style="background: #ebebeb69; border-radius: 20px;"class="input-group" id="serching">
                                                <span style="padding: 10px">
                                                    <i class="ti-search" aria-hidden="true" id="search-icon"></i>
                                                </span>
                                                <input type="text" class="form-control primary-input input-left-icon" placeholder="Search"
                                                       id="search" onkeyup="showResult(this.value)" style="border-bottom: none; padding: 20px"/>
                                                
                                            </div>
                                            <div id="livesearch"></div>
                                        </li>
                                    </ul>
                                </div>
                                @foreach($cons as $con)
                                <div class="chat_list @if($loop->index == 0) active_chat @endif" id="ac{{$con->id}}">
                                    <div class="chat_people">
                                        @if($con->id_to != Auth::user()->id)
                                            @php
                                                $user = App\User::find($con->id_to);
                                                $last_mess = Modules\ChatBox\Entities\MessageOneOneDetail::where('posted_by', $con->lastest_sender)->where('id_message', $con->id)->orderBy('updated_at', 'desc')->first();
                                                if ($user->role_id == 2) {
                                                    $LoginUser = App\SmStudent::where('user_id', $user->id)->first();
                                                    $profile = $LoginUser ? $LoginUser->student_photo : '';
                                                } elseif ($user->role_id == 3) {
                                                    $LoginUser = App\SmParent::where('user_id', $user->id)->first();
                                                    $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                                                } else {
                                                    $LoginUser = App\SmStaff::where('user_id', $user->id)->first();
                                                    $profile = $LoginUser ? $LoginUser->staff_photo : '';
                                                }
                                            @endphp
                                            <a href="#cons{{$con->id}}" class="@if($loop->index == 0) active show @endif recent" onclick="removeActive(this)" role="tab" data-toggle="tab" >
                                                <div class="chat_img"> 
                                                    <span id="userOnline{{$con->id}}" @if(!Cache::has('user-is-online-' . $user->id)) style="display: none" @endif class="online"></span>
                                                    <img style="border-radius: 50%;" src="{{ file_exists($profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="avatar"> 
                                                </div>
                                                <div class="chat_ib">
                                                    <h5>{{Modules\ChatBox\Http\Controllers\MessageOneOneController::subName($user->full_name)}} <span class="chat_date">{{App\SmGeneralSettings::DateConvater($con->updated_day)}}</span></h5>
                                                    <p @if($con->status == 1 && $last_mess->posted_by != Auth::user()->id) style="color: blue" @endif id="notice{{$con->id}}">
                                                        @if(isset($last_mess))
                                                            @php
                                                                $name = App\User::find($last_mess->posted_by)->full_name;
                                                            @endphp
                                                            {{$last_mess->posted_by == Auth::user()->id ? 'Me' : $name}} : 
                                                            @if(count($last_mess->images))
                                                                Image
                                                            @elseif($last_mess->files)
                                                                File
                                                            @else
                                                                {{$last_mess->detail}}
                                                            @endif
                                                        @endif
                                                        @if($con->status == 0 && isset($last_mess))
                                                            @if($last_mess->posted_by == Auth::user()->id)
                                                                <img id="avtNextFullName{{$con->id}}" style="border-radius: 50%; position: absolute; right: 0" src="{{ file_exists($profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" width="20px" >
                                                            @endif
                                                        @elseif($con->status == 1 && isset($last_mess))
                                                            @if($last_mess->posted_by == Auth::user()->id)
                                                                <img id="avtNextFullName{{$con->id}}" style="border-radius: 50%; position: absolute; right: 0" src="{{asset('public/svg/yes.svg')}}" width="20px" >
                                                            @endif
                                                        @endif
                                                    </p>
                                                </div>  
                                            </a>
                                        @elseif($con->id_from != Auth::user()->id)
                                            @php
                                                $user = App\User::find($con->id_from);
                                                $last_mess = Modules\ChatBox\Entities\MessageOneOneDetail::where('posted_by', $con->lastest_sender)->where('id_message', $con->id)->orderBy('updated_at', 'desc')->first();
                                                if ($user->role_id == 2) {
                                                    $LoginUser = App\SmStudent::where('user_id', $user->id)->first();
                                                    $profile = $LoginUser ? $LoginUser->student_photo : '';
                                                } elseif ($user->role_id == 3) {
                                                    $LoginUser = App\SmParent::where('user_id', $user->id)->first();
                                                    $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                                                } else {
                                                    $LoginUser = App\SmStaff::where('user_id', $user->id)->first();
                                                    $profile = $LoginUser ? $LoginUser->staff_photo : '';
                                                }
                                            @endphp
                                            <a href="#cons{{$con->id}}"  class="@if($loop->index == 0) active show @endif recent" onclick="removeActive(this)" role="tab" data-toggle="tab" >
                                                <div class="chat_img"> 
                                                    <span id="userOnline{{$con->id}}" @if(!Cache::has('user-is-online-' . $user->id)) style="display: none" @endif class="online"></span>
                                                    <img style="border-radius: 50%;" src="{{ file_exists($profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="avatar"> 
                                                </div>
                                                <div class="chat_ib">
                                                    <h5>{{Modules\ChatBox\Http\Controllers\MessageOneOneController::subName($user->full_name)}} <span class="chat_date">{{App\SmGeneralSettings::DateConvater($con->updated_day)}}</span></h5>
                                                    <p @if($con->status == 1 && $last_mess->posted_by != Auth::user()->id) style="color: blue" @endif id="notice{{$con->id}}">
                                                        @if(isset($last_mess))
                                                            @php
                                                                $name = App\User::find($last_mess->posted_by)->full_name;
                                                            @endphp
                                                            {{$last_mess->posted_by == Auth::user()->id ? 'Me' : $name}} : 
                                                            @if(count($last_mess->images))
                                                                Image
                                                            @elseif($last_mess->files)
                                                                File
                                                            @else
                                                                {{$last_mess->detail}}
                                                            @endif
                                                        @endif
                                                        @if($con->status == 0 && isset($last_mess))
                                                            @if($last_mess->posted_by == Auth::user()->id)
                                                                <img id="avtNextFullName{{$con->id}}" style="border-radius: 50%; position: absolute; right: 0" src="{{ file_exists($profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" width="20px" >
                                                            @endif
                                                        @elseif($con->status == 1 && isset($last_mess))
                                                            @if($last_mess->posted_by == Auth::user()->id)
                                                                <img id="avtNextFullName{{$con->id}}" style="border-radius: 50%; position: absolute; right: 0" src="{{asset('public/svg/yes.svg')}}" width="20px" >
                                                            @endif
                                                        @endif
                                                    </p>
                                                </div>
                                            </a>
                                        @endif
                                    </div>
                                    <script>
                                        document.getElementsByClassName('inbox_chat')[0].style.height = window.innerHeight - 51;
                                    </script>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div onclick="clickedMesgsDiv()" class="mesgs">
                            
                            <div class="msg_history" id="msg_history">
                                
                                @foreach($cons as $con)
                                
                                @php
                                    $focusUser = App\User::find($con->id_to == Auth::user()->id ? $con->id_from : $con->id_to);
                                    if ($focusUser->role_id == 2) {
                                        $LoginUser = App\SmStudent::where('user_id', $focusUser->id)->first();
                                        $avatar = $LoginUser ? $LoginUser->student_photo : '';
                                    } elseif ($focusUser->role_id == 3) {
                                        $LoginUser = App\SmParent::where('user_id', $focusUser->id)->first();
                                        $avatar = $LoginUser ? $LoginUser->fathers_photo : '';
                                    } else {
                                        $LoginUser = App\SmStaff::where('user_id', $focusUser->id)->first();
                                        $avatar = $LoginUser ? $LoginUser->staff_photo : '';
                                    }
                                @endphp
                                
                                <div style="padding-left: 25px" class="tab-pane fade @if($loop->index == 0) active show @else othersCons @endif noneCons"  role="tabpanel" id="cons{{$con->id}}">
                                    
                                    <div class="chat_header">
                                        <img src="{{ file_exists($avatar) ? asset($avatar) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="avatar">
                                        <span>{{$focusUser->full_name}}</span>
                                        <button class="infoGroup btn btn-secondary infoOneOne" type="button">
                                            <img src="{{asset('public/svg/information.svg')}}" alt="">
                                            <p class="infoOneOne_f" style="display: none">{{ file_exists($avatar) ? asset($avatar) : asset('public/uploads/staff/demo/staff.jpg') }}</p>
                                            <p class="infoOneOne_s" style="display: none">{{$focusUser->full_name}}</p>
                                            <p class="infoOneOne_t" style="display: none">{{$con->id}}</p>
                                        </button>
                                    </div>
                                    @php
                                        $record = Modules\ChatBox\Entities\MessageOneOneDetail::where('id_message', $con->id)->get();
                                    @endphp
                                    <input type="hidden" value="{{$record->count()}}" id="max_record{{$con->id}}">
                                    <div style="width: 0%; margin: auto; display: none;" class="spinload">
                                        <div class="spinner-border spinner-border-sm text-info" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                    
                                    <div id="messDiv{{$con->id}}">
                                        @foreach(Modules\ChatBox\Entities\MessageOneOneDetail::where('id_message', $con->id)->orderby('created_at','asc')->skip($record->count() - 10)->take(10)->get() as $item)
                                            
                                            @if($item->posted_by != Auth::user()->id)
                                                @php
                                                    $user = App\User::find($item->posted_by);
                                                    if ($user->role_id == 2) {
                                                        $LoginUser = App\SmStudent::where('user_id', $user->id)->first();
                                                        $profile = $LoginUser ? $LoginUser->student_photo : '';
                                                    } elseif ($user->role_id == 3) {
                                                        $LoginUser = App\SmParent::where('user_id', $user->id)->first();
                                                        $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                                                    } else {
                                                        $LoginUser = App\SmStaff::where('user_id', $user->id)->first();
                                                        $profile = $LoginUser ? $LoginUser->staff_photo : '';
                                                    }
                                                @endphp
                                                @if($item->difference_time == "y")
                                                    @php
                                                        $indays = Carbon::create($item->created_at->year, $item->created_at->month, $item->created_at->day, 
                                                            $item->created_at->hour, $item->created_at->minute, $item->created_at->second)->diffInDays(Carbon::now());
                                                        $forhuman = Carbon::create($item->created_at->year, $item->created_at->month, $item->created_at->day, 
                                                            $item->created_at->hour, $item->created_at->minute, $item->created_at->second)->diffForHumans(Carbon::now());
                                                    @endphp
                                                    @if($indays > 1)
                                                        <hr style="width: 96%">
                                                        <div class="diffTime">
                                                            <p style="text-align: center">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</p>
                                                        </div>  
                                                    @else
                                                        <hr style="width: 96%">
                                                        <div class="diffTime">
                                                            <p style="text-align: center">{{$forhuman}}</p>
                                                        </div>
                                                    @endif
                                                @endif
                                                <div class="incoming_msg">
                                                    <div class="incoming_msg_img"> <img data-toggle="tooltip" title="{{$user->full_name}}" style="border-radius: 50%;" src="{{ file_exists($profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="avatar"> </div>
                                                    <div class="received_msg">
                                                        <div class="received_withd_msg">
                                                            @if(count($item->images) > 0)
                                                            @if(count($item->images) > 1)
                                                                <p class="multiImages">
                                                                    @foreach($item->images as $image)
                                                                        <a target="_blank" href="{{asset($image->url)}}" data-lightbox="cons{{$con->id}}">
                                                                            <img width="{{93/count($item->images)}}%" height="100px" src="{{asset($image->url)}}" alt="">
                                                                        </a>
                                                                    @endforeach
                                                                </p>
                                                            @else
                                                                <a class="oneImg" target="_blank" href="{{asset($item->images[0]->url)}}" data-lightbox="cons{{$con->id}}">
                                                                    <img src="{{asset($item->images[0]->url)}}" alt="">
                                                                </a>
                                                            @endif
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                            @elseif($item->files)
                                                            <a href="{{$item->files->url}}" download><img width="60px" src="https://icon-library.com/images/file-icon/file-icon-6.jpg">{{str_replace("Modules/ChatBox/public/files/",'',$item->files->url)}}</a>
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                            @elseif($item->detail != NULL)
                                                                <p>{{$item->detail}}</p>
                                                                <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                @if($item->difference_time == "y")
                                                    @php
                                                        $indays = Carbon::create($item->created_at->year, $item->created_at->month, $item->created_at->day, 
                                                            $item->created_at->hour, $item->created_at->minute, $item->created_at->second)->diffInDays(Carbon::now());
                                                        $forhuman = Carbon::create($item->created_at->year, $item->created_at->month, $item->created_at->day, 
                                                            $item->created_at->hour, $item->created_at->minute, $item->created_at->second)->diffForHumans(Carbon::now());
                                                    @endphp
                                                    @if($indays > 1)
                                                        <hr style="width: 96%">
                                                        <div class="diffTime">
                                                            <p style="text-align: center">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</p>
                                                        </div>
                                                    @else
                                                        <hr style="width: 96%">
                                                        <div class="diffTime">
                                                            <p style="text-align: center">{{$forhuman}}</p>
                                                        </div>
                                                    @endif
                                                @endif
                                                <div class="outgoing_msg">
                                                    <div class="sent_msg">
                                                        @if(count($item->images) > 0)
                                                            @if(count($item->images) > 1)
                                                                <p class="multiImages">
                                                                    @foreach($item->images as $image)
                                                                    <a target="_blank" href="{{asset($image->url)}}" data-lightbox="cons{{$con->id}}">
                                                                        <img width="{{93/count($item->images)}}%" height="100px" src="{{asset($image->url)}}" alt="">
                                                                    </a>
                                                                    @endforeach
                                                                </p>
                                                            @else
                                                                <a class="oneImg" target="_blank" href="{{asset($item->images[0]->url)}}" data-lightbox="cons{{$con->id}}">
                                                                    <img src="{{asset($item->images[0]->url)}}" alt="">
                                                                </a>
                                                            @endif
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}    |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                        @elseif($item->files)
                                                            <a href="{{$item->files->url}}" download><img width="60px" src="https://icon-library.com/images/file-icon/file-icon-6.jpg">{{str_replace("Modules/ChatBox/public/files/",'',$item->files->url)}}</a>
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                        @elseif($item->detail != NULL)
                                                            <p>{{$item->detail}}</p>
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}    |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                        </div>
                                        <div id="loadingMessage{{$con->id}}" style="position: absolute; bottom: 65px; right: 1%; display: none">
                                            <div style="display: none" id="isLoading{{$con->id}}" class="spinner-border spinner-border-sm text-info" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                            <img id="loaded{{$con->id}}" style="display: none" src="{{asset('public/svg/yes.svg')}}" width="20px" >
                                            <img id="avtSeen{{$con->id}}" style="display: none; border-radius: 50%" src="{{ file_exists($avatar) ? asset($avatar) : asset('public/uploads/staff/demo/staff.jpg') }}" width="20px" >
                                        </div>  
                                    </div>
                                
                                @endforeach
                                
                                <script>
                                    var objDiv = document.getElementsByClassName("msg_history")[0];
                                    objDiv.style.height = window.innerHeight - 58;
                                    objDiv.scrollTop = objDiv.scrollHeight;
                                </script>
                            </div>
                            

                            <div class="type_msg">
                                <div class="input_msg_write">
                                    <textarea style="overflow-y: scroll" data-emoji-input="unicode" data-emojiable="true" cols="70" rows="2" placeholder="Type a message"></textarea>
                                    <button class="msg_send_btn" type="button">
                                        <img src="{{asset('public/svg/send.svg')}}" alt="">
                                    </button>
                                    <button type="button" onclick="open_hideDropdown1()" data-toggle="dropdown" class="likeBtn" style="width: auto">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    </button>
                                    <div class="dropdown-menu" id="moreAction">
                                        <label for="images_send" class="dropdown-item"><i class="fa fa-picture-o" aria-hidden="true"></i> Image</label>
                                        <label for="files_send" class="dropdown-item"><i class="fa fa-file" aria-hidden="true"></i>  File</label>
                                    </div>
                                </div>
                                <input type="file" multiple="" name="images_send[]" id="images_send" style="display: none;" accept="image/*" >
                                <input type="file" multiple="" name="files_send[]" id="files_send" style="display: none;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="width: 100%" class="container tab-pane fade" role="tabpanel" id="chatframes2">
                
                <div class="messaging">
                    <div class="inbox_msg">
                        <div class="inbox_people">
                            <div class="headind_srch">
                                <div class="recent_heading">
                                    <h4>Recent</h4>
                                </div>
                            </div>
                            <div class="inbox_chat" role="tablist">
                                <div class="srch_bar">
                                    <ul class="nav navbar-nav mr-auto search-bar" style="z-index:0">
                                        <li class="">
                                            <div style="background: #ebebeb69; border-radius: 20px;"class="input-group" id="serching">
                                                <span style="padding: 10px">
                                                    <i class="ti-search" aria-hidden="true" id="search-icon"></i>
                                                </span>
                                                <input type="text" class="form-control primary-input input-left-icon" placeholder="Search"
                                                       id="search" onkeyup="gr_showResult(this.value)" style="border-bottom: none; padding: 20px"/>
                                                
                                            </div>
                                            <div id="livesearch"></div>
                                        </li>
                                    </ul>
                                </div>
                                
                                
                                
                                @foreach($recentGroup as $con)
                                <div class="gr_chat_list @if($loop->index == 0) active_chat @endif" id="gr_ac{{$con->conversation_id}}">
                                    <div class="chat_people">
                                        @php
                                            $groupInfo = Modules\ChatBox\Entities\conversation::find($con->conversation_id);
                                            $last_mess = Modules\ChatBox\Entities\conversationDetails::where('conversation_id', $con->conversation_id)->orderBy('updated_at', 'desc')->first();
                                        @endphp
                                        <a href="#gr_cons{{$con->conversation_id}}" class=" gr_recent" onclick="gr_removeActive(this)" role="tab" data-toggle="tab" >
                                            <div class="chat_img"> <img style="border-radius: 50%;" src="{{asset('public/sg_star_logo.png')}}" alt="avatar"> </div>
                                            <div id="chat_ib{{$con->conversation_id}}" class="chat_ib">
                                                <h5>{{$groupInfo->name}} <span class="chat_date">{{App\SmGeneralSettings::DateConvater($groupInfo->updated_at)}}</span></h5>
                                                <p id="gr_notice{{$con->conversation_id}}">
                                                    @if(isset($last_mess))
                                                        @php
                                                            $name = App\User::find($last_mess->posted_by)->full_name;
                                                        @endphp
                                                        {{$last_mess->posted_by == Auth::user()->id ? 'Me' : $name}} : 
                                                        @if(count($last_mess->images))
                                                            Image
                                                        @elseif($last_mess->files)
                                                            File
                                                        @elseif($last_mess->is_system_notice != 1)
                                                            {{$last_mess->detail}}
                                                        @endif
                                                    @endif
                                                </p>
                                            </div>  
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                                <script>
                                    document.getElementsByClassName('inbox_chat')[1].style.height = window.innerHeight - 51;
                                </script>
                            </div>
                        </div>
                        
                        <div onclick="clickedMesgsDiv()" class="mesgs">
                        
                            <div class="msg_history" id="gr_msg_history">
                                @foreach($recentGroup as $con)
                                @php
                                    $groupInfo = Modules\ChatBox\Entities\conversation::find($con->conversation_id);
                                    $last_mess = Modules\ChatBox\Entities\conversationDetails::where('conversation_id', $con->conversation_id)->orderBy('updated_at', 'desc')->first();
                                @endphp
                                <div style="padding-left: 25px" class="tab-pane fade @if($loop->index == 0) active show @else othersCons @endif gr_noneCons"  role="tabpanel" id="gr_cons{{$groupInfo->id}}">
                                    <div class="chat_header">
                                        <img src="{{asset('public/sg_star_logo.png')}}" alt="avatar">
                                        <span>{{Modules\ChatBox\Entities\conversation::find($con->conversation_id)->name}}</span>
                                        <button class="infoGroup btn btn-secondary" onclick="openRightMenu(this)" id="infoGroup{{$con->conversation_id}}" type="button">
                                            <p style="display: none">{{Modules\ChatBox\Entities\conversation::find($con->conversation_id)->name}}</p>
                                            <p style="display: none">{{Modules\ChatBox\Entities\conversation_users::where('conversation_id', $con->conversation_id)->count()}} Participants</p>
                                            <img src="{{asset('public/svg/information.svg')}}" alt="">
                                            <div style="display: none"></div>
                                            <p style="display: none">{{Modules\ChatBox\Entities\conversation::find($con->conversation_id)->created_by}}</p>
                                            <p style="display: none">{{$con->conversation_id}}</p>
                                        </button>
                                    </div>
                                    
                                    @php
                                        $gr_record = Modules\ChatBox\Entities\conversationDetails::where('conversation_id', $con->conversation_id)->orderBy('updated_at', 'asc')->get();
                                    @endphp
                                    <input type="hidden" value="{{$gr_record->count()}}" id="gr_max_record{{$con->conversation_id}}">
                                    <div style="width: 0%; margin: auto; display: none;" class="spinload">
                                        <div class="spinner-border spinner-border-sm text-info" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                    <div id="gr_messDiv{{$con->conversation_id}}">
                                    
                                        @foreach(Modules\ChatBox\Entities\conversationDetails::where('conversation_id', $con->conversation_id)->orderBy('updated_at', 'asc')->skip($gr_record->count() - 10)->take(10)->get() as $item)
                                            
                                            @if($item->posted_by != Auth::user()->id && $item->is_system_notice ==0)
                                                @php
                                                    $user = App\User::find($item->posted_by);
                                                    if ($user->role_id == 2) {
                                                        $LoginUser = App\SmStudent::where('user_id', $user->id)->first();
                                                        $profile = $LoginUser ? $LoginUser->student_photo : '';
                                                    } elseif ($user->role_id == 3) {
                                                        $LoginUser = App\SmParent::where('user_id', $user->id)->first();
                                                        $profile = $LoginUser ? $LoginUser->fathers_photo : '';
                                                    } else {
                                                        $LoginUser = App\SmStaff::where('user_id', $user->id)->first();
                                                        $profile = $LoginUser ? $LoginUser->staff_photo : '';
                                                    }
                                                @endphp
                                                @if($item->difference_time == "y")
                                                    @php
                                                        $indays = Carbon::create($item->created_at->year, $item->created_at->month, $item->created_at->day, 
                                                            $item->created_at->hour, $item->created_at->minute, $item->created_at->second)->diffInDays(Carbon::now());
                                                        $forhuman = Carbon::create($item->created_at->year, $item->created_at->month, $item->created_at->day, 
                                                            $item->created_at->hour, $item->created_at->minute, $item->created_at->second)->diffForHumans(Carbon::now());
                                                    @endphp
                                                    @if($indays > 1)
                                                        <hr style="width: 96%">
                                                        <div class="diffTime">
                                                            <p style="text-align: center">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</p>
                                                        </div>
                                                    @else
                                                        <hr style="width: 96%">
                                                        <div class="diffTime">
                                                            <p style="text-align: center">{{$forhuman}}</p>
                                                        </div>
                                                    @endif
                                                @endif
                                                <div class="incoming_msg">
                                                    <div class="incoming_msg_img"> <img data-toggle="tooltip" title="{{$user->full_name}}" style="border-radius: 50%;" src="{{ file_exists($profile) ? asset($profile) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="avatar"> </div>
                                                    <div class="received_msg">
                                                        <div class="received_withd_msg">
                                                            @if(count($item->images) > 0)
                                                                @if(count($item->images) > 1)
                                                                    <p class="multiImages">
                                                                    @foreach($item->images as $image)
                                                                        <a target="_blank" href="{{asset($image->url)}}" data-lightbox="gr_cons{{$con->conversation_id}}">
                                                                            <img width="{{93/count($item->images)}}%" height="100px" src="{{asset($image->url)}}" alt="">
                                                                        </a>
                                                                    @endforeach
                                                                    </p>
                                                                @else
                                                                    <a class="oneImg" target="_blank" href="{{asset($item->images[0]->url)}}" data-lightbox="gr_cons{{$con->conversation_id}}">
                                                                        <img src="{{asset($item->images[0]->url)}}" alt="">
                                                                    </a>
                                                                @endif
                                                                <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                            @elseif($item->files)
                                                            <a href="{{$item->files->url}}" download><img width="60px" src="https://icon-library.com/images/file-icon/file-icon-6.jpg">{{str_replace("Modules/ChatBox/public/files/",'',$item->files->url)}}</a>
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                            @elseif($item->detail != NULL)
                                                                <p>{!!$item->detail!!}</p>
                                                                <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($item->posted_by == Auth::user()->id && $item->is_system_notice ==0)
                                                @if($item->difference_time == "y")
                                                    @php
                                                        $indays = Carbon::create($item->created_at->year, $item->created_at->month, $item->created_at->day, 
                                                            $item->created_at->hour, $item->created_at->minute, $item->created_at->second)->diffInDays(Carbon::now());
                                                        $forhuman = Carbon::create($item->created_at->year, $item->created_at->month, $item->created_at->day, 
                                                            $item->created_at->hour, $item->created_at->minute, $item->created_at->second)->diffForHumans(Carbon::now());
                                                    @endphp
                                                    @if($indays > 1)
                                                        <hr style="width: 96%">
                                                        <div class="diffTime">
                                                            <p style="text-align: center">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</p>
                                                        </div>
                                                    @else
                                                        <hr style="width: 96%">
                                                        <div class="diffTime">
                                                            <p style="text-align: center">{{$forhuman}}</p>
                                                        </div>
                                                    @endif
                                                @endif
                                                <div class="outgoing_msg">
                                                    <div class="sent_msg">
                                                        @if(count($item->images) > 0)
                                                            @if(count($item->images) > 1)
                                                                <p class="multiImages">
                                                                    @foreach($item->images as $image)
                                                                        <a target="_blank" href="{{asset($image->url)}}" data-lightbox="gr_cons{{$con->conversation_id}}">
                                                                            <img width="{{93/count($item->images)}}%" height="100px" src="{{asset($image->url)}}" alt="">
                                                                        </a>
                                                                    @endforeach
                                                                </p>
                                                            @else
                                                                <a class="oneImg" target="_blank" href="{{asset($item->images[0]->url)}}" data-lightbox="gr_cons{{$con->conversation_id}}">
                                                                    <img src="{{asset($item->images[0]->url)}}" alt="">
                                                                </a>
                                                            @endif
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                        @elseif($item->files)
                                                            <a href="{{$item->files->url}}" download><img width="60px" src="https://icon-library.com/images/file-icon/file-icon-6.jpg">{{str_replace("Modules/ChatBox/public/files/",'',$item->files->url)}}</a>
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                        @elseif($item->detail != null)
                                                            <p>{!!$item->detail!!}</p>
                                                            <span class="time_date">{{str_pad($item->created_at->hour, 2, "0", STR_PAD_LEFT)}} : {{str_pad($item->created_at->minute, 2, "0", STR_PAD_LEFT)}}     |    {{App\SmGeneralSettings::DateConvater($item->created_at)}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            @elseif($item->posted_by ==1 && $item->is_system_notice == 1)
                                                <div class="alert alert-danger received_msg">
                                                    <h2><i class="fa fa-calendar fa-2x" aria-hidden="true"></i> Saigon star Calendar</h2>
                                                  {!!$item->detail!!}
                                                </div>
                                            @endif
                                       
                                        @endforeach
                                    </div>
                                </div>
                                
                                @endforeach
                                <div id="gr_loadingMessage" style="position: absolute; bottom: 9%; right: 1%; display: none">
                                    <div style="display: none" id="gr_isLoading" class="spinner-border spinner-border-sm text-info" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                    <img id="gr_loaded" style="display: none" src="{{asset('public/svg/yes.svg')}}" width="20px" >
                                </div>  
                                <script>
                                    var objDiv = document.getElementsByClassName("msg_history")[1];
                                    objDiv.style.height = window.innerHeight - 58;
                                    objDiv.scrollTop = objDiv.scrollHeight; 
                                </script>

                            </div>
                            
                            
                            <div class="type_msg">
                                <div class="input_msg_write">
                                    <textarea  style="overflow-y: scroll" data-emoji-input="unicode" data-emojiable="true" cols="70" rows="2" placeholder="Type a message"></textarea>
                                    <button class="msg_send_btn" type="button">
                                        <img src="{{asset('public/svg/send.svg')}}" alt="">
                                    </button>
                                    <button type="button" data-toggle="dropdown" onclick="open_hideDropdown2()" class="likeBtn" style="width: auto">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    </button>
                                    <div class="dropdown-menu" id="moreAction2">
                                        <label for="gr_images_send" class="dropdown-item"><i class="fa fa-picture-o" aria-hidden="true"></i> Image</label>
                                        <label for="gr_files_send" class="dropdown-item"><i class="fa fa-file" aria-hidden="true"></i>  File</label>
                                    </div>
                                </div>
                                <input type="file" multiple="" name="gr_images_send[]" id="gr_images_send" style="display: none;" accept="image/jpg,image/png,image/jpeg,image/gif,image/tiff" >
                                <input type="file" multiple="" name="gr_files_send[]" id="gr_files_send" style="display: none;">
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                
        </div>
    </div>
</div>
@endsection

<script>
    function clickedMesgsDiv(){
        document.getElementById('moreAction').style.display = "none";
        document.getElementById('moreAction2').style.display = "none";
    }
    function open_hideDropdown1(){
        var dis = document.getElementById('moreAction').style.display;
        if(!dis || dis == "none"){
            document.getElementById('moreAction').style.display = "block";
        }else{
            document.getElementById('moreAction').style.display = "none";
        }
    }
    function open_hideDropdown2(){
        var dis = document.getElementById('moreAction2').style.display;
        if(!dis || dis == "none"){
            document.getElementById('moreAction2').style.display = "block";
        }else{
            document.getElementById('moreAction2').style.display = "none";
        }
        
    }
    function cancelEdit(x){
        document.getElementById('group_name_sidebar').value = document.getElementById('group_name_sidebar_temp').value;
        document.getElementById('saveGroupName').style.display = "none";
        document.getElementById('cancelEditGroupName').style.display = "none";
    }
    function editGroupName(x){
        if(x != document.getElementById('group_name_sidebar_temp').value){
            document.getElementById('saveGroupName').style.display = "block";
            document.getElementById('cancelEditGroupName').style.display = "block";
        }
        else{
            document.getElementById('saveGroupName').style.display = "none";
            document.getElementById('cancelEditGroupName').style.display = "none";
        }
    }
    function closeRightMenuOneOne(x) {
        document.getElementById("rightMenuOneOne").style.display = "none";
    }
    function openRightMenu(x) {
        document.getElementById("rightMenu").style.display = "block";
        document.getElementById('group_name_sidebar').value = x.children[0].innerText;
        document.getElementById('group_name_sidebar_temp').value = x.children[0].innerText;
        document.getElementById('groupId').value = x.children[5].innerText;
        document.getElementById('group_participants_sidebar').innerText = x.children[1].innerText;
        var length = document.getElementById("group_participants_name_sidebar").children.length;
        for (i = 0; i < length; i++) {
            document.getElementById("group_participants_name_sidebar").children[0].remove();
        }
        
        var g_id = document.getElementById('groupId').value = x.children[5].innerText;
        getNamesImages(g_id);
        getImages(g_id);
        getFiles(g_id);
        
        if($('#authUser_id').val() == x.children[4].innerText){
            document.getElementsByClassName('more-people')[0].style.display = "block";
            document.getElementsByClassName('more-people')[1].style.display = "block";
            document.getElementsByClassName('more-people')[0].children[0].href = "chat/group/add-people/" + document.getElementById('groupId').value;
            document.getElementsByClassName('more-people')[1].children[0].href = "chat/group/edit-people/" + document.getElementById('groupId').value;
            document.getElementsByClassName('leaveGr')[0].style.display = "none";
        }else{
            document.getElementsByClassName('more-people')[0].style.display = "none";
            document.getElementsByClassName('more-people')[1].style.display = "none";
            document.getElementsByClassName('leaveGr')[0].style.display = "block";
            document.getElementsByClassName('leaveGr')[0].href = "chatbox/leave/" + document.getElementById('groupId').value + "/" + $('#authUser_id').val();
        }
    }
    function closeRightMenu(x) {
        document.getElementById("rightMenu").style.display = "none";
        empty();
    }
    function openLeftMenu(x) {
        document.getElementById("leftMenu").style.display = "block";
        for (i = 0; i < document.getElementsByClassName('w3-button').length; i++) {
            document.getElementsByClassName('w3-button')[i].className = document.getElementsByClassName('w3-button')[i].className.replace(" activeTab", "");
        }
        x.className += " activeTab";
    }
    function closeLeftMenu() {
        document.getElementById("leftMenu").style.display = "none";
    }
    function gr_openLeftMenu(x) {
        document.getElementById("gr_leftMenu").style.display = "block";
        for (i = 0; i < document.getElementsByClassName('w3-button').length; i++) {
            document.getElementsByClassName('w3-button')[i].className = document.getElementsByClassName('w3-button')[i].className.replace(" activeTab", "");
        }
        x.className += " activeTab";
    }
    function gr_closeLeftMenu() {
        document.getElementById("gr_leftMenu").style.display = "none";
    }
    function changeActiveTab(x) {
        var objDiv = document.getElementsByClassName("msg_history")[1];
        objDiv.scrollTop = objDiv.scrollHeight; 
        for (i = 0; i < document.getElementsByClassName('changeActive').length; i++) {
            document.getElementsByClassName('changeActive')[i].className = document.getElementsByClassName('changeActive')[i].className.replace(" activeTab", "");
        }
        x.className += " activeTab";
    }
    function removeActive(x){
        for(var i = 0; i <  document.getElementsByClassName('recent').length; i++){
            document.getElementsByClassName('recent')[i].className = document.getElementsByClassName('recent')[i].className.replace(" active show", "");
        }
        for(var i = 0; i <  document.getElementsByClassName('noneCons').length; i++){
            document.getElementsByClassName('noneCons')[i].style.display = 'none';
        }
        document.getElementById(x.hash.slice(1)).style.display = "block";
        
        for(var i = 0; i <  document.getElementsByClassName('chat_list').length; i++){
            document.getElementsByClassName('chat_list')[i].className = document.getElementsByClassName('chat_list')[i].className.replace(" active_chat", "");
        }
        document.getElementById("ac" + x.hash.slice(5)).className += " active_chat";
        
        for(var i = 0; i <  document.getElementsByClassName('chat_list_mobile').length; i++){
            document.getElementsByClassName('chat_list_mobile')[i].style.background = "white";
        }
        document.getElementById("acmo" + x.hash.slice(5)).style.background = "rgb(204 241 239 / 40%)";
        var objDiv = document.getElementsByClassName("msg_history")[0];
        objDiv.scrollTop = objDiv.scrollHeight;
    }
    function gr_removeActive(x){
        document.getElementById("rightMenu").style.display = "none";
        for(var i = 0; i <  document.getElementsByClassName('gr_recent').length; i++){
            document.getElementsByClassName('gr_recent')[i].className = document.getElementsByClassName('gr_recent')[i].className.replace(" active show", "");
        }
        for(var i = 0; i <  document.getElementsByClassName('gr_noneCons').length; i++){
            document.getElementsByClassName('gr_noneCons')[i].style.display = 'none';
        }
        document.getElementById(x.hash.slice(1)).style.display = "block";
        
        for(var i = 0; i <  document.getElementsByClassName('gr_chat_list').length; i++){
            document.getElementsByClassName('gr_chat_list')[i].className = document.getElementsByClassName('gr_chat_list')[i].className.replace(" active_chat", "");
        }
        document.getElementById("gr_ac" + x.hash.slice(8)).className += " active_chat";
        
        for(var i = 0; i <  document.getElementsByClassName('chat_list_mobile').length; i++){
            document.getElementsByClassName('chat_list_mobile')[i].style.background = "white";
        }
        document.getElementById("gr_acmo" + x.hash.slice(8)).style.background = "rgb(204 241 239 / 40%)";
        var objDiv = document.getElementsByClassName("msg_history")[1];
        objDiv.scrollTop = objDiv.scrollHeight;
    }
    function showResult(x){
        if(x == false){
            for(var i = 0; i < document.getElementsByClassName('chat_list').length; i ++){
                document.getElementsByClassName('chat_list')[i].style.display = "block";
            }
        }
        else{
            for(var i = 0; i < document.getElementsByClassName('chat_list').length; i ++){
                if(document.getElementsByClassName('chat_list')[i].children[0].children[0].children[1].children[0].innerText.toLowerCase().indexOf(x.toLowerCase()) == -1){
                    document.getElementsByClassName('chat_list')[i].style.display = "none";
                }
            }
        }
    }
    function gr_showResult(x){
        if(x == false){
            for(var i = 0; i < document.getElementsByClassName('gr_chat_list').length; i ++){
                document.getElementsByClassName('gr_chat_list')[i].style.display = "block";
            }
        }
        else{
            for(var i = 0; i < document.getElementsByClassName('gr_chat_list').length; i ++){
                if(document.getElementsByClassName('gr_chat_list')[i].children[0].children[0].children[1].children[0].innerText.toLowerCase().indexOf(x.toLowerCase()) == -1){
                    document.getElementsByClassName('gr_chat_list')[i].style.display = "none";
                }
            }
        }
    }
</script>

