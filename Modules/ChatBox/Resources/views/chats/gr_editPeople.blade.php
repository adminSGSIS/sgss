<style>
    .modal-header{
        background: linear-gradient(to right, #085078, #57c3b6) !important;
    }
    .optionBtn{
        background: linear-gradient(to right, #47d0b6, #7587da) !important;
    }
</style>
{{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'chatbox/update', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
    <input name="id_conv" value="{{$id}}" type="hidden">
    @foreach ($participants as $participant)
        @php
            $user = App\User::find($participant->user_id);
        @endphp
        <div class="mt-2 {{ $errors->has('staffs') ? ' is-invalid' : '' }}" style="color: #828bb2; display: flex">
            <input style="width: 15px; height: 15px; margin-top: 5px" type="checkbox" class="mr-2 common-checkbox" checked name="user_ids[]" value="{{$user->id}}" id="{{$user->email}}">
            <label for="{{$user->email}}">{{$user->full_name}}</label>
        </div>
    @endforeach
    <button class="primary-btn fix-gr-bg mt-3 optionBtn" style="margin-left: 150px" type="submit">Apply</button>  
{{ Form::close() }}