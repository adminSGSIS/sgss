<?php
 
    use App\SmNotification;
    use App\SmParent;   
    use App\User;

    if (Auth::user() == "") {
        header('location:' . url('/login'));
        exit();
    }

    $active_style = DB::table('sm_styles')->where('is_active', 1)->first();
    $styles = DB::table('sm_styles')->get();
    $generalSetting = $config = DB::table('sm_general_settings')->first();
    $schoolSetting = $school_config = DB::table('sm_general_settings')->where('school_id',Auth::user()->school_id)->first();
    $system_date_foramt = App\SmDateFormat::find($config->date_format_id); 
    $dashboard_background = DB::table('sm_background_settings')->where([['is_default', 1], ['title', 'Dashboard Background']])->first();

     
    if (!empty(Auth::user())) {

        $ROLE_ID = Auth::user()->role_id; 

        if ($ROLE_ID != 1  && $ROLE_ID != 10) {
            $notifications = SmNotification::notifications();
            
        } else {
            $notifications = [];
        }
        if ($ROLE_ID == 1) {
            $notifications = SmNotification::notifications();

        }
        
        if (Auth::user()->role_id == 3) {

            $childrens = SmParent::myChildrens();
        }
    }



        if ($ROLE_ID == 2) {

            $LoginUser = App\SmStudent::where('user_id', Auth::user()->id)->first();
            if (empty($LoginUser)) {
                $profile = 'public/backEnd/img/admin/message-thumb.png';
            } else {
                $profile = $LoginUser->student_photo;
            }
        } elseif ($ROLE_ID == 3) {
            $LoginUser = App\SmParent::where('user_id', Auth::user()->id)->first();
            if (empty($LoginUser)) {
                $profile = 'public/backEnd/img/admin/message-thumb.png';
            } else {
                $profile = $LoginUser->fathers_photo;
            }
        } else {
            $LoginUser = App\SmStaff::where('user_id', Auth::user()->id)->first();

            if (empty($LoginUser)) {
                $profile = 'public/backEnd/img/admin/message-thumb.png';
            } else {
                $profile = $LoginUser->staff_photo;
            }
        }
    if (empty($dashboard_background)) {
        $css = "background: url('/public/backEnd/img/body-bg.jpg')  no-repeat center; background-size: cover; ";
    } else {
        if (!empty($dashboard_background->image)) {
            $css = "background: url('" . url($dashboard_background->image) . "')  no-repeat center; background-size: cover; ";
        } else {
            $css = "background:" . $dashboard_background->color;
        }
    } 

 
    if (!empty($school_config->logo)) {
        $logo = $school_config->logo;
    } else {
        $logo = 'public/uploads/settings/logo.png';
    }

    if (!empty($school_config->favicon)) {
        $fav = $school_config->favicon;
    } else {
        $fav = 'public/backEnd/img/favicon.png';
    }
    if (!empty($school_config->site_title)) {
        $site_title = $school_config->site_title;
    } else {
        $site_title = 'School Management System';
    }
    if (!empty($school_config->school_name)) {
        $school_name = $school_config->school_name;
    } else {
        $school_name = 'Infix Edu ERP';
    }

    //DATE FORMAT
    $DATE_FORMAT =  $system_date_foramt->format;

    $ttl_rtl = isset($config->ttl_rtl) ? $config->ttl_rtl : 2;

    $selected_language=App\SmLanguage::where('active_status',1)->first();
    if ($selected_language) {
        $language_universal= $selected_language->language_universal;
    } else {
        $language_universal='en';
    }
    
?><!DOCTYPE html>
{{-- <html lang="{{$language_universal }}" @if(isset ($ttl_rtl ) && $ttl_rtl ==1) dir="rtl" class="rtl" @endif > --}}
<html lang="{{ app()->getLocale() }}" @if(isset ($ttl_rtl ) && $ttl_rtl ==1) dir="rtl" class="rtl" @endif >
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel="icon" href="{{url('/')}}/{{isset($fav)?$fav:''}}" type="image/png"/>
    <title>{{$school_name}} | {{$site_title}}</title>
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <input type="hidden" name="auth_id" id="auth_id" value="{{Auth::user()->id}}">
    <!-- Bootstrap CSS -->

    @if(isset ($ttl_rtl ) && $ttl_rtl ==1)
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/rtl/bootstrap.min.css"/> 
    @else
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/bootstrap.css"/>
    @endif

    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/jquery-ui.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/jquery.data-tables.css">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/themify-icons.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/flaticon.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/nice-select.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/magnific-popup.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/fastselect.min.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/toastr.min.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/js/select2/select2.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/fullcalendar.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/css')}}/lightbox.css">

    <link rel='stylesheet' href='https://onesignal.github.io/emoji-picker/lib/css/emoji.css'>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    {{-- <link rel="stylesheet" href="{{asset('public/landing/css/toastr.css')}}"> --}}
    @yield('css')
    <link rel="stylesheet" href="{{asset('public/backEnd/css/loade.css')}}"/> 
    @if(isset ($ttl_rtl ) && $ttl_rtl ==1)
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/rtl/style.css"/>
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/rtl/infix.css"/>
    @else
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/{{@$active_style->path_main_style}}"/>
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/{{@$active_style->path_infix_style}}"/>
    @endif
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover { background: {{$active_style->primary_color2}} !important; }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover { background: {{$active_style->primary_color2}}  !important; }
        ::placeholder { color: {{$active_style->primary_color}}  !important; } 
        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{ z-index: 99999999999 !important; background: #fff !important;        }
        .input-effect { float: left;  width: 100%; }
    </style>

    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : (event.keyCode);
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }
            return true;
        }
    </script>
   <script src="{{asset('public/backEnd/js/')}}/lightbox-plus-jquery.js"></script>
</head>
<body class="admin"
      style="background: white;">

        @php 
            // $tt = file_get_contents(url('/').'/'.$generalSetting->logo);
             $tt='';
        @endphp
        <input type="text" hidden value="{{ base64_encode($tt) }}" id="logo_img">
        <input type="text" hidden value="{{ $generalSetting->school_name }}" id="logo_title">
        <input type="hidden" value="{{ Auth::user()->id }}" id="authUser_id">
<div class="main-wrapper" style="min-height: 600px; background: white">
    <!-- Page Content  -->
    <div style="margin-left: 0; padding: 0; width: 100%;" id="main-content">
        

        @yield('mainContent')






    @php
    $setting = App\SmGeneralSettings::where('school_id',Auth::user()->school_id)->first();
    if(isset($setting->copyright_text)){ $copyright_text = $setting->copyright_text; }else{ $copyright_text = 'Copyright 2019 All rights reserved by Codethemes'; }

@endphp
</div>
</div>

<div class="has-modal modal fade" id="showDetaildModal">
    <div class="modal-dialog modal-dialog-centered" id="modalSize">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="showDetaildModalTile">@lang('lang.new_client_information')</h4>
                <button type="button" class="close icons" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" id="showDetaildModalBody">

            </div>

            <!-- Modal footer -->

        </div>
    </div>
</div>


<!--  Start Modal Area -->
<div class="modal fade invoice-details" id="showDetaildModalInvoice">
    <div class="modal-dialog large-modal modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('lang.add') @lang('lang.invoice')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body" id="showDetaildModalBodyInvoice">
            </div>

        </div>
    </div>
</div>

<!--
<div class="skype-button bubble" data-bot-id="spondonit"></div>


================Footer Area ================= -->


<?php

use App\SmStaff;

$staff = SmStaff::where('email', auth()->user()->email)->get()->first();

$update_info = false;
if($staff){
    $staff_role_number = (int) substr($staff->staff_no, 2, 2);

    if ($staff_role_number > 1) {
        $current_year  = (int)date("Y");
        $current_month = (int)date("m");

        if ($current_year > $staff->updated_profile && $current_month > 8) {
            $update_info = true;
        }
    }
}




?>

<div class="modal fade" id="update_info">
    <div class="modal-dialog large-modal modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('DECLARATION')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="col-10">
                        <div class="row">
                            <p>
                                <strong>1</strong>. I confirm that I have reviewed the details held about me, either on the <span style="color:red">Registration screens or printed
                                    on a registration form</span>, and agree that they are correct at this time and/or that I have indicated as
                                appropriate where changes need to be made.
                            </p>
                        </div>
                        <div class="row">
                            <p>
                                <strong>2</strong>. With regard to the personal data displayed on the <span style="color:red">Registration screens or printed on a registration
                                    form</span>, or obtained about me otherwise, I agree to Saigon Star International School processing such data,
                                in accordance with the Data Protection Statement, which I have read and understood.
                            </p>
                        </div>
                        <div class="row">
                            <p>
                                <strong>3</strong>. I agree to abide by all the <span style="color:red">Statutes, Ordinances, Regulations and Rules</span> of Saigon Star International
                                School for the time being in force, or other requirements which are notified to me from time to time
                                through course handbooks or otherwise: in particular, I accept responsibility for any loss or damage to
                                the School property rightly attributable to me, <span style="color:red">and I acknowledge that I am ultimately personally liable
                                    for the payment of any tuitions fee unpaid by my sponsors</span>.
                            </p>
                        </div>
                        <div class="row">
                            <p>
                                <strong>4</strong>. I agree to use the School’s computing facilities and resources in a responsible way and to abide by the
                                rules and policies that govern their use. Specifically, I will adhere to the School’s Acceptable Use Policy,
                                all legislative Acts and Laws described therein.
                            </p>
                        </div>
                        <div class="row">
                            <p>
                                <strong>5</strong>. I agree to access and read the <span style="color:red">programme handbook (and placement handbook where applicable)</span>
                                which will be made available as a .pdf document once my registration has been completed.
                            </p>
                        </div>
                        <div class="row">
                            <p>
                                <strong>6</strong>. I understand that I must keep my contact details up to date at all times.
                            </p>
                        </div>
                        <div class="row">
                            <p>
                                <strong>7</strong>. I confirm that I have a valid permission to be in Vietnam and that all documents used to gain entry to
                                Saigon Star International School and enter Vietnam (where applicable) are genuine. I understand that
                                the School may require me to provide relevant documents to prove this at any point <span style="color:red">during my studies</span>.
                            </p>
                        </div>
                        <div class="row mt-20">
                            <div class="form-check">
                                <input form="update_form" class="form-check-input" type="checkbox" id="gridCheck1" required>
                                <label class="form-check-label" for="gridCheck1">
                                    I have read and accept the Terms and Conditions of Registration
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="modal-footer">
                <form action="/staff_info/skip" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="staff_id" value="{{ $staff->id }}">
                    <button class="primary-btn fix-tr-bg" id="save_button_query" type="submit">@lang('Skip')</button>
                </form>
                <form action="/staff_info/update" method="post" id="update_form">
                    {{ csrf_field() }}
                    <input type="hidden" name="staff_id" value="{{ $staff->id }}">
                    <button class="primary-btn fix-gr-bg" id="save_button_query" type="submit">@lang('lang.update')</button>
                </form>
            </div> --}}
        </div>
    </div>
</div>

<!-- ================End Footer Area ================= -->

<script src="{{asset('public/backEnd/')}}/vendors/js/jquery-3.2.1.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/jquery-ui.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/jquery.data-tables.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/dataTables.buttons.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/buttons.flash.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/jszip.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/pdfmake.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/vfs_fonts.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/buttons.html5.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/buttons.print.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/dataTables.rowReorder.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/dataTables.responsive.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/buttons.colVis.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/popper.js">
</script>
{{--<script src="{{asset('public/backEnd/')}}/vendors/js/bootstrap.min.js">--}}
{{--</script>--}}
<script src="{{asset('public/backEnd/')}}/css/rtl/bootstrap.min.js">
</script>

<link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/js/select2/select2.css"/>
<script src="{{asset('public/backEnd/')}}/vendors/js/nice-select.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/jquery.magnific-popup.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/fastselect.standalone.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/raphael-min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/morris.min.js">
</script>
<!--<script src="{{asset('public/backEnd/')}}/vendors/js/ckeditor.js"></script>-->

<script type="text/javascript" src="{{asset('public/backEnd/')}}/vendors/js/toastr.min.js"></script>

<!--<script type="text/javascript" src="{{asset('public/backEnd/')}}/vendors/js/moment.min.js"></script>-->
<!--<script src="{{asset('public/backEnd/')}}/vendors/js/ckeditor.js"></script>-->
<!--<script src="{{asset('public/backEnd/')}}/vendors/js/bootstrap_datetimepicker.min.js"></script>-->
<!--<script src="{{asset('public/backEnd/')}}/vendors/js/bootstrap-datepicker.min.js"></script>-->

<!--<script type="text/javascript" src="{{asset('public/backEnd/')}}/vendors/js/fullcalendar.min.js"></script>-->


<script type="text/javascript" src="{{asset('public/backEnd/')}}/js/jquery.validate.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/select2/select2.min.js"></script>

<script src="{{asset('public/backEnd/')}}/js/custom.js"></script>


@if(App\SmGeneralSettings::isModule('Saas')== TRUE)
<script src="{{asset('public/backEnd/')}}/saas/js1/custom.js"></script>
@endif

<script src="{{asset('public/')}}/js/registration_custom.js"></script>
<script src="{{asset('public/backEnd/')}}/js/developer.js"></script>
<script src="{{asset('Modules/ChatBox/')}}/public/js/chatbox.js"></script>
<script src="//js.pusher.com/3.1/pusher.min.js"></script>


<script src='https://onesignal.github.io/emoji-picker/lib/js/config.js'></script>
<script src='https://onesignal.github.io/emoji-picker/lib/js/util.js'></script>
<script src='https://onesignal.github.io/emoji-picker/lib/js/jquery.emojiarea.js'></script>
<script src='https://onesignal.github.io/emoji-picker/lib/js/emoji-picker.js'></script>

<script type="text/javascript">
    @if($update_info)
        $(document).ready(function() {
            $("#update_info").modal("show");
    });
    @endif
    //$('table').parent().addClass('table-responsive pt-4');
    // for select2 multiple dropdown in send email/Sms in Individual Tab
    
    $("#checkbox").click(function () {
        if ($("#checkbox").is(':checked')) {
            $("#selectStaffss > option").prop("selected", "selected");
            $("#selectStaffss").trigger("change");
        } else {
            $("#selectStaffss > option").removeAttr("selected");
            $("#selectStaffss").trigger("change");
        }
    });


    // for select2 multiple dropdown in send email/Sms in Class tab
    
    $("#checkbox_section").click(function () {
        if ($("#checkbox_section").is(':checked')) {
            $("#selectSectionss > option").prop("selected", "selected");
            $("#selectSectionss").trigger("change");
        } else {
            $("#selectSectionss > option").removeAttr("selected");
            $("#selectSectionss").trigger("change");
        }
    });
</script>

<script>
    $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: 'http://onesignal.github.io/emoji-picker/lib/img/',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });
</script>

 <script>


    $('.close_modal').on('click', function() {
        $('.custom_notification').removeClass('open_notification');
    });
    $('.notification_icon').on('click', function() {
        $('.custom_notification').addClass('open_notification');
    });
    $(document).click(function(event) {
        if (!$(event.target).closest(".custom_notification").length) {
            $("body").find(".custom_notification").removeClass("open_notification");
        }
    });
    
    $(document).ready(function() {
        $("#msg_history").on("scroll", function() {
            if($("#msg_history")[0].scrollTop == 0){
                for (i = 0; i < $("#msg_history")[0].children.length; i++) {
                    if($("#msg_history")[0].children[i].className.indexOf("active show") != -1){
                        var divId = $("#msg_history")[0].children[i].id;
                        var cons_id = $("#msg_history")[0].children[i].id.slice(4);
                        break;
                    }
                }
                if($("#max_record" + cons_id).val() > $('#messDiv' + cons_id).children().length){
                    $(".spinload").css('display', 'block');
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "/chat/loadmore/" + cons_id + "/" + $('#messDiv' + cons_id).children().length,
                        cache: false,
                        success: function(data) {
                            
                            for (i = 0; i < data.details.length; i++) {
                                if(data.details[i].posted_by != $('#authUser_id').val()){
                                    if(data.details[i].detail == null){
                                        if(data.details[i].images.length > 0){
                                            if(data.details[i].images.length > 1){
                                                $('#messDiv' + cons_id + ' ').prepend("<div class='incoming_msg'><div class='incoming_msg_img'> <img data-toggle='tooltip' title='" + data.incomFullName + "' style='border-radius: 50%;' src='/" + data.avt + "' alt='avatar'></div> <div class='received_msg'><div class='received_withd_msg'><p class='multiImages' id='detail" + i + "'></p><span class='time_date'>" + data.time[i] + "</span></div></div></div>"); 
                                                for(j = 0; j < data.details[i].images.length; j++){
                                                    $('#detail' + i + ' ').prepend("<a target='_blank' data-lightbox='cons" + cons_id + "' href='/" + data.details[i].images[j].url + "'><img width='" + (93/data.details[i].images.length) + '%' + "' height='100px' src='/" + data.details[i].images[j].url + "'alt=''> </a>"); 
                                                }
                                            }else{
                                                $('#messDiv' + cons_id + ' ').prepend("<div class='incoming_msg'><div class='incoming_msg_img'> <img data-toggle='tooltip' title='" + data.incomFullName + "' style='border-radius: 50%;' src='/" + data.avt + "' alt='avatar'></div> <div class='received_msg'><div class='received_withd_msg'><a class='oneImg' target='_blank' data-lightbox='cons" + cons_id + "' href='/" + data.details[i].images[0].url + "'><img src='/" + data.details[i].images[0].url + "'alt=''> </a><span class='time_date'>" + data.time[i] + "</span></div></div></div>"); 
                                            }
                                                  
                                        }else if(data.details[i].files){
                                            $('#messDiv' + cons_id + ' ').prepend("<div class='incoming_msg'><div class='incoming_msg_img'> <img data-toggle='tooltip' title='" + data.incomFullName + "' style='border-radius: 50%;' src='/" + data.avt + "' alt='avatar'></div> <div class='received_msg'><div class='received_withd_msg'><a href='/" + data.details[i].files.url + "' download><img width='60px' src='https://icon-library.com/images/file-icon/file-icon-6.jpg'>" + data.details[i].files.url.slice(data.details[i].files.url.lastIndexOf("/") + 1) + "</a><span class='time_date'>" + data.time[i] + "</span></div></div></div>")
                                        }
                                        
                                    }
                                    else{
                                      $('#messDiv' + cons_id + ' ').prepend("<div class='incoming_msg'><div class='incoming_msg_img'> <img data-toggle='tooltip' title='" + data.incomFullName + "' style='border-radius: 50%;' src='/" + data.avt + "' alt='avatar'></div> <div class='received_msg'><div class='received_withd_msg'><p>" + data.details[i].detail + "</p><span class='time_date'>" + data.time[i] + "</span></div></div></div>")
                                    }
                                }
                                else{
                                    
                                    if(data.details[i].detail == null){
                                        if(data.details[i].images.length > 0){
                                            if(data.details[i].images.length > 1){
                                                $('#messDiv' + cons_id + ' ').prepend("<div class='outgoing_msg'><div class='sent_msg'><p class='multiImages' id='detail" + i + "'></p><span class='time_date'>" + data.time[i] + "</span></div></div>"); 
                                                for(j = 0; j < data.details[i].images.length; j++){
                                                    $('#detail' + i + ' ').prepend("<a target='_blank' data-lightbox='cons" + cons_id + "' href='/" + data.details[i].images[j].url + "'><img width='" + (93/data.details[i].images.length) + '%' + "' height='100px' src='/" + data.details[i].images[j].url + "'alt=''> </a>"); 
                                                }
                                            }else{
                                                $('#messDiv' + cons_id + ' ').prepend("<div class='outgoing_msg'><div class='sent_msg'><a class='oneImg' target='_blank' data-lightbox='cons" + cons_id + "' href='/" + data.details[i].images[0].url + "'><img src='/" + data.details[i].images[0].url + "'alt=''> </a><span class='time_date'>" + data.time[i] + "</span></div></div>"); 
                                            }
                                           
                                        }
                                        else if(data.details[i].files){
                                            $('#messDiv' + cons_id + ' ').prepend("<div class='outgoing_msg'><div class='sent_msg'><a href='/" + data.details[i].files.url + "' download><img width='60px' src='https://icon-library.com/images/file-icon/file-icon-6.jpg'>" + data.details[i].files.url.slice(data.details[i].files.url.lastIndexOf("/") + 1) + "</a><span class='time_date'>" + data.time[i] + "</span></div></div>"); 
                                        }
                                        
                                    }
                                    else{
                                        $('#messDiv' + cons_id + ' ').prepend("<div class='outgoing_msg'><div class='sent_msg'><p>" + data.details[i].detail + "</p><span class='time_date'>" + data.time[i] + "</span></div></div>"); 
                                    }
                                }
                            }
                            $(".spinload").css('display', 'none');
                            $("#msg_history").scrollTop(500);
                        }
                    });
                }
            }
            
        });
    });

    $(document).ready(function() {
        $("#gr_msg_history").on("scroll", function() {
            if($("#gr_msg_history")[0].scrollTop == 0){
                for (i = 0; i < $("#gr_msg_history")[0].children.length; i++) {
                    if($("#gr_msg_history")[0].children[i].className.indexOf("active show") != -1){
                        var divId = $("#gr_msg_history")[0].children[i].id;
                        var gr_cons_id = $("#gr_msg_history")[0].children[i].id.slice(7);
                        break;
                    }
                }
                if($("#gr_max_record" + gr_cons_id).val() > $('#gr_messDiv' + gr_cons_id).children().length){
                    $(".spinload").css('display', 'block');
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "/chat/gr-loadmore/" + gr_cons_id + "/" + $('#gr_messDiv' + gr_cons_id).children().length,
                        cache: false,
                        success: function(data) {
                            
                            for (i = 0; i < data.details.length; i++) {
                                if(data.details[i].posted_by != $('#authUser_id').val()){
                                    if(data.details[i].images.length > 0){
                                        if(data.details[i].images.length > 1){
                                            $('#gr_messDiv' + gr_cons_id + ' ').prepend("<div class='incoming_msg'><div class='incoming_msg_img'> <img data-toggle='tooltip' title='" + data.incomFullName[i] + "' style='border-radius: 50%;' src='/" + data.avt[i] + "' alt='avatar'></div> <div class='received_msg'><div class='received_withd_msg'><p class='multiImages' id='gr_detail" + i + "'></p><span class='time_date'>" + data.time[i] + "</span></div></div></div>"); 
                                            for(j = 0; j < data.details[i].images.length; j++){
                                                $('#gr_detail' + i + ' ').prepend("<a target='_blank' data-lightbox='gr_cons" + gr_cons_id + "' href='/" + data.details[i].images[j].url + "'><img width='" + (93/data.details[i].images.length) + '%' + "' height='100px' src='/" + data.details[i].images[j].url + "'alt=''> </a>"); 
                                            }
                                        }else{
                                            $('#gr_messDiv' + gr_cons_id + ' ').prepend("<div class='incoming_msg'><div class='incoming_msg_img'> <img data-toggle='tooltip' title='" + data.incomFullName[i] + "' style='border-radius: 50%;' src='/" + data.avt[i] + "' alt='avatar'></div> <div class='received_msg'><div class='received_withd_msg'><a class='oneImg' target='_blank' data-lightbox='gr_cons" + gr_cons_id + "' href='/" + data.details[i].images[0].url + "'><img src='/" + data.details[i].images[0].url + "'alt=''> </a><span class='time_date'>" + data.time[i] + "</span></div></div></div>"); 
                                        }
                                    }else if(data.details[i].files){
                                        $('#gr_messDiv' + gr_cons_id + ' ').prepend("<div class='incoming_msg'><div class='incoming_msg_img'> <img data-toggle='tooltip' title='" + data.incomFullName[i] + "' style='border-radius: 50%;' src='/" + data.avt[i] + "' alt='avatar'></div> <div class='received_msg'><div class='received_withd_msg'><a href='/" + data.details[i].files.url + "' download><img width='60px' src='https://icon-library.com/images/file-icon/file-icon-6.jpg'>" + data.details[i].files.url.slice(data.details[i].files.url.lastIndexOf("/") + 1) + "</a><span class='time_date'>" + data.time[i] + "</span></div></div></div>")
                                    }
                                    else{
                                      $('#gr_messDiv' + gr_cons_id + ' ').prepend("<div class='incoming_msg'><div class='incoming_msg_img'> <img data-toggle='tooltip' title='" + data.incomFullName[i] + "' style='border-radius: 50%;' src='/" + data.avt[i] + "' alt='avatar'></div> <div class='received_msg'><div class='received_withd_msg'><p>" + data.details[i].detail + "</p><span class='time_date'>" + data.time[i] + "</span></div></div></div>")
                                    }
                                }
                                else{
                                    if(data.details[i].detail == null){
                                        if(data.details[i].images.length > 0){
                                            if(data.details[i].images.length > 1){
                                                $('#gr_messDiv' + gr_cons_id + ' ').prepend("<div class='outgoing_msg'><div class='sent_msg'><p class='multiImages' id='detail" + i + "'></p><span class='time_date'>" + data.time[i] + "</span></div></div>"); 
                                                for(j = 0; j < data.details[i].images.length; j++){
                                                    $('#detail' + i + ' ').prepend("<a target='_blank' data-lightbox='gr_cons" + gr_cons_id + "' href='/" + data.details[i].images[j].url + "'><img width='" + (93/data.details[i].images.length) + '%' + "' height='100px' src='/" + data.details[i].images[j].url + "'alt=''> </a>"); 
                                                }
                                            }else{
                                                $('#gr_messDiv' + gr_cons_id + ' ').prepend("<div class='outgoing_msg'><div class='sent_msg'><a class='oneImg' target='_blank' data-lightbox='gr_cons" + gr_cons_id + "' href='/" + data.details[i].images[0].url + "'><img src='/" + data.details[i].images[0].url + "'alt=''> </a><span class='time_date'>" + data.time[i] + "</span></div></div>"); 
                                            }
                                        }
                                        else{
                                            $('#gr_messDiv' + gr_cons_id + ' ').prepend("<div class='outgoing_msg'><div class='sent_msg'><a href='/" + data.details[i].files.url + "' download><img width='60px' src='https://icon-library.com/images/file-icon/file-icon-6.jpg'>" + data.details[i].files.url.slice(data.details[i].files.url.lastIndexOf("/") + 1) + "</a><span class='time_date'>" + data.time[i] + "</span></div></div>"); 
                                        }
                                    }
                                    else{
                                        $('#gr_messDiv' + gr_cons_id + ' ').prepend("<div class='outgoing_msg'><div class='sent_msg'><p>" + data.details[i].detail + "</p><span class='time_date'>" + data.time[i] + "</span></div></div>"); 
                                    }
                                }
                            }
                            $(".spinload").css('display', 'none');
                            $("#gr_msg_history").scrollTop(500);
                        }
                    });
                }
            }
            
        });
    });
    
    
    $(document).ready(function() {
        $("#saveGroupName").on("click", function() {
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "/chat/group/update-name-people/" + $('#groupId').val() + '/' + $("#group_name_sidebar").val(),
                cache: false,
                success: function(data) {
                    $("#saveGroupName").css("display", "none");
                    $("#cancelEditGroupName").css("display", "none");
                    $("#group_name_sidebar_temp").val($("#group_name_sidebar").val());
                    $("#infoGroup" + $('#groupId').val() + " p").first().text($("#group_name_sidebar").val());
                    $("#gr_cons" + $('#groupId').val() + " div").first().children("span").text($("#group_name_sidebar").val());
                    $("#chat_ib" + $('#groupId').val() + " h5").text($("#group_name_sidebar").val());
                }
            });
        });
    });
    
    $(document).ready(function() {
        $("#prevImage").on("click", function() {
            $(".lb-prev")[0].click();
        });
        
        $("#nextImage").on("click", function() {
            $(".lb-next")[0].click();
        });
    });
    
    $(document).ready(function() {
        $(".infoOneOne").on("click", function() {
            $("#rightMenuOneOne").css("display", "block");
            $("#avtOneOne").attr("src", $(this).children(".infoOneOne_f").text());
            $("#name_sidebar").text($(this).children(".infoOneOne_s").text());
            $('#loadImage').empty();
            $('#loadFile').empty();
            $.ajax({
                type: "GET",  
                url: "/chat/getInfoImgFile/" + $(this).children(".infoOneOne_t").text(),
                dataType: "json",
                processData: false,
                contentType: false,
                success:function(data){
                    for (i = 0; i < data.images.length; i++) {
                        var url = data.images[i].url;
                        $('#loadImage').append('<a href="' + url + '" data-lightbox="cons' + $(this).children(".infoOneOne_t").text() + '"><img src="' + url + '" alt=""></a>');
                    }
                    
                    for (i = 0; i < data.files.length; i++) {
                        var url = data.files[i].url;
                        $('#loadFile').append('<div style="padding: 15px;"><img src="/public/svg/files.svg" width="30px"><a style="margin-left: 10px;" download href="' + url + '">' + url.replace('Modules/ChatBox/public/files/', "") + '</a></div>');
                    }
                }                  
            });  
        });
    });
    
    $(document).ready(function() {
        $(".imageShared").on("click", function() {
            if($("#loadImage").css("display") == "block"){
                $("#loadImage").css("display", "none");
                $("#img_f").css("display", "block");
                $("#img_s").css("display", "none");
            }else{
                $("#loadImage").css("display", "block");
                $("#img_f").css("display", "none");
                $("#img_s").css("display", "block");
            }
        });
    });
    
    $(document).ready(function() {
        $(".fileShared").on("click", function() {
            if($("#loadFile").css("display") == "block"){
                $("#loadFile").css("display", "none");
                $("#file_f").css("display", "block");
                $("#file_s").css("display", "none");
            }else{
                $("#loadFile").css("display", "block");
                $("#file_f").css("display", "none");
                $("#file_s").css("display", "block");
            }
        });
    });
    
</script>
@yield('script')

</body>
</html>


