<style>
    #gr_name{
        display: none;
    }
    .online_list_user{
        width: 10px;
        height: 10px;
        position: absolute;
        background: #08d408;
        border-radius: 50%;
        bottom: 0;
        left: 12%;
        box-shadow: 0px 0px 0px 6px #fff;
    }
</style>
<div style="height: 450px">
    <div class="srch_bar">
        <ul class="nav navbar-nav mr-auto search-bar" style="z-index:0">
            <li class="">
                <div style="background: #ebebeb69; border-radius: 20px;"class="input-group" id="serching">
                    <span style="padding: 10px">
                        <i class="ti-search" aria-hidden="true" id="search-icon"></i>
                    </span>
                    <input type="text" class="form-control primary-input input-left-icon" placeholder="Search"
                        onkeyup="contactShowResult(this.value)" style="border-bottom: none; padding: 20px"/>
                </div>
            </li>
        </ul>
    </div>
    <form id="formmm" method="POST" action="/chat/new/cons" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        @csrf
        <div style="margin: auto; width: 35%;">
            <input style="width: 15px; height: 15px; margin-top: 5px" type="checkbox" onclick="checkAll(this)" class="common-checkbox" id="check_all">
            <label style="margin-top: 10px" class="ml-4" for="check_all">Check All</label>
        </div>
        
        <div style="overflow-y: scroll; height: 280px">
            @foreach ($users as $index=>$user)
                @php
                    $focusUser = App\User::find($user->id);
                    if ($focusUser->role_id == 2) {
                        $LoginUser = App\SmStudent::where('user_id', $focusUser->id)->first();
                        $avatar = $LoginUser ? $LoginUser->student_photo : '';
                    } elseif ($focusUser->role_id == 3) {
                        $LoginUser = App\SmParent::where('user_id', $focusUser->id)->first();
                        $avatar = $LoginUser ? $LoginUser->fathers_photo : '';
                    } else {
                        $LoginUser = App\SmStaff::where('user_id', $focusUser->id)->first();
                        $avatar = $LoginUser ? $LoginUser->staff_photo : '';
                    }
                @endphp
                <div class="mt-2 mb-4 contact-name" style="color: #828bb2; display: flex; position: relative">
                    @if(Cache::has('user-is-online-' . $user->id))
                        <span class="online_list_user"></span>
                    @endif
                    <img class="ml-3 mr-3" src="{{ file_exists($avatar) ? asset($avatar) : asset('public/uploads/staff/demo/staff.jpg') }}" style="border-radius: 50%; width: 40px" >
                    <input style="width: 15px; height: 15px; margin-top: 5px" onclick="countChecked()" type="checkbox" class="mr-2 common-checkbox" name="user_ids[]" value="{{$user->id}}" id="{{$index}}{{$user->email}}">
                    <label style="margin-top: 10px" for="{{$index}}{{$user->email}}">{{$user->full_name}}</label>
                </div>
            @endforeach 
        </div>
        <div id="gr_name" class="col-lg-12 mt-3">
            <div class="input-effect">
                <input class="primary-input form-control has-content"
                type="text" name="conv_name" autocomplete="off">
                <label>Group Name<span></span> </label>
                <span class="focus-border textarea"></span>
            </div>
        </div>
        <button class="primary-btn fix-gr-bg mt-3 optionBtn" style="margin-left: 150px" type="submit">Apply</button>  
    </form>
</div>
<script>
    function checkAll(x){
        if(x.checked == true){
            for (var i = 0; i < document.getElementsByClassName('contact-name').length; i++){
                document.getElementsByClassName('contact-name')[i].children[1].checked = true;
            }
            document.getElementById("gr_name").style.display = "block";
            document.getElementById("formmm").action = "/chatbox/create-group";
        }else{
            for (var i = 0; i < document.getElementsByClassName('contact-name').length; i++){
                document.getElementsByClassName('contact-name')[i].children[1].checked = false;
            }
            document.getElementById("gr_name").style.display = "none";
            document.getElementById("formmm").action = "/chat/new/cons";
        }
    }
    function countChecked(){
        var count = 0;   
        for(var i = 0; i < document.getElementsByClassName('contact-name').length; i++){
            if(document.getElementsByClassName('contact-name')[i].children[1].checked == true){
                count += 1;
            }
        }
        if(count > 1){
            document.getElementById("gr_name").style.display = "block";
            document.getElementById("formmm").action = "/chatbox/create-group";
        }else{
            document.getElementById("gr_name").style.display = "none";
            document.getElementById("formmm").action = "/chat/new/cons";
        }
    }
    function contactShowResult(x){
        if(x == false){
            for(var i = 0; i < document.getElementsByClassName('contact-name').length; i ++){
                document.getElementsByClassName('contact-name')[i].style.display = "flex";
            }
        }
        else{
            for(var i = 0; i < document.getElementsByClassName('contact-name').length; i ++){
                if(document.getElementsByClassName('contact-name')[i].children[2].innerText.toLowerCase().indexOf(x.toLowerCase()) == -1){
                    document.getElementsByClassName('contact-name')[i].style.display = "none";
                }
            }
        }
    }
</script>