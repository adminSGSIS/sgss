@php
    $departments = App\SmHumanDepartment::where('active_status', '=', '1')->where('school_id', Auth::user()->school_id)->get();
    $staffs = App\SmStaff::where('school_id', Auth::user()->school_id)->get();
@endphp

{{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'chatbox/create-multiple', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}

    <div class="col-lg-12">
        <select class="niceSelect w-100 bb form-control mb-3" onchange="addStaffFunc()" name="role_id" id="role_id">
            <option data-display="Role" value=""> @lang('lang.select') </option>
            @foreach($role as $key=>$value)
                @php
                    $InfixRole = Modules\RolePermission\Entities\InfixRole::find($value);
                @endphp
                <option value="{{$value}}">{{$InfixRole->name}}</option>
            @endforeach
        </select>
        <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="addStaff" onclick="addStaffButton()" >
            <img width="20px" src="https://static.thenounproject.com/png/261370-200.png" alt="">
            <label style="position: absolute; top: 53px; width: 100px">ADD STAFF</label>
        </button>
        <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="closeAddStaff" onclick="closeAddStaffButton()">
            <img style="margin-top: -3px" width="20px" src="https://static.thenounproject.com/png/261368-200.png" alt="">
            <label style="position: absolute; top: 50px; width: 100px">ADD STAFF</label> 
        </button>
        @foreach ($role as $value)
        <div style="display: none" id="r{{$value}}">
            @foreach ($users as $user)
                @if ($user->role_id == $value)
                    <div class="mt-2 {{ $errors->has('staffs') ? ' is-invalid' : '' }}" style="color: #828bb2; display: flex">
                        <input style="width: 15px; height: 15px; margin-top: 5px" type="checkbox" class="mr-2 common-checkbox" name="user_ids[]" value="{{$user->id}}" id="{{$user->email}}">
                        <label for="{{$user->email}}">{{$user->full_name}}</label>
                    </div>
                @endif
            @endforeach
        </div>
        @endforeach
    </div>

<div class="col-lg-12 mb-5 mt-4">
    <div class="input-effect">
        <textarea class="primary-input form-control has-content"
        type="text" name="detail" autocomplete="off" cols="30" rows="10" required></textarea>
        <label>Content<span></span> </label>
        <span class="focus-border textarea"></span>
    </div>
</div>
<button class="primary-btn fix-gr-bg mt-3 optionBtn" style="margin-left: 150px" type="submit">Apply</button>  
{{ Form::close() }}
<script>
    var id, oldId;
    function addStaffFunc(){
        var x = document.getElementById("role_id").value;
        id = x;
        document.getElementById("addStaff").style.display = 'block';
        if(oldId){
            document.getElementById('r'+oldId).style.display = 'none';
        }
        else{
            document.getElementById('r'+x).style.display = 'none';
        }
        
        document.getElementById('closeAddStaff').style.display = 'none';
    }
    function addStaffButton(){
        oldId = id;
        document.getElementById('r'+id).style.display = 'block';
        document.getElementById('closeAddStaff').style.display = 'block';
        document.getElementById('addStaff').style.display = 'none';
    }
    function closeAddStaffButton(){
        document.getElementById('r'+id).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
        document.getElementById('addStaff').style.display = 'block';
    }
</script>