<a class="deleteUrl removeHover dropdown-item" data-modal-size="modal-md" title="New Conversation" href="/chat/new/cons" >
    <button type="button" class="primary-btn fix-gr-bg mt-3 optionBtn" style="width: 100%">New Conversation</button>
</a>

<a class="deleteUrl removeHover dropdown-item" data-modal-size="modal-md" title="New Group" href="/chat/new/group" >
    <button type="button" class="primary-btn fix-gr-bg mt-3 optionBtn" style="width: 100%">New Group</button>
</a>
<a class="deleteUrl removeHover dropdown-item" data-modal-size="modal-md" title="New Multiple" href="/chat/new/multiple" >
    <button type="button" class="primary-btn fix-gr-bg mt-3 optionBtn" style="width: 100%">New Multiple</button>
</a>