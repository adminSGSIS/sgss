@extends('backEnd.master')
@section('mainContent')


	
<div class="container">
	<form id="create-group" method="post" action="/chatbox/create-group">
			@csrf
			<h3>Create Group Conversation</h3>
			<div class="row mt-40">
                            <div class="col-lg-12" id="selectTeacherDiv">
                                <label for="checkbox" class="mb-2">@lang('lang.user') *</label>
                                    <select multiple id="selectSectionss" {{ @$errors->has('user_ids') ? ' is-invalid' : '' }} name="user_ids[]" style="width:300px">
                                        @foreach($users as $user)
                                            
                                                <option value="{{$user->id }}">{{$user->full_name }}</option>
                                            

                                        @endforeach
                                    </select>
                                    <span>** You can select more than 1 user to create a group conversation **</span>
                                    @if ($errors->has('user_ids'))
                                        <span class="invalid-feedback" role="alert" style="display:block">
                                            <strong>{{ $errors->first('user_ids') }}</strong>
                                        </span>
                                    @endif
                            </div>
                        </div>
			
			<label>Name (optional)</label>
			<input type="text" name="conv_name" class="form-control">
			<label>Subject (optional)</label>
			<textarea name="subject" class="form-control"></textarea>
			<br>
			<button class="btn btn-primary">submit</button>
	</form>
	
</div>

@endsection

