@extends('backEnd.master')
@section('mainContent')	
<div class="container">
	<form id="create-group" method="post" action="{{url('chatbox/update')}}">
			@csrf
            <input type="hidden" name="id_conv" value="@if(isset($conv)) {{$conv->id}} @endif">
			<h3>Edit Conversation</h3>
			<div class="row mt-40">
                            <div class="col-lg-12" id="selectTeacherDiv">
                                <label for="checkbox" class="mb-2">@lang('lang.user') *</label>
                                    <select multiple id="selectSectionss" {{ @$errors->has('user_ids') ? ' is-invalid' : '' }} name="user_ids[]" style="width:300px">
                                    
                                        @foreach($users as $user)                                            
                                                <option  value="{{$user->id }}"@if(isset($edit)) @foreach($edit as $editdata) @if($editdata->id == $user->id) selected @endif @endforeach @endif>{{$user->full_name }}</option>                                      
                                        @endforeach
                                    
                                    </select>
                                    <span>** You can select more than 1 user **</span>
                                    @if ($errors->has('user_ids'))
                                        <span class="invalid-feedback" role="alert" style="display:block">
                                            <strong>{{ $errors->first('user_ids') }}</strong>
                                        </span>
                                    @endif
                            </div>
                        </div>			
			<label>Name (optional)</label>
			<input type="text" name="conv_name" class="form-control" value="@if(isset($conv)) {{$conv->name}}  @endif ">
			<label>Subject (optional)</label>
			<textarea name="subject" class="form-control">@if(isset($conv)) {{$conv->subject}} @endif</textarea>
			<br>
			<button class="btn btn-primary">submit</button>
	</form>
</div>
@endsection

