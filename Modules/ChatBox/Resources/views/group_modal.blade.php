<div class="modal fade" id="newGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Add to Group</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="col-lg-12" id="selectTeacherDiv">
                              <form action="#" method="post">
                                @csrf
                                <label for="checkbox" class="mb-2">@lang('lang.user') *</label>
                                    <select required="" multiple id="selectSections" name="user_ids[]" style="width:300px" class="groupUsers" >
                                        
                                    </select>
                                    <span>** You can select more than 1**</span><br>
                                    
                                   
                                  <br>
                                  <button class="btn btn-primary" type="button" id="add_to_group">submit</button>
                                  </form>
                            </div>
              </div>
              
            </div>
          </div>
        </div> 