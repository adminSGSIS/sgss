@extends('backEnd.master')
@section('css')
<link rel="stylesheet" href="{{asset('Modules/ChatBox/')}}/public/css/chat.css">
@endsection
@section('mainContent')
<div class="content">
<input type="hidden" id="auth_id" name="auth_id" value="{{Auth::user()->id}}">
<input type="hidden" id="current_con_id" name="current_con_id" @if(isset($id)) value="{{$id}}" @else value="0" @endif>

  <div class="row rounded-lg overflow-hidden shadow">
    <!-- Users box-->
    <div class="col-lg-4 col-md-5 px-0 message-background">
      <div class="bg-white">
        <div class="bg-gray px-4 py-2 bg-light">
          <div class="row">
            <div class="col-8">
              <form action="#" method="post">
                @csrf
                <div class="row">
                  <div class="col-12">
                    <div class="input-effect">
                      <input class="primary-input form-control" type="text" name="search" id="search_data" placeholder="Search" value="">
                    </div>
                  </div>
                  
                </div>
              </form>
          </div>
            <div class="col-1"><button id="refresh" class="btn btn-info"><i class="fa fa-refresh" aria-hidden="true"></i></button></div>
            <div class="col-1">
              <button id="refresh" class="btn btn-info" data-toggle="dropdown"><i class="fa fa-plus" aria-hidden="true"></i></button>
                    <div class="dropdown-menu">
                      
                      <a href="#" class="dropdown-item"  data-toggle="modal" type="button" data-target="#newGroup">New group</a>
                      <a href="#" class="dropdown-item"  data-toggle="modal" type="button" data-target="#newMessage">New message</a>
                      
                    </div>
            </div>
          </div>

          <!-- Modal New Group  -->
          
        <!-- Modal New Group  -->   
        <!-- Modal New Message  -->
         
          
        <!-- Modal New Message  -->  
          <div class="spinner-border text-muted"></div>
        </div>
        @if(isset($cons))
        <div class="pre-loading">
            <div class="loader2"></div>
          </div>       
        <div class="messages-box">
          
          @foreach($cons as $con)
          <div id="conv{{$con->id}}" class="list-group-item list-group-item-action @if(isset($id)&&$con->id == $id) active1 @endif text-white rounded-0" >
              <div class="row">
                <div class="col-10">
                    <a href="{{url('chatbox/Msg-detail')}}/{{$con->id}}" id="{{$con->id}}" >
                  <div class="media"><img src="{{asset('public/sg_star_logo.png')}}" alt="user" width="50" class="rounded-circle">
                    
                    <div class="media-body ml-4">
                      <div class="d-flex align-items-center justify-content-between mb-1">
                        <p style="font-size: 18px; color: black;" class="font-weight-bold mb-0 text-big">{{$con->name}}</p>                      
                      </div>
                      <p class="font-italic mb-0 text-small">{{$con->subject}}</p>
                    </div>
                  </div>
                </a>
                </div>
                <div class="col-1">
                  @if($con->status == 1)                
                        <i id="HaveMessage{{$con->id}}" class="material-icons settings" style="font-size:30px;color:gray;display: none;">add_alert</i>
                  @else
                        <i id="HaveMessage{{$con->id}}" class="material-icons settings" style="font-size:30px;color:gray;display: block;">add_alert</i>
                  @endif
                                                          
                </div>
                <div class="col-1">
                  <div class="dropdown">
                    <button type="button" class="fa fa-ellipsis-v button1" data-toggle="dropdown">
                     
                    </button>
                    <div class="dropdown-menu">
                      @if($con->created_by == Auth::user()->id)
                      <a class="dropdown-item" href="{{url('chatbox/settings')}}/{{$con->id}}">Settings</a>
                      <a class="dropdown-item delete" id="{{$con->id}}" href="{{url('chatbox/delete')}}/{{$con->id}}" >Delete</a>
                      @else
                      <a class="dropdown-item leave" id="{{$con->id}}" href="{{url('chatbox/leave')}}/{{$con->id}}/{{Auth::user()->id}}" >Leave</a>
                      @endif
                      <a data-toggle="collapse" role="button" class="dropdown-item" href="#seeparticipants{{$con->id}}">Participants</a>
                    </div>
                  </div>
                </div>
            </div> 
            <br>
            <div class="collapse" id="seeparticipants{{$con->id}}">
              <p id="participants{{$con->id}}">@foreach($con->User as $user)<label class="btn btn-success btn-sm">{{$user->full_name}} </label>@endforeach</p>
            </div>
                    
          </div>
          @endforeach
        </div>
        
        @endif
      </div>
    </div>
    <!-- Chat Box-->
    <div class="col-lg-8 col-md-7 px-0">
      <div class="px-4 py-5 bg-white">
        
        <!-- Sender Message-->
        <div class="chat-box">
        @if(isset($details))
        @foreach($details as $detail)
        @if($detail->posted_by != Auth::user()->id)
        <div class="media w-50 mb-3"><img src="{{url('/')}}/{{$detail->image}}" alt="user" width="50" class="rounded-circle">
          <div class="media-body ml-3">
            <div class="bg-light rounded py-2 px-3 mb-2">
              @if($detail->Images)
              <img src="{{url($detail->Images->url)}}" width="100%" height="100%">
              @endif
              <p class="text-small mb-0 text-muted">{{$detail->detail}}</p>
            </div>
            <p class="small text-muted">from: {{$detail->user->full_name}} at: {{$detail->created_at}}</p>
          </div>
        </div>
        @elseif($detail->posted_by == Auth::user()->id)
        <!-- Reciever Message-->
        <div class="media w-50 ml-auto mb-3">
          <div class="media-body">
            <div class="bg-primary rounded py-2 px-3 mb-2">
              @if($detail->Images)
              <img src="{{url($detail->Images->url)}}" width="100%" height="100%">
              @endif
              <p class="text-small mb-0 text-white">{{$detail->detail}}</p>
            </div>
            <p class="small text-muted">from: {{$detail->user->full_name}} at: {{$detail->created_at}}</p>
          </div>
        </div>
        @endif
        @endforeach
        @endif
        </div>
        
      </div>

      <!-- Typing area -->
      
      
        <form action="#" class="bg-light">
        <div class="input-group">
          <input type="text" placeholder="Type a message" id="message" aria-describedby="button-addon2" class="form-control rounded-0 border-0 py-4 bg-light">
          <div class="input-group-append">
            <button id="button-addon1" type="button" class="btn btn-link"> <i class="fa fa-file-image-o"></i></button>
            <input type="file" id="upload" style="display:none">
            <button id="button-addon2" type="submit" class="btn btn-link"> <i class="fa fa-paper-plane"></i></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('script')
  <script type="text/javascript">
    // $( document ).ready(function() {
      
    //   $(".pre-loading").fadeOut("slow");
    //   $('#search_data').unbind().bind('input', function(){ 
    //     var data = $(this).val().toLowerCase();  
    //     if(data == "")
    //     {
    //       $('.list-group-item').show();    
    //     }
    //     else{
    //       $('.list-group-item').hide();
    //       $('.text-big').each(function(){            
    //           var check = $(this).text();
    //           var n = check.includes(data);            
    //           if(n==true){
    //             $(this).closest(".list-group-item").show();
    //           }             
    //       });
    //       $('.text-small').each(function(){            
    //           var check2 = $(this).text();
    //           var m = check2.includes(data);            
    //           if(m==true){
    //             $(this).closest(".list-group-item").show();
    //           }             
    //       });
    //     }
    //   });

      // $('#button-addon1').bind('click',function(){
      //     $('#upload').click();               
      // }); 
      // $('#upload').on('change',function(){ 
      //      var img_path = $('#upload')[0].files[0];
      //      if(img_path == null){return false;}
      //      var form_data = new FormData();
      //      var _token=  $('meta[name="_token"]').attr('content');
      //      form_data.append('file',img_path);
      //      form_data.append('token',_token);
      //      $.ajax({
      //           type: "POST",  
      //           url: "/chatbox/image_upload",
      //           dataType: "html",
      //           data: form_data,
      //           processData: false,
      //           contentType: false,
      //           success:function(data){
      //             $('#img-preview').css('display','block');               
      //             $('#img-preview').attr('src',"/"+data);
      //             img_source = data;
      //           }                  
      //       });       

      //     // if (this.files && this.files[0]) {
      //     //   var reader = new FileReader();
      //     //   reader.onload = function (e) { 
      //     //       $('#img-preview').css('display','block');               
      //     //       $('#img-preview').attr('src', e.target.result);                               
      //     //   }
      //     //   reader.readAsDataURL(this.files[0]);
      //     // }
      // });
      
    // });
  </script>

</script>
@endsection