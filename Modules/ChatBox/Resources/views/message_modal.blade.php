<div class="modal fade" id="newMessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">New Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="col-lg-12" id="selectTeacherDiv">
                                <label for="checkbox" class="mb-2">@lang('lang.user') *</label>
                                    <select class="form-control messageUsers" multiple  {{ @$errors->has('user_ids') ? ' is-invalid' : '' }} name="user_ids[]" style="width:300px">
                                        @foreach($users as $user)
                                            
                                                <option value="{{$user->id }}">{{$user->full_name }}</option>
                                      
                                        @endforeach
                                    </select>
                                    <span>** You can select more than 1**</span>
                                    @if ($errors->has('user_ids'))
                                        <span class="invalid-feedback" role="alert" style="display:block">
                                            <strong>{{ $errors->first('user_ids') }}</strong>
                                        </span>
                                    @endif
                                    <label>Name</label>
                                  <input required="" type="text" name="conv_name" id="message_name" class="form-control" value="">
                                  <label>Subject</label>
                                  <input required="" type="text" name="conv_name" id="message_subject" class="form-control" value="">
                                  <label>Detail</label>
                                  <textarea required="" name="subject" class="form-control" id="message_detail"> </textarea>
                                  <br>
                                  <button class="btn btn-primary" type="button" id="message_create">submit</button>
                            </div>
              </div>
              
            </div>
          </div>
        </div> 