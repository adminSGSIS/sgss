<?php
use Modules\ChatBox\Entities\conversationDetails;
use Spatie\GoogleCalendar\Event;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	
		Route::prefix('chatbox')->group(function() {
		    Route::get('/','AdminController@index');
			Route::get('Msg-detail/{id}','MessageController@getDetails');
			Route::get('Msg-detail2/{id}','MessageController@getDetails2');
			Route::get('admin','AdminController@index');
			Route::post('submit','AdminController@submit');
			Route::get('add','AdminController@add');
			Route::get('settings/{id}','MessageController@edit');
			Route::post('update','MessageController@update');
			Route::post('create-group','AdminController@createGroup');
			Route::post('checkAuth','AdminController@checkAuth');
			Route::get('add-multiple','AdminController@addMultiple');
			Route::post('create-multiple','AdminController@createMultiple');
			Route::get('check','AdminController@check');
			Route::get('leave/{conv_id}/{user_id}','AdminController@leave');
			Route::get('delete/{conv_id}','AdminController@delete');	
			Route::get('refresh_chat_list','AdminController@refresh_chat_list');
			Route::post('search','AdminController@search');	
			Route::post('image_upload','MessageController@imgUpload');
			Route::post('post_image','MessageController@postImage');

			Route::post('message_send','MessageOneOneController@sendMessage');
			Route::post('submitOneOne','MessageOneOneController@submitOneOne');
			Route::post('post_image2','MessageOneOneController@postImage2');

			Route::get('add-to-group/{id}','AdminController@getAddtogroup');
			Route::post('add_to_group','AdminController@Addtogroup');
			Route::get('kick/{group_id}/{user_id}','AdminController@kickgroup');
			Route::get('load_more/{group_id}/{node_id}','AdminController@loadmore');
			Route::get('load_more2/{talk_id}/{node_id2}','MessageOneOneController@loadmore2');
		});
		
		Route::get('chat','MessageOneOneController@chats');
		Route::get('chat/gr-loadmore/{id}/{take_number}','MessageOneOneController@grchatsLoadMore');
		Route::get('chat/loadmore/{id}/{take_number}','MessageOneOneController@chatsLoadMore');
		Route::get('chat/option','MessageOneOneController@chatsOption');
		Route::get('chat/new/cons','MessageOneOneController@chatsCons');
		Route::post('chat/new/cons','MessageOneOneController@newCons');
		Route::get('chat/new/group','MessageOneOneController@chatsGroup');
		Route::get('chat/new/multiple','MessageOneOneController@chatsMultiple');
		Route::get('chat/group/add-people/{id}','MessageOneOneController@chatsGroupAddPeople');
		Route::get('chat/group/edit-people/{id}','MessageOneOneController@chatsGroupEditPeople');
		Route::get('chat/group/update-name-people/{id}/{value}','MessageOneOneController@chatsGroupUpdateName');
		Route::post('chat/group/update-people','MessageOneOneController@chatsGroupUpdatePeople');
		Route::get('chat/contact','MessageOneOneController@chatsContact');


		Route::post('chat/submit','ChatController@submit');
		Route::post('chat/checkAuth','ChatController@checkAuth');

		Route::get('getroles/{id}','ChatController@getroles');
		Route::post('chat/image_upload','ChatController@imgUpload');
		Route::post('chat/gr_image_upload','ChatController@groupimgUpload');
		Route::post('chat/file_upload','ChatController@fileUpload');
		Route::post('chat/gr_file_upload','ChatController@groupfileUpload');
		Route::get('chat/update_status/{id}','ChatController@updateStatus');
		
		Route::get('chat/getImgList/{id}','ChatController@getImgList');
		Route::get('chat/getFileList/{id}','ChatController@getFileList');
		Route::get('chat/getNamesImages/{id}','ChatController@getNamesImages');
		Route::get('chat/getInfoImgFile/{id}','ChatController@getInfoImgFile');
		
		Route::get('chat/update',function(){
		    $events = Event::get();
            $i=0;
            $task="";
            $cDay = Carbon::today()->format('l d/m/Y');
            foreach ($events as $event) { 
                if( date("Y-m-d", strtotime($event->start->dateTime))==Carbon::today()->toDateString())
                {
                    $eventStartTime = date(" H : i", strtotime($event->start->dateTime));
                    $eventEndTime = date(" H : i", strtotime($event->end->dateTime));
                    $eventLocation = $event->location;
                    $eventTitle = "<a style='color:blue;' target='_blank' href=".$event->htmlLink.">".$event->summary."</a>";
                    $task =  $task."<div class='task'><b class='event_title'>".$eventTitle."</b><br>".$eventStartTime." - ".$eventEndTime."<br>".$eventLocation."</div><br>";
                    $i++;
                }   
    
            }
    
            if($i<1)
                $event = " event";
            else
                $event = " events";
            $count ="Today,".$cDay." we have ".$i.$event."<br>";
            $tasks = $count.$task;
		    $notice = new conversationDetails();
            $notice ->conversation_id = 28;
            $notice ->posted_by = 1;
            $notice ->detail = $tasks;
            $notice ->is_system_notice=1;
            $notice ->save();
		});
		
		
	

