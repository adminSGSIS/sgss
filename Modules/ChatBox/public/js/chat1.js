  $( document ).ready(function() {
    $('#list-group-chat').hide();  
    $(".loading-message").hide(); 
    $(".pre-loading").fadeOut("slow");
    $(".chat-box").animate({scrollTop: $('.chat-box').prop("scrollHeight")}, "fast"); 
    //lay id cua cuoc tro chuyen hien tai
    var conv_id = $('#current_con_id').val();
    var talking_id = -1;
    var message_type = -1;
    var auth_id = $('#auth_id').val();
    var get_count_message = $('#messagecounter').text(); 
    var count =parseInt(get_count_message);
    var img_source = "null";
    var message_id = -1;
    var id_one_to_one = -1;
    var loading =0;
    //Lay thoi gian hien tai
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var post_time = d.getFullYear() + '-' +
    ((''+month).length<2 ? '0' : '') + month + '-' +
    ((''+day).length<2 ? '0' : '') + day+" "+d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

    var pusher = new Pusher('d269c80a507e99df75c3', {
          cluster: "ap1",
          authEndpoint: "/chatbox/checkAuth",//Goi toi route xac minh user co quyen listen channel private nay khong
          auth: {
            headers: {
              'X-CSRF-Token': $('meta[name="_token"]').attr('content'),
            },params: {
              'conver_iv' : conv_id,
            }        
          }       
      });
    
  
    function sendMessage(){
      if(conv_id == 0) {
          return false;
        }
        if($.trim(message) == '') {
          return false;
        }
      var img_path = $('#upload')[0].files[0];
      var message = $('#message').val();
      // if(img_path == null)
      // {          
       // alert('khong co anh');
       if(conv_id == 0) {
          return false;
        }
        if($.trim(message) == '') {
          return false;
        }
        $('.loader').css('display','block');
        $('#button-addon2').hide();
          $.ajax({
                  url: '/chatbox/submit',
                  type: 'POST',
                  dataType: 'html',
                  
                  data: {
                      _token:   $('meta[name="_token"]').attr('content'),
                      img_source : img_source,
                      message   : message, 
                      auth_id   : auth_id,
                      conv_id   : conv_id,                
                  }
              }).done(function(data) {
                $('#message').val(""); 
                $('#img-preview').css('display','none');               
                   $('#img-preview').attr('src',"");
                    
              });
      
    } 
    function sendMessage2(){
      loading =1;
      var message = $('#message2').val();
      // if(img_path == null)
      // {          
       // alert('khong co anh');
       if(message_id == -1) {
          return false;
        }
        if($.trim(message) == '') {
          return false;
        }
        

        
          $.ajax({
                  url: '/chatbox/submit',
                  type: 'POST',
                  dataType: 'html',
                  
                  data: {
                      _token:   $('meta[name="_token"]').attr('content'),
                      img_source : img_source,
                      message   : message, 
                      auth_id   : auth_id,
                      conv_id   : message_id,                
                  }
              }).done(function(data) {
                loading=0
                $('#message2').val(""); 
                $('#img-preview').css('display','none');               
                $('#img-preview').attr('src',"");
                    
              });
      
    }       
    $('#button-addon2').on('click', function(e){ 
        e.preventDefault();   
          sendMessage();        
    });
    $('#button-addon3').on('click', function(e){ 
        e.preventDefault(); 
      
        if(message_type == 1)        
          {
            if(loading==0)
            {sendMessage2();}
            else{return false;}
          }
        else
          {
            if(loading==0)
            {sendMessage3();}
            else{return false;}
          }  

    });
    $(window).on('keydown', function(e) {
      if (e.which == 13) {     
          if(message_type == 1)        
          {
            if(loading==0)
            {sendMessage2();}
            else{return false;}

          }
        else
          {
           if(loading==0)
            {sendMessage3();}
            else{return false;}
          }  
          return false;
        }
    }); 
    $('#message-button').click(function(){
      $('.MesDetails3').css('background-color','');
      $('#list-message').show();
      $('#list-group-chat').hide();
      message_type = -1;
      talking_id = -1;
      message_id = -1;
      id_one_to_one = -1;
      $('.chat-box2').empty();
      $('#dropdown-users').empty();
      $('#dropdown-options').empty(); 
      

      
    });
    $('#group-button').click(function(){
      $('.MesDetails2').css('background-color','');
      $('#list-message').hide();
      $('#list-group-chat').show();
      message_type = 1;
      talking_id = -1;
      message_id = -1;
      id_one_to_one = -1;
      $('.chat-box2').empty();
      $('#dropdown-users').empty();
      $('#dropdown-options').empty(); 

      
    });

    $(document).on('click', '.message_1-1', function (e) {
      e.preventDefault();
    $(".loading-message").show();
        $('.MesDetails3').css('background-color','');
        $(this).css('background-color','#d9d7d7;'); 
          var id_user = $(this).attr('id');
          $.ajax({
                  url: '/chatbox/message_send',
                  type: 'POST',
                  dataType: 'json',
                  
                  data: {
                      _token:   $('meta[name="_token"]').attr('content'),
                      id_user :id_user,                
                  }
              }).done(function(data) {
                $('.chat-box2').empty();
                $(".loading-message").hide();
                    $('#notice'+(id_user)+'').css('display','none');
                    talking_id = id_user;
                    id_one_to_one = data.id_one_one;
                    node_id2 = data.node_id2.id;
                $.each(data.details,function(i,value){
                  if(value.posted_by == auth_id)
                  {
                    $('.chat-box2').prepend("<div class='media w-60 ml-auto mb-3'><div class='media-body'><div id='mesx"+(value.id)+"' class='bg-primary rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-white'>"+(value.detail)+"</p></div><p class='small text-muted'>from: "+(value.staff_name)+" at:"+(value.created_at)+"</p></div></div>");            
                      if(value.img_source != 'null')
                      {
                         $('#mesx'+value.id).prepend("<a target='_blank' href='/"+value.img_source+"'> <img src='/"+(value.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                      }
                  }
                  else
                  {
                    $('.chat-box2').prepend("<div class='media w-60 mb-3'><img src='/"+(value.staff_photo)+"' alt='user' width='30' class='rounded-circle'><div class='media-body ml-3'><div id='mesx"+(value.id)+"' class='bg-light rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-muted'>"+(value.detail)+"</p></div><p class='small text-muted'>from: "+(value.staff_name)+" at:"+(value.created_at)+"</p></div></div>");               
                      if(value.img_source != 'null')
                      {
                          $('#mesx'+value.id).prepend("<a target='_blank' href='/"+value.img_source+"'><img  src='/"+(value.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                      }

                  }
                  
                });
                $(".chat-box2").animate({scrollTop: $('.chat-box2').prop("scrollHeight")}, 1000);
              });
            });
          var channel4 = pusher.subscribe('message-received2');
                  channel4.bind('Modules\\ChatBox\\Events\\NewMessage2',function (data){
                    
                    
                       if(data.message.ids == auth_id && data.message.conv_id == id_one_to_one)
                       {     
                               
                            $('.chat-box2').append("<div class='media w-60 mb-3'><img src='/"+(data.message.staff_photo)+"' alt='user' width='30' class='rounded-circle'><div class='media-body ml-3'><div id='mesx"+(data.message.mes_id)+"' class='bg-light rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-muted'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+"</p></div></div>");                                
                            if(data.message.img_source != 'null')
                                {
                                   $('#mesx'+data.message.mes_id).prepend("<a target='_blank' href='/"+(data.message.img_source)+"'><img src='/"+(data.message.img_source)+"' width='100%' height='100%' style='display:block;'> </a>");
                                }

                           
                       }
                       else
                       {
                        if(data.message.ids != auth_id && data.message.conv_id == id_one_to_one)
                        {
                           
                          $('.chat-box2').append("<div class='media w-60 ml-auto mb-3'><div class='media-body'><div id='mesx"+(data.message.mes_id)+"' class='bg-primary rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-white'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+"</p></div></div>");
                            if(data.message.img_source != 'null')
                                {
                                   $('#mesx'+data.message.mes_id).prepend("<a target='_blank' href='/"+(data.message.img_source)+"' ><img src='/"+(data.message.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                                }
                            
                          
                         
                        }
                        
                          
                       }
                        
                       if(data.message.conv_id != id_one_to_one && data.message.ids == auth_id)
                       {
                          
                         $('#notice'+(data.message.id_sender)+'').css('display','block');
                       }
                       
                 $(".chat-box2").animate({scrollTop: $('.chat-box2').prop("scrollHeight")}, 1000);

                   
                 });    


  function sendMessage3 ()
  {
        loading =1;
        var receive_user = talking_id;
        var message = $('#message2').val();
        if($.trim(message) == '') {
          return false;
        }

        $.ajax({
                  url: '/chatbox/submitOneOne',
                  type: 'POST',
                  dataType: 'html',
                  
                  data: {
                      _token:   $('meta[name="_token"]').attr('content'),
                      id_one_one : id_one_to_one,
                      message   : message, 
                      receive_user : receive_user,                
                  }
              }).done(function(data) {
                loading=0;
                $('#message2').val(""); 
                $('#img-preview').css('display','none');               
                $('#img-preview').attr('src',"");
                    
              });
  }

    $('#group_create').on('click',function(e){
        e.preventDefault();
        
            var user_ids = $('#selectSections').val();
            var conv_name = $('#group_name').val();
            var subject = $('#group_subject').val();

            $.ajax({
                type: "POST",  
                url: "/chatbox/create-group",
                dataType: "html",
                data: {
                  _token:   $('meta[name="_token"]').attr('content'),
                  conv_name : conv_name,
                  subject : subject,
                  user_ids : user_ids
                },success:function(data){
                  $('.close').click();
                  refresh();
                }                  
            });       
    });

    $('#message_create').on('click',function(e){
        e.preventDefault();        
            var user_ids = $('.messageUsers').val();
            var conv_name = $('#message_name').val();
            var subject = $('#message_subject').val();
            var detail = $('#message_detail').val();
            $.ajax({
                type: "POST",  
                url: "/chatbox/create-multiple",
                dataType: "html",
                data: {
                  _token:   $('meta[name="_token"]').attr('content'),
                  conv_name : conv_name,
                  subject : subject,
                  user_ids : user_ids,
                  detail : detail
                },success:function(data){
                  $('.close').click();
                  refresh();
                }                  
            });       
    });

    $(document).on('click', '.leave', function (e) {      
      e.preventDefault();
      var c = confirm('Are you sure??');
      if (c == true) {
        $('.pre-loading').show();
        var id_message = $(this).attr('id').replace('leave','');

        
        $.ajax({
          type: "GET",  
              url: "/chatbox/leave/"+(id_message)+"/"+(auth_id)+"",
              dataType: "html",
              
              success:function()
              {
                alert('success');
                $('#group'+(id_message)+'').remove();
                $('.chat-box2').empty();
                talking_id = -1;
                id_one_to_one = -1;
              }
        });
      }else{return false;}
    });

    
    $(document).on('click', '.delete', function (e) {
      e.preventDefault();
      var c = confirm('Are you sure??');
      if (c == true) {
        $('.pre-loading').show();     
        var conver_id = $(this).attr('id').replace('delete','');
        $.ajax({
          type: "GET",  
              url: "/chatbox/delete/"+(conver_id)+"",
              dataType: "html",
              
              success:function()
              {
                alert('success');
                $('#group'+(conver_id)+'').remove();
                $('.chat-box2').empty();
                talking_id = -1;
                id_one_to_one = -1;
              }
        });
      }else{return false;}
    });

    $('#refresh').on('click', function(){     
      $(this).prop('disabled',true);
        
        refresh();
    });

    function refresh()
    {     
      $('.pre-loading').show();
        $.ajax({               
                type: "GET",  
                url: "/chatbox/refresh_chat_list",
                dataType: "json",
                data: {}, 
                
                success: function(data){ 
                    $('.messages-box').empty();
  
                    $.each(data.data,function(i,value){

                        if (value.created_by == auth_id) {
                          if(value.status == 1)
                          {
                            $('.messages-box').append('<div id="conv'+(value.id)+'" class="list-group-item list-group-item-action  text-white rounded-0"><div class="row"><div class="col-10"><a href="/chatbox/Msg-detail/'+(value.id)+'" id="'+(value.id)+'" class="MesDetails"><div class="media"><img src="/public/sg_star_logo.png" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><p style="font-size: 18px; color: black;" class="font-weight-bold mb-0 text-big">'+(value.name)+'</p></div><p class="font-italic mb-0 text-small">'+(value.subject)+' </p></div></div></a></div><div class="col-1"><i id="HaveMessage'+(value.id)+'" class="material-icons settings" style="font-size:30px;color:gray;display: none;">add_alert</i></div><div class="col-1"><div class="dropdown"><button type="button" class="fa fa-ellipsis-v button1" data-toggle="dropdown"></button><div class="dropdown-menu"><a class="dropdown-item" href="/chatbox/settings/'+(value.id)+'">Settings</a><a id="'+(value.id)+'" class="dropdown-item delete" href="/chatbox/delete/'+(value.id)+'" onclick="return confirm("Are you absolutely sure you want to delete?")">Delete</a><a data-toggle="collapse" role="button" class="dropdown-item" href="#seeparticipants'+(value.id)+'">Participants</a></div></div></div></div><br> <div class="collapse" id="seeparticipants'+(value.id)+'"><p id="participants'+(value.id)+'"></p></div></div>');
                          } 
                          else
                          {
                            $('.messages-box').append('<div id="conv'+(value.id)+'" class="list-group-item list-group-item-action  text-white rounded-0"><div class="row"><div class="col-10"><a href="/chatbox/Msg-detail/'+(value.id)+'" id="'+(value.id)+'" class="MesDetails"><div class="media"><img src="/public/sg_star_logo.png" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><p style="font-size: 18px; color: black;" class="font-weight-bold mb-0 text-big">'+(value.name)+'</p></div><p class="font-italic mb-0 text-small">'+(value.subject)+' </p></div></div></a></div><div class="col-1"><i id="HaveMessage'+(value.id)+'" class="material-icons settings" style="font-size:30px;color:gray;display: block;">add_alert</i></div><div class="col-1"><div class="dropdown"><button type="button" class="fa fa-ellipsis-v button1" data-toggle="dropdown"></button><div class="dropdown-menu"><a class="dropdown-item" href="/chatbox/settings/'+(value.id)+'">Settings</a><a id="'+(value.id)+'" class="dropdown-item delete" href="/chatbox/delete/'+(value.id)+'" onclick="return confirm("Are you absolutely sure you want to delete?")">Delete</a><a data-toggle="collapse" role="button" class="dropdown-item" href="#seeparticipants'+(value.id)+'">Participants</a></div></div></div></div><br> <div class="collapse" id="seeparticipants'+(value.id)+'"><p id="participants'+(value.id)+'"></p></div></div>');
                          }
                        }
                        
                        else
                        {
                          if(value.status ==1)
                          {
                           $('.messages-box').append('<div id="conv'+(value.id)+'" class="list-group-item list-group-item-action  text-white rounded-0"><div class="row"><div class="col-10"><a href="/chatbox/Msg-detail/'+(value.id)+'" id="'+(value.id)+'" class="MesDetails"><div class="media"><img src="/public/sg_star_logo.png" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><p style="font-size: 18px; color: black;" class="font-weight-bold mb-0 text-big">'+(value.name)+'</p></div><p class="font-italic mb-0 text-small">'+(value.subject)+' </p></div></div></a></div><div class="col-1"><i id="HaveMessage'+(value.id)+'" class="material-icons settings" style="font-size:30px;color:gray;display: none;">add_alert</i></div><div class="col-1"><div class="dropdown"><button type="button" class="fa fa-ellipsis-v button1" data-toggle="dropdown"></button><div class="dropdown-menu"><a class="dropdown-item leave" id="'+(value.id)+'" href="/chatbox/leave/'+(value.id)+'/'+(auth_id)+'" onclick="return confirm("Are you absolutely sure you want to leave?")">Leave</a><a data-toggle="collapse" role="button" class="dropdown-item" href="#seeparticipants'+(value.id)+'">Participants</a></div></div></div></div><br> <div class="collapse" id="seeparticipants'+(value.id)+'"><p id="participants'+(value.id)+'"></p></div></div>');
                          }
                          else{
                            $('.messages-box').append('<div id="conv'+(value.id)+'" class="list-group-item list-group-item-action  text-white rounded-0"><div class="row"><div class="col-10"><a href="/chatbox/Msg-detail/'+(value.id)+'" id="'+(value.id)+'" class="MesDetails"><div class="media"><img src="/public/sg_star_logo.png" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><p style="font-size: 18px; color: black;" class="font-weight-bold mb-0 text-big">'+(value.name)+'</p></div><p class="font-italic mb-0 text-small">'+(value.subject)+' </p></div></div></a></div><div class="col-1"><i id="HaveMessage'+(value.id)+'" class="material-icons settings" style="font-size:30px;color:gray;display: block;">add_alert</i></div><div class="col-1"><div class="dropdown"><button type="button" class="fa fa-ellipsis-v button1" data-toggle="dropdown"></button><div class="dropdown-menu"><a class="dropdown-item leave" id="'+(value.id)+'" href="/chatbox/leave/'+(value.id)+'/'+(auth_id)+'" onclick="return confirm("Are you absolutely sure you want to leave?")">Leave</a><a data-toggle="collapse" role="button" class="dropdown-item" href="#seeparticipants'+(value.id)+'">Participants</a></div></div></div></div> <br><div class="collapse" id="seeparticipants'+(value.id)+'"><p id="participants'+(value.id)+'"></p></div></div>');
                          }
                        }
                        $.each(value.participants,function(i,part){
                                $('#participants'+value.id).append(part);
                        });

                        if (value.id == conv_id) {
                            $('#conv'+value.id).attr('class','list-group-item list-group-item-action active1 text-white rounded-0');
                        }
                    });                   
                  
                  $(".pre-loading").fadeOut("slow");
                  $('#refresh').prop('disabled',false);

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                    $('#refresh').prop('disabled',false);
                }       
            });
    }   
    $('#button-addon1').bind('click',function(){
      if(conv_id == 0) {
          return false;
        }
          $('#upload').click();               
      }); 
      $('#upload').on('change',function(){ 
           var img_path = $('#upload')[0].files[0];
           if(img_path == null){return false;}
           var form_data = new FormData();
           var _token=  $('meta[name="_token"]').attr('content');
           form_data.append('file',img_path);
           form_data.append('token',_token);
           form_data.append('conv_id',conv_id);
           $.ajax({
                type: "POST",  
                url: "/chatbox/image_upload",
                dataType: "html",
                data: form_data,
                processData: false,
                contentType: false,
                success:function(data){
                  // $('#img-preview').css('display','block');               
                  // $('#img-preview').attr('src',"/"+data);
                  // img_source = data;
                }                  
            });       

          
      });


      $('#button-addon4').on('click',function(){
        if(message_id == -1) {
          if(id_one_to_one== -1)
          {return false;}        
        }
        $('#chatbox2-image').click();
    });

      $('#chatbox2-image').on('change',function(){ 
           var receive_user = talking_id;
           var image_path = $('#chatbox2-image')[0].files[0];
           if(image_path == null){return false;}
           var form_data = new FormData();
           var _token=  $('meta[name="_token"]').attr('content');
           form_data.append('file',image_path);
           form_data.append('token',_token);
           form_data.append('receive_user',receive_user);
           

           if(message_type == 1)        
            {
              form_data.append('message_id',message_id);
              $.ajax({
                type: "POST",  
                url: "/chatbox/post_image",
                dataType: "json",
                data: form_data,
                processData: false,
                contentType: false,
                success:function(data){
                    
                }                  
              });       
            }
            else
            {
              form_data.append('id_one_to_one',id_one_to_one);
                $.ajax({
                type: "POST",  
                url: "/chatbox/post_image2",
                dataType: "json",
                data: form_data,
                processData: false,
                contentType: false,
                success:function(data){
                      
                }                  
              });       
            }  
           

          
      });
  // Khoi tao mot bien pusher de listen 
     
  //Gan channel cho bien pusher moi tao
    var privateChannel = pusher.subscribe('private-room.'+conv_id);//private channel phai co "private-"dung truoc ten channel
          privateChannel.bind('Modules\\ChatBox\\Events\\NewMessage', function (data) {
          
          //Bat su kien listen thanh cong
          $(".chat-box").animate({scrollTop: $('.chat-box').prop("scrollHeight")}, 1000); 
           
          //neu id cua tai khoan dang login == id cua nguoi post message
          if(data.message.posted_by == auth_id)
          {               
                
                if(data.message.img_source != "null")
                {
                  //$('.chat-box2').append("<div class='media w-50 ml-auto mb-3'><div class='media-body'><div class='bg-primary rounded py-2 px-3 mb-2'><img class='img_source' src=/"+(data.message.img_source)+" height='100%' width='100%' style='display:none;'><p class='text-small mb-0 text-white'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+" at: "+(post_time)+" </p></div></div>");                  
                  $('.chat-box').append("<div class='media w-50 ml-auto mb-3'><div class='media-body'><div class='bg-primary rounded py-2 px-3 mb-2'><img class='img_source' src=/"+(data.message.img_source)+" height='100%' width='100%' style='display:none;'><p class='text-small mb-0 text-white'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+" at: "+(post_time)+" </p></div></div>");
                  $('.loader').css('display','none'); 
                  $('#button-addon2').show();
                  $('.img_source').css('display','block');
                  img_source = "null";
                }
                else{
                  //$('.chat-box2').append("<div class='media w-50 ml-auto mb-3'><div class='media-body'><div class='bg-primary rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-white'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+" at: "+(post_time)+" </p></div></div>");
                  
                  $('.chat-box').append("<div class='media w-50 ml-auto mb-3'><div class='media-body'><div class='bg-primary rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-white'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+" at: "+(post_time)+" </p></div></div>");
                  $('.loader').css('display','none'); 
                  $('#button-addon2').show();
                }
                
          }
          //neu id cua tai khoan dang login != id cua nguoi post message
          else
          {
                
                if(data.message.img_source != "null")
                {
                  //$('.chat-box2').append("<div class='media w-50 mb-3'><img src='/"+(data.message.staff_photo)+"' alt='user' width='50' class='rounded-circle'><div class='media-body ml-3'><div class='bg-light rounded py-2 px-3 mb-2'><img class='img_source' src=/"+(data.message.img_source)+" height='100%' width='100%' style='display:none;'><p class='text-small mb-0 text-muted'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+" at: "+(post_time)+"</p></div></div>");                  
                  $('.chat-box').append("<div class='media w-50 mb-3'><img src='/"+(data.message.staff_photo)+"' alt='user' width='50' class='rounded-circle'><div class='media-body ml-3'><div class='bg-light rounded py-2 px-3 mb-2'><img class='img_source' src=/"+(data.message.img_source)+" height='100%' width='100%' style='display:none;'><p class='text-small mb-0 text-muted'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+" at: "+(post_time)+"</p></div></div>");
                  $('.loader').css('display','none'); 
                  $('#button-addon2').show();
                  count++; 
                  $('#messagecounter').text(count);
                  $('.img_source').css('display','block');
                  img_source = "null";
                }
                else
                {
                  //$('.chat-box2').append("<div class='media w-50 mb-3'><img src='/"+(data.message.staff_photo)+"' alt='user' width='50' class='rounded-circle'><div class='media-body ml-3'><div class='bg-light rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-muted'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+" at: "+(post_time)+"</p></div></div>");               
                  $('.chat-box').append("<div class='media w-50 mb-3'><img src='/"+(data.message.staff_photo)+"' alt='user' width='50' class='rounded-circle'><div class='media-body ml-3'><div class='bg-light rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-muted'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+" at: "+(post_time)+"</p></div></div>");
                  $('.loader').css('display','none'); 
                  $('#button-addon2').show();
                  count++; 
                  $('#messagecounter').text(count);
                }
                  
          }    
          });
                

    var channel2 = pusher.subscribe('create-conversation');
        channel2.bind('Modules\\ChatBox\\Events\\CreateConversation',function(data){  
            
            
              var conver_id = data.conv_id;              
              var stop =0;                                                         
              $.each(data.id,function(i,value){
                if(data.id_from == auth_id)
                  {
                    $('#list-group-chat').prepend('<a href="/chatbox/Msg-detail2/'+(conver_id)+'" id="'+(conver_id)+'" class="MesDetails2 list-group-item list-group-item-action text-white rounded-0"><div class="media"><img src="/public/sg_star_logo.png" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><h6 class="mb-0">'+(data.conv_name)+'</h6></div><p class="font-italic mb-0 text-small">'+(data.conv_subject)+'</p></div></div></a>');                   
                  }
                  if(value == auth_id)
                  {
                    $('#group'+conver_id).remove();
                    
                    $('.messages-box').prepend('<div id="conv'+(conver_id)+'" class="list-group-item list-group-item-action  text-white rounded-0"><div class="row"><div class="col-10"> <a href="/chatbox/Msg-detail/'+(conver_id)+'" id="'+(value.id)+'" class="MesDetails"><div class="media"><img src="/public/sg_star_logo.png" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><p style="font-size: 18px; color: black;" class="font-weight-bold mb-0 text-big">'+(data.conv_name)+'</p></div><p class="font-italic mb-0 text-small">'+(data.conv_subject)+' </p></div></div></a></div><div class="col-1"><i id="HaveMessage'+(conver_id)+'" class="material-icons settings" style="font-size:30px;color:gray;display: block;">add_alert</i></div><div class="col-1"><div class="dropdown"><button type="button" class="fa fa-ellipsis-v button1" data-toggle="dropdown"></button><div class="dropdown-menu"><a id="'+(conver_id)+'" class="dropdown-item leave" href="/chatbox/leave/'+(conver_id)+'/'+(auth_id)+'" onclick="return confirm("Are you absolutely sure you want to leave?")">Leave</a><a data-toggle="collapse" role="button" class="dropdown-item" href="#seeparticipants'+(conver_id)+'">Participants</a></div></div></div></div><br><div class="collapse" id="seeparticipants'+(conver_id)+'"><p id="participants'+(conver_id)+'"></p></div></div>');
                    $('#list-group-chat').prepend('<a href="/chatbox/Msg-detail2/'+(conver_id)+'" id="group'+(conver_id)+'" class="MesDetails2 list-group-item list-group-item-action text-white rounded-0"><div class="media" id="msg'+(conver_id)+'"><img src="/public/sg_star_logo.png" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><h6 class="mb-0">'+(data.conv_name)+'</h6></div><p class="font-italic mb-0 text-small">'+(data.conv_subject)+'</p></div><span id="material-icons'+(conver_id)+'" class="material-icons" style="color:black;">notification_important</span></div></a>');
                    stop =1;
                    
                    $.each(data.name,function(i,part){
                          $('#participants'+conver_id).append(part);
                          
                        });  
                  }
                   
                  if (stop ==1) { stop =0 ; return false;}
              });
            
        });

  $(document).on('click', '.MesDetails2', function (e) {
          $(".loading-message").show();
          $('.MesDetails2').css('background-color','');
          $(this).css('background-color','#d9d7d7;');      
          e.preventDefault();        
          var msg_id = $(this).attr('id').replace('group','');
          var auth_id = $('#auth_id').val();
          message_id = msg_id;
          $.ajax({
            type: 'GET',
            url: '/chatbox/Msg-detail2/'+msg_id,
            dataType: 'json',
            cache: false,
            success: function(data){ 
              node_id = data.node_id;
              $('#dropdown-users').empty();
              $('#dropdown-options').empty();          
              $('.chat-box2').empty();
              $('#material-icons'+msg_id).remove();
              // $('#current_con_id').val(data.id);
              $.each(data.details,function(i,value){

                if(value.posted_by == auth_id)
                {
                      $('.chat-box2').prepend("<div class='media w-60 ml-auto mb-3'><div class='media-body'><div id='mes"+(value.id)+"' class='bg-primary rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-white'>"+(value.detail)+"</p></div><p class='small text-muted'>from: "+(value.user_create)+" at: "+(value.created_at)+" </p></div></div>");            
                      if(value.img_source != 'null')
                      {
                         $('#mes'+value.id).prepend("<a target='_blank' href='/"+value.img_source+"'> <img src='/"+(value.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                      }
                }
                else
                {
                      $('.chat-box2').prepend("<div class='media w-60 mb-3'><img src='/"+(value.image)+"' alt='user' width='30' class='rounded-circle'><div class='media-body ml-3'><div id='mes"+(value.id)+"' class='bg-light rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-muted'>"+(value.detail)+"</p></div><p class='small text-muted'>from: "+(value.user_create)+" at: "+(value.created_at)+"</p></div></div>");               
                      if(value.img_source != 'null')
                      {
                          $('#mes'+value.id).prepend("<a target='_blank' href='/"+value.img_source+"'><img  src='/"+(value.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                      }
                }
              });  

              

              if(data.created_by == data.auth)
              {
                $('#dropdown-options').append('<a href="#" id="addGroup'+(data.id)+'" class="dropdown-item add-user" data-toggle="modal" type="button" data-target="#newGroup" >Add user</a>');
                $('#dropdown-options').append('<a href="#" id="delete'+(data.id)+'" class="dropdown-item delete">Delete</a>');
                $('#dropdown-options').append('<a href="/chatbox/settings/'+(data.id)+'" class="dropdown-item ">Settings</a>');

                $.each(data.users,function(i,value){
               
                 $('#dropdown-users').append('<span class="dropdown-item"><a href="#" id="delfromgroup'+(value.id)+'" class="fa fa-close delete-from-group"></a>     '+(value.full_name)+'</span>');
                });
              }
              else
              {
                $('#dropdown-options').append('<a href="#" id="leave'+(data.id)+'" class="dropdown-item leave">Leave</a>');

                $.each(data.users,function(i,value){
               
                 $('#dropdown-users').append('<span class="dropdown-item">     '+(value.full_name)+'</span>');
                });
              }

              $(".chat-box2").animate({scrollTop: $('.chat-box2').prop("scrollHeight")}, 1000);  
              $(".loading-message").hide();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                      alert("Status: " + textStatus); alert("Error: " + errorThrown);                     
                  }  
          });
           
    });

  $('.chat-box2').scroll(function() {
    var pos = $('.chat-box2').scrollTop();
    if (pos == 0) 
    {
        if(message_type == 1)
        {
          var group_id = message_id;
                $.ajax({
                    type: 'GET',
                    url: "/chatbox/load_more/"+(group_id)+"/"+(node_id)+"",
                    dataType: 'json',
                    cache: false,
                    success: function(data){ 
                      $.each(data.details,function(i,value){
                        node_id = data.node_id.id;
                        if(value.posted_by == auth_id)
                        {
                              $('.chat-box2').prepend("<div class='media w-60 ml-auto mb-3'><div class='media-body'><div id='mes"+(value.id)+"' class='bg-primary rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-white'>"+(value.detail)+"</p></div><p class='small text-muted'>from: "+(value.user_create)+" at: "+(value.created_at)+" </p></div></div>");            
                              if(value.img_source != 'null')
                              {
                                 $('#mes'+value.id).prepend("<a target='_blank' href='/"+value.img_source+"'> <img src='/"+(value.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                              }
                        }
                        else
                        {
                              $('.chat-box2').prepend("<div class='media w-60 mb-3'><img src='/"+(value.image)+"' alt='user' width='30' class='rounded-circle'><div class='media-body ml-3'><div id='mes"+(value.id)+"' class='bg-light rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-muted'>"+(value.detail)+"</p></div><p class='small text-muted'>from: "+(value.user_create)+" at: "+(value.created_at)+"</p></div></div>");               
                              if(value.img_source != 'null')
                              {
                                  $('#mes'+value.id).prepend("<a target='_blank' href='/"+value.img_source+"'><img  src='/"+(value.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                              }
                        }
                      });
                    }
                });
        }
        else
        {
          var talk_id = id_one_to_one;
                $.ajax({
                    type: 'GET',
                    url: "/chatbox/load_more2/"+(talk_id)+"/"+(node_id2)+"",
                    dataType: 'json',
                    cache: false,
                    success: function(data){ 
                      $.each(data.details,function(i,value){
                        node_id2 = data.node_id2.id;
                        if(value.posted_by == auth_id)
                        {
                              $('.chat-box2').prepend("<div class='media w-60 ml-auto mb-3'><div class='media-body'><div id='mes"+(value.id)+"' class='bg-primary rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-white'>"+(value.detail)+"</p></div><p class='small text-muted'>from: "+(value.user_create)+" at: "+(value.created_at)+" </p></div></div>");            
                              if(value.img_source != 'null')
                              {
                                 $('#mes'+value.id).prepend("<a target='_blank' href='/"+value.img_source+"'> <img src='/"+(value.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                              }
                        }
                        else
                        {
                              $('.chat-box2').prepend("<div class='media w-60 mb-3'><img src='/"+(value.image)+"' alt='user' width='30' class='rounded-circle'><div class='media-body ml-3'><div id='mes"+(value.id)+"' class='bg-light rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-muted'>"+(value.detail)+"</p></div><p class='small text-muted'>from: "+(value.user_create)+" at: "+(value.created_at)+"</p></div></div>");               
                              if(value.img_source != 'null')
                              {
                                  $('#mes'+value.id).prepend("<a target='_blank' href='/"+value.img_source+"'><img  src='/"+(value.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                              }
                        }
                      });
                    }
                });
        }
    }
});
    $(document).on('click', '.delete-from-group', function (e) {
      e.preventDefault();
      var c = confirm('Are you sure??');
      if (c == true) {
        var user_id = $(this).attr('id').replace('delfromgroup','');
        var group_id = message_id;
        $.ajax({
            type: "GET",  
                url: "/chatbox/kick/"+(group_id)+"/"+(user_id)+"",
                dataType: "html",
                
                success:function()
                {
                  alert('success');
                  $('#delfromgroup'+(user_id)+'').parent().remove();
                  
                }
          });
      }
      else{return false;}
    });


    $(document).on('click', '.add-user', function (e) {
      e.preventDefault();
        var group_id = $(this).attr('id').replace('addGroup','');
        $.ajax({
            type: 'GET',
            url: '/chatbox/add-to-group/'+group_id,
            dataType: 'json',
            cache: false,
            success: function(data){ 
              $('#selectSections').empty();
               $.each(data,function(i,value){
                  $('#selectSections').append('<option  value="'+(value.id)+'">'+(value.full_name)+'</option>');
               });
            }

        });
    });

    $(document).on('click', '#add_to_group', function (e) {
        e.preventDefault();
        var user_ids = $('#selectSections').val();
        $.ajax({
            type: 'POST',
            url: '/chatbox/add_to_group',
            dataType: 'json',
            
            data : {
                _token  : $('meta[name="_token"]').attr('content'),
                group_id : message_id,
                user_ids : user_ids
            },
            success: function(data){ 
                $.each(data.names,function(i,value){
                 $('#dropdown-users').append('<span class="dropdown-item"><a href="#" id="delfromgroup'+(value.id)+'" class="fa fa-close delete-from-group"></a>     '+(value.full_name)+'</span>');
                });
                $('.select2-search-choice').remove();
                $('#selectSections').empty();
                $('.close').click();
            }

        });

    });
    var channel6 = pusher.subscribe('add-to-group');
        channel6.bind('Modules\\ChatBox\\Events\\AddToGroup',function (data){
          $.each(data.ids,function(i,value){
             var stop11 = 0;
             if(value == auth_id)
             {
              $('#list-group-chat').prepend('<a href="/chatbox/Msg-detail2/'+(data.group_id)+'" id="group'+(data.group_id)+'" class="MesDetails2 list-group-item list-group-item-action text-white rounded-0"><div class="media" id="msg'+(data.group_id)+'"><img src="/public/sg_star_logo.png" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><h6 class="mb-0">'+(data.name)+'</h6></div><p class="font-italic mb-0 text-small">'+(data.subject)+'</p></div><span id="material-icons'+(data.group_id)+'" class="material-icons" style="color:black;">notification_important</span></div></a>');                   
              stop11 =1;
             }
             if (stop11 ==1 ) { stop11=0; return false;}
          });
        });
    var channel7 = pusher.subscribe('delete-from-group');
        channel7.bind('Modules\\ChatBox\\Events\\DeleteFromGroup',function (data){
          
             if(data.user_id == auth_id)
             {
              $('#group'+(data.group_id)+'').remove();
              
             }
          
        });
    var channel8 = pusher.subscribe('delete-group');
        channel8.bind('Modules\\ChatBox\\Events\\DeleteGroup',function (data){
              $('#group'+(data.conv_id)+'').remove();
        });

    var channel9 = pusher.subscribe('message-alert');
        channel9.bind('Modules\\ChatBox\\Events\\MessageAlert',function (data){
             if(data.message.receiver == auth_id  )
             {
              $('#message-icon').attr('src','Modules/ChatBox/public/message_alert.png');
              if(data.message.conv_id != id_one_to_one)
              {
                $('#'+(data.message.sender)+'').remove();
                $('#list-message').prepend('<a href="#" class=" MesDetails3 list-group-item list-group-item-action text-white rounded-0 message_1-1" id="'+(data.message.sender)+'"><div id="oneone'+(data.message.sender)+'" class="media"><img src="'+(data.message.photo)+'" alt="user" width="50" class="rounded-circle"><div class="media-body ml-4"><div class="d-flex align-items-center justify-content-between mb-1"><h6 class="mb-0 text-big2">'+(data.message.sender_name)+'</h6><small class="small font-weight-bold"></small></div><p class="font-italic mb-0 text-small2" style="color: black;"></p></div><span id="notice'+(data.message.sender)+'" class="fa fa-envelope-o" style="color:red;display: block;"></span></div></a>');
              
              }
              
            }
            
        });
    var channel = pusher.subscribe('message-received');
        channel.bind('Modules\\ChatBox\\Events\\NewMessage',function (data){
          var posted_by = data.message.posted_by;
          $.each(data.message.ids,function(i,value){
            if(auth_id == value && conv_id != data.message.conv_id)
              {
                var id = data.message.conv_id;
                $('#HaveMessage'+id).show();
              }
            

            if(auth_id!=posted_by && message_id != data.message.conv_id)
            {
              
              $('#material-icons'+data.message.conv_id).remove();
              $('#msg'+data.message.conv_id).append('<span id="material-icons'+(data.message.conv_id)+'" class="material-icons" style="color:black;">notification_important</span>')
                    
            }
            if(auth_id == value && auth_id != posted_by && message_id == data.message.conv_id)
                {
                  
                  $('.chat-box2').append("<div class='media w-60 mb-3'><img src='/"+(data.message.staff_photo)+"' alt='user' width='30' class='rounded-circle'><div class='media-body ml-3'><div id='mess"+(data.message.mes_id)+"' class='bg-light rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-muted'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+"</p></div></div>");                                
                  if(data.message.img_source != 'null')
                      {
                         $('#mess'+data.message.mes_id).prepend("<a target='_blank' href='/"+(data.message.img_source)+"'><img src='/"+(data.message.img_source)+"' width='100%' height='100%' style='display:block;'> </a>");
                      }
                  }
              }); 
            if(auth_id == posted_by && message_id == data.message.conv_id)
                {
                  
                  $('.chat-box2').append("<div class='media w-60 ml-auto mb-3'><div class='media-body'><div id='mess"+(data.message.mes_id)+"' class='bg-primary rounded py-2 px-3 mb-2'><p class='text-small mb-0 text-white'>"+(data.message.message)+"</p></div><p class='small text-muted'>from: "+(data.message.name)+"p></div></div>");
                  if(data.message.img_source != 'null')
                      {
                         $('#mess'+data.message.mes_id).prepend("<a target='_blank' href='/"+(data.message.img_source)+"' ><img src='/"+(data.message.img_source)+"' width='100%' height='100%' style='display:block;'></a>");
                      }
                }
          
          $(".chat-box2").animate({scrollTop: $('.chat-box2').prop("scrollHeight")}, 1000);
          
        });



  $('#message_box').click(function(e){
    e.preventDefault();
    $('.minibox').toggle();
    $('#message-icon').attr('src','/Modules/ChatBox/public/message.jpg');
  });

    $('#search_data').on('input', function(){ 
        var data = $(this).val().toLowerCase();  
        if(data == "")
        {
          $('.list-group-item').show();    
        }
        else{
          $('.list-group-item').hide();
          $('.text-big').each(function(){            
              var check = $(this).text();
              var n = check.includes(data);            
              if(n==true){
                $(this).closest(".list-group-item").show();
              }             
          });
          $('.text-small').each(function(){            
              var check2 = $(this).text();
              var m = check2.includes(data);            
              if(m==true){
                $(this).closest(".list-group-item").show();
              }             
          });
        }
      });

    $('#search_data2').on('input', function(){ 
        var data = $(this).val().toLowerCase();
        if(data == "")
        {
          $('.list-group-item').show();    
        }
        else{
          $('.list-group-item').hide();
          $('.text-big2').each(function(){            
              var check = $(this).text().toLowerCase();
              var n = check.includes(data);            
              if(n==true){
                $(this).closest(".list-group-item").show();
              }             
          });
          $('.text-small2').each(function(){            
              var check2 = $(this).text().toLowerCase();
              var m = check2.includes(data);            
              if(m==true){
                $(this).closest(".list-group-item").show();
              }             
          });
        }
      }); 
});