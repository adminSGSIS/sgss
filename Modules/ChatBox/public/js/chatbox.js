$( document ).ready(function(){ 
    cur_mes = $('.inbox_chat').children().eq(1).attr('id');
    cur_mes_id = cur_mes.replace("ac", "");
    auth_id = $('#auth_id').val();
    mes_type = 'direct';
    $(".msg_history").animate({scrollTop: $('.msg_history').prop("scrollHeight")}, 1000); 
    
    listen();
    listen2();
});

$('#ListImages').click(function(){
    if($('#ImgsList').css('display') == "block"){
        $('#ImgsList').css('display','none');
        $('#gr_img_f').css('display','block');
        $('#gr_img_s').css('display','none');
    }else{
        $('#ImgsList').css('display','block');
        $('#gr_img_f').css('display','none');
        $('#gr_img_s').css('display','block');
    }
});

$('#ListFiles').click(function(){
    if($('#FilesList').css('display') == "block"){
        $('#FilesList').css('display','none');
        $('#gr_file_f').css('display','block');
        $('#gr_file_s').css('display','none');
    }else{
        $('#FilesList').css('display','block');
        $('#gr_file_f').css('display','none');
        $('#gr_file_s').css('display','block');
    }
});

$('#ListNamesImages').click(function(){
    if($('#group_participants_name_sidebar').css('display') == "block"){
        $('#group_participants_name_sidebar').css('display','none');
        $('#gr_names_img_f').css('display','block');
        $('#gr_names_img_s').css('display','none');
    }else{
        $('#group_participants_name_sidebar').css('display','block');
        $('#gr_names_img_f').css('display','none');
        $('#gr_names_img_s').css('display','block');
    }
});

$('#select-group-roles').on('change',function(e){
    e.preventDefault();
    var id_role = $(this).val();
    $.ajax({
        url: '/getroles/'+(id_role)+'',
        type: 'GET',
        dataType: 'json',
        cache: 'true',
        data: {}
    }).done(function(data) {
        $.each(data.users,function(i,val){
            $('#group_add_selections').append('<option value="'+(val.id)+'">'+(val.full_name)+'</option>');
        });
    });
});

$('.chat_list').on('click', function(e){
    cur_mes = $(this).attr('id');
    cur_mes_id = cur_mes.replace("ac", "");
}); 

$('.gr_chat_list').on('click',function(){
    cur_mes = $(this).attr('id');
    cur_mes_id = cur_mes.replace("gr_ac", "");
});

$('.direct-chat').on('click', function(e){
    mes_type = 'direct';
    cur_mes = $('.chat_list').filter(".active_chat").attr('id');
    cur_mes_id = cur_mes.replace("ac", "");
    $(".msg_history").animate({scrollTop: 100000}, 1000); 
}); 
$('.group-chat').on('click', function(e){
    mes_type = 'group';
    cur_mes = $('.gr_chat_list').filter(".active_chat").attr('id');
    cur_mes_id = cur_mes.replace("gr_ac", "");
    $(".msg_history").animate({scrollTop: 100000}, 1000); 
}); 

$('.msg_send_btn').on('click',function(){
    sendMessage();
});

$('#images_send').on('change',function(e){
    e.preventDefault();
    var fileExtension = ['png','jpeg','jpg','gif','tiff','jfif'];
    if ($.inArray($('#images_send').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only .png,.jpeg,.jpg,.gif,.tiff format is allowed.");
        $('#images_send').val(''); // Clean field
        return false;
    }
    var dt = new Date();
    time = dt.getHours() +' '+" : "+' ' + dt.getMinutes();
    day = dt.getDate();
    month = dt.toLocaleString('default', { month: 'short' });
    year = dt.getFullYear();
   
    cur_mes_id = cur_mes_id;
    var form_data = new FormData();
    var _token=  $('meta[name="_token"]').attr('content');
    
    $.each($('#images_send')[0].files,function(i,value){
        form_data.append('images[]',value);
    });
    
    form_data.append('_token',_token);
    form_data.append('cur_mes_id',cur_mes_id);
    $('#notice'+cur_mes_id+'').text('Me : Image');
    $("#loadingMessage"+cur_mes_id).css("display", "block");
    $("#isLoading"+cur_mes_id).css("display", "block");
    $("#avtSeen"+cur_mes_id).css("display", "none");
    $("#loaded"+cur_mes_id).css("display", "none");
    $.ajax({
        type: "POST",  
        url: "/chat/image_upload",
        dataType: "json",
        data: form_data,
        processData: false,
        contentType: false,
        success:function(data){
            if(data.check == false){
                $('#userOnline' + cur_mes_id).css('display', 'none');
            }else{
                $('#userOnline' + cur_mes_id).css('display', 'block');
            }
            if(data.images.length > 1){
                $('#messDiv'+(data.cur_mes_id)+'').append('<div class="outgoing_msg"><div class="sent_msg"><p class="multiImages" id="detail'+(data.detail_id)+'"></p><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div>');
                $.each(data.images,function(i,value){
                    $('#detail'+(data.detail_id)+'').append('<a data-lightbox="'+ data.cur_mes_id + '" target="_blank" href="/Modules/ChatBox/public/images/'+(value)+'"><img width="'+ (93/data.images.length) + "%" + '" height="100px" src="/Modules/ChatBox/public/images/'+(value)+'"></img></a>');
                });
            }else{
                $('#messDiv'+(data.cur_mes_id)+'').append('<div class="outgoing_msg"><div class="sent_msg"><a class="oneImg" data-lightbox="'+ data.cur_mes_id + '" target="_blank" href="/Modules/ChatBox/public/images/'+data.images[0]+'"><img src="/Modules/ChatBox/public/images/'+data.images[0]+'"></img></a><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div>');
            }
            $(".msg_history").animate({scrollTop: 100000}, 1000); 
            $('#images_send').val('');
            $("#isLoading"+cur_mes_id).css("display", "none");
            $("#loaded"+cur_mes_id).css("display", "block");
        }                  
    });       
});


$('#gr_images_send').on('change',function(e){
    e.preventDefault();
    var fileExtension = ['png','jpeg','jpg','gif','tiff','jfif'];
    if ($.inArray($('#gr_images_send').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only .png,.jpeg,.jpg,.gif,.tiff format is allowed.");
        $('#gr_images_send').val(''); // Clean field
        return false;
    }
    var dt = new Date();
    time = dt.getHours() +' '+" : "+' ' + dt.getMinutes();
    day = dt.getDate();
    month = dt.toLocaleString('default', { month: 'short' });
    year = dt.getFullYear();
   
    cur_mes_id = cur_mes_id;
    var form_data = new FormData();
    var _token=  $('meta[name="_token"]').attr('content');
    
    $.each($('#gr_images_send')[0].files,function(i,value){
        form_data.append('images[]',value);
    });
    
    form_data.append('_token',_token);
    form_data.append('cur_mes_id',cur_mes_id);
    $('#gr_notice'+cur_mes_id+'').text('Me : Image');
    $.ajax({
        type: "POST",  
        url: "/chat/gr_image_upload",
        dataType: "json",
        data: form_data,
        processData: false,
        contentType: false,
        success:function(data){
            if(data.images.length > 1){
                $('#gr_messDiv'+(data.cur_mes_id)+'').append('<div class="outgoing_msg"><div class="sent_msg"><p class="multiImages" id="gr_detail'+(data.gr_detail_id)+'"></p><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div>');
                $.each(data.images,function(i,value){
                    $('#gr_detail'+(data.gr_detail_id)+'').append('<a target="_blank" href="/Modules/ChatBox/public/images/'+(value)+'"><img width="'+ (93/data.images.length) + "%" + '" height="100px" src="/Modules/ChatBox/public/images/'+(value)+'"></img></a>');
                });
            }else{
                $('#gr_messDiv'+(data.cur_mes_id)+'').append('<div class="outgoing_msg"><div class="sent_msg"><a class="oneImg" target="_blank" href="/Modules/ChatBox/public/images/'+data.images[0]+'"><img src="/Modules/ChatBox/public/images/'+data.images[0]+'"></img></a><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div>');
            }
            $(".msg_history").animate({scrollTop: 100000}, 1000); 
            $('#gr_images_send').val('');
        }                  
    });       
});

$('#files_send').on('change',function(e){
    e.preventDefault();
    var dt = new Date();
    time = dt.getHours() +' '+" : "+' ' + dt.getMinutes();
    day = dt.getDate();
    month = dt.toLocaleString('default', { month: 'short' });
    year = dt.getFullYear();
   
    cur_mes_id = cur_mes_id;
    var form_data = new FormData();
    var _token=  $('meta[name="_token"]').attr('content');

    $.each($('#files_send')[0].files,function(i,value){
        form_data.append('files_send[]',value);
    });
    form_data.append('_token',_token);
    form_data.append('cur_mes_id',cur_mes_id);
    $('#notice'+cur_mes_id+'').text('Me : File');
    $("#loadingMessage"+cur_mes_id).css("display", "block");
    $("#isLoading"+cur_mes_id).css("display", "block");
    $("#avtSeen"+cur_mes_id).css("display", "none");
    $("#loaded"+cur_mes_id).css("display", "none");
    $.ajax({
        type: "POST",  
        url: "/chat/file_upload",
        dataType: "json",
        data: form_data,
        processData: false,
        contentType: false,
        success:function(data){
            if(data.check == false){
                $('#userOnline' + cur_mes_id).css('display', 'none');
            }else{
                $('#userOnline' + cur_mes_id).css('display', 'block');
            }
            $.each(data.files,function(i,value){
                $('#messDiv'+(data.cur_mes_id)+'').append('<div class="outgoing_msg"><div class="sent_msg"><a href="/Modules/Chatbox/public/files/'+(value)+'" download> <img width="60px" src="https://icon-library.com/images/file-icon/file-icon-6.jpg">'+(value)+'</a><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div>');
            });
            $(".msg_history").animate({scrollTop: 100000}, 1000); 
            $('#files_send').val('');
            $("#isLoading"+cur_mes_id).css("display", "none");
            $("#loaded"+cur_mes_id).css("display", "block");
        }                  
    });       
});


$('#gr_files_send').on('change',function(e){
    e.preventDefault();
    var dt = new Date();
    time = dt.getHours() +' '+" : "+' ' + dt.getMinutes();
    day = dt.getDate();
    month = dt.toLocaleString('default', { month: 'short' });
    year = dt.getFullYear();
   
    cur_mes_id = cur_mes_id;
    var form_data = new FormData();
    var _token=  $('meta[name="_token"]').attr('content');

    $.each($('#gr_files_send')[0].files,function(i,value){
        form_data.append('gr_files_send[]',value);
    });
    form_data.append('_token',_token);
    form_data.append('cur_mes_id',cur_mes_id);
    $('#gr_notice'+cur_mes_id+'').text('Me : File');
    $.ajax({
        type: "POST",  
        url: "/chat/gr_file_upload",
        dataType: "json",
        data: form_data,
        processData: false,
        contentType: false,
        success:function(data){
            $.each(data.files,function(i,value){
                $('#gr_messDiv'+(data.cur_mes_id)+'').append('<div class="outgoing_msg"><div class="sent_msg"><a href="/Modules/Chatbox/public/files/'+(value)+'" download><img width="60px" src="https://icon-library.com/images/file-icon/file-icon-6.jpg">'+(value)+'<span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div>');
            });
            $(".msg_history").animate({scrollTop: 100000}, 1000); 
            $('#gr_files_send').val('');
        }                  
    });       
});

$('.recent').on('click',function(e){
    e.preventDefault();
    var id = $(this).attr('href').replace('#cons','');
    mesgsId = id;
    $('#notice'+(id)+'').css('color','gray');
    $.ajax({
        type: "GET",  
        url: "/chat/update_status/"+(id)+"",
        dataType: "html",
        success:function(data){}
    });
});

$(window).on('keydown', function(e){
    if (e.which == 13){     
      sendMessage();
      return false;
    }
}); 

function sendMessage(){
    mes_type = mes_type;
    cur_mes_id = cur_mes_id;
    cur_mes_detail = $('.emoji-wysiwyg-editor').text();
    if(cur_mes_detail=="") return false;
    
    var dt = new Date();
    time = dt.getHours() +' '+" : "+' ' + dt.getMinutes();
    day = dt.getDate();
    month = dt.toLocaleString('default', { month: 'short' });
    year = dt.getFullYear();
    $('.emoji-wysiwyg-editor').text("");
    $(".msg_history").animate({scrollTop: 100000}, 1000);
    if(mes_type == 'direct'){
        $("#loadingMessage"+cur_mes_id).css("display", "block");
        $("#isLoading"+cur_mes_id).css("display", "block");
        $("#avtSeen"+cur_mes_id).css("display", "none");
        $("#loaded"+cur_mes_id).css("display", "none");
        $('#notice'+cur_mes_id+'').text('Me : ' + cur_mes_detail);
        $('#notice'+(cur_mes_id)+'').css('color','gray');
        $('#messDiv'+(cur_mes_id)+'').append('<div class="outgoing_msg"><div class="sent_msg"><p>'+(cur_mes_detail)+'</p><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div>');
    }
    else{
        
        $("#gr_loadingMessage").css("display", "block");
        $("#gr_isLoading").css("display", "block");
        $('#gr_notice'+cur_mes_id+'').text('Me : ' + cur_mes_detail);
        $('#gr_messDiv'+(cur_mes_id)+'').append('<div class="outgoing_msg"><div class="sent_msg"><p>'+(cur_mes_detail)+'</p><span class="time_date">'+(time)+'    |    '+(day)+' '+(month)+', '+(year)+'</span></div></div>');
    }
    $.ajax({
        url: '/chat/submit',
        type: 'POST',
        dataType: 'html',
        cache: 'true',
        
        data: {
            _token:   $('meta[name="_token"]').attr('content'),
            cur_mes_id : cur_mes_id,
            cur_mes_detail   : cur_mes_detail, 
            mes_type : mes_type,
        }
    }).done(function(data) {
        if(mes_type == 'direct'){
            $("#isLoading"+cur_mes_id).css("display", "none");
            $("#loaded"+cur_mes_id).css("display", "block");
            if(data){
                $('#userOnline' + cur_mes_id).css('display', 'none');
            }else{
                $('#userOnline' + cur_mes_id).css('display', 'block');
            }
        }
        else{
            $("#gr_isLoading").css("display", "none");
            $("#gr_loaded").css("display", "block");
            setTimeout(function(){ 
                $("#gr_loadingMessage").css("display", "none");
                $("#gr_loaded").css("display", "none");
            }, 1000);
        }
    });
}  
function listen2(){
    var dt = new Date();
    time = dt.getHours() +' '+" : "+' ' + dt.getMinutes();
    day = dt.getDate();
    month = dt.toLocaleString('default', { month: 'short' });
    year = dt.getFullYear();

    var pusher = new Pusher('d269c80a507e99df75c3', {
        cluster: "ap1"
    });
    var channel = pusher.subscribe('message-received2');
    channel.bind('Modules\\ChatBox\\Events\\NewMessage2',function(data){ 
        if(data.message.id_sender != auth_id)
        {
            if(data.message.changeStatus){
                $("#loadingMessage"+data.message.mes_id).css("display", "block");
                $("#loaded"+data.message.mes_id).css("display", "none");
                $("#avtSeen"+data.message.mes_id).css("display", "block");
                $('#userOnline' + data.message.mes_id).css('display', 'block');
            }else{
                $("#loadingMessage"+data.message.mes_id).css("display", "none");
                $("#loaded"+data.message.mes_id).css("display", "none");
                $("#avtSeen"+data.message.mes_id).css("display", "none");
                $(".msg_history").animate({scrollTop: $('.msg_history').prop("scrollHeight")}, 1000);
                if(data.message.files_source.length > 0)
                {
                    $('#notice'+(data.message.mes_id)+'').text(''+(data.message.name)+' : File ');
                    $.each(data.message.files_source,function(i,value){
                      $('#messDiv'+(data.message.mes_id)+'').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img data-toggle="tooltip" title="" style="border-radius: 50%;" src="/'+(data.message.staff_photo)+'" alt="avatar" data-original-title="'+(data.message.name)+'"> </div><div class="received_msg"><div class="received_withd_msg"><a href="/Modules/ChatBox/public/files/'+(value)+'" download><img width="60px" src="https://icon-library.com/images/file-icon/file-icon-6.jpg">'+(value)+'</a><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div></div>') ;
                    });
                }
                else{
                    if(data.message.img_source.length > 0){
                        $('#notice'+(data.message.mes_id)+'').text(''+(data.message.name)+' : Image ');
                        if(data.message.img_source.length > 1){
                            $.each(data.message.img_source,function(i,value){
                              $('#detail'+(data.message.detail_id)+'').append('<a target="_blank" href="/Modules/ChatBox/public/images/'+(value)+'"><img width="' + (93/data.message.img_source.length) + "%" + '" height="100px" src="/Modules/ChatBox/public/images/'+(value)+'"></img></a>');
                            });
                        }
                        else{
                            $('#messDiv'+(data.message.mes_id)+'').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img data-toggle="tooltip" title="" style="border-radius: 50%;" src="/'+(data.message.staff_photo)+'" alt="avatar" data-original-title="'+(data.message.name)+'"> </div><div class="received_msg"><div class="received_withd_msg"><a class="oneImg" target="_blank" href="/Modules/ChatBox/public/images/'+data.message.img_source[0]+'"><img src="/Modules/ChatBox/public/images/'+data.message.img_source[0]+'"></img></a><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div></div>') ;
                        }
                    }
                    else{
                        $('#notice'+(data.message.mes_id)+'').text(''+(data.message.name)+' : '+(data.message.message)+'');
                        $('#messDiv'+(data.message.mes_id)+'').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img data-toggle="tooltip" title="" style="border-radius: 50%;" src="/'+(data.message.staff_photo)+'" alt="avatar" data-original-title="'+(data.message.name)+'"> </div><div class="received_msg"><div class="received_withd_msg"><p id="detail'+(data.message.detail_id)+'">'+(data.message.message)+'</p><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div></div>') ;
                    }
                }
                if(!($("#cons" + data.message.mes_id).hasClass('active'))){
                    $('#notice'+(data.message.mes_id)+'').css('color','blue');
                }else{
                    document.getElementById("ac" + data.message.mes_id).children[0].children[0].click();
                }
            }
        }
    });
}

function listen(){
    var dt = new Date();
    time = dt.getHours() +' '+" : "+' ' + dt.getMinutes();
    day = dt.getDate();
    month = dt.toLocaleString('default', { month: 'short' });
    year = dt.getFullYear();

    var pusher = new Pusher('d269c80a507e99df75c3', {
      cluster: "ap1"
    });
    var channel = pusher.subscribe('message-received');
    
    channel.bind('Modules\\ChatBox\\Events\\NewMessage',function(data){ 
        if(data.message.id_sender != auth_id){
            if(data.message.files_source.length > 0){
                $('#gr_notice'+(data.message.mes_id)+'').text(''+(data.message.name)+' : File ');
                $.each(data.message.files_source,function(i,value){
                  $('#gr_messDiv'+(data.message.mes_id)+'').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img data-toggle="tooltip" title="" style="border-radius: 50%;" src="/'+(data.message.staff_photo)+'" alt="avatar" data-original-title="'+(data.message.name)+'"> </div><div class="received_msg"><div class="received_withd_msg"><a href="/Modules/ChatBox/public/files/'+(value)+'" download><img width="60px" src="https://icon-library.com/images/file-icon/file-icon-6.jpg">'+(value)+'</a><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div></div>') ;
                });
                $(".msg_history").animate({scrollTop: 100000}, 1000); 
            }
            else{
                if(data.message.img_source.length > 0){
                    $('#gr_notice'+(data.message.mes_id)+'').text(''+(data.message.name)+' : Image ');
                    if(data.message.img_source.length > 1){
                        $('#gr_messDiv'+(data.message.mes_id)+'').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img data-toggle="tooltip" title="" style="border-radius: 50%;" src="/'+(data.message.staff_photo)+'" alt="avatar" data-original-title="'+(data.message.name)+'"> </div><div class="received_msg"><div class="received_withd_msg"><p class="multiImages" id="gr_detail'+(data.message.gr_detail_id)+'">'+(data.message.message)+'</p><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div></div>') ;
                        $.each(data.message.img_source,function(i,value){
                          $('#gr_detail'+(data.message.gr_detail_id)+'').append('<a target="_blank" href="/Modules/ChatBox/public/images/'+(value)+'"><img width="'+ (93/data.message.img_source.length) + "%" + '"height="100px" src="/Modules/ChatBox/public/images/'+(value)+'"></img></a>');
                        });
                    }else{
                        $('#gr_messDiv'+(data.message.mes_id)+'').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img data-toggle="tooltip" title="" style="border-radius: 50%;" src="/'+(data.message.staff_photo)+'" alt="avatar" data-original-title="'+(data.message.name)+'"> </div><div class="received_msg"><div class="received_withd_msg"><a class="oneImg" target="_blank" href="/Modules/ChatBox/public/images/'+data.message.img_source[0]+'"><img src="/Modules/ChatBox/public/images/'+data.message.img_source[0]+'"></img></a><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div></div>') ;
                    }
                }
                else{
                    $('#gr_notice'+(data.message.mes_id)+'').text(''+(data.message.name)+' : '+(data.message.message)+'');
                    $('#gr_messDiv'+(data.message.mes_id)+'').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img class="oneImg" data-toggle="tooltip" title="" style="border-radius: 50%;" src="/'+(data.message.staff_photo)+'" alt="avatar" data-original-title="'+(data.message.name)+'"> </div><div class="received_msg"><div class="received_withd_msg"><p id="detail'+(data.message.detail_id)+'">'+(data.message.message)+'</p><span class="time_date">'+(time)+'    |    '+(day)+'  '+(month)+', '+(year)+'</span></div></div></div>') ;
                }
                $(".msg_history").animate({scrollTop: 100000}, 1000); 
            }
        }
    });
}

function getNamesImages($g_id)
{
    id = $g_id;
    $.ajax({
        type: "GET",  
        url: "/chat/getNamesImages/"+id+"",
        dataType: "json",
        processData: false,
        contentType: false,
        success:function(data){
            for (i = 0; i < data.names.length; i++) {
                $('#group_participants_name_sidebar').append('<div><img src="' + data.avt[i] + '" alt="" width="40" height="40"><span>' + data.names[i] + '</span></a>');
            }
        }                  
    });       
}

function getImages($g_id)
{
    id = $g_id;

    $.ajax({
        type: "GET",  
        url: "/chat/getImgList/"+id+"",
        dataType: "json",
        processData: false,
        contentType: false,
        success:function(data){
            $.each(data.images,function(i,value){
                $("#ImgsList").append("<a target='_blank' href='"+(value.url)+"' data-lightbox='gr_list_img"+(id)+"'><img src='"+(value.url)+"'></a>");
            });
            
        }                  
    });       
    
}

function getFiles($g_id)
{
    id = $g_id;
    $.ajax({
        type: "GET",  
        url: "/chat/getFileList/"+id+"",
        dataType: "json",
        processData: false,
        contentType: false,
        success:function(data){
            $.each(data.files,function(i,value){
                name = value.url.replace('Modules/ChatBox/public/files/','');
                $("#FilesList").append('<div style="padding: 15px;"><img src="/public/svg/files.svg" width="30px"><a style="margin-left: 10px;" download href="' + value.url + '">' + name + '</a></div>');
            });
            
        }                  
    });       
}



function empty()
{
    $("#ImgsList").empty();
    $("#FilesList").empty();
}


   
   
   
   