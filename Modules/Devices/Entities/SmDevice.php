<?php

namespace Modules\Devices\Entities;

use Illuminate\Database\Eloquent\Model;

class SmDevice extends Model
{
    protected $guarded = [];

    public $table = "sm_devices";

    public function setDeviceTypeAttribute($value)
    {
        $this->attributes['device_type'] = SmDeviceType::where('title', $value)->first()->id;
    }

    public function deviceType()
    {
        return $this->belongsTo('Modules\Devices\Entities\SmDeviceType', 'device_type', 'id');
    }

    public function role()
    {
        return $this->belongsTo('Modules\RolePermission\Entities\InfixRole');
    }
}
