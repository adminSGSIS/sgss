<?php

namespace Modules\Devices\Entities;

use Illuminate\Database\Eloquent\Model;

class SmDeviceType extends Model
{
    public $table = "sm_devices_type";

    protected $guarded = [];

    public function scopeDisabled($query)
    {
        return $query->where('disabled', '0');
    }

    public function devices()
    {
        return $this->hasMany('Modules\Devices\Entities\SmDevice', 'device_type');
    }
}
