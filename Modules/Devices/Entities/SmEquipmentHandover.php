<?php

namespace Modules\Devices\Entities;

use Illuminate\Database\Eloquent\Model;

class SmEquipmentHandover extends Model
{
    protected $guarded = [];
    public $table = "sm_equipment_handover";

    public function setIdAttribute($value)
    {
        $this->attributes['device'] = $value;
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value;
        if (auth()->user()->role_id == 1 || auth()->user()->role_id == 16) {
            $this->attributes['status'] = "Approved";
        }
    }
}
