<?php

namespace Modules\Devices\Entities;

use Illuminate\Database\Eloquent\Model;

class SmReleasedDevice extends Model
{
    protected $guarded = [];
    public $table = "sm_released_device";

    public function setIdAttribute($value)
    {
        $this->attributes['device'] = $value;
    }
}
