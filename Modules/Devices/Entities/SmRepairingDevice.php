<?php

namespace Modules\Devices\Entities;

use Illuminate\Database\Eloquent\Model;

class SmRepairingDevice extends Model
{
    protected $guarded = [];
    public $table = "sm_repairing_device";
}
