<?php

namespace Modules\Devices\Http\Controllers;

use App\User;
use App\SmStaff;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Modules\Devices\Entities\SmDevice;
use Modules\Devices\Entities\SmReleasedDevice;
use Modules\Devices\Entities\SmRepairingDevice;
use Modules\Devices\Entities\SmEquipmentHandover;

class BorrowDevicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function error($message = 'Operation Failed')
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }

    private function success($message = 'Operation Success')
    {
        Toastr::success($message, 'Success');
        return redirect()->back();
    }

    public function index()
    {
        $user = auth()->user();
            
        $query = SmEquipmentHandover::query()
            ->where('school_id', $user->school_id)
            ->where('handed_over', '0');

        if ($user->role_id == 1 || $user->role_id == 16) {
            $equipments = $query->get();
        } else {
            $equipments = $query->where('created_by', $user->id)->get();
        }
        return view('devices::equipmentsHandover', compact('equipments'));
    }
    
    public function create(SmDevice $device)
    {
        return view('devices::borrowDevice', compact('device'));
    }

    public function store(Request $request, SmDevice $device)
    {
        $request->validate(['condition' => 'required']);
       
        if ($device->quantity == 0) {
            return $this->error('Device not ready to loan');
        }
        try {
            DB::beginTransaction();
            
            $device->lockForUpdate();
            $device->update(['quantity' => 0]);
            
            SmEquipmentHandover::create($request->all());
            
            DB::commit();
            return $this->success();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error();
        }
    }

    public function delete(SmEquipmentHandover $equipment)
    {
        $device = SmDevice::find($equipment->device);
        $device->quantity = (int)$device->quantity + (int)$equipment->quantity;
        $device->save();

        $equipment->delete();
        return $this->success();
    }

    public function approve(SmEquipmentHandover $equipment)
    {
        try {
            DB::beginTransaction();

            if ($equipment->status == 'Handing Over') {
                $equipment->status = "Handed Over";
                $equipment->handed_over = 1;
                $equipment->save();

                SmDevice::find($equipment->device)->update(['quantity' => 1]);

                DB::commit();
                return $this->success();
            }
            $equipment->update(['status' => "Approved"]);

            DB::commit();
            return $this->success();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error();
        }
    }

    public function handover(SmEquipmentHandover $equipment)
    {
        try {
            DB::beginTransaction();
            $equipment->status = "Handing Over";
            $role_id = auth()->user()->role_id;
            if ($role_id == 1 || $role_id == 16) {
                $equipment->status = "Handed Over";
                $equipment->handed_over = 1;
                
                SmDevice::find($equipment->device)->update(['quantity' => 1]);
            }
            $equipment->save();
            DB::commit();
            return $this->success();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error();
        }
    }

    public function handedOver()
    {
        $user = auth()->user();
        if ($user->role_id == 1 || $user->role_id == 16) {
            $equipments = SmEquipmentHandover::where('handed_over', '1')->get();
        } else {
            $equipments = SmEquipmentHandover::where('handed_over', '1')->where('created_by', $user->id)->get();
        }
        return view('devices::equipmentsHandedover', compact('equipments'));
    }

    public function form(SmEquipmentHandover $equipment)
    {
        return view('devices::formEquipmentsHandover', compact('equipment'));
    }

    public function formReturn(SmEquipmentHandover $equipment)
    {
        return view('devices::formReturnEquipmentsHandover', compact('equipment'));
    }
    
    public function qrCodeOption($id)
    {
        try {
            $user = auth()->user();
            if ($user->role_id == 1 || $user->role_id == 16) {
                return $this->error();
            }

            DB::beginTransaction();
            $equipment = SmEquipmentHandover::where('device', $id)->where('handed_over', '0')->first();
            if ($equipment) {
                $equipment->status = "Handed Over";
                $equipment->handed_over = 1;
                $equipment->save();
                
                SmDevice::find($equipment->device)->update(['quantity' => 1]);
               
                DB::commit();
                Toastr::success('Return device from '. User::find($equipment->created_by)->full_name . ' success', 'Success');
                return redirect('/devices/equipments-handover/handedover');
            }
            
            $device = SmDevice::find($id);
            $releasedDevice = SmReleasedDevice::where('device', $id)->first();
            $reparingDevice = SmRepairingDevice::where('device', $id)->where('is_done', '0')->first();
            $status = '';
            if ($releasedDevice) {
                $status = "Is released";
            } elseif ($reparingDevice) {
                $status = "Is repairing";
            } else {
                $status = "Ready to loan";
            }
            return view('devices::qr_code_option', compact('device', 'status'));
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error();
        }
    }
    
    public function qrCodeHandover($id)
    {
        try {
            $user = auth()->user();
            if ($user->role_id == 1 || $user->role_id == 16) {
                return $this->error();
            }
            
            if (SmDevice::where('id', $id)->where('quantity', '0')->first()) {
                return $this->error('Device not ready to loan');
            }
            
            $device = SmDevice::find($id);
            $users = SmStaff::orderBy('full_name', 'asc')->get();
            $role = [];
            foreach ($users as $user) {
                if (!in_array($user->role_id, $role)) {
                    $role[] = $user->role_id;
                }
            }
            return view('devices::qr_code_borrow', compact('device', 'role', 'users'));
        } catch (\Exception $e) {
            return $this->error();
        }
    }
}
