<?php

namespace Modules\Devices\Http\Controllers;

use App\SmStaff;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Modules\Devices\Entities\SmDevice;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Modules\Devices\Entities\SmReleasedDevice;
use Modules\RolePermission\Entities\InfixRole;
use Modules\Devices\Entities\SmRepairingDevice;
use Modules\Devices\Entities\SmEquipmentHandover;

class DevicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function error($message = 'Operation Failed')
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }

    private function success($message = 'Operation Success')
    {
        Toastr::success($message, 'Success');
        return redirect()->back();
    }

    public function index()
    {
        $roles = InfixRole::whereNotIn('id', [1, 2, 3])->get();
        $devices = SmDevice::where('quantity', '!=', '0')->get();
        return view('devices::index', compact('roles', 'devices'));
    }

    public function store(Request $request)
    {
        $request->validate(['role_id' => 'required']);

        if (!($request->device_type)) {
            Toastr::error('Please choose device type', 'Failed');
            return back()->withInput();
        }
        
        for ($i = 0; $i < $request->quantity; $i++) {
            $device = SmDevice::create($request->all());

            $filename = 'qr_' . round(microtime(true) * 1000) . '.svg';
            $data = url('/devices/qr-code/option') . "/" . $device->id;
            QrCode::format('svg')->size(50)->generate($data, 'public/device-code/' . $filename);
            
            $device->update(['qr_code_path' => 'public/device-code/' . $filename]);
        }

        return $this->success();
    }

    public function show(SmDevice $device)
    {
        return view('devices::deviceView', compact('device'));
    }

    public function edit(SmDevice $editData)
    {
        $role_id = auth()->user()->role_id;
        if (!($role_id == 1) && !($role_id == 16)) {
            return $this->error('You don\'t have permission');
        }
        $roles = InfixRole::whereNotIn('id', [1, 2, 3])->get();
        $devices = SmDevice::where('quantity', '!=', '0')->get();
        return view('devices::index', compact('roles', 'devices', 'editData'));
    }

    public function update(Request $request, SmDevice $device)
    {
        $device->update($request->all());
        return $this->success();
    }

    public function deleteView(SmDevice $device)
    {
        $title = "Are you sure to release ?";
        $url = url('devices/release/' . $device->id);
        return view('devices::release', compact('title', 'url', 'device'));
    }

    public function destroy(Request $request)
    {
        if (SmDevice::where('id', $request->id)->where('quantity', '0')->first()) {
            return $this->error('Device not ready to release');
        }

        try {
            DB::beginTransaction();
            $device = SmDevice::find($request->id);
            $device->update(['quantity' => 0]);

            SmReleasedDevice::create($request->all());
            DB::commit();
            return $this->success();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error();
        }
    }

    public function releasedDevice()
    {
        $releasedDevice = SmReleasedDevice::all();
        return view('devices::releasedDevice', compact('releasedDevice'));
    }
}
