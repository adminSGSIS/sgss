<?php

namespace Modules\Devices\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Modules\Devices\Entities\SmDeviceType;

class DevicesTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function error($message = 'Operation Failed')
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }

    private function success($message = 'Operation Success')
    {
        Toastr::success($message, 'Success');
        return redirect()->back();
    }

    public function index()
    {
        return view('devices::chooseDeviceType');
    }

    public function create()
    {
        return view('devices::newDeviceType');
    }

    public function store(Request $request)
    {
        if (SmDeviceType::where('title', $request->title)->disabled()->first()) {
            return $this->error('Duplicate title');
        }
        SmDeviceType::create($request->all());
        return $this->success();
    }

    public function edit(SmDeviceType $deviceType)
    {
        return view('devices::newDeviceType', compact('deviceType'));
    }

    public function update(Request $request, SmDeviceType $deviceType)
    {
        if (SmDeviceType::where('title', $request->title)->disabled()->first()) {
            return $this->error('Duplicate title');
        }

        $deviceType->update($request->all());
        return $this->success();
    }

    public function destroy(SmDeviceType $deviceType)
    {
        if ($deviceType->devices->first()) {
            return $this->error('Please release device first');
        }

        $deviceType->update(['disabled' => 1]);
        return $this->success();
    }
}
