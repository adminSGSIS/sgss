<?php

namespace Modules\Devices\Http\Controllers;

use App\SmStaff;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Modules\Devices\Entities\SmDevice;
use Modules\Devices\Entities\SmRepairingDevice;

class RepairDevicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function error($message = 'Operation Failed')
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }

    private function success($message = 'Operation Success')
    {
        Toastr::success($message, 'Success');
        return redirect()->back();
    }

    public function index()
    {
        $role_id = auth()->user()->role_id;
        if ($role_id != 1 && $role_id != 16) {
            return $this->error('You don\'t have permission');
        }
        $repairDevice = SmRepairingDevice::where('is_done', '0')->get();
        return view('devices::repairingDevice', compact('repairDevice'));
    }

    public function create(SmDevice $device)
    {
        $staffs = SmStaff::where('role_id', '12')->get();
        return view('devices::repairDevice', compact('device', 'staffs'));
    }

    public function store(Request $request, SmDevice $device)
    {
        if (SmDevice::where('id', $device->id)->where('quantity', '0')->first()) {
            return $this->error('Device not ready to repair');
        }
        
        try {
            DB::beginTransaction();
            SmRepairingDevice::create($request->all());

            $device->update(['quantity' => 0]);
            DB::commit();
            return $this->success();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error();
        }
    }

    public function repaired()
    {
        $role_id = auth()->user()->role_id;
        if ($role_id != 1 && $role_id != 16) {
            return $this->error('You don\'t have permission');
        }
        $repairDevice = SmRepairingDevice::where('is_done', '1')->get();
        return view('devices::repairedDevice', compact('repairDevice'));
    }

    public function repairDone(SmRepairingDevice $repairDevice)
    {
        try {
            DB::beginTransaction();
            $repairDevice->update(['is_done' => 1]);

            $device = SmDevice::find($repairDevice->device);
            $device->update(['quantity' => 1]);
            DB::commit();
            return $this->success();
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->error();
        }
    }

    public function edit(SmRepairingDevice $editData)
    {
        $device = SmDevice::find($editData->device);
        $staffs = SmStaff::where('role_id', '12')->get();
        return view('devices::repairDevice', compact('device', 'staffs', 'editData'));
    }

    public function update(Request $request, SmRepairingDevice $repairDevice)
    {
        $repairDevice->update($request->all());
        return $this->success();
    }
}
