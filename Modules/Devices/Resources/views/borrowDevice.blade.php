@extends('backEnd.master')
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('devices/equipments-handover')}}">Equipments Handover</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                Loan Device
                            </h3>
                        </div>
                       
                        @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/borrow/edit/'.$editData->id,
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @else
                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/borrow/'.$device->id,
                            'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @endif
                        <input type="hidden" name="quantity" value="1">
                        <input type="hidden" name="school_id" value="{{ auth()->user()->school_id }}">
                        <input type="hidden" name="created_by" value="{{ auth()->id() }}">
                        <input type="hidden" name="status" value="Pending">
                        <input type="hidden" name="device" value="{{ $device->id }}">
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(session()->has('message-success'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success') }}
                                            </div>
                                        @elseif(session()->has('message-danger'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger') }}
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                            <div class="error text-danger">
                                                <strong>{{ 'Please fill up the required fields' }}</strong></div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" autocomplete="off" value="{{isset($editData)? $editData->device_name : $device->device_name }}" readonly>
                                                    <label>Device Name<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div> 
                                            
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('condition') ? ' is-invalid' : '' }}" name="condition">
                                                        <option data-display="condition" value="">Select</option>
                                                        <option data-display="Working" value="Working">Working</option>
                                                        <option data-display="Not Working" value="Not Working">Not Working</option>
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('condition'))
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong>{{ $errors->first('condition') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <input class="primary-input form-control"
                                            type="text" name="remarks" autocomplete="off" value="{{isset($editData)? $editData->remarks : old('remarks') }}" id="remarks">
                                            <label>Remarks<span></span> </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span>
                                            @if(isset($editData))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.save')
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                         
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            
        </div>
    </section>
    
@endsection