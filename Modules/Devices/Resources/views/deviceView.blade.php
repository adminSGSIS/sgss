<p style="font-weight: 800">Device name : {{$device->device_name}}</p>
<p style="font-weight: 800">Device Code : <img src="{{asset($device->qr_code_path)}}" alt=""></p>
<p style="font-weight: 800">Role : {{Modules\RolePermission\Entities\InfixRole::find($device->role_id)->name}}</p>
<p style="font-weight: 800">Created at : {{$device->created_at}}</p>
<p style="font-weight: 800">Desciption : {{$device->desciption}}</p>