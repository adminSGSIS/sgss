@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('devices')}}">All devices</a>
                    <a href="{{url('devices/equipments-handover/handedover')}}">Equipments Handed Over</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Equipments Handover List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success-delete') }}
                                            </div>
                                        @elseif(session()->has('message-danger-delete'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger-delete') }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            <tr class="centerTh">
                                <th>Device Code</th>
                                <th>Device name</th>
                                <th>condition</th>
                                <th>quantity</th>
                                <th>Created by</th>
                                <th>Status</th>
                                <th>action</th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($equipments as $value)
                                    <tr class="centerTd">
                                        @php
                                            $device = Modules\Devices\Entities\SmDevice::find($value->device);
                                        @endphp
                                        <td>
                                            <img src="{{asset($device->qr_code_path)}}" alt="">
                                        </td>
                                        <td>{{$device->device_name}}</td>
                                        <td>{{$value->condition}}</td>
                                        <td>{{$value->quantity}}</td>
                                        <td>{{App\User::find($value->created_by)->full_name}}</td>
                                        <td>{{$value->status}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                                                        @if ($value->status != 'Approved')
                                                            <a class="dropdown-item" href="{{url('devices/equipments-handover/approve/'.$value->id)}}">Approve</a>
                                                        @endif

                                                        @if ($value->status == 'Pending')
                                                            <a class="dropdown-item" href="{{url('devices/equipments-handover/reject/'.$value->id)}}">Reject</a>
                                                        @endif
                                                    @endif
                                                        <a href="{{url('devices/view/'.$device->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Device Details" data-modal-size="modal-md">view</a>
                                                    
                                                    @if ($value->status != 'Pending')
                                                        <a class="dropdown-item" href="{{url('devices/equipments-handover/form/'.$value->id)}}">form</a>
                                                    @endif
                                                    
                                                    @if (App\SmStaff::where('email', Auth::user()->email)->first()->id == $value->created_by && $value->status == 'Pending')
                                                        <a class="dropdown-item" href="{{url('devices/equipments-handover/delete/'.$value->id)}}">delete</a>
                                                    @elseif(App\SmStaff::where('email', Auth::user()->email)->first()->id == $value->created_by && $value->status != 'Handing Over')
                                                        <a class="dropdown-item" href="{{url('devices/equipments-handover/handover/'.$value->id)}}">Handover</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection