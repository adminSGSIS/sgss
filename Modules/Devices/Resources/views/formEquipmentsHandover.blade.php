@extends('backEnd.master')
@section('css')
<style>
    .editP p{
        font-size: 16px;
        color: #445292;
    }
    table, th, td {
        border: 1px solid #445292;
        border-collapse: collapse;
    }
    th, td {
        color: #445292;
        padding: 5px;
        text-align: center;    
    }
    .footer-area{
        display: none;
    }
    @media print{    
        button{
            display: none !important;
        }
    }
</style>
@endsection
@section('mainContent')
<h1 style="text-align: center">Equipments Handover Form</h1>
<div style="display: flex" class="mt-4">
    <div>
        <img width="300px" class="mr-4" src="{{asset('public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png') }}" alt="logo">
    </div>
    <div class="editP">
        <p>Date : {{$equipment->created_at}}</p>
        <p>Name of person handing over : {{App\User::find($equipment->created_by)->full_name}}</p>
        <p>Signature of person handing over : .....................................................................</p>
    </div>
</div>

<table style="width:100%" class="mt-4 mb-4">
    <tr>
        <th>No</th>
        <th>Item Description</th>
        <th>Quantity</th>
        <th>Condition</th>
        <th>Remarks</th>
    </tr>
    <tr>
        @php
            $device = Modules\Devices\Entities\SmDevice::find($equipment->device);
        @endphp
        <td>1</td>
        <td>
            Name : {{$device->device_name}} <br>
            Type : {{Modules\Devices\Entities\SmDeviceType::find($device->device_type)->title}} <br>
            Role : {{Modules\RolePermission\Entities\InfixRole::find($device->role_id)->name}} <br>
        </td>
        <td>{{$equipment->quantity}}</td>
        <td>{{$equipment->condition}}</td>
        <td style="width: 220px;"></td>
    </tr>
</table>
<h1>Recipient</h1>

<div class="editP mt-3">
    <p>I, Ms/Mr ................................................................................................................ hereby acknowledge 
        that I have received the above metioned equipments. I understand that these equipments belong to Saigon Star Internation School and is under
        my possession for carrying out</p>
    <p class="mt-4">Signature : .....................................................................</p>
</div>

<button type="button" class="btn btn-success" onclick="window.print()">Print</button>


@endsection