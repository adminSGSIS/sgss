@extends('backEnd.master')
@section('css')
<style>
    p{
        font-size: 16px;
        color: #445292;
    }
    .editP div p{
        font-size: 14px;
        line-height: 0.8;
    }
    table, th, td {
        border: 1px solid #445292;
        border-collapse: collapse;
    }
    th, td {
        color: #445292;
        padding: 5px;
        text-align: center;    
    }
    .footer-area{
        display: none;
    }
    @media print{    
        button{
            display: none !important;
        }
    }
</style>
@endsection
@section('mainContent')
<img width="100px" src="{{asset('public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png') }}" alt="logo">

<h1 class="mt-4">CHECKLIST FORM ON RESIGNATION</h1>
<div style="display: flex">
    <p class="mr-5">Name of the resignee : {{App\SmStaff::find($equipment->created_by)->full_name}}</p>
    <p class="ml-5">Department : {{App\SmHumanDepartment::find(App\SmStaff::find($equipment->created_by)->department_id)->name}}</p>
</div>
<p>Leaving Date : {{$equipment->created_at}}</p>

<p>I.- ASSETS CHECKLIST</p>
<table style="width:100%" class="mt-4 mb-4">
    <tr>
        <th>No</th>
        <th>Description</th>
        <th>Assets code</th>
        <th>Amount</th>
        <th>Asset Condition</th>
        <th>Signature Receiving person</th>
    </tr>
    @php
        $device = Modules\Devices\Entities\SmDevice::find($equipment->device);
    @endphp
    <tr>
        <td>1</td>
        <td>
            Laptop/Desktop <br>
            Model : <br>
            Log-on password : <br>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>2</td>
        <td>
            Employee Card
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>3</td>
        <td>
            Mobile phone :
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>4</td>
        <td>
            - Room <br>
            - Drawer/Desk <br>
            - Cabinet <br>
            - Company office
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>5</td>
        <td>
           Cash advance
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>6</td>
        <td>
           Company Creit Card
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>7</td>
        <td>
           Health Insurance Card
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>8</td>
        <td>
           Uniform/PPE
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>9</td>
        <td>
           Other Company assets (taxi card, ...)
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>10</td>
        <td>
           Professional issues/work
           - <br>
           - <br>
           - <br>
           - <br>
           - <br>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    
</table>

<div class="editP">
    <div style="display: flex">
        <p class="mr-5">(1), (2), (3), (9)</p>
        <p>To be returned and checked by IT</p>
    </div>
    <div style="display: flex">
        <p class="mr-5">(7)</p>
        <p>To be returned and checked by HR and Administration</p>
    </div>
    <div style="display: flex">
        <p class="mr-5">(4), (8)</p>
        <p>To be returned and checked by Security Supervisor/functional department/Admin</p>
    </div>
    <div style="display: flex">
        <p class="mr-5">(5), (6)</p>
        <p>To be returned and checked by Facilities & Administrative (F&A) relevant persons and/or Department Head</p>
    </div>
    <div style="display: flex">
        <p class="mr-5">(10)</p>
        <p>To be returned and checked by functional department/Department Head/Head Teacher</p>
    </div>    
</div>

<p class="mt-4">II.- ESSENTIAL INFORMATION</p>
<div style="display: flex">
    <p style="font-size: 14px">1) &nbsp; Have you communicated all your passwords to substitute</p>
    <input type="checkbox" id="cb1" style="margin-top: 5px" class="ml-5">
    <label for="cb1">&nbsp;Yes</label>
    <input type="checkbox" id="cb2" style="margin-top: 5px" class="ml-5">
    <label for="cb2">&nbsp;No</label>
</div>

<div style="display: flex">
    <p style="font-size: 14px">2) &nbsp; Have you transfered all Company vouchers/documents to your substitute</p>
    <input type="checkbox" id="cb1" style="margin-top: 5px" class="ml-5">
    <label for="cb1">&nbsp;Yes</label>
    <input type="checkbox" id="cb2" style="margin-top: 5px" class="ml-5">
    <label for="cb2">&nbsp;No</label>
</div>

<div style="display: flex">
    <p style="font-size: 14px">3) &nbsp; Have you deleted your personal files from your PC (you agree that after your leaving date, IT Department will delete all your personal files off the server/local hard disk)</p>
    <input type="checkbox" id="cb1" style="margin-top: 5px">
    <label for="cb1">&nbsp;Yes</label>
    <input type="checkbox" id="cb2" style="margin-top: 5px" class="ml-3">
    <label for="cb2">&nbsp;No</label>
</div>

<p class="mb-5 mt-5">RESIGNEE</p>
<p class="mt-5">Date : .......................................</p>

<button type="button" class="btn btn-success" onclick="window.print()">Print</button>


@endsection