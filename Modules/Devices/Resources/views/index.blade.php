@extends('backEnd.master')
<script type="text/javascript" src="{{asset('public/js/qrcode.js')}}"></script>
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                    <a href="{{url('devices/released')}}">Released devices</a>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-title">
                                <h3 class="mb-30">
                                    @if(isset($editData))
                                        @lang('lang.update')
                                    @else
                                        @lang('lang.add')
                                    @endif
                                    Device
                                </h3>
                            </div>
                        
                            @if(isset($editData))
                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/edit/'.$editData->id,
                            'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                            @else
                                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices',
                                'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                            @endif
                            <input type="hidden" name="school_id" value="{{ auth()->user()->school_id }}">
                            <div class="white-box">
                                <div class="add-visitor">
                                    <div class="row">
                                        <div class="col-md-6">
                                            
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control"
                                                        type="text" name="device_name" autocomplete="off" value="{{isset($editData)? $editData->device_name : old('device_name') }}" required>
                                                        <label>Device Name<span></span> </label>
                                                        <span class="focus-border textarea"></span>
                                                    </div>
                                                </div>
                                                
                                                @if(!isset($editData))
                                                    <div class="col-lg-12 mt-3">
                                                        <div class="input-effect">
                                                            <input class="primary-input form-control"
                                                            type="number" min="1" name="quantity" autocomplete="off" value="{{isset($editData)? $editData->quantity : old('quantity') }}" id="quantity" required>
                                                            <label>Quantity<span></span> </label>
                                                            <span class="focus-border textarea"></span>
                                                        </div>
                                                    </div>
                                                @endif
                                                
                                                
                                                
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-6">
                                        
                                            <div class="row no-gutters input-right-icon">
                                                <div class="col">
                                                    <div class="row no-gutters input-right-icon">
                                                        <div class="col">
                                                            <div class="input-effect">
                                                                <input class="primary-input" type="text" id="placeholderFileOneName"
                                                                    placeholder="CHOOSE DEVICE TYPE"
                                                                    readonly
                                                                >
                                                                <span class="focus-border"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <a class="deleteUrl" data-modal-size="modal-md" title="Choose Device Type" href="/devices/type">
                                                                <button class="primary-btn-small-input" type="button">
                                                                    <label class="primary-btn small fix-gr-bg"
                                                                        >@lang('lang.browse')</label>
                                                                    <input type="text" class="d-none">
                                                                </button>
                                                            </a>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-lg-12">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control has-content"
                                                        type="text" name="device_type" id="device_type" autocomplete="off" value="{{isset($editData)? Modules\Devices\Entities\SmDeviceType::find($editData->device_type)->title : old('device_type') }}  " id="device_type" readonly>
                                                        <label>Device Type<span></span> </label>
                                                        <span class="focus-border textarea"></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="mt-4 mb-5">
                                        <div class="input-effect">
                                            <select class="niceSelect w-100 bb form-control{{ $errors->has('role_id') ? ' is-invalid' : '' }}" name="role_id">
                                                <option data-display="Role" value="">Select</option>
                                                @foreach ($roles as $item)
                                                    <option data-display="{{$item->name}}" value="{{$item->id}}" @if(isset($editData)) @if($editData->role_id == $item->id) selected @endif @elseif(old('role_id') == $item->id) selected @endif>{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('role_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong>{{ $errors->first('role_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control"
                                            type="text" name="description" autocomplete="off" value="{{isset($editData)? $editData->description : old('description') }}" id="description">
                                            <label>Description<span></span> </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                    @php 
                                        $tooltip = "";
                                        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16){
                                            $tooltip = "";
                                        }else{
                                            $tooltip = "You have no permission to add";
                                            if(isset($editData)){
                                                $tooltip = "You have no permission to update";
                                            } 
                                        }
                                    @endphp
                                    <div class="row mt-40">
                                        <div class="col-lg-12 text-center">
                                            <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">
                                                <span class="ti-check"></span>
                                                @if(isset($editData))
                                                    @lang('lang.update')
                                                @else
                                                    @lang('lang.save')
                                                @endif
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            @endif

            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Devices List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success-delete') }}
                                            </div>
                                        @elseif(session()->has('message-danger-delete'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger-delete') }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            <tr class="centerTh">
                                <th>Device Code</th>
                                <th>Device name</th>
                                <th>Device type</th>
                                <th>role</th>
                                <th>action</th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($devices as $value)
                                    <tr class="centerTd">
                                        <td>
                                            <img src="{{asset($value->qr_code_path)}}" alt="">
                                        </td>
                                        <td>{{$value->device_name}}</td>
                                        <td>{{$value->deviceType->title}}</td>
                                        <td>{{$value->role->name}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{url('devices/borrow/'.$value->id)}}" class="dropdown-item">loan</a>
                                                    <a href="{{url('devices/view/'.$value->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Device Details" data-modal-size="modal-md">view</a>
                                                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                                                        <a class="dropdown-item" href="{{url('devices/repair/'.$value->id)}}">Repair</a>
                                                        <a class="dropdown-item" href="{{url('devices/edit/'.$value->id)}}">@lang('lang.edit')</a>
                                                        <a href="{{url('devices/release-view/'.$value->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Release device" data-modal-size="modal-md">
                                                            release
                                                        </a>
                                                    @endif
                                                    
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection