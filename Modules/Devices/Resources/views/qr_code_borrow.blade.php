@extends('backEnd.master')
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('devices/equipments-handover')}}">Equipments Handover</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                Loan Device
                            </h3>
                        </div>
                       
                        @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/borrow/edit/'.$editData->id,
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @else
                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/borrow/'.$device->id,
                            'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @endif
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(session()->has('message-success'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success') }}
                                            </div>
                                        @elseif(session()->has('message-danger'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger') }}
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                            <div class="error text-danger">
                                                <strong>{{ 'Please fill up the required fields' }}</strong></div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" name="device_name" autocomplete="off" value="{{isset($editData)? $editData->device_name : $device->device_name }}" readonly>
                                                    <label>Device Name<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div> 
                                            
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('condition') ? ' is-invalid' : '' }}" name="condition">
                                                        <option data-display="condition" value="">Select</option>
                                                        <option data-display="Working" value="Working">Working</option>
                                                        <option data-display="Not Working" value="Not Working">Not Working</option>
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('condition'))
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong>{{ $errors->first('condition') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="">
                                    <select class="niceSelect w-100 bb form-control mb-3" onchange="addStaffFunc()" name="role_id" id="role_id">
                                        <option data-display="Role" value=""> @lang('lang.select') </option>
                                        @foreach($role as $value)
                                            @php
                                                $InfixRole = Modules\RolePermission\Entities\InfixRole::find($value);
                                            @endphp
                                            <option value="{{$value}}">{{$InfixRole->name}}</option>
                                        @endforeach
                                    </select>
                                    <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="addStaff" onclick="addStaffButton()" >
                                        <img width="20px" src="https://static.thenounproject.com/png/261370-200.png" alt="">
                                        <label>ADD STAFF</label>
                                    </button>
                                    <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="closeAddStaff" onclick="closeAddStaffButton()">
                                        <img style="margin-top: -3px" width="20px" src="https://static.thenounproject.com/png/261368-200.png" alt="">
                                        <label>ADD STAFF</label> 
                                    </button>
                                    @foreach ($role as $value)
                                    <div style="display: none" id="r{{$value}}">
                                        @foreach ($users as $user)
                                            @if ($user->role_id == $value)
                                                <div class="mt-2 {{ $errors->has('staffs') ? ' is-invalid' : '' }}" style="color: #828bb2; display: flex">
                                                    <input style="width: 15px; height: 15px; margin-top: 5px" type="radio" class="mr-2 common-radio" name="user_id" value="{{$user->id}}" id="{{$user->email}}">
                                                    <label for="{{$user->email}}">{{$user->full_name}}</label>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    @endforeach
                                </div>
                                <div class="row mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <input class="primary-input form-control"
                                            type="text" name="remarks" autocomplete="off" value="{{isset($editData)? $editData->remarks : old('remarks') }}" id="remarks">
                                            <label>Remarks<span></span> </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span>
                                            @if(isset($editData))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.save')
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                         
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            
        </div>
    </section>
    
@endsection
<script>
    var id, oldId;
    function addStaffFunc(){
        var x = document.getElementById("role_id").value;
        id = x;
        document.getElementById("addStaff").style.display = 'block';
        if(oldId){
            document.getElementById('r'+oldId).style.display = 'none';
        }
        else{
            document.getElementById('r'+x).style.display = 'none';
        }
        
        document.getElementById('closeAddStaff').style.display = 'none';
    }
    function addStaffButton(){
        oldId = id;
        document.getElementById('r'+id).style.display = 'block';
        document.getElementById('closeAddStaff').style.display = 'block';
        document.getElementById('addStaff').style.display = 'none';
    }
    function closeAddStaffButton(){
        document.getElementById('r'+id).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
        document.getElementById('addStaff').style.display = 'block';
    }
</script>