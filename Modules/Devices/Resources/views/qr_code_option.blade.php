@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('devices')}}">All devices</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">QRCODE Option</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success-delete') }}
                                            </div>
                                        @elseif(session()->has('message-danger-delete'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger-delete') }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            <tr class="centerTh">
                                <th>Device No</th>
                                <th>Device name</th>
                                <th>Device type</th>
                                <th>role</th>
                                <th>status</th>
                                <th>action</th>
                            </tr>
                            </thead>

                            <tbody>
                                <tr class="centerTd">
                                    <td>
                                        <img src="{{asset($device->qr_code_path)}}" alt="">
                                    </td>
                                    <td>{{$device->device_name}}</td>
                                    <td>{{Modules\Devices\Entities\SmDeviceType::find($device->device_type)->title}}</td>
                                    <td>{{Modules\RolePermission\Entities\InfixRole::find($device->role_id)->name}}</td>
                                    <td>{{$status}}</td>
                                    @if($status == "Is released" || $status == "Is repairing")
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{url('devices/view/'.$device->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Device Details" data-modal-size="modal-md">view</a>
                                                </div>
                                            </div>
                                        </td>
                                    @else
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{url('devices/qr-code/handover/'.$device->id)}}" class="dropdown-item">Handover</a>
                                                    <a href="{{url('devices/view/'.$device->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Device Details" data-modal-size="modal-md">view</a>
                                                    @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                                                        <a class="dropdown-item" href="{{url('devices/repair/'.$device->id)}}">Repair</a>
                                                        <a class="dropdown-item" href="{{url('devices/edit/'.$device->id)}}">@lang('lang.edit')</a>
                                                        <a href="{{url('devices/release-view/'.$device->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Release device" data-modal-size="modal-md">
                                                            release
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    @endif
                                        
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection