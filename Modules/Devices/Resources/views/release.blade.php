<div class="text-center mb-3">
   <h4>{{ $title }}</h4>
</div>
{{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/release', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}


<input type="hidden" name="id" value="{{$device->id}}">
<input type="hidden" name="school_id" value="{{ auth()->user()->school_id }}">

<button class="primary-btn fix-gr-bg mt-3" style="margin-left: 150px" type="submit">Apply</button>  
{{ Form::close() }}

