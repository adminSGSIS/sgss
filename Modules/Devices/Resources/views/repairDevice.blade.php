@extends('backEnd.master')
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('devices')}}">All devices</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                Repair Device
                            </h3>
                        </div>
                       
                        @if(isset($editData))
                            @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/repair/edit/'.$editData->id,
                                'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                            @endif
                        @elseif(Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/repair/'.$device->id,
                            'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @endif
                        <input type="hidden" name="device" value="{{ $device->id }}">
                        <input type="hidden" name="school_id" value="{{ auth()->user()->school_id }}">
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(session()->has('message-success'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success') }}
                                            </div>
                                        @elseif(session()->has('message-danger'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger') }}
                                            </div>
                                        @endif
                                        @if ($errors->any())
                                            <div class="error text-danger">
                                                <strong>{{ 'Please fill up the required fields' }}</strong></div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" autocomplete="off" value="{{$device->device_name }}" readonly>
                                                    <label>Device Name<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div> 
                                            
                                        </div>

                                        
                                       
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" autocomplete="off" value="{{Modules\Devices\Entities\SmDeviceType::find($device->device_type)->title }}" readonly>
                                                    <label>Device Type<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>

                                </div>
                                <div class="mt-3">
                                    <div class="input-effect">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('staff_id') ? ' is-invalid' : '' }}" name="staff_id">
                                            <option data-display="maintenance staff" value="">Select</option>
                                            @foreach ($staffs as $item)
                                                <option data-display="{{$item->full_name}}" @if(isset($editData)) @if($editData->staff_id == $item->id) selected @endif @elseif(old('role_id') == $item->id) selected @endif value="{{$item->id}}">{{$item->full_name}}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('staff_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('staff_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    <div class="input-effect mt-3 mb-3">
                                        <input class="primary-input form-control"
                                        type="text" name="notes" autocomplete="off" value="{{isset($editData)? $editData->remarks : old('notes') }}">
                                        <label>notes<span></span> </label>
                                        <span class="focus-border textarea"></span>
                                    </div>
                                </div>
                                @php 
                                    $tooltip = "";
                                    if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16){
                                        $tooltip = "";
                                    }else{
                                        $tooltip = "You have no permission to add";
                                        if(isset($editData)){
                                            $tooltip = "You have no permission to update";
                                        } 
                                    }
                                @endphp
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">
                                            <span class="ti-check"></span>
                                            @if(isset($editData))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.save')
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                         
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            
        </div>
    </section>
    
@endsection