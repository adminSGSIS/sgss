@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('devices')}}">All devices</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Repaired Devices List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success-delete') }}
                                            </div>
                                        @elseif(session()->has('message-danger-delete'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger-delete') }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            <tr class="centerTh">
                                <th>Device No</th>
                                <th>Device name</th>
                                <th>Staff Name</th>
                                <th>Notes</th>
                                <th>Created At</th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($repairDevice as $value)
                                    <tr class="centerTd">
                                        @php
                                            $device = Modules\Devices\Entities\SmDevice::find($value->device);
                                        @endphp
                                        <td>{{$device->device_id}}</td>
                                        <td>{{$device->device_name}}</td>
                                        <td>{{App\SmStaff::find($value->staff_id)->full_name}}</td>
                                        <td>{{$device->notes}}</td>
                                        <td>{{$value->created_at}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection