@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('devices/repaired')}}">Repaired Devices</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Repairing Devices List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success-delete') }}
                                            </div>
                                        @elseif(session()->has('message-danger-delete'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger-delete') }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            <tr class="centerTh">
                                <th>Device Code</th>
                                <th>Device name</th>
                                <th>Staff Name</th>
                                <th>Notes</th>
                                <th>action</th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($repairDevice as $value)
                                    <tr class="centerTd">
                                        @php
                                            $device = Modules\Devices\Entities\SmDevice::find($value->device);
                                        @endphp
                                        <td>
                                            <img src="{{asset($device->qr_code_path)}}" alt="">
                                        </td>
                                        <td>{{$device->device_name}}</td>
                                        <td>{{App\SmStaff::find($value->staff_id)->full_name}}</td>
                                        <td>{{$device->notes}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="{{url('devices/view/'.$device->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Device Details" data-modal-size="modal-md">view</a>
                                                    <a class="dropdown-item" href="{{url('devices/repair/edit/'.$value->id)}}">@lang('lang.edit')</a>
                                                    <a class="dropdown-item" href="{{url('devices/repair/done/'.$value->id)}}">Repair Done</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection