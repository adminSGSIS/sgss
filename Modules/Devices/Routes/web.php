<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('devices')->group(function () {
    Route::get('type', 'DevicesTypeController@index');
    Route::get('type/new', 'DevicesTypeController@create');
    Route::post('type/new', 'DevicesTypeController@store');
    Route::get('type/edit/{deviceType}', 'DevicesTypeController@edit');
    Route::post('type/edit/{deviceType}', 'DevicesTypeController@update');
    Route::get('type/delete/{deviceType}', 'DevicesTypeController@destroy');

    Route::get('/', 'DevicesController@index');
    Route::post('/', 'DevicesController@store');
    Route::get('view/{device}', 'DevicesController@show');
    Route::get('edit/{editData}', 'DevicesController@edit');
    Route::post('edit/{device}', 'DevicesController@update');
    Route::get('release-view/{device}', 'DevicesController@deleteView');
    Route::post('release', 'DevicesController@destroy');
    Route::get('released', 'DevicesController@released');

    Route::get('repair/{device}', 'RepairDevicesController@create');
    Route::post('repair/{device}', 'RepairDevicesController@store');
    Route::get('repairing', 'RepairDevicesController@index');
    Route::get('repaired', 'RepairDevicesController@repaired');
    Route::get('repair/done/{repairDevice}', 'RepairDevicesController@repairDone');
    Route::get('repair/edit/{editData}', 'RepairDevicesController@edit');
    Route::post('repair/edit/{repairDevice}', 'RepairDevicesController@update');
    
    Route::get('equipments-handover', 'BorrowDevicesController@index');
    Route::get('borrow/{device}', 'BorrowDevicesController@create');
    Route::post('borrow/{device}', 'BorrowDevicesController@store');
    Route::get('equipments-handover/delete/{equipment}', 'BorrowDevicesController@delete');
    Route::get('equipments-handover/approve/{equipment}', 'BorrowDevicesController@approve');
    Route::get('equipments-handover/reject/{id}', 'BorrowDevicesController@delete');
    Route::get('equipments-handover/handover/{equipment}', 'BorrowDevicesController@handover');
    Route::get('equipments-handover/handedover', 'BorrowDevicesController@handedOver');
    Route::get('equipments-handover/form/{equipment}', 'BorrowDevicesController@form');
    Route::get('equipments-handover/form-return/{equipment}', 'BorrowDevicesController@formReturn');
    Route::get('qr-code/option/{id}', 'BorrowDevicesController@qrCodeOption');
    Route::get('qr-code/handover/{id}', 'BorrowDevicesController@qrCodeHandover');
});
