<?php

namespace Modules\Docs\Entities;

use Illuminate\Database\Eloquent\Model;

class Docs extends Model
{
    protected $guarded = [];

    public $table = "docs";

    public function scopeStfOrStd($query, $value)
    {
        return $query->where('stf_or_std', $value);
    }

    public function scopeApproved($query)
    {
        return $query->where('approve_status', 1);
    }

    public function setDocsTypeAttribute($value)
    {
        $this->attributes['docs_type_id'] = DocsType::where('title', $value)->first()->id;
    }

    public function setFileAttribute($value)
    {
        $document_file = "";
        if (request()->file('file') != "") {
            $file = request()->file('file');
            $this->attributes['file_name'] = strtoupper($file->getClientOriginalName());
            $document_file = 'docs-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/docs/', $document_file);
            $document_file =  'public/uploads/docs/' . $document_file;
        }
        $this->attributes['file'] = $document_file;
    }
}
