<?php

namespace Modules\Docs\Entities;

use Illuminate\Database\Eloquent\Model;

class DocsType extends Model
{
    public $table = "docs_type";

    protected $guarded = [];

    public function docs()
    {
        return $this->hasMany('Modules\Docs\Entities\Docs');
    }
}
