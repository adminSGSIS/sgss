<?php

namespace Modules\Docs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Docs\Entities\Docs;
use Brian2694\Toastr\Facades\Toastr;
use App\SmClass;
use App\SmGeneralSettings;
use App\SmStudent;

class DocsController extends Controller
{
    private function error($message = 'Operation Failed')
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }

    private function success($message = 'Operation Success')
    {
        Toastr::success($message, 'Success');
        return redirect()->back();
    }

    public function getStudent(Request $request)
    {
        $students = SmStudent::where('class_id', $request->id)->get();
        return response()->json([$students]);
    }

    public function index()
    {
        $user = auth()->user();
        if ($user->role_id == 2 || $user->role_id == 3) {
            return $this->error('You don\'t have permission');
        }
        $classes = SmClass::where('academic_id', SmGeneralSettings::find(1)->session_id)->get();
        $docs = Docs::all();
        return view('docs::index', compact('docs', 'classes'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'stf_or_std' => "required",
            'docs_type' => 'required'
        ]);

        if ($request->stf_std == 'std' && $request->public == '') {
            Toastr::error('Please choose public or private', 'Failed');
            return back()->withInput();
        }

        if (!($request->student) && $request->public == "0" && $request->stf_std == "std") {
            Toastr::error('Please choose student for private', 'Failed');
            return back()->withInput();
        }
        Docs::create($request->all());
        return $this->success();
    }

    public function show(Docs $docs)
    {
        return view('docs::docsView', compact('docs'));
    }

    public function edit(Docs $editData)
    {
        $classes = SmClass::where('academic_id', SmGeneralSettings::find(1)->session_id)->get();
        $docs = Docs::all();
        return view('docs::index', compact('docs', 'classes', 'editData'));
    }

    public function update(Request $request, Docs $docs)
    {
        $request->validate([
            'stf_or_std' => "required",
            'docs_type' => 'required'
        ]);

        if (!($request->student) && $request->public == "0" && $request->stf_std == "std" && !($docs->private_stu_id)) {
            Toastr::error('Please choose student for private', 'Failed');
            return back()->withInput();
        }

        if ($request->file('file') != "" && $docs->file != '') {
            unlink($docs->file);
        }
        
        $docs->update($request->all());
        
        return $this->success();
    }

    public function deleteView($id)
    {
        $title = "Are you sure to detete ?";
        $url = url('docs/delete/' . $id);
        return view('backEnd.modal.delete', compact('id', 'title', 'url'));
    }

    public function destroy(Docs $docs)
    {
        if ($docs->file != '') {
            unlink($docs->file);
        }
        $docs->delete();
        return $this->success();
    }

    public function changeApproveStatus(Request $request)
    {
        $docs = Docs::find($request->id);
        $docs->update(['approve_status' => ($docs->approve_status == 0) ? 1 : 0]);
        return response()->json([$docs]);
    }

    public function pinToTop(Docs $docs)
    {
        $docs->update(['pin_to_top' => 1]);
        return $this->success();
    }

    public function unpinFromTop(Docs $docs)
    {
        $docs->update(['pin_to_top' => 0]);
        return $this->success();
    }
}
