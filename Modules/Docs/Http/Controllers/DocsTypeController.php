<?php

namespace Modules\Docs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Docs\Entities\DocsType;
use Brian2694\Toastr\Facades\Toastr;

class DocsTypeController extends Controller
{
    private function error($message = 'Operation Failed')
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }

    private function success($message = 'Operation Success')
    {
        Toastr::success($message, 'Success');
        return redirect()->back();
    }

    public function index()
    {
        $docs_type = DocsType::all();
        return view('docs::chooseDocsType', compact('docs_type'));
    }

    public function create()
    {
        return view('docs::newDocsType');
    }

    public function store(Request $request)
    {
        if (DocsType::where('title', $request->title)->first()) {
            return $this->error('Duplicate title');
        }

        DocsType::create($request->all());
        return $this->success();
    }

    public function edit(DocsType $docs_type)
    {
        return view('docs::newDocsType', compact('docs_type'));
    }

    public function update(Request $request, DocsType $docs_type)
    {
        if (DocsType::where('title', $request->title)->first()) {
            return $this->error('Duplicate title');
        }
        
        $docs_type->update($request->all());
        return $this->success();
    }

    public function destroy(DocsType $docs_type)
    {
        $docs_type->delete();
        return $this->success();
    }
}
