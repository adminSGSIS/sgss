<?php

namespace Modules\Docs\Http\Controllers;

use Illuminate\Routing\Controller;
use App\SmStudent;
use App\SmParent;
use Modules\Docs\Entities\Docs;
use Modules\Docs\Entities\DocsType;
use Brian2694\Toastr\Facades\Toastr;

class DocumentController extends Controller
{
    private function error($message = 'Operation Failed')
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }
    
    public function document()
    {
        return view('docs::document');
    }

    public function RenderStaffDocs($id)
    {
        $user = auth()->user();
        if ($user->role_id == 2 || $user->role_id == 3) {
            return $this->error('You don\'t have permission');
        }
        $docs =  Docs::where('docs_type_id', $id)
                ->stfOrStd('stf')
                ->approved()
                ->get();
        $docsType = DocsType::find($id);
        return view('docs::renderDocs', compact('docs', 'docsType'));
    }

    public function RenderStudentPublicDocs($id)
    {
        $docs =  Docs::where('docs_type_id', $id)
            ->where('public', '1')
            ->stfOrStd('std')
            ->approved()
            ->get();
        $docsType = DocsType::find($id);
        return view('docs::renderDocs', compact('docs', 'docsType'));
    }

    public function RenderStudentPrivateDocs($id)
    {
        try {
            $docsType = DocsType::find($id);

            $query = Docs::query()
                ->where('docs_type_id', $id)
                ->where('public', '0')
                ->stfOrStd('std')
                ->approved();

            $user = auth()->user();

            $docs =  $query->get();

            if ($user->role_id == 2) {
                $stu_id = SmStudent::where('user_id', $user->id)->first()->id;
                $docs =  $query->where('private_stu_id', $stu_id)->get();
            } elseif ($user->role_id == 3) {
                $parent_id = SmParent::where('user_id', $user->id)->first()->id;

                $stu_id = SmStudent::where('parent_id', $parent_id)->first()->id;
                
                $docs =  $query->where('private_stu_id', $stu_id)->get();
            }

            if ($docs->count() == 0) {
                return $this->error();
            }
            
            return view('docs::renderDocs', compact('docs', 'docsType'));
        } catch (\Exception $e) {
            return $this->error();
        }
    }

    public function allFiles()
    {
        try {
            $query = Docs::query()
                ->stfOrStd('std')
                ->where('public', '0')
                ->where('file', '!=', '')
                ->approved();

            $user = auth()->user();

            $publicFiles = Docs::stfOrStd('std')->where('public', '1')->where('file', '!=', "")->approved()->get();
            $privateFiles = null;

            if (auth()->check()) {
                $privateFiles = $query->get();

                if ($user->role_id == 2) {
                    $stu_id = SmStudent::where('user_id', $user->id)->first()->id;

                    $privateFiles = $query->where('private_stu_id', $stu_id)->get();
                } elseif ($user->role_id == 3) {
                    $parent_id = SmParent::where('user_id', $user->id)->first()->id;

                    $stu_id = SmStudent::where('parent_id', $parent_id)->first()->id;
                    
                    $privateFiles = $query->where('private_stu_id', $stu_id)->get();
                }
            }

            return view('docs::additionalFiles', compact('publicFiles', 'privateFiles'));
        } catch (\Exception $e) {
            return $this->error();
        }
    }

    public function DocsIT_app()
    {
        return view('docs::IT.app');
    }
}
