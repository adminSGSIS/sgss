<em>
	<!DOCTYPE HTML>
	<html>
		<head>
			<title>Documentation by "Team IT saigonstar international school"</title>
			<meta charset="utf-8" />
			<meta name="viewport" content="width=device-width, initial-scale=1" />
			<link rel="stylesheet" href="{{ asset('Modules/Docs/Resources/assets/css/main.css') }}" />
			<link rel="icon" href="{{ asset('public/uploads/settings/3ad73d8b576df04e1867cb5bde826068.png') }}" type="image/png"/>
		</head>
		<body>
			<section id="header">
				<header>
					<span class="image avatar"><img src="{{ asset('Modules/Docs/public/images/app_icon.png') }}" alt="" /></span>
					<h4 id="logo"><a href="/dashboard">Saigon star</a></h4>
				</header>
				<nav id="nav">
					<ul>
						<li><a href="#one" class="active">Table of Contents</a></li>
						<li><a href="#two" class="active">Flutter Plugin</a></li>
						<li><a href="#four" class="active">Flutter In Xcode</a></li>
						<li><a href="#five" class="active">Change Package Name</a></li>
						<li><a href="#six" class="active">Change domain url</a></li>
						<li><a href="#seven" class="active">Payment Gateway</a></li>
						<li><a href="#eight" class="active">Push Notification</a></li>
						<li><a href="#nine" class="active">Add language</a></li>
					</ul>
				</nav>
				<footer>
					<ul class="icons">
						<li><a href="https://www.facebook.com/saigonstarschool" target="_blank" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="https://twitter.com/SaigonStarIS" target="_blank" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="https://www.youtube.com/channel/UCcKlFmxYK5C6piLNe18hhRg/featured" target="_blank" class="icon fa fa-youtube-play"><span class="label">Youtube</span></a></li>
					</ul>
				</footer>
			</section>
			<div id="wrapper">
				<div id="main">
					<section id="one">
						<div class="container">
							<header class="major">
								<h2>Saigon star  (Cross Platform)</h2>
								<h2>Saigon star international school</h2>
							</header>
							<div class="borderTop">
								<div class="span-6 colborder info prepend-1">
									<p class="prepend-top"> <strong> Created: 25 December 2020<br>
										Company: Saigon Star international school<br>
									</p>
								</div>
								<div class="span-12 last">
									<p class="prepend-top append-0">Thank you for purchasing the app. If you have any questions that are beyond the scope of this help file, please feel free to message us <a href="mailto:support@spondonit.com" style='color:#3c55d1' target="_blank">it@sgstar.edu.vn</a>. Thanks so much!</p>
								</div>
							</div>
							<h1 id="toc" > Table of Contents</h1>
							<li><a href='#eclipseversion' style="color:#06F">Which Android Studio version is needed?</a></li>
							<li><a href='#importproject' style="color:#06F">How to open the project in Android Studio?</a></li>
							<li><a href='#packagename' style="color:#06F">How to change the package name?</a></li>
							<li><a href='#appname' style="color:#06F">How to change app name?</a></li>
							<li><a href='#serverurl' style="color:#06F">Where to put Server Url in Application?</a></li>
							<li><a href="#graphics" style="color:#06F">Graphics</a></li>
							<li><a href="#showcase" style="color:#06F">App Showcase</a></li>
							<li><a href="#rating" style="color:#06F">Rating</a></li>
							<li><a href="#credits" style="color:#06F">Credits</a></li>
							<section id="eclipseversion">
								<div class="container">
									<br>
									<h2>Which Android Studio version is needed? </h2>
									<p> Latest Android Studio version is recommended, which can be downloaded from here:<br />
										<a style="color:#3c55d1" href="http://developer.android.com/intl/es/sdk/index.html" target="_blank">http://developer.android.com/intl/es/sdk/index.html</a> 
									</p>
									<a class="lightbox" href="#images_one"> <img src="{{ asset('Modules/Docs/public/images/android_studio_ver.png') }}" alt=""/> </a>
									<div class="lightbox-target" id="images_one"> <img src="{{ asset('Modules/Docs/public/images/android_studio_ver.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
									<a class="lightbox" href="#images_two"> <img src="{{ asset('Modules/Docs/public/images/android_studio_ver_2.png') }}" alt=""/> </a>
									<div class="lightbox-target" id="images_two"> <img src="{{ asset('Modules/Docs/public/images/android_studio_ver_2.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
									<div class="section_bdr"></div>
								</div>
							</section>
							<section id="importproject">
								<div class="container">
									<br>
									<h2>How to open the project in Android Studio? </h2>
									<ul>
										<li>Open Android Studio > Open an Existing Android Studio Project > Select Your Project build.gradle file > ok </li>
										<li> Select Project </li>
									</ul>
									<a class="lightbox" href="#images_three"> <img src="{{ asset('Modules/Docs/public/images/import_1.png') }}" alt=""/> </a>
									<div class="lightbox-target" id="images_three"> <img src="{{ asset('Modules/Docs/public/images/import_1.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
									<a class="lightbox" href="#images_four"> <img src="{{ asset('Modules/Docs/public/images/import_2.png') }}" alt=""/> </a>
									<div class="lightbox-target" id="images_four"> <img src="{{ asset('Modules/Docs/public/images/import_2.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
									<div class="section_bdr"></div>
								</div>
							</section>
							<section id="two">
								<div class="container">
									<br>
									<h2>Android Studio — Install Flutter and Dart plugins</h2>
									<ul>
										<li>Open Android Studio > Open an Existing Android Studio Project > Select Your flutter project</li>
									</ul>
									<p class="prepend-top append-0">If you want you may go through this link <a href="https://flutter.dev/docs/get-started/install" style='color:#3c55d1' target="_blank">https://flutter.dev/</a>. Thanks so much!</p>
									<a class="lightbox" href="#images_five"> <img src="{{ asset('Modules/Docs/public/images/flutter_1.png') }}" alt=""/> </a>
									<div class="lightbox-target" id="images_five"> <img src="{{ asset('Modules/Docs/public/images/flutter_1.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
									<a class="lightbox" href="#images_six"> <img src="{{ asset('Modules/Docs/public/images/flutter_2.png') }}" alt=""/> </a> 
									<div class="lightbox-target" id="images_six"> <img src="{{ asset('Modules/Docs/public/images/flutter_2.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								</div>
							</section>
							<section id="four">
								<div class="container">
									<br>
									<h2>How to open in xcode? </br>project => ios folder => runner folder => open info.plist in top right side click open project in xcode </h2>
									<ul>
										<li>Then Runner open in xcode </li>
									</ul>
									<p class="prepend-top append-0">If you want you may go through this link <a href="https://medium.com/@agavatar/flutter-environment-setup-for-ios-791e64cd37d8" style='color:#3c55d1' target="_blank">open xcode</a>. Thanks so much!</p>
									<a class="lightbox" href="#images_xcodeOne"> <img src="{{ asset('Modules/Docs/public/images/xcode.png') }}" alt=""/> </a>
									<div class="lightbox-target" id="images_xcodeOne"> <img src="{{ asset('Modules/Docs/public/images/xcode.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
									<a class="lightbox" href="#images_xcode2"> <img src="{{ asset('Modules/Docs/public/images/xcode1.png') }}" alt=""/> </a> 
									<div class="lightbox-target" id="images_xcode2"> <img src="{{ asset('Modules/Docs/public/images/xcode1.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								</div>
							</section>
							<section id="five">
								<div class="container">
									<br>
									<h2>How to change the package name? </h2>
									<p> This is how you can rename package for both ios and android</p>
									<ul>
										<a class="lightbox" href="#images_seven"> <img src="{{ asset('Modules/Docs/public/images/packageNameflutter.png') }}" alt=""/> </a> 
										<div class="lightbox-target" id="images_seven"> <img src="{{ asset('Modules/Docs/public/images/packageNameflutter.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
										<a class="lightbox" href="#images_eight"> <img src="{{ asset('Modules/Docs/public/images/packageName_11.png') }}" alt=""/> </a> 
										<div class="lightbox-target" id="images_eight"> <img src="{{ asset('Modules/Docs/public/images/packageName_11.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								</div>
								</ul>
								<strong>Note: When renaming com in Android Studio, it might give a warning. In such case, select rename all.</strong> <br>
								<br>
								<a class="lightbox" href="#packagename_1"> <img src="{{ asset('Modules/Docs/public/images/packagename_1.png') }}" alt=""/> </a>
								<div class="lightbox-target" id="packagename_1"> <img src="{{ asset('Modules/Docs/public/images/packagename_1.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								<a class="lightbox" href="#packagename_2"> <img src="{{ asset('Modules/Docs/public/images/packagename_2.png') }}" alt=""/> </a>
								<div class="lightbox-target" id="packagename_2"> <img src="{{ asset('Modules/Docs/public/images/packagename_2.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								<a class="lightbox" href="#packagename_3"> <img src="{{ asset('Modules/Docs/public/images/packagename_3.png') }}" alt=""/> </a>
								<div class="lightbox-target" id="packagename_3"> <img src="{{ asset('Modules/Docs/public/images/packagename_3.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								<a class="lightbox" href="#packagename_4"> <img src="{{ asset('Modules/Docs/public/images/packagename_4.png') }}" alt=""/> </a>
								<div class="lightbox-target" id="packagename_4"> <img src="{{ asset('Modules/Docs/public/images/packagename_4.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								<a class="lightbox" href="#packagename_6"> <img src="{{ asset('Modules/Docs/public/images/packagename_6.png') }}" alt=""/> </a>
								<div class="lightbox-target" id="packagename_6"> <img src="{{ asset('Modules/Docs/public/images/packagename_6.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								<div class="section_bdr"></div>
						</div>
						</section>
						<section id="appname">
							<div class="container">
								<br>
								<h2>How to change app name? </h2>
								<a class="lightbox" href="#app_name"> <img src="{{ asset('Modules/Docs/public/images/appNameChange.png') }}" alt=""/> </a>
								<div class="lightbox-target" id="app_name"> <img src="{{ asset('Modules/Docs/public/images/appNameChange.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
								<div class="section_bdr"></div>
								</ul>
							</div>
						</section>
						<section id="six">
							<div class="container">
								<br>
								<h2>Where to put Server Url in Application? </h2>
								<p>
								<ul>
									<li>Select Project > go to lib folder > Utils folder > apis folder > Apis.dart > then change the root url with your url</li>
									<li> now if your Sever is http://www.abc.com so your service url like that <br>
										<b>http://www.abc.com/</b> <br>
								</ul>
								</p>
								<img class="lightbox" href="#app_name"> <img src="{{ asset('Modules/Docs/public/images/serverUrl.png') }}" alt=""/> </img>
								<br><br>
								<div class="section_bdr"></div>
							</div>
							</ul>
				</div>
				</section>
				<section id="graphics">
				<div class="container"> <br>
				<h2>Change App all graphics </h2>
				<p>You will find all image into /res/drawable-mdpi/,/res/drawable-hdpi/,/res/drawable-xhdpi/ folder. Just create your own graphics and replace those files. Create seperate graphics of different device screen sizes & place them into other drawble folders (drawable-hdpi & drawable-xhdpi) with same file name found at drawable-mdpi folder.  Check here for more information: <a style="color:#06F" href='http://developer.android.com/design/style/devices-displays.html' target='_blank'>Devices and Displays</a> & <a style="color:#06F" href='http://developer.android.com/guide/practices/screens_support.html' target='_blank'>Supporting Multiple Screens</a>.</p>
				<a class="lightbox" href="#Graphics_1"> <img src="{{ asset('Modules/Docs/public/images/Graphics_1.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="Graphics_1"> <img src="{{ asset('Modules/Docs/public/images/Graphics_1.png') }}" alt=""/><a class="lightbox-close" href="#"></a> </div>
				<br>
				<a class="lightbox" href="#Graphics_2"> <img src="{{ asset('Modules/Docs/public/images/Graphics_2.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="Graphics_2"> <img src="{{ asset('Modules/Docs/public/images/Graphics_2.png') }}" alt=""/><a class="lightbox-close" href="#"></a> </div>
				<div class="section_bdr"></div>
				</ul>
				</div>
				</section>
				<section id="showcase">
				<div class="container"> <br>
				<h2>Application Showcase in PlayStore </h2>
				<p>Once you will publish your app to Google Play or any other Android store, send us your app link. We will happy to showcase your app into our website & CodeCanyon page.</p>
				<div class="section_bdr"></div>
				</ul>
				</div>
				</section>
				<section id="rating">
				<div class="container"> <br>
				<h2>Rating </h2>
				<p>If you like our app, we will highly appreciate if you can provide us a rating of 5. You can rate us from your CodeCanyon Menu > Download page.</p>
				<div class="section_bdr"></div>
				</ul>
				</div>
				</section>
				<section class="container" id = "seven">
				<h1 >Payment Gateway</h1>
				<li><a href='#paytm' style="color:#06F">Paytm</a></li>
				<li><a href='#paypal' style="color:#06F">Paypal</a></li>
				<li><a href='#square' style="color:#06F">Gpay and Apple pay</a></li>
				<li><a href='#razor' style="color:#06F">Razor</a></li>
				</section>
				<section id="paytm">
				<div class="container"> <br>
				<h3>Paytm</h3>
				<h1>Go to https://developer.paytm.com/ and create an account.</h1>
				<ul>
				<li>
				Then fetch merchant id and keys from https://dashboard.paytm.com/next/apikey</li>
				</ul>
				<p class="prepend-top append-0">If you want you may go through this link <a href="https://medium.com/@iqan/flutter-payments-using-paytm-7c48539dfdee" style='color:#3c55d1' target="_blank">paytm</a></p>
				<h3>You should have a file from the path payment gateway=>paytm=>server=>config  => which named appConfig.js replace below keys with yours</h3>
				<a class="lightbox" href="#paytmappconfig.png"> <img src="{{ asset('Modules/Docs/public/images/paytmRoot.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="paytmappconfig.png"> <img src="{{ asset('Modules/Docs/public/images/paytmRoot.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<a class="lightbox" href="#paytmappconfig.png"> <img src="{{ asset('Modules/Docs/public/images/paytmappconfig.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="paytmappconfig.png"> <img src="{{ asset('Modules/Docs/public/images/paytmappconfig.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<ul>
				<li>
				Then in flutter project infixEdu=>lib=>paymentGateway=>paytm=>settings.dart , replace url where you deploy your backend server code or if you work on localhost you have to node.js install and start npm server , replace below url with your url along with ip address (example:http//your-computer-ip-address:3000/api/v1/paytm/initialPayment)</li>
				</ul>
				<a class="lightbox" href="#flutterpaytm.png"> <img src="{{ asset('Modules/Docs/public/images/flutterpaytm.png') }}" alt=""/> </a> 
				<div class="lightbox-target" id="iflutterpaytm.png"> <img src="{{ asset('Modules/Docs/public/images/flutterpaytm.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				</div>
				</section>
				</section>
				<section id="paypal">
					<div class="container">
						<br>
						<h3>Paypal</h3>
						<h1>Go to paypal account and create sandbox account for individual and business(test purpose)https://developer.paypal.com/</h1>
						<h1>You need business account for receive money and individual account for pay money . You can login  sandbox account through this business and individual email and password</h1>
						<ul>
							<li>
								Then fetch client id and client secret key from  your business account
							</li>
							<li>
								You should have a file from the path payment gateway=>paypal=>which named app.js replace below keys with yours
							</li>
							<li>
								You should have migrate mode after test with 'sandbox' to 'live' <strong style = "color:#000">(you need to change client id and client secret key as well)</strong> if you want to get real payment in your business account
							</li>
						</ul>
						<img class="lightbox" href="#paypalTwo.png" src="{{ asset('Modules/Docs/public/images/paypalTwo.png') }}" alt=""/>
						<div class="lightbox-target" id="paypalTwo.png"> <img src="{{ asset('Modules/Docs/public/images/paypalTwo.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
						<img class="lightbox" href="#paypalOne.png" src="{{ asset('Modules/Docs/public/images/paypalOne.png') }}" alt=""/>
						<div class="lightbox-target" id="paypalOne.png"> <img src="{{ asset('Modules/Docs/public/images/paypalOne.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
						<p class="prepend-top append-0">If you want you may go through this link <a href="https://www.youtube.com/watch?v=7k03jobKGXM" style='color:#3c55d1' target="_blank">paypal with nodejs</a></p>
						<h3>Then you need to change apiUrl from your flutter app for that you need to install nodeJs and start nodejs server , see below image carefully at line 18 .</h3>
						<a class="lightbox" href="#paypalThree.png"> <img src="{{ asset('Modules/Docs/public/images/paypalThree.png') }}" alt=""/> </a>
						<div class="lightbox-target" id="paypalThree.png"> <img src="{{ asset('Modules/Docs/public/images/paypalThree.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
						<ul>
							<li>
								In flutter project infixEdu=>lib=>paymentGateway=>payapal=>PaypalHomeScreen.dart , replace url where you deploy your backend server code or if you work on localhost you have to node.js install and start npm server , replace below url with your url along with ip address (for example : http//'your-computer-ip-address:3000/)
							</li>
						</ul>
				</section>
				</section>
				<section id="square">
				<div class="container"> <br>
				<h2>Square API For Gpay And Apple Pay</h1>
				<h1>Go to https://developer.squareup.com/us/en and create an account . Click on the new application to bring up the Square application settings pages.You will need the Application ID from the Credentials page to configure In-App Payments SDK</h2>
				<p class="prepend-top append-0">If you want you may go through this link <a href="https://github.com/square/in-app-payments-flutter-plugin/blob/master/doc/get-started.md" style='color:#3c55d1' target="_blank">Square</a></p>
				<p>You should have Application ID,Location ID and Apple Merchant ID to get payment from Gpay ,Apple Pay and Master Card,Credit Card</p>
				<a class="lightbox" href="#squareup.png"> <img src="{{ asset('Modules/Docs/public/images/squareup.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="squareup.png"> <img src="{{ asset('Modules/Docs/public/images/squareup.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<ul>
				<li>
				Then in flutter project infixEdu=>lib=>Utils=>config.dart , replace respective keys which are getting from squareup developer account</li>
				</ul>
				<a class="lightbox" href="#squareapiOne.png"> <img src="{{ asset('Modules/Docs/public/images/squareapiOne.png') }}" alt=""/> </a> 
				<div class="lightbox-target" id="squareapiOne.png"> <img src="{{ asset('Modules/Docs/public/images/squareapiOne.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				</div>
				</section>
				<section id="razor">
				<div class="container"> <br>
				<h3>Razor Payment Gateway</h3>
				<h1>Create an account to <a href="https://dashboard.razorpay.com/#/access/signin" style='color:#3c55d1' target="_blank">Razor</a>
				. Select the mode (Test or Live) for which you can to generate the API key.
				You have to generate separate API Keys for the test and live modes. No money is deducted from your account in test mode.
				</h1>
				<p class="prepend-top append-0">If you want you may go through this link <a href=" https://razorpay.com/docs/payment-gateway/flutter-integration/" style='color:#3c55d1' target="_blank">razor documentation</a></p>
				<h3>You find the  Key ID from your razor dashboard=>settings section</h3>
				<a class="lightbox" href="#razorOne.png"> <img src="{{ asset('Modules/Docs/public/images/razorOne.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="razorOne.png"> <img src="{{ asset('Modules/Docs/public/images/razorOne.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<ul>
				<li>
				Then in flutter project infixEdu=>lib=>paymentGateway=>RazorPayHome.dart , replace respective keys which are getting from razor developer account</li>
				</ul>
				<a class="lightbox" href="#razorTwow.png"> <img src="{{ asset('Modules/Docs/public/images/razorTwow.png') }}" alt=""/> </a> 
				<div class="lightbox-target" id="razorTwow.png"> <img src="{{ asset('Modules/Docs/public/images/razorTwow.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				</div>
				</section>
				<section id="eight">
				<div class="container"> <br>
				<h4>Add Firebase push notification to your Flutter app(android/ios)</h4>
				<h4>Before add firebase to your flutter app, you need to create a firebase project to connect to your app.
				</h4>
				<h4>At first create project at firebase</h4>
				<a class="lightbox" href="#rfire1.png"> <img src="{{ asset('Modules/Docs/public/images/fire1.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="fire1.png"> <img src="{{ asset('Modules/Docs/public/images/fire1.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<h4>Gradle file (android/build.gradle),add rules to include the Google Services Gradle plugin.Check that you have Google’s Maven repository</h4>
				<h1> classpath 'com.google.gms:google-services:4.3.3'</h1>
				<a class="lightbox" href="#fire2.png"> <img src="{{ asset('Modules/Docs/public/images/fire2.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="fire2.png"> <img src="{{ asset('Modules/Docs/public/images/fire2.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<h4>In your module (app-level) Gradle file (usually android/app/build.gradle), apply the Google Services Gradle plugin at bottom.</h4>
				<h1>apply plugin: 'com.google.gms.google-services'</h1>
				<a class="lightbox" href="#fire3.png"> <img src="{{ asset('Modules/Docs/public/images/fire3.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="fire3.png"> <img src="{{ asset('Modules/Docs/public/images/fire3.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<h4>In your module (app-level) Gradle file (usually android/app/build.gradle), apply the Google Services Gradle plugin at bottom.</h4>
				<h1>apply plugin: 'com.google.gms.google-services'</h1>
				<a class="lightbox" href="#fire3.png"> <img src="{{ asset('Modules/Docs/public/images/fire3.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="fire3.png"> <img src="{{ asset('Modules/Docs/public/images/fire3.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<h4>In your firebase application dashboard => Cloud Messaging => general create config file for android and ios . You have to provide package name for android and bundle identifier for ios. Download google-service.json then paste into android=>app folder , Download GoogleService-info.plist then paste into your ios=>Runner folder
				<a class="lightbox" href="#fire4.png"> <img src="{{ asset('Modules/Docs/public/images/fire4.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="fire4.png"> <img src="{{ asset('Modules/Docs/public/images/fire4.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<p class="prepend-top append-0">If you want you may go through this link <a href="https://firebase.google.com/docs/flutter/setup?platform=android" style='color:#3c55d1' target="_blank">firebase(android/ios) documentation</a></p>
				</div>
				</section>
				</section>
				<section id="nine">
				<div class="container"> <br>
				<h4>For adding new language , you need to add a file for example you want to add english so your file name should be localization_en.json (here localization_yourLanguageCode in my case here *en for english . so if you add language like bangla you should rename the file like localization_bn.json) </h4>
				<h4>Before add file to your flutter app, you need to translate all the word.
				</h4>
				<h4>So infixedu=>assets=>locale put your file in this folder</h4>
				<a class="lightbox" href="#lang1.png"> <img src="{{ asset('Modules/Docs/public/images/lang1.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="lang1.png"> <img src="{{ asset('Modules/Docs/public/images/lang1.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<h4>See below image, here we store data as "key":"value" pair ( "Students": "Students",) so make sure that you dont change key , always change value(right side string)</h4>
				<a class="lightbox" href="#lang2.png"> <img src="{{ asset('Modules/Docs/public/images/lang2.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="lang2.png"> <img src="{{ asset('Modules/Docs/public/images/lang2.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<a class="lightbox" href="#lang3.png"> <img src="{{ asset('Modules/Docs/public/images/lang3.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="lang3.png"> <img src="{{ asset('Modules/Docs/public/images/lang3.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<h4>In application.dart file you have to add your language name and language code carefully</h1>
				<a class="lightbox" href="#lang4.png"> <img src="{{ asset('Modules/Docs/public/images/lang4.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="lang4.png"> <img src="{{ asset('Modules/Docs/public/images/lang4.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				<h4>In application.dart file you also have to add languagelist[index] and languageCodeList[index] here index increase when you add a new language every time so see your last index carefully and increase by 1</h1>
				<a class="lightbox" href="#lang5.png"> <img src="{{ asset('Modules/Docs/public/images/lang5.png') }}" alt=""/> </a>
				<div class="lightbox-target" id="lang5.png"> <img src="{{ asset('Modules/Docs/public/images/lang5.png') }}" alt=""/> <a class="lightbox-close" href="#"></a> </div>
				</div>
				</section>
				<section id="credits">
				<div class="container"> <br>
				<h2>Credits</h2>
				<p>We've used folowing sources into this app:
				<ul>
				<li>http request for api call</li>
				<li>Network Image for showing image</li>
				<li>CardView</li>
				<li>OneSignal</li>
				<li>ListView</li>
				</ul>
				<hr>
				<div class="section_bdr"></div>
				</ul>
				</div>
				</section>
				</div>
				<div style="clear:both"></div>
				<hr>
				<section id="three" class="screen_slt_item">
				</section>
				<div style="clear:both"></div>
				<hr>
				<!-- Footer -->
				<section id="footer">
					<div class="container">
						<ul class="copyright">
							<li>
								<h3><span style="color:#0e8bcb;font-style:italic">SAIGON STAR SCHOOL INTERNATIONAL SHCOOL</span> <span style="color:#79b800;font-style:italic">Application</span></h3>
							</li>
							<li><a href="https://spondonit.com" target="_blank"><span style="color:#9f9f9f;font-style:italic">TEAM IT SAIGON STAR SCHOOL INTERNATIONAL SCHOOL</span></a></li>
						</ul>
					</div>
				</section>
			</div>
			<!-- Scripts --> 
			<script src="{{ asset('Modules/Docs/Resources/assets/js/jquery.min.js') }}"></script> 
			<script src="{{ asset('Modules/Docs/Resources/assets/js/jquery.scrollzer.min.js') }}"></script> 
			<script src="{{ asset('Modules/Docs/Resources/assets/js/jquery.scrolly.min.js') }}"></script> 
			<script src="{{ asset('Modules/Docs/Resources/assets/js/skel.min.js') }}"></script> 
			<script src="{{ asset('Modules/Docs/Resources/assets/js/util.js') }}"></script> 
			<script src="{{ asset('Modules/Docs/Resources/assets/js/main.js') }}"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$(window).scroll(function(){
						if ($(this).scrollTop() > 100) {
							$('.scrollup').fadeIn();
						} else {
							$('.scrollup').fadeOut();
						}
					});
					$('.scrollup').click(function(){
						$("html, body").animate({ scrollTop: 0 }, 600);
						return false;
					});
				});
			</script>
			<div><a href="#" class="scrollup">Scroll</a></div>
		</body>
	</html>
</em>