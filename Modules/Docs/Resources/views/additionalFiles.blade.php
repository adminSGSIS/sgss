@extends('docs::layouts.document_layout')
@section('mainContent')

<h1 style="font-size: 60px" class="mb-3">Additional Files</h1>

@if (count($publicFiles) > 0)
    <h1 style="font-size: 40px" class="mb-3">Public Files</h1>
@endif
@foreach ($publicFiles as $item)
    <h1># {{$item->title}}</h1>
    <p class="mt-3">Additional file : <a href="{{asset($item->file)}}" download>{{$item->file_name}}</a></p>
@endforeach

    @if($privateFiles != null)

        @if (count($privateFiles) > 0)
            <h1 style="font-size: 40px" class="mb-3">Private Files</h1>
        @endif

        @foreach ($privateFiles as $item)
            <h1># {{$item->title}}</h1>
            <p class="mt-3">Additional file : <a href="{{asset($item->file)}}" download>{{$item->file_name}}</a></p>
        @endforeach
        
    @endif

@endsection