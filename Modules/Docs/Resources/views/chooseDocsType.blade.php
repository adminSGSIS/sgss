<p>Docs Type</p>
@foreach ($docs_type as $item)
    <button type="button" value="{{$item->id}}" class="btn btn-default mt-2 tagsList removeOutline" 
        onclick="showStickIcon(this)" style="width: 85%; box-shadow: none;">
    {{$item->title}}
    <img style="display: none; width: 13px; position: absolute; right: 25%" 
    class="mt-1" src="{{ asset('public/svg/stick.svg') }}" />
</button>

<button type="button" class="btn mt-2 ml-2 removeOutline">
    <a class="deleteUrl dropdown-item" style="padding: 0" data-modal-size="modal-md" title="Update Tags" href={{"/docs/type/edit/" . $item->id}} >
        <img style="width: 18px" src="{{ asset('public/svg/note.svg') }}" />
    </a>
</button>
@endforeach

<a class="deleteUrl removeHover dropdown-item" data-modal-size="modal-md" title="New Tags" href="/docs/type/new" >
    <button type="button" class="primary-btn fix-gr-bg mt-3" style="width: 100%">New Type</button>
</a>
<script>
    function showStickIcon(x){
        for (let i = 0; i < document.getElementsByClassName('tagsList').length; i++) {
            document.getElementsByClassName('tagsList')[i].children[0].style.display = "none";
        }

        x.children[0].style.display = "inline";
        document.getElementById('docs_type').value = x.innerText;
    }
</script>