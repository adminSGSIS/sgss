@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
<link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/css')}}/purchase-proposal.css">
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Documents</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('docs')}}">New docs</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                @if(isset($editData))
                                    @lang('lang.update')
                                @else
                                    @lang('lang.add')
                                @endif
                                Document
                            </h3>
                        </div>
                       
                        @if(isset($editData))
                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'docs/edit/'.$editData->id,
                            'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'add-income-update']) }}
                        @else
                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'docs',
                            'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'add-income']) }}
                        @endif
                        <input type="hidden" name="created_by" value="{{ auth()->id() }}">
                        <input type="hidden" name="updated_by" value="{{ auth()->id() }}">
                        <input type="hidden" name="school_id" value="{{ auth()->user()->school_id }}">
                        <input type="hidden" name="approve_status" value="0">
                        <input type="hidden" name="pin_to_top" value="0">
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" name="title" autocomplete="off" value="{{isset($editData)? $editData->title : old('title') }}" required>
                                                    <label>Title<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12 mt-3">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('stf_or_std') ? ' is-invalid' : '' }}" name="stf_or_std" id="stf_std">
                                                        <option data-display="Docs for staffs or students ?" value="">Select</option>
                                                        <option data-display="Staffs" value="stf" @if(old('stf_or_std') == 'stf') selected @endif @if(isset($editData)) @if($editData->stf_or_std == "stf") selected @endif @endif>Staffs</option>
                                                        <option data-display="Students" value="std" @if(old('stf_or_std') == 'std') selected @endif @if(isset($editData)) @if($editData->stf_or_std == "std") selected @endif @endif>Students</option>
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('stf_or_std'))
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong>{{ $errors->first('stf_or_std') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                        

                                            <div style="display: none" id="public" class="col-lg-12 mt-3">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('public') ? ' is-invalid' : '' }}" name="public">
                                                        <option data-display="Do you want to public ?" value="">Select</option>
                                                        <option data-display="Yes" value="1" @if(old('public') == '1') selected @endif @if(isset($editData)) @if($editData->public == "1") selected @endif @endif>Yes</option>
                                                        <option data-display="No" value="0" @if(old('public') == '0') selected @endif @if(isset($editData)) @if($editData->public == "0") selected @endif @endif>No</option>
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('public'))
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong>{{ $errors->first('public') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div style="display: none" id="class" class="col-lg-12 mt-3">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('class') ? ' is-invalid' : '' }}">
                                                        <option data-display="Class" value="">Select</option>
                                                        @foreach($classes as $key=>$value)
                                                        <option  value="{{$value->id}}"
                                                        {{ old('class')==$value ? 'selected' : '' }}
                                                        >{{$value->class_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('class'))
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong>{{ $errors->first('class') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-6">
                                       

                                        <div class="row no-gutters input-right-icon">
                                            <div class="col">
                                                <div class="row no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="input-effect">
                                                            <input class="primary-input" type="text" id="placeholderFileOneName" placeholder="File" readonly="">
                                                            <span class="focus-border"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="primary-btn-small-input" type="button">
                                                            <label class="primary-btn small fix-gr-bg"
                                                                   for="document_file_1">@lang('lang.browse')</label>
                                                            <input type="file" class="d-none" name="file" id="document_file_1">
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-gutters input-right-icon mt-2">
                                            <div class="col">
                                                <div class="row no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="input-effect">
                                                            <input class="primary-input" type="text" id="placeholderFileOneName"
                                                                   placeholder="CHOOSE DOCS TYPE"
                                                                   readonly
                                                            >
                                                            <span class="focus-border"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <a class="deleteUrl" data-modal-size="modal-md" title="Choose Docs Type" href="/docs/type">
                                                            <button class="primary-btn-small-input" type="button">
                                                                <label class="primary-btn small fix-gr-bg"
                                                                       >@lang('lang.browse')</label>
                                                                <input type="text" class="d-none">
                                                            </button>
                                                        </a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control has-content"
                                                    type="text" name="docs_type" autocomplete="off" value="{{isset($editData)? Modules\Docs\Entities\DocsType::find($editData->docs_type_id)->title : old('docs_type') }}" id="docs_type" readonly>
                                                    <label>Docs Type<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div style="display: none" id="student" class="row mt-3">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('class') ? ' is-invalid' : '' }}" name="private_stu_id">
                                                        <option  data-display="{{isset($editData->private_stu_id) ? App\SmStudent::find($editData->private_stu_id)->full_name : "Student" }}" value="">Select</option>
                                                        
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('student'))
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong>{{ $errors->first('student') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div> 
                                        </div>

                                    </div>

                                </div>
                                <div class="row mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control" cols="0" rows="4" id="summernote" name="content">
                                                {{!empty($editData)? $editData->content : old('content')}}
                                            </textarea>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span>
                                            @if(isset($editData))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.save')
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                         
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Documents List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success-delete') }}
                                            </div>
                                        @elseif(session()->has('message-danger-delete'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger-delete') }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            <tr class="centerTh">
                                <th>@lang('lang.title')</th>
                                <th>docs for</th>
                                <th>docs type</th>
                                <th>public</th>
                                <th>created at</th>
                                <th>action</th>
                                @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 5)
                                <th>approve</th>
                                @endif
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($docs as $value)
                                    <tr class="centerTd">
                                        <td>{{$value->title}}</td>
                                        <td>{{$value->stf_or_std == "stf" ? 'Staff' : 'Student'}}</td>
                                        <td>{{Modules\Docs\Entities\DocsType::find($value->docs_type_id)->title}}</td>
                                        <td>{{$value->public == "0" ? 'No' : 'Yes'}}</td>
                                        <td>{{$value->created_at}}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    @if ($value->pin_to_top == 0)
                                                        <a href="{{url('docs/pinToTop/'.$value->id)}}" class="dropdown-item" title="Docs Details" data-modal-size="modal-md">Pin to top</a>
                                                    @else
                                                        <a href="{{url('docs/unpinFromTop/'.$value->id)}}" class="dropdown-item" title="Docs Details" data-modal-size="modal-md">UnPin from top</a>
                                                    @endif
                                                    <a href="{{url('docs/view/'.$value->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Docs Details" data-modal-size="modal-md">view</a>
                                                    <a class="dropdown-item" href="{{url('docs/edit/'.$value->id)}}">@lang('lang.edit')</a>
                                                    <a href="{{url('docs/delete-view/'.$value->id)}}" class="dropdown-item small fix-gr-bg modalLink" title="Delete Docs" data-modal-size="modal-md">
                                                        @lang('lang.delete')
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 5)
                                            <td>
                                                <div class="cbx">
                                                    <input @if($value->approve_status != 0) checked @endif value="{{$value->id}}" id="cbx" class="changeApproveStatus" type="checkbox"/>
                                                    <label for="cbx"></label>
                                                    <svg width="15" height="14" viewbox="0 0 15 14" fill="none">
                                                    <path d="M2 8.36364L6.23077 12L13 2"></path>
                                                    </svg>
                                                </div>
                                            </td>
                                        @elseif(Auth::user()->role_id == 1 || Auth::user()->role_id == 5)
                                            <td></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection
@section('script')
<script type="text/javascript">
    $('#summernote').summernote({

       toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['fontname', ['fontname']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'video']],
    ['view', ['fullscreen', 'codeview', 'help']],
    ['height', ['height']]
],
});
</script>

<script type="text/javascript">

    $(document).ready(function(){
        var value=$('#stf_std').val();
        var public_value = $('#public select').val();
        if(value == "std"){
            $('#public').css("display", "inline");
            if(public_value == "0"){
                $('#class').css("display", "inline");
                $('#student').css("display", "block");
            }
            else{
                $('#class').css("display", "none");
                $('#student').css("display", "none");
                $('#student select').val("");
            }
        }
        else if(value == "stf"){
            $('#public').css("display", "none");
            
        }
    });

    $(document).ready(function(){
        $('#stf_std').change(function(e){
            var value=$(this).val();
            if(value == "std"){
                $('#public').css("display", "inline");
            }
            else if(value == "stf"){
                $('#student select').val("");
                $('#public select').val("0");
                $('#public div div span').text("NO")
                $('#public').css("display", "none");
                $('#class').css("display", "none");
                $('#student').css("display", "none");
            }
        });
    });

    $(document).ready(function(){
        $('#public select').change(function(e){
            var value=$(this).val();
            if(value == "0"){
                $('#class').css("display", "inline");
                $('#student').css("display", "block");
            }
            else if(value == "1"){
                $('#student select').val("");
                $('#class').css("display", "none");
                $('#student').css("display", "none");
                $('#student select').val("");
            }
        });
    });

    $(document).ready(function() {
        $("#class select").on("change", function() {
            var url = $("#url").val();

            var formData = {
                id: $(this).val(),
            };
            $.ajax({
                type: "GET",
                data: formData,
                dataType: "json",
                url: url + "/" + "docs/getStudent",
                success: function(data) {
                    var a = "";
                    $.each(data, function(i, item) {
                        if (item.length) {
                            $("#student select").find("option").not(":first").remove();
                            $("#student ul").find("li").not(":first").remove();

                            $.each(item, function(i, student) {
                                $("#student select").append(
                                    $("<option>", {
                                        value: student.id,
                                        text: student.full_name,
                                    })
                                );

                                $("#student ul").append(
                                    "<li data-value='" +
                                    student.id +
                                    "' class='option'>" +
                                    student.full_name +
                                    "</li>"
                                );
                            });
                        } else {
                            $("#student .current").html("Student");
                            $("#student select").find("option").not(":first").remove();
                            $("#student ul").find("li").not(":first").remove();
                        }
                    });
                },
                error: function(data) {
                    console.log('Error:', data);
                },
            });
        });
    });

    $(document).ready(function(){
        $(".changeApproveStatus").click(function(){
            var url = $("#url").val();

            var formData = {
                id: $(this).val(),
            };
            $.ajax({
                type: "GET",
                data: formData,
                dataType: "json",
                url: url + "/" + "docs/changeApproveStatus",
                success: function (data) {
                    console.log(data);
                }
            });
        });
    });
    
    
</script>


@endsection