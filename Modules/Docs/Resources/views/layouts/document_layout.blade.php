<?php
 
    use App\SmNotification;
    use App\SmParent;   
    use App\User;

    $active_style = DB::table('sm_styles')->where('is_active', 1)->first();
    $styles = DB::table('sm_styles')->get();
    $generalSetting = $config = DB::table('sm_general_settings')->first();
    //$schoolSetting = $school_config = DB::table('sm_general_settings')->where('school_id',Auth::user()->school_id)->first();
    $system_date_foramt = App\SmDateFormat::find($config->date_format_id); 
    $dashboard_background = DB::table('sm_background_settings')->where([['is_default', 1], ['title', 'Dashboard Background']])->first();

     
    
    if (empty($dashboard_background)) {
        $css = "background: url('/public/backEnd/img/body-bg.jpg')  no-repeat center; background-size: cover; ";
    } else {
        if (!empty($dashboard_background->image)) {
            $css = "background: url('" . url($dashboard_background->image) . "')  no-repeat center; background-size: cover; ";
        } else {
            $css = "background:" . $dashboard_background->color;
        }
    } 

 
    if (!empty($school_config->logo)) {
        $logo = $school_config->logo;
    } else {
        $logo = 'public/uploads/settings/logo.png';
    }

    if (!empty($school_config->favicon)) {
        $fav = $school_config->favicon;
    } else {
        $fav = 'public/backEnd/img/favicon.png';
    }
    if (!empty($school_config->site_title)) {
        $site_title = $school_config->site_title;
    } else {
        $site_title = 'School Management System';
    }
    if (!empty($school_config->school_name)) {
        $school_name = $school_config->school_name;
    } else {
        $school_name = 'Infix Edu ERP';
    }

    //DATE FORMAT
    $DATE_FORMAT =  $system_date_foramt->format;

    $ttl_rtl = isset($config->ttl_rtl) ? $config->ttl_rtl : 2;

    $selected_language=App\SmLanguage::where('active_status',1)->first();
    if ($selected_language) {
        $language_universal= $selected_language->language_universal;
    } else {
        $language_universal='en';
    }
    
?><!DOCTYPE html>
{{-- <html lang="{{$language_universal }}" @if(isset ($ttl_rtl ) && $ttl_rtl ==1) dir="rtl" class="rtl" @endif > --}}
<html lang="{{ app()->getLocale() }}" @if(isset ($ttl_rtl ) && $ttl_rtl ==1) dir="rtl" class="rtl" @endif >
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel="icon" href="{{asset('public/uploads/settings/3ad73d8b576df04e1867cb5bde826068.png')}}" type="image/png"/>
    <title>Document</title>
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Bootstrap CSS -->

    @if(isset ($ttl_rtl ) && $ttl_rtl ==1)
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/rtl/bootstrap.min.css"/> 
    @else
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/bootstrap.css"/>
    @endif

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/jquery-ui.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/jquery.data-tables.css">
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/rowReorder.dataTables.min.css">
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/responsive.dataTables.min.css">


    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/bootstrap-datepicker.min.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/themify-icons.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/flaticon.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/nice-select.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/magnific-popup.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/fastselect.min.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/toastr.min.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/js/select2/select2.css"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/fullcalendar.min.css">
    {{-- <link rel="stylesheet" href="{{asset('public/landing/css/toastr.css')}}"> --}}
    @yield('css')
    <link rel="stylesheet" href="{{asset('public/backEnd/css/loade.css')}}"/> 
    @if(isset ($ttl_rtl ) && $ttl_rtl ==1)
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/rtl/style.css"/>
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/rtl/infix.css"/>
    @else
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/{{@$active_style->path_main_style}}"/>
        <link rel="stylesheet" href="{{asset('public/backEnd/')}}/css/{{@$active_style->path_infix_style}}"/>
    @endif
    <style>
        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover { background: {{$active_style->primary_color2}} !important; }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover { background: {{$active_style->primary_color2}}  !important; }
        ::placeholder { color: {{$active_style->primary_color}}  !important; } 
        .datepicker.datepicker-dropdown.dropdown-menu.datepicker-orient-left.datepicker-orient-bottom{ z-index: 99999999999 !important; background: #fff !important;        }
        .input-effect { float: left;  width: 100%; }
    </style>

    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : (event.keyCode);
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }
            return true;
        }
    </script>
   
</head>
<body class="admin"
      style="background: #dde7f7;">

        @php 
            // $tt = file_get_contents(url('/').'/'.$generalSetting->logo);
             $tt='';
        @endphp
        <input type="text" hidden value="{{ base64_encode($tt) }}" id="logo_img">
        <input type="text" hidden value="{{ $generalSetting->school_name }}" id="logo_title">
<div class="main-wrapper" style="min-height: 600px">
    <!-- Sidebar  -->




<input type="hidden" name="url" id="url" value="{{url('/')}}">
<nav style="background: linear-gradient(to right, #e0eafc, #cfdef3);" id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="{{url('/')}}">
          <img  src="{{asset('public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png') }}" alt="logo">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    <ul class="list-unstyled components">
            <li>
                <a href="#subMenuStudent" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-reading"></span>
                    Academic
                </a>
                <ul class="collapse list-unstyled" id="subMenuStudent">
                    @foreach ($docs_type as $item)
                        @if($item->docs->where('stf_or_std', 'std')->where('public', '1')->where('approve_status', 1)->where('pin_to_top', 0)->first())
                            <li>
                                <a href="{{url('document/std/y/'.$item->id)}}">{{$item->title}}</a>
                            </li>
                        @endif
                    @endforeach
                    @if(Auth::check())
                        @if (Auth::user()->role_id == 2)
                            @foreach ($docs_type as $item)
                                @if($item->docs->where('stf_or_std', 'std')->where('public', '0')->where('approve_status', 1)->where('pin_to_top', 0)
                                ->where('private_stu_id', App\SmStudent::where('user_id', Auth::user()->id)->first()->id)->first())
                                    <li>
                                        <a href="{{url('document/std/n/'.$item->id)}}">{{$item->title}} (private)</a>
                                    </li>
                                @endif
                            @endforeach
                        @elseif(Auth::user()->role_id == 3)
                            @foreach ($docs_type as $item)
                                @if($item->docs->where('stf_or_std', 'std')->where('public', '0')->where('approve_status', 1)->where('pin_to_top', 0)
                                ->where('private_stu_id', App\SmStudent::where('parent_id', App\SmParent::where('user_id', Auth::user()->id)->first()->id)->first()->id)->first())
                                    <li>
                                        <a href="{{url('document/std/n/'.$item->id)}}">{{$item->title}} (private)</a>
                                    </li>
                                @endif
                            @endforeach
                        @else
                            @foreach ($docs_type as $item)
                                @if($item->docs->where('stf_or_std', 'std')->where('public', '0')->where('approve_status', 1)->where('pin_to_top', 0)->first())
                                    <li>
                                        <a href="{{url('document/std/n/'.$item->id)}}">{{$item->title}} (private)</a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    @endif
                </ul>
            </li>

            @if(Auth::check())
                @if(Auth::user()->role_id != 2 && Auth::user()->role_id != 3)
                    <li>
                        <a href="#subMenuStaff" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <span class="flaticon-consultation"></span>
                            Office
                        </a>

                        <ul class="collapse list-unstyled" id="subMenuStaff">
                            @foreach ($docs_type as $item)
                                @if($item->docs->where('stf_or_std', 'stf')->where('approve_status', 1)->where('pin_to_top', 0)->first())
                                    <li>
                                        <a href="{{url('document/stf/'.$item->id)}}">{{$item->title}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @endif        
            @endif

            @if(Auth::check())
                @if(Auth::user()->role_id != 2 && Auth::user()->role_id != 3)
                    <li>
                        <a href="#subMenuIT" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <span class="fa fa-desktop" style="margin-left: 21px;"></span>
                            IT
                        </a>
                        
                        <ul class="collapse list-unstyled" id="subMenuIT">
                            <li>
                                <a href="/document/it/app">App</a>
                            </li>
                        </ul>
                    </li>
                @endif        
            @endif

            <li>
                <a href="/document/files">
                    <span class="flaticon-analysis"></span>
                    additional files
                </a>
            </li>

            @foreach ($docs_type as $item)
                @if($item->docs->where('stf_or_std', 'std')->where('public', '1')->where('approve_status', 1)->where('pin_to_top', 1)->first())
                    <li>
                        <a href="{{url('document/std/y/'.$item->id)}}">
                            <span>
                                <img style="margin-left: 18px" src="https://www.flaticon.com/svg/static/icons/svg/3068/3068315.svg" width="16px" alt="">
                            </span>
                            {{$item->title}}
                        </a>
                    </li>
                @endif
            @endforeach

            @if(Auth::check())
                @if (Auth::user()->role_id == 2)
                    @foreach ($docs_type as $item)
                        @if($item->docs->where('stf_or_std', 'std')->where('public', '0')->where('approve_status', 1)->where('pin_to_top', 1)
                        ->where('private_stu_id', App\SmStudent::where('user_id', Auth::user()->id)->first()->id)->first())
                            <li>
                                <a href="{{url('document/std/n/'.$item->id)}}">
                                    <span>
                                        <img style="margin-left: 18px" src="https://www.flaticon.com/svg/static/icons/svg/3068/3068315.svg" width="16px" alt="">
                                    </span>
                                    {{$item->title}} (private)
                                </a>
                            </li>
                        @endif
                    @endforeach
                @elseif(Auth::user()->role_id == 3)
                    @foreach ($docs_type as $item)
                        @if($item->docs->where('stf_or_std', 'std')->where('public', '0')->where('approve_status', 1)->where('pin_to_top', 1)
                        ->where('private_stu_id', App\SmStudent::where('parent_id', App\SmParent::where('user_id', Auth::user()->id)->first()->id)->first()->id)->first())
                            <li>
                                <a href="{{url('document/std/n/'.$item->id)}}">
                                    <span>
                                        <img style="margin-left: 18px" src="https://www.flaticon.com/svg/static/icons/svg/3068/3068315.svg" width="16px" alt="">
                                    </span>
                                    {{$item->title}} (private)
                                </a>
                            </li>
                        @endif
                    @endforeach
                @else
                    @foreach ($docs_type as $item)
                        @if($item->docs->where('stf_or_std', 'std')->where('public', '0')->where('approve_status', 1)->where('pin_to_top', 1)->first())
                            <li>
                                <a href="{{url('document/std/n/'.$item->id)}}">
                                    <span>
                                        <img style="margin-left: 18px" src="https://www.flaticon.com/svg/static/icons/svg/3068/3068315.svg" width="16px" alt="">
                                    </span>
                                    {{$item->title}} (private)
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endif

            @if(Auth::check())
                @if(Auth::user()->role_id != 2 && Auth::user()->role_id != 3)
                @foreach ($docs_type as $item)
                @if($item->docs->where('stf_or_std', 'stf')->where('approve_status', 1)->where('pin_to_top', 1)->first())
                    <li>
                        <a href="{{url('document/stf/'.$item->id)}}">
                            <span>
                                <img style="margin-left: 18px" src="https://www.flaticon.com/svg/static/icons/svg/3068/3068315.svg" width="16px" alt="">
                            </span>
                            {{$item->title}}
                        </a>
                    </li>
                @endif
            @endforeach
                @endif        
            @endif
    </ul>
</nav>
    <div id="main-content">

        <button type="button" id="sidebarCollapse" class="btn d-lg-none nav_icon">
            <i class="ti-more"></i>
        </button>


        @yield('mainContent')

</div>
</div>

<div class="has-modal modal fade" id="showDetaildModal">
    <div class="modal-dialog modal-dialog-centered" id="modalSize">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="showDetaildModalTile">@lang('lang.new_client_information')</h4>
                <button type="button" class="close icons" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" id="showDetaildModalBody">

            </div>

            <!-- Modal footer -->

        </div>
    </div>
</div>


<!--  Start Modal Area -->
<div class="modal fade invoice-details" id="showDetaildModalInvoice">
    <div class="modal-dialog large-modal modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('lang.add') @lang('lang.invoice')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body" id="showDetaildModalBodyInvoice">
            </div>

        </div>
    </div>
</div>

<!--
<div class="skype-button bubble" data-bot-id="spondonit"></div>


================Footer Area ================= -->
<footer class="footer-area">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 text-center">
                <p>Copyright &copy; 2020 All rights Saigon Star | This application is made by Saigon Star International school</p>
            </div>
        </div>
    </div>
</footer>
<!-- ================End Footer Area ================= -->

<script src="{{asset('public/backEnd/')}}/vendors/js/jquery-3.2.1.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/jquery-ui.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/jquery.data-tables.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/dataTables.buttons.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/buttons.flash.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/jszip.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/pdfmake.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/vfs_fonts.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/buttons.html5.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/buttons.print.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/dataTables.rowReorder.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/dataTables.responsive.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/buttons.colVis.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/popper.js">
</script>
{{--<script src="{{asset('public/backEnd/')}}/vendors/js/bootstrap.min.js">--}}
{{--</script>--}}
<script src="{{asset('public/backEnd/')}}/css/rtl/bootstrap.min.js">
</script>


<script src="{{asset('public/backEnd/')}}/vendors/js/nice-select.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/jquery.magnific-popup.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/fastselect.standalone.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/raphael-min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/morris.min.js">
</script>
<script src="{{asset('public/backEnd/')}}/vendors/js/ckeditor.js"></script>

<script type="text/javascript" src="{{asset('public/backEnd/')}}/vendors/js/toastr.min.js"></script>

<script type="text/javascript" src="{{asset('public/backEnd/')}}/vendors/js/moment.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/ckeditor.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/bootstrap_datetimepicker.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript" src="{{asset('public/backEnd/')}}/vendors/js/fullcalendar.min.js"></script>


<script type="text/javascript" src="{{asset('public/backEnd/')}}/js/jquery.validate.min.js"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/select2/select2.min.js"></script>

<script src="{{asset('public/backEnd/')}}/js/main.js"></script>
<script src="{{asset('public/backEnd/')}}/js/custom.js"></script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>

@if(App\SmGeneralSettings::isModule('Saas')== TRUE)
<script src="{{asset('public/backEnd/')}}/saas/js1/custom.js"></script>
@endif

<script src="{{asset('public/')}}/js/registration_custom.js"></script>
<script src="{{asset('public/backEnd/')}}/js/developer.js"></script>


<script type="text/javascript">
    //$('table').parent().addClass('table-responsive pt-4');
    // for select2 multiple dropdown in send email/Sms in Individual Tab
    $("#selectStaffss").select2();
    $("#checkbox").click(function () {
        if ($("#checkbox").is(':checked')) {
            $("#selectStaffss > option").prop("selected", "selected");
            $("#selectStaffss").trigger("change");
        } else {
            $("#selectStaffss > option").removeAttr("selected");
            $("#selectStaffss").trigger("change");
        }
    });


    // for select2 multiple dropdown in send email/Sms in Class tab
    $("#selectSectionss").select2();
    $("#checkbox_section").click(function () {
        if ($("#checkbox_section").is(':checked')) {
            $("#selectSectionss > option").prop("selected", "selected");
            $("#selectSectionss").trigger("change");
        } else {
            $("#selectSectionss > option").removeAttr("selected");
            $("#selectSectionss").trigger("change");
        }
    });

</script>

 <script>


    $('.close_modal').on('click', function() {
        $('.custom_notification').removeClass('open_notification');
    });
    $('.notification_icon').on('click', function() {
        $('.custom_notification').addClass('open_notification');
    });
    $(document).click(function(event) {
        if (!$(event.target).closest(".custom_notification").length) {
            $("body").find(".custom_notification").removeClass("open_notification");
        }
    });



</script>
<script src="{{asset('public/backEnd/')}}/js/search.js"></script>
{{-- <script src="{{asset('public/landing/js/toastr.js')}}"></script> --}}
{!! Toastr::message() !!}

{{-- <script src="{{asset('Modules/Saas/Resources/assets/saas/')}}/js/main.js"></script> --}}
{{-- <script src="{{asset('Modules/Saas/Resources/assets/saas/')}}/js/saas.js"></script> --}}
{{-- <script src="{{asset('Modules/Saas/Resources/assets/saas/')}}/js/developer.js"></script>
<script src="{{asset('Modules/Saas/Resources/assets/saas/')}}/js/search.js"></script> --}}
@yield('script')

</body>
</html>

