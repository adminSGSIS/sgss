@if (isset($docs_type))
    {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'docs/type/edit/' . $docs_type->id, 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
@else
    {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'docs/type/new', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
@endif
<input type="hidden" name="school_id" value="{{ auth()->user()->school_id }}">
<div class="col-lg-12 mb-5">
    <div class="input-effect">
        <input class="primary-input form-control has-content" type="text" name="title" autocomplete="off" required
            value="@if (isset($docs_type)) {{ $docs_type->title }} @endif">
        <label>Docs Type Title<span></span> </label>
        <span class="focus-border textarea"></span>
    </div>
</div>

@if (isset($docs_type))
    <button class="primary-btn fix-gr-bg" type="submit">apply</button>
    {{ Form::close() }}
    <a href="/docs/type/delete/{{ $docs_type->id }}">
        <button class="primary-btn fix-gr-bg" style="position: absolute; right: 10%; bottom: 19%">
            delete
        </button>
    </a>
@else
    <button class="primary-btn fix-gr-bg mt-3" style="margin-left: 150px" type="submit">Apply</button>
    {{ Form::close() }}
@endif
