@extends('docs::layouts.document_layout')
@section('css')
<style>
   ::-webkit-scrollbar-track{
        box-shadow: none !important;
    }
</style>
@endsection
@section('mainContent')

<h1 style="font-size: 60px" class="mb-3">{{$docsType->title}}</h1>

@foreach ($docs as $item)
    <h1 style="font-weight: 400;">
        <a href="#{{$item->id}}-docs-type" style="color: #415094">
            # {{$item->title}}
        </a>
    </h1>
@endforeach

<p class="mt-4"></p>
@foreach ($docs as $item)
    <div class="mb-4">
        <h1 style="font-size: 30px" id="{{$item->id}}-docs-type"># {{$item->title}}</h1>
        <div class="editScrollbar" style="width: 90%; overflow-x: scroll">{!! $item->content !!}</div>
        @if ($item->file)
            <p class="mt-3">Additional file : <a href="{{asset($item->file)}}" download>{{$item->file_name}}</a></p>
        @endif
    </div>
@endforeach

@endsection