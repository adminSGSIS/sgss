<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('docs')->middleware('auth')->group(function () {
    Route::get('type', 'DocsTypeController@index');
    Route::get('type/new', 'DocsTypeController@create');
    Route::post('type/new', 'DocsTypeController@store');
    Route::get('type/edit/{docs_type}', 'DocsTypeController@edit');
    Route::post('type/edit/{docs_type}', 'DocsTypeController@update');
    Route::get('type/delete/{id}', 'DocsTypeController@destroy');

    Route::get('getStudent', 'DocsController@getStudent');
    Route::get('pinToTop/{docs}', 'DocsController@pinToTop');
    Route::get('unpinFromTop/{docs}', 'DocsController@unpinFromTop');
    Route::get('/', 'DocsController@index');
    Route::post('/', 'DocsController@store');
    Route::get('view/{docs}', 'DocsController@show');
    Route::get('edit/{editData}', 'DocsController@edit');
    Route::post('edit/{docs}', 'DocsController@update');
    Route::get('delete-view/{id}', 'DocsController@deleteView');
    Route::get('delete/{docs}', 'DocsController@destroy');
    Route::get('changeApproveStatus', 'DocsController@changeApproveStatus');
});
Route::prefix('document')->group(function () {
    Route::get('/', 'DocumentController@document');
    Route::get('stf/{id}', 'DocumentController@RenderStaffDocs');
    Route::get('std/y/{id}', 'DocumentController@RenderStudentPublicDocs');
    Route::get('std/n/{id}', 'DocumentController@RenderStudentPrivateDocs');
    Route::get('files', 'DocumentController@allFiles');
    Route::get('it/app', 'DocumentController@DocsIT_app');
});
