<?php

namespace Modules\Event\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Event\Entities\EventParticipation;
use App\SmEvent;
use Mail;
use Barryvdh\DomPDF\Facade as PDF;
use DateTime;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('event::summer-camp');
    }

    public function SummerCamp()
    {
        return view('event::summer-camp');
    }
    public function WinterCamp()
    {
        return view('event::winter-camp');
    }


    public function SummerCampRegister(Request $request)
    {
       
        $new = new EventParticipation();
        $new->id_event = 6;
        $new->name = $request->p_name;
        $new->phone = $request->p_phone;
        $new->email = $request->p_email;
        $new->address = $request->p_bus_address;

        $new->first_child = $request->f_child_name;
        $new->first_child_dob =  DateTime::createFromFormat('d-m-Y',$request->f_child_dob)->format('Y/m/d');
        if($request->s_child_name != null)
        {
            $new->second_child = $request->s_child_name;
            $new->second_child_dob = DateTime::createFromFormat('d-m-Y',$request->s_child_dob)->format('Y/m/d');
        }
        if($request->t_child_name)
        {
            $new->third_child = $request->t_child_name;
            $new->third_child_dob = DateTime::createFromFormat('d-m-Y',$request->t_child_dob)->format('Y/m/d');
        }
        

        $week = null;
        if(isset($request->all_week))
        {
            $week="Week1, Week2, Week3, Week4, Week5";
        }
        else{
            foreach ($request->week as $key => $value) {
                $week =$week.", ".$value;
            }
            $week = substr($week, 2);
        }
        

        
        $new->participate_in = $week;
        $new->save();

        $data['parent_name']=$request->p_name;
        $data['parent_phone']=$request->p_phone;
        $data['parent_email']=$request->p_email;
        $data['parent_adress']=$request->p_bus_address;
        $data['event'] = "Summer Camp";

        $data['child_1']=$request->f_child_name;
        $data['child_1_dob']=$request->f_child_dob;
        $data['child_2']=$request->s_child_name;
        $data['child_2_dob']=$request->s_child_dob;
        $data['child_3']=$request->t_child_name;
        $data['child_3_dob']=$request->t_child_dob;
        $data['participate_in']=$week;

        Mail::send('event::parent-notice', compact('data'), function ($message) use($data){
                $message->to($data['parent_email'])->subject('Saigon Star Summer Camp');
        });
        Mail::send('event::admissions-notice', compact('data'), function ($message) use($data){
                $message->to('duonghuynh@sgstar.edu.vn')->subject('Saigon Star Summer Camp Registration Notice');
        });

        return redirect()->back()->with('success', 'register completed, please check your email');   
    }
    public function WinterCampRegister(Request $request)
    {
        $new = new EventParticipation();
        $new->id_event = 8;
        $new->name = $request->p_name;
        $new->phone = $request->p_phone;
        $new->email = $request->p_email;
        $new->address = $request->p_bus_address;

        $new->first_child = $request->f_child_name;
        $new->first_child_dob =  DateTime::createFromFormat('d-m-Y',$request->f_child_dob)->format('Y/m/d');
      
        $new->second_child = $request->s_child_name;
        $new->second_child_dob = DateTime::createFromFormat('d-m-Y',$request->s_child_dob)->format('Y/m/d');

        $new->third_child = $request->t_child_name;
        $new->third_child_dob = DateTime::createFromFormat('d-m-Y',$request->t_child_dob)->format('Y/m/d');;

        $week = null;
        if(isset($request->all_week))
        {
            $week="Week1, Week2, Week3, Week4, Week5";
        }
        else{
            foreach ($request->week as $key => $value) {
                $week =$week.", ".$value;
            }
            $week = substr($week, 2);
        }
        
        $new->participate_in = $week;

        $new->save();

        $data['parent_name']=$request->p_name;
        $data['parent_phone']=$request->p_phone;
        $data['parent_email']=$request->p_email;
        $data['parent_adress']=$request->p_bus_address;
        $data['event'] = "Winter Camp";

        $data['child_1']=$request->f_child_name;
        $data['child_1_dob']=$request->f_child_dob;
        $data['child_2']=$request->s_child_name;
        $data['child_2_dob']=$request->s_child_dob;
        $data['child_3']=$request->t_child_name;
        $data['child_3_dob']=$request->t_child_dob;
        $data['participate_in']=$week;

        Mail::send('event::parent-notice', compact('data'), function ($message) use($data){
                $message->to($data['parent_email'])->subject('Saigon Star Winter Camp');
        });
        Mail::send('event::admissions-notice', compact('data'), function ($message) use($data){
                $message->to('anhcao@sgstar.edu.vn')->subject('Saigon Star Winter Camp Registration Notice');
        });

        return redirect()->back()->with('success', 'register completed, please check your email');   
    }

    public function find()
    {
        $events = SmEvent::all();
        return view('event::list-participant',compact('events'));
    }

    public function getData (Request $request)
    {
        $selected_event = $request->id_event;
        $events = SmEvent::all();
        $participants = EventParticipation::all()->where('id_event',$request->id_event);
        return view('event::list-participant',compact('selected_event','events','participants'));
    }

    public function summerCampPDF($id)
    {
        $participants = EventParticipation::findOrFail($id);
        $title = "Summer Camp";
        $pdf = PDF::loadView('event::summer-camp-pdf',compact('participants'));
        return $pdf->stream('summer-camp-pdf.pdf');
    }
    public function winterCampPDF($id)
    {
        $participants = EventParticipation::findOrFail($id);
        $title= "Winter Camp";
        $pdf = PDF::loadView('event::summer-camp-pdf',compact('title','participants'));
        return $pdf->stream('summer-camp-pdf.pdf');
    }
}
