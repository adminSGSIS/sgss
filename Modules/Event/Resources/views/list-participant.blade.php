@extends('backEnd.master')
@section('mainContent')
<div class="white-box ">
	<form method="POST" action="{{url('event-participation')}}">
		@csrf
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="col-lg-4">

				<select required="" name="id_event" id="id_event"  class="niceSelect w-100 bb form-control" >
					<option data-display="@lang('Event name') *" value="" >@lang('Event name') *</option>
					@foreach($events as $event)
					    
					    <option label="{{@$event->event_title}}" value="{{@$event->id}}" {{isset($selected_event) && $selected_event == $event->id ? 'selected' : ''}}>{{@$event->event_title}}</option>
					   
                    @endforeach
				</select>
			
		</div>
		<div class="col-lg-4"></div>
		<div class="d-flex justify-content-end center" style="padding-top: 35px;">
	            <input type="submit" class="primary-btn small fix-gr-bg" value="search">
	    </div>
	</div>
	
    </form>
</div>
@if(isset($participants))
<br><br>
	<table id="table_id" class="display school-table dataTable no-footer dtr-inline">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Bus Address</th>
				<th>Children name / date of birth</th>
				<th>Week of participation</th>
				<th>action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($participants as $participant)
			<tr>
				<td>{{$participant->name}}</td>
				<td>{{$participant->email}}</td>
				<td>{{$participant->phone}}</td>
				<td>{{$participant->address}}</td>
				<td>
					{{$participant->first_child}} / {{ $participant->first_child_dob ? date_format(DateTime::createFromFormat('Y-m-d', $participant->first_child_dob) , 'd-m-Y'): '' }} <br>
					@if($participant->second_child != null)
					{{$participant->second_child}} / {{ $participant->second_child_dob ? date_format(DateTime::createFromFormat('Y-m-d', $participant->second_child_dob) , 'd-m-Y'): '' }} <br>
					@endif
					@if($participant->third_child != null)
					{{$participant->third_child}} / {{ $participant->third_child_dob ? date_format(DateTime::createFromFormat('Y-m-d', $participant->third_child_dob) , 'd-m-Y'): '' }} 
					@endif
				</td>
				<td>{{$participant->participate_in}}</td>
				<td>
					@if($selected_event == 6)
					<a href="{{ route('summer-pdf',$participant->id) }}" class="btn primary-btn small fix-gr-bg">Export</a>
					@elseif($selected_event == 8)
					<a href="{{ route('winter-pdf',$participant->id) }}" class="btn primary-btn small fix-gr-bg">Export</a>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endif
@endsection