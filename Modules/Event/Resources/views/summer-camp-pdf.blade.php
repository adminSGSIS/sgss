<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    <style>
        body {
            height: 100%;
            width: 100%;
            font-family: 'DejaVu Sans';
        }
    
        .letter_color {
            color: #424040d2;
        }
        .ft200 {
            font-size: 200% !important;
        }
        div>h1 {}
    
        .footer {
            bottom: 0;
            position: fixed;
            width: 100%;
        }
    
        .info {
            font-size: 170% !important;
        }
    
        .p-2>h1 {
            font-size: 100px;
        }
    
        table {
            border-collapse: collapse;
            width: 100%;
        }
    
        td,
        th {
            border: 1px solid #31b6f0;
            border-top: 1px #31b6f0 solid!important;
            text-align: left;
            padding: 8px 8px 8px 8px !important;
            font-size: 18px;
            vertical-align: top;
            color: #0D52AD;
        }
        @page {
            margin-bottom: 0;
            margin-top: 0;
        }
    
        .bold {
            font-weight: bold;
        }
    
        .ft300 {
            font-size: 18px;
            color: rgba(66, 64, 64, 0.822);
        }
    
        .fz25 {
            font-size: 22px;
            line-height: 1.3;
        }
        .w-50{
            width: 50%;
        }
    </style>
</head>
<body>
    <img src="{{ asset('public/header_letter.png') }}" alt="" width="100%" >
    <div style="padding-bottom: 60px;padding-top:30px">
        <table>
            <thead>
                <tr>
                    <td colspan="4" style="background-color: #b0dcff;text-align:center">                    
                        <h2><b>{{$title}}</b></h2>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2" class="w-50">Parent's name: <b>{{ $participants->name ?? '' }}</b></td>
                    <td colspan="2" class="w-50"> Parent's phone: <b>{{ $participants->phone ?? '' }}</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="w-50">Parent's email: <b>{{ $participants->email ?? '' }}</b></td>
                    <td colspan="2" class="w-50">Parent's buss address: <b>{{ $participants->address ?? '' }}</b></td>
                </tr>
            </tbody>
        </table>
    </div>
    <table class="table tablePDF">
        <tbody>
            <tr>
                <td colspan="2" class="w-50">
                    First child's name: <b>{{ $participants->first_child ?? '' }}</b>
                </td>
                <td colspan="2" class="w-50">
                    First child's DOB: <b>{{ $participants->first_child_dob ? date_format(DateTime::createFromFormat('Y-m-d', $participants->first_child_dob) , 'd-m-Y'): '' }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="w-50">
                    Second child's name: <b>{{ $participants->second_child ?? '' }}</b>
                </td>
                <td colspan="2" class="w-50">
                    Second child's DOB: <b>{{ $participants->second_child_dob ? date_format(DateTime::createFromFormat('Y-m-d', $participants->second_child_dob) , 'd-m-Y'): '' }}</b>
                </td>

            </tr>
    
    
            <tr>
                <td colspan="2" class="w-50">
                    Third child's name: <b>{{ $participants->third_child ?? '' }}</b>
                </td>
                <td colspan="2" class="w-50">
                    Third child's DOB: <b>{{ $participants->third_child_dob ? date_format(DateTime::createFromFormat('Y-m-d', $participants->third_child_dob) , 'd-m-Y'): '' }}</b>
                </td>
            </tr>
            
    
            <tr>
                <td colspan="4">
                    Week of participation: 
                    <b>{{ $participants->participate_in ?? '' }}</b>
                </td>
            </tr>
        </tbody>
    </table>
    <img src="{{ asset('public/footer_letter.png') }}" alt="" style="bottom: 150px;position: fixed;width: 100%;"> 
</body>
</html>

