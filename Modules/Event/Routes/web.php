<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('event')->group(function() {
    Route::get('/', 'EventController@index');
    
});

Route::get('summer-camp-register', 'EventController@SummerCamp');
Route::post('summer-camp-register', 'EventController@SummerCampRegister');
Route::get('winter-camp-register','EventController@WinterCamp');
Route::post('winter-camp-register', 'EventController@WinterCampRegister');
Route::get('winter-camp-register/pdf/{id}', 'EventController@winterCampPDF')->name('winter-pdf');

Route::get('event-participation', 'EventController@find');

Route::post('event-participation', 'EventController@getData');

Route::get('summer-camp-register/pdf/{id}', 'EventController@summerCampPDF')->name('summer-pdf');

