<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFeesList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_list', function (Blueprint $table) {
             $table->increments('id');
            $table->integer('id_student')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('discount')->nullable();
            $table->integer('amount_applied_discount')->nullable();
            $table->integer('paid')->nullable();
            $table->string('academic_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
