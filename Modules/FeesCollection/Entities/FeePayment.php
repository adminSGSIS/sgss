<?php

namespace Modules\FeesCollection\Entities;

use Illuminate\Database\Eloquent\Model;

class FeePayment extends Model
{
    protected $fillable = [];
    protected $table="fee_payment";
    
    public function proof(){
    	return $this->HasMany('Modules\FeesCollection\Entities\ProofOfPayment', 'id_payment', 'id');
    }
}
