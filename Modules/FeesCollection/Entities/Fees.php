<?php

namespace Modules\FeesCollection\Entities;

use Illuminate\Database\Eloquent\Model;

class Fees extends Model
{
    protected $fillable = ['fee_name','id_group','amount','class_id','route_id'];
    protected $table="fee";

    public function groupfee(){
    	return $this->belongsTo('Modules\FeesCollection\Entities\GroupFee', 'id_group', 'id');
    }
    
    public function class(){
    	return $this->belongsTo('App\SmClass', 'class_id', 'id');
    }
}
