<?php

namespace Modules\FeesCollection\Entities;

use Illuminate\Database\Eloquent\Model;

class FeesDiscount extends Model
{
    protected $fillable = [];
    protected $table="fee_discount";
}
