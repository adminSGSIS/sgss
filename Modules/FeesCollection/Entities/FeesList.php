<?php

namespace Modules\FeesCollection\Entities;

use Illuminate\Database\Eloquent\Model;

class FeesList extends Model
{
    protected $fillable = [];
    protected $table="fee_list";
    public function student(){
    	return $this->belongsTo('App\SmStudent', 'id_student', 'id');
    }
    public function payment(){
    	return $this->hasMany('Modules\FeesCollection\Entities\FeePayment', 'id_fee_list', 'id');
    }

}
