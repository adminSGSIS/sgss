<?php

namespace Modules\FeesCollection\Entities;

use Illuminate\Database\Eloquent\Model;

class FeesStudent extends Model
{
    protected $fillable = [];
    protected $table="fees_student";
    public function student(){
    	return $this->belongsTo('App\SmStudent', 'id_student', 'id');
    }

    public function fee(){
    	return $this->belongsTo('Modules\FeesCollection\Entities\Fees', 'id_fee', 'id');
    }

    public function feegroup(){
    	return $this->belongsTo('Modules\FeesCollection\Entities\GroupFee', 'id_fee_group', 'id');
    }
}
