<?php

namespace Modules\FeesCollection\Entities;

use Illuminate\Database\Eloquent\Model;

class GroupFee extends Model
{
    protected $fillable = ['group_name'];
    protected $table="group_fee";
}
