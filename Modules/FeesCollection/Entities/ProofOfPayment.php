<?php

namespace Modules\FeesCollection\Entities;

use Illuminate\Database\Eloquent\Model;

class ProofOfPayment extends Model
{
    protected $fillable = [];
    protected $table="payment_proof";
    
    public function payment(){
    	return $this->belongsTo('Modules\FeesCollection\Entities\FeePayment', 'id_payment', 'id');
    }
}
