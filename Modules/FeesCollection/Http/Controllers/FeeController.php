<?php

namespace Modules\FeesCollection\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\FeesCollection\Entities\Fees;
use Modules\FeesCollection\Entities\GroupFee;
use Brian2694\Toastr\Facades\Toastr;
use App\SmClass;
use Illuminate\Http\Request;

class FeeController extends Controller
{
    public function index()
    {
        try {
            $classes = SmClass::All()->sortBy('stt');
            $groupFee = GroupFee::all()->where('id','!=','1');
            $fees = Fees::all()->where('id_group','!=','1');
            
            return view('feescollection::fee.index', compact('fees','groupFee','classes'));
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    public function create()
    {
        try {
            $groupFee = GroupFee::all();
            return view('feescollection::fee.create', compact('groupFee'));
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
        
    }

    public function store()
    {
        try {
            Fees::create(request()->all());
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try {
            $EditData = Fees::find($id);
            $classes = SmClass::All()->sortBy('stt');
            $groupFee = GroupFee::all()->where('id','!=','1');
            $fees = Fees::all()->where('id_group','!=','1');

            return view('feescollection::fee.index', compact('EditData','fees','groupFee','classes'));
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    public function update(Request $request)
    {
        
        try {
            $update = Fees::find($request->edit_id);
            $update->fee_name = $request->fee_name;
            $update->id_group = $request->id_group;
            $update->amount = $request->amount;
            $update->class_id = $request->class_id;
            
            $update->save();
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    public function destroy(Fees $fee)
    {
        try {
            $fee->delete();
            Toastr::success('Operation successful', 'Success');
            return redirect()->to('fee');
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }
}
