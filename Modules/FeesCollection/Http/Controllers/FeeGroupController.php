<?php

namespace Modules\FeesCollection\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\FeesCollection\Entities\GroupFee;
use Brian2694\Toastr\Facades\Toastr;

class FeeGroupController extends Controller
{
    public function index()
    {
        $groupFee = GroupFee::all();
        return view('feescollection::fee_group.index', compact('groupFee'));
    }

    public function create()
    {
        return view('feescollection::fee_group.create');
    }

    public function store()
    {
        try{
            GroupFee::create(request()->all());
            Toastr::success('Operation successful', 'Success');
            return redirect()->to('fee-group');
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    public function edit(GroupFee $groupFee)
    {
        return view('feescollection::fee_group.edit', compact('groupFee'));
    }

    public function update(GroupFee $groupFee)
    {
        try{
            $groupFee->update(request()->all());
            Toastr::success('Operation successful', 'Success');
            return redirect()->to('fee-group');
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    public function destroy(GroupFee $groupFee)
    {
        try{
            $groupFee->delete();
            Toastr::success('Operation successful', 'Success');
            return redirect()->to('fee-group');
        }catch (\Exception $e) {
            //  dd($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }
}
