<?php

namespace Modules\FeesCollection\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\FeesCollection\Entities\GroupFee;
use Modules\FeesCollection\Entities\Fees;
// use Modules\FeesCollection\Entities\FeesDiscount;
use Modules\FeesCollection\Entities\FeesList;
use Modules\FeesCollection\Entities\FeesStudent;
use Modules\FeesCollection\Entities\FeePayment;
use Modules\FeesCollection\Entities\ProofOfPayment;
use Barryvdh\DomPDF\Facade as PDF;
use App\SmStudent;
use App\YearCheck;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use App\Admission;
use DateTime;
use Mail;
use App\User;
use App\SmParent;
use App\SmAcademicYear;
use App\SmClass;

class FeesCollectionController extends Controller
{
    public function FeeGroup()
    {
        $fee_groups = GroupFee::all();
        return view('feescollection::fee-group',compact('fee_groups'));
    }

    // public function CreatePaySlip()
    // {
    //     $feegroups = GroupFee::all();
    //     $students = SmStudent::all();
    //     $fees = Fees::all();
    //     $discounts = FeesDiscount::all();
    //     return view('feescollection::payslip',compact('students','fees','discounts','feegroups'));
    // }

    public function StoreSlip (Request $request)
    {
        //dd($request);
        $total = str_replace(",","",$request->total_fee_slip);
        $amount = str_replace(",","",$request->slip_amount);
        
        $feelist = new FeesList();
        $feelist->id_student = $request->student_id;
        $feelist->amount = $amount;
        $feelist->discount = $amount - $total;
        $feelist->amount_applied_discount = $total;
        $feelist->deposit = $request->deposit;
        $feelist->academic_id = YearCheck::getAcademicId();
        $feelist ->save();

        for( $i= 0; $i < count($request->id_fees) ; $i++)
        {
            $fee_std = new FeesStudent();
            $fee_std->id_student = $request->student_id;
            $fee_std->id_fee_list = $feelist->id;
            $fee_std->id_fee_group = Fees::find($request->id_fees[$i])->id_group;
            $fee_std->id_fee = $request->id_fees[$i];
            if ($request->discount[$i] == null || $request->discount[$i] == "") {
                $fee_std->discount = 0;
            }
            else
            {
                $fee_std->discount = $request->discount[$i];
            }
            $fee_std->discount_applied = str_replace(",","",$request->amount[$i]);
            $fee_std->discount_reason = $request->discount_reason[$i];
            $fee_std->save();
        }
        if($request->options == 'option_A')
        {
            $paymentA = new FeePayment();
            $paymentA->id_fee_list = $feelist->id;
            $paymentA->payment_method = "";
            $paymentA->amount_payment = $total;
            $paymentA->payer_name = "";
            $paymentA->payer_phone = "";
            $paymentA->payer_email = "";
            $paymentA->paid = 0;
            $paymentA->due_date = Carbon::now()->year."-08-19";
            $paymentA->save();

        }
        else
        {
            $paymentB1 = new FeePayment();
            $paymentB1->id_fee_list = $feelist->id;
            $paymentB1->payment_method = "";
            $paymentB1->amount_payment = $total / 100 * 30;
            $paymentB1->payer_name = "";
            $paymentB1->payer_phone = "";
            $paymentB1->payer_email = "";
            $paymentB1->paid = 0;
            $paymentB1->due_date = Carbon::now()->year."-04-01";
            $paymentB1->save();

            $paymentB2 = new FeePayment();
            $paymentB2->id_fee_list = $feelist->id;
            $paymentB2->payment_method = "";
            $paymentB2->amount_payment = $total / 100 * 30;
            $paymentB2->payer_name = "";
            $paymentB2->payer_phone = "";
            $paymentB2->payer_email = "";
            $paymentB2->paid = 0;
            $paymentB2->due_date = Carbon::now()->year."-08-01";
            $paymentB2->save();

            $paymentB3 = new FeePayment();
            $paymentB3->id_fee_list = $feelist->id;
            $paymentB3->payment_method = "";
            $paymentB3->amount_payment = $total / 100 * 40;
            $paymentB3->payer_name = "";
            $paymentB3->payer_phone = "";
            $paymentB3->payer_email = "";
            $paymentB3->paid = 0;
            $paymentB3->due_date = Carbon::now()->year."-12-01";
            $paymentB3->save();
        }
        Toastr::success('Operation successful', 'Success');
        return $this->QuotationPdf($feelist->id);
        // return redirect('feescollection/view-slip/'.$feelist->id);
    }
    public function QuotationPdf($id){
        $slip = FeesList::find($id);
        $fees = FeesStudent::where('id_fee_list' , $id)->get();
        return view('feescollection::payment-detail',compact('slip','fees'));
    }
    public function ListSlip()
    {
        $slips = FeesList::all();
        $academics = SmAcademicYear::all();
        return view('feescollection::list-slip',compact('slips','academics'));
    }

    public function ViewSlip($id)
    {
        $slip = FeesList::find($id);
        $fees = FeesStudent::where('id_fee_list' , $id)->get();
        $payments = FeePayment::where('id_fee_list' , $id)->get();
        
        return view('feescollection::view-slip',compact('slip','fees','payments'));
    }

    public function ProceedPayment(Request $request)
    {
        $payment = FeePayment::find($request->id_payment);
        if(str_replace(",","",$request->amount_payment) > $payment->amount_payment - $payment->paid)
        {
            Toastr::error('Operation Fail', 'payment amount is not within the limits allowed');
            return redirect()->back();
        }
        $slip = FeesList::find($request->id_slip);
        $slip->paid = $slip->paid + str_replace(",","",$request->amount_payment);
        if($slip->paid > 0)
        {
            if($slip->amount_applied_discount - $slip->paid == 0)
            {
                $slip->status = 1;
            }
            if($slip->amount_applied_discount - $slip->paid > 0)
            {
                $slip->status = 2;
            }
        }
        if($slip->paid == 0)
            {
                $slip->status = 0;
            }
        $slip->save();

        
        
        if ($request->file('proof_of_payment') != "") {
            $file = $request->file('proof_of_payment');
            $proof = md5($file->getClientOriginalName()) ."-".Carbon::now()->toDateString()."." . $file->getClientOriginalExtension();
            $file->move('public/uploads/fee/', $proof);
            $proof = 'public/uploads/fee/' . $proof;
            
            
            $proofOfpayment = new ProofOfPayment();
            $proofOfpayment->name = Carbon::now()->toDateString().'-payment file';
            $proofOfpayment->payment_date = Carbon::now()->toDateString();
            $proofOfpayment->amount = str_replace(",","",$request->amount_payment);
            $proofOfpayment->payment_method = $request->payment_method;
            $proofOfpayment->id_payment = $payment->id;
            $proofOfpayment->url_of_proof = $proof;
            $proofOfpayment->save();
            
        }

        
        $payment->paid =$payment->paid + str_replace(",","",$request->amount_payment);
        
        
        
        if($payment->amount_payment -  $payment->paid == 0)
        {
           
            $payment->status = 1;
        }
        if($payment->amount_payment -  $payment->paid > 0)
        {
            
            $payment->status = 2;
        }
        $payment->save();
        Toastr::success('Operation successful', 'Success');
        return redirect()->back();
    }


    public function SearchPaySlip()
    {
        $feegroups = GroupFee::all();
        $students = SmStudent::all();
        $fees = Fees::all();
        // $discounts = FeesDiscount::all();
        return view('feescollection::payslip-search',compact('students','fees','feegroups'));
    }

    public function ShowPaySlip(Request $request)
    {
        return $this->CreatePaySlip($request->student_id);
    }
    public function CreatePaySlip($id)
    {
        $feegroups = GroupFee::all();
        $students = SmStudent::all();
        $studentSingle=SmStudent::findOrFail($id);
        $admission=Admission::where('student_id',$id)->first();
        $fees = Fees::all();
        // $discounts = FeesDiscount::all();

        $section ="";

        if(str_contains(strtolower($studentSingle->section->section_name) , 'early'))
        {
            $section='Early';
        }
        if(str_contains(strtolower($studentSingle->section->section_name) , 'primary'))
        {
            $section='Primary';
        }
       
        
        return view('feescollection::payslip',compact('section','students','fees','feegroups','studentSingle','admission'));
    }

    public function sendNotifications($id)
    {
            $id_student = FeesList::find($id)->id_student;
            $id_parent = SmStudent::find($id_student)->parent_id;
            $parent_email = User::find(SmParent::find($id_parent)->user_id)->email;
            
            
            $payment = FeePayment::where('id_fee_list' , $id)->first();
            $data['payment_date'] = $payment->due_date;
            $data['amount'] = $payment->amount_payment;
            $data['std_name'] = SmStudent::find($id_student)->full_name;
            
            Mail::send('feescollection::notificationsMail', compact('data'), function ($message) use($parent_email){
                
                //$message->to($parent_email)->subject('School payment notification');
                $message->to('duonghuynh@sgstar.edu.vn')->subject('School payment notification');

            });
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();    
    }

    public function extendDuedate(Request $request)
    {
        
        try{
            $extends = FeePayment::findOrFail($request->payment_id);
           
            $extends->due_date = DateTime::createFromFormat('d-m-Y', $request->due_date)->format('Y/m/d');
            
            $extends->save();

            Toastr::success('Operation successful', 'Success');
            return redirect()->back();    
        
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function ListInvoiceSearch (Request $request)
    {
        $slip = null;
        if($request->status == null && $request->academic == null)
        {
            $slips = FeesList::all();
        }
        if($request->status != null && $request->academic != null)
        {
            $slips = FeesList::where('academic_id',$request->academic)->where('status',$request->status)->get();
        }
        if($request->status != null && $request->academic == null)
        {
            $slips = FeesList::where('status',$request->status)->get();
        }
        if($request->status == null && $request->academic != null)
        {
            $slips = FeesList::where('academic_id',$request->academic)->get();
        }
        if($request->class !=null){
            foreach($slips as $key =>$value){
                if(!$value->student){
                    $slips->forget($key);
                }
                if(isset($value->student) && $value->student->class_id !=$request->class)
                {
                    $slips->forget($key);
                }

            }
        }
        $section_status=$request->status;
        $section_class=$request->class;
        $section_academic=$request->academic;
        $classes=SmClass::all()->sortBy('stt');
        $academics = SmAcademicYear::all();
        return view('feescollection::list-slip',compact('classes','slips','academics','selected_status','selected_class','selected_academic'));
    }
    public function PDF_file($id){
        $slip = FeesList::find($id);
        $fees = FeesStudent::where('id_fee_list' , $id)->get();
        $pdf = PDF::setPaper('A4','Portraits');
        $pdf = PDF::loadView('feescollection::quotation-pdf',compact('slip','fees'));
        return $pdf->download('summer-camp-pdf.pdf');
    }
    public function PDF_detail($id){
        $slip = FeesList::find($id);
        $fees = FeesStudent::where('id_fee_list' , $id)->get();
        $pdf = PDF::setPaper('A4','Portraits');
        $pdf = PDF::loadView('feescollection::quotation-pdf',compact('slip','fees'));
        return $pdf->stream('pdf.summer-camp');
    }
    
}
