@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>FEE</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="{{url('fee/create')}}">New fee</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <form action="{{ url('fee/update').'/'.$fee->id }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h4>Update Fee</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-30 mt-3">
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" required 
                                    name="fee_name" value="{{ $fee->fee_name }}" placeholder="Fee Name *">
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="number" required 
                                    name="amount" value="{{ $fee->amount }}" placeholder="Fee Amount *">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-3">
                            <div class="col-12">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('id_group') ? ' is-invalid' : '' }}" 
                                        name="id_group">
                                        <option data-display="Fee Group *" value="">Fee Group *</option>
                                        @foreach($groupFee as $value)
                                            <option value="{{$value->id}}" {{ $fee->id_group == $value->id ? 'selected': '' }}>
                                                {{$value->group_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('id_group'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('id_group') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-40">
                        <div class="col-lg-12 text-center">
                            <button class="primary-btn fix-gr-bg">
                                <span class="ti-check"></span>
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection
