@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>FEE</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">List</h3>
                        </div>
                    </div>
                </div>
                @if(isset($EditData))
                <form action="{{ url('fee/update') }}" method="POST">
                    <input type="hidden" name="edit_id" value="{{$EditData->id}}">
                @else
                <form action="{{ url('fee') }}" method="POST">
                @endif
                @csrf
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 ">
                        <div class="white-box mt-30">
                             
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h4>Create Fee</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-30 mt-3">
                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" required 
                                    name="fee_name" value="{{ isset($EditData) ? $EditData->fee_name : '' }}" placeholder="Fee Name *">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        <br>
                        <div class="col-12 mt-30">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="number" required 
                                    name="amount" value="{{ isset($EditData) ? $EditData->amount : '' }}" placeholder="Fee Amount *">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-3">
                            <div class="col-12">
                                
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select id="select_group" class="niceSelect w-100 bb form-control{{ $errors->has('id_group') ? ' is-invalid' : '' }}" 
                                        name="id_group">
                                        <option data-display="Fee Group *" value="">Fee Group *</option>
                                        @foreach($groupFee as $value)
                                            <option value="{{$value->id}}" {{ isset($EditData) && $EditData->groupfee->id == $value->id ? 'selected' : '' }}>
                                                {{$value->group_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('id_group'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('id_group') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                       
                        <div id="class_select" class="row mb-40 mt-3" style="display:none;">
                            <div class="col-12">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select id="select_class" class="niceSelect w-100 bb form-control{{ $errors->has('id_group') ? ' is-invalid' : '' }}" 
                                        name="class_id">
                                        <option data-display="Class *" value="">Class *</option>
                                        @foreach($classes as $value)
                                            <option value="{{$value->id}}" {{ isset($EditData) && $EditData->id_group == 2 && $EditData->class->id == $value->id ? 'selected' : '' }}>
                                                {{$value->class_name}}
                                            </option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row mt-40">
                        <div class="col-lg-12 text-center">
                            <button class="primary-btn fix-gr-bg">
                                <span class="ti-check"></span>
                                Save
                            </button>
                        </div>
                    </div>
                    </div>
                
                </form>        
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>Fee name</th>
                                    <th>Amount</th>
                                    <th>Group name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($fees as $value)
                                    <tr class="centerTd">
                                        <td>{{ $value->fee_name }}</td>
                                        <td>{{ number_format($value->amount) }}</td>
                                        <td>{{ $value->groupfee->group_name }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="/fee/edit/{{ $value->id }}">edit</a>
                                                    <a class="dropdown-item" href="/fee/delete/{{ $value->id }}">delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
<script>
    $( document ).ready(function() {
        if($('#select_group').val() == 2)
        {
            $('#class_select').show();
            $('#select_class').attr('required','required');
        }
        else{
            $('#class_select').hide();
            $('#select_class').removeAttr('required');
            $('#select_class option:selected').val('');
        }
    });
    $('#select_group').on('change',function(){
        if($(this).val() == 2)
        {
            $('#class_select').show();
            $('#select_class').attr('required','required');
        }
        else{
            $('#class_select').hide();
            $('#select_class').removeAttr('required');
            $('#select_class option:selected').val('');
        }
    });
</script>
@endsection