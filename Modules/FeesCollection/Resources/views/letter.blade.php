<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
<div class="pdf" style="position: relative">
        <img src="{{ asset('public/header_letter.png') }}" width="100%">
        <div style="padding-top:10px" class="letter_color">
            <div class="letter">
                <div style="padding-bottom: 55px;padding-top:25px">
                    <div class="logan">
                        <div>
                            <img src="{{asset('Modules/FeesCollection/public/Letter_Header.png') }}" alt="" width="100%">
                        </div>                        
                        <div class="row fz25 letter_header">
                            <h2>WELCOME LETTER</h2>        
                        </div>
                        {{-- <div class="left-icon">
                            <img src="{{ asset('public/tmp_image/left_icon.png') }}" alt="" width="22%">
                        </div>
                        
                        <div class="right-icon">
                            <img src="{{ asset('public/tmp_image/right_icon.png') }}" alt="" width="22%">
                        </div> --}}
                    </div>
                </div>
                
                <p>Dear parent of {{$slip->student->full_name}},</p>
                <br>
                Welcome to Saigon Star International School; a boutique international school with a real sense of family 
                at its heart. Having been at Saigon Star since 2014, I feel incredibly proud of the journey we have been 
                on together; with every single member of our community contributing to our growth, development and 
                improvement, establishing us as a diverse, inclusive and welcoming school that is fully focused on learning 
                and enrichment. <br><br>
                We are proud to be the first and only international school that is accredited with the International Primary 
                Curriculum (IPC) in Vietnam; a curriculum which we believe helps us to achieve our Shared Vision, embrace 
                our community’s wide diversity and foster a love for lifelong learning. If you haven’t already, we would 
                encourage you to come and enjoy a visit to our peaceful, green and tranquil surroundings of Thu Duc City, 
                HCMC, and see first-hand the tangible development of our learner’s intrinsic values, balanced with their 
                academic goals. Our caring, supportive and internationally qualified teachers demonstrate their capacity 
                to provide more personalized learning and a strong commitment to ensuring your child is healthy, happy 
                and learning focused. <br><br>
                At Saigon Star, we supplement the IEYC (International Early Years Curriculum), the IPC with the English 
                National Curriculum. The IEYC is integrated with components of the child-centered approach, and we 
                provide specialist teachers with expertise in Sports, Music, Performing Arts and Languages. <br><br>
                We hope that you will join our fantastic family environment and support us in becoming the strong 
                foundation for your child on their long educational journey ahead. <br><br>
                Thank you for your interest in Saigon Star; I look forward to our strong partnership together.<br><br>

                Yours faithfully,<br><br><br> <br><br><br>

                Mr. James Quantrill
                <br>
                Headteacher
            </div>
        </div>
            <div class="footer">
                <img src="{{ asset('public/footer_letter.png') }}" width="100%">
            </div>
</div>