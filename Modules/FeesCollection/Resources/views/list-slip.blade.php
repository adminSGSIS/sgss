@extends('backEnd.master')
@section('mainContent')
<div class="col-lg-12">
        <form method="POST" action="{{url('feescollection/list-invoice-search')}}">
            @csrf
                <div class="white-box">
                    <div class="row">
                        <div class="col-lg-4 sm_mb_20 sm2_mb_20 md_mb_20">
                            <select class="niceSelect w-100 bb" name="status"> 
                                <option value="" data-display="Select Status">Select Status</option>
                                <option value="" data-display="Select Status">All</option>
                                <option value="0" {{isset($selected_status) && $selected_status == "0" ? 'selected':''}}>Unpaid</option>
                                <option value="1" {{isset($selected_status) && $selected_status == "1" ? 'selected':''}}>Paid</option>
                                <option value="2" {{isset($selected_status) && $selected_status == "2" ? 'selected':''}}>Deposit</option>
                            </select>
                    	</div>
                    	<div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <select class="niceSelect w-100 bb" name="class">
                                <option value="" data-display="Select Class">Select Class</option>
                                <option value="" data-display="Select Class">All</option>
                                @foreach($classes as $class)
                                <option value="{{$class->id}}" {{isset($selected_class) && $selected_class == $class->id ? 'selected':''}}>{{$class->class_name}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <select class="niceSelect w-100 bb" name="academic">
                                <option value="" data-display="Select Academic Year">Select Academic Year</option>
                                <option value="" data-display="Select Academic Year">All</option>
                                @foreach($academics as $academic)
                                <option value="{{$academic->id}}" {{isset($selected_academic) && $selected_academic == $academic->id ? 'selected':''}}>{{$academic->year}}[{{$academic->title}}]</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 mt-20 text-right">
                            <button type="submit" class="primary-btn small fix-gr-bg" id="btnsubmit">
                                <span class="ti-search pr-2"></span>
                                Search
                            </button>
                        </div>
                    </div>
                    </div>
        </form>
                </div>
        
<br><br>
<table id="table_id" class="display school-table dataTable no-footer dtr-inline">
	<thead>
		<tr>
			<th>Student name</th>
			<th>amount(vnd)</th>
			<th>discount(vnd)</th>
			<th>total(vnd)</th>
			<th>payment status</th>
			<th>Next due date</th>
			<th>action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($slips as $slip)
		<tr role="row">
			<td>{{$slip->student->full_name}}</td>
			<td>{{number_format($slip->amount)}}</td>
			<td>{{number_format($slip->discount)}}</td>
			<td>{{number_format($slip->amount_applied_discount)}}</td>
			<td>
				@foreach($slip->payment as $payment)
				@if($payment->status == 1)
				<button class="btn btn-success">Paid</button>
				@elseif($payment->status == 0)
				<button class="btn btn-danger" style="font-size: 13px;">Unpaid</button>
				@elseif($payment->status == 2)
				<button class="btn btn-warning" style="font-size: 13px;">Deposit</button>
				@endif
				@endforeach
			</td>
			<td>

				@foreach($slip->payment as $payment)
				@if($payment->status == 0 || $payment->status == 2)
					<?php 
					$date = Carbon\Carbon::parse($payment->due_date);
					$now  = Carbon\Carbon::now();
					
					?>
					@if($date->diffInDays($now) >= 21)
					    @if($date->gte($now) == true)
					    <button class="btn btn-success" style="font-size: 13px;">{{date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')}}</button>
					    @else
					    <button class="btn btn-warning" style="font-size: 13px;">{{date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')}}</button>
					    @endif
					@elseif($date->diffInDays($now) >= 7 && $date->diffInDays($now) <=21 )
					    @if($date->gte($now) == true)
					    <button class="btn btn-warning" style="font-size: 13px;">{{date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')}}</button>
					    @else
					    <button class="btn btn-warning" style="font-size: 13px;">{{date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')}}</button>
					    @endif
					
					@elseif(($date->diffInDays($now) <= 7))
					    @if($date->gte($now) == true)
					    <button class="btn btn-danger" style="font-size: 13px;">{{date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')}}</button>
					    @else
					    <button class="btn btn-warning" style="font-size: 13px;">{{date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')}}</button>
					    @endif
					@endif
					@break
				@elseif($payment->status == 1)
					@continue
				@endif
				@endforeach
			</td>
			<td>
				<div class="dropdown">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                        @lang('lang.select')
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{url('feescollection/view-slip')}}/{{$slip->id}}">@lang('lang.view')</a>
                        <a class="dropdown-item" href="{{url('feescollection/send-notifications')}}/{{$slip->id}}">@lang('send notice')</a>
                    </div>
                </div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endsection