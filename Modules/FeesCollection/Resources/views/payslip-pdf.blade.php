@section('css')
    <style>
        .pdf {
            display: none;
        }
        body {
            -webkit-print-color-adjust: exact !important;
        }
        .box-title {
            background-color: #008fc5 !important;
        }
        .title {
            color: white !important;
        }
        .border {
            align-items: center;
            border: 1px solid #ced4da;
            background: white;
            height: calc(2.875rem + 2px);
            padding: .5rem 1rem;
            border-radius: .3rem;
        }
        .student-photo {
            margin-top: -105px;
        }
        .student-photo img {
            object-fit: cover;
        }
        .pdf-footer {
            border-top: 1px solid #008fc5;
            padding-top: 10px;
        }
        .last-page {
            height: 550px;
        }
        .last-page > div{
            background-image: url('/public/images/pic5.jpg');
            background-repeat: no-repeat;
            background-size: 100% 500px;
            height: 100%;
        }
        .last-page > div > img {
            width: 253px;
            margin-top: -14px;
        }
        .our-school {
            background: #008fc5;
            width: 70%;
            margin: auto;
            border-radius: 17px;
            margin-top: -240px;
            padding: 10px;
        }
        .our-school > h1 {
            color: white;
            font-size: 70px;
            text-align: center;
        }
        @page { 
            margin-bottom: 0;
            margin-top: 0; 
        }
        @media print{
            .student-details, .sms-breadcrumb, #message_box, footer, .print {
                display: none !important;
            }
            .box-title{
                display: block !important;
            }
            .pdf {
                display: block;
            }
            #add-fee{
                display: none;
            }
        }
    </style>
@endsection
<div class="pdf">
    <img src="/public/header_pdf.jpeg" width="100%">
    <div style="height: 1500px; color: #00548a">
        <div class="row justify-content-center align-items-center" style="height: 87%">
            <img src="/public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png" width="400">
        </div>
        <div class="d-flex align-items-center ml-5 pt-3" style="border-top: 1px solid #008fc5;">
            <i class="fa fa-map-marker ml-3"></i>
            <span class="ml-2">Su Hy Nhan Street, Residential Area No. 5 Thanh My Loi Ward,Thu Duc City</span>
        </div>
        <div class="d-flex align-items-center justify-content-between ml-5">
            <div class="col-3">
                <i class="fa fa-globe" aria-hidden="true"></i>
                <span>www.sgstar.edu.vn</span>
            </div>
            <div class="col-3">
                <i class="fa fa-envelope-square" aria-hidden="true"></i>
                <span>info@sgstar.edu.vn</span>
            </div>
            <div class="col-3">
                <i class="fa fa-headphones" aria-hidden="true"></i>
                <span>028374232222</span>
            </div>
            <div class="col-3">
                <i class="fa fa-phone-square" aria-hidden="true"></i>
                <span>0888006996</span>
            </div>
        </div>
    </div>


   {{--  <div class="pdf-footer d-flex justify-content-start">
        1 &nbsp; | &nbsp; SGSS APPLICATION FORM 
    </div> --}}<img src="/public/header_pdf.jpeg" width="100%">
</div>
