@extends('backEnd.master')
@section('mainContent')
    <form method="Get" action="{{ route('ShowPaySlip') }}">
@csrf
<div class="white-box">
	<h2 class="center" style="text-align: center;">Create Quotation</h2>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<select required="" name="student_id" id="student_list"  class="niceSelect w-100 bb form-control{{ @$errors->has('students') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('student name') *" value="" >@lang('student name') *</option>
					@foreach($students as $student)
					    @if(isset($add_income))
					    <option value="{{@$student->id}}"
					        {{@$add_income->student_id == @$student->id? 'selected': ''}}>{{@$student->full_name}}  |  {{@$student->admission_no}}</option>
					    @else
					    <option label="{{@$student->admission_no}}" value="{{@$student->id}}" {{old('student_list') == @$student->id? 'selected' : ''}}>{{@$student->full_name}}  |  {{@$student->admission_no}}</option>
					    @endif
                        @endforeach
				</select>
			</div>
			<div class="col-lg-3 col-md-3">
 				
			</div>
            
		</div>
        <div class="d-flex justify-content-end" style="padding-top: 35px">
            <input type="submit" class="primary-btn small fix-gr-bg" value="search">
        </div>
	</div>
</div>

<br>
</div>
</form>



@endsection
@section('script')
<script type="text/javascript">
	$('#student_list').on('change',function(){
		var id = $('#student_list option:selected').attr('label');
		$('#admission_no').val(id);
	});	
</script>
@endsection