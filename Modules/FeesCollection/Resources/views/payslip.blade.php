@extends('backEnd.master')
@section('mainContent')
<link rel="stylesheet" href="{{asset('Modules/FeesCollection/public/fee.css')}}"/>
<form method="POST" action="{{url('feescollection/store-slip')}}">
	@csrf

	<div id="pay-slip">
	   <div class="white-box">
		<h2 class="center" style="text-align: center;">Create Quotation</h2>
			<div class="container">
				<div class="row">
				<div class="col-lg-4 col-md-4">
					Name: {{ $studentSingle->full_name }}
				</div>
				<div class="col-lg-4 col-md-4">
					date of birth: {{ $studentSingle->date_of_birth }}
				</div>
				<div class="col-lg-4 col-md-4">
					Class: {{ $studentSingle->class->class_name }}
				</div>
				<div class="col-lg-4 col-md-4">
					Student ID: {{ $studentSingle->admission_no }}
				</div>
				<div class="col-lg-4 col-md-4">
					Status: {{ $studentSingle->is_trial ? 'Prospective student' : 'Current student' }}
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="white-box">
		<input type="text" value="{{ $studentSingle->id }}" name="student_id" hidden>
		<h2 class="center" style="text-align: center;">Fees</h2>
		<div class="row">
			<div class="col-lg-2 col-md-2">Title</div>
			<div class="col-lg-3 col-md-3">Fee name</div>
			<div class="col-lg-2 col-md-2">Amount (vnd)</div>
			<div class="col-lg-1 col-md-1">Discount (%)</div>
			<div class="col-lg-2 col-md-2">Reason</div>
			<div class="col-lg-2 col-md-2">Total amount (vnd)</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<h4 class="center">Registration fee</h4>
			</div>
			<div class="col-lg-3 col-md-3">
				<select name="id_fees[]" id="registration"
					class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}">
					<option data-display="@lang('Select Registration fee') *" value="" >@lang('Select Registration fee') *</option>

					@foreach($fees as $fee)
					@if($fee->groupfee->id == 3)
    					<option label="{{@$fee->amount}}" value="{{@$fee->id}}"
    						{{$studentSingle->is_trial==1 && $fee->id == 4 ? 'selected' : ''}}
    						{{$studentSingle->is_trial==0 && $fee->id == 14 ? 'selected' : ''}}
    						>{{@$fee->fee_name}}
    					</option>
					@endif
					@endforeach
				</select>
				<i class="warning-validate" id="registration-validate">please select one fee*</i>
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" id="regis_amount" value="{{$studentSingle->is_trial==1 ? '46000000' : '0'}}" class="form-control primary-input number"	placeholder="amount" readonly="">				
			</div>
			
			<div class="col-lg-1 col-md-1">
				<input type="text" maxlength="2" name="discount[]" class="form-control primary-input number"
					placeholder="discount=" value="0" id="discount-registration">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input" name="discount_reason[]"
					id="discount-reason-tuition" placeholder="discount reason" value="">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input number" name="amount[]"
					placeholder="Amount" id="registration_fee" readonly="" value="{{$studentSingle->is_trial==1 ? '46000000' : '0'}}">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<h4 class="center">Tuition Fee</h4>
			</div>
			<div class="col-lg-3 col-md-3">
				<select name="id_fees[]" id="tuition"
					class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}">
					<option data-display="@lang('Select Tuition fees') *" value="" >@lang('Select Tuition fees') *</option>
					@foreach($fees as $fee)
					@if($fee->groupfee->id == 2)
					<option class="fees group_{{$fee->groupfee->id}}" label="{{@$fee->amount}}" value="{{@$fee->id}}"
						{{$studentSingle->class_id == $fee->id ? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					@endif
					@endforeach
				</select>
				<i class="warning-validate" id="tuition-validate">please select one fee*</i>
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" id="tuition_amount" class="form-control primary-input number" placeholder="amount"
					readonly="" value="0">
			</div>
			
			<div class="col-lg-1 col-md-1">
				<input type="text" maxlength="2" class="form-control primary-input" name="discount[]"
					id="discount-tuition" placeholder="Amount discount" value="0">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input" name="discount_reason[]"
					id="discount-reason-tuition" placeholder="discount reason" value="">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input number" name="amount[]"
					placeholder="Amount" id="tuition_fee" readonly="" value="0">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<h4 class="center">Bus fee</h4>
			</div>
			<div class="col-lg-3 col-md-3">
				<select name="id_fees[]" id="transport"
					class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}">
					<option data-display="@lang('Select Bus fees') *" value="" >@lang('Select Bus fees') *</option>
					@foreach($fees as $fee)
					@if($fee->groupfee->id == 1)
					<option label="{{@$fee->amount}}" value="{{@$fee->id}}"
						{{$studentSingle->route_list_id == $fee->route_id ? 'selected' : ''}}>{{str_limit(@$fee->fee_name,50)}}</option>
					@endif
					@endforeach
				</select>
				<i class="warning-validate" id="transport-validate">please select one fee*</i>
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" id="transport_amount" value="0" class="form-control primary-input number"
					placeholder="amount" readonly="">
			</div>
			<div class="col-lg-1 col-md-1">
				<input type="text" maxlength="2" class="form-control primary-input" name="discount[]"
					id="discount-transport" placeholder="Amount discount" value="0">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input" name="discount_reason[]"
					id="discount-reason-tuition" placeholder="discount reason" value="">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input number" name="amount[]"
					placeholder="Amount" id="transport_fee" readonly="" value="0">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<h4 class="center">Supplement fee</h4>
				
			</div>
			<div class="col-lg-3 col-md-3">
				
				<select name="id_fees[]" id="supplement"
					class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}">
					<option data-display="@lang('Select Supplement fees') *" value="" >@lang('Select Supplement fees') *</option>
					@foreach($fees as $fee)
					@if($fee->groupfee->id == 5)
					<option class="fees group_{{$fee->groupfee->id}}" label="{{@$fee->amount}}" value="{{@$fee->id}}"
						{{isset($section) && $section=='Early' && $fee->id == 6 ? 'selected' : ''}}
						{{isset($section) && $section=='Primary' && $fee->id == 7 ? 'selected' : ''}}
						>
						{{@$fee->fee_name}}</option>
					@endif
					@endforeach
				</select>
				<i class="warning-validate" id="supplement-validate">please select one fee*</i>
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" id="supplement_amount"  class="form-control primary-input number"
					placeholder="amount" readonly="" value="{{isset($section) && $section=='Early' ? '12815000' : ''}}
					{{isset($section) && $section=='Primary' ? '15145000' : ''}}">
			</div>
			<div class="col-lg-1 col-md-1">
				<input type="text" maxlength="2" class="form-control primary-input input-left-icon" name="discount[]"
					placeholder="discount" id="discount-supplement" value="0">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input" name="discount_reason[]"
					id="discount-reason-tuition" placeholder="discount reason" value="">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input number" name="amount[]"
					placeholder="Amount" id="supplement_fee" readonly="" value="{{isset($section) && $section=='Early' ? '12815000' : ''}}
					{{isset($section) && $section=='Primary' ? '15145000' : ''}}">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<h4 class="center">Lunch fee</h4>
			</div>
			<div class="col-lg-3 col-md-3">
				<select name="id_fees[]" id="lunch"
					class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}">
					<option data-display="@lang('Select Lunch fee') *" value="" >@lang('Select Lunch fee') *</option>
					@foreach($fees as $fee)
					@if($fee->groupfee->id == 7)
					<option label="{{@$fee->amount}}" value="{{@$fee->id}}"
						{{isset($studentSingle) && $studentSingle->lunch_request == 1 && $fee->id == 5 ? 'selected' : ''}}
						{{isset($studentSingle) && $studentSingle->lunch_request == 0 && $fee->id == 16 ? 'selected' : ''}}
						>{{@$fee->fee_name}}</option>
					@endif

					@endforeach
				</select>
				<i class="warning-validate" id="lunch-validate">please select one fee*</i>
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" id="lunch_amount" value="{{isset($studentSingle) && $studentSingle->lunch_request == 1 ? '29125000' : '0'}}" class="form-control primary-input number" placeholder="amount"
					readonly="">
			</div>
			<div class="col-lg-1 col-md-1">
				<input type="text" maxlength="2" class="form-control primary-input" name="discount[]"
					id="discount-lunch" placeholder="Amount discount" value="0">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input" name="discount_reason[]"
					id="discount-reason-tuition" placeholder="discount reason" value="">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input number" name="amount[]"
					placeholder="Amount" id="lunch_fee" readonly="" value="{{isset($studentSingle) && $studentSingle->lunch_request == 1 ? '29125000' : '0'}}">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<h4 class="center">EAL fee</h4>
			</div>
			<div class="col-lg-3 col-md-3">
				<select name="id_fees[]" id="eal"
					class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}">
					<option data-display="@lang('Select EAL fees') *" value="" >@lang('Select EAL fees') *</option>
					@foreach($fees as $fee)
					@if($fee->groupfee->id == 6)
					<option class="fees group_{{$fee->groupfee->id}}" label="{{@$fee->amount}}" value="{{@$fee->id}}"
						{{old('student_list') == @$fee->id? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					@endif
					@endforeach
				</select>
				<i class="warning-validate" id="eal-validate">please select one fee*</i>
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" id="eal_amount" value="0" class="form-control primary-input number" placeholder="amount"
					readonly="">
			</div>
			<div class="col-lg-1 col-md-1">
				<input type="text" maxlength="2" class="form-control primary-input" name="discount[]"
					placeholder="discount" id="discount-eal" value="0">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input" name="discount_reason[]"
					id="discount-reason-tuition" placeholder="discount reason" value="">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input number" name="amount[]"
					placeholder="Amount" id="eal_fee" readonly="" value="0">
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<h4 class="center">SEN fee</h4>
			</div>
			<div class="col-lg-3 col-md-3">
				<select name="id_fees[]" id="sen"
					class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}">
					{{-- <option data-display="@lang('Select Application fee') *" value="" >@lang('Select Application fee') *</option> --}}

					@foreach($fees as $fee)
					@if($fee->groupfee->id == 4)
					<option label="{{@$fee->amount}}" value="{{@$fee->id}}"
						{{ $fee->id == 3 ? 'selected' : ''}}
						>{{@$fee->fee_name}}
					</option>
					@endif
					@endforeach
				</select>
				<i class="warning-validate" id="sen-validate">please select one fee above*</i>
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" id="sen_amount" value="{{$studentSingle->is_trial==1 ? '2500000' : '0'}}" class="form-control primary-input number" placeholder="amount">
			</div>
			<div class="col-lg-1 col-md-1">
				<input type="text" maxlength="2" class="form-control primary-input" name="discount[]"
					placeholder="discount" value="0" id="discount-sen">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input" name="discount_reason[]"
					id="discount-reason-tuition" placeholder="discount reason" value="">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input number" name="amount[]"
					placeholder="Amount" id="sen_fee" readonly="" value="{{$studentSingle->is_trial==1 ? '2500000' : '0'}}">
			</div>
		</div>
		<br>
		<hr>
		<div class="row">
			<div class="col-lg-2 col-md-2">
				<h4 class="center">Total</h4>
			</div>
			<div class="col-lg-3 col-md-3">
			</div>
			<div class="col-lg-3 col-md-3">
			</div>
			<div class="col-lg-2 col-md-2">
			    <input type="text" class="form-control primary-input number"  name="deposit"
					placeholder="deposit (%)">
			</div>
			<div class="col-lg-2 col-md-2">
				<input type="text" class="form-control primary-input number" id="total_fee_slip" name="total_fee_slip"
					readonly="" value="0">
				<input type="hidden" name="slip_amount" id="slip_amount" value="0">
			</div>
		</div>
	</div>
	<br>
	<div class="white-box text-center">
		<h4 class="center">Payment option</h4>
		<input type="radio" name="options" value="option_A" checked="">&nbsp;&nbsp;<label>Option A</label>
		&nbsp;&nbsp;&nbsp;
		<input type="radio" name="options" value="option_B">&nbsp;&nbsp;<label>Option B</label>
		<br>
		<button id="add-fee" class="primary-btn fix-gr-bg" data-toggle="tooltip" title="" data-original-title="">
			<span class="ti-check">
				Save
			</span>
		</button>
	</div>
	</div> 
	</div>
	
</form>



@endsection
@section('script')
<script src="{{url('Modules/Form/public/js/jquery.number.min.js')}}"></script>

<script type="text/javascript">

	$(document).ready(function () {
		
		$('.number').number( true);

		$('#transport_amount').val($('#transport option:selected').attr('label'));
		$('#transport_fee').val($('#transport option:selected').attr('label'));
		$('#tuition_amount').val($('#tuition option:selected').attr('label'));
		$('#tuition_fee').val($('#tuition option:selected').attr('label'));
		calculate();
	});

	function exportPdf() {
        window.print();
    }

	function calculate() {
		regis_fee = parseInt($('#regis_amount').val());
		sen_fee = parseInt($('#sen_amount').val());
		tuition_fee = parseInt($('#tuition_amount').val());
		transport_fee = parseInt($('#transport_amount').val());
		supplement_fee = parseInt($('#supplement_amount').val());
		lunch_fee = parseInt($('#lunch_amount').val());
		eal_fee = parseInt($('#eal_amount').val());
		total1 = regis_fee + sen_fee + tuition_fee + transport_fee + supplement_fee + lunch_fee + eal_fee;
		$('#slip_amount').val(total1);

		regis_fee = parseInt($('#registration_fee').val());
		sen_fee = parseInt($('#sen_fee').val());
		tuition_fee = parseInt($('#tuition_fee').val());
		transport_fee = parseInt($('#transport_fee').val());
		supplement_fee = parseInt($('#supplement_fee').val());
		lunch_fee = parseInt($('#lunch_fee').val());
		eal_fee = parseInt($('#eal_fee').val());
		total2 = regis_fee + sen_fee + tuition_fee + transport_fee + supplement_fee + lunch_fee + eal_fee;

		$('#total_fee_slip').val(total2);


	}


	$('#tuition').on('change', function () {
		fee = parseInt($('#tuition option:selected').attr('label'));
		$('#tuition_fee').val(fee);
		$('#tuition_amount').val(fee);
		calculate();
	});

	$('#transport').on('change', function () {
		fee = parseInt($('#transport option:selected').attr('label'));
		$('#transport_fee').val(fee);
		$('#transport_amount').val(fee);
		calculate();
	});

	$('#supplement').on('change', function () {
		fee = parseInt($('#supplement option:selected').attr('label'));
		$('#supplement_fee').val(fee);
		$('#supplement_amount').val(fee);
		calculate();
	});

	$('#registration').on('change', function () {
		fee = parseInt($('#registration option:selected').attr('label'));
		$('#registration_fee').val(fee);
		$('#regis_amount').val(fee);
		calculate();
	});

	$('#sen').on('change', function () {
		fee = 0;
		$('#sen_fee').val(fee);
		$('#sen_amount').val(fee);
		calculate();
	});

	$('#lunch').on('change', function () {
		fee = parseInt($('#lunch option:selected').attr('label'));
		$('#lunch_fee').val(fee);
		$('#lunch_amount').val(fee);
		calculate();
	});

	$('#eal').on('change', function () {
		fee = parseInt($('#eal option:selected').attr('label'));

		$('#eal_fee').val(fee);
		$('#eal_amount').val(fee);
		calculate();
	});

	$('#discount-tuition').on('keyup', function () {
		discount = parseInt($(this).val());
		applied_discount = parseInt($('#tuition option:selected').attr('label')) / 100 * (100 - discount);
		$('#tuition_fee').val(applied_discount);
		calculate();
	});

	$('#discount-transport').on('keyup', function () {
		discount = parseInt($(this).val());
		applied_discount = parseInt($('#transport option:selected').attr('label')) / 100 * (100 - discount);

		$('#transport_fee').val(applied_discount);
		calculate();
	});

	$('#discount-lunch').on('keyup', function () {
		discount = parseInt($(this).val());
		applied_discount = parseInt($('#lunch option:selected').attr('label')) / 100 * (100 - discount);

		$('#lunch_fee').val(applied_discount);
		calculate();
	});

	$('#discount-eal').on('keyup', function () {
		discount = parseInt($(this).val());
		applied_discount = parseInt($('#eal option:selected').attr('label')) / 100 * (100 - discount);

		$('#eal_fee').val(applied_discount);
		calculate();
	});

	$('#discount-supplement').on('keyup', function () {
		discount = parseInt($(this).val());
		applied_discount = parseInt($('#supplement option:selected').attr('label')) / 100 * (100 - discount);

		$('#supplement_fee').val(applied_discount);
		calculate();
	});

	$('#discount-registration').on('keyup', function () {
		discount = parseInt($(this).val());
		applied_discount = parseInt($('#registration option:selected').attr('label')) / 100 * (100 - discount);

		$('#registration_fee').val(applied_discount);
		calculate();
	});
	$('#discount-sen').on('keyup', function () {
		discount = parseInt($(this).val());
		applied_discount = parseInt($('#sen_amount').val()) / 100 * (100 - discount);

		$('#sen_fee').val(applied_discount);
		calculate();
	});


	$('#sen_amount').on('keyup',function(){
		value = parseInt($(this).val());
		discount = $('#discount-sen').val();
		real_val = value/100*(100-discount);
		$('#sen_fee').val(real_val);
		calculate();
	});


	$('#add-fee').on('click',function(e){

		if($('#transport').val()=="")
		{
			$('#transport-validate').css('display','block');
			return false;
		}
		if($('#tuition').val()=="")
		{
			$('#tuition-validate').css('display','block');
			return false;
		}
		if($('#eal').val()=="")
		{
			$('#eal-validate').css('display','block');
			return false;
		}
		if($('#lunch').val()=="")
		{
			$('#lunch-validate').css('display','block');
			return false;
		}
		if($('#supplement').val()=="")
		{
			$('#supplement-validate').css('display','block');
			return false;
		}
		if($('#application').val()=="")
		{
			$('#application-validate').css('display','block');
			return false;
		}
		if($('#registration').val()=="")
		{
			$('#registration-validate').css('display','block');
			return false;
		}
	});
</script>
@endsection