@extends('backEnd.master')
@section('mainContent')
<form method="POST" action="{{url('feescollection/store-slip')}}">
@csrf
<div class="white-box">
	<h2 class="center" style="text-align: center;">Create Payment</h2>
		<div class="container">
			<div class="row">
			<div class="col-lg-6 col-md-6">
				<select required="" name="student_id" id="student_list" class="niceSelect w-100 bb form-control{{ @$errors->has('students') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('student name') *" value="" >@lang('student name') *</option>
					@foreach($students as $student)
					    @if(isset($add_income))
					    <option value="{{@$student->id}}"
					        {{@$add_income->student_id == @$student->id? 'selected': ''}}>{{@$student->full_name}}</option>
					    @else
					    <option label="{{@$student->admission_no}}" value="{{@$student->id}}" {{old('student_list') == @$student->id? 'selected' : ''}}>{{@$student->full_name}}</option>
					    @endif
					@endforeach
				</select>
			</div>
			<div class="col-lg-6 col-md-6">
				<input type="text" class="form-control primary-input input-left-icon" name="admission_no" placeholder="Admission no" id="admission_no" >
			</div>
		</div>
	</div>
</div>
<br>
<div class="white-box">
	<h2 class="center" style="text-align: center;">Fees</h2>
	<div class="row">
		<div class="col-lg-2 col-md-2">Title</div>
		<div class="col-lg-3 col-md-3">Fee name</div>
		<div class="col-lg-2 col-md-2">Amount (vnd)</div>
		<div class="col-lg-2 col-md-2">Discount (%)</div>
		<div class="col-lg-3 col-md-3">Discount Applied (vnđ)</div>
	</div>
	<div class="row">
		 <div class="col-lg-2 col-md-2">
		 	<h4 class="center">Registration fee</h4>
		 </div>
		 <div class="col-lg-3 col-md-3">
		 	<select name="id_fees[]" id="registration" class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('Registration fee') *" value="" >@lang('Registration fee') *</option>
					
					@foreach($fees as $fee)
						@if($fee->groupfee->id == 3)
					    <option label="{{@$fee->amount}}" value="{{@$fee->id}}" {{old('student_list') == @$fee->id? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					    @endif
					@endforeach
				</select>
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" id="regis_amount" value="0" class="form-control primary-input" placeholder="amount" readonly="" >
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" maxlength="2" name="discount[]"  class="form-control primary-input input-left-icon" placeholder="discount not applicable" readonly="" 
				>
		 </div>
		 <div class="col-lg-3 col-md-3">
				<input type="text" class="form-control primary-input input-left-icon" name="amount[]" placeholder="Amount" id="regis_fee" readonly="" value="{{isset($regis_fee) ? '46000000' : '0'}}">
		 </div>
	</div>
	<br>
	<div class="row">
		 <div class="col-lg-2 col-md-2">
		 	<h4 class="center">Application fee</h4>
		 </div>
		 <div class="col-lg-3 col-md-3">
		 	<select name="id_fees[]" id="application" class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('Application fee') *" value="" >@lang('Application fee') *</option>
			
					@foreach($fees as $fee)
						@if($fee->groupfee->id == 4)
					    <option label="{{@$fee->amount}}" value="{{@$fee->id}}" {{old('student_list') == @$fee->id? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					    @endif
					@endforeach
				</select>
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" id="apply_amount" value="0" class="form-control primary-input" placeholder="amount" readonly="" >
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" maxlength="2" class="form-control primary-input" name="discount[]" placeholder="discount not applicable" readonly="" 
				>
		 </div>
		 <div class="col-lg-3 col-md-3">
				<input type="text" class="form-control primary-input input-left-icon" name="amount[]" placeholder="Amount" id="apply_fee" readonly="" value="{{isset($apply_fee) ? '2500000' : '0'}}">
		 </div>
	</div>
	<br>
	<div class="row">
		 <div class="col-lg-2 col-md-2">
		 	<h4 class="center">Tuition Fee</h4>
		 </div>
		 <div class="col-lg-3 col-md-3">
		 	<select name="id_fees[]" id="tuition" class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('Tuition fees') *" value="" >@lang('Tuition fees') *</option>
					@foreach($fees as $fee)
						@if($fee->groupfee->id == 2)
					    <option class="fees group_{{$fee->groupfee->id}}" label="{{@$fee->amount}}" value="{{@$fee->id}}" {{old('student_list') == @$fee->id? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					    @endif
					@endforeach
				</select>
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" id="tuition_amount" value="0" class="form-control primary-input" placeholder="amount" readonly="" >
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" maxlength="2" class="form-control primary-input" name="discount[]" id="discount-tuition" placeholder="Amount discount"
				value="0">
		 </div>
		 <div class="col-lg-3 col-md-3">
				<input type="text" class="form-control primary-input input-left-icon" name="amount[]" placeholder="Amount" id="tuition_fee" readonly="" value="0">
		 </div>
	</div>
	<br>
	<div class="row">
		 <div class="col-lg-2 col-md-2">
		 	<h4 class="center">Transport fee</h4>
		 </div>
		 <div class="col-lg-3 col-md-3">
		 	<select name="id_fees[]" id="transport" class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('Transport fees') *" value="" >@lang('Transport fees') *</option>
					
					@foreach($fees as $fee)
						@if($fee->groupfee->id == 1)
					    <option class="fees group_{{$fee->groupfee->id}}" label="{{@$fee->amount}}" value="{{@$fee->id}}" {{old('student_list') == @$fee->id? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					    @endif
					@endforeach
				</select>
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" id="transport_amount" value="0" class="form-control primary-input" placeholder="amount" readonly="" >
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" maxlength="2" class="form-control primary-input" name="discount[]" id="discount-transport" placeholder="Amount discount" 
				value="0">
		 </div>
		 <div class="col-lg-3 col-md-3">
				<input type="text" class="form-control primary-input input-left-icon" name="amount[]" placeholder="Amount" id="transport_fee" readonly="" value="0">
		 </div>
	</div>
	<br>
	<div class="row">
		 <div class="col-lg-2 col-md-2">
		 	<h4 class="center">Supplement fee</h4>
		 </div>
		 <div class="col-lg-3 col-md-3">
		 	<select name="id_fees[]" id="supplement" class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('Supplement fees') *" value="" >@lang('Supplement fees') *</option>
					@foreach($fees as $fee)
						@if($fee->groupfee->id == 5)
					    <option class="fees group_{{$fee->groupfee->id}}" label="{{@$fee->amount}}" value="{{@$fee->id}}" {{old('student_list') == @$fee->id? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					    @endif
					@endforeach
				</select>
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" id="supplement_amount" value="0" class="form-control primary-input" placeholder="amount" readonly="" >
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" maxlength="2" class="form-control primary-input input-left-icon" name="discount[]" placeholder="discount not applicable" readonly="" 
				>
		 </div>
		 <div class="col-lg-3 col-md-3">
				<input type="text" class="form-control primary-input input-left-icon"  name="amount[]" placeholder="Amount" id="supplement_fee" readonly="" value="0">
		 </div>
	</div>
	<br>
	<div class="row">
		 <div class="col-lg-2 col-md-2">
		 	<h4 class="center">Lunch fee</h4>
		 </div>
		 <div class="col-lg-3 col-md-3">
		 	<select name="id_fees[]" id="lunch" class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('Lunch fee') *" value="" >@lang('Lunch fee') *</option>
					@foreach($fees as $fee)
						@if($fee->groupfee->id == 7)
					    <option label="{{@$fee->amount}}" value="{{@$fee->id}}" {{old('student_list') == @$fee->id? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					    @endif
					@endforeach
				</select>
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" id="lunch_amount" value="0" class="form-control primary-input" placeholder="amount" readonly="" >
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" maxlength="2" class="form-control primary-input" name="discount[]" id="discount-lunch" placeholder="Amount discount" 
				value="0">
		 </div>
		 <div class="col-lg-3 col-md-3">
				<input type="text" class="form-control primary-input input-left-icon" name="amount[]" placeholder="Amount" id="lunch_fee" readonly="" 
				value="0">
		 </div>
	</div>
	<br>
	<div class="row">
		 <div class="col-lg-2 col-md-2">
		 	<h4 class="center">EAL fee</h4>
		 </div>
		 <div class="col-lg-3 col-md-3">
		 	<select name="id_fees[]" id="eal" class="niceSelect w-100 bb form-control{{ @$errors->has('fees') ? ' is-invalid' : '' }}" >
					<option data-display="@lang('EAL fees') *" value="" >@lang('EAL fees') *</option>
					@foreach($fees as $fee)
						@if($fee->groupfee->id == 6)
					    <option class="fees group_{{$fee->groupfee->id}}" label="{{@$fee->amount}}" value="{{@$fee->id}}" {{old('student_list') == @$fee->id? 'selected' : ''}}>{{@$fee->fee_name}}</option>
					    @endif
					@endforeach
				</select>
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" id="eal_amount" value="0" class="form-control primary-input" placeholder="amount" readonly="" >
		 </div>
		 <div class="col-lg-2 col-md-2">
		 	<input type="text" maxlength="2" class="form-control primary-input" name="discount[]" placeholder="discount not applicable" readonly="" 
				>
		 </div>
		 <div class="col-lg-3 col-md-3">
				<input type="text" class="form-control primary-input input-left-icon" name="amount[]" placeholder="Amount" id="eal_fee" readonly="" 
				value="0">
		 </div>
	</div>
	<br>
	<div class="row">
		 <div class="col-lg-2 col-md-2">
		 	<h4 class="center">Total</h4>
		 </div>
		 <div class="col-lg-3 col-md-3">
		 </div>
		 <div class="col-lg-2 col-md-2">
		 </div>
		 <div class="col-lg-2 col-md-2">
		 </div>
		 <div class="col-lg-3 col-md-3">
				<input type="text" class="form-control primary-input" id="total_fee_slip" name="total_fee_slip" readonly="" 
				value="0">
				<input type="hidden" name="slip_amount" id="slip_amount" value="0">
		 </div>
	</div>
</div>
<br>
<div class="white-box text-center">
	<h4 class="center">Payment option</h4>
	<input type="radio" name="options" value="option_A" checked="">&nbsp;&nbsp;<label>Option A</label>
	&nbsp;&nbsp;&nbsp;
	<input type="radio" name="options" value="option_B">&nbsp;&nbsp;<label>Option B</label>
	<br>
	<button id="add-fee" class="primary-btn fix-gr-bg" data-toggle="tooltip" title="" data-original-title="">
            <span class="ti-check">
            	Save
            </span>
		</button>
</div>
</div>
</form>



@endsection
@section('script')
	<script src="{{url('Modules/Form/public/js/jquery.number.min.js')}}"></script>
	<script type="text/javascript">
		$( document ).ready(function() {
		    calculate();
		    $('.primary-input').number( true);
		});
		function calculate()
		{
			regis_fee = parseInt($('#regis_amount').val());
			apply_fee = parseInt($('#apply_amount').val());
			tuition_fee = parseInt($('#tuition_amount').val());
			transport_fee = parseInt($('#transport_amount').val());
			supplement_fee = parseInt($('#supplement_amount').val());
			lunch_fee = parseInt($('#lunch_amount').val());
			eal_fee = parseInt($('#eal_amount').val());
			total1 = regis_fee + apply_fee + tuition_fee + transport_fee + supplement_fee + lunch_fee + eal_fee;
			$('#slip_amount').val(total1);

			regis_fee = parseInt($('#regis_fee').val());
			apply_fee = parseInt($('#apply_fee').val());
			tuition_fee = parseInt($('#tuition_fee').val());
			transport_fee = parseInt($('#transport_fee').val());
			supplement_fee = parseInt($('#supplement_fee').val());
			lunch_fee = parseInt($('#lunch_fee').val());
			eal_fee = parseInt($('#eal_fee').val());
			total2 = regis_fee + apply_fee + tuition_fee + transport_fee + supplement_fee + lunch_fee + eal_fee;

			$('#total_fee_slip').val(total2);
		}


		$('#tuition').on('change',function(){
			fee = parseInt($('#tuition option:selected').attr('label'));
			$('#tuition_fee').val(fee);
			$('#tuition_amount').val(fee);
			calculate();
		});

		$('#transport').on('change',function(){
			fee = parseInt($('#transport option:selected').attr('label'));
			$('#transport_fee').val(fee);
			$('#transport_amount').val(fee);
			calculate();
		});

		$('#supplement').on('change',function(){
			fee = parseInt($('#supplement option:selected').attr('label'));
			$('#supplement_fee').val(fee);
			$('#supplement_amount').val(fee);
			calculate();
		});

		$('#registration').on('change',function(){
			fee = parseInt($('#registration option:selected').attr('label'));
			$('#regis_fee').val(fee);
			$('#regis_amount').val(fee);
			calculate();
		});

		$('#application').on('change',function(){
			fee = parseInt($('#application option:selected').attr('label'));
			$('#apply_fee').val(fee);
			$('#apply_amount').val(fee);
			calculate();
		});

		$('#lunch').on('change',function(){
			fee = parseInt($('#lunch option:selected').attr('label'));
			$('#lunch_fee').val(fee);
			$('#lunch_amount').val(fee);
			calculate();
		});

		$('#eal').on('change',function(){
			fee = parseInt($('#eal option:selected').attr('label'));

			$('#eal_fee').val(fee);
			$('#eal_amount').val(fee);
			calculate();
		});

		$('#discount-tuition').on('keyup',function(){
			discount = parseInt($(this).val());
			applied_discount = parseInt($('#tuition option:selected').attr('label')) / 100 * (100 - discount);
			$('#tuition_fee').val(applied_discount);
			calculate();
		});

		$('#discount-transport').on('keyup',function(){
			discount = parseInt($(this).val());
			applied_discount = parseInt($('#transport option:selected').attr('label')) / 100 * (100 - discount);
			
			$('#transport_fee').val(applied_discount);
			calculate();
		});

		$('#discount-lunch').on('keyup',function(){
			discount = parseInt($(this).val());
			applied_discount = parseInt($('#lunch option:selected').attr('label')) / 100 * (100 - discount);
			
			$('#lunch_fee').val(applied_discount);
			calculate();
		});
	</script>
@endsection