<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quotation SGSI</title>
    <link rel="stylesheet" href="{{ asset('fonts/roboto.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <script src="{{ asset('bootstrap/bootstrap.min.js') }}"></script>
</head>
<style>
    * {
        font-family: 'Roboto Condensed', sans-serif;
    }

    body {
        font-family: 'DejaVu Sans';
        /* 
        font-family: 'DejaVu Sans'; */
        font-size: 92% !important;
    }

    .font-dejavu {
        font-family: 'DejaVu Sans';
    }

    .letter_color {
        color: #424040d2;
    }

    .letter_header {
        position: absolute;
        font-size: 150%;
        margin-top: -5px;
        left: 33%;
        font-weight: bold;
    }

    .bank_color {
        color: #58595b;
    }
    .float-right{
        float: right
    }
    .logan {
        /* background-color: #31b6f0; */
        width: 100%;
        height: 15px;
        display: flex;
        position: relative justify-content: center;
        align-items: center;
        border-radius: 0px 0px 30px 30px;
        font-size: 80px;
    }

    .logan h2 {
        font-size: 200% !important;
        color: #fff;
        font-weight: bold;
    }

    .letter {
        text-align: justify;
        padding-left: 50px !important;
        padding-right: 50px !important;
        font-size: 20px;
        padding: 8px;
        line-height: 1.3;
    }

    .footer {
        bottom: 75px;
        position: fixed;
        width: 100%;
    }

    .header {
        padding: 30px;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #31b6f0;
        border-top: 1px #31b6f0 solid !important;
        text-align: center;
        padding: 8px 8px 8px 8px !important;
        /* font-size: 18px; */
        vertical-align: top;
        color: #191970
    }

    .bold {
        font-weight: bold;
    }

    .bg-header {
        background-color: #e6f2fa !important;

        -webkit-print-color-adjust: exact;

    }

    .fz25 {
        /* font-size: 22px; */
        line-height: 1.3;
    }

    .font-dejavu b {
        font-family: 'DejaVu Sans';
        font-size: 80%!important;
    }
</style>

<body>
    <div style="position: relative;page-break-after:always">
        <img src="{{ asset('public/header_letter.png') }}" width="100%">
        <div style="padding-top:5px" class="letter_color">
            <div class="letter">
                <div style="padding-bottom: 55px;padding-top:5px">
                    <div class="logan">
                        <div>
                            <img src="{{ asset('public/welcome_header.png') }}" alt="" width="100%">
                        </div>
                        <div class="row fz25 letter_header">
                            <h2>WELCOME LETTER</h2>
                        </div>
                    </div>
                </div>
                <p>Dear parent of {{$slip->student->full_name}},</p>
                <br>
                Welcome to Saigon Star International School; a boutique international school with a real sense of family
                at its heart. Having been at Saigon Star since 2014, I feel incredibly proud of the journey we have been
                on together; with every single member of our community contributing to our growth, development and
                improvement, establishing us as a diverse, inclusive and welcoming school that is fully focused on
                learning
                and enrichment. <br><br>
                We are proud to be the first and only international school that is accredited with the International
                Primary
                Curriculum (IPC) in Vietnam; a curriculum which we believe helps us to achieve our Shared Vision,
                embrace
                our community’s wide diversity and foster a love for lifelong learning. If you haven’t already, we would
                encourage you to come and enjoy a visit to our peaceful, green and tranquil surroundings of Thu Duc
                City,
                HCMC, and see first-hand the tangible development of our learner’s intrinsic values, balanced with their
                academic goals. Our caring, supportive and internationally qualified teachers demonstrate their capacity
                to provide more personalized learning and a strong commitment to ensuring your child is healthy, happy
                and learning focused. <br><br>
                At Saigon Star, we supplement the IEYC (International Early Years Curriculum), the IPC with the English
                National Curriculum. The IEYC is integrated with components of the child-centered approach, and we
                provide specialist teachers with expertise in Sports, Music, Performing Arts and Languages. <br><br>
                We hope that you will join our fantastic family environment and support us in becoming the strong
                foundation for your child on their long educational journey ahead. <br><br>
                Thank you for your interest in Saigon Star; I look forward to our strong partnership together.<br><br>

                Yours faithfully,<br><br><br> <br>

                Mr. James Quantrill
                <br>
                Headteacher
            </div>
        </div>
        <div class="footer">
            <img src="{{ asset('public/footer_letter.png') }}" width="100%">
        </div>
    </div>
    {{-- <link href="{{ asset('ro') }}" rel="stylesheet"> --}}
    <div>
        <img src="{{ asset('public/header_letter.png') }}" width="100%">

        <br>
        <div style="position:relative;">
            <div>
                <img src="{{ asset('public/quotation.png') }}" width="100%">
            </div>
            <br>
            <div class="fz25 bank-color" style="position:absolute; top:15px;left:370px;">

                Student's name: {{$slip->student->full_name}}<br>
                Parent's name: {{$slip->student->full_name}}<br>
                Class: {{$slip->student->class->class_name}}&nbsp;&nbsp;
                Year: {{date('Y')}}

            </div>
        </div>
        <br>
        <div>

            <table class="table table-bordered " style="text-align: center;color:#191970">
                <thead>
                    <tr class="bg-header">
                        <td width="25%" class="ft200 bold">TYPES OF FEE</td>
                        <td width="25%" class="ft200 bold">AMOUNT (VND)</td>
                        <td width="25%" class="ft200 bold">DISCOUNT (%)</td>
                        <td width="25%" class="ft200 bold">TOTAL AMOUNT (VND)</td>
                    </tr>
                    @foreach($fees as $fee)
                    <tr>
                        <td width="25%" class="ft200 ">{{$fee->feegroup->group_name}}</td>
                        <td width="25%" class="ft200 bold">{{number_format($fee->fee->amount)}}</td>
                        <td width="25%" class="ft200 bold">{{$fee->discount}} %</td>
                        <td width="25%" class="ft200 bold">
                            {{number_format($fee->fee->amount / 100 * (100 - $fee->discount))}}</td>
                    </tr>
                    @endforeach
                </thead>
            </table>

            <div class="row" style="margin-bottom : 20px;left:350px">
                <div class="col-lg-6 col-md-6 row bg-header"
                    style="border:1px solid #7dd3f7;padding:15px;color:#191970;width:45.5%;text-align:left!important;float:right;line-height:1.3;font-size:80%!important">
                    <div class="col-6">
                        <div class="bold" style="display: flex">
                            <div>
                                TOTAL: <div class="float-right">{{number_format($slip->amount_applied_discount)}} vnd </div> <br>
                                @if($slip->deposit != 0)
                                DEPOSIT: <div class="float-right">{{$slip->deposit}} %</div> <br>
                                DEPOSIT AMOUNT: <div class="float-right">{{number_format($slip->amount_applied_discount / 100 * $slip->deposit)}}
                                    vnd<br></div>  <br>
                                @endif
                                PAID: <div class="float-right">{{number_format($slip->paid)}} vnd <br></div><br>
                                BALANCE: <div class="float-right">{{number_format($slip->amount_applied_discount-$slip->paid)}} vnd <br></div> <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="padding-top: 85px">
            <img src="{{ asset('Modules/FeesCollection/public/quotation_banner.png') }}" width="100%">
        </div>

        <div class="row bank_color ft200">
            <div class="col-12" style="line-height:1.2">
                Payment by bank transfer or cash <br>
            </div>

            <div class="col-12 font-dejavu" style="line-height:1.2">
                Account Name: <b>SAIGON STAR INTERNATIONAL LIABILITY LIMITED COMPANY </b><br>
                Account Number (USD): <b>060 245 314011 </b><br>
                Bank Name: <b>SAIGON THUONG TIN COMMERCIAL JOINT STOCK BANK (SACOMBANK) </b><br>
                Bank Address: <b>50 TRAN NAO STR, BINH AN WARD, THU DUC CITY, HCM CITY, VIETNAM</b><br>
                SWIFT CODE: <b>SGTTVNVX</b><br>
            </div>
            <div class="col-12">
                <img src="{{ asset('Modules/FeesCollection/public/quotation_hr.png') }}" width="100%">
            </div>
            <div class="col-12 font-dejavu" style="line-height:1.2">
                Tên Tài Khoản: <b>CÔNG TY TNHH QUỐC TẾ NGÔI SAO SÀI GÒN </b><br>
                Số Tài khoản (VND): <b>060 245 300551 </b><br>
                Tên Ngân Hàng: <b>NGÂN HÀNG THƯƠNG MẠI CỔ PHẦN SÀI GÒN THƯƠNG TÍN - SACOMBANK CHI NHÁNH QUẬN 2
                </b><br>
                Địa chỉ: <b>50 TRẦN NÃO, PHƯỜNG BÌNH AN, THÀNH PHỐ THỦ ĐỨC, THÀNH PHỐ HỒ CHÍ MINH</b><br>
                Mã SWIFT: <b>SGTTVNVX</b><br><br><br><br>
            </div>
        </div>

        <div class="footer">
            <img src="{{ asset('public/footer_letter.png') }}" alt="" width="100%">
        </div>
    </div>
</body>

</html>
