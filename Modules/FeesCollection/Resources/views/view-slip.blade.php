@extends('backEnd.master')
@section('mainContent')
<style>
td{
    font-size: 15px!important;
}
</style>
<div class="row print">
    <div class="col-lg-8 col-md-6">
        <div class="main-title">
            <h3 class="mb-30">EXPORT</h3>
        </div>
    </div>
    <div class="col-lg-4 text-right">
        <a href="#" onclick="exportPdf()" class="primary-btn small fix-gr-bg">
            <span class="ti-plus pr-2"></span>
            EXPORT TO PDF
        </a>
    </div>
</div>
@include('feescollection::view-slip-pdf')
{{-- @include('view-slip-pdf') --}}
@include('feescollection::letter')
<div id="view-slip">
    <div class="white-box">
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <div class="single-meta mt-20">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Student's Name
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                {{$slip->student->full_name}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-meta">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Father’s Name
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                {{$slip->student->parents->fathers_name}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-meta">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Mobile
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                {{$slip->student->parents->fathers_mobile}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="offset-lg-2 col-lg-5 col-md-6">
                <div class="single-meta mt-20">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Class
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                {{$slip->student->className->class_name}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-meta">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Student id
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                {{$slip->student->admission_no}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-meta">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Student number
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                {{$slip->student->roll_no}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <table class="dataTable">
        <thead>
            <tr>
                <th>Type of fee</th>
                <th>Amount (vnd)</th>
                <th>Discount (%)</th>
                <th>Discount reason</th>
                <th>Total amount (vnd)</th>
            </tr>
        </thead>
        <tbody>
            @foreach($fees as $fee)
            <tr>
                <td>{{$fee->feegroup->group_name}}</td>
                <td>{{number_format($fee->fee->amount)}}</td>
                <td>{{$fee->discount}} %</td>
                <td>{{$fee->discount_reason}}</td>
                <td>{{number_format($fee->fee->amount / 100 * (100 - $fee->discount))}}</td>
            </tr>
            @endforeach
            <tr>
                <td><b>Total</b></td>
                <td></td>
                <td></td>
                <td></td>

                <td><b>{{number_format($slip->amount_applied_discount)}}</b></td>
            </tr>
        </tbody>
    </table>
    <br>
    <div class="row" style="padding:15px">
        <div class="col-lg-9 col-md-9 col-sm-0"></div>
        <div class="col-lg-3 col-md-3 col-sm-12 white-box">
            <div class="row">
                <div class="col-6">
                    Total:
                </div>
                <div class="col-6">
                    {{number_format($slip->amount_applied_discount)}} vnd
                </div>
            </div>
            @if($slip->deposit != 0)
            <div class="row">
                <div class="col-6">
                    Deposit:
                </div>
                <div class="col-6">
                    {{$slip->deposit}} %
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Deposit amount:
                </div>
                <div class="col-6">
                    {{number_format($slip->amount_applied_discount / 100 * $slip->deposit)}} vnd
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-6">
                    Paid:
                </div>
                <div class="col-6">
                    {{number_format($slip->paid)}}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-6">
                    <b>Balance: </b>
                </div>
                <div class="col-6">
                    <b>{{number_format($slip->amount_applied_discount - $slip->paid)}} vnd</b>
                </div>
            </div>
        </div>
    </div>
    <br>
    <table class="dataTable" >
			<thead>
				<tr>
					<th>Amount(vnd)</th>
					<th>Due date</th>
					<th>Paid(vnd)</th>
					<th>Balance(vnd)</th>
					<th>Payment status</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				@foreach($payments as $payment)
				<tr>
					<td>{{number_format($payment->amount_payment)}}</td>
    <td>{{date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')}}</td>
    <td>{{number_format($payment->paid)}}</td>
    <td>{{number_format($payment->amount_payment-$payment->paid)}}</td>
    <td>
        @if($payment->status == 0)
        <button class="btn btn-danger btn-sm" style="border-radius: 20px;">unpaid</button>
        @elseif($payment->status == 1)
        <button class="btn btn-success" style="border-radius: 20px;">paid</button>
        @elseif($payment->status == 2)
        <button class="btn btn-warning btn-sm" style="border-radius: 20px;">deposit</button>
        @endif
    </td>
    <td>
        <div class="dropdown">
            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                @lang('lang.select')
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                @if($payment->status == 0)
                <a class="dropdown-item" data-toggle="modal" data-target="#ProceedPayment{{$payment->id}}">Pay</a>
                @elseif($payment->status == 1)
                <a class="dropdown-item" data-toggle="modal" data-target="#ProceedPayment{{$payment->id}}">Paid</a>
                @elseif($payment->status == 2)
                <a class="dropdown-item" data-toggle="modal" data-target="#ProceedPayment{{$payment->id}}">Deposit</a>
                @endif
                <a class="dropdown-item" data-toggle="modal" data-target="#ExtendDueDate{{$payment->id}}">Extend</a>
            </div>
        </div>
    </td>
    </tr>
    </tbody>
    <div class="modal fade admin-query" id="ProceedPayment{{$payment->id}}">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{url('feescollection/proceed-payment')}}"
                        enctype="multipart/form-data">
                        @csrf
                        Amount : {{number_format($payment->amount_payment)}} vnđ<br><br>
                        Paid : {{number_format($payment->paid)}} vnđ<br><br>
                        Balance : {{number_format($payment->amount_payment-$payment->paid)}} <br><br>
                        Amount payment (vnđ) <input type="text" min="0"
                            max="{{$payment->amount_payment - $payment->paid}}"
                            class="form-control primary-input number amount_payment" name="amount_payment" required=""
                            value="0" {{$payment->status == 1 ? 'readonly' : ''}} required="">
                        <input type="hidden" name="id_payment" value="{{$payment->id}}">
                        <input type="hidden" name="id_slip" value="{{$slip->id}}">
                        <br>
                        <h4>Payment method</h4>
                        <div class="row">
                            <div class="col-3">
                                <input type="radio"name="payment_method"value="cash" checked="">&nbsp;<label>Cash</label></div>
                            <div class="col-3">
                                <input type="radio"name="payment_method"value="cheque">&nbsp;<label>Cheque</label></div>
                            <div class="col-3">
                                <input type="radio"name="payment_method"value="bank transfer">&nbsp;<label>Bank tranfer</label></div>
                            <div class="col-3">
                                <input type="radio"name="payment_method"value="credit card">&nbsp;<label>Credit card</label></div>
                        </div>
                        <br>
                        <h4>Select proof of payment</h4>
                        <input type="file" name="proof_of_payment" required=""><br><br>

                        @if(count($payment->proof) > 0)
                        <h4>Payment history</h4>
                        <div class="row">
                            <div class="col-3">
                                Date
                            </div>
                            <div class="col-3">
                                Amount
                            </div>
                            <div class="col-3">
                                Payment method
                            </div>
                            <div class="col-3">
                                Proof
                            </div>
                        </div>
                        
                        @foreach($payment->proof as $proof)
                            <div class="row">
                                <div class="col-3">
                                    {{date_format(DateTime::createFromFormat('Y-m-d', $proof->payment_date) , 'd-m-Y')}}
                                </div>
                                <div class="col-3">
                                    {{number_format($proof->amount)}}vnd 
                                </div>
                                <div class="col-3">
                                    {{$proof->payment_method}} 
                                </div>
                                <div class="col-3">
                                    <a href="{{url('')}}/{{$proof->url_of_proof}}" download>view proof</a><br>
                                </div>
                            </div>
                             
                            
                            
                                
                        @endforeach
                        @endif
                        @if($payment->amount_payment - $payment->paid > 0)
                        <br>
                        <button class="btn btn-primary">Save</button>
                        @endif
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="modal fade admin-query" id="ExtendDueDate{{$payment->id}}">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Extend due date</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <form method="POST" action="{{url('feescollection/extend-duedate')}}">
                        @csrf
                        <div class="text-center">
                            <input type="hidden" name="payment_id" value="{{$payment->id}}">
                            <h4>Old due date :
                                {{date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')}}</h4>
                            <h4>New due date :</h4><input type="text" id="duedate" class="form-control" name="due_date"
                                placeholder="dd-mm-yyyy" required="" autocomplete="off">
                        </div>
                        <div class="mt-40 d-flex justify-content-between">
                            <button type="button" class="primary-btn tr-bg"
                                data-dismiss="modal">@lang('Cancel')</button>
                            <input type="hidden" name="id" value="" id="ncome_id">
                            <button class="primary-btn fix-gr-bg" type="submit">@lang('Submit')</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    @endforeach
    </table>
</div>

@endsection
@section('script')
<script src="{{url('Modules/Form/public/js/jquery.number.min.js')}}"></script>
<script type="text/javascript">
    function exportPdf() {
        window.print();
    }
    $(function () {
        $("#duedate").datepicker({
            format: "dd-mm-yyyy"
        }).val();
    });
    $('.number').number(true);
</script>

@endsection