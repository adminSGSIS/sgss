<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('feescollection')->group(function() {
    Route::get('/', 'FeesCollectionController@index');
    Route::get('fee-group','FeesCollectionController@FeeGroup');
    Route::get('pay-slip','FeesCollectionController@CreatePaySlip');
    Route::post('store-slip','FeesCollectionController@StoreSlip');
    Route::get('list-invoice','FeesCollectionController@ListSlip');
    Route::get('view-slip/{id}','FeesCollectionController@ViewSlip');
    Route::post('proceed-payment','FeesCollectionController@ProceedPayment');
    Route::get('download-pdf/{id}','FeesCollectionController@PDF_file');
    Route::get('detail-pdf/{id}','FeesCollectionController@PDF_detail')->name('pdf_detail');

    // Route::get('pay-slip','FeesCollectionController@CreatePaySlip')->name("pay-slip");
	Route::Get('pay-slip-search','FeesCollectionController@SearchPaySlip');
	Route::Get('ShowPaySlip','FeesCollectionController@ShowPaySlip')->name("ShowPaySlip");

	Route::get('send-notifications/{id}','FeesCollectionController@sendNotifications');

	Route::post('extend-duedate','FeesCollectionController@extendDuedate');
	
	Route::post('list-invoice-search','FeesCollectionController@ListInvoiceSearch');
});

Route::prefix('fee-group')->group(function () {
    Route::get('/', 'FeeGroupController@index');
    Route::get('create', 'FeeGroupController@create');
    Route::post('/', 'FeeGroupController@store');
    Route::get('edit/{groupFee}', 'FeeGroupController@edit');
    Route::post('update/{groupFee}', 'FeeGroupController@update');
    Route::get('delete/{groupFee}', 'FeeGroupController@destroy');
});

Route::prefix('fee')->group(function () {
    Route::get('/', 'FeeController@index');
    Route::get('create', 'FeeController@create');
    Route::post('/', 'FeeController@store');
    Route::get('edit/{id}', 'FeeController@edit');
    Route::post('update', 'FeeController@update');
    Route::get('delete/{fee}', 'FeeController@destroy');
});
