<?php

namespace Modules\Form\Entities;

use Illuminate\Database\Eloquent\Model;

class Enrolment extends Model
{
	protected $table = 'admissions';
}
