<?php

namespace Modules\Form\Http\Controllers;

use App\FeesGroup;
use App\SmAcademicYear;
use App\SmBaseSetup;
use App\SmClass;
use App\SmParent;
use App\SmPaymentMethhod;
use App\SmStudent;
use App\User;
use App\YearCheck;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use App\SmRoute;
use Modules\Form\Entities\Enrolment;
use Modules\Form\Entities\Assessment;
use App\SmSection;
use App\SmGeneralSettings;
use File;
use Image;
use Illuminate\Support\Facades\Mail;
use App\SmClassSection;
use App\SmStaff;
use Carbon\Carbon;
use Brian2694\Toastr\Facades\Toastr;


class FormController extends Controller
{
    public function enrolmentform()
    {
        $classes = SmClass::all()->sortBy('stt');
        $transports = SmRoute::all();
        $parents = SmParent::all();
        $teachers = SmStaff::where('role_id',4)->get();
        return view('form::enrolment-form',compact('parents','transports','classes','teachers'));
    }

    public function storeEnrolment(Request $request)
    {
        $_count = SmStudent::count();
        $parent_id = '';
        $parent_mail = "";
        if($request->father_email != "")
            {
                $parent_mail = $request->father_email;
            }
            else{
                $parent_mail = $request->morther_email;
            }
        if($request->parent_selected == "") // nếu không select family có sẵn nào thì tạo family mới
        {
            $parent_user = new User();
            if($request->father_name != "")
            {
                $parent_user->full_name = $request->father_name;
                $parent_user->username = $request->father_email;
                $parent_user->email = $request->father_email;
                
            }
            else{
                    $parent_user->full_name = $request->mother_name;
                    $parent_user->username = $request->morther_email;
                    $parent_user->email = $request->morther_email;
                   
                
            }
            $parent_user->password = Hash::make(123456);
            $parent_user->role_id = 3;
            $parent_user->save();
            
            $parent = new SmParent();
            if($request->father_english!="")
            {
                $parent->fathers_language_level = $request->father_english;
            }
            else{
                $parent->fathers_language_level = $request->father_english1;
            }
            $parent->fathers_name = $request->father_name;
            $parent->father_role_is = $request->family_1_radio;
            $parent->fathers_nationality = $request->father_nationality;
            $parent->fathers_occupation = $request->father_occupation;
            $parent->fathers_company = $request->father_copany;
            $parent->fathers_work_address = $request->father_work_address;
            $parent->fathers_mobile = $request->father_phone;
            $parent->fathers_email = $request->father_email;
            $parent->fathers_first_language = $request->father_language;
            

            $parent->mothers_name = $request->mother_name;
            $parent->mother_role_is = $request->family_2_radio;
            $parent->mothers_nationality = $request->mother_nationality;
            $parent->mothers_occupation = $request->mother_occupation;
            $parent->mothers_company = $request->mother_copany;
            $parent->mothers_work_address = $request->mother_work_address;
            $parent->mothers_mobile = $request->mother_phone;
            $parent->mothers_email = $request->mother_email;
            $parent->mothers_first_language = $request->mother_language;
            if($request->mother_english!="")
            {
                $parent->mothers_language_level = $request->mother_english;
            }
            else{
                $parent->mothers_language_level = $request->mother_english1;
            }

            $parent->user_id = $parent_user->id;
            $parent->save();
            $parent_id = $parent->id;
        }
        else{ 
            $parent_id = $request->parent_selected;
            $parent = SmParent::find($parent_id);
            if($request->father_english!="")
            {
                $parent->fathers_language_level = $request->father_english;
            }
            else{
                $parent->fathers_language_level = $request->father_english1;
            }
            $parent->father_role_is = $request->family_1_radio;
            $parent->fathers_name = $request->father_name;
            $parent->fathers_nationality = $request->father_nationality;
            $parent->fathers_occupation = $request->father_occupation;
            $parent->fathers_company = $request->father_copany;
            $parent->fathers_work_address = $request->father_work_address;
            $parent->fathers_mobile = $request->father_phone;
            $parent->fathers_email = $request->father_email;
            $parent->fathers_first_language = $request->father_language;
            
            $parent->mother_role_is = $request->family_2_radio;
            $parent->mothers_name = $request->mother_name;
            $parent->mothers_nationality = $request->mother_nationality;
            $parent->mothers_occupation = $request->mother_occupation;
            $parent->mothers_company = $request->mother_copany;
            $parent->mothers_work_address = $request->mother_work_address;
            $parent->mothers_mobile = $request->mother_phone;
            $parent->mothers_email = $request->mother_email;
            $parent->mothers_first_language = $request->mother_language;
            if($request->mother_english!="")
            {
                $parent->mothers_language_level = $request->mother_english;
            }
            else{
                $parent->mothers_language_level = $request->mother_english1;
            }
            $parent->save();

        }

        $student_user = new User();
        $student_user->full_name = $request->f_name." ".$request->m_name." ".$request->l_name;
        $student_user->username = 'sgstar' . str_pad($_count + 1, 3, '0', STR_PAD_LEFT) . '@sgstar.edu.vn';
        $student_user->email = 'sgstar' . str_pad($_count + 1, 3, '0', STR_PAD_LEFT) . '@sgstar.edu.vn';
        $student_user->password = Hash::make(123456);
        $student_user->role_id = 2;
        $student_user->save();

        $student = new SmStudent();
        $student->admission_no = substr(date('Y'), 2, 2).'01'.str_pad($_count + 1, 4, '0', STR_PAD_LEFT);
        $student->token = $request->_token;
        $student->roll_no = SmStudent::max('roll_no') + 1;
        $student->admission_date = Carbon::now()->toDateString();
        $student->user_id = $student_user->id;
        
        if ($request->file('std_photo') != "") {
            $file = $request->file('std_photo');
            $image = $student->id.'-'.$student->admission_no ."." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/', $image);
            $image = 'public/uploads/student/' . $image;
            $student->student_photo = $image;
        }
        
        $student->first_name = $request->f_name;
        $student->middle_name = $request->m_name;
        $student->last_name = $request->l_name;
        $student->full_name = $request->f_name." ".$request->m_name." ".$request->l_name;
        $student->nickname = $request->p_name;
        $student->date_of_birth = $request->date_of_birth;
        $student->gender_id = $request->gender;
        $student->nationality_1 = $request->nationality_1;
        $student->nationality_2 = $request->nationality_2;
        $student->class_id = $request->enrol_in;
        $student->section_id = SmClassSection::where('class_id',$request->enrol_in)->first()->section_id;
        $student->session_id = YearCheck::getAcademicId();
        $student->academic_id = YearCheck::getAcademicId();
        $student->parent_id = $parent_id;
        $student->first_language = $request->first_language;
        $student->second_language = $request->second_language;
        $student->other_language_proficient = $request->std_other_language;
        $student->english_proficient = $request->std_english;
        $student->role_id = 2;
        $student->is_trial = 1;
        $student->save();

        $enrolment = new Enrolment();
        $enrolment->token = $request->_token;
        $enrolment->applicant = $request->applicant;
        $enrolment->primary_contact = $request->primary_contact_radio;
        $enrolment->parent_id = $parent_id;
        $enrolment->student_id = $student->id;
        $enrolment->home_adress_in_hcm = $request->hcmc_address;
        $enrolment->save();

        $data['admission_no'] = $student->admission_no;
        $data['std_name'] = $request->f_name." ".$request->m_name." ".$request->l_name;
        $data['parent_email'] = $parent_mail;
        $data['token'] = $student->token;
        $data['parent_name'] = $parent->fathers_name;

        Mail::send('form::enrol-success', compact('data'), function ($message) use ($request , $parent_mail) {
                $setting = SmGeneralSettings::find(1);
                $email = $setting->email;
                $school_name = $setting->school_name;
                $message->to($email, $school_name)->subject('Enquiry form completed!');
                $message->from($email, $school_name);
            });
        Toastr::success('Operation successful', 'Success');
        return redirect()->back();  
    }


    public function enrolcompleted()
    {
        return view('form::enrolment-completed');
    }

    public function getsiblings($id){
        $siblings = SmStudent::where('parent_id' , $id)->get();
        foreach ($siblings as $key => $sibling) {
            $class = SmClass::find($sibling->class_id);
            $sibling->class = $class->class_name;
        }
        return $siblings;
    }

    public function searchStudent($admission_no)
    {
        $student = SmStudent::where('admission_no' , $admission_no)->first();
        $parent_find = SmParent::find($student->parent_id);
        $enrolment = Enrolment::where('student_id',$student->id)->where('parent_id',$parent_find->id)->first();
        $classes = SmClass::all();
        $transports = SmRoute::all();
        $parents = SmParent::all();
        $teachers = SmStaff::where('role_id',4)->get();
        $siblings = SmStudent::where('parent_id',$student->parent_id)->where('id','!=',$student->id)->get();
        return view('form::enrolment-form',compact('admission_no','parents','transports','classes','student','parent_find','enrolment','teachers','siblings'));
        
    }

    public function application ($admission_no,$token)
    {
        $student = SmStudent::where('admission_no' , $admission_no)->where('token',$token)->first();
        $parent_find = SmParent::find($student->parent_id);
        $enrolment = Enrolment::where('student_id',$student->id)->where('parent_id',$parent_find->id)->first();
        $classes = SmClass::all()->sortBy('stt');
        $transports = SmRoute::all();
        $parents = SmParent::all();
        $teachers = SmStaff::where('role_id',4)->get();
        $siblings = SmStudent::where('parent_id',$student->parent_id)->where('id','!=',$student->id)->get();
        $enrol = Enrolment::where('token',$token)->where('student_id',$student->id)->first();
        $blood_groups = SmBaseSetup::where('base_group_id',3)->get();
        return view('form::application',compact('blood_groups','admission_no','token','enrol','admission_no','parents','transports','classes','student','parent_find','enrolment','teachers','siblings'));
    }

    public function parentApplication(Request $request)
    {
        //dd($request);

        

    }

    public function ApplicationParent(Request $request)
    {
        
        $enrolment = Enrolment::where('student_id' , $request->student_id)->first();
        $enrolment->particular_interests = $request->std_interest;
        $enrolment->talents_or_abilities = $request->std_talents;
        $enrolment->primary_contact = $request->primary_contact_radio;
        $enrolment->trial_from_date = $request->trial_date_from;
        $enrolment->trial_to_date = $request->trial_date_to;
        $enrolment->trial_class_id = $request->trial_class;
        $enrolment->trial_teacher_id = $request->trial_teacher;
        $enrolment->school_history_name = $request->cur_school_name;
        $enrolment->school_history_city = $request->city_country;
        $enrolment->school_history_from_date = $request->school_his_date_from;
        $enrolment->school_history_to_date = $request->school_his_date_to;
        $enrolment->school_history_language_instruction = $request->lan_of_intruction;
        $enrolment->school_history_withdrawal_reason = $request->withdrawal_reason;

        $enrolment->health_information_1 = $request->health_1;
        $enrolment->health_information_2 = $request->health_2;
        $enrolment->health_information_3 = $request->health_3;
        $enrolment->health_information_4 = $request->health_4;
        $enrolment->health_information_5 = $request->health_5;
        // $enrolment->particular_interests = $request->parent_signature;
        $enrolment->additional_needs = $request->add_needs;

        $enrolment->bus_service_request = $request->bus_route;
        $enrolment->have_lunch_request = $request->have_lunch_request;
        $enrolment->lunch_request = $request->lunch_request;
        // $enrolment->eal_support = $request->eal_support;
        // $enrolment->sen_support = $request->sen_support;

        $enrolment->emergency_contact_name = $request->emergency_first_name;
        $enrolment->emergency_relationship = $request->relationship_to_child;
        $enrolment->emergency_contact_family_name = $request->emergency_family_name;
        $enrolment->emergency_contact_mobile_1 = $request->emergency_phone_1;
        $enrolment->emergency_contact_mobile_2 = $request->emergency_phone_2;
        $enrolment->emergency_contact_address = $request->emergency_address;
        $enrolment->save();

        $student = SmStudent::find($request->student_id);
        if ($request->file('std_photo') != "") {
            $file = $request->file('std_photo');
            $image = $student->id.'-'.$student->admission_no."." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/', $image);
            $image = 'public/uploads/student/' . $image;
            $student->student_photo = $image;
        }
        if ($request->file('health_info') != "") {
            $file = $request->file('health_info');
            $info = $student->id.'-'. md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/health_info', $info);
            $file = 'public/uploads/student/' . $info;
            $student->health_info = $info;
        }
        $student->first_name = $request->f_name;
        $student->middle_name = $request->m_name;
        $student->last_name = $request->l_name;
        $student->full_name = $request->f_name." ".$request->m_name." ".$request->l_name;
        $student->nickname = $request->p_name;
        $student->date_of_birth = $request->date_of_birth;
        $student->gender_id = $request->gender;
        $student->nationality_1 = $request->nationality_1;
        $student->nationality_2 = $request->nationality_2;
        $student->class_id = $request->enrol_in;
        $student->section_id = SmClassSection::where('class_id',$request->enrol_in)->first()->section_id;
        $student->session_id = YearCheck::getAcademicId();
        $student->first_language = $request->first_language;
        $student->second_language = $request->second_language;
        $student->other_language_proficient = $request->std_other_language;
        $student->english_proficient = $request->std_english;
        $student->height = $request->height;
        $student->weight = $request->weight;
        $student->bloodgroup_id = $request->blood_group;
        $student->first_language = $request->first_language;
        $student->second_language = $request->second_language;
        $student->english_proficient = $request->std_english;
        $student->other_language_proficient = $request->std_other_language;
        $student->is_trial = 1;
        $student->route_list_id = $request->bus_route;
        $student->save();
        
        
        $parent = SmParent::find($request->parent_selected);
        $parent->mothers_name = $request->mother_name;
        $parent->mother_role_is = $request->family_4_radio;
        $parent->mothers_mobile = $request->mother_phone;
        $parent->mothers_occupation = $request->mother_occupation;
        $parent->mothers_company = $request->mother_copany;
        $parent->mothers_nationality = $request->mother_nationality;
        $parent->mothers_work_address = $request->mother_work_address;
        $parent->mothers_email = $request->mother_email;
        $parent->mothers_first_language = $request->mother_language;
        $parent->mothers_language_level = $request->mother_english;

        $parent->fathers_name = $request->father_name;
        $parent->father_role_is = $request->family_3_radio;
        $parent->fathers_mobile = $request->father_phone;
        $parent->fathers_occupation = $request->father_occupation;
        $parent->fathers_company = $request->father_copany;
        $parent->fathers_nationality = $request->father_nationality;
        $parent->fathers_work_address = $request->father_work_address;
        $parent->fathers_email = $request->father_email;
        $parent->fathers_first_language = $request->father_language;
        $parent->fathers_language_level = $request->father_english;
        $parent->save();
            
        
        $assessment = Assessment::where('student_id',$student->id)->first();
        if(!$assessment)
        {
            $assessment = new Assessment();
            $assessment->student_id = $student->id;  
            $assessment->save();

            
        }
        
        $teacher_email = '';
        if($request->trial_teacher != '0')
        {
            
            $teacher = SmStaff::find($request->trial_teacher);
            $teacher_email = $teacher->email;
        }
        

        $headteacher_email = SmStaff::where('designation_id',13)->first()->email;
        $eal_email = SmStaff::where('designation_id',14)->first()->email;
        $sen_email = SmStaff::where('designation_id',45)->first()->email;
        //$emails = [$headteacher_email,$teacher_email,$eal_email,$sen_email];
        $emails = ['haiprobaby1998@gmail.com','haipham@sgstar.edu.vn','duonghuynh@sgstar.edu.vn'];
        $data['id_student'] = $student->id;


        Mail::send('form::mail-to-trial-teachers', compact('data'), function ($message) use ($request , $emails) {
            
            $setting = SmGeneralSettings::find(1);
            $email = $setting->email;
            $school_name = $setting->school_name;
            $message->to($emails, $school_name)->subject('Enrolment success');
            $message->from($email, $school_name);
        });
        Toastr::success('Operation successful', 'Success');
        return redirect()->back();     
    }
}
