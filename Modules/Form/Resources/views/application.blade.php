@extends('frontEnd.home.layout.front_master')
@push('css')
<link rel="stylesheet" href="{{asset('Modules/Form/public')}}/css/enrolment.css" media='all' />
<link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/nice-select.css" media='all'/>
<link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/toastr.min.css"/>
@endpush
@section('main_content')
	<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>How to apply?</h3>
            <div id="scroll"></div>

        </div>      
	</div>
	
</div>
<div class="c-color">
	<div class="container enrol-form">

		<form method="post" action="{{url('form/application-parent')}}" enctype="multipart/form-data">
			<input type="hidden" name="student_id" value="{{isset($student) ? $student->id : '' }}">
			<input type="hidden" name="parent_selected" id="parent_selected" value="{{isset($parent_find) ? $parent_find->id : ''}}">
			<input type="hidden" name="enrolment_id" id="enrolment_id" value="{{isset($enrolment) ? $enrolment->id : ''}}">
			<input type="hidden" name="admission_no" id="enrolment_id" value="{{isset($admission_no) ? $admission_no : ''}}">
			
			@csrf
			<div id="step-2" class="collapse show">
				@include('form::steps.step-2')
			</div>
			<div id="step-3" class="collapse show">
				@include('form::steps.step-3')
			</div>
			
		<div class="btn-save">
			<button class="btn btn-primary" id="enrol-save" type="submit">Save</button>
		</div>
		</form>
	<br><br>

</div>
@endsection
@section('script')
<script src="{{url('Modules/Form/public/js/printThis.js')}}"></script>
<script src="{{url('Modules/Form/public/js/jquery.number.min.js')}}"></script>
<script src="{{asset('public/backEnd/')}}/vendors/js/nice-select.min.js"></script>
<script src="{{url('Modules/Form/public/js/enrolment.js')}}"></script>
<script type="text/javascript" src="{{asset('public/backEnd/')}}/vendors/js/toastr.min.js"></script>
{!! Toastr::message() !!}


@endsection