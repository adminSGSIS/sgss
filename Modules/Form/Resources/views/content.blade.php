<div class="tab-content">
  <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
    <h4 class="text-center">Personal Info</h4>
    @include('form::forms.step1')
  </div>
  <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
    <h4 class="text-center">Parents Info</h4>
    @include('form::forms.step2')
    <h4 class="text-center">Emergency Contact</h4>
    @include('form::forms.step21')
  </div>
  <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
    <h4 class="text-center">School History</h4>
    @include('form::forms.step3')
  </div>
  <div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-4">
    <h4 class="text-center">Health Info</h4>
    @include('form::forms.step4')
  </div>
  <div id="step-5" class="tab-pane" role="tabpanel" aria-labelledby="step-5">
    <h4 class="text-center">Permissions</h4>
    @include('form::forms.step5')
    <h4 class="text-center">Special Educational Needs (SEN)</h4>
    @include('form::forms.step51')
  </div>
  <div id="step-6" class="tab-pane" role="tabpanel" aria-labelledby="step-6">
    <h4 class="text-center">Payment Info</h4>
    @include('form::forms.step6')
  </div>
  <div id="step-7" class="tab-pane" role="tabpanel" aria-labelledby="step-7">
    <h4 class="text-center">Fees</h4>
    @include('form::forms.step7')
  </div>
</div>