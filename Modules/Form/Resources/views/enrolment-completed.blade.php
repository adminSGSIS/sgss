@extends('frontEnd.home.layout.front_master')
@push('css')
<link rel="stylesheet" href="{{asset('Modules/Form/public')}}/css/enrolment.css" />
<link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/nice-select.css"/>
@endpush
@section('main_content')
	
	<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>Enrolment Completed!!!<br>
            	Welcome to Saigon Star International School
            </h3>
        </div>
	</div>
</div>
@endsection