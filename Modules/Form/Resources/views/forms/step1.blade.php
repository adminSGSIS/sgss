<input class="primary-input date" id="admissionDate" type="hidden" name="admission_date" value="{{old('admission_date') != ""? old('admission_date'):date('m/d/Y')}}" autocomplete="off">

<div class="row mb-40 mt-30">
  <div class="col-3">
    <div class="input-effect">
      <input class="
          primary-input 
          form-control{{ $errors->has('student_full_name') ? ' is-invalid' : '' }}
        " type="text" name="student_full_name" value="{{old('student_full_name')}}">
      <label>Student’s full name *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="student_full_name_feedback" role="alert"></span>
      @if ($errors->has('student_full_name'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('student_full_name') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="
          primary-input 
          form-control{{ $errors->has('student_preferred_name') ? ' is-invalid' : '' }}
        " type="text" name="student_preferred_name" value="{{old('student_preferred_name')}}">
      <label>Student’s preferred name</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="student_preferred_name_feedback" role="alert"></span>
      @if ($errors->has('student_preferred_name'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('student_preferred_name') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="no-gutters input-right-icon">
      <div class="col">
        <div class="input-effect sm2_mb_20 md_mb_20">
          <input class="primary-input date form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" id="startDate" type="text" name="date_of_birth" value="{{old('date_of_birth')}}" autocomplete="off">
          <label>Date of birth *</label>
          <span class="focus-border"></span>
          @if ($errors->has('date_of_birth'))
          <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('date_of_birth') }}</strong>
          </span>
          @endif
        </div>
      </div>
      <!--<div class="col-auto">-->
      <!--  <button class="" type="button">-->
      <!--    <i class="ti-calendar" id="start-date-icon"></i>-->
      <!--  </button>-->
      <!--</div>-->
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('personal_email_address') ? ' is-invalid' : '' }}" type="text" name="personal_email_address" value="{{old('personal_email_address')}}">

      <label>Email address</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="personal_email_address_feedback" role="alert"></span>
      @if ($errors->has('personal_email_address'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('personal_email_address') }}</strong>
      </span>
      @endif
    </div>
  </div>
</div>

<div class="row mb-10">
  <div class="col-lg-12 d-flex">
    <p class="text-uppercase fw-100 mb-10">Gender *</p>
    <div class="d-flex radio-btn-flex ml-40">
      @foreach($genders as $gender)
      <div class="mr-30">
        <input type="radio" name="gender" id="relationFather{{$gender->id}}" value="{{$gender->id}}" class="common-radio relationButton" @if(old('gender')==$gender->id) checked @endif>
        <label for="relationFather{{$gender->id}}">{{$gender->base_setup_name}}</label>
      </div>
      @endforeach
    </div>
  </div>
  <div class="col-lg-12 d-flex">
    <small style="color: #DC3545;">{{ $errors->first('gender') }}</small>
  </div>
</div>

<div class="row mb-40">
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('first_nationality') ? ' is-invalid' : '' }}" type="text" name="first_nationality" value="{{old('first_nationality')}}">

      <label>First Nationality *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="first_nationality_feedback" role="alert"></span>
      @if ($errors->has('first_nationality'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('first_nationality') }}</strong>
      </span>
      @endif
    </div>
  </div>
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('second_nationality') ? ' is-invalid' : '' }}" type="text" name="second_nationality" value="{{old('second_nationality')}}">

      <label>Second Nationality</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="second_nationality_feedback" role="alert"></span>
      @if ($errors->has('second_nationality'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('second_nationality') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-6">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('residential_address_in_vietnam') ? ' is-invalid' : '' }}" type="text" name="residential_address_in_vietnam" value="{{old('residential_address_in_vietnam')}}">

      <label>Residential address in Vietnam *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="residential_address_in_vietnam_feedback" role="alert"></span>
      @if ($errors->has('residential_address_in_vietnam'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('residential_address_in_vietnam') }}</strong>
      </span>
      @endif
    </div>
  </div>
</div>

<div class="row mb-40">
  <div class="col-4">
    <fieldset>
      <p>Student lives with *</p>
      @php
      $checked_father = false;
      $checked_mother = false;
      $checked_stepmother = false;
      $checked_stepfather = false;
      $checked_guardian = false;
      $checked_other = false;
      @endphp
      @if (old('student_lives_with'))
      @foreach (old('student_lives_with') as $i)
      @if ($i == "father")
      @php
      $checked_father = true
      @endphp
      @endif

      @if ($i == "mother")
      @php
      $checked_mother = true
      @endphp
      @endif

      @if ($i == "stepmother")
      @php
      $checked_stepmother = true
      @endphp
      @endif

      @if ($i == "stepfather")
      @php
      $checked_stepfather = true
      @endphp
      @endif

      @if ($i == "guardian")
      @php
      $checked_guardian = true
      @endphp
      @endif

      @if ($i == "other")
      @php
      $checked_other = true
      @endphp
      @endif
      @endforeach
      @endif
      <div>
        <input type="checkbox" id="check_father" name="student_lives_with[]" value="father" @if($checked_father) checked @endif>
        <label for="check_father">Father</label>
      </div>
      <div>
        <input type="checkbox" id="check_mother" name="student_lives_with[]" value="mother" @if($checked_mother) checked @endif>
        <label for="check_mother">Mother</label>
      </div>
      <div>
        <input type="checkbox" id="check_stepmother" name="student_lives_with[]" value="stepmother" @if($checked_stepmother) checked @endif>
        <label for="check_stepmother">Stepmother</label>
      </div>
      <div>
        <input type="checkbox" id="check_stepfather" name="student_lives_with[]" value="stepfather" @if($checked_stepfather) checked @endif>
        <label for="check_stepfather">Stepfather</label>
      </div>
      <div>
        <input type="checkbox" id="check_guardian" name="student_lives_with[]" value="guardian" @if($checked_guardian) checked @endif>
        <label for="check_guardian">Guardian</label>
      </div>
      <div>
        <input type="checkbox" id="check_other" name="student_lives_with[]" value="other" @if($checked_other) checked @endif>
        <label for="check_other">Other</label>
      </div>
      <small style="color: #DC3545;">{{ $errors->first('student_lives_with') }}</small>
    </fieldset>
  </div>
  <div class="col-8">
    <div class="row">
      <div class="col">
        <div class="row">
          <div class="input-effect">
            <input class="primary-input form-control{{ $errors->has('childs_first_language') ? ' is-invalid' : '' }}" type="text" name="childs_first_language" value="{{old('childs_first_language')}}">
            <label>Child's first language *</label>
            <span class="focus-border"></span>
            <span class="invalid-feedback" id="childs_first_language_feedback" role="alert"></span>
            @if ($errors->has('childs_first_language'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('childs_first_language') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="row mt-30">
          <div class="input-effect">
            <input class="primary-input form-control{{ $errors->has('childs_second_language') ? ' is-invalid' : '' }}" type="text" name="childs_second_language" value="{{old('childs_second_language')}}">
            <label>Child's second language</label>
            <span class="focus-border"></span>
            <span class="invalid-feedback" id="child_second_language_feedback" role="alert"></span>
            @if ($errors->has('childs_second_language'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('childs_second_language') }}</strong>
            </span>
            @endif
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <fieldset>
          <small style="color: #DC3545;">{{ $errors->first('level_of_E_language_experience') }}</small>
          <p>Child's level of English Language experience</p>
          <div>
            <input type="radio" id="native" class="common-radio relationButton" name="level_of_E_language_experience" value="0" @if(old('level_of_E_language_experience')=='0' ) checked @endif>
            <label for="native">Native</label>
            <br>
          </div>
          <div>
            <input type="radio" id="more_than_5_years" class="common-radio relationButton" name="level_of_E_language_experience" value="1" @if(old('level_of_E_language_experience')=='1' ) checked @endif>
            <label for="more_than_5_years">More than 5 years</label>
            <br>
          </div>
          <div>
            <input type="radio" id="2-5_years" class="common-radio relationButton" name="level_of_E_language_experience" value="2" @if(old('level_of_E_language_experience')=='2' ) checked @endif>
            <label for="2-5_years">2-5 years</label>
            <br>
          </div>
          <div>
            <input type="radio" id="less_than_2_years" class="common-radio relationButton" name="level_of_E_language_experience" value="3" @if(old('level_of_E_language_experience')=='3' ) checked @endif>
            <label for="less_than_2_years">Less than 2 years</label>
            <br>
          </div>
          <div>
            <input type="radio" id="no_pior_experience" class="common-radio relationButton" name="level_of_E_language_experience" value="4" @if(old('level_of_E_language_experience')=='4' ) checked @endif>
            <label for="no_pior_experience">No pior experience</label>
            <br>
          </div>
          <div>
            <input type="radio" id="other" class="common-radio relationButton" name="level_of_E_language_experience" value="5" @if(old('level_of_E_language_experience')=='5' ) checked @endif>
            <label for="other">Other</label>
            <br>
          </div>
        </fieldset>
      </div>
    </div>
  </div>
</div>
