<div class="row mb-40 mt-30">
  <div class="col-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('fathers_full_name') ? ' is-invalid' : '' }}" type="text" name="fathers_full_name" value="{{old('fathers_full_name')}}">

      <label>Father’s full name *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="fathers_name_feedback" role="alert"></span>
      @if ($errors->has('fathers_full_name'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('fathers_full_name') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('fathers_nationality') ? ' is-invalid' : '' }}" type="text" name="fathers_nationality" value="{{old('fathers_nationality')}}">
      <label>Father's nationality *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="fathers_nationality_feedback" role="alert"></span>
      @if ($errors->has('fathers_nationality'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('fathers_nationality') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('fathers_contact_phone_number') ? ' is-invalid' : '' }}" name="fathers_contact_phone_number" value="{{old('fathers_contact_phone_number')}}">

      <label>Father's contact phone number *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="fathers_contact_phone_number_feedback" role="alert"></span>
      @if ($errors->has('fathers_contact_phone_number'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('fathers_contact_phone_number') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('fathers_email_address') ? ' is-invalid' : '' }}" type="text" name="fathers_email_address" value="{{old('fathers_email_address')}}">

      <label>Father's email address *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="fathers_email_address_feedback" role="alert"></span>
      @if ($errors->has('fathers_email_address'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('fathers_email_address') }}</strong>
      </span>
      @endif
    </div>
  </div>

</div>

<div class="row mb-40 mt-30">
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('mothers_full_name') ? ' is-invalid' : '' }}" type="text" name="mothers_full_name" value="{{old('mothers_full_name')}}">

      <label>Mother’s full name *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="mothers_full_name_feedback" role="alert"></span>
      @if ($errors->has('mothers_full_name'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('mothers_full_name') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('mothers_nationality') ? ' is-invalid' : '' }}" type="text" name="mothers_nationality" value="{{old('mothers_nationality')}}">

      <label>Mother's nationality *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="mothers_nationality_feedback" role="alert"></span>
      @if ($errors->has('mothers_nationality'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('mothers_nationality') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('mothers_contact_phone_number') ? ' is-invalid' : '' }}" type="text" name="mothers_contact_phone_number" value="{{old('mothers_contact_phone_number')}}">

      <label>Mother's contact phone number*</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="mothers_contact_phone_number_feedback" role="alert"></span>
      @if ($errors->has('mothers_contact_phone_number'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('mothers_contact_phone_number') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('mothers_email_address') ? ' is-invalid' : '' }}" type="text" name="mothers_email_address" value="{{old('mothers_email_address')}}">

      <label>Mother's email address *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="mothers_email_address_feedback	" role="alert"></span>
      @if ($errors->has('mothers_email_address'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('mothers_email_address') }}</strong>
      </span>
      @endif
    </div>
  </div>
</div>

<div class="row justify-content-around mb-40">
  <div class="col-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('fathers_occupation') ? ' is-invalid' : '' }}" type="text" name="fathers_occupation" value="{{old('fathers_occupation')}}">
      <label>Father's occupation</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="fathers_occupation_feedback	" role="alert"></span>
      @if ($errors->has('fathers_occupation'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('fathers_occupation') }}</strong>
      </span>
      @endif
    </div>
  </div>
  <div class="col-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('mothers_occupation') ? ' is-invalid' : '' }}" type="text" name="mothers_occupation" value="{{old('mothers_occupation')}}">
      <label>Mother's occupation</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="mothers_occupation_feedback	" role="alert"></span>
      @if ($errors->has('mothers_occupation'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('mothers_occupation') }}</strong>
      </span>
      @endif
    </div>
  </div>
</div>
