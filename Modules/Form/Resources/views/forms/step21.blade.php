<div class="row mb-40 mt-30 justify-content-around">
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('emergency_full_name') ? ' is-invalid' : '' }}" type="text" name="emergency_full_name" value="{{old('emergency_full_name')}}">

      <label>Full name *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_full_name_feedback" role="alert"></span>
      @if ($errors->has('emergency_full_name'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('emergency_full_name') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('emergency_relationship_to_parents') ? ' is-invalid' : '' }}" type="text" name="emergency_relationship_to_parents" value="{{old('emergency_relationship_to_parents')}}">

      <label>Relationship to parents *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_relationship_to_parents_feedback" role="alert"></span>
      @if ($errors->has('emergency_relationship_to_parents'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('emergency_relationship_to_parents') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('emergency_contact_phone_number') ? ' is-invalid' : '' }}" type="text" name="emergency_contact_phone_number" value="{{old('emergency_contact_phone_number')}}">

      <label>Contact phone number *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_contact_phone_number_feedback" role="alert"></span>
      @if ($errors->has('emergency_contact_phone_number'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('emergency_contact_phone_number') }}</strong>
      </span>
      @endif
    </div>
  </div>

</div>

<div class="row justify-content-around mb-40">
  <div class="col-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('emergency_occupation') ? ' is-invalid' : '' }}" type="text" name="emergency_occupation" value="{{old('emergency_occupation')}}">
      <label>Emergency's occupation</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_occupation_feedback	" role="alert"></span>
      @if ($errors->has('emergency_occupation'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('emergency_occupation') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('emergency_address') ? ' is-invalid' : '' }}" type="text" name="emergency_address" value="{{old('emergency_address')}}">

      <label>Emergency's address </label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_address_feedback" role="alert"></span>
      @if ($errors->has('emergency_address'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('emergency_address') }}</strong>
      </span>
      @endif
    </div>
  </div>
</div>