<div class="row mb-40 mt-30">
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('school_name') ? ' is-invalid' : '' }}" type="text" name="school_name" value="{{old('school_name')}}">

      <label>School Name</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="school_name_feedback" role="alert"></span>
      @if ($errors->has('school_name'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('school_name') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('location_or_country') ? ' is-invalid' : '' }}" type="text" name="location_or_country" value="{{old('location_or_country')}}">

      <label>Location/Country</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="location_or_country_feedback" role="alert"></span>
      @if ($errors->has('location_or_country'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('location_or_country') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('language_of_instruction') ? ' is-invalid' : '' }}" type="text" name="language_of_instruction" value="{{old('language_of_instruction')}}">

      <label>Language of Instruction</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="language_of_instruction_feedback" role="alert"></span>
      @if ($errors->has('language_of_instruction'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('language_of_instruction') }}</strong>
      </span>
      @endif
    </div>
  </div>

</div>

<div class="row mb-40 mt-30">
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('from') ? ' is-invalid' : '' }}" type="text" name="from" value="{{old('from')}}">

      <label>From</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="from_feedback" role="alert"></span>
      @if ($errors->has('from'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('from') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('to') ? ' is-invalid' : '' }}" type="text" name="to" value="{{old('to')}}">

      <label>To</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="to_feedback" role="alert"></span>
      @if ($errors->has('to'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('to') }}</strong>
      </span>
      @endif
    </div>
  </div>
</div>