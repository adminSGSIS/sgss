@php
$checked_0 = false;
$checked_1 = false;
@endphp
@if (old('health_permissions'))
@foreach (old('health_permissions') as $i)
@if ($i == "0")
@php
$checked_0 = true
@endphp
@endif

@if ($i == "1")
@php
$checked_1 = true
@endphp
@endif
@endforeach
@endif
<div class="row mt-30">
  <div class="col-lg-12">
    <fieldset>
      <small style="color: #DC3545;">{{ $errors->first('health_permissions') }}</small>
      <div>
        <input type="checkbox" id="health1" name="health_permissions[]" value="0" @if($checked_0) checked @endif>
        <label for="health1">I DO NOT consent to the school nurse administering over-the-counter medications for symptom relief of minor illnesses</label>
      </div>
      <div>
        <input type="checkbox" id="health2" name="health_permissions[]" value="1" @if($checked_1) checked @endif>
        <label for="health2">I DO NOT consent to emergency hospital treatment for my child</label>
      </div>
    </fieldset>
  </div>
</div>

@php
$checked_0 = false;
$checked_1 = false;
@endphp
@if (old('other_permissions'))
@foreach (old('other_permissions') as $i)
@if ($i == "0")
@php
$checked_0 = true
@endphp
@endif
@if ($i == "1")
@php
$checked_1 = true
@endphp
@endif
@endforeach
@endif
<div class="row">
  <div class="col-lg-12">
    <fieldset>
      <small style="color: #DC3545;">{{ $errors->first('other_permissions') }}</small>
      <div>
        <input type="checkbox" id="permissions0" name="other_permissions[]" value="0" @if($checked_0) checked @endif>
        <label for="permissions0">Please tick if you DO NOT consent to group photographs of your child being used for school advertising and marketing purposes</label>
      </div>
      <div>
        <input type="checkbox" id="permissions1" name="other_permissions[]" value="1" @if($checked_1) checked @endif>
        <label for="permissions1">Please tick if you DO NOT consent to joining a class parents What'sApp group (to learn about birthday parties etc)</label>
      </div>
    </fieldset>
  </div>
</div>
