<div class="row mt-10">
  <div class="col-lg-2">
    <small style="color: #DC3545;">{{ $errors->first('lunch_requested') }}</small>
  </div>
  <div class="col-lg-4">
    <small style="color: #DC3545;">{{ $errors->first('school_bus') }}</small>
  </div>
  <div class="col-lg-3">
    <small style="color: #DC3545;">{{ $errors->first('payment_by') }}</small>
  </div>
  <div class="col-lg-3">
    <small style="color: #DC3545;">{{ $errors->first('VAT') }}</small>
  </div>
</div>
<div class="row mb-40">
  <div class="col-lg-2">
    <fieldset>
      <p>Lunch requested: *</p>
      <div>
        <input type="radio" id="lunch_requested_yes" class="common-radio relationButton" name="lunch_requested" value="1" @if(old('lunch_requested')=='1' ) checked @endif>
        <label for="lunch_requested_yes">YES</label>
        <br>
      </div>
      <div>
        <input type="radio" id="lunch_request_no" class="common-radio relationButton" name="lunch_requested" value="0" @if(old('lunch_requested')=='0' ) checked @endif>
        <label for="lunch_request_no">NO</label>
        <br>
      </div>
    </fieldset>
  </div>
  <div class="col-lg-4">
    <fieldset>
      <p>School bus service requested *</p>
      <div>
        <input type="radio" id="bus_0" class="common-radio relationButton" name="school_bus" value="0" @if(old('school_bus')=='0' ) checked @endif>
        <label for="bus_0">Vista Verde or Diamond Island</label>
        <br>
      </div>
      <div>
        <input type="radio" id="bus_1" class="common-radio relationButton" name="school_bus" value="1" @if(old('school_bus')=='1' ) checked @endif>
        <label for="bus_1">District 2</label>
        <br>
      </div>
      <div>
        <input type="radio" id="bus_2" class="common-radio relationButton" name="school_bus" value="2" @if(old('school_bus')=='2' ) checked @endif>
        <label for="bus_2">Other districts</label>
        <br>
      </div>
      <div>
        <input type="radio" id="bus_3" class="common-radio relationButton" name="school_bus" value="3" @if(old('school_bus')=='3' ) checked @endif>
        <label for="bus_3">No</label>
        <br>
      </div>
      <div>
        <input type="radio" id="bus_4" class="common-radio relationButton" name="school_bus" value="4" @if(old('school_bus')=='4' ) checked @endif>
        <label for="bus_4">Other</label>
        <br>
      </div>
    </fieldset>
  </div>
  <div class="col-lg-3">
    <fieldset>
      <p>Payment by *</p>
      <div>
        <input type="radio" id="payment_by_0" class="common-radio relationButton" name="payment_by" value="0" @if(old('payment_by')=='0' ) checked @endif>
        <label for="payment_by_0">Father</label>
        <br>
      </div>
      <div>
        <input type="radio" id="payment_by_1" class="common-radio relationButton" name="payment_by" value="1" @if(old('payment_by')=='1' ) checked @endif>
        <label for="payment_by_1">Mother</label>
        <br>
      </div>
      <div>
        <input type="radio" id="payment_by_2" class="common-radio relationButton" name="payment_by" value="2" @if(old('payment_by')=='2' ) checked @endif>
        <label for="payment_by_2">Company</label>
        <br>
      </div>
      <div>
        <input type="radio" id="payment_by_3" class="common-radio relationButton" name="payment_by" value="3" @if(old('payment_by')=='3' ) checked @endif>
        <label for="payment_by_3">Other</label>
        <br>
      </div>
    </fieldset>
  </div>
  <div class="col-lg-3">
    <fieldset>
      <p>Do you require a VAT Invoice? *</p>
      <div>
        <input type="radio" id="VAT0" class="common-radio relationButton" name="VAT" value="0" @if(old('VAT')=='0' ) checked @endif>
        <label for="VAT0">Yes</label>
        <br>
      </div>
      <div>
        <input type="radio" id="VAT1" class="common-radio relationButton" name="VAT" value="1" @if(old('VAT')=='1' ) checked @endif>
        <label for="VAT1">No</label>
        <br>
      </div>
      <div>
        <input type="radio" id="VAT2" class="common-radio relationButton" name="VAT" value="2" @if(old('VAT')=='2' ) checked @endif>
        <label for="VAT2">Other</label>
        <br>
      </div>
    </fieldset>
  </div>
</div>

<div class="row mb-40 mt-30">
  <div class="col-lg-4">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" type="text" name="company_name" value="{{old('company_name')}}">

      <label>Company Name (if payment by company)</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="company_name_feedback" role="alert"></span>
      @if ($errors->has('company_name'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('company_name') }}</strong>
      </span>
      @endif
    </div>
  </div>
  <div class="col-lg-4">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('contact_name') ? ' is-invalid' : '' }}" type="text" name="contact_name" value="{{old('contact_name')}}">

      <label>Contact Name</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="contact_name_feedback" role="alert"></span>
      @if ($errors->has('contact_name'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('contact_name') }}</strong>
      </span>
      @endif
    </div>
  </div>
  <div class="col-lg-4">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" type="text" name="address" value="{{old('address')}}">

      <label>Address</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="address_feedback" role="alert"></span>
      @if ($errors->has('address'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('address') }}</strong>
      </span>
      @endif
    </div>
  </div>
</div>