<input type="hidden" name="url" id="url" value="{{URL::to('/')}}">

<div class="row mt-15 justify-content-start">
  <div class="col">
    More Information, click here <a href="/admissions" target="_blank" rel="noopener noreferrer">Fees & Tuition</a>
  </div>
</div>

<div class="row mt-15 justify-content-start">
  <div class="col-3">
    <div class="input-effect sm2_mb_20 md_mb_20">
      <select class="w-100 bb form-control" name="session" id="academic_year_form">
        <option data-display="@lang('lang.academic_year_form') *" value="">@lang('lang.academic_year')</option>
        @foreach($sessions as $session)
        <option value="{{$session->id}}" {{old('session') == $session->id? 'selected': ''}}>{{$session->year}}[{{$session->title}}]</option>
        @endforeach
      </select>
      <span class="focus-border"></span>
      @if ($errors->has('session'))
      <span class="invalid-feedback invalid-select" role="alert">
        <strong>{{ $errors->first('session') }}</strong>
      </span>
      @endif
    </div>
  </div>

  <div class="col-3">
    <div class="input-effect sm2_mb_20 md_mb_20" id="class-div">
      <select class=" w-100 bb form-control" name="class" id="classSelectStudent_enrolment">
        <option data-display="@lang('lang.class') *" value="">@lang('lang.class')</option>

      </select>
      <span class="focus-border"></span>
      @if ($errors->has('class'))
      <span class="invalid-feedback invalid-select" role="alert">
        <strong>{{ $errors->first('class') }}</strong>
      </span>
      @endif
    </div>
  </div>

  @if(!empty(old('class')))

  @php
  $old_sections = DB::table('sm_class_sections')->where('class_id', '=', old('class'))
  ->join('sm_sections','sm_class_sections.section_id','=','sm_sections.id')
  ->get();
  @endphp

  <div class="col-3">
    <div class="input-effect sm2_mb_20 md_mb_20" id="sectionStudentDiv_enrolment">
      <select class=" w-100 bb form-control" name="section" id="sectionSelectStudent_enrolment">
        <option data-display="@lang('lang.section') *" value="">@lang('lang.section')</option>
        @foreach ($old_sections as $old_section)
        <option value="{{ $old_section->id }}" {{ old('section')==$old_section->id ? 'selected' : '' }}>
          {{ $old_section->section_name }}</option>
        @endforeach
      </select>
      <span class="focus-border"></span>
      @if ($errors->has('section'))
      <span class="invalid-feedback invalid-select" role="alert">
        <strong>{{ $errors->first('section') }}</strong>
      </span>
      @endif
    </div>
  </div>
  @else

  <div class="col-3">
    <div class="input-effect sm2_mb_20 md_mb_20" id="sectionStudentDiv_enrolment">
      <select class=" w-100 bb form-control" name="section" id="sectionSelectStudent_enrolment">
        <option data-display="@lang('lang.section') *" value="">@lang('lang.section')</option>
      </select>
      <span class="focus-border"></span>
      @if ($errors->has('section'))
      <span class="invalid-feedback invalid-select" role="alert">
        <strong>{{ $errors->first('section') }}</strong>
      </span>
      @endif
    </div>
  </div>
  @endif
</div>

<div class="row mt-15 justify-content-start">
  <div class="col-3">
    <div class="input-effect">
      <input class="primary-input form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" type="text" name="price" value="{{old('price')}}">

      <label>Price VND *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="student_preferred_name_feedback" role="alert"></span>
      @if ($errors->has('price'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('price') }}</strong>
      </span>
      @endif
    </div>
  </div>
  <div class="col-3">
    <div class="row">
      <div class="col">
        <small style="color: #DC3545;">{{ $errors->first('method') }}</small>
      </div>
      <div class="col">
        <p class="text-uppercase fw-100 mb-10">Option</p>
        <div class="d-flex radio-btn-flex ml-40" style="flex-wrap:wrap">
          @foreach ($payment_methods as $i)
          <div class="mt-10">
            <input type="radio" name="method" id="{{$i->id}}" value="{{$i->method}}" class="common-radio relationButton" @if(old('method')==$i->method ) checked @endif>
            <label for="{{$i->id}}">{{$i->method}}</label>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
