LETTER OF CONFIRMATION

<p> Dear  {{ $studentSingle->full_name }</p> thành <p>Dear parent of {{ $studentSingle->full_name }},</p>

We are delighted to confirm a place for your child/children in Year ____ at Saigon Star International School.<br>

Your child/children’s first day at school will be ____________________. <br>

{{-- The school bus will collect your child/children from _________________ at _______. <br> --}}

{{-- The drop off time in the afternoon is ______. Please also take note of the bus supervisor’s name and phone number  __________________. <br> --}}

Before your child begins / children begin school, we invite you to collect uniforms from the school office, which is open 8.15am to 4.30pm, Monday to Friday, during term time.<br>

Next steps<br>

If you have not already done so, we kindly ask you to submit the following documents to the school office:<br>

•	2 current passport photographs of the student <br>
•	Medical/Physical Record signed by a Doctor <br>
•	Copy of student’s passport or birth certificate <br>
•	Learning Support Assessment documentation attached (if applicable). This could be examination certificates school reports, recommendations or a transcript <br>
•	A Confidential Letter of Reference submitted by the child’s most recent school if applicable <br>
We also ask parents to read our parent handbook, then sign and return the declaration on the final page.<br>
Warm regards,<br>
The Saigon Star Admissions Team<br>
