<ul class="nav">
  <li class="nav-item">
    <a class="nav-link" href="#step-1">
      <i class="material-icons">person</i>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#step-2">
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#step-3">
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#step-4">
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#step-5">
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#step-6">
      <i class="material-icons">payment</i>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#step-7">
      <i class="material-icons">school</i>
    </a>
  </li>
  {{--
  <li class="nav-item">
    <a class="nav-link" href="#step-8">
      <i class="material-icons">attach_money</i>
    </a>
  </li>
  --}}
</ul>