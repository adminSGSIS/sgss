<div id="std-info">
<div class="top-header">
	<h3 class="center text-header">STUDENT INFORMATION</h3>
</div>
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-12">
		<div class="enrol-input">
			<input type="text" value="{{isset($student)? $student->first_name : '' }}" class="form-control" id="f_name" name="f_name" placeholder="First name (Given name) *" required="required">
			<div class="validate" id="f_name_validate">*this field cannot be empty*</div>
		</div>
		<div class="enrol-input">
			<input type="text" value="{{isset($student)? $student->middle_name : '' }}" class="form-control" name="m_name" placeholder="Middle name(s) (if any)">
		</div>
		<div class="enrol-input">
			<input type="text" value="{{isset($student)? $student->last_name : '' }}" class="form-control" id="l_name" name="l_name" placeholder="Last name (Family name) *" required="required">
			<div class="validate" id="l_name_validate">*this field cannot be empty*</div>
		</div>
		<p>* please capitalize the LAST NAME / FAMILY NAME</p>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 center">
		<div class="std-img-picker">
			<img src="{{url('/')}}/{{isset($student)? $student->student_photo: ''}}" id="imgPreview" name="std_photo">
		</div>
		<a  id="std_photo_btn" class="btn btn-primary" style="margin-top: 10px;background: #7dd3f7;color:#fff">Photo</a>
		<input type="file" name="std_photo" id="std_photo_btn_real" class="hidden" >
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="enrol-input">
			<input type="text" value="{{isset($student)? $student->nickname:""}}" class="form-control" name="p_name" placeholder="Preferred name (or nick name)">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="enrol-input">
			<input type="text" value="{{isset($student)? $student->date_of_birth: ''}}" class="form-control date-input" name="date_of_birth" id="date_of_birth" placeholder="Date of Birth (dd/mm/yyyy) *" required="required">
			<div class="validate" id="date_of_birth_validate">*this field cannot be empty*</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="gender-container">
			<div class="row">
				<div class="col-3">Gender</div>
				<div class="col-4"><input type="radio" {{isset($student) && $student->gender_id == 1? 'checked': ''}} name="gender" value="1"> Male</div>
				<div class="col-5"><input type="radio" {{isset($student) && $student->gender_id == 2? 'checked': ''}} name="gender" value="2"> Female</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="enrol-input">
			<input type="text" value="{{isset($student)? $student->nationality_1: ''}}" class="form-control" name="nationality_1" id="nationality_1" placeholder="Nationality 1  *" required="required">
			<div class="validate" id="nationality_1_validate">*this field cannot be empty*</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="enrol-input">
			<input type="text" value="{{isset($student)? $student->nationality_2: ''}}" class="form-control" name="nationality_2" placeholder="Nationality 2 (if any)">
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="enrol-input">
			<select class="form-control" name="enrol_in" id="enrol_in" required="required">
				<option value="0">Enrol in  *</option>
				@foreach($classes as $class)
				<option value="{{$class->id}}" {{isset($student)&& $student->class_id == $class->id ? 'selected' : ''}}>{{$class->class_name}}</option>
				@endforeach
			</select>
			<div class="validate" id="enrol_in_validate">*this field cannot be empty*</div>
		</div>
	</div>
	{{-- <div class="col-lg-3 col-md-4 col-sm-12">
		<div class="enrol-input">
			<select class="form-control" id="applicant_select" name="applicant">
			<option value="1">Father</option>
			<option value="2">Mother</option>
		</select>
		</div>
		
	</div> --}}
	{{-- <div class="col-lg-6 col-md-6 col-sm-12">
		<div class="enrol-input">
			<input type="text" class="form-control" id="applicant_name" name="applicant_name" placeholder="Applicant's name" required="">
		</div>
	</div> --}}
</div>
</div>
{{-- <div class="top-header">
	<h3 class="center text-header">FAMILY INFORMATION</h3>
</div>

<br>
<div id="family-select">
	<select class="niceSelect w-100 form-control" id="parent-select" >
	<option value="0" id="first-select">Select family</option>
	@foreach($parents as $parent)
		<option {{isset($parent_find) && $parent_find->id == $parent->id ? 'selected' : ''}}  value="{{$parent->id}}">Father : {{$parent->fathers_name != "" ? $parent->fathers_name : ""}} / Mother : {{$parent->mothers_name != "" ? $parent->mothers_name : ""}}
		</option>
	@endforeach
	</select>
	
</div> --}}
<div id="trial-form">
<div class="top-header">
	<h3 class="center text-header">TRIAL FORM <i class="fa fa-print right" id="print-trial" aria-hidden="true"></i></h3>
</div>
<h4 class="text-sub-header">STUDENT INTERESTS & LANGUAGE </h4>
<div class="gender-container">
	<div class="row b-bottom-input-group">
		<div class="col-lg-3 col-md-4 col-sm-12"><div class="sm-content">Student's particular interests?</div></div>
		<div class="col-lg-9 col-md-8 col-sm-12">
			<input type="text" name="std_interest" class="b-bottom-input form-control" value="{{isset($enrolment) ? $enrolment->particular_interests : ''}}">
		</div>
	</div>
	<div class="row b-bottom-input-group">
		<div class="col-lg-12 col-md-12 col-sm-12">What talents or abilities does your child have and wish to develop further? (e.g.music, athletics)</div>
		<br>
		<div class="col-lg-12 col-md-12 col-sm-12">
			<input type="text" name="std_talents" class="b-bottom-input form-control" value="{{isset($enrolment) ? $enrolment->talents_or_abilities: ''}}">
		</div>
	</div>
</div>

<div class="gender-container">
	<div class="row b-bottom-input-group">
		<div class="col-lg-2 col-md-2 col-sm-12"><div class="sm-content">First language</div></div>
		<div class="col-lg-4 col-md-4 col-sm-12">
			<input type="text" name="first_language" class="b-bottom-input form-control" value="{{isset($student) ? $student->first_language: ''}}">
			<div class="validate" id="first_language_validate">*this field cannot be empty*</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-12"><div class="sm-content">Second language (in any)</div></div>
		<div class="col-lg-3 col-md-3 col-sm-12">
			<input type="text" name="second_language" class="b-bottom-input form-control" value="{{isset($student) ? $student->second_language: ''}}">
		</div>
	</div>
	<div>Other spoken language: (from most proficient lo least proficient )</div>
	<div class="row checkbox-group">
		<div class="col-lg-2 col-md-3 col-sm-6">
			<input type="radio" name="std_other_language" value="1" {{isset($student) && $student->other_language_proficient == 1 ? 'checked': ''}}> Beginner
		</div>
		<div class="col-lg-3 col-md-4 col-sm-6">
			<input type="radio" name="std_other_language" value="2" {{isset($student) && $student->other_language_proficient == 2 ? 'checked': ''}}> Gaining confidence
		</div>
		<div class="col-lg-2 col-md-3 col-sm-6">
			<input type="radio" name="std_other_language" value="3" {{isset($student) && $student->other_language_proficient == 3 ? 'checked': ''}}> Confidence
		</div>
		<div class="col-lg-2 col-md-3 col-sm-6">
			<input type="radio" name="std_other_language" value="4" {{isset($student) && $student->other_language_proficient == 4 ? 'checked': ''}}> Fluent
		</div>
		<div class="col-lg-2 col-md-3 col-sm-6">
			<input type="radio" name="std_other_language" value="5" {{isset($student) && $student->other_language_proficient == 5 ? 'checked': ''}}> Native
		</div>
	</div>
	<hr>
	<div>If the student is none native English speaker, please check the student's appropiate level of proficiency in English</div>
	<div class="row checkbox-group">
		<div class="col-lg-2 col-md-3 col-sm-6">
			<input type="radio" name="std_english" value="1" {{isset($student) && $student->english_proficient == 1 ? 'checked': ''}}> Beginner
		</div>
		<div class="col-lg-3 col-md-4 col-sm-6">
			<input type="radio" name="std_english" value="2" {{isset($student) && $student->english_proficient == 2 ? 'checked': ''}}> Gaining confidence
		</div>
		<div class="col-lg-2 col-md-3 col-sm-6">
			<input type="radio" name="std_english" value="3" {{isset($student) && $student->english_proficient == 3 ? 'checked': ''}}>  Confidence
		</div>
		<div class="col-lg-2 col-md-3 col-sm-6">
			<input type="radio" name="std_english" value="4" {{isset($student) && $student->english_proficient == 4 ? 'checked': ''}}> Fluent
		</div>
		<div class="col-lg-2 col-md-3 col-sm-6">
			<input type="radio" name="std_english" value="5" {{isset($student) && $student->english_proficient == 5 ? 'checked': ''}}> Native
		</div>
	</div>	
</div>
<div id="trial-date">
<h4 class="text-sub-header">TRIAL DATE </h4>
<div class="row">
	<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="enrol-input fontcalender">
					<input type="text" name="trial_date_from" class="form-control date-input" placeholder="Date from"
						value="{{isset($enrolment) ? $enrolment->trial_from_date: ''}}" id="date_from" autocomplete="off">
						<label for="date_from"><i class="fa fa-calendar fa-lg" aria-hidden="true" ></i></label>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="enrol-input fontcalender">
					<input type="text" name="trial_date_to" class="form-control date-input" placeholder="Date to"
						value="{{isset($enrolment) ? $enrolment->trial_to_date: ''}}" id="date_to" autocomplete="off">
						<label for="date_to"><i class="fa fa-calendar fa-lg" aria-hidden="true" ></i></label>
				</div>
			</div>
	<div class="col-lg-4 col-md-4 col-sm-12">
		<div class="enrol-input">
			<select class="form-control" name="trial_class">
				<option>Class</option>
				@foreach($classes as $class)
				@if($class->id != 26)
				<option value="{{$class->id}}" {{isset($enrolment) && $enrolment->trial_class_id == $class->id ? 'selected' : ''}}>{{$class->class_name}}</option>
				@endif
				@endforeach
				@foreach($classes as $class)
				@if($class->id == 26)
				<option value="{{$class->id}}" {{isset($enrolment) && $enrolment->trial_class_id == $class->id ? 'selected' : ''}}>{{$class->class_name}}</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="enrol-input">
			
			<select name="trial_teacher" class="form-control">
				<option value="0">Teacher</option>
				@foreach($teachers as $teacher)
					<option value="{{$teacher->id}}" {{isset($enrolment) && $enrolment->trial_teacher_id == $teacher->id ? 'selected' : '' }}>{{$teacher->full_name}}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>
</div>
<div id="school-history">
<h4 class="text-sub-header">SCHOOL HISTORY </h4>
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="enrol-input">
			<input type="text" name="cur_school_name" class="form-control" placeholder="Current school name" value="{{isset($enrolment) ? $enrolment->school_history_name: ''}}">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="enrol-input">
			<input type="text" name="city_country" class="form-control" placeholder="City, Country" value="{{isset($enrolment) ? $enrolment->school_history_city: ''}}">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input fontcalender">
					<input type="text" name="school_his_date_from" class="form-control date-input"
						placeholder="Date from"
						value="{{isset($enrolment) ? $enrolment->school_history_from_date: ''}}" id="school_from" autocomplete="off">
						<label for="school_from"><i class="fa fa-calendar fa-lg" aria-hidden="true" ></i></label>	
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input fontcalender">
					<input type="text" name="school_his_date_to" class="form-control date-input" placeholder="Date to"
						value="{{isset($enrolment) ? $enrolment->school_history_to_date: ''}}" id="school_to" autocomplete="off">
						<label for="school_to"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i></label>
				</div>
			</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="enrol-input">
			<input type="text" name="lan_of_intruction" class="form-control" placeholder="Language of intruction" value="{{isset($enrolment) ? $enrolment->school_history_language_instruction: ''}}">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="enrol-input">
			<input type="text" name="withdrawal_reason" class="form-control" placeholder="Reason for withdrawal" value="{{isset($enrolment) ? $enrolment->school_history_withdrawal_reason: ''}}">
		</div>
	</div>
</div>
</div>
<div class="top-header">
	<h3 class="center text-header">HEALTH INFORMATION</h3>
</div>
<i>* Please submit copies of the child's offcial immunization records.</i><br>
<i>* Please submit the Evaluation form with doctor's signature.</i>
<div class="gender-container">
	<div class="row">
		<div class="col-md-4 col-sm-12">
			<div class="sm-content col-12">Height (cm)</div>
			<input type="text" name="height" class="b-bottom-input form-control" value="{{isset($student) ? $student->height: ''}}">
		</div>
		<div class="col-md-4 col-sm-12">
			<div class="sm-content col-12">Weight (kg)</div>
			<input type="text" name="weight" class="b-bottom-input form-control" value="{{isset($student) ? $student->weight: ''}}">
		</div>
		<div class="col-md-4 col-sm-12">
			<div class="sm-content col-12">Blood group</div>
			{{-- <input type="text" name="blood_group" class="b-bottom-input form-control"> --}}
			<select class="form-control" name="blood_group" >
				<option value="0">select blood group</option>
				@foreach($blood_groups as $blood_group)
				<option value="{{$blood_group->id}}" {{isset($student) && $student->bloodgroup_id == $blood_group->id ? 'selected' : ''}}>{{$blood_group->base_setup_name}}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-4 col-sm-12">
			<div class="sm-content col-12">Health info</div>
			<input type="text" onclick ="document.getElementById('health_info').click();" class="b-bottom-input form-control" value="{{isset($student) ? $student->health_info: ''}}">
			<input type="file" name="health_info" id="health_info">
			
		</div>
		<div class="sm-content col-12">Allergies to medications and/or foods</div>
		<div class="col-12">
			<input type="text" name="health_1" class="b-bottom-input form-control" value="{{isset($enrolment) ? $enrolment->health_information_1:''}}">
		</div>
		<div class="sm-content col-12">Medicaltions taken on a regular basic</div>
		<div class="col-12">
			<input type="text" name="health_2" class="b-bottom-input form-control" value="{{isset($enrolment) ? $enrolment->health_information_2:''}}">
		</div>
		<div class="sm-content col-12">If your child has any other medical condition which may affect his/her daily routine, please describe it here.</div>
		<div class="col-12">
			<input type="text" name="health_3" class="b-bottom-input form-control" value="{{isset($enrolment) ? $enrolment->health_information_3:''}}">
		</div>
		<div class="row col-12">
			<div class="col-1">
				<input type="checkbox" name="health_4" value="health_4" {{isset($enrolment)&&$enrolment->health_information_4=='health_4' ? 'checked':''}}>
			</div>
			<div class="col-11">
				We consent to the school nurse administering over-the-counter medicaltions for symptom relief of minor illnesses.
			</div>
		</div>
		<div class="row col-12">
			<div class="col-1">
				<input type="checkbox" name="health_5" value="health_5" {{isset($enrolment)&&$enrolment->health_information_5=='health_5' ? 'checked':''}}>
			</div>
			<div class="col-11">
				We agree to provide any professional reports relevant to my child's educational history e.g.Psychologist,Speech and Language Therapist, Psychiatrist, Behaviour Therapist, Ocupational or Physiotherapist.
			</div>
		</div>
		<div class="col-12 row">
			<div class="col-lg-9 col-md-8 col-sm-6">
				
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6">
				Signature of Parent/Guardian
				<input type="text" name="parent_signature" class="b-bottom-input form-control">
			</div>
		</div>
	</div>
</div>
<div class="top-header">
	<h3 class="center text-header">ADDITIONAL NEEDS</h3>
</div>
<div class="gender-container">
<div class="sm-content">Please describe any other Special Education Need that Saigon Star should be aware of in order to make your child's learning is a positive experience e.g. emotional/ behavioural difficulities, speech and language disability, general learning disability, autism spectrum disorder.</div>
<textarea name="add_needs" class="b-bottom-input form-control" rows="5">
	{{isset($enrolment) ? $enrolment->additional_needs :''}}
</textarea>

</div>
</div>