<div class="top-header">
	<h3 class="center text-header">FAMILY INFORMATION</h3>
</div>
	<h4 class="text-sub-header">FATHER INFORMATION </h4>
	<div class="main-family" id="father-22"> 
	<div class="row">
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="family_3_radio" id="father_radio" value="father" {{isset($parent_find) && $parent_find->father_role_is =='father' ? 'checked' : 'checked'}}> FATHER
		 </div>
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="family_3_radio" id="guardian_radio" value="guardian" {{isset($parent_find) && $parent_find->mother_role_is =='guardian' ? 'checked' : ''}}> GUARDIAN
		 </div>
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="primary_contact_radio" value="father" id="guardian_radio" {{isset($enrolment) && $enrolment->primary_contact == 'father' ? 'checked' : ''}}> PRIMARY CONTACT
		 </div>
	</div>
	
	<div class="father-info family-info">
		
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->fathers_name : ''}}" name="father_name" placeholder="Full name" id="father_name2" >
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->fathers_nationality : ''}}" name="father_nationality" placeholder="Nationality" id="father_nationality2">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->fathers_occupation : ''}}" name="father_occupation" placeholder="Occupation" id="father_occupation2">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->fathers_company : ''}}"  name="father_copany" placeholder="Company" id="father_company2">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->fathers_work_address : ''}}" name="father_work_address" placeholder="Work address" id="father_work_address2">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->fathers_mobile : ''}}" name="father_phone" placeholder="Phone" id="father_work_email2">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->fathers_email : ''}}" name="father_email" placeholder="Personal email" id="father_email2">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->fathers_first_language : ''}}" name="father_language" placeholder="First language" id="father_language2">
				</div>
			</div>
		</div>
		<div class="gender-container">
			<div>English level: please check a box</div>
			<div class="row checkbox-group">
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="father_english" value="1" {{isset($parent_find) && $parent_find->fathers_language_level == 1 ? 'checked' : ''}}> Beginner
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<input type="radio" name="father_english" value="2" {{isset($parent_find) && $parent_find->fathers_language_level == 2 ? 'checked' : ''}}> Gaining confidence
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="father_english" value="3" {{isset($parent_find) && $parent_find->fathers_language_level == 3 ? 'checked' : ''}}> Confidence
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="father_english" value="4" {{isset($parent_find) && $parent_find->fathers_language_level == 4 ? 'checked' : ''}}> Fluent
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="father_english" value="5" {{isset($parent_find) && $parent_find->fathers_language_level == 5 ? 'checked' : ''}}> Native
				</div>
			</div>	
		</div>
	
	</div>
	<br><br>
</div>

<div class="main-family" id="mother-22">
	<h4 class="text-sub-header">MOTHER INFORMATION </h4>
<div class="row">
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="family_4_radio" id="father_radio" value="mother" {{isset($parent_find) && $parent_find->mother_role_is =='mother' ? 'checked' : 'checked'}}> MOTHER
		 </div>
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="family_4_radio" id="mother_radio" value="guardian" {{isset($parent_find) && $parent_find->mother_role_is =='guardian' ? 'checked' : ''}}> GUARDIAN
		 </div>
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="primary_contact_radio" value="mother" id="guardian_radio" {{isset($enrolment) && $enrolment->primary_contact == 'mother' ? 'checked' : ''}}> PRIMARY CONTACT	
		 </div>
	</div>
	<div class="mother-info family-info">
		
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->mothers_name : ''}}" name="mother_name" placeholder="Full name" id="mother_name2">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->mothers_nationality : ''}}" name="mother_nationality" id="mother_nationality2" placeholder="Nationality">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->mothers_occupation : ''}}" name="mother_occupation" id="mother_occupation2" placeholder="Occupation">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->mothers_company : ''}}" name="mother_copany" placeholder="Company" id="mother_copany2">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->mothers_work_address : ''}}" name="mother_work_address" placeholder="Work address" id="mother_work_address2">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->mothers_mobile : ''}}" name="mother_phone" placeholder="Phone" id="mother_phone2">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->mothers_email : ''}}" name="mother_email" placeholder="Personal email" id="mother_email2">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="{{isset($parent_find) ? $parent_find->mothers_first_language : ''}}" name="mother_language" placeholder="First language" id="mother_language2">
				</div>
			</div>
		</div>
		<div class="gender-container">
			<div>English level: please check a box</div>
			<div class="row checkbox-group">
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="mother_english" value="1" {{isset($parent_find) && $parent_find->mothers_language_level == 1 ? 'checked' : ''}}> Beginner
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<input type="radio" name="mother_english" value="2" {{isset($parent_find) && $parent_find->mothers_language_level == 2 ? 'checked' : ''}}> Gaining confidence
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="mother_english" value="3" {{isset($parent_find) && $parent_find->mothers_language_level == 3 ? 'checked' : ''}}> Confidence
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="mother_english" value="4" {{isset($parent_find) && $parent_find->mothers_language_level == 4 ? 'checked' : ''}}> Fluent
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="mother_english" value="5" {{isset($parent_find) && $parent_find->mothers_language_level == 5 ? 'checked' : ''}}> Native
				</div>
			</div>	
		</div>
	</div>

		<h4 class="text-sub-header">SIBLINGS IN SCHOOL</h4>
	<div class="gender-container" id="list-sibling">
		@if(isset($siblings))
		@foreach($siblings as $sibling)
			<div class="row">
			<div class="col-md-5 col-sm-12 row">
				<div class="col-md-4 col-sm-12">
					<div class="sm-content">child's name</div>
				</div>
				<div class="col-md-8 col-sm-12">
					<input type="text" name="child_name" class="b-bottom-input form-control" value="{{$sibling->full_name}}" readonly="">
				</div>
			</div>
			<div class="col-md-4 col-sm-6 row">
				<div class="col-6">
					<div class="sm-content">Admission date</div>
				</div>
				<div class="col-6">
					<input type="text" name="child_enrol_date" class="b-bottom-input form-control" value="{{$sibling->admission_date}}" readonly="">
				</div>
			</div>
			<div class="col-md-3 col-sm-6 row">
				<div class="col-6">
					<div class="sm-content">Grade</div>
				</div>
				<div class="col-6">
					<input type="text" name="child_grade" class="b-bottom-input form-control" value="{{$sibling->class->class_name}}" readonly="">
				</div>
			</div>
		</div>
		@endforeach
		@endif
	</div>
	</div>
	

	
<div id="validate_2"></div>
<h4 class="text-sub-header">EMERGENCY CONTACT</h4>
<div class="gender-container">
		<div class="row" >
			<div class="col-lg-6 com-md-6 col-sm-12">
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="sm-content">First name</div>
					</div>
					<div class="col-sm-6 col-md-8">
						<input type="text" name="emergency_first_name" class="b-bottom-input form-control" id="emer_name_1" 
						value="{{isset($enrolment) ? $enrolment->emergency_contact_name : ''}}">
						<div class="validate" id="emergency_first_name_validate">*this field cannot be empty*</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 com-md-6 col-sm-12">
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="sm-content">Family name</div>
					</div>
					<div class="col-sm-6 col-md-8">
						<input type="text" name="emergency_family_name" class="b-bottom-input form-control" id="emer_name_2" 
						value="{{isset($enrolment) ? $enrolment->emergency_contact_family_name : ''}}">
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-sm-6 col-md-2">
						<div class="sm-content">Relationship to child</div>
					</div>
					<div class="col-sm-6 col-md-10">
						<input type="text" name="relationship_to_child" class="b-bottom-input form-control" id="relationship_to_child"
						value="{{isset($enrolment) ? $enrolment->emergency_relationship : ''}}">
					</div>
				</div>
			</div>
			<div class="col-lg-6 com-md-6 col-sm-12">
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="sm-content">Phone number 1</div>
					</div>
					<div class="col-sm-6 col-md-8">
						<input type="text" name="emergency_phone_1" class="b-bottom-input form-control" id="emer_phone_1" 
						value="{{isset($enrolment) ? $enrolment->emergency_contact_mobile_1 : ''}}">
						<div class="validate" id="emergency_phone_1_validate">*this field cannot be empty*</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 com-md-6 col-sm-12">
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="sm-content">Phone number 2</div>
					</div>
					<div class="col-sm-6 col-md-8">
						<input type="text" name="emergency_phone_2" class="b-bottom-input form-control" id="emer_phone_2"
						value="{{isset($enrolment) ? $enrolment->emergency_contact_mobile_2 : ''}}">
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-sm-6 col-md-2">
						<div class="sm-content">Contact address</div>
					</div>
					<div class="col-sm-6 col-md-10">
						<input type="text" name="emergency_address" class="b-bottom-input form-control" id="emer_address" 
						value="{{isset($enrolment) ? $enrolment->emergency_contact_address : ''}}">
						<div class="validate" id="emergency_address_validate">*this field cannot be empty*</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="top-header">
	<h3 class="center text-header">SCHOOL BUS SERVICE REQUESTED</h3>
</div>
<br>
<table class="table table-bordered bus-table">
  <tbody>
  	<tr>
  		<th scope="row" width="15%">
  			<input type="radio" name="bus_route" value="0" class="bus_radio" placeholder="0" checked="checked">
  		</th>
  		<td>NO BUS SERVICE</td>
  	</tr>
  	@if(isset($transports))
  	@foreach($transports as $transport)
    <tr>
      <th scope="row" width="15%"><input type="radio" {{isset($enrolment) && $enrolment->bus_service_request == $transport->id ? 'checked' : ''}} name="bus_route" value="{{$transport->id}}" class="bus_radio" placeholder="{{$transport->far}}"></th>
      <td>{{$transport->title}}</td>
    </tr>
    @endforeach
   @endif
  </tbody>
</table>
<br>
<div class="top-header">
	<h3 class="center text-header">LUNCH REQUESTED</h3>
</div>
<div class="gender-container">
	<div class="row">
		<div class="col-8">
			<div class="sm-content">Any special request for lunch?</div>
		</div>
		<div class="col-2">
			<input type="radio" value="0" name="have_lunch_request" {{isset($enrolment) && $enrolment->have_lunch_request != "0" ? 'checked' : ''}}> Yes
		</div>
		<div class="col-2">
			<input type="radio" value="1" name="have_lunch_request" {{isset($enrolment) && $enrolment->have_lunch_request == "1" ? 'checked' : ''}}> No
		</div>
	</div>
	
	<input type="text" name="lunch_request" id="lunch_request" class="b-bottom-input form-control" value="{{isset($enrolment) ? $enrolment->lunch_request : ''}}">
</div>
<!--<div class="top-header">-->
<!--	<h3 class="center text-header">SUPPORT</h3>-->
<!--</div>-->
<!--<div class="gender-container">-->
<!--	<div class="row">-->
<!--		<div class="col-lg-2 col-md-2 col-sm-5">-->
<!--			<div class="sm-content">EAL support</div>-->
<!--		</div>-->
<!--		<div class="col-lg-8 col-md-8 col-sm-4">-->
<!--			<input type="text" name="eal_support" class="b-bottom-input form-control" value="{{isset($enrolment) ? $enrolment->eal_support : ''}}" readonly="">-->
<!--		</div>-->
<!--		<div class="col-lg-2  col-md-2 col-sm-3">-->
<!--			<div class="sm-content">hour / week</div>-->
<!--		</div>-->
<!--	</div>-->
<!--	<div class="row">-->
<!--		<div class="col-sm-5 col-md-2 col-lg-2">-->
<!--			<div class="sm-content">SEN support</div>-->
<!--		</div>-->
<!--		<div class="col-sm-2 col-md-8 col-lg-8">-->
<!--			<input type="text" name="sen_support" class="b-bottom-input form-control" value="{{isset($enrolment) ? $enrolment->sen_support : ''}}" readonly="">-->
<!--		</div>-->
<!--		<div class="col-sm-5 col-md-2 col-lg-2">-->
<!--			<div class="sm-content">hour / week</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->

