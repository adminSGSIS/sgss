<div class="top-header">
	<h3 class="center text-header">FEES AND DISCOUNTS</h3>
</div>

<div class="gender-container">
	<h4 class="text-sub-header">FEES</h4>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">APPLICATION FEE</div>
		</div>
		<div class="col-5">
			<input type="text" max="25000000" min="0" name="application_fee" id="application_fee" class="b-bottom-input form-control number" value="2500000" readonly="">
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">REGISTRATION FEE</div>
		</div>
		<div class="col-5">
			<input type="text" max="46000000" min="0" name="registration_fee" id="registration_fee" class="b-bottom-input form-control number" value="46000000" readonly="">
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">SUPPLEMENT FEE</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->supplement_fee : '0'}}" max="46000000" min="0" name="supplement_fee" class="b-bottom-input form-control number" readonly="" id="supplement_fee">
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">LUNCH FEE</div>
		</div>
		<div class="col-5">
			<input type="text" max="46000000" min="0" name="lunch_fee" class="b-bottom-input form-control number" value="29125000" readonly="" id="lunch_fee">
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">TUITION FEE</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->tuition_fee : '0'}}" max="460000000" min="0" name="tuition_fee" class="b-bottom-input form-control number" readonly="" id="tuition_fee" >
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">SEN FEE</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->sen_fee : '0'}}" max="46000000" min="0" name="sen_fee" class="b-bottom-input form-control number" value="0" readonly="" id="sen_fee">
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">EAL FEE</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->eal_fee : '0'}}" max="46000000" min="0" name="eal_fee" class="b-bottom-input form-control number" id="eal_fee" readonly="">
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">BUS FEE</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->bus_fee : '0'}}" max="46000000" min="0" name="bus_fee" id="bus_fee" class="b-bottom-input form-control number" value="0" readonly="">
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">TOTAL FEE</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->total_fee : '0'}}" max="100000000000" min="0" name="total_fee" id="total_fee" class="b-bottom-input form-control number" readonly="">
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
			
		</div>
		
	</div>
	<div class="center">
			<a id="fees_cal" class="btn btn-primary calculate">calculate</a>
		</div>
</div>

<div class="gender-container">
	<h4 class="text-sub-header">DISCOUNTS</h4>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">LOYALTY & SIBLING DISCOUNT</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->loyalty_sibling_discount : '0'}}" maxlength="2" name="sibling_discount" id="sibling_discount" class="b-bottom-input form-control discount-input" value="0">
		</div>
		<div class="col-2">
			<div class="sm-content">%</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">BUS DISCOUNT </div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->bus_discount : '0'}}" maxlength="2" name="bus_discount" id="bus_discount" class="b-bottom-input form-control discount-input" value="0">
		</div>
		<div class="col-2">
			<div class="sm-content">%</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">LATE ENROLMENT DISCOUNT</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->late_enrol_discount : '0'}}" maxlength="2" name="late_enrol_discount" id="late_enrol_discount" class="b-bottom-input form-control discount-input" value="0">
		</div> 
		<div class="col-2">
			<div class="sm-content">%</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">LUNCH DISCOUNT</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->lunch_discount : '0'}}" maxlength="2" name="lunch_discount" id="lunch_discount" class="b-bottom-input form-control discount-input" value="0">
		</div>
		<div class="col-2">
			<div class="sm-content">%</div>
		</div>
	</div>
	<div class="row">
		<div class="col-5">
			<div class="sm-content">TOTAL FEE AFTER DISCOUNTS</div>
		</div>
		<div class="col-5">
			<input type="text" value="{{isset($enrolment) ? $enrolment->total_fee_after_discounts : ''}}"  max="100" min="0" name="total_fee_after_discount " id="total_fee_after_discount" class="b-bottom-input form-control number" readonly="" >
		</div>
		<div class="col-2">
			<div class="sm-content">vnd</div>
		</div>
	</div>
</div>


