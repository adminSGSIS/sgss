@extends('frontEnd.home.layout.front_master')

@push('css')
<link rel="stylesheet" href="{{asset('public/')}}/frontend/css/new_style.css" />
<link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/')}}/css/croppie.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endpush

@section('main_content')
<div class="container">
    <section class="student-details">
        <div class="row justify-content-center">
            <div class="col-5">
                <!-- Start Student Meta Information -->
                <div class="main-title text-center">
                    <h3 class="mt-20">Welcome {{ $student_detail->last_name }}</h3>
                </div>
                <div class="student-meta-box">
                    <div class="student-meta-top"></div>
                    <img class="student-meta-img img-100" src="{{ file_exists(@$student_detail->student_photo) ? asset($student_detail->student_photo) : asset('public/uploads/staff/demo/staff.jpg') }}" alt="">

                    <div class="white-box radius-t-y-0">
                        <div class="single-meta mt-10">
                            <div class="d-flex justify-content-between">
                                <div class="name">
                                    @lang('lang.student') @lang('lang.name')
                                </div>
                                {{-- {{ dd($student_detail) }} --}}
                                <div class="value">
                                    {{@$student_detail->first_name.' '.@$student_detail->last_name}}
                                </div>
                            </div>
                        </div>
                        <div class="single-meta">
                            <div class="d-flex justify-content-between">
                                <div class="name">
                                    @lang('lang.admission') @lang('lang.number')
                                </div>
                                <div class="value">
                                    {{@$student_detail->admission_no}}
                                </div>
                            </div>
                        </div>
                        <div class="single-meta">
                            <div class="d-flex justify-content-between">
                                <div class="name">
                                    @lang('lang.roll') @lang('lang.number')
                                </div>
                                <div class="value">
                                    {{@$student_detail->roll_no}}
                                </div>
                            </div>
                        </div>
                        <div class="single-meta">
                            <div class="d-flex justify-content-between">
                                <div class="name">
                                    @lang('lang.class')
                                </div>
                                <div class="value">
                                    @if($student_detail->className!="" && $student_detail->session_id!="")
                                    {{@$student_detail->className->class_name}}
                                    ({{@$academic_year->year}})
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="single-meta">
                            <div class="d-flex justify-content-between">
                                <div class="name">
                                    @lang('lang.section')
                                </div>
                                <div class="value">
                                    {{@$student_detail->section->section_name}}
                                </div>
                            </div>
                        </div>
                        <div class="single-meta">
                            <div class="d-flex justify-content-between">
                                <div class="name">
                                    @lang('lang.gender')
                                </div>
                                <div class="value">

                                    {{@$student_detail->gender !=""?$student_detail->gender->base_setup_name:""}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection