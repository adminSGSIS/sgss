<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('form')->middleware("XSS")->group(function() {
    Route::get('/', 'FormController@index');
    Route::get('/student/{admission_no}', 'FormController@student');
    Route::post('/', 'FormController@create');
    Route::get('/ajaxSectionStudent_fees', 'FormController@ajaxForm');
    Route::get('enrolment-form','FormController@enrolmentform');
    Route::get('enrolment-form/{admission_no}','FormController@searchStudent');
    Route::post('student-enrolment','FormController@storeEnrolment');
    Route::get('enrolment-completed','FormController@enrolcompleted');
    Route::get('get-siblings/{id}','FormController@getsiblings');
    Route::get('application/{admission_no}/{token}','FormController@application');
    Route::post('application','FormController@parentApplication');
    Route::post('application-parent','FormController@ApplicationParent');
});
