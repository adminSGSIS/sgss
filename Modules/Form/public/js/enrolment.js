


// $('.family-radio').on('change',function(){
// 	if($(this).attr('id') == 'father_radio')
// 	{
// 		$('.family-info').hide();
// 		// $('.family-info input').attr('disabled','disabled');
// 		$('.father-info').show();
// 		// $('.father-info input').removeAttr('disabled');
// 	}
// 	if($(this).attr('id') == 'mother_radio')
// 	{
// 		$('.family-info').hide();
// 		// $('.family-info input').attr('disabled','disabled');
// 		$('.mother-info').show();
// 		// $('.mother-info input').removeAttr('disabled');
// 	}
// 	if($(this).attr('id') == 'guardian_radio')
// 	{
// 		$('.family-info').hide();
// 		// $('.family-info input').attr('disabled','disabled');
// 		$('.guardian-info').show();
// 		// $('.guardian-info input').removeAttr('disabled');
// 	}
// });

$('#applicant_name').on('keyup',function(){
	if($('.applicant').is(':checked')) {
		var radio = $('.applicant:checked');
		var id = radio.attr('id');
		if(id == '1')
		{
			$('#father_name').val($(this).val());
		}
		if(id == '2')
		{
			$('#mother_name').val($(this).val());
		}
		if(id == '3')
		{
			$('#guardian_name').val($(this).val());
		}
	}
});

$('.applicant').on('change',function(){
	if($(this).attr('id') == '1')
	{
		$('#father_radio').attr('checked',true);
	}

	if($(this).attr('id') == '2')
	{
		$('#mother_radio').attr('checked',true);
	}

	if($(this).attr('id') == '3')
	{
		$('#guardian_radio').attr('checked',true);
	}

});

// $('.bus_table_radio').click(function(){
// 	if($(this).attr('id') == 'hide_bus_table')
// 	{
// 		$('.bus-table').hide();
// 		$('.bus_radio').attr('disabled',true);
// 	}
// 	if($(this).attr('id') == 'show_bus_table')
// 	{
// 		$('.bus-table').show();
// 		$('.bus_radio').removeAttr('disabled');
// 	}
// });

$('.bus_radio').click(function(){
	var bus_fee = parseInt($(this).attr('placeholder'));
	$('#bus_fee').val(bus_fee);
});

$('#enrol_in').on('change',function(){
	var enrol_in = $(this).val();
	if(enrol_in == 1 || enrol_in == 21 || enrol_in == 22)
	{
		$('#supplement_fee').val('12815000');
		$('#eal_fee').val('0');
		switch(enrol_in)
		{
			case '1': $('#tuition_fee').val('176400000'); break;
			case '21': $('#tuition_fee').val('245175000'); break;
			case '22': $('#tuition_fee').val('260400000'); break;
		}
	}
	if(enrol_in == 23 || enrol_in == 24 || enrol_in == 25 || enrol_in == 30 || enrol_in == 31 || enrol_in == 26)
	{
		$('#supplement_fee').val('15145000');
		$('#eal_fee').val('44000000');
		switch(enrol_in)
		{
			case '23': $('#tuition_fee').val('363000000'); break;
			case '24': $('#tuition_fee').val('363000000'); break;
			case '25': $('#tuition_fee').val('363000000'); break;
			case '30': $('#tuition_fee').val('363000000'); break;
			case '31': $('#tuition_fee').val('375000000'); break;
			case '26': $('#tuition_fee').val('375000000'); break;
		}
	}
});

$('#fees_cal').on('click',function(){
	var fee_1 = parseInt($('#application_fee').val().replace(",",""));
	var fee_2 = parseInt($('#registration_fee').val().replace(",",""));
	var fee_3 = parseInt($('#supplement_fee').val().replace(",",""));
	var fee_4 = parseInt($('#lunch_fee').val().replace(",",""));
	var fee_5 = parseInt($('#tuition_fee').val().replace(",",""));
	var fee_6 = parseInt($('#sen_fee').val().replace(",",""));
	var fee_7 = parseInt($('#eal_fee').val().replace(",",""));
	var fee_7 = parseInt($('#bus_fee').val().replace(",",""));

	var total = fee_1+fee_2+fee_3+fee_4+fee_5+fee_6+fee_7;

	$('#total_fee').val(total);
	$('#total_fee_after_discount').val(total);
});

$('.discount-input').on('keyup',function(){

	var tuition_discount = parseInt($('#sibling_discount').val()) + parseInt($('#late_enrol_discount').val());

	var bus_discount = parseInt($('#bus_discount').val());
	var lunch_discount = parseInt($('#lunch_discount').val());

	var fee_1 = parseInt($('#application_fee').val().replace(",","")) ;
	var fee_2 = parseInt($('#registration_fee').val().replace(",",""));
	var fee_3 = parseInt($('#supplement_fee').val().replace(",",""));
	var fee_4 = parseInt($('#lunch_fee').val().replace(",","")) / 100 * (100 - lunch_discount);
	var fee_5 = parseInt($('#tuition_fee').val().replace(",","")) / 100 * (100 - tuition_discount);
	var fee_6 = parseInt($('#sen_fee').val().replace(",",""));
	var fee_7 = parseInt($('#eal_fee').val().replace(",",""));
	var fee_7 = parseInt($('#bus_fee').val().replace(",","")) / 100 * (100 - bus_discount);
	var total = fee_1+fee_2+fee_3+fee_4+fee_5+fee_6+fee_7;
	$('#total_fee_after_discount').val(total);
});

// $('#select-family').on('click',function(){
// 	$('#family-select').toggle();
// });

// $('#add-family').on('click',function(){
// 	$('#family').toggle();
// 	$('#family input').val("");
// 	$('#main-family input').removeAttr('disabled');
// 	$('#family-select').hide();
// 	$('#parent_selected').val('0');
// });

$(document).ready(function() {
  var current_step = 1;
  $('.niceSelect').niceSelect();
  $('.date-input').datepicker({
  	format: 'dd/mm/yyyy'
  	});

  $('#std_photo_btn_real').change(function(){
        const file = this.files[0];
        console.log(file);
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            console.log(event.target.result);
            $('#imgPreview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
  });

	$('#step1').click(function(){
		current_step = 1;
		$('.collapse').hide();
		$('#step-1').show();
		$(window).scrollTop($('#scroll').offset().top);
	});
	$('#step2').click(function(){
		if (checkrequirefields1() == false) 
		{
			$(window).scrollTop($('.btn-group-enrol').offset().top);
			return false;
		}
		else{
			current_step = 2;
			$('.collapse').hide();
			$('#step-2').show();
			$(window).scrollTop($('#scroll').offset().top);
		}
	});
	$('#step3').click(function(){
		current_step = 3;
		$('.collapse').hide();
		$('#step-3').show();
		$(window).scrollTop($('#scroll').offset().top);
	});
	$('#step4').click(function(){

		current_step = 4;
		$('.collapse').hide();
		$('#step-4').show();
		$(window).scrollTop($('#scroll').offset().top);
		
	});
	$('#step5').click(function(){
		// if(checkrequirefields2() == false)
		// {
		// 	$(window).scrollTop($('#validate_2').offset().top-100);
		// 	return false;
		// }
		// else{
			current_step = 5;
			$('.collapse').hide();
			$('#step-5').show();
			$(window).scrollTop($('#scroll').offset().top);
		// }
		
	});
	$('#step6').click(function(){
			current_step = 6;
			$('.collapse').hide();
			$('#step-6').show();
			$(window).scrollTop($('#scroll').offset().top);
		
		
	});
	


	$('#std_photo_btn').click(function(){
		$('#std_photo_btn_real').click();
	});
 
  $('#enrol-next').on('click',function(){
  		
  		if(current_step <=6 && current_step >= 1)
  		{
  			if (current_step == 6) {return false;}
			switch(current_step)
			{
				case 1: $('#step2').trigger( "click" ); break;
				case 2: $('#step3').trigger( "click" ); break;
				case 3: $('#step4').trigger( "click" ); break;
				case 4: $('#step5').trigger( "click" ); break;
				case 5: $('#step6').trigger( "click" ); break;
				
			}

		}
  	});
  $('#enrol-prev').on('click',function(){
  	
  	if(current_step <=6 && current_step >= 1)
  	{
  		if (current_step == 1) {return false;}
  		switch(current_step)
  		{
  			case 2: $('#step1').trigger( "click" ); break;
  			case 3: $('#step2').trigger( "click" ); break;
  			case 4: $('#step3').trigger( "click" ); break;
  			case 5: $('#step4').trigger( "click" ); break;
  			case 6: $('#step5').trigger( "click" ); break;
  			case 7: $('#step6').trigger( "click" ); break;
  			
  		}
  	}
  });


});

$('.step-enrol').click(function(){
	if($(this).attr('id') == "step7")
	{
		return false;
	}
	if($(this).attr('id') == "step2")
	{
		if(checkrequirefields1() == false)
		{
			return false;
		}
	}
	
	$('.step-enrol').removeClass('show-step');
	$('.enrol-icon').removeClass('show');
	$('.enrol-icon').addClass('hide');
	$(this).addClass('show-step');
	$(this).find('.enrol-icon').addClass('show');

});



function checkrequirefields1(){
	var pass = true;
	// if($('#f_name').val()=="")
	// {
	//     $('#f_name_validate').show();
	// 	pass = false;
	// }
	// if($('#l_name').val()=="")
	// {
	//     $('#l_name_validate').show();
	// 	pass = false;
	// }
	// if($('#date_of_birth').val()=="")
	// {
	//     $('#date_of_birth_validate').show();
	// 	pass = false;
	// }
	// if($('#nationality_1').val()=="")
	// {
	//     $('#nationality_1_validate').show();
	// 	pass = false;
	// }
	// if($('#enrol_in option:selected').val()=="0")
	// {
	//     $('#enrol_in_validate').show();
	// 	pass = false;
	// }
	return pass;
}

function checkrequirefields2(){
	var pass = true;
	// if($('#emer_name_1').val()=="")
	// {
	//     $('#emergency_first_name_validate').show();
	// 	pass = false;
	// }
	// if($('#emer_phone_1').val()=="")
	// {
	//     $('#emergency_phone_1_validate').show();
	// 	pass = false;
	// }
	// if($('#emer_address').val()=="")
	// {
	//     $('#emergency_address_validate').show();
	// 	pass = false;
	// }
	return pass;
}

$(document).ready(function() {
	$('#mother-1').hide();
	$('#mother-1 input').attr('disabled','disabled');
	$('#father-2').hide();
	$('#father-2 input').attr('disabled','disabled');


	$('.number').number( true);
});

	$('#applicant_select').on('change',function(){
		if($('#applicant_select option:selected').val() == 1)
		{
			$('#mother-1').hide();
			$('#mother-1 input').attr('disabled','disabled');

			$('#mother-2').show();
			$('#mother-2 input').removeAttr('disabled');
			
			$('#father-1').show();
			$('#father-1 input').removeAttr('disabled');

			$('#father-2').hide();
			$('#father-2 input').attr('disabled','disabled');
			
		}

		if($('#applicant_select option:selected').val() == 2)
		{
			$('#mother-1').show();
			$('#mother-1 input').removeAttr('disabled');

			$('#mother-2').hide();
			$('#mother-2 input').attr('disabled','disabled');
			
			$('#father-1').hide();
			$('#father-1 input').attr('disabled','disabled');

			$('#father-2').show();
			$('#father-2 input').removeAttr('disabled');
			
		}
	});

	$('#print-trial').on('click',function(){
		$('#step-1,#step-2,#step-3,#step-4,#step-5').show();
		window.print();
		$('#step-1,#step-3,#step-4,#step-5').hide();
});

	