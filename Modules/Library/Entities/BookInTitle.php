<?php

namespace Modules\Library\Entities;

use Illuminate\Database\Eloquent\Model;

class BookInTitle extends Model
{
    protected $table = "book_in_title";

    public function Book()
    {
    	return $this->belongsTo('App\SmBook','book_title_id','id');
    }

    public function Shelf()
     {
     	return $this->belongsTo('Modules\Library\Entities\Shelves','id_shelf','id');
     }
}
