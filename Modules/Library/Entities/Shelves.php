<?php

namespace Modules\Library\Entities;

use Illuminate\Database\Eloquent\Model;

class Shelves extends Model
{
     protected $table = "shelves";

     public function Books()
     {
     	return $this->HasMany('Modules\Library\Entities\BookInTitle','id_shelf','id');
     }
}
