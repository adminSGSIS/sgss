<?php

namespace Modules\Library\Entities;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = "rental_ticket";

    public function User()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }

    public function Books()
    {
    	return $this->belongsToMany('Modules\Library\Entities\BookInTitle','Modules\Library\Entities\TicketBook','ticket_id','book_id')->withPivot('return_day');
    }
}
