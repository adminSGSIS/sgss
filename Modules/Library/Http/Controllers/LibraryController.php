<?php

namespace Modules\Library\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use Modules\Library\Entities\Ticket;
use Modules\Library\Entities\TicketBook;
use Modules\Library\Entities\BookInTitle;
use Modules\Library\Entities\Shelves;
use Brian2694\Toastr\Facades\Toastr;
use Auth;
use App\SmBook;
use Carbon\Carbon;
use App\SmBookCategory;

class LibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {   
        $covers = SmBook::all()->count();
        $books = BookInTitle::all()->count();
        $cates = SmBookCategory::all()->count();$patrons = User::all()->where('role_id','!=',3);
        $tickets = Ticket::all()->sortByDesc("id");
        
        $shelves = Shelves::all()->count();
        $notices = $tickets->where(Carbon::today(),'<','exp_day')->where('status','!=','returned');
        return view('library::index',compact('patrons','tickets','covers','books','cates','notices','shelves'));
    }

    public function getaddticket()
    {
        $patrons = User::all()->where('role_id','!=',3);
        $titles = SmBook::all();
        $books = BookInTitle::all();
        return view('library::add-ticket',compact('books','patrons','titles'));
    }
    
    public function addTicket(Request $request)
    {
       
        $validatedData = $request->validate([
            'patron_id' => 'required',
            
        ]);
        $check = Ticket::where('user_id',$request->patron_id)->orderBy('created_at','desc')->first();
        if(isset($check) && $check->status != 'returned')
        {
            $error = "User have not complete return the latest ticket";
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('error', $error);
        }
        $ticket = new Ticket();
        $ticket->user_id = $request->patron_id;
        $ticket->start_day = date('Y-m-d',strtotime($request->startDay));
        if($request->expDay!=null)
        {
            $ticket->exp_day = date('Y-m-d',strtotime($request->expDay));
        }
        $ticket->created_by = Auth::user()->full_name;
        $ticket->status = 'active';
        $ticket->save();

        for ($i=0; $i < count($request->list_code) ; $i++) { 
            $book = BookInTitle::where('barcode',$request->list_code[$i])->first();
            $book->borrowed = 1;
            $book->save();

            $pivot = new TicketBook();
            $pivot->ticket_id = $ticket->id;
            $pivot->book_id = $book->id;
            $pivot->book_title_id = $book->Book->id;
            $pivot->barcode = $request->list_code[$i];
            $pivot->save();
        }
        
        Toastr::success('Operation successful', 'Success');
        return redirect('library');

    }

    public function detail ($id)
    {
        try{
            $detail = Ticket::find($id);
            
            foreach ($detail->Books as $book) {
                $list_barcode []= $book->barcode;
            }
            
            return view('library::detail',compact('list_barcode','id','detail'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
        
    }

    public function createTicket(Request $request)
    {

        for ($i=0; $i < count($request->book_ids) ; $i++) { 
            $new = new TicketBook();
            $new->ticket_id = $request->ticket_id;
            $new->book_id = $request->book_ids[$i];
            $new->qty = $request->book_qty[$i];
            $new->save();
        }
        $update = Ticket::find($request->ticket_id);
        $update->status = "Actice";
        $update->save();

        return redirect('library'); 
    }

    public function findBook($code)
    {
        $book = BookInTitle::where('barcode',$code)->first();
        $title = SmBook::find($book->book_title_id);
        return response()->json([
            'book' => $book,
            'title' => $title
        ]);
    }

    public function returnTicket(Request $request)
    {
        
        for ($i=0; $i < count($request->return_books) ; $i++) { 
            $update = BookInTitle::where('barcode',$request->return_books[$i])->first();
            $update->status = $request->status[$i];
            $update->borrowed = 0;
            $update->save();

            $update2 = TicketBook::where('ticket_id',$request->ticket_id)->where('barcode',$request->return_books[$i])->first();
            $update2->return_day = Carbon::now();
            $update2->return_status = $request->status[$i];
            $update2->save();
                
        }

        $update3 = Ticket::find($request->ticket_id);
        $update3->status = 'returned';
        $update3->return_day = Carbon::today();
        $update3->save();

        Toastr::success('Operation successful', 'Success');
        return redirect('library'); 
            
    }


    public function addBook(Request $request)
    {
        $check = BookInTitle::where('barcode',$request->barcode)->first();
        if($check == null)
        {
            $create = new BookInTitle();
            $create->book_title_id = $request->title_id;
            $create->barcode = $request->barcode;
            $create->rack = Shelves::find($request->rack)->title;
            $create->id_shelf = $request->rack;
            $create->status = $request->status;
            $create->save();

            $qty = SmBook::find($request->title_id);
            $qty->quantity++;
            $qty->save();

            return response()->json([
                'id' => $create->id,
                'success' => 'The book has been added!!',
                'error' => 'none',
            ]);
        }
        else
        {
            return response()->json([
                'error' => 'This barcode has already existed!!',
                'success' => 'none',
            ]);
        }

        
        
    }

    

    public function borrowCards()
    {
        $cards = Ticket::all();
        return view('library::borrow-cards',compact('cards'));
    }

    public function search()
    {
        $cates = SmBookCategory::all();
        $books = BookInTitle::all();
        $cards = Ticket::all();
        $shelves = Shelves::all();
        return view('library::search',compact('cards','books','cates','shelves'));
    }

    public function shelves()
    {
        $shelves = Shelves::all();
        return view('library::shelves',compact('shelves'));
    }

    public function addShelf(Request $request)
    {
        try{
            $add = new Shelves();
            $add->title = $request->shelf_title;
            $add->save();

           Toastr::success('Operation successful', 'Success');
           return redirect()->back(); 
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function editShelf($id)
    {
        try{
            $editData = Shelves::find($id);
            $shelves = Shelves::all();
            return view('library::shelves',compact('editData','shelves'));

        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }

    }

    public function updateShelf(Request $request,$id)
    {
        try{
            $update = Shelves::find($id);
            $update->title = $request->shelf_title;
            $update->save();
           
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }

    }

     public function deleteShelf($id)
    {

        try{
            $delete = Shelves::find($id);
            $delete->delete();
            
           
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }

    }

    public function deleteBook($id)
    {
        $check = BookInTitle::find($id);
        if($check->borrowed == 1)
        {
            return response()->json([
                'error' => 'This book cannot be deleted now!!',
                'success' => 'none',
            ]);
        }
        else
        {
            $check->delete();
            return response()->json([
                'error' => 'none',
                'success' => 'yes',
            ]);
        }
    }

    public function patrons()
    {
        $patrons = User::all()->where('role_id','!=',3);
        return view('library::patrons',compact('patrons'));
    }

    public function patron($id)
    {
        $patron = User::find($id);
        return view('library::patron-detail',compact('patron'));
    }

    public function extend(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required',
        ]);

        try{
            $extend = Ticket::find($request->id);
            if($request->new_due_date != null)
            {
                $extend->exp_day = date('Y-m-d',strtotime($request->new_due_date));
            }
            else
            {
                $extend->exp_day = null;
            }
            $extend->save();
            
           
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function deleteCard($id)
    {
        try{
            $delete = Ticket::find($id);
            if($delete->status != 'active')
            {
                $delete->delete();
            }
            else
            {
                Toastr::error('Operation Failed', 'Cannot delete');
                return redirect()->back(); 
            }
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }
}
