@extends('backEnd.master')
@section('mainContent')

<style type="text/css">
    .qty{
        width: 100px;
    }
</style>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" action="{{url('library/add-ticket')}}">
	@csrf
	<h2>Create Ticket</h2>
<div class="input-form white-box">
	<div class="row">
		<div class="col-lg-4">
            <div class="input-effect">
                
                 <select class="niceSelect w-100 bb form-control" name="patron_id" required="">
                    <option selected="" value="">* Select patron</option>
                    @foreach($patrons as $patron)
                    <option value="{{$patron->id}}">{{$patron->full_name}}</option>
                    @endforeach
                 </select>
                 
                 @if (session('error'))
                    
                         <strong style="color: red;">{{session('error')}}</strong>
                   
                  @endif 
                <span class="focus-border"></span>

            </div>
        </div>
		<div class="col-lg-3">
			<div class="input-effect">
                 <input class="primary-input date read-only-input has-content" id="startDay" name="startDay" type="text" readonly="" required="" value="<?php echo date('m/d/Y'); ?>" >
                 <label>Start date</label>
                 <span class="focus-border"></span>
            </div>
		</div>
		<div class="col-lg-3">
			<div class="input-effect">
                 <input class="primary-input date read-only-input has-content" id="expDay" name="expDay" type="text" readonly="" required="" value="<?php echo date('m/d/Y', strtotime('+7 days'));?>">
                 <label>Due date</label>
                 <span class="focus-border"></span>
            </div>
		</div>
    <div class="col-lg-2">
      <div class="input-effect">

                 <select id="period">
                   <option selected value="7d">7 days</option>
                   <option value="14d">14 days</option>
                   <option value="21d">21 days</option>
                   <option value="1m">1 month</option>
                   <option value="nodue">no due date</option>
                 </select><br>
                 <span>or click the duedate field and select custom due date</span>
                 <span class="focus-border"></span>
            </div>
    </div>
		
		<div class="col-lg-4">
            <br>
			<div class="input-effect">
                
              	
                <input class="primary-input " id="barcode" type="text" value="" autofocus="">
                 <label>BarCode</label>
                 <span class="focus-border"></span>

     		</div>
		</div>
		

                				    
                               

	</div>
	<div id="post_list">
        
    </div>
	
<br><br>
<h3>Book list</h3>
                    <table class="table">

                            <thead>
                                
                                <tr>
                                	<th width="25%">book name</th>
                                    <th>ISBN code</th>
                                    <th>rack number</th>
                                    <th>BarCode</th>
                                    <th>Status</th>
                                    <th>options</th>
                                </tr>
                            </thead>

                            <tbody>
                            	
                              
                            </tbody>

                        </table>



<br>
                                    <div class="col-lg-12 text-center">
                                        <br><br>
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" type="Submit">
                                            <span class="ti-check"></span>
                                            Save
                                        </button>
                                    </div>
</div>
</form>
@endsection

@section('script')
<script src="{{url('Modules/Library/public/barcode-scanner/jquery-code-scanner.js')}}"></script>
<script type="text/javascript">
    $( document ).ready(function() {    
    
    var titles = <?php echo json_encode($titles); ?>;
    var books = <?php echo json_encode($books); ?>;
    var selected =[];
    	$('#barcode').on('input',function(){
            barcode = $(this).val().replace(" ","");
            check = 0;
            $.each(selected,function (i,s){
                if(s==barcode){check = 1;}
            });
            if(check == 1) {check =0;return false;}
            $.each(books, function(i, book) {
                if (book.barcode == barcode) {
                    if(book.borrowed==1 || book.status=='lost')
                    {
                        alert('this book has already been borrowed or lost!');
                        $('#barcode').val("");
                    }
                    else
                    {
                        $.each(titles, function(i, title) {
                            if (title.id == book.book_title_id) {
                                 $('tbody').append('<tr><td>'+(title.book_title)+'</td><td>'+(title.isbn_no)+'</td><td>'+(book.rack)+'</td><td>'+(book.barcode)+'</td><td>'+(book.status)+'</td><td><a href="#" id="delete'+(book.barcode)+'" class="fa fa-trash fa-2x"></a></td></tr>');
                                 selected.push(barcode);
                                 $('#post_list').append('<input type="hidden" id="'+(barcode)+'" name="list_code[]" value="'+(barcode)+'">');
                                 $('#barcode').val("");
                            }
                        });
                    }
                    
                }
            });
            
        });

       $(document).on('click', '.fa-trash', function () {
            barcode = $(this).attr('id').replace('delete','');
            $(this).parent().parent().remove();
            selected.splice(selected.indexOf(barcode),1);
            $('#'+(barcode)+'').remove();
        });

       $('#period').on('change',function(){

            if($(this).val()=='7d')
            {
               date = moment().add(7,'d').toDate();
            }
            if($(this).val()=='14d')
            {
               date = moment().add(14,'d').toDate();
            }
            if($(this).val()=='21d')
            {
               date = moment().add(21,'d').toDate();
            }
            if($(this).val()=='1m')
            {
               date = moment().add(1,'M').toDate();
            }
            if($(this).val()=='nodue')
            {
               $('#expDay').val('');
               return false;
            }
           
            day = '';
            if(date.getDate() < 10)
            {
                day='0'+date.getDate();
            }
            else
            {
              day = date.getDate();
            }
            month = date.getMonth()+1;
            year = date.getFullYear();
            $('#expDay').val(month+'/'+day+'/'+year);
            
       });

       

    });

    $('#barcode').codeScanner({

      onScan: function ($element, code) {
        console.log(code);
      }
    });

    var barcode="";
    $(document).keydown(function(e) 
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13)// Enter key hit
        {
            $('#barcode').val(barcode).trigger('input');
            barcode="";
        }
        else if(code==9)// Tab key hit
        {
            $('#barcode').val(barcode).trigger('input');
            barcode="";
        }
        else
        {
            barcode=barcode+String.fromCharCode(code);
        }
    });
</script>
@endsection