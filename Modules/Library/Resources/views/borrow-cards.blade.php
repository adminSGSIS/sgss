@extends('backEnd.master')
@section('mainContent')

<h1>Borrow Cards Manage</h1>
<div class="row white-box">
      <div class="col-lg-3">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <input class="primary-input form-control" type="text" name="name" autocomplete="off" value="" id="name">
              <label>Patron name </label>
              <span class="focus-border"></span>
                                                  </div>
      </div>
      
      
      <div class="col-lg-3">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <input class="primary-input form-control date" type="text" name="rDate" autocomplete="off" id="rDate">
              <label>Return Date</label>
              <span class="focus-border"></span>
                                                  </div>
      </div>
      

      <div class="col-lg-3">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <select id="status" class="niceSelect w-100 bb form-control" name="status">
                  <option>Status</option>
                  <option value="">All</option>
                  <option value="active">Active</option>
                  <option value="expired">Expired</option>
                  <option value="returned">Returned</option>
              </select>
              
              <span class="focus-border"></span>
                                                  </div>
      </div>
    </div>
          <br>	
<h1>List all transactions</h1>
<a href="{{url('library/addticket')}}" type="button" class="primary-btn fix-gr-bg" data-toggle="tooltip">
                                            <span class="ti-check"></span>
                                            Add
</a>
<div class="col-lg-12">

                        <table id="table" class="display school-table library-table" cellspacing="0" width="100%">

                            <thead>
                                @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message-success-delete') }}
                                        </div>
                                        @elseif(session()->has('message-danger-delete'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('message-danger-delete') }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                	<th width="5%">id</th>
                                    <th width="20%">Patron name</th>
                                    <th>start date</th>
                                    <th>due date</th>
                                    <th>return date</th>
                                    <th>created by</th>
                                    <th>status</th>
                                    <th>@lang('lang.action')</th>
                                </tr>
                            </thead>

                            <tbody>
                               @php $i=1; @endphp
                               @foreach($cards as $card)
                                <tr>
                                	<td>{{$i++}}</td>
                                    <td>{{$card->User->full_name}}</td>
                                    <td>{{date('m/d/Y', strtotime($card->start_day))}}</td>
                                    <td>{{date('m/d/Y', strtotime($card->exp_day))}}</td>
                                    <td>{{$card->return_day!=""? date('m/d/Y', strtotime($card->return_day)) : ""}}</td>
                                    <td>{{$card->created_by}}</td>
                                    <td>{{$card->status}}</td>
                                    <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            @lang('lang.select')
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        	@if(in_array(302, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            	<a class="dropdown-item" href="{{url('library/ticket-detail')}}/{{$card->id}}">@lang('detail')</a>
                                        	@endif
                                           
	                                        @if(in_array(303, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
	                                            <a class="dropdown-item" data-toggle="modal" data-target="#deleteCard{{$card->id}}"  href="#">@lang('lang.delete')</a>
	                                        @endif
                                       </div>
                                   </div>
                                   <div class="modal fade admin-query" id="deleteCard{{$card->id}}" >
                                  <div class="modal-dialog modal-dialog-centered">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <h4 class="modal-title">@lang('lang.delete')</h4>
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          </div>

                                          <div class="modal-body">
                                              <div class="text-center">
                                                  <h4>@lang('lang.are_you_sure_to_delete')</h4>
                                              </div>

                                              <div class="mt-40 d-flex justify-content-between">
                                                  <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
                                                  
                                                  <a href="{{url('library/deleteCard/'.$card->id)}}" class="primary-btn fix-gr-bg" type="submit">@lang('lang.delete')</a>
                                                   
                                              </div>
                                          </div>

                                      </div>
                                  </div>

                              </div>
                               </td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    var table = $('#table').DataTable({
        responsive: true
    });
    $('.dataTables_filter').remove();

    $('#name').on( 'keyup', function () {
    table
        .columns( 1 )
        .search( this.value )
        .draw();
    });
    $('#status').on( 'change', function () {
    table
        .columns( 6 )
        .search( this.value )
        .draw();
    });

    $('#rDate').on('change',function(){
    
    table
        .columns( 4 )
        .search( this.value )
        .draw();
        
    });

    
});


</script>
@endsection