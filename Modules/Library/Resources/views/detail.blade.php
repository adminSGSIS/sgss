@extends('backEnd.master')
@section('mainContent')
<form method="post" action="{{url('library/returnTicket')}}">
	@csrf
	<input type="hidden" name="ticket_id" value="{{$id}}">
<div class="input-form white-box">
	<div class="row">
		<div class="col-lg-4">
			<div class="input-effect">
              	<div class="input-effect">
                 <input class="primary-input" readonly="" value="{{$detail->User->full_name}}" >
                 <label>Patron name</label>
                 <span class="focus-border"></span>
            </div>
     		</div>
		</div>
		<div class="col-lg-4">
			<div class="input-effect">
                 <input class="primary-input" id="startDay" type="text" readonly="" required="" value="{{$detail->start_day}}" >
                 <label>Start date</label>
                 <span class="focus-border"></span>
            </div>
		</div>
		<div class="col-lg-4">
			<div class="input-effect">
                 <input class="primary-input" id="expDay" type="text" readonly="" required="" value="{{$detail->exp_day}}">
                 <label>Due date</label>
                 <span class="focus-border"></span>
            </div>
		</div>
		<div class="col-lg-4">
            <br>
			<div class="input-effect">
                
              	
                <input class="primary-input " id="barcode" type="text"  value="">
                 <label>BarCode</label>
                 <span class="focus-border"></span>

     		</div>
		</div>

                               

	</div>
	<br>
	<h3>Book list</h3>
                    <table class="table">

                            <thead>
                                
                                <tr>
                                	<th width="25%">book name</th>
                                    <th>ISBN code</th>
                                    <th>rack number</th>
                                    <th>BarCode</th>
                                    <th>Return status</th>
                                    <th>Return</th>
                                </tr>
                            </thead>

                            <tbody>
                            	@foreach($detail->Books as $book)
                            	<tr>
                            		<td>{{$book->Book->book_title}}</td>
                            		<td>{{$book->Book->isbn_no}}</td>
                            		<td>{{$book->rack}}</td>
                            		<td>{{$book->barcode}}</td>
                            		
                            		
                            			@if($book->pivot->return_day == null)
                            			<td>
                            			<select required="" id="select{{$book->barcode}}" name="status[]">
                            				<option value="">status</option>
                            				<option disabled=""  value="good">Good</option>
                            				<option disabled=""  value="damaged">Damaged</option>
                            				<option  value="lost" >Lost</option>
                            			</select>
                            			<input type="hidden" id="book{{$book->barcode}}" value="{{$book->barcode}}" name="return_books[]">
                            			</td>
                            			<td>
                            			<i id="check{{$book->barcode}}" style="color: #00ff00;display: none;" class="fa fa-check-circle fa-2x"></i>
                            			</td>
                            			@else
                            			<td>{{$book->status}}</td>
                            			<td>
                            			<i id="check{{$book->barcode}}" style="color: #00ff00;display:@if($book->status == 'lost') none @else block @endif;" class="fa fa-check-circle fa-2x"></i>
                            			</td>
                            			@endif
                            		
                            		
                            	</tr>
                              	@endforeach
                            </tbody>

                        </table>
<br>
								@if($detail->status != "returned")
                                    <div class="col-lg-12 text-center">
                                        <br><br>
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" type="Submit">
                                            <span class="ti-check"></span>
                                            Submit
                                        </button>
                                    </div>
                                @endif
</div>
	
</div>
<br><br><br>


</form>
@endsection

@section('script')
<script type="text/javascript">
    $( document ).ready(function() {   
    	list_barcode =  <?php echo json_encode($list_barcode); ?>;
    	$('#barcode').on('input',function(){
            barcode = $(this).val();
            $.each(list_barcode,function (i,v){
            	if(v==barcode)
            	{
            		$('#check'+(barcode)+'').css('display','block');
		            $('#select'+(barcode)+' option[value="good"]').attr("disabled", false);
		            $('#select'+(barcode)+' option[value="good"]').attr("selected", true);
		            $('#select'+(barcode)+' option[value="damaged"]').attr("disabled", false);
		            $('#select'+(barcode)+' option[value="lost"]').attr("disabled", true);
		            $('#barcode').val("");
            	}
                
            });
            
        });

       
    });
</script>
@endsection