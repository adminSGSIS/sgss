@extends('backEnd.master')
@section('mainContent')

<h1>Saigon Star Library Management System </h1>
<div class="row">
        
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('book-list')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Books Manage</h3>
                            <p class="mb-0">In total {{$covers}} covers</p>
                        </div>
                        <h1 class="gradient-color2">
                            {{$books}}
                        </h1>
                    </div>
                </div>
            </a>
        </div>
                
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('library/borrow-cards')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Transactions Manage</h3>
                            <p class="mb-0">Total transaction</p>
                        </div>
                        <h1 class="gradient-color2">
                            {{count($tickets)}}
                        </h1>
                    </div>
                </div>
            </a>
        </div>
                        
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('library/shelves')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Bookshelves Manage</h3>
                            <p class="mb-0">Total Bookshelf</p>
                        </div>
                        <h1 class="gradient-color2">
                            {{$shelves}}    
                        </h1>
                    </div>
                </div>
            </a>
        </div>
                
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('book-category-list')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Categories Manage</h3>
                            <p class="mb-0">Total category</p>
                        </div>
                        <h1 class="gradient-color2">
                             {{$cates}}    
                        </h1>
                   </div>
               </div>
           </a>
       </div>

       <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('library/patrons')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Patrons Manage</h3>
                            <p class="mb-0">Total patron</p>
                        </div>
                        <h1 class="gradient-color2">
                               
                        </h1>
                   </div>
               </div>
           </a>
       </div>

       <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('library/search')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Advanced Search</h3>
                            <p class="mb-0">Click to use the advanced search</p>
                        </div>
                        <h1 class="gradient-color2">
                               <i class="fa fa-search"></i>
                        </h1>
                   </div>
               </div>
           </a>
       </div>
</div>
          <br>	
<h1>Active transactions</h1>
<a href="{{url('library/addticket')}}" type="button" class="primary-btn fix-gr-bg" data-toggle="tooltip">
                                            <span class="ti-check"></span>
                                            Add
</a>
<div class="col-lg-12">

                        <table id="table_id" class="display school-table library-table" cellspacing="0" width="100%">

                            <thead>
                                @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message-success-delete') }}
                                        </div>
                                        @elseif(session()->has('message-danger-delete'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('message-danger-delete') }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                	<th>id</th>
                                    <th>Patron name</th>
                                    <th>start date</th>
                                    <th>due date</th>
                                    
                                    <th>created by</th>
                                    <th>status</th>
                                    <th>@lang('lang.action')</th>
                                </tr>
                            </thead>

                            <tbody>
                            	@php $stt=1; @endphp
                               @foreach($notices as $notice)
                                <tr>
                                	<td>{{$stt++}}</td>
                                    <td>{{$notice->User->full_name}}</td>
                                    <td>{{$notice->start_day}}</td>
                                    <td>{{isset($notice->exp_day) ? $notice->exp_day : 'no due'}}</td>
                                    
                                    <td>{{$notice->created_by}}</td>
                                    <td>
                                    	@if(isset($notice->exp_day))
                                    		@if(Carbon\Carbon::today() < $notice->exp_day )
                                    		<b style="color: #34eb49">active</b>
                                    		@else
                                    		<b style="color: red">expired</b>
                                    		@endif
                                    	@else
                                    		<b style="color: #34eb49">active</b>
                                    	@endif
                                    </td>
                                    <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            @lang('lang.select')
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        	@if(in_array(302, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            	<a class="dropdown-item" href="{{url('library/ticket-detail')}}/{{$notice->id}}">@lang('return book')</a>
                                        	@endif
                                            @if(in_array(302, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            	<a class="dropdown-item" data-toggle="modal" data-target="#extendCard{{$notice->id}}"  href="#">@lang('change due date')</a>
                                        	@endif
	                                        @if(in_array(303, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
	                                            <a class="dropdown-item" data-toggle="modal" data-target="#deleteCard{{$notice->id}}"  href="#">@lang('lang.delete')</a>
	                                        @endif
                                       </div>
                                   </div>

                                   <div class="modal fade admin-query" id="deleteCard{{$notice->id}}" >
			                            <div class="modal-dialog modal-dialog-centered">
			                                <div class="modal-content">
			                                    <div class="modal-header">
			                                        <h4 class="modal-title">@lang('lang.delete')</h4>
			                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
			                                    </div>

			                                    <div class="modal-body">
			                                        <div class="text-center">
			                                            <h4>@lang('lang.are_you_sure_to_delete')</h4>
			                                        </div>

			                                        <div class="mt-40 d-flex justify-content-between">
			                                            <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
			                                            
			                                            <a href="{{url('library/deleteCard/'.$notice->id)}}" class="primary-btn fix-gr-bg" type="submit">@lang('lang.delete')</a>
			                                             
			                                        </div>
			                                    </div>

			                                </div>
			                            </div>
			                        </div>
			                        <div class="modal fade admin-query" id="extendCard{{$notice->id}}" >
			                            <div class="modal-dialog modal-dialog-centered">
			                                <div class="modal-content">
			                                    <div class="modal-header">
			                                        <h4 class="modal-title">@lang('change due date')</h4>
			                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
			                                    </div>

			                                    <div class="modal-body">
			                                    	<form action="{{url('library/extend-due-date')}}" method="POST">
			                                    		@csrf
			                                    	<input type="hidden" name="id" value="{{$notice->id}}">
			                                    	    <select class="extend" id="{{$notice->id}}">
			                                        		   <option value="0" selected>Select period</option>
											                   <option value="7d">7 days</option>
											                   <option value="14d">14 days</option>
											                   <option value="21d">21 days</option>
											                   <option value="1m">1 month</option>
											                   <option value="nodue">no due date</option>
											                
			                                        	</select><br><br>
			                                        <div class="input-effect">
										                 <input name="new_due_date" class="primary-input date read-only-input has-content" type="text" id="duedate{{$notice->id}}" value="{{isset($notice->exp_day) ? date('m/d/Y',strtotime($notice->exp_day)) : date('m/d/Y')}}">
										                 <input class="primary-input date read-only-input has-content" type="hidden" id="duedatehidden{{$notice->id}}" value="{{isset($notice->exp_day) ? date('m/d/Y',strtotime($notice->exp_day)) : date('m/d/Y')}}">
										                 <label>Due date</label>
										                 <span class="focus-border"></span> 
									     			</div>
									     			<br>

			                                        <div class="mt-40 d-flex justify-content-between">
			                                            <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
			                                            	<button class="primary-btn fix-gr-bg" type="submit">@lang('OK')</button>
			                                            </form>
			                                        </div>
			                                    </div>

			                                </div>
			                            </div>
			                        </div>
                               </td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
@endsection


@section('script')
<script type="text/javascript">
	
    $( document ).ready(function() {   
		$('.extend').on('change', function(){
		  var id = $(this).attr('id');
		  var date = $('#duedatehidden'+id+'').val();
		  	
		  	if($(this).val()=='7d')
            {
               var newdate = moment(date).add(7,'d').toDate();
            }
            if($(this).val()=='14d')
            {
               var newdate = moment(date).add(14,'d').toDate();
            }
            if($(this).val()=='21d')
            {
               var newdate = moment(date).add(21,'d').toDate();
            }
            if($(this).val()=='1m')
            {
               var newdate = moment(date).add(1,'M').toDate();
            }
            if($(this).val()=='nodue')
            {
               $('#duedate'+id+'').val("");
            }
            if($(this).val()=='0')
            {
               $('#duedate'+id+'').val(date);
            }
           
            day = '';
            month='';
            if(newdate.getDate() < 10)
            {
                day='0'+newdate.getDate();
            }
            else
            {
              day = newdate.getDate();
            }
            if(newdate.getMonth()+1 < 10)
            {
                month = '0'+newdate.getMonth()+1;
            }
            else
            {
                month = newdate.getMonth()+1;
            }
           
            year = newdate.getFullYear();
            $('#duedate'+id+'').val(month+'/'+day+'/'+year);
            
		});
    });
</script>
@endsection