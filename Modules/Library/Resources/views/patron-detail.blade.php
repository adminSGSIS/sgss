@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('Patron page')</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="{{url('library')}}">@lang('lang.library')</a>
                <a href="#">@lang('Patron Details')</a>
            </div>
        </div>
    </div>
</section>
<h2>Patron information</h2>
<div class="row white-box ">

	<div class="col-lg-3">
        <div class="input-effect sm2_mb_20 md_mb_20">
            <input
                class="primary-input form-control"
                type="text" name="author_name" autocomplete="off"
                value="{{$patron->full_name}}" readonly>
            <label>@lang('lang.name')</label>
            <span class="focus-border"></span>
            
        </div>
    </div>
    <div class="col-lg-3">
        <div class="input-effect sm2_mb_20 md_mb_20">
            <input
                class="primary-input form-control"
                type="text" name="author_name" autocomplete="off"
                value="{{$patron->roles->name}}" readonly>
            <label>@lang('lang.role')</label>
            <span class="focus-border"></span>
            
        </div>
    </div>
    <div class="col-lg-3">
        <div class="input-effect sm2_mb_20 md_mb_20">
            <input
                class="primary-input form-control"
                type="text" name="author_name" autocomplete="off"
                value="{{isset($patron->staff->departments->name)? $patron->staff->departments->name : 'Student'}}" readonly>
            <label>@lang('lang.department')</label>
            <span class="focus-border"></span>
            
        </div>
    </div>
    <div class="col-lg-3">
        <div class="input-effect sm2_mb_20 md_mb_20">
            <input
                class="primary-input form-control"
                type="text" name="author_name" autocomplete="off"
                value="{{$patron->email}}" readonly>
            <label>@lang('lang.email')</label>
            <span class="focus-border"></span>
            
        </div>
    </div>

    <div class="col-lg-3">
    	<br>
        <div class="input-effect sm2_mb_20 md_mb_20">
            <input
                class="primary-input form-control"
                type="text" name="author_name" autocomplete="off"
                value="{{isset($patron->staff->mobile)? $patron->staff->mobile : $patron->student->mobile}}" readonly>
            <label>@lang('lang.phone')</label>
            <span class="focus-border"></span>
            
        </div>
    </div>
</div>
<br><br>
<div class="col-lg-12">
    <table id="table_id" class="table">
    	<thead>
    		<tr>
	        	<th width="5%">id</th>
	            <th width="20%">Patron name</th>
	            <th>start date</th>
	            <th>due date</th>
	            <th>return date</th>
	            <th>created by</th>
	            <th>status</th>
	            <th>@lang('lang.action')</th>
	        </tr>
    	</thead>
    	
    		<tbody>
               @php $i=1; @endphp
               @foreach($patron->libraryCards as $card)
                <tr>
                	<td>{{$i++}}</td>
                    <td>{{$card->User->full_name}}</td>
                    <td>{{date('m/d/Y', strtotime($card->start_day))}}</td>
                    <td>{{date('m/d/Y', strtotime($card->exp_day))}}</td>
                    <td>{{$card->return_day!=""? date('m/d/Y', strtotime($card->return_day)) : ""}}</td>
                    <td>{{$card->created_by}}</td>
                    <td>{{$card->status}}</td>
                    <td>
                    <div class="dropdown">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            @lang('lang.select')
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                        	@if(in_array(302, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                            	<a class="dropdown-item" href="{{url('library/ticket-detail')}}/{{$card->id}}">@lang('Return')</a>
                        	@endif
                            
                            @if(in_array(303, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                <a class="dropdown-item" data-toggle="modal" data-target="#deleteCard{{$card->id}}"  href="#">@lang('lang.delete')</a>
                            @endif
                       </div>
                   </div>
                   <div class="modal fade admin-query" id="deleteCard{{$card->id}}" >
			                            <div class="modal-dialog modal-dialog-centered">
			                                <div class="modal-content">
			                                    <div class="modal-header">
			                                        <h4 class="modal-title">@lang('lang.delete')</h4>
			                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
			                                    </div>

			                                    <div class="modal-body">
			                                        <div class="text-center">
			                                            <h4>@lang('lang.are_you_sure_to_delete')</h4>
			                                        </div>

			                                        <div class="mt-40 d-flex justify-content-between">
			                                            <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
			                                            
			                                            <a href="{{url('library/deleteCard/'.$card->id)}}" class="primary-btn fix-gr-bg" type="submit">@lang('lang.delete')</a>
			                                             
			                                        </div>
			                                    </div>

			                                </div>
			                            </div>
			                        </div>
               </td>
                </tr>
               @endforeach
            </tbody>
    </table>
</div>
@endsection