@extends('backEnd.master')
@section('mainContent')
<h2>Patron list</h2>
	<div class="col-lg-12">
                    <table id="table_id" class="table">
        
		<thead>
			<tr>
				<th>Name</th>
				<th>Role</th>
				<th>Current checked out</th>
				<th>Current over due</th>
				<th>Total transactions</th>
				<th>Option</th>
			</tr>
		</thead>
		<tbody>
			@foreach($patrons as $patron)
			<tr>
				<td>{{$patron->full_name}}</td>
				<td>{{$patron->roles->name}}</td>
				<td>{{$patron->LibraryCards->where('status','active')->count()}}</td>
				<td>{{$patron->LibraryCards->where('exp_date','<',Carbon\Carbon::today())->count()}}</td>
				<td>{{$patron->LibraryCards->count()}}</td>
				<td>
					<div class="dropdown">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            @lang('lang.select')
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                           @if(in_array(302, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                            <a class="dropdown-item" href="{{url('library/patron/'.$patron->id)}}">@lang('Details')</a>
                        	@endif
                       
                       </div>
                   </div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection