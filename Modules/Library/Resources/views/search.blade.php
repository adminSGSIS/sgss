@extends('backEnd.master')
@section('mainContent')

<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <h2>Enter Search Values</h2>
    <div class="row white-box">
      <div class="col-lg-2">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <input class="primary-input form-control" type="text" name="title" autocomplete="off" value="" id="title">
              <label>Book Title </label>
              <span class="focus-border"></span>
                                                  </div>
      </div>
      <div class="col-lg-2">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <select id="rack" class="niceSelect w-100 bb form-control" name="rack">
                  <option>Rack number</option>
                  <option value="">All</option>
                  @foreach($shelves as $shelf)
                      
                  <option value="{{$shelf->title}}">{{$shelf->title}}</option>
                     
                  @endforeach
              </select>
              <span class="focus-border"></span>
                                                  </div>
      </div>
       <div class="col-lg-2">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <input class="primary-input form-control" type="text" name="publisher" autocomplete="off" value="" id="publisher">
              <label>Publisher <span>*</span> </label>
              <span class="focus-border"></span>
                                                  </div>
      </div>
      <div class="col-lg-2">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <input class="primary-input form-control" type="text" name="author" autocomplete="off" value="" id="author">
              <label>Author <span>*</span> </label>
              <span class="focus-border"></span>
                                                  </div>
      </div>
      <div class="col-lg-2">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <select id="cate" class="niceSelect w-100 bb form-control" name="cate">
                  <option>Category</option>
                  <option value="">All</option>
                  @foreach($cates as $cate)
                      
                  <option value="{{@$cate->category_name}}">{{@$cate->category_name}}</option>
                     
                  @endforeach
              </select>
              
              <span class="focus-border"></span>
                                                  </div>
      </div>
      
     
      
      <div class="col-lg-2">
          <div class="input-effect sm2_mb_20 md_mb_20">
              <select id="available" class="niceSelect w-100 bb form-control" name="available">
                  <option>Avaibalbe</option>
                  <option value="">All</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
              </select>
              
              <span class="focus-border"></span>
                                                  </div>
      </div>
    </div>
    <br>
    <table id="table1">
      <thead>
          <tr>
            <th width="20%">book name</th>
              <th>rack number</th>
              <th>Publisher</th>
              <th>Author</th>
              <th>Category</th>
              <th>Status</th>
              <th>Available</th>
          </tr>
      </thead>

      <tbody>
        @foreach($books as $book)
        <tr>
          <td>{{$book->Book->book_title}}</td>
          
          <td>{{$book->rack}}</td>
          <td>{{$book->Book->publisher_name}}</td>
          <td>{{$book->Book->author_name}}</td>
          <td>{{$book->Book->bookCategory->category_name}}</td>
          <td>{{$book->status}}</td>
          <td>
              @if($book->status != 'lost' && $book->borrowed ==0)
                  <i style="color: #34eb4f;" class="fa fa-check-circle fa-2x">
                    <span style="display: none;">yes</span>
                  </i>
              @else
                  <i style="color: red;" class="fa fa-exclamation-triangle fa-2x" aria-hidden="true">
                    <span style="display: none;">no</span>
                  </i>
              @endif
          </td>
        </tr>
          @endforeach
      </tbody>

  </table>
  </div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    
  </div>
  
</div>
@endsection 

@section('script')
<script type="text/javascript">
  var table = $('#table1').DataTable({
    responsive: true
  });
  
  $('.dataTables_filter').remove();

  $('#title').on( 'keyup', function () {
    table
        .columns( 0 )
        .search( this.value )
        .draw();
    } );
  $('#cate').on( 'change', function () {
    table
        .columns( 4 )
        .search( this.value )
        .draw();
    } );
  $('#rack').on( 'change', function () {
    table
        .columns( 1 )
        .search( this.value )
        .draw();
    } );
  $('#publisher').on( 'keyup', function () {
    table
        .columns( 2 )
        .search( this.value )
        .draw();
    } );
  $('#author').on( 'keyup', function () {
    table
        .columns( 3 )
        .search( this.value )
        .draw();
    } );
  $('#available').on( 'change', function () {
    table
        .columns( 6 )
        .search( this.value )
        .draw();
    } );
</script>
@endsection