<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('library')->group(function() {
    Route::get('/', 'LibraryController@index');
    Route::get('addticket','LibraryController@getaddticket');
    Route::post('add-ticket', 'LibraryController@addTicket');
    Route::get('ticket-detail/{id}', 'LibraryController@detail');
    Route::post('createTicket','LibraryController@createTicket');
    Route::post('returnTicket','LibraryController@returnTicket');
    Route::post('add-book','LibraryController@addBook');
    Route::get('borrow-cards','LibraryController@borrowCards');
    Route::get('search','LibraryController@search');
    Route::get('shelves','LibraryController@shelves');
    Route::post('add-shelf','LibraryController@addShelf');
    Route::get('edit-shelf/{id}','LibraryController@editShelf');
    Route::put('update-shelf/{id}','LibraryController@updateShelf');
    Route::get('delete-shelf/{id}','LibraryController@deleteShelf');
    Route::get('delete-book/{id}','LibraryController@deleteBook');
    Route::get('patrons','LibraryController@patrons');
    Route::get('patron/{id}','LibraryController@patron');
    Route::post('extend-due-date','LibraryController@extend');
    Route::get('deleteCard/{id}','LibraryController@deleteCard');
});
