<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table='guest';
    protected $fillable = [];
}
