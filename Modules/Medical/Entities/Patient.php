<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'patient';
    protected $fillable = [
        "patient_name",
        "date",
        "case",
        "temperaturte",
        "respiratory",
        "symtome_type",
        "symtome_describe",
        "old_patient",
        "referencess",
        "consultant"
    ];

    public function symtomeRelation(){
        return $this->hasOne('Modules\Medical\Entities\item_evaluation','id','symtome_type');
    }
    public function prescriptionsRelation(){
        return $this->hasOne('Modules\Medical\Entities\prescription','patient_id','id');
        /* return $this->hasMany('Modules\Medical\Entities\prescription','patient_id','id'); */
    }
    
}
