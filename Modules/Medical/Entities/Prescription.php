<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    //
    public function PatientRelation()
    {
        return $this->hasOne('Modules\Medical\Entities\Patient','id','patient_id');
    }
}
