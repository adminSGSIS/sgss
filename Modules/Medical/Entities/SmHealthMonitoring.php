<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class SmHealthMonitoring extends Model
{
    protected $table = 'health_monitoring';
    protected $fillable = [
        'id_student',
        'student_name',
        'class',
        'blood_pressure',
        'heart_beat',
        'myopic',
        'right_eye',
        'left_eye'
    ];
    public function classRelation()
    {
        return $this->belongsTo('App\SmClass', 'trial_class_id');
    }

    public function studentRelation()
    {
        return $this->belongsTo('App\SmStudent');
    }
    public function className()
    {
        return $this->belongsTo('App\SmClass', 'class', 'id');
    }
}
