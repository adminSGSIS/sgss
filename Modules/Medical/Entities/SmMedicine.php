<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;

class SmMedicine extends Model
{
    protected $table = 'medicine';
    protected $fillable = [];

    public function medicineTypeRelation(){
        return $this->belongsTo('Modules\Medical\Entities\SmMedicineType','medicine_type','id');
    }
}
