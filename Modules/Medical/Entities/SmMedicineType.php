<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;

class SmMedicineType extends Model
{
    protected $table = 'medicine_type';
    protected $fillable = [];
}
