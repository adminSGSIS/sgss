<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Medical\Entities\SmMedicineType;
use Modules\Medical\Entities\SmMedicine;
class SmSupplyMedicine extends Model
{
    protected $table='sm_supply_medicines';
    protected $fillable = [];
    public function medicineTypeRelation(){
        return $this->belongsTo('Modules\Medical\Entities\SmMedicineType','medicine_type','id');
    }

    public function medicineNameRelation(){
        return $this->belongsTo('Modules\Medical\Entities\SmMedicine','medicine_name','id');
    }
}
