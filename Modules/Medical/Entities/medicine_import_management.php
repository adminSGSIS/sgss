<?php

namespace Modules\Medical\Entities;

use App\SmHumanDepartment;
use Illuminate\Database\Eloquent\Model;
use App\SmStaff;
use App\SmSupplier;
use Modules\Medical\Entities\SmMedicineType;
class medicine_import_management extends Model
{
    protected $table ='medicine_import_manaher';
    protected $fillable = [];

    public function staffRelation(){
        return $this->belongsTo('App\SmStaff','staff','id');
    }
    public function departmentRelation(){
        return $this->belongsTo('App\SmHumanDepartment','deparment','id');
    }
    public function supplierRelation(){
        return $this->belongsTo('App\SmSupplier','supplier','id');
    }
    public function medicineRelation(){
        return $this->belongsTo('Modules\Medical\Entities\SmMedicine','medicine','id');
    }
    public function medicineTypeRelation(){
        return $this->belongsTo('Modules\Medical\Entities\SmMedicineType','medicine_type','id');
    }
}
