<?php

namespace Modules\Medical\Http\Controllers;

use App\SmParent;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Medical\Entities\Patient;
use App\SmStudent;
use Brian2694\Toastr\Facades\Toastr;
use App\Smstaff;
use Modules\Medical\Entities\item_evaluation;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Null_;
use Carbon\Carbon;
use App\SmBaseSetup;
use Modules\Medical\Entities\Guest;
use Modules\Medical\Entities\Prescription;
use PayPal\Api\Presentation;
use Modules\Medical\Entities\SmHealthMonitoring;
use Barryvdh\DomPDF\Facade as PDF;
use App\Exports\patientExportExcel;
use Maatwebsite\Excel\Facades\Excel;

class MedicalController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $patients=Patient::all();
        $blood_groups = SmBaseSetup::where('base_group_id',3)->get();
        $gender= SmBaseSetup::where('base_group_id',1)->get();
        
        $teachers   = Smstaff::join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id','=','9')
                                        ->orwhere('sm_human_departments.id','=','2')
                                        ->orwhere('sm_human_departments.id','=','10')
                                        ->orwhere('sm_human_departments.id','=','12')
                                        ->orwhere('sm_human_departments.id','=','13')
                                        ->get(['sm_staffs.*']);
        $staffs     = DB::table('sm_staffs')->join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id','=','1')
                                        ->orwhere('sm_human_departments.id','=','3')
                                        ->orwhere('sm_human_departments.id','=','4')
                                        ->orwhere('sm_human_departments.id','=','5')
                                        ->orwhere('sm_human_departments.id','=','6')  
                                        ->orwhere('sm_human_departments.id','=','7')
                                        ->orwhere('sm_human_departments.id','=','8')
                                        ->orwhere('sm_human_departments.id','=','14')
                                        ->orwhere('sm_human_departments.id','=','15')
                                        ->orwhere('sm_human_departments.id','=','16')
                                        ->orwhere('sm_human_departments.id','=','17')
                                        ->orwhere('sm_human_departments.id','=','18')
                                        ->orwhere('sm_human_departments.id','=','19')
                                        ->orwhere('sm_human_departments.id','=','20')
                                        ->orwhere('sm_human_departments.id','=','21')
                                        ->orwhere('sm_human_departments.id','=','22')
                                        ->orwhere('sm_human_departments.id','=','23')
                                        ->orwhere('sm_human_departments.id','=','24')
                                        ->orwhere('sm_human_departments.id','=','25')
                                        ->orwhere('sm_human_departments.id','=','26')
                                        ->get(['sm_staffs.*']);
                                        
        $students  = SmStudent::all();
        
        return view('medical::medicalSearch',compact('students','teachers'),compact('staffs','blood_groups','gender','patients'));
    }
    public function ShowMedical(Request $request)
    {
        if ($request->teacher_id!=null) {
            return $this->CreateStaffMedical($request->teacher_id, $request->patientType);
        }
        else if($request->student_id!=null)
        {
            return $this->CreateStudentMedical($request->student_id,$request->patientType);
        }
            else if($request->staff_id!=null)
            {
                return $this->CreateStaffMedical($request->staff_id,$request->patientType);
            }
            Toastr::error('Error', 'Please choose Patient');
            return back();    
    }
    public  function CreateStudentMedical($id,$type)
    {        
        $patients=Patient::all();
        $items = item_evaluation::all();
        $student=SmStudent::findOrFail($id);
        $parent=SmParent::where('id',$student->parent_id)->first();
        return view('medical::index',compact('student','parent'),compact('type','items','patients','id'));
    }
    public  function CreateStaffMedical($id,$type)
    {   
        $patients=Patient::all();
        $items = item_evaluation::all();
        $staffs=Smstaff::findOrFail($id);
        return view('medical::index',compact('staffs','type'),compact('items','patients','id'));
    }
    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('medical::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function StoreMedical(Request $request)
    {
        //     
        $patient= new Patient();
        $request->validate([
            'caseInput'=>'required',
            'temperature'=>'required',
            'respiratory'=>'required',        
        ]);
        if($request->student_name!=null)
        {
            $patient->patient_name=$request->student_name;
            $patient->gender=$request->student_gender;
            $patient->date_of_birth=$request->student_birth;
            $patient->address=$request->student_address;
        }
        elseif($request->teacher_name!=null)
        {
            $patient->patient_name=$request->teacher_name;
            $patient->gender=$request->teacher_gender;
            $patient->date_of_birth=$request->teacher_birth;
            $patient->address=$request->teacher_address;
        }
        else{
            $patient->patient_name=$request->staff_name;
            $patient->gender=$request->staff_gender;
            $patient->date_of_birth=$request->staff_birth;
            $patient->address=$request->staff_address;
        }
        $patient->patient_id=$request->patient_id;
        $patient->date=$request->date;
        $patient->case=$request->caseInput;
        $patient->temperaturte=$request->temperature;
        $patient->respiratory=$request->respiratory;
        $patient->hospital=$request->hospital;
        $patient->patient_type=$request->patient_type;
        $patient->symtome_type=$request->symtomstype;
        $patient->symtome_describe=$request->symtondes;
        if($request->oldpatient!=0)
        $patient->old_patient=$request->oldpatient;
        else
        $patient->old_patient=0;
        $patient->referencess=$this->ImgUpload($request);
        $patient->consultant=$request->consultant;
        /* dd($patient); */
        $patient->save();

        $prescription = new Prescription;           
        $prescription->patient_id = $patient->id;
        $prescription->id_medicine = $request->medicine_id;
        $prescription->diagnostic = $request->diagnostic;
        $prescription->medicine_name = $request ->medicine_name;
            /* if ($request->file('image') != "") {
                $file = $request->file('image');
                $image = 'stu-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/prescriptions/', $image);
            }
            $prescription->image = $image; */
        $prescription->using = $request->using;
        $prescription->amount = $request->amount;
        $prescription->origin = $request->origin;
        $prescription->save();
        Toastr::success('Saved', 'Medical had been created');
        return redirect()->route('MedicalSearch');
        /* dd($prescription); */
       /*  $patient->save();
         */
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('medical::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id,$type,$patient_id)
    {
                
        $items = item_evaluation::all();
        $parent=null;
        $student=null;
        $guest=null;
        $staffs=null;
        if($type==1)
        {
            $student=SmStudent::findOrFail($patient_id);
            $parent=SmParent::where('id',$student->parent_id)->first();
        }        
        elseif($type==2||$type==3)
        $staffs=Smstaff::findOrFail($patient_id);
        elseif($type==4)
        $guest=Guest::findOrFail($patient_id);
        $patientData=Patient::find($id);
        $editData = Prescription::where('patient_id',$id)->first();
        /* dd($editData,$staffs,$student,$parent); */
        return view('medical::index',compact('student','parent'),compact('type','items','patientData','guest','editData','staffs','id'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
        $patient=Patient::find($id);
        $request->validate([
            'caseInput'=>'required',
            'temperature'=>'required',
            'respiratory'=>'required',        
        ]);
        if($request->student_name!=null)
        {
            $patient->patient_name=$request->student_name;
            $patient->gender=$request->student_gender;
            $patient->date_of_birth=$request->student_birth;
            $patient->address=$request->student_address;
        }
        elseif($request->teacher_name!=null)
        {
            $patient->patient_name=$request->teacher_name;
            $patient->gender=$request->teacher_gender;
            $patient->date_of_birth=$request->teacher_birth;
            $patient->address=$request->teacher_address;
        }
        else{
            $patient->patient_name=$request->staff_name;
            $patient->gender=$request->staff_gender;
            $patient->date_of_birth=$request->staff_birth;
            $patient->address=$request->staff_address;
        }
        $patient->date=$request->date;
        $patient->case=$request->caseInput;
        $patient->temperaturte=$request->temperature;
        $patient->respiratory=$request->respiratory;
        $patient->patient_type=$request->patient_type;
        $patient->symtome_type=$request->symtomstype;
        $patient->symtome_describe=$request->symtondes;
        $patient->hospital=$request->hospital;
        if($request->oldpatient!=0)
        $patient->old_patient=$request->oldpatient;
        else
        $patient->old_patient=0;
        $patient->consultant=$request->consultant;
        /* dd($patient); */
        $patient->save();
        $prescriptionId =Prescription::where('patient_id',$id)->first();
        $prescription= Prescription::find($prescriptionId->id);
        $prescription->id_medicine = $request->medicine_id;
        $prescription->diagnostic = $request->diagnostic;
        $prescription->medicine_name = $request ->medicine_name;
        $prescription->using = $request->using;
        $prescription->amount = $request->amount;
        $prescription->origin = $request->origin;
        
        if($prescription->save()){
            Toastr::success('Saved', 'Successful Update');
            return redirect()->back()->with('message-success', 'Successful Update');
        }else{
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        $patient=Patient::findOrFail($id);
        $prescription = Prescription::where('patient_id',$id)->first();
        /* dd($prescription); */
        if($prescription!=null)
        $prescription->delete();

        $patient->delete();
        Toastr::success('Deleted', 'Medical had been deleted');
        return redirect()->route('MedicalSearch');
    }
    public function patientExcel(){
        return Excel::download(new patientExportExcel, 'patient.xlsx');
    }
    public function patientPDF($id){   
        $patient = Patient::findOrFail($id);
        $prescription=Prescription::where('patient_id',$id)->first();
        $title = "Patient SGSI";
        $pdf = PDF::loadView('medical::patient-PDF',compact('patient','title','prescription'));
        return $pdf->stream('patientPDF.pdf');
    }
    public function ImgUpload(Request $request)
    {
        if($request->hasFile('image'))
        {
            if($request->file('image')->isValid())
            {
                $request->validate(
                [
                    'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                $imageName=time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads/medicals'),$imageName);
                return $imageName;
            }
            return '';
        }

    }
    public function newGuest(Request $request)
    {
        $guest=new Guest();
        $guest->name=$request->name;
        $guest->email=$request->email;
        $guest->phone=$request->phone;
        $guest->address=$request->address;
        $guest->gender=$request->gender;
        $guest->day_of_birth=Carbon::parse($request->day_of_birth)->format('Y-m-d');
        $guest->height=$request->height;
        $guest->weight=$request->weight;
        $guest->blood_group=$request->bloodgroup;
        $guest->save();  
        return $this->newGuestMedical($guest->id,$request->patientType);
    }

    public function newGuestMedical($id,$patientType)
    {
        return $this->CreateGuestMedical($patientType,$id);
    }
    public  function CreateGuestMedical($type,$id)
    {   $items = item_evaluation::all();
        $guest=Guest::findOrFail($id);
        return view('medical::index',compact('guest','type','items','id'));
    }

    public function healthMonitoring(){
        $patients=Patient::all()->where('patient_type',1);
        $students=SmStudent::all();
        $healthMonitorings=SmHealthMonitoring::all();
        return view('medical::health-monitoring',compact('students','patients','healthMonitorings'));
    }

    public function healthMonitorCreate(Request $request){
        $patients=Patient::all()->where('patient_id',$request->student_id);
        $student_health=SmHealthMonitoring::where('student_id',$request->student_id)->first();
        $student=Smstudent::find($request->student_id);
        /* dd($student_health); */
        return view('medical::healthMonitoringCreate',compact('student','patients','student_health'));
    }

    public function healthMonitorEdit($id){
        $patients=Patient::all()->where('patient_id',$id);
        $student_health=SmHealthMonitoring::where('student_id',$id)->first();
        $student=Smstudent::find($id);
        /* dd($student_health); */
        return view('medical::healthMonitoringCreate',compact('student','patients','student_health'));
    }

    public function healthMonitorStore(Request $request){
        $healthMonitoring=new SmHealthMonitoring();
        /* $healthMonitoring=$request->all(); */
        $healthMonitoring->student_id=$request->id_student;
        $healthMonitoring->student_name=$request->student_name;
        $healthMonitoring->class=$request->class;
        $healthMonitoring->blood_pressure=$request->blood_pressure;
        $healthMonitoring->heart_beat=$request->heart_beat;
        $healthMonitoring->myopic=$request->myopic;
        $healthMonitoring->right_eye=$request->right_eye;
        $healthMonitoring->left_eye=$request->left_eye;
        if($healthMonitoring->save()){
            Toastr::success('Operation successful', 'Success');
            return redirect()->back()->with('message-success', 'Prescription Save');
        }else{
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
       /* dd($healthMonitoring); */
    }

    public function Summary($id,$patient_id){
        $student=Smstudent::find($patient_id);
        $patientData=Patient::find($id);        
        return view('medical::summary-health',compact('student','patientData'));
    }

    public function healthMonitorUpdate(Request $request,$id){
        $healthMonitoring=SmHealthMonitoring::where('student_id',$id)->first();        
        $healthMonitoring->student_id=$request->id_student;
        $healthMonitoring->student_name=$request->student_name;
        $healthMonitoring->class=$request->class;
        $healthMonitoring->blood_pressure=$request->blood_pressure;
        $healthMonitoring->heart_beat=$request->heart_beat;
        $healthMonitoring->myopic=$request->myopic;
        $healthMonitoring->right_eye=$request->right_eye;
        $healthMonitoring->left_eye=$request->left_eye;
        /* return redirect()->url('medical/healthMonitoring-create',$id); */
        if($healthMonitoring->save()){
            Toastr::success('Operation successful', 'Success');
            return redirect()->back()->with('message-success', 'Prescription Save');
        }else{
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }

    public function healthMonitorDelete($id){
        $student_health=SmHealthMonitoring::where('student_id',$id)->first();
        /* dd($student_health); */
        $student_health->delete();
        Toastr::success('Deleted', 'Health Monitoring had been deleted');
        return redirect()->route('healthMonitoring');
    }

    public function healthMonitorPDF($id,$student_id)
    {
        $student=SmStudent::where('id',$student_id)->first();        
        $healthMonitoring = SmHealthMonitoring::findOrFail($id);
        $title = "Health Monitoring";
        $pdf = PDF::loadView('medical::healthMonitoringPDF',compact('healthMonitoring','title','student'));
        return $pdf->stream('healthMonitoringPDF.pdf');
    }

    
}