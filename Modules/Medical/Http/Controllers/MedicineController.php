<?php

namespace Modules\Medical\Http\Controllers;

use App\SmStaff;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Medical\Entities\SmMedicineType;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Modules\Medical\Entities\SmMedicine;
use DateTime;
use Illuminate\Support\Facades\DB;
use App\SmSupplier;
use App\SmHumanDepartment;
use Modules\Medical\Entities\medicine_import_management;
use Barryvdh\DomPDF\Facade as PDF;
use Modules\Medical\Entities\Patient;
use Modules\Medical\Entities\Prescription;
use Modules\Medical\Entities\SmSupplyMedicine;
use Illuminate\Support\Facades\Mail;


class MedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $medicine_types=SmMedicineType::all();
        return view('medical::Medicine.medicine_type', compact('medicine_types'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('medical::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
        $medicine_type=new SmMedicineType();
        $medicine_type->name=$request->name;
        $medicine_type->save();
        Toastr::success('Medicine Type created successful', 'Saved');
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('medical::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $medicine_types=SmMedicineType::all();
        $editData=SmMedicineType::find($id);
        return view('medical::Medicine.medicine_type', compact('medicine_types', 'editData'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
        $medicine_type=SmMedicineType::find($id);
        $medicine_type->name=$request->name;
        $medicine_type->save();
        Toastr::success('Medicine type updated successful', 'Saved');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
        $medicine_type=SmMedicineType::find($id);
        $medicine_type->delete();
        Toastr::success('Medicine type deleted successful', 'Saved');
        return back();
    }

    public function medicine_index()
    {
        $medicines=SmMedicine::all();
        $medicine_types=SmMedicineType::all();
        return view('medical::Medicine.medicine', compact('medicines', 'medicine_types'));
    }
    public function medicine_store(Request $request)
    {
        $medicine=new SmMedicine();
        $medicine->medicine_type=$request->medicinetype;
        $medicine->medicine_code=$request->medicinecode;
        $medicine->medicine_name=$request->name;
        $medicine->unit=$request->unit;
        $medicine->price=$request->price;
        $medicine->manufacturing_date=date('Y-m-d', strtotime($request->manufacturingdate));
        $medicine->expiry_date=date('Y-m-d', strtotime($request->expirydate));
        $medicine->producer=$request->producer;
        $medicine->stock=$request->stock;
        $medicine->save();
        Toastr::success('Medicine created successful', 'Saved');
        return back();
    }
    public function medicine_edit($id)
    {
        $medicines=SmMedicine::all();
        $editData=SmMedicine::find($id);
        $medicine_types=SmMedicineType::all();
        return view('medical::Medicine.medicine', compact('medicines', 'editData', 'medicine_types'));
    }
    public function medicine_update(Request $request, $id)
    {
        /* $medicine=new SmMedicine(); */
        $medicine=SmMedicine::find($id);
        $medicine->medicine_type=$request->medicinetype;
        $medicine->medicine_code=$request->medicinecode;
        $medicine->medicine_name=$request->name;
        $medicine->unit=$request->unit;
        $medicine->price=$request->price;
        $medicine->manufacturing_date=date('Y-m-d', strtotime($request->manufacturingdate));
        $medicine->expiry_date=date('Y-m-d', strtotime($request->expirydate));
        $medicine->producer=$request->producer;
        $medicine->stock=$request->stock;
        Toastr::success('Medicine updated successful', 'Saved');
        $medicine->save();
        return redirect()->route('medicine-index');
    }
    public function medicine_delete($id)
    {
        $medicine=SmMedicine::find($id);
        $medicine->delete();
        Toastr::success('Medicine deleted successful', 'Saved');
        return back();
    }


    public function medicinetManagement_index()
    {
        $staffs     = DB::table('sm_staffs')->join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id', '=', '1')
                                        ->orwhere('sm_human_departments.id', '=', '3')
                                        ->orwhere('sm_human_departments.id', '=', '4')
                                        ->orwhere('sm_human_departments.id', '=', '5')
                                        ->orwhere('sm_human_departments.id', '=', '6')
                                        ->orwhere('sm_human_departments.id', '=', '7')
                                        ->orwhere('sm_human_departments.id', '=', '8')
                                        ->orwhere('sm_human_departments.id', '=', '14')
                                        ->orwhere('sm_human_departments.id', '=', '15')
                                        ->orwhere('sm_human_departments.id', '=', '16')
                                        ->orwhere('sm_human_departments.id', '=', '17')
                                        ->orwhere('sm_human_departments.id', '=', '18')
                                        ->orwhere('sm_human_departments.id', '=', '19')
                                        ->orwhere('sm_human_departments.id', '=', '20')
                                        ->orwhere('sm_human_departments.id', '=', '21')
                                        ->orwhere('sm_human_departments.id', '=', '22')
                                        ->orwhere('sm_human_departments.id', '=', '23')
                                        ->orwhere('sm_human_departments.id', '=', '24')
                                        ->orwhere('sm_human_departments.id', '=', '25')
                                        ->orwhere('sm_human_departments.id', '=', '26')
                                        ->get(['sm_staffs.*']);
        $suppliers=SmSupplier::all();
        $departments=SmHumanDepartment::all();
        $medicine_types=SmMedicineType::all();
        $medicines=SmMedicine::all();
        $medicineManagements=medicine_import_management::all();
        return view('medical::Medicine.medicine_management', compact('staffs', 'suppliers', 'departments', 'medicine_types', 'medicines', 'medicineManagements'));
    }
    public function medicinetManagement_store(Request $request)
    {
        $medicineManagement=new medicine_import_management();
        $medicineManagement->staff=$request->staff;
        $medicineManagement->deparment=$request->department;
        $medicineManagement->supplier=$request->supplier;
        $medicineManagement->medicine=$request->medicine;
        $medicineManagement->import_date=date('Y-m-d', strtotime($request->importdate));
        $medicineManagement->medicine_type=$request->medicine_type;
        $medicineManagement->note=$request->note;
        $medicineManagement->amount=$request->amount;
        $medicineManagement->save();
        Toastr::success('Medicine store successful', 'Saved');
        return back();
    }
    public function medicinetManagement_edit($id)
    {
        $staffs     = DB::table('sm_staffs')->join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id', '=', '1')
                                        ->orwhere('sm_human_departments.id', '=', '3')
                                        ->orwhere('sm_human_departments.id', '=', '4')
                                        ->orwhere('sm_human_departments.id', '=', '5')
                                        ->orwhere('sm_human_departments.id', '=', '6')
                                        ->orwhere('sm_human_departments.id', '=', '7')
                                        ->orwhere('sm_human_departments.id', '=', '8')
                                        ->orwhere('sm_human_departments.id', '=', '14')
                                        ->orwhere('sm_human_departments.id', '=', '15')
                                        ->orwhere('sm_human_departments.id', '=', '16')
                                        ->orwhere('sm_human_departments.id', '=', '17')
                                        ->orwhere('sm_human_departments.id', '=', '18')
                                        ->orwhere('sm_human_departments.id', '=', '19')
                                        ->orwhere('sm_human_departments.id', '=', '20')
                                        ->orwhere('sm_human_departments.id', '=', '21')
                                        ->orwhere('sm_human_departments.id', '=', '22')
                                        ->orwhere('sm_human_departments.id', '=', '23')
                                        ->orwhere('sm_human_departments.id', '=', '24')
                                        ->orwhere('sm_human_departments.id', '=', '25')
                                        ->orwhere('sm_human_departments.id', '=', '26')
                                        ->get(['sm_staffs.*']);
        $suppliers=SmSupplier::all();
        $departments=SmHumanDepartment::all();
        $medicine_types=SmMedicineType::all();
        $medicines=SmMedicine::all();
        $medicineManagements=medicine_import_management::all();
        $editData=medicine_import_management::find($id);
        return view('medical::Medicine.medicine_management', compact('staffs', 'suppliers', 'departments', 'medicine_types', 'medicines', 'medicineManagements', 'editData'));
    }

    public function medicinetManagement_update(Request $request, $id)
    {
        $medicineManagement=medicine_import_management::find($id);
        $medicineManagement->staff=$request->staff;
        $medicineManagement->deparment=$request->department;
        $medicineManagement->supplier=$request->supplier;
        $medicineManagement->medicine=$request->medicine;
        $medicineManagement->import_date=date('Y-m-d', strtotime($request->importdate));
        $medicineManagement->medicine_type=$request->medicine_type;
        $medicineManagement->note=$request->note;
        $medicineManagement->amount=$request->amount;
        $medicineManagement->save();
        return redirect()->route('medicine-management-index');
    }

    public function medicineTypeCheck()
    {
        $medicine_type = $_POST['id'];
        $data = SmMedicine::where('medicine_type', $medicine_type)->get();
        $name = '';
        foreach ($data  as $key => $obj) {
            $name .= '<option value="'.$obj->id.'">'.$obj->medicine_name.'</option>';
        }
        echo $name;
    }

    public function report()
    {
        return view('medical::Medicine.report-medicine');
    }
    public function reportPDF(Request $request)
    {
        try {
            if ($request->month==1||$request->month==2||$request->month==3||$request->month==4||$request->month==5||$request->month==6||$request->month==7||
            $request->month==8||$request->month==9||$request->month==10||$request->month==11||$request->month==12) {
                $title = "Clinic report SGSI";
                $now=Carbon::now();
                $patients=Patient::whereMonth('date', $request->month)->whereYear('date', $now->year)->get();
                $importMedicines=medicine_import_management::whereMonth('created_at', $request->month)->whereYear('created_at', $now->year)->get();
                $supplyMedicines =SmSupplyMedicine::whereMonth('created_at', $request->month)->whereYear('created_at', $now->year)->get();
                $medicines=SmMedicine::all();
                Toastr::success('Report Sended', 'Saved');
                $pdf = PDF::loadView('medical::report-PDF', compact('title', 'patients', 'importMedicines', 'supplyMedicines', 'medicines'));
                return $pdf->stream('patientPDF.pdf');
            }
            Toastr::error('Please select month', 'error');
            return back();
        } catch (\Throwable $th) {
            return back();
        }
    }


    public function reportMail(Request $request){
        try {if($request->month==1||$request->month==2||$request->month==3||$request->month==4||$request->month==5||$request->month==6||$request->month==7||
        $request->month==8||$request->month==9||$request->month==10||$request->month==11||$request->month==12)
        {
                $title = "Clinic report SGSI";        
                $now=Carbon::now();        
                $patients=Patient::whereMonth('date',$request->month)->whereYear('date',$now->year)->get();
                $importMedicines=medicine_import_management::whereMonth('created_at',$request->month)->whereYear('created_at',$now->year)->get();
                $supplyMedicines =SmSupplyMedicine::whereMonth('created_at',$request->month)->whereYear('created_at',$now->year)->get();        
                $medicines=SmMedicine::all();
                $data = [
                    'patients' => $patients,
                    'importMedicines' => $importMedicines,
                    'supplyMedicines' => $supplyMedicines,
                    'medicines'       => $medicines,
         ];
                            Mail::send('medical::reportMail', compact('data'), function ($message) {
                                /* $message->to('tvdkhoa1801@gmail.com')->subject('Report clinic'); */
                                $message->to('duonghuynh@sgstar.edu.vn')->subject('Report clinic');
                                
                                
                            });
                            Toastr::success('Report Sended', 'Saved');
                            return back();
        }
        Toastr::error('Please select month', 'error');
        return back();
    } catch (\Throwable $th) {
                return back();
            }
    }
}