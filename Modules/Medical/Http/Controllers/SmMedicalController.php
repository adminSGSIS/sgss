<?php

namespace Modules\Medical\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Medical\Entities\Prescription;
use Illuminate\Routing\Controller;
Use \Carbon\Carbon;
use Brian2694\Toastr\Facades\Toastr;
use Defuse\Crypto\File;
use Illuminate\Support\Str;
use Illuminate\Contracts\Support\Renderable;
use Image;
use Illuminate\Support\Facades\DB;
use Modules\Medical\Entities\Patient;
use Modules\Medical\Entities\item_evaluation;
use Barryvdh\DomPDF\Facade as PDF;
use App\SmStaff;
use Modules\Medical\Entities\SmMedicine;
use Modules\Medical\Entities\SmMedicineType;
use App\SmStudent;
use Modules\Medical\Entities\SmSupplyMedicine;
use App\smMedicineProposal;
use App\SmSupplier;
use Illuminate\Support\Facades\Mail;

class SmMedicalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function prescription (){
        $prescription = Prescription::all();
        $patients=Patient::all();
        
    	return view('medical::Mediance.prescription',compact('prescription','patients'));
    }

    public function prescriptionStore(Request $request){
        try {
            $prescription = new Prescription;
           
            $prescription->patient_id = $request->patient_id;
            $prescription->id_medicine = $request->medicine_id;
            $prescription->diagnostic = $request->diagnostic;
            $prescription->medicine_name = $request ->medicine_name;
            if ($request->file('image') != "") {
                $file = $request->file('image');
                $image = 'stu-' . md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/prescriptions/', $image);
            }
            $prescription->image = $image;
            $prescription->using = $request->using;
            $prescription->amount = $request->amount;
            $prescription->origin = $request->origin;

        
            if($prescription->save()){
                Toastr::success('Operation successful', 'Success');
                return redirect()->back()->with('message-success', 'Prescription Save');
            }else{
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }

    public function prescriptionEdit($id){
        try{
            $prescription = Prescription::all();
            $editData = Prescription::find($id);
            $patients=Patient::all();
            $medicine = Prescription::all();
            return view('medical::Mediance.prescription', compact('editData','medicine'),compact('patients','prescription'));
         }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        } 
    }


    public function prescriptionUpdate(Request $request){
        try{
            $update = Prescription::find($request->id);
            $image = "";
            $ok = false;
            if ($request->file('image') != "") {
               
                if ($update->image != "") {
                    $ok = true;
                }
    
                $file = $request->file('image');
                $image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/prescriptions/', $image);
            }
            $update->id_medicine = $request->medicine_id;
            $update->diagnostic = $request->diagnostic;
            $update->medicine_name = $request ->medicine_name;
            
            if ($ok) {
                $update->image = $image;
            }
            
            $update->using = $request->using;
            $update->amount = $request->amount;
            $update->origin = $request->origin;
            if ($update->save()) {
                Toastr::success('Operation successful', 'Success');
               return redirect()->back(); 
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function prescriptionDetails($id){  
        //try{
            $prescription = Prescription::find($id);
            $patient_infos=Patient::findOrFail($prescription->patient_id);
            return view('medical::Mediance.prescription_details', compact('prescription','patient_infos'));
        // }catch (\Exception $e) {
        //    Toastr::error('Operation Failed', 'Failed');
        //    return redirect()->back(); 
        // }
    }

    /*PDF*/
    public function prescriptionPDF($id){
        $prescription = Prescription::find($id);
        $patient_infos=Patient::findOrFail($prescription->patient_id);
        $pdf = PDF::loadView('medical::Mediance.prescription-pdf',compact('prescription','patient_infos'));
        return $pdf->stream('prescription-pdf.pdf');
    }
    /*PDF*/
    
    public function forDeletePrescription($id){
        try{
            $url ='/medical/delete-prescription/'.$id;
            return view('medical::Mediance.delete_modal', compact('url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function prescriptionDestroy($id){
        try{
            $prescription = Prescription::find($id);
            $image_path = base_path('public/uploads/prescriptions/'.$prescription->image);  //Đường dẫn lưu tạm 
            if(file_exists($image_path)) 
            { 
                unlink($image_path);   ///Xóa ảnh từ folder tạm sau khi dùng xong
            }
            $result = $prescription->delete();
            
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function addItems(){
        $items = item_evaluation::all();
        return view('medical::Mediance.add_items',compact('items'));
    }

    public function addItemStore(Request $request){
        try{
            $item = new item_evaluation;
            $item->name = $request->name; 
            if ($item->save()) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }

        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }

    public function itemEdit($id){
        try{

            $editData = item_evaluation::find($id); 
            $items = item_evaluation::all();
            return view('medical::Mediance.add_items', compact('editData','items'));

        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function itemUpdate(Request $request){
        try{
            $item_update = item_evaluation::find($request->id);
            $item_update->name = $request->name; 
            if ($item_update->save()) {
                Toastr::success('Operation successful', 'Success');
                return redirect('/medical/clinical-evaluation');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }

        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }

    public function forDeleteItem($id){
        try{
            $url = '/medical/delete-item/'.$id;
            return view('medical::Mediance.delete_modal', compact('url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }
    
    public function itemDestroy($id){
        try{
            $item = item_evaluation::find($id);
            $result = $item->delete();
           
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('/medical/clinical-evaluation');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }
    
    // public function addMedicine (Request $request)
    // {
    // 	$medicine = new Medicine();
    // 	$medicine->name = $request->name;
    // 	$medicine->save();
    // 	return $medicine->id;
    // }

    public function supplyMedicine(){
        $teachers   = Smstaff::join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id','=','9')
                                        ->orwhere('sm_human_departments.id','=','2')
                                        ->orwhere('sm_human_departments.id','=','10')
                                        ->orwhere('sm_human_departments.id','=','12')
                                        ->orwhere('sm_human_departments.id','=','13')
                                        ->get(['sm_staffs.*']);
        $staffs     = DB::table('sm_staffs')->join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id','=','1')
                                        ->orwhere('sm_human_departments.id','=','3')
                                        ->orwhere('sm_human_departments.id','=','4')
                                        ->orwhere('sm_human_departments.id','=','5')
                                        ->orwhere('sm_human_departments.id','=','6')  
                                        ->orwhere('sm_human_departments.id','=','7')
                                        ->orwhere('sm_human_departments.id','=','8')
                                        ->orwhere('sm_human_departments.id','=','14')
                                        ->orwhere('sm_human_departments.id','=','15')
                                        ->orwhere('sm_human_departments.id','=','16')
                                        ->orwhere('sm_human_departments.id','=','17')
                                        ->orwhere('sm_human_departments.id','=','18')
                                        ->orwhere('sm_human_departments.id','=','19')
                                        ->orwhere('sm_human_departments.id','=','20')
                                        ->orwhere('sm_human_departments.id','=','21')
                                        ->orwhere('sm_human_departments.id','=','22')
                                        ->orwhere('sm_human_departments.id','=','23')
                                        ->orwhere('sm_human_departments.id','=','24')
                                        ->orwhere('sm_human_departments.id','=','25')
                                        ->orwhere('sm_human_departments.id','=','26')
                                        ->get(['sm_staffs.*']);
                                        
        $students  = SmStudent::all();
        $medicine_types=SmMedicineType::all();
        $medicines=SmMedicine::all();
        $supplyMedicine = SmSupplyMedicine::all();
        return view('medical::supply-medicine-to-patient',compact('students','teachers','staffs','medicine_types','medicines','supplyMedicine'));
    }

    public function medicineTypeCheck(){
        $medicine_type = $_POST['id'];
        $data = SmMedicine::where('medicine_type',$medicine_type)->get();
        $name = '';
        foreach($data  as $key => $obj){
            $name .= '<option value="'.$obj->id.'">'.$obj->medicine_name.'</option>';
        }
        echo $name;
    }

    public function supplyMedicineStore(Request $request){
        try{
           
            $supplyMedicine = new SmSupplyMedicine;

            $supplyMedicine->staff_name = $request->staff_name;
            $supplyMedicine->medicine_type = $request->medicine_type;
            $supplyMedicine->date = $request->date;
            $supplyMedicine->medicine_name = $request->medicine_name;
            $supplyMedicine->amount = $request->amount;
            
            if($request->patient_student != null && $request->staffType==1){
                $supplyMedicine->patient_name = $request->patient_student;
            } elseif($request->patient_teacher != null && $request->staffType==2){
                $supplyMedicine->patient_name = $request->patient_teacher;
            } else {
                $supplyMedicine->patient_name = $request->patient_staff;
            }
            if($supplyMedicine->save()){
                Toastr::success('Operation successful', 'Success');
                return redirect()->back()->with('message-success', 'Supply Medicine Save');
            }else{
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }
    public function supplyMedicineEdit($id){
        try{
            $teachers   = Smstaff::join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id','=','9')
                                        ->orwhere('sm_human_departments.id','=','2')
                                        ->orwhere('sm_human_departments.id','=','10')
                                        ->orwhere('sm_human_departments.id','=','12')
                                        ->orwhere('sm_human_departments.id','=','13')
                                        ->get(['sm_staffs.*']);
            $staffs     = DB::table('sm_staffs')->join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id','=','1')
                                        ->orwhere('sm_human_departments.id','=','3')
                                        ->orwhere('sm_human_departments.id','=','4')
                                        ->orwhere('sm_human_departments.id','=','5')
                                        ->orwhere('sm_human_departments.id','=','6')  
                                        ->orwhere('sm_human_departments.id','=','7')
                                        ->orwhere('sm_human_departments.id','=','8')
                                        ->orwhere('sm_human_departments.id','=','14')
                                        ->orwhere('sm_human_departments.id','=','15')
                                        ->orwhere('sm_human_departments.id','=','16')
                                        ->orwhere('sm_human_departments.id','=','17')
                                        ->orwhere('sm_human_departments.id','=','18')
                                        ->orwhere('sm_human_departments.id','=','19')
                                        ->orwhere('sm_human_departments.id','=','20')
                                        ->orwhere('sm_human_departments.id','=','21')
                                        ->orwhere('sm_human_departments.id','=','22')
                                        ->orwhere('sm_human_departments.id','=','23')
                                        ->orwhere('sm_human_departments.id','=','24')
                                        ->orwhere('sm_human_departments.id','=','25')
                                        ->orwhere('sm_human_departments.id','=','26')
                                        ->get(['sm_staffs.*']);
            $students  = SmStudent::all();
            $medicine_types=SmMedicineType::all();
            $editData = SmSupplyMedicine::find($id);
            $medicines = SmMedicine::all();
            $supplyMedicine = SmSupplyMedicine::all();
            return view('medical::supply-medicine-to-patient', compact('teachers','supplyMedicine','editData','students','medicine_types','staffs','medicines'));
         }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        } 
    }

    public function supplyMedicineUpdate(Request $request){
        try{

            $supplyMedicine = SmSupplyMedicine::find($request->id);

            $supplyMedicine->staff_name = $request->staff_name;
            $supplyMedicine->medicine_type = $request->medicine_type;
            $supplyMedicine->date = $request->date;
            $supplyMedicine->medicine_name = $request->medicine_name;
            $supplyMedicine->amount = $request->amount;
            
            if($request->patient_student != null && $request->staffType==1){
                $supplyMedicine->patient_name = $request->patient_student;
            } elseif($request->patient_teacher != null && $request->staffType==2){
                $supplyMedicine->patient_name = $request->patient_teacher;
            } else {
                $supplyMedicine->patient_name = $request->patient_staff;
            }
            if($supplyMedicine->save()){
                Toastr::success('Operation successful', 'Success');
                return redirect('/medical/medicine-to-patient');
            }else{
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }
    public function forDeletesupplyMedicine($id){
       
        try{
            $url = '/medical/delete-medicine-to-patient/'.$id;
            return view('medical::Mediance.delete_modal', compact('url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function supplyMedicineDestroy($id){
        try{
            $item = SmSupplyMedicine::find($id);
            $result = $item->delete();
           
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('/medical/medicine-to-patient');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function purchaseProposal(){
        $staffs     = DB::table('sm_staffs')->join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                                        ->where('sm_human_departments.id','=','1')
                                        ->orwhere('sm_human_departments.id','=','3')
                                        ->orwhere('sm_human_departments.id','=','4')
                                        ->orwhere('sm_human_departments.id','=','5')
                                        ->orwhere('sm_human_departments.id','=','6')  
                                        ->orwhere('sm_human_departments.id','=','7')
                                        ->orwhere('sm_human_departments.id','=','8')
                                        ->orwhere('sm_human_departments.id','=','14')
                                        ->orwhere('sm_human_departments.id','=','15')
                                        ->orwhere('sm_human_departments.id','=','16')
                                        ->orwhere('sm_human_departments.id','=','17')
                                        ->orwhere('sm_human_departments.id','=','18')
                                        ->orwhere('sm_human_departments.id','=','19')
                                        ->orwhere('sm_human_departments.id','=','20')
                                        ->orwhere('sm_human_departments.id','=','21')
                                        ->orwhere('sm_human_departments.id','=','22')
                                        ->orwhere('sm_human_departments.id','=','23')
                                        ->orwhere('sm_human_departments.id','=','24')
                                        ->orwhere('sm_human_departments.id','=','25')
                                        ->orwhere('sm_human_departments.id','=','26')
                                        ->get(['sm_staffs.*']);
        $medicine_types = SmMedicineType::all();
        $medicine_proposal = smMedicineProposal::all();
        $supplier = SmSupplier::all();
        return view('medical::purchase-proposal',compact('staffs','medicine_types','medicine_proposal','supplier')); 
    }

    public function purchaseProposalStore(Request $request){
        try{
         
            $medicine_proposal = new smMedicineProposal;
            $medicine_proposal->staff_name = $request->staff_name;
            $medicine_proposal->medicine_type = $request->medicine_type;
            $medicine_proposal->medicine_name = $request->medicine_name;
            $medicine_proposal->date = $request->date;
            $medicine_proposal->supplier = $request->supplier;
            $medicine_proposal->quantity = $request->quantity;
            $medicine_proposal->description = $request->description;

            $data['staff_name'] = $request->staff_name;
            $data['date'] = $request->date;
            $data['medicine_type'] = $request->medicine_type;
            $data['medicine_name'] = $request->medicine_name;
            $data['supplier'] = $request->supplier;
            $data['quantity'] = $request->quantity;
            $data['description'] = $request->description;

            
            if($medicine_proposal->save()){
                Mail::send('medical::Template.reportMail', compact('data'), function ($message) use($data){
                    /* $message->to('anhcao@sgstar.edu.vn')->subject('Medicine Purchase Proposal'); */
                    /* $message->to('tvdkhoa1801@gmail.com')->subject('Medicine Purchase Proposal'); */
                    $message->to('duonghuynh@sgstar.edu.vn')->subject('Medicine Purchase Proposal');
                });
                Toastr::success('Operation successful', 'Success');
                return redirect()->back()->with('message-success', 'Supply Medicine Save');
            }else{
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }

    public function forDeletePurchaseProposal($id){
        try{
            $url = '/medical/delete-medicine-proposal/'.$id;
            return view('medical::Mediance.delete_modal', compact('url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }

    public function PurchaseProposalDestroy($id){
        try{
            $item = smMedicineProposal::find($id);
            $result = $item->delete();
           
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('/medical/purchase-proposal');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        }
    }
    
    public function editPurchaseProposal($id){
        try{
            $staffs     = DB::table('sm_staffs')->join('sm_human_departments', 'sm_staffs.department_id', '=', 'sm_human_departments.id')
                        ->where('sm_human_departments.id','=','1')
                        ->orwhere('sm_human_departments.id','=','3')
                        ->orwhere('sm_human_departments.id','=','4')
                        ->orwhere('sm_human_departments.id','=','5')
                        ->orwhere('sm_human_departments.id','=','6')  
                        ->orwhere('sm_human_departments.id','=','7')
                        ->orwhere('sm_human_departments.id','=','8')
                        ->orwhere('sm_human_departments.id','=','14')
                        ->orwhere('sm_human_departments.id','=','15')
                        ->orwhere('sm_human_departments.id','=','16')
                        ->orwhere('sm_human_departments.id','=','17')
                        ->orwhere('sm_human_departments.id','=','18')
                        ->orwhere('sm_human_departments.id','=','19')
                        ->orwhere('sm_human_departments.id','=','20')
                        ->orwhere('sm_human_departments.id','=','21')
                        ->orwhere('sm_human_departments.id','=','22')
                        ->orwhere('sm_human_departments.id','=','23')
                        ->orwhere('sm_human_departments.id','=','24')
                        ->orwhere('sm_human_departments.id','=','25')
                        ->orwhere('sm_human_departments.id','=','26')
                        ->get(['sm_staffs.*']);
            $editData = smMedicineProposal::find($id); 
            $medicine_types = SmMedicineType::all();
            $medicine_proposal = smMedicineProposal::all();
            $supplier = SmSupplier::all();
            return view('medical::purchase-proposal', compact('staffs','supplier','editData','medicine_types','medicine_proposal'));
         }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back(); 
        } 
    }

    public function updatePurchaseProposal(Request $request){
        try{
            
            $item_update = smMedicineProposal::find($request->id);
            $item_update->staff_name = $request->staff_name;
            $item_update->medicine_type = $request->medicine_type;
            $item_update->medicine_name = $request->medicine_name;
            $item_update->date = $request->date;
            $item_update->supplier = $request->supplier;
            $item_update->quantity = $request->quantity;
            $item_update->description = $request->description;
            if($item_update->save()){
                Toastr::success('Operation successful', 'Success');
                return redirect('/medical/purchase-proposal');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }

        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }
}
