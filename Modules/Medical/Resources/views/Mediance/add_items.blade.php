@extends('backEnd.master')
@section('mainContent')

<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Add Item</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
       
        <div class="row">
           
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($editData))
                                    @lang('lang.edit')
                                @else
                                    @lang('lang.add')
                                @endif
                                Items
                            </h3>
                        </div>
                        @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/medical/add-item-update',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @else
                         {{-- @if(in_array(140, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 93 ) --}}

                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/medical/add-item-store',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        {{-- @endif --}}
                        @endif
                        <div class="white-box">
                            <div class="add-visitor">
                           
                                <div class="row  mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <input class="primary-input form-control"
                                                type="text" name="name" value="{{!empty($editData)? $editData->name: ''}}" maxlength="300">
                                            <label>@lang('lang.name') <span>*</span></label>
                                           
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="{{!empty($editData)? $editData->id: ''}}">
                               
                                {{-- @php 
                                  $tooltip = "";
                                  if(in_array(140, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 93 ){
                                        $tooltip = "";
                                    }else{
                                        $tooltip = "You have no permission to add";
                                    }
                                @endphp --}}
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{@$tooltip}}">
                                            <span class="ti-check"></span>
                                            {{!isset($editData)? "save":"update"}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Items @lang('lang.list')</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                                @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message-success-delete') }}
                                        </div>
                                        @elseif(session()->has('message-danger-delete'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('message-danger-delete') }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <th>No.</th>
                                    <th>@lang('lang.name')</th>
                                    <th>@lang('lang.action')</th>
                                </tr>
                            </thead>
                            @php $count=1; @endphp
                            @foreach($items as $value)
                            <tbody>
                                <th>{{$count++}}</th>
                                <th>{{$value->name}}</th>
                                <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                @lang('lang.select')
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                            @if(in_array(302, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 93 )
                                                <a class="dropdown-item" href="{{url('medical/edit-item/'.$value->id)}}">@lang('lang.edit')</a>
                                            @endif
                                            
                                            @if(in_array(303, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 93 )
                                                <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Items" href="{{url('medical/for-delete-item/'.$value->id)}}">@lang('lang.delete')</a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="modal fade admin-query" id="deleteAddIncomeModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('lang.delete') @lang('lang.income')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="text-center">
                    <h4>@lang('lang.are_you_sure_to_delete')</h4>
                </div>

                <div class="mt-40 d-flex justify-content-between">
                    <button type="button" class="primary-btn tr-bg" data-dismiss="modal">@lang('lang.cancel')</button>
                     {{ Form::open(['route' => 'add_income_delete', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                     <input type="hidden" name="id" value="" id="ncome_id">
                    <button class="primary-btn fix-gr-bg" type="submit">@lang('lang.delete')</button>
                     {{ Form::close() }}
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
