@extends('backEnd.master')
@section('mainContent')
@php
    function showPicName($data){
        $name = explode('/', $data);
        return $name[3];
    }
@endphp

<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1> @if(!empty($editData))
                Update
                @else
                Create
                @endif
                Prescription
            </h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
            </div>
        </div>
    </div>
</section>

<section class="white-box">
    <div class="row">
        <div class="col-md-4">
            <img src="https://sgstar.asia/public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png" width="300" alt="">
        </div>
        
    </div>
    <div class="col-12 mt-30 d-flex justify-content-center">
        <h1>@if(!empty($editData))
                UPDATE
                @else
                CREATE
                @endif PRESCRIPTION
        </h1>
    </div>

        @if(!empty($editData))
            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'medical/update-prescription',
            'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        @else
            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'medical/save-prescription',
            'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        @endif

        @csrf        
        @include('backEnd.partials.alertMessage')                 
            <div class="col-12 row mt-30">
                <div class="col-md-6 col-sm-12">
                    <div class="input-effect sm2_mb_20 md_mb_20">
                        <select
                            class="niceSelect w-100 bb form-control{{ $errors->has('patient_id') ? ' is-invalid' : '' }}"
                            name="patient_id" id="patient_id">
                            <option data-display="Select Patient*"
                                    value="">@lang('lang.select')</option>
                                    @foreach ($patients as $patient)
                                <option value="{{$patient->id}}" <?php if( !empty($editData)) {if($patient->id==$editData->patient_id){echo 'selected';} }  ?> >{{ $patient->patient_name }}</option>
                            @endforeach
                            
                        </select>
                        <span class="focus-border"></span>
                        @if ($errors->has('patient_id'))
                            <span class="invalid-feedback invalid-select" role="alert">
                        <strong>{{ $errors->first('patient_id') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>
                <input type="hidden" name="id" value="{{!empty($editData)? $editData->id: ''}}">
                <div class="col-md-6 col-sm-12">
                    <div class="row  input-right-icon">
                        <div class="col">
                            <div class="row no-gutters input-right-icon">
                                <div class="col">
                                    <div class="input-effect">
                                        <input class="primary-input" type="text" id="placeholderFileOneName"
                                                placeholder=" {{!empty($editData)? ($editData->image != '' ) ? showPicName($editData->image) :'image' :'PRESCRIPTION FILE' }}"
                                                
                                                 readonly>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button class="primary-btn-small-input" type="button">
                                        <label class="primary-btn small fix-gr-bg"
                                                for="document_file_1">@lang('lang.browse')</label>
                                        <input type="file" class="d-none" name="image" id="document_file_1">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            <div class="col-12 row">
                {{-- <div class="col-md-6 col-sm-12">
                    <div class="input-effect sm2_mb_20 md_mb_20">
                        <label class="mt-20">Information of Patient <span></span> </label>
                        <textarea class="primary-input form-control" cols="0" rows="4" name="details" id="details"></textarea>
                        
                        <span class="focus-border textarea"></span>

                    </div>
                </div> --}}

                <div class="col-md-12 col-sm-12">
                    <div class="input-effect sm2_mb_20 md_mb_20">
                        <label class="mt-20">Diagnostic<span></span> </label>
                        <textarea class="primary-input form-control" cols="0" rows="4" name="diagnostic" >{{!empty($editData)? $editData->diagnostic: ''}}</textarea>
                        
                        <span class="focus-border textarea"></span>

                    </div>
                </div>
            </div>

            <div class="col-12 row pt-30">
                <div class="col-md-6 col-sm-12">
                    <div class="input-effect sm2_mb_20 md_mb_20">
                        <input
                            class="primary-input form-control"
                            type="text" name="medicine_id" autocomplete="off" 
                            value="{{!empty($editData)? $editData->id_medicine: ''}}">
                        <label>Medicine ID</label>
                        <span class="focus-border"></span>
                        
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="input-effect sm2_mb_20 md_mb_20">
                        <input
                            class="primary-input form-control"
                            type="text" name="origin" autocomplete="off" 
                            value="{{!empty($editData)? $editData->origin: ''}}">
                        <label>Origin</label>
                        <span class="focus-border"></span>
                        
                    </div>
                </div>
            </div>

            <div class="col-12 row pt-30">
                <div class="col-md-6 col-sm-12">
                    <div class="input-effect sm2_mb_20 md_mb_20">
                        <input
                            class="primary-input form-control"
                            type="text" name="medicine_name" autocomplete="off" 
                            value="{{!empty($editData)? $editData->medicine_name: ''}}">
                        <label>Medicine name</label>
                        <span class="focus-border"></span>
                        
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="input-effect sm2_mb_20 md_mb_20">
                        <input
                            class="primary-input form-control"
                            type="text" name="amount" autocomplete="off" 
                            value="{{!empty($editData)? $editData->amount: ''}}">
                        <label>Quantity</label>
                        <span class="focus-border"></span>
                        
                    </div>
                </div>
            </div>
        
            <div class="col-12 pt-30">
                <div class="input-effect sm2_mb_20 md_mb_20">
                    <input
                        class="primary-input form-control"
                        type="text" name="using" autocomplete="off" 
                        value="{{!empty($editData)? $editData->using: ''}}">
                    <label>Using</label>
                    <span class="focus-border"></span>
                    
                </div>
            </div>
        
            <div class="row mt-30">
                @php 
                    $tooltip = "";
                    if(in_array(300, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                        $tooltip = "";
                    }else{
                        $tooltip = "You have no permission to add";
                    }
                @endphp
                <div class="col-lg-12 text-center mt-30">
                    <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">
                        <span class="ti-check"></span>
                        @if(isset($editData))
                            @lang('lang.update')
                        @else
                            @lang('lang.save')
                        @endif
 
                    </button>
                </div>                  
            </div>
            
    {{ Form::close() }}

</section>

<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row mt-50">
            <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <table id="table_id" class="display school-table mt-sm-20 mt-md-30" cellspacing="0" width="100%">

                        <thead> 
                            
                            <tr>
                                <th>@lang('lang.no')</th>
                                <th>Patient ID</th>
                                <th>Medicine</th>
                                <th>Medicine ID</th>
                                <th>Diagnostic</th>
                                
                                <th>Using</th>
                                <th>Amount</th>
                                <th>Origin</th>
                                <th>Attached files</th>
                                <th>@lang('lang.action')</th>
                            </tr>
                        </thead>

                        <tbody>
                            @php $count=1; @endphp
                            @foreach($prescription as $value)
                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$value->PatientRelation->patient_name}} </td>
                                <td>{{$value->medicine_name}}</td>
                                <td>{{$value->id_medicine}}</td>
                                <td>{{$value->diagnostic}}</td>
                                
                                <td>{{$value->using}}</td>
                                <td>{{$value->amount}}</td>
                                <td>{{$value->origin}}</td>
                                <td>{{$value->book_price}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            @lang('lang.select')
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        @if(in_array(302, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            <a class="dropdown-item" href="{{url('medical/edit-prescription/'.$value->id)}}">@lang('lang.edit')</a>
                                        @endif
                                        @if(in_array(303, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            <a class="dropdown-item small fix-gr-bg modalLink" data-modal-size="modal-lg" title="Prescription Detail" href="{{url('medical/prescriptionDetails/'.$value->id)}}">@lang('lang.view')</a>
                                        @endif
                                        @if(in_array(303, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Prescription" href="{{url('medical/for-delete-prescription/'.$value->id   )}}">@lang('lang.delete')</a>
                                        @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
