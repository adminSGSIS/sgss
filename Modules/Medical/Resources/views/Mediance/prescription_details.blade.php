{{-- <script type="text/javascript" src="{{asset('public/backEnd/js/main.js')}}"></script> --}}

<div class="container-fluid" >
<div id="printMe">
    <div class="row">
        <div class="col-12">
            <h3 class="text-center">PRESCRIPTION</h3>
            <div class="row mt-20">
                <div class="col-8">Date: {{ $patient_infos->date }}</div>
                <div class="col-4">Patient ID: {{ $patient_infos->id }}</div>
            </div>
            <div class="row mt-10">
                <div class="col-5">Patient Name: {{ $patient_infos->patient_name }}</div>
                <div class="col-3">Sexual: 
                    @if ( $patient_infos->gender ==1)
                        Male
                    @elseif ($patient_infos->gender ==2)
                        Female
                    @endif
                    </div>
                <div class="col-4">Date Of Birth: {{ $patient_infos->date_of_birth }}</div>
            </div>
            <div class="mt-10">Address: {{ $patient_infos->address }}</div>
            <div class="mt-10">Diagnostic: {{$prescription->diagnostic}}</div>
        </div>
        
    </div>
    <div class="mt-30">
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Medicine name</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Using</th>
                    <th>Origin</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>{{$prescription->medicine_name}}</td>
                    <td>{{$prescription->amount}}</td>
                    <td>{{$prescription->using}}</td>
                    <td>{{$prescription->origin}}</td>
                </tr> 
            </tbody>
        </table>
    
    </div>
    </div>
    <div class="mt-30">
        <a href="{{$prescription->image}}" target="_blank">
            <button type="button" class="btn btn-success">Attached File</button>
        </a>
        <button  type="button" onclick="printDiv('printMe')" class="float-right btn btn-primary">Print</button>
    </div>
</div>
<script>
		function printDiv(divName){
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;

			document.body.innerHTML = printContents;

			window.print();

			document.body.innerHTML = originalContents;

		}
</script>
