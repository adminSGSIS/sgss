@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Medicine</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>

            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($editData))
                                @lang('lang.edit')
                                @else
                                @lang('lang.add')
                                @endif
                                Medicine
                            </h3>
                        </div>
                        @if(isset($editData))
                        <form action="{{ route('medicine-update',$editData->id) }}" method="post">
                            <!--Update-->
                            @csrf
                            @else
                            <form action="{{ route('medicine-store') }}" method="POST">
                                <!--Create-->
                                @csrf
                                @endif
                                <div class="white-box">
                                    <div class="add-visitor">
                                        <div class="row  mt-25">
                                            <div class="col-lg-12">
                                                <select name="medicinetype" id="symtomsType"
                                                    class="form-control w-100 niceSelect bb">
                                                    <option selected disabled><label for="symtomsType">Medicine
                                                            Type</label></option>
                                                    @foreach ($medicine_types as $medicine_type)
                                                    <option value="{{ $medicine_type->id }}"
                                                        {{-- < ?php if( !empty($patientData)) {if($patientData->symtome_type==$symtome->id){echo 'selected';} }  ?> --}}>
                                                        {{ $medicine_type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row  mt-25">
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text"
                                                        name="medicinecode" required
                                                        value="{{!empty($editData)? $editData->medicine_code: ''}}">
                                                    <label>@lang('Medicine code') <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="name"
                                                        value="{{!empty($editData)? $editData->medicine_name: ''}}"
                                                        required>
                                                    <label>@lang('lang.name') <span>*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row  mt-25">
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="unit"
                                                        value="{{!empty($editData)? $editData->unit: ''}}" required>
                                                    <label>@lang('Unit') <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="price"
                                                        value="{{!empty($editData)? $editData->price: ''}}" required>
                                                    <label>@lang('Price') <span>*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row  mt-25">
                                            <div class="col-lg-6">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control date" type="text"
                                                            name="manufacturingdate" id="apply_date" required
                                                            value="{{!empty($editData)? $editData->manufacturing_date: ''}}">
                                                        <label>@lang('Manufacturing Date') <span>*</span></label>
                                                        <span class="focus-border"></span>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="" type="button">
                                                            <i class="ti-calendar" id="apply_date_icon"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control date" type="text"
                                                            name="expirydate" id="expiry_date" required
                                                            value="{{!empty($editData)? $editData->expiry_date: ''}}">
                                                        <label>@lang('Expiry Date') <span>*</span></label>
                                                        <span class="focus-border"></span>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="" type="button">
                                                            <label for="expiry_date"><i class="ti-calendar"
                                                                    id="expiry_date_icon"></i></label>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row  mt-25">
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text"
                                                        name="producer"
                                                        value="{{!empty($editData)? $editData->producer: ''}}" required>
                                                    <label>@lang('Producer') <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="stock"
                                                        value="{{!empty($editData)? $editData->stock: ''}}" required>
                                                    <label>@lang('Stock') <span>*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="{{!empty($editData)? $editData->id: ''}}">
                                        <div class="row mt-40">
                                            <div class="col-lg-12 text-center">
                                                <button class="primary-btn fix-gr-bg" data-toggle="tooltip"
                                                    title="{{@$tooltip}}">
                                                    <span class="ti-check"></span>
                                                    {{!isset($editData)? "save":"update"}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Medicine @lang('lang.list')</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                                @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message-success-delete') }}
                                        </div>
                                        @elseif(session()->has('message-danger-delete'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('message-danger-delete') }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <th>No.</th>
                                    <th>@lang('Medicine Type')</th>
                                    <th>@lang('Medicine code')</th>
                                    <th>@lang('Medicine name')</th>
                                    <th>@lang('Unit')</th>
                                    <th>@lang('Price')</th>
                                    <th>@lang('Producer')</th>
                                    <th>@lang('Stock')</th>
                                    <th>@lang('Manufacturing date')</th>
                                    <th>@lang('Expiry Date')</th>
                                    <th>@lang('lang.action')</th>
                                </tr>
                            </thead>
                            @php $count=1; @endphp
                            <tbody>
                                @foreach($medicines as $value)
                                <tr>
                                    <th>{{$count++}}</th>
                                    <th>{{$value->medicineTypeRelation->name}}</th>
                                    <th>{{$value->medicine_code}}</th>
                                    <th>{{$value->medicine_name}}</th>
                                    <th>{{$value->unit}}</th>
                                    <th>{{$value->price}}</th>                                    
                                    <th>{{$value->producer}}</th>
                                    <th>{{$value->stock}}</th>
                                    <th>{{$value->manufacturing_date}}</th>
                                    <th>{{$value->expiry_date}}</th>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                @lang('lang.select')
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">

                                                <a class="dropdown-item"
                                                    href="{{url('medical/medicine-edit/'.$value->id)}}">@lang('lang.edit')</a>
                                                <a href="{{ url('medical/medicine-delete/'.$value->id) }}"
                                                    class="dropdown-item">@lang('lang.delete')</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection