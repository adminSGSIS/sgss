@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Medicine import Manager</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>

            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="white-box">
                    @if (isset($editData))
                    <form action="{{ route('medicine-management-update',$editData->id) }}" method="POST">
                        @csrf
                        @else
                        <form action="{{ route('medicine-management-store') }}" method="POST">
                            @csrf
                            @endif

                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="input-effect">
                                            <input class="primary-input form-control" type="text" name="name"
                                                value="<?php echo date("Y/m/d");?>" readonly>
                                            <label>@lang('lang.date') <span>*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-effect">
                                            <select name="staff" id="" class="niceSelect w-100 bb form-control">
                                                <option disabled selected><label>@lang('employee')
                                                        <span>*</span></label>
                                                </option>
                                                @foreach ($staffs as $staff)
                                                <option value="{{ $staff->id }}"
                                                    <?php if( !empty($editData)) {if($editData->staff==$staff->id){echo 'selected';} }  ?>>
                                                    {{ $staff->full_name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-effect">
                                            <select name="department" id="" class="niceSelect w-100 bb form-control">
                                                <option disabled selected><label>@lang('department')
                                                        <span>*</span></label>
                                                </option>
                                                @foreach ($departments as $department)
                                                <option value="{{ $department->id }}"
                                                    <?php if( !empty($editData)) {if($editData->deparment==$department->id){echo 'selected';} }  ?>>
                                                    {{ $department->name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-effect">
                                            <select name="supplier" id="" class="niceSelect w-100 bb form-control">
                                                <option disabled selected><label>@lang('supplier')
                                                        <span>*</span></label>
                                                </option>
                                                @foreach ($suppliers as $supplier)
                                                <option value="{{ $supplier->id }}"
                                                    <?php if( !empty($editData)) {if($editData->supplier==$supplier->id){echo 'selected';} }  ?>>
                                                    {{ $supplier->company_name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-5" style="padding-top: 25px">
                                    <h3>@lang('Medicine detail')</h3>
                                    <div class="container">
                                        <div class="row mb-5">
                                            <div class="col-lg-6">
                                                {{-- <div id="render"></div> --}}
                                                <div class="input-effect">
                                                    <label>@lang('Medicine type') <span>*</span></label>
                                                    <select name="medicine_type" id="medicine_type"
                                                        class="niceSelect w-100 bb form-control">
                                                        @foreach ($medicine_types as $medicine_type)
                                                        <option value="{{ $medicine_type->id }}"
                                                            <?php if( !empty($editData)) {if($editData->medicine_type==$medicine_type->id){echo 'selected';} }  ?>>
                                                            {{ $medicine_type->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="focus-border"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="modal">Name of Medicine</label>
                                                    <select name="medicine" id="modal"
                                                        class="custom-select primary-input ">
                                                        <option value="">Select Medicine</option>
                                                    </select>
                                                </div>
                                                {{-- <div class="input-effect">
                                                    <label>@lang('Medicine Code') <span>*</span></label>
                                                    <select id="abc" name="medicinecode"
                                                        class="niceSelect w-100 bb form-control">
                                                        @foreach ($medicines as $medicine)
                                                        <option  value="{{ $medicine->id }}"
                                                < ?php if( !empty($editData)) {if($editData->
                                                    medicine_code==$medicine->id){echo 'selected';} } ?>>
                                                    {{ $medicine->medicine_code }}
                                                    </option>
                                                    @endforeach
                                                    </select>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12 mb-5">
                                                    <div class="no-gutters input-right-icon">
                                                        <div class="input-effect">
                                                            <input class="primary-input form-control date" type="text"
                                                                name="importdate" id="apply_date" required
                                                                value="{{!empty($editData)? $editData->import_date: ''}}">
                                                            <label>@lang('import Date') <span>*</span></label>
                                                            <span class="focus-border"></span>
                                                        </div>
                                                        <div class="col-auto">
                                                            <button class="" type="button">
                                                                <i class="ti-calendar" id="apply_date_icon"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control" type="text"
                                                            name="amount"
                                                            value="{{ isset($editData)? $editData->amount: '' }}"
                                                            required>
                                                        <label>@lang('amount') <span>*</span></label>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-lg-6">
                                            <div class="input-effect sm2_mb_20 md_mb_20">
                                                <textarea required class="primary-input form-control" name="note"
                                                    autocomplete="off" id="" cols="30"
                                                    rows="8">{{ !empty($editData) ? $editData->note:'' }}</textarea>
                                                <label>@lang('note')<span> *</span> </label>
                                                <span class="focus-border"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-40">
                                <div class="col-lg-12 text-center">
                                    <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{@$tooltip}}">
                                        <span class="ti-check"></span>
                                        {{!isset($editData)? "save":"update"}}
                                    </button>
                                </div>
                            </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">Medicine @lang('lang.list')</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                        <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                            <tr>
                                <td colspan="7">
                                    @if(session()->has('message-success-delete'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message-success-delete') }}
                                    </div>
                                    @elseif(session()->has('message-danger-delete'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('message-danger-delete') }}
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <th>No.</th>
                                <th>@lang('staff ')</th>
                                <th>@lang('deparment ')</th>
                                <th>@lang('supplier')</th>
                                <th>@lang('medicine name')</th>
                                <th>@lang('amount')</th>
                                <th>@lang('import_date')</th>
                                <th>@lang('medicine_type')</th>
                                <th>@lang('note')</th>
                                <th>@lang('lang.action')</th>
                            </tr>
                        </thead>
                        @php $count=1; @endphp
                        
                        <tbody>
                            @foreach($medicineManagements as $value)
                            <tr>
                                <th>{{$count++}}</th>
                            <th>{{$value->staffRelation->full_name}}</th>
                            <th>{{$value->departmentRelation->name}}</th>
                            <th>{{$value->supplierRelation->company_name}}</th>
                            <th>{{$value->medicineRelation->medicine_name}}</th>
                            <th>{{$value->amount}}</th>
                            <th>{{$value->import_date}}</th>
                            <th>{{$value->medicineTypeRelation->name}}</th>
                            <th>{{$value->note}}</th>
                            <td>
                                <div class="dropdown">
                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                        @lang('lang.select')
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">

                                        <a class="dropdown-item"
                                            href="{{url('medical/medicine-management-edit/'.$value->id)}}">@lang('lang.edit')</a>
                                        <a href="{{ url('medical/medicine-delete/'.$value->id) }}"
                                            class="dropdown-item">@lang('lang.delete')</a>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $("#medicine_type").change(function (event) {
            medicinetypeID = $("#medicine_type").val();
            console.log(medicinetypeID);
            $.ajax({
                url: "/medical/medicine-type-check",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: medicinetypeID
                },
                success: function (result) {
                    console.log($("#modal").html(result));
                    //dữ liệu trả về từ controller
                    $("#modal").html(result);
                }
            });
        });
    });
</script>
@endsection