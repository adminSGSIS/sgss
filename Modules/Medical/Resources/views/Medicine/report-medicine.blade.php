@extends('backEnd.master')
@section('mainContent')
<style>
    .pad-top-20 {
        padding-top: 20px;
    }
    .pad-bot-20{
        padding-bottom: 20px;
    }
    .hidden {
        display: none;
    }
</style>
<div class="white-box">
    <h2 class="center" style="text-align: center;">Create Report</h2>    
    <div class="container">
        <div class="pad-top-20">
            <h1>Select method</h1>
            <div class="row">
                <div class="col-lg-3">                
                </div>
                <div class="col-lg-3 input-effect d-flex justify-content-end">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="2" name="patientType" id="mailRadio"
                            onclick="checkType()">
                        <label class="form-check-label" for="mailRadio">
                            Email
                        </label>
                    </div>
                </div>
                <div class="col-lg-3 input-effect">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="3" name="patientType" id="pdfRadio"
                            onclick="checkType()">
                        <label class="form-check-label" for="pdfRadio">
                            Pdf
                        </label>
                    </div>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
        </div>
        <div class="pad-top-20 hidden" id="mailSelect">
            <form {{-- action="{{ route('report-pdf') }} --}} action="{{ route('report-mail') }}">
                @csrf
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <select name="month" id="" class="niceSelect w-100 bb form-control">
                            <option disabled selected>Month</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <div class="d-flex justify-content-center" style="padding-top: 35px">
                    {{-- <input type="submit" class="primary-btn small fix-gr-bg" value="PDF"> --}}
                    <input type="submit" class="primary-btn small fix-gr-bg" value="Send report to mail">
                </div>
            </form>
        </div>
        <div class="pad-top-20 hidden" id="pdfSelect">
            <form action="{{ route('report-pdf') }}" formtarget="_blank" target="_blank">
                @csrf
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <select name="month" id="" class="niceSelect w-100 bb form-control">
                            <option disabled selected>Month</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
                <div class="d-flex justify-content-center" style="padding-top: 35px">
                    {{-- <input type="submit" class="primary-btn small fix-gr-bg" value="PDF"> --}}
                    <input type="submit" class="primary-btn small fix-gr-bg" value="Export to PDF">
                </div>
            </form>
        </div>
    </div>
</div>
</div>

@endsection
@section('script')
<script>
    function checkType() {
        if (document.getElementById('pdfRadio').checked) {
            document.getElementById("mailSelect").setAttribute("class", "hidden");
            document.getElementById("pdfSelect").removeAttribute("class", "hidden");
        } else if (document.getElementById('mailRadio').checked) {            
            document.getElementById("mailSelect").removeAttribute("class", "hidden");
            document.getElementById("pdfSelect").setAttribute("class", "hidden")
        }
    }
</script>
@endsection