<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <title>{{$title}}</title> --}}
    <style>
        body {
            height: 100%;
            width: 100%;
            font-family: 'DejaVu Sans';            
        }
        *{
            font-size: 17px!important;
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            font-size: 20px;
        }

        @page {
            margin: 50px 25px;
        }
        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 150px;
        }

        footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px
        }
        .bold {
            font-weight: bold;
        }
    </style>
</head>

<body>
    <img src="https://sgstar.edu.vn/public/header_letter.png" alt="" style="width: 100%;">
    {{-- <header></header> --}}
    <footer><img src="{{ asset('public/footer_letter.png') }}" alt="" style="width: 100%;"></footer>
    <main style="padding-top: 60px">
        <div style="page-break-inside: auto">

            <div style="text-align: center;">
                <h1>Medicine Purchase Proposal</h1>
            </div>
            <div class="container">
                <div style="text-align: justify">
                    Dear Saigon Star International School Board of Directors. <br><br>
                    My name is {{ Auth::user()->full_name }}, I suggest to buy the following medicine.
                </div>
                <br>
            </div>
            Report <br>
            <h3>Medicine Purchase Proposal</h3>
            <table class="student-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Staff Name</th>
                        <th colspan="2">Date</th>
                        <th>Medicine Type</th>
                        <th>Medicine Name</th>
                        <th colspan="3">Supplier</th>
                        <th>Quantity</th>
                        <th colspan="3">Description</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $number=1;
                    @endphp
                   
                    <tr>
                        <td>{{ $number++ }}</td>
                        <td>{{$data['staff_name']}}</td>
                        <td colspan="2">{{ date('d/m/Y', strtotime($data['date'])) ?? ''}}</td>
                        <td>{{ $data['medicine_type']}}</td>
                        <td>{{ $data['medicine_name']}}</td>
                        <td colspan="3"> {{$data['supplier']}} </td>
                        <td>{{ $data['quantity']}}</td>
                        <td colspan="3">{{$data['description']}}</td>
                    </tr>
                   
                </tbody>
            </table>
            <br>
           
           
    </main>
    </div>
    <img src="https://sgstar.edu.vn/public/footer_letter.png" alt="" style="width: 100%;">
</body>