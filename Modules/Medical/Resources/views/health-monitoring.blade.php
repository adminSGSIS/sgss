@extends('backEnd.master')
@section('mainContent')
<div class="mb-5">
    <form method="Get" action="{{ route('healthMonitoringCreate') }}">
        @csrf
        <div class="white-box">
            <h2 class="center" style="text-align: center;">Health Monitoring</h2>
            <div class="container">
                <div id="studentBoard">
                    <div class="pad-top-20">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <select name="student_id" id="student_list"
                                    class="niceSelect w-100 bb form-control{{ @$errors->has('students') ? ' is-invalid' : '' }}">
                                    <option data-display="@lang('student name') *" value="">@lang('student name') *</option>
                                    @foreach($students as $student)
                                    @if(isset($add_income))
                                    <option value="{{@$student->id}}"
                                        {{@$add_income->student_id == @$student->id? 'selected': ''}}>
                                        {{@$student->full_name}} | {{@$student->admission_no}}</option>
                                    @else
                                    <option label="{{@$student->admission_no}}" value="{{@$student->id}}"
                                        {{old('student_list') == @$student->id? 'selected' : ''}}>{{@$student->full_name}} |
                                        {{@$student->admission_no}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12"></div>
                </div>
                <div class="d-flex justify-content-end" style="padding-top: 35px">
                    <input type="submit" class="primary-btn small fix-gr-bg" value="search">
                </div>
            </div>
        </div>
    </form>
</div>

<div>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3>@lang('Health Monitoring')</h3>
                        </div>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-lg-12">
    
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
    
                            <thead>
                                @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message-success-delete') }}
                                        </div>
                                        @elseif(session()->has('message-danger-delete'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('message-danger-delete') }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <th>@lang('No')</th>
                                    <th>@lang('Name')</th>
                                    <th>@lang('Class')</th>
                                    <th>@lang('Update time')</th>
                                    <th>@lang('action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($healthMonitorings as $healthMonitoring)
                                <tr>
                                    <td>{{ $healthMonitoring->id }}</td>
                                    <td>{{ $healthMonitoring->student_name }}</td>
                                    <td>{{ $healthMonitoring->className->class_name }}</td>
                                    <td>
                                        @php
                                            $healthUpdate=$healthMonitoring->updated_at;
                                            $trialExpires = $healthUpdate->addMonth(6);
                                            $now=Carbon::now();
                                        @endphp
                                        @if($trialExpires->lt($now))
                                        <button class="btn btn-danger" style="font-size: 13px;">Updated Now</button>
                                        @else
                                        <button class="btn btn-success" style="font-size: 13px;">{{date('d-m-Y', strtotime($trialExpires))}}</button>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                @lang('lang.select')
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a title="View/Edit Leave Details"
                                                    class="dropdown-item" href="{{ url('medical/healthMonitoring-edit/'.$healthMonitoring->student_id) }}">@lang('Detail')</a>
                                                    <a title="View/Edit Leave Details" target="_blank"
                                                    class="dropdown-item" href="{{ url('medical/healthMonitoring-PDF/'.$healthMonitoring->id.'/'.$healthMonitoring->student_id) }}">@lang('Export PDF')</a>
                                                <a class="dropdown-item" data-toggle="modal"
                                                    data-target="#DeleteHealthMonitoring{{$healthMonitoring->id}}"
                                                    href="#">@lang('lang.delete')</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade admin-query" id="DeleteHealthMonitoring{{$healthMonitoring->id}}">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">@lang('lang.delete') @lang('lang.item')</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4>@lang('lang.are_you_sure_to_delete')</h4>
                                                </div>
    
                                                <div class="mt-40 d-flex justify-content-between">
                                                    <button type="button" class="primary-btn tr-bg"
                                                        data-dismiss="modal">@lang('lang.cancel')</button>
                                                    {{ Form::open(['url' => 'medical/healthMonitoring-delete/'.$healthMonitoring->student_id, 'method' => 'DELETE', 'enctype' => 'multipart/form-data']) }}
                                                    <button class="primary-btn fix-gr-bg"
                                                        type="submit">@lang('lang.delete')</button>
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
    
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection