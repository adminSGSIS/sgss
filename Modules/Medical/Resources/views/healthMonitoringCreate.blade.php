@extends('backEnd.master')
@section('mainContent')
<div class="mb-3">
    <div class="text-center">
        <h1>Health Monitoring</h1>
    </div>

    <div class="row mb-3">
        <div class="col-lg-4">
            <h3>Creator</h3>
            <div class="white-box">
                Full Name: {{ Auth::user()->full_name }} <br><br>
                Email: {{ Auth::user()->email }} <br><br>
                Phone: {{ Auth::user()->staff->mobile }}
            </div>
        </div>
        <div class="col-lg-8">
            <h3>Student Name</h3>
            <div class="white-box">
                <div class="row mb-3">
                    <div class="col-lg-6">Full Name: {{ $student->full_name }}</div>
                    <div class="col-lg-6">Age: {{ $student->age }}</div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Height: {{ $student->height }}</div>
                    <div class="col-lg-6">Weight: {{ $student->weight }}</div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Class: {{ $student->className->class_name }}</div>
                    <div class="col-lg-6">Date of birth: {{ $student->date_of_birth }}</div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Blood pressure:
                        {{ !empty($student_health) ? $student_health->blood_pressure : '' }}</div>
                    <div class="col-lg-6">Heartbeat: {{ !empty($student_health) ? $student_health->heart_beat : '' }}
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Right eye: {{ !empty($student_health) ? $student_health->right_eye : '' }}
                    </div>
                    <div class="col-lg-6">Left eye: {{ !empty($student_health) ? $student_health->left_eye: '' }}</div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Myopic: {{ !empty($student_health) && $student_health->myopic}}</div>
                    <div hidden></div>
                </div>
            </div>
        </div>
    </div>
    <div> @if(empty($student_health))
        <form action="{{ route('healthMonitoringStore') }}">
            @csrf
            @else
            <form action="{{ route('healthMonitoringUpdate',$student_health->student_id) }}">
                @csrf
                @endif
                <input type="text" value="{{ $student->id }}" name="id_student" hidden>
                <input type="text" value="{{ $student->full_name }}" name="student_name" hidden>
                <input type="text" value="{{ $student->class_id }}" name="class" hidden>
                <div class="white-box mb-3">
                    <div class="row mb-3">
                        <div class="col-lg-12">
                            <div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="input-efect">
                                            <label for="date">Day of birth:</label>
                                            <input type="text" name="date" class="form-control date primary-input"
                                                value="{{ $student->date_of_birth }}" readonly>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="input-efect">
                                            <label for="age">Age:</label>
                                            <input type="text" name="age" class="form-control primary-input"
                                                value="{{ $student->age }}" readonly>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3>Physical</h3>
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <div class="form-group input-effect">
                                <label for="date">Height:</label>
                                <input type="text" name="height" class="form-control primary-input"
                                    value="{{ $student->height }}" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group input-effect">
                                <label for="date">Weight:</label>
                                <input type="text" name="weight" class="form-control primary-input"
                                    value="{{ $student->weight }}" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group input-effect">
                                <label for="date">Blood pressure:</label>
                                <input type="text" name="blood_pressure" class="form-control primary-input"
                                    value="{{ !empty($student_health) ? $student_health->blood_pressure : '' }}">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group input-effect">
                                <label for="date">Heartbeat:</label>
                                <input type="text" name="heart_beat" class="form-control primary-input"
                                    value="{{ !empty($student_health) ? $student_health->heart_beat : '' }}">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h3 class="mb-3">Eyes sight</h3>
                        <div class="form-check mb-3">
                            <input type="checkbox" name="myopic" value="1" id="eyesight" class="form-check-input"
                                name="eyesight"
                                {{ !empty($student_health) && $student_health->myopic == 1 ? 'checked': ''}}>
                            <label for="eyesight" class="form-check-label">Myopic</label>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group input-effect">
                                    <label for="date">Right eye:</label>
                                    <input type="text" name="right_eye" class="form-control primary-input"
                                        value="{{ !empty($student_health) ? $student_health->right_eye : '' }}">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group input-effect">
                                    <label for="date">Left eye:</label>
                                    <input type="text" name="left_eye" class="form-control primary-input"
                                        value="{{ !empty($student_health) ? $student_health->left_eye : '' }}">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(empty($student_health))
                    <div class="col-lg-12 text-center mt-30">
                        <button class="primary-btn fix-gr-bg">
                            <span class="ti-check"></span>
                            @lang('lang.save')
                        </button>
                    </div>
                    @else
                    <div class="col-lg-12 text-center mt-30">
                        <button class="primary-btn fix-gr-bg">
                            <span class="ti-check"></span>
                            @lang('lang.update')
                        </button>
                    </div>
                    @endif
            </form>
    </div>
</div>

</div>


<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3>@lang('Patient History')</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                        <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                            <tr>
                                <td colspan="7">
                                    @if(session()->has('message-success-delete'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message-success-delete') }}
                                    </div>
                                    @elseif(session()->has('message-danger-delete'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('message-danger-delete') }}
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <th>@lang('No')</th>
                                <th>@lang('Name')</th>
                                <th>@lang('Class')</th>
                                <th>@lang('Date')</th>
                                <th>@lang('Symtome Type')</th>
                                <th>@lang('action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($patients as $patient)
                            <tr>
                                <td>{{ $patient->id }}</td>
                                <td>{{ $patient->patient_name }}</td>
                                <td>{{ $patient->symtome_type }}</td>
                                <td>{{ $patient->date }}</td>
                                <td>{{ $patient->symtomeRelation->name }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            @lang('lang.select')
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a title="Health Summary" class="dropdown-item"
                                                href="{{ url('medical/healthMonitoring-Summary/'.$patient->id.'/'.$patient->patient_id) }}">@lang('Health
                                                Summary')</a>
                                            {{-- <a class="dropdown-item" data-toggle="modal"
                                                data-target="#DeletePatient{{$patient->id}}"
                                                href="#">@lang('lang.delete')
                                            </a> --}}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            {{-- <div class="modal fade admin-query" id="DeletePatient{{$patient->id}}">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">@lang('lang.delete') @lang('lang.item')</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <h4>@lang('lang.are_you_sure_to_delete')</h4>
                                            </div>

                                            <div class="mt-40 d-flex justify-content-between">
                                                <button type="button" class="primary-btn tr-bg"
                                                    data-dismiss="modal">@lang('lang.cancel')</button>
                                                {{ Form::open(['url' => 'medical/patient-delete/'.$patient->id, 'method' => 'DELETE', 'enctype' => 'multipart/form-data']) }}
                                                <button class="primary-btn fix-gr-bg"
                                                    type="submit">@lang('lang.delete')</button>
                                                {{ Form::close() }}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div> --}}
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection