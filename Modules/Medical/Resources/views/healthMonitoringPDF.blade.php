<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    <style>
        body {
            height: 100%;
            width: 100%;
            font-family: 'DejaVu Sans';
        }
    
        .letter_color {
            color: #424040d2;
        }
        .ft200 {
            font-size: 200% !important;
        }
        div>h1 {}
    
        .footer {
            bottom: 0;
            position: fixed;
            width: 100%;
        }
    
        .info {
            font-size: 170% !important;
        }
    
        .p-2>h1 {
            font-size: 100px;
        }
    
        table {
            border-collapse: collapse;
            width: 100%;
        }
        .student-table td,th{
            width: 100%;
  border-collapse: collapse;
  border: 1px solid white!important;
  text-align: center;
        }
        td,
        th {
            border: 1px solid #31b6f0;
            border-top: 1px #31b6f0 solid!important;
            text-align: justify;
            padding: 8px 8px 8px 8px !important;
            font-size: 18px;
            vertical-align: top;
            /* color: #0D52AD; */
        }
        .c-black{
            color: #000!important;
        }
        @page {
            margin-bottom: 0;
            margin-top: 0;
        }
    
        .bold {
            font-weight: bold;
        }
    
        .ft300 {
            font-size: 18px;
            color: rgba(66, 64, 64, 0.822);
        }
    
        .fz25 {
            font-size: 22px;
            line-height: 1.3;
        }
        .w-50{
            width: 50%;
        }
    </style>
</head>
<body>
    <img src="{{ asset('public/header_letter.png') }}" alt="" width="100%">
    <div style="text-align: center">
        <h1>Health Monitoring</h1>
        <h3>Student name: {{ $student->full_name }}</h3>
        <table class="student-table">
            <tbody>
                <td class="w-50 c-black" colspan="2"><h4>Class: {{ $student->className->class_name }}</h4></td>
                <td class="w-50 c-black" colspan="2"><h4>Date of birth: {{ $student->date_of_birth }}</h4></td>
            </tbody>
        </table>
    </div>
        <div>            
            <table class="table tablePDF">
                <tbody>
                    <tr>
                        <td class="w-50" colspan="2">Height: {{ $student->height }}</td>
                        <td class="w-50" colspan="2">Weight: {{ $student->weight }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" >Myopic: {{ ( $healthMonitoring->myopic == 1)?'yes':'no' }}</td>
                        <td colspan="2" >Age: {{ $student->age }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="w-50">Left eye: {{ $healthMonitoring->left_eye }}</td>
                        <td colspan="2" class="w-50">Right eye: {{ $healthMonitoring->right_eye }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="w-50">Blood pressure: {{ $healthMonitoring->blood_pressure }}</td>
                        <td colspan="2" class="w-50">Heartbeat: {{ $healthMonitoring->heart_beat }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    <img src="{{ asset('public/footer_letter.png') }}" alt="" style="bottom: 150px;position: fixed;width: 100%;"> 
</body>