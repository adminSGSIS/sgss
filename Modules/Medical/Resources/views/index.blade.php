@extends('backEnd.master')
@section('mainContent')
<style>
    .pad-top-20 {
        padding-top: 20px;
    }

    .hidden {
        display: none;
    }
</style>

<div>
    @if (empty($patientData))
    <form action="{{ route('StoreMedical') }}" method="POST" enctype="multipart/form-data">
        @csrf
    @else
    <form action="{{ url('medical/update-medical'.'/'.$patientData->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
    @endif        
        <h1>Medical</h1>
        <input type="text" value="{{ $id ?? ''}}" hidden name="patient_id">
        <input type="text" value="{{ $type ?? ''}}" id="type" hidden name="patient_type" onloadstart="checktype()">
        <div class="white-box">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group mb-3 input-effect">
                        <label for="admissionDate">Date</label>
                        <div>
                            <input type="text" name="date" class="form-control primary-input"
                                value="<?php echo date("Y/m/d");?>" readonly>
                        </div>
                        <span class="focus-border"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group mb-3 input-effect">
                        <label for="Case">Case</label>
                        <input type="text" class="form-control primary-input" id="Case" name="caseInput" required value="{{ !empty($patientData) ? $patientData->case : '' }}">
                        <span class="focus-border"></span>
                    </div>
                </div>
            </div>
            <div id="studentBoard" class="hidden">
                <div class="pad-top-20">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group mb-3 input-effect">
                                <label for="studentName">Student Name</label>
                                <input type="text" name="student_name" class="form-control primary-input"
                                    id="studentName" value="{{ $student->full_name ?? '' }}" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group mb-3 input-effect">
                                <label for="Admission">Admission Number</label>
                                <input type="text" class="form-control primary-input" id="Admission"
                                    value="{{ $student->admission_no ?? '' }}" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3 input-effect">
                                <label for="patientId">Blood Group</label>
                                <input type="text" class="form-control primary-input" id="bloodGroup"
                                    value="{{ $student->bloodgroup_id ?? ''}}" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3 input-effect">
                                <label for="height">Height</label>
                                <input type="text" class="form-control primary-input" id="height"
                                    value="{{ $student->height ?? '' }}" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3 input-effect">
                                <label for="weight">Weight</label>
                                <input type="text" class="form-control primary-input" id="weight"
                                    value="{{ $student->weight ?? '' }}" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3 input-effect">
                                <label for="adress">Address</label>
                                <input type="text" class="form-control primary-input" id="adress"
                                    value="{{ $student->current_address ?? '' }}" readonly name="student_address">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3 input-effect">
                                <label for="weight">Gender</label>
                                <input type="text" class="form-control primary-input" id="weight"
                                    value="{{ $student->gender_id ?? '' }}" readonly name="student_gender">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3 input-effect">
                                <label for="weight">Date Of Birth</label>
                                <input type="text" class="form-control primary-input" id="weight"
                                    value="{{ $student->date_of_birth ?? '' }}" readonly name="student_birth">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pad-top-20">
                    <h1>Contact Parent</h1>
                    <div class="row">
                        <div class="col-lg-4">
                            Father name: {{ $parent->fathers_name ?? ''}}
                        </div>
                        <div class="col-lg-4">
                            Email: {{ $parent->fathers_email ?? ''}}
                        </div>
                        <div class="col-lg-4">
                            Phone: {{ $parent->fathers_phone ?? ''}}
                        </div>
                    </div>
                    <p></p>
                    <div class="row">
                        <div class="col-lg-4">
                            Mother name: {{ $parent->mothers_name ?? ''}}
                        </div>
                        <div class="col-lg-4">
                            Email: {{ $parent->mothers_email ?? ''}}
                        </div>
                        <div class="col-lg-4">
                            Phone: {{ $parent->mothers_phone ?? ''}}
                        </div>
                    </div>
                </div>
            </div>
            <div id="teacherBoard" class="pad-top-20 hidden">
                <div class="pad-top-20">
                    <h2>Teacher information</h2>
                    <div class="row">
                        <div class="col-lg-3">Teacher's number: <input type="text" class="form-control primary-input"
                                readonly value="{{ $staffs->staff_no ?? ''}}"></div>
                        <div class="col-lg-3">Full name: <input name="teacher_name" type="text"
                                class="form-control primary-input" readonly value="{{ $staffs->full_name ?? ''}}"></div>
                        <div class="col-lg-3">Email: <input type="text" class="form-control primary-input" readonly
                                value="{{ $staffs->email ?? ''}}"></div>
                        <div class="col-lg-3">Mobile: <input type="text" class="form-control primary-input" readonly
                                value="{{ $staffs->mobile ?? ''}}"></div>
                        <div class="col-lg-3">Current Address: <input type="text" class="form-control primary-input"
                                readonly value="{{ $staffs->current_address ?? ''}}" readonly name="teacher_address">
                        </div>
                        <div class="col-lg-3">Gender: <input type="text" class="form-control primary-input" readonly
                                value="{{ $staffs->gender_id ?? ''}}" name="teacher_gender"></div>
                        <div class="col-lg-3">Date Of Birth: <input type="text" class="form-control primary-input"
                                readonly value=" {{ $staffs->date_of_birth ?? ''}}" name="teacher_birth"></div>
                    </div>
                </div>
            </div>
            <div id="staffBoard" class="pad-top-20 hidden">
                <div class="pad-top-20">
                    <h2>Staff information</h2>
                    <div class="row">
                        <div class="col-lg-3">Teacher's number: <input type="text" class="form-control primary-input"
                                readonly value="{{ !empty($staffs)? $staffs->staff_no : ''}}"></div>
                        <div class="col-lg-3">Full name: <input name="staff_name" type="text"
                                class="form-control primary-input" readonly value="{{ $staffs->full_name ?? ''}}"></div>
                        <div class="col-lg-3">Email: <input type="text" class="form-control primary-input" readonly
                                value="{{ $staffs->email ?? ''}}"></div>
                        <div class="col-lg-3">Mobile: <input type="text" class="form-control primary-input" readonly
                                value="{{ $staffs->mobile ?? ''}}"></div>
                        <div class="col-lg-3">Current Address: <input type="text" class="form-control primary-input"
                                readonly value="{{ $staffs->current_address ?? ''}}" readonly name="staff_address">
                        </div>
                        <div class="col-lg-3">Gender: <input type="text" class="form-control primary-input" readonly
                                value="{{ $staffs->gender_id ?? ''}}" name="staff_gender"></div>
                        <div class="col-lg-3">Date Of Birth: <input type="text" class="form-control primary-input"
                                readonly value="{{ $staffs->date_of_birth ?? ''}}" name="staff_birth"></div>
                    </div>
                </div>
            </div>
            <div id="guestBoard" class="pad-top-20 hidden">
                <div class="pad-top-20">
                    <h2>Guest information</h2>
                    <div class="row">
                        <div class="col-lg-3">Guest's number: <input type="text" class="form-control primary-input"
                                readonly value="{{ $guest->id ?? ''}}"></div>
                        <div class="col-lg-3">Full name: <input name="staff_name" type="text"
                                class="form-control primary-input" readonly value="{{ $guest->name ?? ''}}"></div>
                        <div class="col-lg-3">Email: <input type="text" class="form-control primary-input" readonly
                                value="{{ $guest->email ?? ''}}"></div>
                        <div class="col-lg-3">Mobile: <input type="text" class="form-control primary-input" readonly
                                value="{{ $guest->phone ?? ''}}"></div>
                        <div class="col-lg-3">Current Address: <input type="text" class="form-control primary-input"
                                readonly value="{{ $guest->address ?? ''}}" readonly name="staff_address"></div>
                        <div class="col-lg-3">Gender: <input type="text" class="form-control primary-input" readonly
                                value="{{ $guest->gender ?? ''}}" name="staff_gender"></div>
                        <div class="col-lg-3">Date Of Birth: <input type="text" class="form-control primary-input"
                                readonly value="{{ $guest->day_of_birth ?? ''}}" name="staff_birth"></div>
                    </div>
                </div>
            </div>
            <div class="pad-top-20">
                <h2>Health status</h2>
                <div class="row">
                    <div class="col-lg-6">
                        @error('temperature')
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('temperature') }}
                        </div>
                        @enderror
                        <div>
                            <label for="">Temperature (<span>&#8451;</span>)</label>
                            <input type="text" class="form-control primary-input" name="temperature" required value="{{ !empty($patientData)? $patientData->temperaturte: '' }}">
                            <span class="focus-border"></span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        @error('respiratory')
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('respiratory') }}
                        </div>@enderror
                        <div>
                            <label for="">Respiratory</label>
                            <input type="text" class="form-control primary-input" name="respiratory" required value="{{ !empty($patientData)? $patientData->respiratory: '' }}">
                            <span class="focus-border"></span>
                        </div>
                    </div>
                </div>
                <p></p>
                <div class="row">
                    <div class="col-lg-6">
                        <div>
                            <label for="symtomsType">Symtoms Type</label>
                        </div>
                        <select name="symtomstype" id="symtomsType" class="form-control w-100 niceSelect bb">
                            @foreach ($items as $symtome)
                            <option value="{{ $symtome->id }}" <?php if( !empty($patientData)) {if($patientData->symtome_type==$symtome->id){echo 'selected';} }  ?>>{{ $symtome->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label for="symtonDes">Symtoms Describe</label>
                        <textarea name="symtondes" id="symtonDes" cols="30" rows="10" class="form-control"
                            required>{{ !empty($patientData) ? $patientData->symtome_describe: '' }}</textarea>
                    </div>
                </div>
                
                <div class="pad-top-20">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" value="1" id="oldPatient"
                                    name="oldpatient" {{ !empty($patientData) && $patientData->old_patient == 1 ? 'checked': ''}}>
                                <label for="oldPatient" class="form-check-label">Old Patient</label>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-check">
                                <input type="checkbox" value="1" id="Comsultant" class="form-check-input"
                                    name="consultant" {{ !empty($patientData) && $patientData->consultant == 1 ? 'checked': ''}}>
                                <label for="Comsultant" class="form-check-label">Consultant Doctor</label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div>
                    <div class=" row">
                        <div class="col-md-6 col-sm-12">
                            <div class="input-effect sm2_mb_20 md_mb_20">
                                <label class="mt-20">Diagnostic</label>
                                <textarea required class="primary-input form-control" cols="0" rows="4"
                                    name="diagnostic">{{!empty($editData)? $editData->diagnostic: ''}}</textarea>

                                <span class="focus-border textarea"></span>

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-effect sm2_mb_20 md_mb_20">
                                <input type="checkbox" id="checkHospital" onclick="checkStranfer()">
                                <label for="checkHospital" class="mt-20">Tranfer to Hospital: </label>
                                <div id="hospital" class="hidden">
                                    <textarea class="primary-input form-control" cols="0" rows="4" id="hospital"
                                    name="hospital" placeholder="Hospital name and address">{{ !empty($patientData) ? $patientData->hospital: ''}}</textarea>
                                    <span class="focus-border textarea"></span>
                                </div>
                                
                            </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class=" row pt-30">
                        <div class="col-md-6 col-sm-12">
                            <div class="input-effect sm2_mb_20 md_mb_20">
                                <input required class="primary-input form-control" type="text" name="medicine_id"
                                    autocomplete="off" value="{{!empty($editData)? $editData->id_medicine: ''}}">
                                <label>Medicine ID</label>
                                <span class="focus-border"></span>

                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="input-effect sm2_mb_20 md_mb_20">
                                <input required class="primary-input form-control" type="text" name="origin" autocomplete="off"
                                    value="{{!empty($editData)? $editData->origin: ''}}">
                                <label>Origin</label>
                                <span class="focus-border"></span>

                            </div>
                        </div>
                    </div>

                    <div class=" row pt-30">
                        
                        <div class="col-md-6 col-sm-12">
                            <div class="input-effect sm2_mb_20 md_mb_20">
                                <input required class="primary-input form-control" type="text" name="medicine_name"
                                    autocomplete="off" value="{{!empty($editData)? $editData->medicine_name: ''}}">
                                <label>Medicine name</label>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="input-effect sm2_mb_20 md_mb_20">
                                <input required class="primary-input form-control" type="text" name="amount" autocomplete="off"
                                    value="{{!empty($editData)? $editData->amount: ''}}">
                                <label>Quantity</label>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </div>

                    <div class=" pt-30">
                        <div class="input-effect sm2_mb_20 md_mb_20">
                            <textarea required class="primary-input form-control" name="using"  autocomplete="off" id="" cols="30" rows="10">{{!empty($editData)? $editData->using: ''}}</textarea>
                            
                            <label>Using</label>
                            <span class="focus-border"></span>
                        </div>
                    </div>

                    <div class="row mt-30">
                        @php
                        $tooltip = "";
                        if(in_array(300, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                        $tooltip = "";
                        }else{
                        $tooltip = "You have no permission to add";
                        }
                        @endphp
                        <div class="col-lg-12 text-center mt-30">
                            <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">
                                <span class="ti-check"></span>
                                @if(isset($patientDatabl))
                                @lang('lang.update')
                                @else
                                @lang('lang.save')
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


@endsection
@section('script')
<script>
    if (document.getElementById("type").value == 1) {
        document.getElementById("studentBoard").removeAttribute("class", "hidden");
        document.getElementById("teacherBoard").setAttribute("class", "hidden");
        document.getElementById("staffBoard").setAttribute("class", "hidden");
        document.getElementById("guestBoard").setAttribute("class", "hidden");
    } else if (document.getElementById("type").value == 2) {
        document.getElementById("studentBoard").setAttribute("class", "hidden");
        document.getElementById("teacherBoard").removeAttribute("class", "hidden");
        document.getElementById("staffBoard").setAttribute("class", "hidden");
        document.getElementById("guestBoard").setAttribute("class", "hidden");
    } else if (document.getElementById("type").value == 3) {
        document.getElementById("studentBoard").setAttribute("class", "hidden");
        document.getElementById("teacherBoard").setAttribute("class", "hidden");
        document.getElementById("staffBoard").removeAttribute("class", "hidden;")
        document.getElementById("guestBoard").setAttribute("class", "hidden");
    } else if (document.getElementById("type").value == 4) {
        document.getElementById("studentBoard").setAttribute("class", "hidden");
        document.getElementById("teacherBoard").setAttribute("class", "hidden");
        document.getElementById("staffBoard").setAttribute("class", "hidden")
        document.getElementById("guestBoard").removeAttribute("class", "hidden")
    }
</script>
<script>
    function checkStranfer() {
        if (document.getElementById('checkHospital').checked) {
            document.getElementById("hospital").removeAttribute("class", "hidden");            
        }
        else
        document.getElementById("hospital").setAttribute("class", "hidden");            
    }
</script>
<script>
    function ImgPrview() {
        var src = URL.createObjectURL(event.target.files[0]);
        var preview = document.getElementById("img-preview");
        preview.src = src;
    }
</script>
@endsection