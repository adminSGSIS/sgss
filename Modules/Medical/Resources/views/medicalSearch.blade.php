@extends('backEnd.master')
@section('mainContent')
<style>
    .pad-top-20 {
        padding-top: 20px;
    }
    .pad-bot-20{
        padding-bottom: 20px;
    }
    .hidden {
        display: none;
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #05B7FF !important;
}
</style>
<div class="row pad-bot-20">
    <div class="col-2">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link active" id="v-pills-current-tab" data-toggle="pill" href="#v-pills-current" role="tab" aria-selected="true">Current in school</a>
        <a class="nav-link" id="v-pills-passerby-tab" data-toggle="pill" href="#v-pills-passerby" role="tab" aria-selected="false">Visitor</a>
        </div>
    </div>
    <div class="col-10">
      <div class="tab-content" id="v-pills-tabContent">
        <div class="tab-pane  fade show active" id="v-pills-current" role="tabpanel">
            <form method="Get" action="{{ route('ShowMedical') }}">
                @csrf
                <div class="white-box">
                    <h2 class="center" style="text-align: center;">Create Medical</h2>
                    <div class="container">
                        <div class="pad-top-20">
                            <h1>Patient Type</h1>
                            <div class="row">
                                <div class="col-lg-4 input-effect">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="1" name="patientType" id="studentRadio"
                                            onclick="checkType()">
                                        <label class="form-check-label" for="studentRadio">
                                            Student
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4 input-effect">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="2" name="patientType" id="teacherRadio"
                                            onclick="checkType()">
                                        <label class="form-check-label" for="teacherRadio">
                                            Teacher
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4 input-effect">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="3" name="patientType" id="staffRadio"
                                            onclick="checkType()">
                                        <label class="form-check-label" for="staffRadio">
                                            Employeer
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="studentBoard" class="hidden">
                            <div class="pad-top-20">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <select name="student_id" id="student_list"
                                            class="niceSelect w-100 bb form-control{{ @$errors->has('students') ? ' is-invalid' : '' }}">
                                            <option data-display="@lang('student name') *" value="">@lang('student name') *</option>
                                            @foreach($students as $student)
                                            @if(isset($add_income))
                                            <option value="{{@$student->id}}"
                                                {{@$add_income->student_id == @$student->id? 'selected': ''}}>
                                                {{@$student->full_name}} | {{@$student->admission_no}}</option>
                                            @else
                                            <option label="{{@$student->admission_no}}" value="{{@$student->id}}"
                                                {{old('student_list') == @$student->id? 'selected' : ''}}>{{@$student->full_name}} |
                                                {{@$student->admission_no}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                </div>
                            </div>
            
                        </div>
                        <div id="teacherBoard" class="pad-top-20 hidden">
                            <div class="pad-top-20">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                    <div class="col-lg-6 col-md-6 sm-12">
                                        <select  id="student_list" name="teacher_id"
                                            class="niceSelect w-100 bb form-control{{ @$errors->has('staffs') ? ' is-invalid' : '' }}">
                                            <option data-display="@lang('staff name') *" value="">@lang('staff name') *</option>
                                            @foreach($teachers as $teacher)
                                            <option value="{{ $teacher->id }}">{{ $teacher->full_name }} | {{ $teacher->staff_no }}</option> @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="staffBoard" class="pad-top-20 hidden">
                            <div class="pad-top-20">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                    <div class="col-lg-6 col-md-6 sm-12">
                                        <select  id="student_list" name="staff_id"
                                            class="niceSelect w-100 bb form-control{{ @$errors->has('staffs') ? ' is-invalid' : '' }}">
                                            <option data-display="@lang('staff name') *" value="">@lang('staff name') *</option>
                                            @foreach($staffs as $staff)
                                            <option value="{{ $staff->id }}">{{ $staff->full_name }} | {{ $staff->staff_no }}</option> @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12"></div>
                        </div>
                        <div class="d-flex justify-content-end" style="padding-top: 35px">
                            <input type="submit" class="primary-btn small fix-gr-bg" value="search">
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="tab-pane fade" id="v-pills-passerby" role="tabpanel">
            <form action="{{ route('newGuest') }}">
                <div class="white-box">
                    <h2 class="center" style="text-align: center;">New Guest</h2>
                    <div class="container">
                        <div class="pad-top-20">
                            <h1>Guest Information</h1>                        
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputName">Name</label>
                                    <input type="text" class="form-control primary-input" id="inputName" name="name" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputEmail">Email (Parent's email)</label>
                                    <input type="text" class="form-control primary-input" id="inputEmail" name="email" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputPhone">Phone (Parent's phone)</label>
                                    <input type="text" class="form-control primary-input" id="inputPhone" name="phone" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputName">Height</label>
                                    <input type="text" class="form-control primary-input" id="inputHeight" name="height" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputEmail">Weight</label>
                                    <input type="text" class="form-control primary-input" id="inputWeight" name="weight" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputBlood">Blood Group</label>
                                    <select name="bloodgroup" id="inputBlood" class="custom-select primary-input">
                                        @foreach ($blood_groups as $item)
                                        <option value="{{ $item->id }}">{{ $item->base_setup_name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputAddress">Address</label>
                                    <input type="text" class="form-control primary-input" id="inputAddress" name="address" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputGender">Gender</label>
                                    <select name="gender" id="inputGender" class="custom-select primary-input">
                                        <option value="" disabled selected hidden>Select</option>
                                        @foreach ($gender as $item)
                                            <option value="{{ $item->id }}">{{ $item->base_setup_name }}</option>
                                        @endforeach
                                    </select>                                    
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputBirth">Day of birth</label>
                                    <input type="text" class="form-control primary-input date" id="inputBirth" name="day_of_birth">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <input type="text" name="patientType" value="4" hidden>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end" style="padding-top: 35px">
                            <input type="submit" class="primary-btn small fix-gr-bg" value="continue">
                        </div>
                    </div>
                </div>
            </form>
            
        </div>
      </div>
    </div>
  </div>
  <p></p>
  <section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3>@lang('Patient List')</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                        <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                            <tr>
                                <td colspan="7">
                                    @if(session()->has('message-success-delete'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message-success-delete') }}
                                    </div>
                                    @elseif(session()->has('message-danger-delete'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('message-danger-delete') }}
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><a href="{{ url('/medical/patient-PDF') }}" class="primary-btn small fix-gr-bg">
                                    @lang('Excel')
                                    </a></th>
                            </tr>
                            <tr>
                                <th>@lang('No')</th>
                                <th>@lang('Patient')</th>
                                <th>@lang('Symtoms Type')</th>
                                <th>@lang('Date')</th>
                                <th>@lang('Old Patitent')</th>
                                <th>@lang('action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($patients as $patient)
                            <tr>
                                <td>{{ $patient->id }}</td>
                                <td>{{ $patient->patient_name }}</td>
                                <td>{{ $patient->symtomeRelation->name }}</td>
                                <td>{{ $patient->date }}</td>
                                <td>
                                    @if(( $patient->old_patient )==1)
                                    Yes
                                    @else
                                    No
                                    @endif
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            @lang('lang.select')
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a title="View/Edit Leave Details"
                                                class="dropdown-item" href="{{ url('medical/patient-edit/'.$patient->id.'/'.$patient->patient_type.'/'.$patient->patient_id) }}">@lang('lang.edit')</a>
                                                <a title="Export PDF file" target="_blank"
                                                class="dropdown-item" href="{{ url('medical/patient-PDF/'.$patient->id) }}">@lang('Export PDF')</a>
                                            <a class="dropdown-item" data-toggle="modal"
                                                data-target="#DeletePatient{{$patient->id}}"
                                                href="#">@lang('lang.delete')</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade admin-query" id="DeletePatient{{$patient->id}}">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">@lang('lang.delete') @lang('lang.item')</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <h4>@lang('lang.are_you_sure_to_delete')</h4>
                                            </div>

                                            <div class="mt-40 d-flex justify-content-between">
                                                <button type="button" class="primary-btn tr-bg"
                                                    data-dismiss="modal">@lang('lang.cancel')</button>
                                                {{ Form::open(['url' => 'medical/patient-delete/'.$patient->id, 'method' => 'DELETE', 'enctype' => 'multipart/form-data']) }}
                                                <button class="primary-btn fix-gr-bg"
                                                    type="submit">@lang('lang.delete')</button>
                                                {{ Form::close() }}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
  
@endsection

@section('script')
<script>
    function checkType() {
        if (document.getElementById('studentRadio').checked) {
            document.getElementById("studentBoard").removeAttribute("class", "hidden");
            document.getElementById("teacherBoard").setAttribute("class", "hidden");
            document.getElementById("staffBoard").setAttribute("class", "hidden")
        } else if (document.getElementById('teacherRadio').checked) {
            document.getElementById("studentBoard").setAttribute("class", "hidden");
            document.getElementById("teacherBoard").removeAttribute("class", "hidden");
            document.getElementById("staffBoard").setAttribute("class", "hidden")
        } else if (document.getElementById('staffRadio').checked) {
            document.getElementById("studentBoard").setAttribute("class", "hidden");
            document.getElementById("teacherBoard").setAttribute("class", "hidden");
            document.getElementById("staffBoard").removeAttribute("class", "hidden")
        }
    }
</script>
<script type="text/javascript">
    $('#student_list').on('change', function () {
        var id = $('#student_list option:selected').attr('label');
        $('#admission_no').val(id);
    });
</script>
@endsection