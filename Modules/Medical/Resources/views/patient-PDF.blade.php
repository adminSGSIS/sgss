<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    <style>
        body {
            height: 100%;
            width: 100%;
            font-family: 'DejaVu Sans';
        }
    
        .letter_color {
            color: #424040d2;
        }
        .ft200 {
            font-size: 200% !important;
        }
        div>h1 {}
    
        .footer {
            bottom: 0;
            position: fixed;
            width: 100%;
        }
    
        .info {
            font-size: 170% !important;
        }
    
        .p-2>h1 {
            font-size: 100px;
        }
    
        table {
            border-collapse: collapse;
            width: 100%;
        }
        .student-table td,th{
            width: 100%;
  border-collapse: collapse;
  border: 1px solid white!important;
  text-align: center;
        }
        td,
        th {
            border: 1px solid #31b6f0;
            border-top: 1px #31b6f0 solid!important;
            text-align: left;
            padding: 8px 8px 8px 8px !important;
            font-size: 18px;
            vertical-align: top;/* 
            color: #0D52AD; */
        }
        .c-black{
            color: #000!important;
        }
        @page {
            margin-bottom: 0;
            margin-top: 0;
        }
    
        .bold {
            font-weight: bold;
        }
    
        .ft300 {
            font-size: 18px;
            color: rgba(66, 64, 64, 0.822);
        }
    
        .fz25 {
            font-size: 22px;
            line-height: 1.3;
        }
        .w-50{
            width: 50%;
        }
    </style>
</head>
<body>
    <img src="{{ asset('public/header_letter.png') }}" alt="" width="100%">
    <div style="text-align:center">
        <h1>Medical</h1>
        <table class="student-table">
            <tbody>
                <td colspan="2" class="w-50"><h4>Name: {{ $patient->patient_name }}</h4></td>
                <td colspan="2" class="w-50"><h4>Date of birth: {{ $patient->date_of_birth }}</h4></td>
            </tbody>
        </table>
        
    </div>
    <div>
        <h3>Basic Information</h3>
        <table>
            <tbody>
                <tr>
                    <td class="w-50" colspan="2">Date create: {{ $patient->case }}</td>
                    <td class="w-50" colspan="2">Case: {{ $patient->date }}</td>
                </tr>
                <tr>
                    <td class="w-50" colspan="2">Gender: {{ $patient->gender==1 ? 'Male' : $patient->gender==2 ? 'Female' :'Other' }}</td>
                    <td class="w-50" colspan="2">Address: {{ $patient->address }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <h3>Patient Checked</h3>
        <table>
            <tbody>
                <tr>
                    <td class="w-50" colspan="2">Symtome type: {{ $patient->symtomeRelation->name }}</td>
                    <td class="w-50" colspan="2">Symtome Describle: {{ $patient->symtome_describe }}</td>
                </tr>
                <tr>
                    <td class="w-50" colspan="2">Old patient: {{ $patient->old_patient==1 ? 'Yes':'No' }}</td>
                    <td class="w-50" colspan="2">Consultant: {{ $patient->consultant==1 ? 'Yes' : 'No' }}</td>
                </tr>
                <tr>
                    <td class="w-50" colspan="2">Hospital: {{ $patient->hospital!=null ? $patient->hospital : 'No need to go to the hospital ' }}</td>
                    <td class="w-50" colspan="2">Patient type: {{ $patient->patient_type==1 ? 'Student' : $patient->patient_type==2 ? 'Teacher' : 'Staff'}}</td>
                </tr>
            </tbody>
        </table>
        <h3>Prescription</h3>        
        <table>
            <tbody>
                <tr>
                    <td colspan="4">Diagnostic: {{ $prescription->id_medicine }}</td>
                </tr>
                <tr>
                    <td class="w-50" colspan="2">Medicine id: {{ $prescription->id_medicine }}</td>
                    <td class="w-50" colspan="2">Medicine name: {{ $prescription->medicine_name }}</td>
                </tr>
                <tr>
                    <td class="w-50" colspan="2">Amount: {{ $prescription->amount}}</td>
                    <td class="w-50" colspan="2">Origin: {{ $prescription->origin}}</td>
                </tr>     
                <tr>
                    <td colspan="4">Using: {{ $prescription->using }}</td>
                </tr>           
            </tbody>
        </table>        
    </div>
    <img src="{{ asset('public/footer_letter.png') }}" alt="" style="bottom: 150px;position: fixed;width: 100%;"> 
</body>