@extends('backEnd.master')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/css')}}/purchase-proposal.css">
@endsection
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Purchase Proposal</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        @if(isset($editData))
        @if(in_array(321, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
           
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('purchase-proposal')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')
                </a>
            </div>
        </div>
        @endif
        @endif
      <div class="row">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($editData))
                                    @lang('lang.edit')
                                @else
                                    @lang('lang.add')
                                @endif
                                Purchase Proposal
                            </h3>
                        </div>
                        @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'medical/purchase-proposal-update/'.$editData->id , 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @else
                        @if(in_array(321, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
           
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'medical/purchase-proposal-store',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @endif
                        @endif
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                   
                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect" id="getabc">
                                            <select class="niceSelect w-100 bb form-control" name="medicine_type" id="medicine_type" >
                                                <option data-display="Medicine Type *" value="">Medicine Type *</option>
                                                @foreach($medicine_types as $value)
                                                <option  value="{{$value->id}}"
                                                @if(isset($editData))
                                                    @if($editData->medicine_type == $value->id)
                                                        @lang('lang.selected')
                                                    @endif
                                                @endif
                                                >{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                            
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <select class="custom-select primary-input" name="medicine_name" id="modal">
                                                <option value="">Select Medicine*</option>  
                                                
                                            </select>
                                            <span class="focus-border"></span>
                                           
                                        </div>
                                    </div>
                                   
                                    <div class="col-lg-12 mb-20">
                                        <div>
                                            <label for="date">Date</label>
                                            <input type="text" value="{{isset($editData)? $editData->date : '' }}" class="form-control primary-input date"autocomplete="off" id="date" name="date" required>
                                            <span class="focus-border"></span>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect" id="getabc">
                                            <select class="niceSelect w-100 bb form-control" name="staff_name" id="category_name" >
                                                <option data-display="staff name *" value="">Staff Name*</option>
                                                @foreach($staffs  as $key=>$value)
                                                <option  value="{{$value->full_name}}"
                                                @if(isset($editData))
                                                @if($editData->staff_name == $value->full_name)
                                                    @lang('lang.selected')
                                                @endif
                                                @endif
                                                >{{$value->full_name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('category_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong>{{ $errors->first('category_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect" >
                                            <select class="niceSelect w-100 bb form-control{{ $errors->has('supplier') ? ' is-invalid' : '' }}" name="supplier" id="supplier" >
                                                <option data-display="select supplier *" value="">select supplier *</option>
                                                @foreach($supplier as $value)
                                                <option  value="{{isset($editData)? $editData->supplier : $value->company_name }}"
                                                @if(isset($editData))
                                                @if($editData->supplier == $value->company_name)
                                                    @lang('lang.selected')
                                                @endif
                                                @endif
                                                >{{$value->company_name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                           
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <input class="primary-input form-control"
                                            type="number"  name="quantity" autocomplete="off" value="{{isset($editData)? $editData->quantity : '' }}" required>
                                            <label>quantity<span></span> </label>
                                            <span class="focus-border textarea"></span>
        
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control" cols="0" rows="4" name="description" id="description">{{isset($editData) ? $editData->description : ''}}</textarea>
                                            <label>@lang('lang.description') <span></span> </label>
                                            <span class="focus-border textarea"></span>

                                        </div>
                                    </div>
                             </div>
                  				@php 
                                  $tooltip = "";
                                  if(in_array(321, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                                        $tooltip = "";
                                    }else{
                                        $tooltip = "You have no permission to add";
                                    }
                                @endphp
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                       <button type="submit" class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">

                                            <span class="ti-check"></span>
                                            @if(isset($editData))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.save')
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
               
          <div class="row">
            <div class="col-lg-4 no-gutters">
                <div class="main-title">
                    <h3 class="mb-0">Purchase Proposal List</h3>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">
                <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                    <thead>
                        @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="6">
                                         @if(session()->has('message-success-delete'))
                                          <div class="alert alert-success">
                                              {{ session()->get('message-success-delete') }}
                                          </div>
                                        @elseif(session()->has('message-danger-delete'))
                                          <div class="alert alert-danger">
                                              {{ session()->get('message-danger-delete') }}
                                          </div>
                                        @endif
                                    </td>
                                </tr>
                             @endif
                        <tr>
                            <th>@lang('lang.staff') @lang('lang.name')</th>
                            <th>@lang('lang.date') </th>
                            <th>Medicine Type</th>
                            <th>Medicine Name</th>
                            <th>Supplier</th>
                            <th>Quantity</th>
                            <th>Description</th>
                            <th>@lang('lang.action')</th>
                            
                        </tr>
                    </thead>

                    <tbody>
                        @if(isset($medicine_proposal))
                        @foreach($medicine_proposal as $value)
                        <tr>
                            <td>{{$value->staff_name}}</td>
                            <td>{{$value->date}}</td>
                            <td >{{$value->medicine_type}}</td>
                            <td >{{$value->medicine_name}}</td>
                            <td>{{$value->supplier}}</td>
                            <td>{{$value->quantity}}</td>
                            <td>{{$value->description}}</td>
                            <td>
                                <div class="dropdown">
                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                        @lang('lang.select')
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                       
                                        @if(in_array(322, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            <a class="dropdown-item" href="{{url('medical/edit-purchase-proposal/'.$value->id)}}">@lang('lang.edit')</a>
                                        @endif
                                        @if(in_array(323, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Item" href="{{url('medical/delete-purchase-proposal/'.$value->id)}}">@lang('lang.delete')</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                           
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $("#medicine_type").change(function(event){
            medicinetypeID =$("#medicine_type").val();
            console.log(medicinetypeID);
            $.ajax({
                    url: "/medical/medicine-type-check", 
                    type:'POST',
                    data: {"_token": "{{ csrf_token() }}",id:medicinetypeID},
                    success: function(result){
                        console.log ($("#modal").html(result));
                        //dữ liệu trả về từ controller
                        $("#modal").html(result);
                    }
            });    
    });
    });
</script>
@endsection