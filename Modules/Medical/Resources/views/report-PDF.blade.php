<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    <style>
        body {
            height: 100%;
            width: 100%;
            font-family: 'DejaVu Sans';
            font-size: 10px;
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            font-size: 8px;
        }

        @page {
            margin: 50px 25px;
        }
        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 150px;
        }

        footer {
            position: fixed;
            bottom: 60px;
            left: 0px;
            right: 0px
        }
        .bold {
            font-weight: bold;
        }
    </style>
</head>

<body>
    <header><img src="{{ asset('public/header_letter.png') }}" alt="" style="width: 100%;"></header>
    <footer><img src="{{ asset('public/footer_letter.png') }}" alt="" style="width: 100%;"></footer>
    <main style="padding-top: 60px">
        <div style="page-break-inside: auto">

            <div style="text-align: center;">
                <h1>REPORT MONTHLY SGSI</h1>
            </div>
            <div class="container">
                <div style="text-align: justify">
                    Dear Saigon Star International School Board of Directors. <br><br>
                    My name is {{ Auth::user()->full_name }}, I represent the school's health department to write a
                    monthly report on the medical
                    situation and drug management.
                </div>
                <br>
            </div>
            Report <br>
            <h3>1. Examination results</h3>
            <table class="student-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th colspan="2">Date</th>
                        <th>Symtome</th>
                        <th>Diagnostic</th>
                        <th colspan="3">Handle at schoo</th>
                        <th>Transfer to</th>
                        <th colspan="3">Result</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $number=1;
                    @endphp
                    @foreach ($patients as $patient)
                    <tr>
                        <td>{{ $number++ }}</td>
                        <td>{{ $patient->patient_name ?? ''}}</td>
                        <td colspan="2">{{ date('d/m/Y', strtotime($patient->date)) ?? ''}}</td>
                        <td>{{ $patient->symtomeRelation->name ?? ''}}</td>
                        <td>{{ $patient->prescriptionsRelation->diagnostic ?? ''}}</td>
                        <td colspan="3"> Medicine name:
                            {{ $patient->prescriptionsRelation->medicine_name ?? ''}}<br>Medicine
                            using:{{ $patient->prescriptionsRelation->using ?? ''}} </td>
                        <td>{{ $patient->hospital ?? ''}}</td>
                        <td colspan="3">Medicine name:
                            {{ $patient->prescriptionsRelation->medicine_name ?? ''}}<br>Medicine
                            using:{{ $patient->prescriptionsRelation->using}} <br> {{ $patient->hospital ?? ''}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <br>
            Total patient: <b>{{ $number-1 }}</b>
            <h3>2. Drug Report</h3>
            <h4>Drug import</h4>
            <table class="student-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Medicine name</th>
                        <th>Medicine code</th>
                        <th>Amount</th>
                        <th>Staff name</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $number_1=1;
                    @endphp
                    @foreach ($importMedicines as $importMedicine)
                    <tr>
                        <td>{{ $number_1++ }}</td>
                        <td>{{ $importMedicine->medicineRelation->medicine_name ?? ''}}</td>
                        <td>{{ $importMedicine->medicineRelation->medicine_code ?? ''}}</td>
                        <td>{{ $importMedicine->amount ?? ''}}</td>
                        <td>{{ $importMedicine->staffRelation->full_name ?? ''}}</td>
                        <td>{{ date('d/m/Y', strtotime($importMedicine->created_at)) ?? ''}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <h4>Drug supply</h4>
            <table class="student-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Staff name</th>
                        <th>Date</th>
                        <th>Medicine name</th>
                        <th>Medicine type</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $number_2=1;
                    @endphp
                    @foreach ($supplyMedicines as $supplyMedicine)
                    <tr>
                        <td>{{ $number_2++ }}</td>
                        <td>{{ $supplyMedicine->staff_name ?? ''}}</td>
                        <td>{{ $supplyMedicine->date ?? ''}}</td>
                        <td>{{ $supplyMedicine->medicine_name ?? ''}}</td>
                        <td>{{ $supplyMedicine->medicine_type ?? ''}}</td>
                        <td>{{ $supplyMedicine->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div style="page-break-before: always">
                <div style="padding-top: 70px">
                    <h4>Drug in stogare</h4>
                <table class="student-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Medicine type</th>
                            <th>Medicine code</th>
                            <th>Medicine name</th>
                            <th>unit</th>
                            <th>Price</th>
                            <th>Manufacturing date</th>
                            <th>Expiry_date</th>
                            <th>producer</th>
                            <th>Stock</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $number_3=1;
                        @endphp
                        @foreach ($medicines as $medicine)
                        <tr>
                            <td>{{ $number_3++ }}</td>
                            <td>{{ $medicine->medicine_type ?? ''}}</td>
                            <td>{{ $medicine->medicine_code ?? ''}}</td>
                            <td>{{ $medicine->medicine_name ?? ''}}</td>
                            <td>{{ $medicine->stock ?? ''}}</td>
                            <td>{{ $medicine->unit }}</td>
                            <td>{{ $medicine->price ?? ''}}</td>
                            <td>{{ $medicine->manufacturing_date ?? ''}}</td>
                            <td>{{ $medicine->manufacturing_date ?? ''}}</td>
                            <td>{{ $medicine->expiry_date }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                
            </div>
    </main>
    </div>
</body>