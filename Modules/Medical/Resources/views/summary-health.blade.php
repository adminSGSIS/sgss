@extends('backEnd.master')
@section('mainContent')
<div class="mb-3">
    <div class="row mb-3">
        <div class="col-lg-4">
            <h3>Creator</h3>
            <div class="white-box">
                Full Name: {{ Auth::user()->full_name }} <br><br>
                Email: {{ Auth::user()->email }} <br><br>
                Phone: {{ Auth::user()->staff->mobile }}
            </div>
        </div>
        <div class="col-lg-8">
            <h3>Student Name</h3>
            <div class="white-box">
                <div class="row mb-3">
                    <div class="col-lg-6">Full Name: {{ $student->full_name }}</div>
                    <div class="col-lg-6">Age: {{ $student->age }}</div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Height: {{ $student->height }}</div>
                    <div class="col-lg-6">Weight: {{ $student->weight }}</div>
                </div>
                <div class="row">
                    <div class="col-lg-6">Class: {{ $student->className->class_name }}</div>
                    <input type="text" value="{{ $student->class_id }}" hidden>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="white-box mb-3">
        <div class="row mb-3">
            <div class="col-lg-12">
                <div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <input type="text" name="date" class="form-control primary-input"
                                    value="{{ !empty($patientData)? $patientData->date: '' }}"" readonly>
                            </div>                            
                                    <div>
                                        <h4>initial guess</h4>
                                        <div>
                                            {{ !empty($patientData)? $patientData->prescriptionsRelation->diagnostic : '' }}
                                        </div>
                                    </div>
                            </div>
                            <div class=" col-lg-6">
                                <div class="mb-4">
                                    <h2>Handling</h2>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div>
                                            <h4>Handle at school</h4>
                                            <div>
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td>Drug name:</td>
                                                            <td>{{ !empty($patientData)? $patientData->prescriptionsRelation->medicine_name : '' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Using:</td>
                                                            <td>{{ !empty($patientData)? $patientData->prescriptionsRelation->using : '' }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Amount:</td>
                                                            <td>{{ !empty($patientData)? $patientData->prescriptionsRelation->amount : '' }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <h4>Send to hospital</h4>
                                        <div>
                                            {{ !empty($patientData) && $patientData->hospital!=null ? $patientData->hospital : 'No need to go to the hospital' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div>
                        <h4>Result</h4>
                        <div>
                            <div>
                                <table class="table ">
                                    <tbody>
                                        <tr>
                                            <td>Drug name:</td>
                                            <td>{{ !empty($patientData)? $patientData->prescriptionsRelation->medicine_name : '' }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Using:</td>
                                            <td>{{ !empty($patientData)? $patientData->prescriptionsRelation->using : '' }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Amount:</td>
                                            <td>{{ !empty($patientData)? $patientData->prescriptionsRelation->amount : '' }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Hospital</td>
                                            <td>{{ !empty($patientData) && $patientData->hospital!=null ? $patientData->hospital : 'No need to go to the hospital' }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection