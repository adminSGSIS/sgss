@extends('backEnd.master')
@section('mainContent')
<style>
    .pad-top-20 {
        padding-top: 20px;
    }
    .pad-bot-20{
        padding-bottom: 20px;
    }
    .hidden {
        display: none;
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #05B7FF !important;
}
</style>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Supply Medicine To Patient</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
       
        <div class="col-12">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane  fade show active" id="v-pills-current" role="tabpanel">
                    
                        @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'medical/medicine-to-patient-update/'.$editData->id , 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @else
                        @if(in_array(321, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
           
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'medical/medicine-to-patient-Store',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @endif
                        @endif
                        @csrf
                        <div class="white-box">
                            <h2 class="center" style="text-align: center;">Supply Medicine To Patient</h2>
                            <div class="container">
                                <div class="row mt-30">
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="staff">Staff Name</label>
                                            <select name="staff_name" id="staff" class="custom-select primary-input">
                                            <option data-display="staff name *" value="">Staff Name*</option>
                                                @foreach($staffs  as $key=>$value)
                                                <option  value="{{$value->full_name}}"
                                                @if(isset($editData))
                                                    @if($editData->staff_name == $value->full_name)
                                                        @lang('lang.selected')
                                                    @endif
                                                    @endif
                                                >{{$value->full_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="medicine_type">Type of Medicine</label>
                                            <select name="medicine_type" id="medicine_type" class="custom-select primary-input" required>
                                                <option value="">Select Type of Medicine</option>
                                                @foreach( $medicine_types as $value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="date">Date</label>
                                            <input type="text" value="{{isset($editData)? $editData->date : '' }}" class="form-control primary-input date"autocomplete="off" id="date" name="date" required>
                                            <span class="focus-border"></span>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-10">
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="modal">Name of Medicine</label>
                                            <select name="medicine_name" id="modal" class="custom-select primary-input">
                                                <option value="">Select Medicine</option>   
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="amount">Quantity</label>
                                            <input class="primary-input form-control"
                                            type="number" id="amount" name="amount" autocomplete="off" value="{{isset($editData)? $editData->amount : '' }}" required>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class=" mt-10">
                                        
                                    <div class="pad-top-20">
                                        <h1>Patient Name</h1>
                                        <div class="row">
                                            <div class="col-lg-4 input-effect">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="1" name="staffType" id="studentRadio"
                                                        onclick="checkType()">
                                                    <label class="form-check-label" for="studentRadio">
                                                        Student
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 input-effect">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="2" name="staffType" id="teacherRadio"
                                                        onclick="checkType()">
                                                    <label class="form-check-label" for="teacherRadio">
                                                        Teacher
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 input-effect">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="3" name="staffType" id="staffRadio"
                                                        onclick="checkType()">
                                                    <label class="form-check-label" for="staffRadio">
                                                        Employeer
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="studentBoard" class="hidden">
                                        <div class="pad-top-20">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <select name="patient_student" id="student_list"
                                                        class="niceSelect w-100 bb form-control{{ @$errors->has('students') ? ' is-invalid' : '' }}">
                                                        <option data-display="@lang('student name') *" value="">@lang('student name') *</option>
                                                        @foreach($students as $student)
                                                        @if(isset($add_income))
                                                        <option value="{{@$student->full_name}}"
                                                            {{@$add_income->student_id == @$student->id? 'selected': ''}}>
                                                            {{@$student->full_name}} | {{@$student->class->class_name}}</option>
                                                        @else
                                                        <option label="{{@$student->admission_no}}" value="{{@$student->full_name}}"
                                                            {{old('student_list') == @$student->id? 'selected' : ''}}>{{@$student->full_name}} |
                                                            {{@$student->class->class_name}}</option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                            </div>
                                        </div>
                        
                                    </div>
                                    <div id="teacherBoard" class="pad-top-20 hidden">
                                        <div class="pad-top-20">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                                <div class="col-lg-6 col-md-6 sm-12">
                                                    <select  {{-- id="student_list" --}} name="patient_teacher"
                                                        class="niceSelect w-100 bb form-control{{ @$errors->has('staffs') ? ' is-invalid' : '' }}">
                                                        <option data-display="@lang('staff name') *" value="">@lang('staff name') *</option>
                                                        @foreach($teachers as $teacher)
                                                        <option value="{{ $teacher->full_name }}">{{ $teacher->full_name }} | {{ $teacher->staff_no }}</option> @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="staffBoard" class="pad-top-20 hidden">
                                        <div class="pad-top-20">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                                <div class="col-lg-6 col-md-6 sm-12">
                                                    <select  {{-- id="student_list" --}} name="patient_staff"
                                                        class="niceSelect w-100 bb form-control{{ @$errors->has('staffs') ? ' is-invalid' : '' }}">
                                                        <option data-display="@lang('staff name') *" value="">@lang('staff name') *</option>
                                                        @foreach($staffs as $staff)
                                                        <option value="{{ $staff->full_name}}">{{ $staff->full_name }} | {{ $staff->staff_no }}</option> @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                       <button  class="primary-btn fix-gr-bg"  >

                                            <span class="ti-check"></span>
                                            @if(isset($editData))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.save')
                                            @endif
                                        </button>
                                    </div>
                                </div> 
                            </div>
                                  
                        </div>
                    </form>
                </div>

            </div>
        </div>
     
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor mt-30">
    <div class="container-fluid p-0">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3>@lang('Supply Medicine List')</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                        <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                            <tr>
                                <td colspan="7">
                                    @if(session()->has('message-success-delete'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message-success-delete') }}
                                    </div>
                                    @elseif(session()->has('message-danger-delete'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('message-danger-delete') }}
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endif
                         
                            <tr>
                                <th>@lang('No')</th>
                                <th>@lang('Staff Name')</th>
                                <th>@lang('Patient Name')</th>
                                <th>@lang('Type of Medicine')</th>
                                <th>@lang('Name of Medicine')</th>
                                <th>@lang('Amount')</th>
                                <th>@lang('Date')</th>
                                <th>@lang('action')</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $i =1;@endphp
                        @foreach($supplyMedicine as $value)
                            <tr>     
                                <td>{{$i++}}</td>
                                <td>{{$value->staff_name}}</td>
                                <td>{{$value->patient_name}}</td>
                                <td>{{$value->medicineTypeRelation->name}}</td>
                                <td>{{$value->medicineNameRelation->medicine_name}}</td>
                                <td>{{$value->amount}}</td>
                                <td>{{$value->date}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            @lang('lang.select')
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{url('medical/edit-medicine-to-patient/'.$value->id)}}">@lang('lang.edit')</a>
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Items"
                                                href="{{url('medical/for-delete-medicine-to-patient/'.$value->id)}}">@lang('lang.delete')</a>
                                                
                                        </div>
                                    </div>
                                </td>
                            </tr>
                           
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $("#medicine_type").change(function(event){
            medicinetypeID =$("#medicine_type").val();
            console.log(medicinetypeID);
            $.ajax({
                    url: "/medical/medicine-type-check", 
                    type:'POST',
                    data: {"_token": "{{ csrf_token() }}",id:medicinetypeID},
                    success: function(result){
                        console.log ($("#modal").html(result));
                        //dữ liệu trả về từ controller
                        $("#modal").html(result);
                    }
            });    
    });
    });
</script>
<script>
    function checkType() {
        if (document.getElementById('studentRadio').checked) {
            document.getElementById("studentBoard").removeAttribute("class", "hidden");
            document.getElementById("teacherBoard").setAttribute("class", "hidden");
            document.getElementById("staffBoard").setAttribute("class", "hidden")
        } else if (document.getElementById('teacherRadio').checked) {
            document.getElementById("studentBoard").setAttribute("class", "hidden");
            document.getElementById("teacherBoard").removeAttribute("class", "hidden");
            document.getElementById("staffBoard").setAttribute("class", "hidden")
        } else if (document.getElementById('staffRadio').checked) {
            document.getElementById("studentBoard").setAttribute("class", "hidden");
            document.getElementById("teacherBoard").setAttribute("class", "hidden");
            document.getElementById("staffBoard").removeAttribute("class", "hidden")
        }
    }
</script>
<script type="text/javascript">
    $('#student_list').on('change', function () {
        var id = $('#student_list option:selected').attr('label');
        $('#admission_no').val(id);
    });
</script>

@endsection
