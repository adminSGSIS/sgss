<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('medical')->group(function() {
    Route::get('/', 'MedicalController@index')->name('MedicalSearch');
    Route::get('/show-medical', 'MedicalController@ShowMedical')->name('ShowMedical');
    Route::post('/store-medical','MedicalController@storeMedical')->name('StoreMedical');
    Route::get('/new-guest', 'MedicalController@newGuest')->name('newGuest');
    Route::get('/patient-PDF/{id}', 'MedicalController@patientPDF')->name('patientPDF');
    Route::delete('/patient-delete/{id}', 'MedicalController@destroy')->name('deletePatient');
    Route::get('/patient-edit/{id}/{type}/{id_patient}', 'MedicalController@edit')->name('editPatient');
    Route::post('/update-medical/{id}','MedicalController@update')->name('updatePatient');
    Route::get('/patient-PDF', 'MedicalController@patientExcel')->name('patientPDF');
    Route::get('healthMonitoring', 'MedicalController@healthMonitoring')->name('healthMonitoring');
    Route::get('healthMonitoring-create', 'MedicalController@healthMonitorCreate')->name('healthMonitoringCreate');
    Route::get('healthMonitoring-edit/{id}', 'MedicalController@healthMonitorEdit')->name('healthMonitoringEdit');
    Route::delete('healthMonitoring-delete/{id}', 'MedicalController@healthMonitorDelete')->name('healthMonitoringDelete');

    Route::get('healthMonitoring-store', 'MedicalController@healthMonitorStore')->name('healthMonitoringStore');
    Route::get('healthMonitoring-update/{id}', 'MedicalController@healthMonitorUpdate')->name('healthMonitoringUpdate');
    Route::get('healthMonitoring-Summary/{id}/{patient_id}', 'MedicalController@Summary')->name('healthMonitoringSummary');
    Route::get('healthMonitoring-PDF/{id}/{patient_id}', 'MedicalController@healthMonitorPDF')->name('healthMonitorPDF');
    

    Route::get('/create-prescription','SmMedicalController@prescription');
    Route::post('/save-prescription', 'SmMedicalController@prescriptionStore');
    Route::get('/for-delete-prescription/{id}', 'SmMedicalController@forDeletePrescription');
    Route::get('/delete-prescription/{id}', 'SmMedicalController@prescriptionDestroy');
    Route::get('/edit-prescription/{id}', 'SmMedicalController@prescriptionEdit');
    Route::post('/update-prescription', 'SmMedicalController@prescriptionUpdate');
    Route::get('/prescriptionDetails/{id}', 'SmMedicalController@prescriptionDetails');

    Route::get('/clinical-evaluation','SmMedicalController@addItems')->name('clinical');
    Route::post('/add-item-store', 'SmMedicalController@addItemStore');
    Route::get('/for-delete-item/{id}', 'SmMedicalController@forDeleteItem');
    Route::get('/delete-item/{id}', 'SmMedicalController@itemDestroy');
    Route::get('/edit-item/{id}', 'SmMedicalController@itemEdit');
    Route::post('/add-item-update', 'SmMedicalController@itemUpdate');

    Route::get('/medicine_type','MedicineController@index');
    Route::post('/medicine_type-store','MedicineController@store')->name('medicine_type-store');
    Route::get('/medicine_type-edit/{id}','MedicineController@edit');
    Route::post('/medicine_type-update/{id}','MedicineController@update')->name('medicine_type-update');
    Route::get('/medicine_type-delete/{id}','MedicineController@destroy');

    Route::get('/medicine','MedicineController@medicine_index')->name('medicine-index');
    Route::post('/medicine-store','MedicineController@medicine_store')->name('medicine-store');
    Route::get('/medicine-edit/{id}','MedicineController@medicine_edit')->name('medicine-edit');
    Route::post('/medicine-update/{id}','MedicineController@medicine_update')->name('medicine-update');
    Route::get('/medicine-delete/{id}','MedicineController@medicine_delete')->name('medicine-delete');

    Route::get('/medicine-management','MedicineController@medicinetManagement_index')->name('medicine-management-index');
    Route::post('/medicine-management-store','MedicineController@medicinetManagement_store')->name('medicine-management-store');
    Route::get('/medicine-management-edit/{id}','MedicineController@medicinetManagement_edit')->name('medicine-management-edit');
    Route::post('/medicine-management-update/{id}','MedicineController@medicinetManagement_update')->name('medicine-management-update');


    Route::get('/medicine-to-patient','SmMedicalController@supplyMedicine');
    Route::post('/medicine-type-check','SmMedicalController@medicineTypeCheck')->name('medicine-type-check');
    Route::post('/medicine-to-patient-Store','SmMedicalController@supplyMedicineStore')->name('medicineToPatientStore');
    Route::get('/for-delete-medicine-to-patient/{id}', 'SmMedicalController@forDeletesupplyMedicine');
    Route::get('/delete-medicine-to-patient/{id}', 'SmMedicalController@supplyMedicineDestroy');
    Route::get('/edit-medicine-to-patient/{id}', 'SmMedicalController@supplyMedicineEdit');
    Route::post('/medicine-to-patient-update/{id}','SmMedicalController@supplyMedicineUpdate');

    Route::get('clinic-report','MedicineController@report')->name('report-clinic');
    Route::get('clinic-report-pdf','MedicineController@reportPDF')->name('report-pdf');
    Route::get('clinic-report-mail','MedicineController@reportMail')->name('report-mail');

    Route::get('/purchase-proposal', 'SmMedicalController@purchaseProposal');
    Route::post('/purchase-proposal-store', 'SmMedicalController@purchaseProposalStore');
    Route::get('/delete-purchase-proposal/{id}', 'SmMedicalController@forDeletePurchaseProposal');
    Route::get('/delete-medicine-proposal/{id}', 'SmMedicalController@PurchaseProposalDestroy');
    Route::get('/edit-purchase-proposal/{id}','SmMedicalController@editPurchaseProposal');
    Route::post('/purchase-proposal-update/{id}','SmMedicalController@updatePurchaseProposal');
});