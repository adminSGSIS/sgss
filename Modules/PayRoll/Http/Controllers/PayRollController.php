<?php

namespace Modules\PayRoll\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use App\SmLeaveRequest;
use Auth;
use App\SmStaffAttendence;
use App\SmHoliday;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use App\SmStaff;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SmStaffAttendenceImport;
use Modules\PayRoll\Entities\SmStaffAttendanceImportHistory;
use DateTime;
use File;

class PayRollController extends Controller
{
    public function __construct()
    {
        if(!Auth::check())
        {
            return redirect('/login');
        }
    }

    public function workday($id,$year)
    {
        for($i = 1 ; $i <= 12 ; $i++)
        {
            $date[$i] = Carbon::parse($year.'-'.$i)->daysInMonth;
        }
        $user = User::find($id);
        $leaveDates = SmLeaveRequest::where('user_id',$id)->whereYear('apply_date',$year)->get();
        $attendances = SmStaffAttendence::whereYear('attendence_date',$year)->where('staff_id',User::find($id)->staff->id)->get(); 
        // $lates = SmStaffAttendence::all()->where('staff_id',Auth::user()->staff->id)->where('attendence_type','L'); 
        // $halfdays = SmStaffAttendence::all()->where('staff_id',Auth::user()->staff->id)->where('attendence_type','F'); 
        $holidays = SmHoliday::whereYear('from_date',$year)->get();
        
        //dd($attendances);
        $leaveCount = 0;
        foreach ($leaveDates as $key => $leave) {
            $datetime1=strtotime($leave->leave_from);
            $datetime2=strtotime($leave->leave_to);
            $leaveCount = $leaveCount + (int)(($datetime2 - $datetime1)/86400)+1;
        }

        $totalLeaves = $leaveCount + $attendances->where('attendence_type','A')->count();

        if(Auth::user()->staff->designation_id == 24 || Auth::user()->staff->designation_id == 4 || Auth::user()->staff->designation_id == 105 || Auth::user()->staff->designation_id == 3 || Auth::user()->staff->designation_id == 13)
        {
            $employees = SmStaff::all()->where('department_id',Auth::user()->staff->department_id);
            return view('payroll::workday',compact('employees','date','year','leaveDates','holidays','totalLeaves','attendances','id','user'));
        }
        else{
            if(Auth::user()->staff->designation_id == 1)
            {
                $employees = SmStaff::all();
            return view('payroll::workday',compact('employees','date','year','leaveDates','holidays','totalLeaves','attendances','id','user'));
            }
            else{
                return view('payroll::workday',compact('date','year','leaveDates','holidays','totalLeaves','attendances','id','user'));
            }
            
        }
        
    }

    public function staffAttendanceImport()
    {

        try{
            $histories = SmStaffAttendanceImportHistory::all();
            return view('payroll::staff_attendance_import',compact('histories'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function staffAttendanceStore(Request $request)
    {

        $request->validate([
            'attendance_date' => 'required',
            'file' => 'required|mimes:xlsx, csv'
        ]);

        $import_date = DateTime::createFromFormat('m/d/Y', $request->attendance_date)->format('Y-m-d');
        $check = SmStaffAttendanceImportHistory::where('import_for_date',$import_date)->first();
        if($check != null)
        {
            Toastr::error('Data for '.$import_date.' existed', 'Failed');
            return redirect()->back();
        }
        try{
            $path = $request->file('file');
            $data = Excel::toArray(new SmStaffAttendenceImport,$path);
            $data = $data['0'];

            if ($request->file('file') != "") {
                $file = $request->file('file');
                $file_name = "staff-attendance-".$import_date."." . $file->getClientOriginalExtension();
                $file->move('Modules/PayRoll/public/excel/staff/',$file_name);

                $history = new SmStaffAttendanceImportHistory();
                $history->import_date = Carbon::now()->toDateString(); 
                $history->import_for_date = DateTime::createFromFormat('m/d/Y', $request->attendance_date)->format('Y-m-d');
                $history->import_by = Auth::user()->full_name;
                $history->url = "Modules/PayRoll/public/excel/staff/".$file_name;
                $history->save();
            }

            $staffs = SmStaff::where('active_status', 1)->where('school_id',Auth::user()->school_id)->get();
            $all_staff_no = [];
            foreach ($staffs as $staff) {
                $all_staff_no[] = $staff->staff_no;
            }

            $all_staff_checkin_no = [];
            
            foreach ($data as $key => $value) {
                if(!in_array($value['employeeid'],$all_staff_no))
                {
                    unset($data[$key]);
                }
            }
            foreach ($data as $key => $value) {
                $first_log = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value['time_of_firstlog']))->format('H:i:s');
                $attend_date = DateTime::createFromFormat('m/d/Y', $request->attendance_date)->format('Y-m-d');
                $all_staff_checkin_no[] = $value['employeeid'];
                //Nếu đến sau 8h15 và trước 8h30 => LATE
                if(strtotime($first_log) > strtotime('08:30:00'))
                {
                    $late = new SmStaffAttendence();
                    $staff_id = SmStaff::where('staff_no',$value['employeeid'])->first();
                    if($staff_id != null)
                    {
                        $late->staff_id = $staff_id->id;
                    }
                    else
                    {
                        $late->staff_id = '';
                    }
                    $late->staff_name = $value['guestname'];
                    $late->staff_no = $value['employeeid'];
                    $late->attendence_type = "L";
                    $late->import_id = $history->id;
                    $late->attendence_date = $attend_date;
                    $late->save();
                }
                else{

                    $present = new SmStaffAttendence();
                    $staff_id = SmStaff::where('staff_no',$value['employeeid'])->first();
                    if($staff_id != null)
                    {
                        $present->staff_id = $staff_id->id;
                    }
                    else
                    {
                        $present->staff_id = '';
                    }
                    $present->staff_name = $value['guestname'];
                    $present->staff_no = $value['employeeid'];
                    $present->attendence_type = "P";
                    $present->import_id = $history->id;
                    $present->attendence_date = $attend_date;
                    $present->save();
                }
            }

            
            //Check các id không có trong file excel (Tức là không tới làm ngày hôm đó) và đánh dấu là Absent
            $staffs_not_present = array_diff($all_staff_no,$all_staff_checkin_no);

            foreach ($staffs_not_present as $key => $value) {
                $absent = new SmStaffAttendence();
                $staff_id = SmStaff::where('staff_no',$value)->first();
                $absent->staff_id = $staff_id->id;
                $absent->staff_name = $staff_id->full_name; 
                $absent->staff_no = $value;  
                $absent->attendence_type = "A";
                $absent->import_id = $history->id;
                $absent->attendence_date = $attend_date;
                $absent->save();
            }

            Toastr::success('Operation successful', 'Success');
            return redirect()->back();
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function deleteImported($id)
    {

        $delete = SmStaffAttendanceImportHistory::find($id);
        
        $data = SmStaffAttendence::where('import_id',$id);

        $delete->delete();
        $data->delete();

        $path = base_path().'/'.$delete->url;
        
        if(File::exists($path)){
            File::delete($path);
        }

        Toastr::success('Operation successful', 'Success');
        return redirect()->back();
    }
}
