@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('lang.staff_attendance')</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.staff_attendance')</a>
                <a href="#">@lang('lang.staff_attendance') @lang('lang.import')</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-6">
                <div class="main-title">
                    <h3>@lang('lang.select_criteria')</h3>
                </div>
            </div>
            {{-- <div class="offset-lg-3 col-lg-3 text-right mb-20">
                <a href="{{url('download-staff-attendance-file')}}" >
                    <button class="primary-btn tr-bg text-uppercase bord-rad">
                        @lang('lang.download_sample_file')
                        <span class="pl ti-download"></span>
                    </button>
                </a>
            </div> --}}
        </div>
        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'staff-attendance-store',
                        'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form']) }}
        <div class="row">
            <div class="col-lg-12">
                @if(session()->has('message-success'))
                  <div class="alert alert-success">
                      {{ session()->get('message-success') }}
                  </div>
                @elseif(session()->has('message-danger'))
                  <div class="alert alert-danger">
                      {{ session()->get('message-danger') }}
                  </div>
                @endif
                <div class="white-box">
                    <div class="">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <div class="box-body">      
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>


                        <input type="hidden" name="url" id="url" value="{{URL::to('/')}}">
                        <div class="row mb-40 mt-30">
                            <div class="col-lg-6 mt-30-md">
                                    <div class="row no-gutters input-right-icon">
                                        <div class="col">
                                            <div class="input-effect">
                                                <input class="primary-input date form-control read-only-input has-content" id="startDate" type="text" name="attendance_date" autocomplete="off" value="{{date('m/d/Y')}}">
                                                <label for="startDate"> @lang('Attendance') @lang('lang.date') *</label>
                                                <span class="focus-border"></span>
                                             </div>
                                        </div>
                                        <div class="col-auto">
                                            <button class="" type="button">
                                                <i class="ti-calendar" id="start-date-icon"></i>
                                            </button>
                                        </div>
                                    </div>
                                    
                                </div>
                            <div class="col-lg-6">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input form-control {{ $errors->has('file') ? ' is-invalid' : '' }}" type="text" id="placeholderPhoto" placeholder="Excel file (xlsx, csv) *"
                                                readonly>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('file'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('file') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="photo">@lang('lang.browse')</label>
                                            <input type="file" class="d-none" name="file" id="photo">
                                        </button>
                                    </div>
                                </div>
                            </div>
                                
                        </div>

                        <div class="row mt-40">
                            <div class="col-lg-12 text-center">
                                <button class="primary-btn fix-gr-bg">
                                    <span class="ti-check"></span>
                                    @lang('lang.import') @lang('lang.attendance')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <br><br>
    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Import Date</th>
                <th>Attendance date</th>
                <th>Import by</th>
                <th>Download (.xlsx)</th>
                <th>Option</th>
            </tr>
        </thead>
        <tbody>
            @foreach($histories as $history)
            <tr>
                <td>{{$history->import_date}}</td>
                <td>{{$history->import_for_date}}</td>
                <td>{{$history->import_by}}</td>
                <td><a href="{{url('')}}/{{$history->url}}" download="" target="_blank">Download</a></td>
                <td>
                    <div class="dropdown">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            @lang('lang.select')
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            @if(in_array(297, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1  )
                              <a class="dropdown-item" href="{{url('delete-imported-data/'.$history->id)}}">delete</a>
                            @endif
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</section>
@endsection
