@extends('backEnd.master')
<style type="text/css">
	.weekend{
		color: rgb(255, 238, 137);
		
	}
	.absent,.late,.halfday{
		color :#3234a8;
	}
	.leave{
		
		color: red;
	}
	.present{
		
		color: green;
	}
	.holiday{
		
		color: blue;
	}
	.square{
		width: 20px;
		height: 20px;
	}
	.hl{
		background-color: blue;
	}
	.wk{
		background-color: rgb(255, 238, 137);
	}
	.lv{
		background-color: red;
	}
	.as{
		background-color: #3234a8;
	}
	.pr{
		background-color: green;
	}
	.hd{
		width: 100px;
	}
	.width-40{
		width: 25%;
	}
	.width-60{
		width: 75%;
	}
	table{
		background: darkgrey;
	}
	tr{
		background-color: #fff;
	}
</style>
@section('mainContent')

<?php
	function checkweekend($day , $month , $year)
	{
		$date = $year.'-'.$month.'-'.$day;
        $MyGivenDateIn = strtotime($date);
        $ConverDate = date("l", $MyGivenDateIn);
        $ConverDateTomatch = strtolower($ConverDate);
        
        if(($ConverDateTomatch == "saturday" )|| ($ConverDateTomatch == "sunday")){
            return true;
        } else {
            return false;
        }
	}

	function checkLeaveDate($day , $month , $year , $leaveFrom , $leaveTo)
	{
		$date = strtotime($day.'-'.$month.'-'.$year);
		$from = strtotime($leaveFrom);
		$to = strtotime($leaveTo);

		if($date >= $from && $date <= $to)
			{
				return true;
			}
		else
			{
				return false;
			}
	}

	function checkHoliday($day , $month , $year , $holidayFrom , $holidayTo)
	{
		$date = strtotime($day.'-'.$month.'-'.$year);
		$from = strtotime($holidayFrom);
		$to = strtotime($holidayTo);
		if($date >= $from && $date <= $to)
			{
				return true;
			}
		else
			{
				return false;
			}
	}

	function checkAttendance($day , $month , $year , $attend_date , $attend_type)
	{
		$date = strtotime($day.'-'.$month.'-'.$year);
		$date_attend = strtotime($attend_date);
		if($date == $date_attend && $attend_type == 'L')
			{return 'L';}
		if($date == $date_attend && $attend_type == 'A')
			{return 'A';}
		if($date == $date_attend && $attend_type == 'F')
			{return 'F';}
		if($date == $date_attend && $attend_type == 'P')
			{return 'P';}
	}
?>

	<h2>Workday {{$user->full_name}}</h2>
	<div class="white-box">
		<div class="row">
			<div class="col-lg-4">
				@if(isset($employees))
					<select class="niceSelect w-100 bb" name="status" id="selectEmployee">
						<option value="0">{{Auth::user()->staff->departments->name}} Department</option>
						@foreach($employees as $employee)
						<option value="{{$employee->staff_user->id}}" {{$employee->staff_user->id == $id? 'selected' : ''}}>{{$employee->full_name}}</option>
                    	@endforeach
                	</select>
				@endif
			</div>
			<div class="col-lg-4">
				<select class="niceSelect w-100 bb" name="status" id="selectYear">
					<?php $thisyear = date('Y') ?>
					@for($i = 8 ; $i >= 1 ; $i--)
					<option value="{{$thisyear-$i}}" {{$thisyear-$i == $year ? 'selected' : ''}}>{{$thisyear-$i}}</option>
					@endfor 
					<option value="{{$thisyear}}" {{$thisyear == $year ? 'selected' : ''}}>{{$thisyear}}</option>
                    
                </select>
			</div>
			<div class="col-lg-4">
				Total leave: {{$totalLeaves}} / 12
			</div>

		</div>
		@if(isset($employees))
		<div class="col-lg-12 mt-20 text-right">
            <button type="submit" class="primary-btn small fix-gr-bg" id="btnsubmit">
                <span class="ti-search pr-2"></span>
                Search
            </button>
        </div>
        @endif
	</div>
	<br>
	<div class="white-box">
		<table class="table-bordered table table-responsive">
			<thead>
				<tr>
					<td>Month name</td>
					@for($i = 1 ; $i <= 31 ; $i++)
					<td>{{$i}}</td>
					@endfor
				</tr>
				@foreach($date as $key => $value)
					<tr>
						<td>{{date("F", mktime(0, 0, 0, $key, 10))}}</td>
						@for($i = 1 ; $i<= $value ; $i++)
							<td class=
							"
								@if(checkweekend($i , $key , $year) == true)
									{{'weekend'}}
								@else
									<?php $check = 0 ?>
									@foreach($attendances as $attendance)
										@switch(checkAttendance($i , $key , $year , $attendance->attendence_date , $attendance->attendence_type))
											@case('A')
												{{'absent'}} 
												{{$check = 1}}
												@break
											@case('L')
												{{'late'}}
												{{$check = 1}}
												@break
											@case('F')
												{{'halfday'}}
												{{$check = 1}}
												@break
											@case('P')
												{{'present'}}
												{{$check = 1}}
												@break
											@default
												{{''}}
												{{$check = 0}} 
												@break
										@endswitch
									@endforeach
									@if($check == 0)
										<?php $checkHoliday=false ?>
										@foreach($holidays as $holiday)
											{{checkHoliday($i , $key , $year , $holiday->from_date , $holiday->to_date) == true ? $checkHoliday = true : ''}}
										@endforeach
										@if($checkHoliday == true)
											{{'holiday'}}
										@else
											@foreach($leaveDates as $leaveDate)
												{{checkLeaveDate($i , $key , $year , $leaveDate->leave_from , $leaveDate->leave_to) == true ? 'leave' : ''}}
											@endforeach
										@endif
									@endif
								@endif
							"
							></td>
						@endfor
					</tr>
				@endforeach
			</thead>
		</table>
		<br>
		<div class="row">
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square wk">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Weekend
						</span>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square lv">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Leave
						</span>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square hl">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Holiday
						</span>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square as">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Absent / Late / Halfday
						</span>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square pr">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Present
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="id_user" value="{{$id}}">
@endsection
@section('script')
<script type="text/javascript">
	$( document ).ready(function() {
		$('.holiday').text('H');
    	$('.leave').text('L');
    	$('.weekend').text('W');
    	$('.absent').text('A');
    	$('.late').text('L');
    	$('.halfday').text('F');
    	$('.present').text('P');
    	
	});

 	$('#btnsubmit').click(function(){
 		var id = $('#selectEmployee').val();
 		var year = $('#selectYear').val();
 		var url = '{{url('workday')}}'+'/'+(id)+'/'+(year);
 		if(id == 0)
 		{
 			return false;
 		}
 		if (url) { // require a URL
              window.location = url; // redirect
          }
        return false;
 	});
</script>
@endsection