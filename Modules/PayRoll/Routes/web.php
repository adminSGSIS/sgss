<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('payroll')->group(function() {
    
    Route::get('workday/{id}/{year}', 'PayRollController@workday');

    Route::get('staff-attendance-import', 'PayRollController@staffAttendanceImport');	
    Route::post('staff-attendance-store', 'PayRollController@staffAttendanceStore');

    Route::get('delete-imported-data/{id}', 'PayRollController@deleteImported');
// });
