<?php

namespace Modules\StudyInformation\Entities;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\SmSubject;

class SmGrade extends Model
{
    protected $fillable = [];
    public $table = 'sm_grades';
    private $terms = [1, 2];
    private $studentInfo;

    //relationships
    public function subject()
    {
        return $this->hasOne('App\SmSubject', 'id', 'subject_id');
    }

    public function student()
    {
        return $this->hasOne('App\SmStudent', 'id', 'student_id');
    }

    public function class()
    {
        return $this->hasOne('App\SmClass', 'id', 'class_id');
    }
    //end - relationships

    private function DB_checkNumberInvalid($value)
    {
        return floatval($value) > 10 ? 0 : floatval($value);
    }

    private function DB_catchError()
    {
        DB::rollBack();
        throw new \Exception('Error Processing Request');
    }

    private function DB_countTheOverallGrade($data)
    {
        return ($data->test_1 + $data->test_2 + $data->test_3 + ($data->middle_exam_1 * 2) + ($data->middle_exam_2 * 2) + ($data->final_exam * 3)) / 10;
    }

    public function DB_getStudentGrade($term, $academic)
    {
        return $this->whereStudent_id($this->studentInfo->id)->whereTerm($term)->whereAcademic_year($academic)->orderBy('id', 'asc')->get();
    }

    public function DB_findStudent($student, $academic)
    {
        $this->studentInfo = $student;
        return $this->whereStudent_id($this->studentInfo->id)->whereAcademic_year($academic)->first();
    }

    public function DB_newStudentGrade($academic)
    {
        try {
            DB::beginTransaction();
            $subjects = $this->studentInfo->class->subjects;
            foreach ($this->terms as $term) {
                foreach ($subjects as $subject) {
                    $sm_grade = new SmGrade();
                    $sm_grade->student_id = $this->studentInfo->id;
                    $sm_grade->class_id = $this->studentInfo->class_id;
                    $sm_grade->term = $term;
                    $sm_grade->subject_id = $subject->id;
                    $sm_grade->academic_year = $academic;
                    $sm_grade->save();
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            $this->DB_catchError();
        }
    }

    public function DB_changeClass($academic)
    {
        $this->whereStudent_id($this->studentInfo->id)->whereAcademic_year($academic)->delete();
        $this->DB_newStudentGrade($academic);
    }

    public function DB_setStudentGrade($student)
    {
        try {
            $this->studentInfo = $student;
            DB::beginTransaction();
            $data = $this->DB_getStudentGrade(request()->term, request()->academic);
            foreach ($data as $index => $item) {
                $item->test_1 = $this->DB_checkNumberInvalid(request()->test_1[$index]);
                $item->test_2 = $this->DB_checkNumberInvalid(request()->test_2[$index]);
                $item->test_3 = $this->DB_checkNumberInvalid(request()->test_3[$index]);
                $item->middle_exam_1 = $this->DB_checkNumberInvalid(request()->middle_exam_1[$index]);
                $item->middle_exam_2 = $this->DB_checkNumberInvalid(request()->middle_exam_2[$index]);
                $item->final_exam = $this->DB_checkNumberInvalid(request()->final_exam[$index]);
                $item->overall = $this->DB_countTheOverallGrade($item);
                $item->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            $this->DB_catchError();
        }
    }
}
