<?php

namespace Modules\StudyInformation\Entities;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\SmSubject;

class TestSchedule extends Model
{
    protected $fillable = [];
    public $table = 'test_schedules';
    private $terms = [1, 2];
    private $class;

    //relationships
    public function subject()
    {
        return $this->hasOne('App\SmSubject', 'id', 'subject_id');
    }

    public function class()
    {
        return $this->hasOne('App\SmClass', 'id', 'class_id');
    }
    //end - relationships

    private function DB_catchError()
    {
        DB::rollBack();
        throw new \Exception('Error Processing Request');
    }

    public function DB_getSchedule($class, $term)
    {
        $this->class = $class;
        return $this->whereClass_id($this->class->id)->whereTerm($term)->orderBy('id', 'asc')->get();
    }

    public function DB_newSchedule()
    {
        try {
            DB::beginTransaction();
            $subjects = $this->class->subjects;
            foreach ($this->terms as $term) {
                foreach ($subjects as $subject) {
                    $schedule = new TestSchedule();
                    $schedule->class_id = $this->class->id;
                    $schedule->subject_id = $subject->id;
                    $schedule->term = $term;
                    $schedule->save();
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            $this->DB_catchError();
        }
    }

    public function DB_updateSchedule($class)
    {
        try {
            DB::beginTransaction();
            $data = $this->DB_getSchedule($class, request()->term);
            foreach ($data as $index => $item) {
                $item->date = request()->date[$index];
                $item->time = request()->time[$index];
                $item->room = request()->room[$index];
                $item->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            $this->DB_catchError();
        }
    }
}
