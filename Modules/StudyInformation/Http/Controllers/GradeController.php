<?php

namespace Modules\StudyInformation\Http\Controllers;

use App\SmAcademicYear;
use Facades\App\SmAssignClassTeacher;
use App\SmStudent;
use Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Facades\Modules\StudyInformation\Entities\SmGrade;

class GradeController extends Controller
{
    private $academic;
    private $student;
    const TERM_1 = 1;
    const TERM_2 = 2;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function catchError($message = "Operation Failed")
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }

    private function checkStudentGradeExist()
    {
        return SmGrade::DB_findStudent($this->student, $this->academic);
    }

    private function createNewStudentGrade()
    {
        $this->checkStudentGradeExist() ? '' : SmGrade::DB_newStudentGrade($this->academic);
    }

    private function checkDiffClassId()
    {
        return $this->checkStudentGradeExist()->class_id != $this->student->class_id;
    }

    private function getStudentGrade($term)
    {
        return SmGrade::DB_getStudentGrade($term, $this->academic);
    }

    public function index($id, $academic)
    {
        try {
            $role = Auth::user()->role_id;
            if ($role == 3 && !in_array($id, Auth::user()->parent->childrens->pluck('id')->toArray())) {
                return $this->catchError();
            }
            if ($role == 2 && $id != Auth::user()->student->id) {
                return $this->catchError();
            }
                
            $academic_years = SmAcademicYear::getAcademicYears();
            $this->academic = $academic;
            $this->student = SmStudent::find($id);
    
            //check $academic exist
            if (!in_array($academic, $academic_years)) {
                return $this->catchError();
            }
            //check if subjects exists in this class
            if ($this->student->class->subjects->count() == 0) {
                if ($role == 3 || $role == 2) {
                    return $this->catchError("There are no results for this student");
                }
                return $this->catchError("Please assign subjects for this class");
            }
    
            //check student grade exist, if not then create
            $this->createNewStudentGrade();

            //check out changes in class information and updates on the latest academic
            $lastestAcademic = SmAcademicYear::getLastestYear();
            if ($academic == $lastestAcademic) {
                $this->checkDiffClassId() ? SmGrade::DB_changeClass($academic) : '';
            }
    
            $student_grade_term1 = $this->getStudentGrade(GradeController::TERM_1);
            $student_grade_term2 = $this->getStudentGrade(GradeController::TERM_2);
            if (SmAssignClassTeacher::checkRoleUser($this->student->class_id) == 0) {
                return view('studyinformation::ReadOnly_getStudentGrade', compact('student_grade_term1', 'student_grade_term2', 'academic_years'));
            }
            return view('studyinformation::getStudentGrade', compact('student_grade_term1', 'student_grade_term2', 'academic_years'));
        } catch (\Exception $e) {
            return $this->catchError();
        }
    }

    public function setStudentGrade()
    {
        try {
            $student = SmStudent::find(request()->student_id);
            SmGrade::DB_setStudentGrade($student);
            return redirect()->back();
        } catch (\Exception $e) {
            return $this->catchError();
        }
    }

    public function chooseChildrens()
    {
        try {
            $childrens = Auth::user()->parent->childrens;
            $lastestAcademic = SmAcademicYear::getLastestYear();
            return view('studyinformation::chooseChildrens', compact('childrens', 'lastestAcademic'));
        } catch (\Exception $e) {
            return $this->catchError();
        }
    }
}
