<?php

namespace Modules\StudyInformation\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\SmClass;
use Facades\Modules\StudyInformation\Entities\TestSchedule;
use Brian2694\Toastr\Facades\Toastr;
use Facades\App\SmAssignClassTeacher;
use Auth;
use App\User;

class TestScheduleController extends Controller
{
    private $class;
    const TERM_1 = 1;
    const TERM_2 = 2;
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function catchError($message = "Operation Failed")
    {
        Toastr::error($message, 'Failed');
        return redirect()->back();
    }

    private function checkScheduleExist()
    {
        return TestSchedule::DB_getSchedule($this->class, 1)->first();
    }

    private function createNewSchedule()
    {
        $this->checkScheduleExist() ? '' : TestSchedule::DB_newSchedule();
    }
    
    private function getSchedules($term)
    {
        return TestSchedule::DB_getSchedule($this->class, $term);
    }

    public function index()
    {
        return view('studyinformation::testSchedule');
    }

    public function schedule($class)
    {
        try {
            $role = Auth::user()->role_id;
            if ($role == 3 && !in_array($class, Auth::user()->parent->childrens->pluck('class_id')->toArray())) {
                return $this->catchError();
            }
            if ($role == 2 && $class != Auth::user()->student->class_id) {
                return $this->catchError();
            }
    
            $this->class = SmClass::find($class);

            //check if subjects exists in this class
            if (count($this->class->subjects) == 0) {
                if ($role == 3 || $role == 2) {
                    return $this->catchError("There are no results for this class");
                }
                return $this->catchError("Please assign subjects for this class");
            }

            $this->createNewSchedule();
            $schedules_term1 = $this->getSchedules(TestScheduleController::TERM_1);
            $schedules_term2 = $this->getSchedules(TestScheduleController::TERM_2);
            if (SmAssignClassTeacher::checkRoleUser($this->class->id) == 0) {
                return view('studyinformation::ReadOnly_testSchedule', compact('schedules_term1', 'schedules_term2'));
            }
            return view('studyinformation::testSchedule', compact('schedules_term1', 'schedules_term2'));
        } catch (\Exception $e) {
            return $this->catchError();
        }
    }

    public function updateSchedule()
    {
        try {
            $this->class = SmClass::find(request('class'));
            TestSchedule::DB_updateSchedule($this->class);
            return redirect()->back();
        } catch (\Exception $e) {
            return $this->catchError();
        }
    }
}
