$(document).ready(function () {
  $("#academic").on("change", function () {
    var url = $(location).attr("href");
    window.location.replace(
      url.slice(0, url.lastIndexOf("/")) + "/" + $("#academic").val()
    );
  });

  $("#classes").on("change", function () {
    var url = $(location).attr("href");
    window.location.replace(
      url.slice(0, url.lastIndexOf("/")) + "/" + $("#classes").val()
    );
  });
});
