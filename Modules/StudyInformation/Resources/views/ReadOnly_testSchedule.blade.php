@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th, .centerTd input{
       text-align: center;
   }
</style>
@endsection
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Exam Schedule</h1>
        </div>
    </div>
</section>

@if (isset($schedules_term1[0]))
<section class="admin-visitor-area up_admin_visitor">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">Term 1</h3>
                    </div>
                </div>
            </div>
            <div style="margin-left: -15px; margin-right: -15px">
                <div class="col-lg-12">
                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                        <thead>
                            <tr class="centerTh">
                                <th>subject</th>
                                <th>class name</th>
                                <th>date</th>
                                <th>time</th>
                                <th>room</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($schedules_term1 as $value)
                                <tr class="centerTd">
                                    <td>
                                        {{$value->subject->subject_name}}
                                    </td>
                                    <td>{{ $value->class->class_name }}</td>
                                    <td>
                                        {{$value->date}}
                                    </td>
                                    <td>
                                        {{$value->time}}
                                    </td>
                                    <td>
                                        {{$value->room}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area up_admin_visitor">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">Term 2</h3>
                    </div>
                </div>
            </div>
            <div style="margin-left: -15px; margin-right: -15px">
                <div class="col-lg-12">
                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                        <thead>
                            <tr class="centerTh">
                                <th>subject</th>
                                <th>class name</th>
                                <th>date</th>
                                <th>time</th>
                                <th>room</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($schedules_term2 as $value)
                                <tr class="centerTd">
                                    <td>
                                        {{$value->subject->subject_name}}
                                    </td>
                                    <td>{{ $value->class->class_name }}</td>
                                    <td>
                                        {{$value->date}}
                                    </td>
                                    <td>
                                        {{$value->time}}
                                    </td>
                                    <td>
                                        {{$value->room}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endif

@endsection