@extends('backEnd.master')
@section('css')
<style>
    .centerTd td, .centerTh th, .centerTd input{
        text-align: center;
    }
    a button{
        color: #828bb2 !important;
    }
</style>
@endsection
@section('mainContent')
<section class="admin-visitor-area up_admin_visitor">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">Childrens</h3>
                    </div>
                </div>
            </div>
            <div style="margin-left: -15px; margin-right: -15px">
                <div class="col-lg-12">
                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                        <thead>
                            <tr class="centerTh">
                                <th>Full Name</th>
                                <th>class name</th>
                                <th>Learning Outcomes</th>
                                <th>exam schedule</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($childrens as $value)
                                <tr class="centerTd">
                                    <td>
                                        {{$value->full_name}}
                                    </td>
                                    <td>{{ $value->class->class_name }}</td>
                                    <td>
                                        <a href="{{url('study-information/grade') . '/' . $value->id . '/' . $lastestAcademic}}">
                                            <button type="button" class="btn btn-light">
                                                View
                                            </button>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{url('study-information/exam-schedule') . '/' . $value->class_id}}">
                                            <button type="button" class="btn btn-light">
                                                View
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection