@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th, .centerTd input{
       text-align: center;
   }
   .custom-button{
        position: absolute !important;
        bottom: 0 !important;
        right: 15px !important;
        z-index: 100000 !important;
   }
</style>
@endsection
@section('mainContent')
@php
    $student_name = $student_grade_term1[0]->student->full_name;
@endphp
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Learning Outcomes [{{ $student_grade_term1[0]->class->class_name }} - {{$student_grade_term1[0]->academic_year}}]</h1>
            <select id="academic" class="mt-3 w-100 bb form-control">
                <option value="">Academic Years</option>
                @foreach ($academic_years as $item)
                <option value="{{$item}}">
                    {{$item}}
                </option>
                @endforeach
            </select>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">{{$student_name}} result - Term 1</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/study-information/grade',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                    <input type="hidden" name="student_id" value="{{$student_grade_term1[0]->student_id}}">
                    <input type="hidden" name="academic" value="{{$student_grade_term1[0]->academic_year}}">
                    <input type="hidden" name="term" value="1">
                    <div class="col-lg-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>subject</th>
                                    <th>15'</th>
                                    <th>15'</th>
                                    <th>15'</th>
                                    <th>45'</th>
                                    <th>45'</th>
                                    <th>Final exam</th>
                                    <th>overall</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach($student_grade_term1 as $value)
                                    <tr class="centerTd">
                                        <td>
                                            {{$value->subject->subject_name}}
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="test_1[]" autocomplete="off" value="{{$value->test_1}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="test_2[]" autocomplete="off" value="{{$value->test_2}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="test_3[]" autocomplete="off" value="{{$value->test_3}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="middle_exam_1[]" autocomplete="off" value="{{$value->middle_exam_1}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="middle_exam_2[]" autocomplete="off" value="{{$value->middle_exam_2}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="final_exam[]" autocomplete="off" value="{{$value->final_exam}}">
                                        </td>
                                        <td>{{$value->overall}}</td>
                                    </tr>
                                @endforeach
                                
                                <button type="submit" class="custom-button primary-btn fix-gr-bg">
                                    <span class="ti-check"></span>
                                    @lang('lang.save')
                                </button>
                            </tbody>
                        </table>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">{{$student_name}} result - Term 2</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/study-information/grade',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                    <input type="hidden" name="student_id" value="{{$student_grade_term2[0]->student_id}}">
                    <input type="hidden" name="academic" value="{{$student_grade_term1[0]->academic_year}}">
                    <input type="hidden" name="term" value="2">
                    <div class="col-lg-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>subject</th>
                                    <th>15'</th>
                                    <th>15'</th>
                                    <th>15'</th>
                                    <th>45'</th>
                                    <th>45'</th>
                                    <th>Final exam</th>
                                    <th>overall</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach($student_grade_term2 as $value)
                                    <tr class="centerTd">
                                        <td>
                                            {{$value->subject->subject_name}}
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="test_1[]" autocomplete="off" value="{{$value->test_1}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="test_2[]" autocomplete="off" value="{{$value->test_2}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="test_3[]" autocomplete="off" value="{{$value->test_3}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="middle_exam_1[]" autocomplete="off" value="{{$value->middle_exam_1}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="middle_exam_2[]" autocomplete="off" value="{{$value->middle_exam_2}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="final_exam[]" autocomplete="off" value="{{$value->final_exam}}">
                                        </td>
                                        <td>{{$value->overall}}</td>
                                    </tr>
                                @endforeach
                                
                                <button type="submit" class="custom-button primary-btn fix-gr-bg">
                                    <span class="ti-check"></span>
                                    @lang('lang.save')
                                </button>
                            </tbody>
                        </table>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">{{$student_name}} result - Overall</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                        <thead>
                            <tr class="centerTh">
                                <th>subject - term</th>
                                <th>Overall</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @php
                                $overall_grade = 0;
                                $term1_grade = 0;
                                $term2_grade = 0;
                            @endphp
                            @foreach($student_grade_term1 as $index=>$value)
                                @php
                                    $term1_grade += $student_grade_term1[$index]->overall;
                                    $term2_grade += $student_grade_term2[$index]->overall;
                                @endphp
                                <tr class="centerTd">
                                    <td>{{$value->subject->subject_name}}</td>
                                    <td>{{round(($student_grade_term1[$index]->overall + $student_grade_term2[$index]->overall * 2) / 3, 2)}}</td>
                                </tr>
                            @endforeach
                            @php
                                $term1_grade /= count($student_grade_term1);
                                $term2_grade /= count($student_grade_term2);
                            @endphp                            
                        </tbody>
                        <tr class="centerTd">
                            <td>Term 1</td>
                            <td>{{round($term1_grade, 2)}}</td>
                        </tr>
                        <tr class="centerTd">
                            <td>Term 2</td>
                            <td>{{round($term2_grade, 2)}}</td>
                        </tr>
                        <tr class="centerTd">
                            <td>Overall</td>
                            <td>{{round(($term1_grade + ($term2_grade * 2)) / 3, 2)}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script src="{{asset('Modules/StudyInformation/Resources/assets/js/app.js')}}"></script>
@endsection
