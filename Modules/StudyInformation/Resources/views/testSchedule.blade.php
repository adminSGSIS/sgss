@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th, .centerTd input{
       text-align: center;
   }
   .custom-button{
        position: absolute !important;
        bottom: 0 !important;
        right: 15px !important;
        z-index: 100000 !important;
   }
</style>
@endsection
@section('mainContent')
@php
    $classes = App\SmClass::all();
@endphp
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Exam Schedule @if (isset($schedules_term1[0])) - {{ $schedules_term1[0]->class->class_name }} @endif</h1>
            <select id="classes" class="mt-3 w-100 bb form-control">
                <option value="">Choose Class</option>
                @foreach ($classes as $item)
                <option value="{{$item->id}}">
                    {{$item->class_name}}
                </option>
                @endforeach
            </select>
        </div>
    </div>
</section>

@if (isset($schedules_term1[0]))
<section class="admin-visitor-area up_admin_visitor">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">Term 1</h3>
                    </div>
                </div>
            </div>
            <div style="margin-left: -15px; margin-right: -15px">
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/study-information/exam-schedule/update',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                    <input type="hidden" name="term" value="1">
                    <input type="hidden" name="class" value="{{ $schedules_term1[0]->class_id }}">
                    <div class="col-lg-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>subject</th>
                                    <th>class name</th>
                                    <th>date</th>
                                    <th>time</th>
                                    <th>room</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach($schedules_term1 as $value)
                                    <tr class="centerTd">
                                        <td>
                                            {{$value->subject->subject_name}}
                                        </td>
                                        <td>{{ $value->class->class_name }}</td>
                                        <td>
                                            <input class="primary-input form-control date"
                                            type="text" name="date[]" autocomplete="off" value="{{$value->date}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control time"
                                            type="text" name="time[]" autocomplete="off" value="{{$value->time}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="room[]" autocomplete="off" value="{{$value->room}}">
                                        </td>
                                    </tr>
                                @endforeach
                                
                                <button type="submit" class="custom-button primary-btn fix-gr-bg">
                                    <span class="ti-check"></span>
                                    @lang('lang.save')
                                </button>
                            </tbody>
                        </table>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area up_admin_visitor">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3 class="mb-0">Term 2</h3>
                    </div>
                </div>
            </div>
            <div style="margin-left: -15px; margin-right: -15px">
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/study-information/exam-schedule/update',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                    <input type="hidden" name="term" value="2">
                    <input type="hidden" name="class" value="{{ $schedules_term1[0]->class_id }}">
                    <div class="col-lg-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>subject</th>
                                    <th>class name</th>
                                    <th>date</th>
                                    <th>time</th>
                                    <th>room</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach($schedules_term2 as $value)
                                    <tr class="centerTd">
                                        <td>
                                            {{$value->subject->subject_name}}
                                        </td>
                                        <td>{{ $value->class->class_name }}</td>
                                        <td>
                                            <input class="primary-input form-control date"
                                            type="text" name="date[]" autocomplete="off" value="{{$value->date}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control time"
                                            type="text" name="time[]" autocomplete="off" value="{{$value->time}}">
                                        </td>
                                        <td>
                                            <input class="primary-input form-control"
                                            type="text" name="room[]" autocomplete="off" value="{{$value->room}}">
                                        </td>
                                    </tr>
                                @endforeach
                                
                                <button type="submit" class="custom-button primary-btn fix-gr-bg">
                                    <span class="ti-check"></span>
                                    @lang('lang.save')
                                </button>
                            </tbody>
                        </table>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@endif

@endsection

@section('script')
<script src="{{asset('Modules/StudyInformation/Resources/assets/js/app.js')}}"></script>
@endsection
