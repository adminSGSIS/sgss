<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('study-information')->group(function () {
    Route::get('/choose-childrens', 'GradeController@chooseChildrens');
    Route::get('/grade/{id}/{academic}', 'GradeController@index');
    Route::post('/grade', 'GradeController@setStudentGrade');

    Route::get('/exam-schedule/0', 'TestScheduleController@index');
    Route::get('/exam-schedule/{class}', 'TestScheduleController@schedule');
    Route::post('/exam-schedule/update', 'TestScheduleController@updateSchedule');
});
