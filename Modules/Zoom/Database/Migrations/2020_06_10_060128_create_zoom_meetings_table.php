<?php

use App\SmLanguagePhrase;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZoomMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zoom_meetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('meeting_id')->nullable();
            $table->string('password')->nullable();
            $table->timestamp('start_time')->nullable();
            $table->timestamp('end_time')->nullable();

            //basic
            $table->string('topic')->nullable();
            $table->string('description')->nullable();
            $table->string('attached_file')->nullable();
            $table->string('date_of_meeting')->nullable();
            $table->string('time_of_meeting')->nullable();
            $table->string('meeting_duration')->nullable();

            // setting
            $table->boolean('join_before_host')->nullable();
            $table->boolean('host_video')->nullable();
            $table->boolean('participant_video')->nullable();
            $table->boolean('mute_upon_entry')->nullable();
            $table->boolean('waiting_room')->nullable();
            $table->string('audio')->default('both')->comment('both, telephony & voip');
            $table->string('auto_recording')->default('none')->comment('local, cloud & none');
            $table->string('approval_type')->default(0)->comment('0 => Automatic, 1 => Manually & 2 No Registration');

            //recurring
            $table->boolean('is_recurring')->nullable();
            $table->tinyInteger('recurring_type')->nullable();
            $table->tinyInteger('recurring_repect_day')->nullable();
            $table->string('recurring_end_date')->nullable();

            $table->boolean('status')->default(1);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('school_id')->nullable();
            $table->timestamps();
        });



        $d = [
            [18, 'zoom', 'Zoom', '', '', ''],
            [18, 'virtual_class', 'Virtual Class', '', '', ''],
            [18, 'topic', 'Topic', '', '', ''],
            [18, 'description', 'Description', '', '', ''],
            [18, 'date_of_meeting', 'Date of Meeting', '', '', ''],
            [18, 'time_of_meeting', 'Time of Meeting', '', '', ''],
            [18, 'meeting_durration', 'Meetting Durration (Minutes)', '', '', ''],
            [18, 'zoom_recurring', 'Recurring', '', '', ''],
            [18, 'zoom_recurring_type', 'Recurrence Type', '', '', ''],
            [18, 'zoom_recurring_daily', 'Daily', '', '', ''],
            [18, 'zoom_recurring_weekly', 'Weekly', '', '', ''],
            [18, 'zoom_recurring_monthly', 'Monthly', '', '', ''],
            [18, 'zoom_recurring_repect', 'Repeat every', '', '', ''],
            [18, 'zoom_recurring_end', 'End Recurrence', '', '', ''],
            [18, 'join_before_host', 'Join before host', '', '', ''],
            [18, 'host_video', 'Host Video', '', '', ''],
            [18, 'participant_video', 'Participant Video', '', '', ''],
            [18, 'mute_upon_entry', 'Participate mic mute', '', '', ''],
            [18, 'watermark', 'Watermark', '', '', ''],
            [18, 'waiting_room', 'Waiting Room', '', '', ''],
            [18, 'auto_recording', 'Auto Recording ', '', '', ''],
            [18, 'audio_options', 'Audio Option', '', '', ''],
            [18, 'meeting_approval', 'Meeting Join Approval', '', '', ''],
            [18, 'meeting_id', 'Meeting ID', '', '', ''],
            [18, 'zoom_start_join', 'Join/Start', '', '', ''],
            [18, 'start', 'Start', '', '', ''],
            [18, 'join', 'Join', '', '', ''],
            [18, 'show', 'Show', '', '', ''],
            [18, 'delete_meetings', 'Delete Meeting', '', '', ''],
            [18, 'are_you_sure_delete', 'Are you sure to delete ?', '', '', ''],
            [18, 'zoom_setting', 'Zoom Setting', '', '', ''],
            [18, 'for_paid_package', 'For Paid Package', '', '', ''],
            [18, 'api_key', 'API Key', '', '', ''],
            [18, 'serect_key', 'Secret Key', '', '', ''],
            [18, 'pakage', 'Pakage', '', '', ''],
            [18, 'join_meeting', 'Join Meeting', '', '', ''],
            [18, 'attached_file', 'Attached File ', '', '', ''],
            [18, 'start_date_time', 'Start Date & Time', '', '', ''],
            [18, 'not_yet_start', 'Not Yet Start', '', '', ''],
            [18, 'closed', 'Closed', '', '', ''],
            [18, 'host_id', 'Host ID', '', '', ''],
            [18, 'timezone', 'Timezone', '', '', ''],
            [18, 'created_at', 'Created At', '', '', ''],
            [18, 'join_url', 'Join URL', '', '', ''],
            [18, 'encrypted', 'Encrypted', '', '', ''],
            [18, 'in_mettings', 'in Mettings', '', '', ''],
            [18, 'cn_mettings', 'cn Mettings', '', '', ''],
            [18, 'use_pmi', 'use pmi', '', '', ''],
            [18, 'enforce_login', 'Enforce Login', '', '', ''],
            [18, 'enforce_login_domains', 'Enforce Login Domains', '', '', ''],
            [18, 'alternative_hosts', 'Alternative Hosts', '', '', ''],
            [18, 'meeting_authentication', 'Meeting Authentication', '', '', ''],
            [18, 'delete_virtual_meeting', 'Delete virtaul meeting', '', '', ''],
            [18, 'room', 'Room', '', '', ''],
            [18, 'meeting', 'Meeting', '', '', ''],
            [18, 'join_class', 'Join Class', '', '', ''],
            [18, 'participants', 'Participants', '', '', ''],
            [18, 'meetings', 'Meetings', '', '', ''],
            [18, 'select_member', ' Select Member', '', '', ''],
            [18, 'change_default_settings', 'Change Default Settings', '', '', ''],
            [18, 'virtual_class_meetting', 'Virtual Class/Meeting', '', '', ''],
            [18, 'class_reports', 'Class Reports', '', '', ''],
            [18, 'class_reports', 'Class Reports', '', '', ''],
            [18, 'meeting_reports', 'Meeting Reports', '', '', ''],
            [18, 'date_of_class', 'Date of class', '', '', ''],
            [18, 'time_of_class', 'Time of class', '', '', ''],
            [18, 'duration_of_class', 'Duration of class', '', '', ''],
            [18, 'virtual_class_id', 'VClass ID', '', '', ''],
            [18, 'virtual_meeting', 'Virtual Meeting', '', '', ''],
            [18, 'virtual_class', 'Virtual class', '', '', '']
        ];

        try {
            foreach ($d as $row) {
                $s = SmLanguagePhrase::where('default_phrases', trim($row[1]))->first();
                if (empty($s)) {
                    $s = new SmLanguagePhrase();
                }
                $s->modules = $row[0];
                $s->default_phrases = trim($row[1]);
                $s->en = trim($row[2]);
                $s->es = trim($row[3]);
                $s->bn = trim($row[4]);
                $s->fr = trim($row[5]);
                $s->save();
            }
        } catch (\Throwable $th) {
            Log::info($th);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zoom_meetings');
    }
}
