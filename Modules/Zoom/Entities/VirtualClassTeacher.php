<?php

namespace Modules\Zoom\Entities;

use Illuminate\Database\Eloquent\Model;

class VirtualClassTeacher extends Model
{
    protected $fillable = [];

      protected $table = "zoom_virtual_class_teachers";
}
