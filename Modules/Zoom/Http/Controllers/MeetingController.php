<?php

namespace Modules\Zoom\Http\Controllers;

use App\User;
use App\SmNotification;
use App\SmGeneralSettings;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use MacsiDigital\Zoom\Facades\Zoom;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Modules\Zoom\Entities\ZoomMeeting;
use Modules\Zoom\Entities\ZoomSetting;
use Modules\RolePermission\Entities\InfixRole;
use App\SmHumanDepartment;
use App\SmStaff;
use Modules\Zoom\Entities\ZoomMeetingUsers;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function about()
    { 
      
        try {
            $data = \App\InfixModuleManager::where('name', 'Zoom')->first();
            return view('zoom::index', compact('data'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    /* Tran Thanh Phu - start meeting online */
    public function index()
    {
        try{
            $departments = SmHumanDepartment::where('school_id', Auth::user()->school_id)->get();
            $staffs = SmStaff::all();
            return view('zoom::zoomMeeting.index', compact('departments', 'staffs'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }

    public function createMeeting(Request $request)
    {
        $zoomSetting = ZoomSetting::all()->first();

            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $validator = $request->validate([
                'password' => 'max:8',
                'date' => 'after:yesterday'
            ]);
            if(strtotime($request->date) == strtotime(date('Y-m-d')) && time() > strtotime($request->time)){
                return redirect('/zoom/meetings')->with('timeError', 'Time must be greater than '.date("h:i", time()));
            }
            if(!($request->staffs)){
                return redirect('/zoom/meetings')->with('staffsError', 'Please choose staffs');
            }
            $user = Zoom::user()->find('me');
            $meeting = Zoom::meeting()->make([
                'topic' => $request->topic,
                'type' => 2,
                'start_time' =>  new Carbon(date("Y-m-d", strtotime($request->date))." ".$request->time),
                'duration' => $request->duration,
                'password' => $request->password,
                'agenda' => $request->description,
                'settings' => [
                    'host_video' => $zoomSetting->host_video,
                    'participant_video' => $zoomSetting->participant_video,
                    'waiting_room' => $zoomSetting->waiting_room,
                    'join_before_host' => $zoomSetting->join_before_host,
                    'audio' => $zoomSetting->audio,
                    'auto_recording' => $zoomSetting->auto_recording,
                    'approval_type' => $zoomSetting->approval_type,
                    'mute_upon_entry' => $zoomSetting->mute_upon_entry
                ]
            ]);
            $user->meetings()->save($meeting);

            $host_id = SmStaff::where('email', Auth::user()->email)->first()->id;

            $zoomMeetings = new ZoomMeeting;

            $zoomMeetings->meeting_id = $meeting->id;
            $zoomMeetings->password = $request->password;
            $zoomMeetings->start_time = date("Y-m-d", strtotime($request->date))." ".date("h:i", strtotime($request->time));
            $zoomMeetings->end_time = date("Y-m-d", strtotime($request->date))." ".date("h:i", strtotime($request->time) + $request->duration * 60);
            $zoomMeetings->topic = $request->topic;
            $zoomMeetings->description = $request->description;
            $zoomMeetings->date_of_meeting = date("Y-m-d", strtotime($request->date));
            $zoomMeetings->meeting_duration = $request->duration;
            $zoomMeetings->school_id = Auth::user()->school_id;
            $zoomMeetings->created_by = $host_id;

            $zoomMeetings->save();
        
            if(!in_array((string)$host_id, $request->staffs)){
                $zoomMeetingUsers = new ZoomMeetingUsers;
                $zoomMeetingUsers->meeting_id = $meeting->id;
                $zoomMeetingUsers->user_id = (string)$host_id;
                $zoomMeetingUsers->save();
            }

            foreach($request->staffs as $item){
                $zoomMeetingUsers = new ZoomMeetingUsers;
                $zoomMeetingUsers->meeting_id = $meeting->id;
                $zoomMeetingUsers->user_id = $item;
                $zoomMeetingUsers->save();
            }
            return redirect('/zoom/meetings');
    }

    public function meetingList()
    {
        try{
            $zoomMeetings = ZoomMeeting::all();
            if(Auth::user()->id != 1){
                $staff = SmStaff::where('email', Auth::user()->email)->first();
                $zoomMeetingUsers = ZoomMeetingUsers::where('user_id', $staff->id)->get();
                $zoomMeetings = [];
                foreach($zoomMeetingUsers as $item){
                    $zoomMeetings[] = ZoomMeeting::where('meeting_id', $item->meeting_id)->first();
                }
            }
            $username = Auth::user()->full_name;
            $participants = [];
            foreach($zoomMeetings as $zoomMeeting){
                $participants[] = ZoomMeetingUsers::where('meeting_id', $zoomMeeting->meeting_id)->count();
            }
            $userId = SmStaff::where('email', Auth::user()->email)->first()->id;
            return view('zoom::zoomMeeting.zoomMeetingList', compact('zoomMeetings', 'username', 'participants', 'userId'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }

    public function deleteMeeting($id)
    {
        try{
            $meeting = Zoom::user()->find('me')->meetings()->find($id);
            $meeting->delete();

            ZoomMeeting::where('meeting_id', $id)->first()->delete();
            ZoomMeetingUsers::where('meeting_id', $id)->delete();

            return redirect('/zoom/meetings-list');
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }

    public function detailParticipants($id)
    {
        try{
            $zoomMeetingUsers = ZoomMeetingUsers::where('meeting_id', $id)->get();
            $participants = [];
            foreach($zoomMeetingUsers as $item){
                $participants[] = SmStaff::where('id', $item->user_id)->first();
            }
            $zoomMeeting = ZoomMeeting::where('meeting_id', $id)->first();
            $userId = SmStaff::where('email', Auth::user()->email)->first()->id;
            return view('zoom::zoomMeeting.detail', compact('participants', 'zoomMeetingUsers', 'userId', 'zoomMeeting'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }

    public function deleteParticipant($meeting_id, $id)
    {
        try{
            $staff = ZoomMeetingUsers::where([
                ['meeting_id', $meeting_id],
                ['user_id', $id]
            ])->first();
            
            
            if($id == SmStaff::where('email', Auth::user()->email)->first()->id){
                $meeting = Zoom::user()->find('me')->meetings()->find($staff->meeting_id);
                $meeting->delete();

                ZoomMeeting::where('meeting_id', $staff->meeting_id)->first()->delete();
                ZoomMeetingUsers::where('meeting_id', $staff->meeting_id)->delete();

                return redirect('/zoom/meetings-list');
            }

            if(ZoomMeetingUsers::where('meeting_id', $staff->meeting_id)->count() == 1){
                ZoomMeeting::where('meeting_id', $staff->meeting_id)->first()->delete();

                $meeting = Zoom::user()->find('me')->meetings()->find($staff->meeting_id);
                $meeting->delete();
            }
            $staff->delete();
            return redirect('/zoom/meetings-list');
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }
    /* Tran Thanh Phu - end meeting online */
    public function meetingStart($id)
    {
        try {
            $meeting = ZoomMeeting::where('meeting_id', $id)->first();
            if (!$meeting->currentStatus == 'started') {
                Toastr::error('Class not yet start, try later', 'Failed');
                return redirect()->back();
            }
            if (!$meeting->currentStatus == 'closed') {
                Toastr::error('Class are closed', 'Failed');
                return redirect()->back();
            }
            $data['url'] = $meeting->url;
            $data['topic'] = $meeting->topic;
            $data['password'] = $meeting->password;
            return redirect(url($meeting->url));
            // return view('zoom::meeting.meetingStart', $data);
        } catch (\Exception $e) {
            Toastr::error($e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'participate_ids' => 'required|array',
            'member_type' => 'required',
            'topic' => 'required',
            'description' => 'nullable',
            'password' => 'required',
            'attached_file' => 'nullable|mimes:jpeg,png,jpg,doc,docx,pdf,xls,xlsx',
            'time' => 'required',
            'durration' => 'required',
            'join_before_host' => 'required',
            'host_video' => 'required',
            'participant_video' => 'required',
            'mute_upon_entry' => 'required',
            'waiting_room' => 'required',
            'audio' => 'required',
            'auto_recording' => 'nullable',
            'approval_type' => 'required',
            'is_recurring' => 'required',
            'recurring_type' => 'required_if:is_recurring,1',
            'recurring_repect_day' => 'required_if:is_recurring,1',
            'recurring_end_date' => 'required_if:is_recurring,1',
        ]);

        try {
            //Available time check for classs
            if ($this->isTimeAvailableForMeeting($request, $id = 0)) {
                Toastr::error('Virtual class time is not available for teacher and student!', 'Failed');
                return redirect()->back();
            }

            //Chekc the number of api request by today max limit 100 request
            if (ZoomMeeting::whereDate('created_at', Carbon::now())->count('id') >= 100) {
                Toastr::error('You can not create more than 100 meeting within 24 hour!', 'Failed');
                return redirect()->back();
            }

            $GSetting = SmGeneralSettings::where('school_id', Auth::user()->school_id)->first();
            $users = Zoom::user()->where('status', 'active')->setPaginate(false)->setPerPage(300)->get()->toArray();
            $profile = $users['data'][0];
            $start_date = Carbon::parse($request['date'])->format('Y-m-d') . ' ' . date("H:i:s", strtotime($request['time']));
            $meeting = Zoom::meeting()->make([
                "topic" => $request['topic'],
                "type" => $request['is_recurring'] == 1 ? 8 : 2,
                "duration" => $request['durration'],
                "timezone" => $GSetting->timeZone->time_zone,
                "password" => $request['password'],
                "start_time" => new Carbon($start_date),
            ]);

            $meeting->settings()->make([
                'join_before_host'  => $this->setTrueFalseStatus($request['join_before_host']),
                'host_video'        => $this->setTrueFalseStatus($request['host_video']),
                'participant_video' => $this->setTrueFalseStatus($request['participant_video']),
                'mute_upon_entry'   => $this->setTrueFalseStatus($request['mute_upon_entry']),
                'waiting_room'      => $this->setTrueFalseStatus($request['waiting_room']),
                'audio'             => $request['audio'],
                'auto_recording'    => $request->has('auto_recording') ? $request['auto_recording'] : 'none',
                'approval_type'     => $request['approval_type'],
            ]);

            if ($request['is_recurring'] == 1) {
                $end_date  = Carbon::parse($request['recurring_end_date'])->endOfDay();
                $meeting->recurrence()->make([
                    'type' =>  $request['recurring_type'],
                    'repeat_interval' => $request['recurring_repect_day'],
                    'end_date_time' => $end_date
                ]);
            }
            $meeting_details  = Zoom::user()->find($profile['id'])->meetings()->save($meeting);

            DB::beginTransaction();
            $fileName = "";
            if ($request->file('attached_file') != "") {
                $file = $request->file('attached_file');
                $fileName = $request['topic'] . time() . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/zoom-meeting/', $fileName);
                $fileName = 'public/uploads/zoom-meeting/' . $fileName;
            }
            $system_meeting =  ZoomMeeting::create([
                'topic' =>  $request['topic'],
                'description' =>  $request['description'],
                'date_of_meeting' =>  $request['date'],
                'time_of_meeting' =>  $request['time'],
                'meeting_duration' =>  $request['durration'],

                'host_video' => $request['host_video'],
                'participant_video' => $request['participant_video'],
                'join_before_host' => $request['join_before_host'],
                'mute_upon_entry' => $request['mute_upon_entry'],
                'waiting_room' => $request['waiting_room'],
                'audio' => $request['audio'],
                'auto_recording' => $request->has('auto_recording') ? $request['auto_recording'] : 'none',
                'approval_type' => $request['approval_type'],

                'is_recurring' =>  $request['is_recurring'],
                'recurring_type' =>   $request['is_recurring'] == 1 ? $request['recurring_type'] : null,
                'recurring_repect_day' =>   $request['is_recurring'] == 1 ? $request['recurring_repect_day'] : null,
                'recurring_end_date' =>  $request['is_recurring'] == 1 ?  $request['recurring_end_date'] : null,
                'meeting_id' =>  $meeting_details->id,
                'password' =>  $meeting_details->password,
                'start_time' =>  Carbon::parse($start_date)->toDateTimeString(),
                'end_time' =>  Carbon::parse($start_date)->addMinute($request['durration'])->toDateTimeString(),
                'attached_file' =>  $fileName
            ]);
            $system_meeting->participates()->attach($request['participate_ids']);
            $this->setNotificaiton($request['participate_ids'], $request['member_type'], $updateStatus = 0);
            DB::commit();   

            if ($system_meeting) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $data['localMeetingData'] = ZoomMeeting::where('meeting_id', $id)->first();
            $data['results'] = Zoom::meeting()->find($id)->toArray();
            if ($data['results']) {
                if (Auth::user()->role_id == 1 || Auth::user()->role_id == 4) {
                    return view('zoom::meeting.meetingDetails', $data);
                } else {
                    return view('zoom::meeting.meetingDetailsStudentParent', $data);
                }
            } else {
                Toastr::error('Operation Failed !', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error($e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $data = $this->defaultPageData();
            $data['editdata'] = ZoomMeeting::findOrFail($id);
            $data['user_type'] = $data['editdata']->participates[0]['role_id'];
            $data['userList'] = User::where('role_id', $data['user_type'])
                ->where('school_id', Auth::user()->school_id)
                ->select('id', 'full_name', 'role_id', 'school_id')->get();
            if (Auth::user()->role_id != 1) {
                if (Auth::user()->id != $data['editdata']->created_by) {
                    Toastr::error('Meeting is created by other, you could not modify !', 'Failed');
                    return redirect()->back();
                }
            }
            return view('zoom::meeting.meeting', $data);
        } catch (\Exception $e) {
            Toastr::error($e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'participate_ids' => 'required|array',
            'member_type' => 'required',
            'topic' => 'required',
            'description' => 'nullable',
            'password' => 'required',
            'attached_file' => 'nullable|mimes:jpeg,png,jpg,doc,docx,pdf,xls,xlsx',
            'time' => 'required',
            'durration' => 'required',
            'join_before_host' => 'required',
            'host_video' => 'required',
            'participant_video' => 'required',
            'mute_upon_entry' => 'required',
            'waiting_room' => 'required',
            'audio' => 'required',
            'auto_recording' => 'nullable',
            'approval_type' => 'required',
            'is_recurring' => 'required',
            'recurring_type' => 'required_if:is_recurring,1',
            'recurring_repect_day' => 'required_if:is_recurring,1',
            'recurring_end_date' => 'required_if:is_recurring,1',
        ]);

        // try{
        $system_meeting = ZoomMeeting::findOrFail($id);

        if ($this->isTimeAvailableForMeeting($request, $id = $id)) {
            Toastr::error('Virtual class time is not available !', 'Failed');
            return redirect()->back();
        }

        $users = Zoom::user()->where('status', 'active')->setPaginate(false)->setPerPage(300)->get()->toArray();
        $profile = $users['data'][0];
        $start_date = Carbon::parse($request['date'])->format('Y-m-d') . ' ' . date("H:i:s", strtotime($request['time']));
        $GSetting = SmGeneralSettings::where('school_id', Auth::user()->school_id)->first();
        $meeting = Zoom::meeting()->find($system_meeting->meeting_id)->make([
            "topic" => $request['topic'],
            "type" => $request['is_recurring'] == 1 ? 8 : 2,
            "duration" => $request['durration'],
            "timezone" => $GSetting->timeZone->time_zone,
            "start_time" => new Carbon($start_date),
            "password" => $request['password'],
        ]);

        $meeting->settings()->make([
            'join_before_host'  => $this->setTrueFalseStatus($request['join_before_host']),
            'host_video'        => $this->setTrueFalseStatus($request['host_video']),
            'participant_video' => $this->setTrueFalseStatus($request['participant_video']),
            'mute_upon_entry'   => $this->setTrueFalseStatus($request['mute_upon_entry']),
            'waiting_room'      => $this->setTrueFalseStatus($request['waiting_room']),
            'audio'             => $request['audio'],
            'auto_recording'    => $request->has('auto_recording') ? $request['auto_recording'] : 'none',
            'approval_type'     => $request['approval_type'],
        ]);

        if ($request['is_recurring'] == 1) {
            $end_date  = Carbon::parse($request['recurring_end_date'])->endOfDay();
            $meeting->recurrence()->make([
                'type' =>  $request['recurring_type'],
                'repeat_interval' => $request['recurring_repect_day'],
                'end_date_time' => $end_date
            ]);
        }

        $meeting_details  = Zoom::user()->find($profile['id'])->meetings()->save($meeting);

        DB::beginTransaction();

        $system_meeting->update([
            'topic' =>  $request['topic'],
            'description' =>  $request['description'],
            'date_of_meeting' =>  $request['date'],
            'time_of_meeting' =>  $request['time'],
            'meeting_duration' =>  $request['durration'],
            'password' =>  $request['password'],

            'host_video' => $request['host_video'],
            'participant_video' => $request['participant_video'],
            'join_before_host' => $request['join_before_host'],
            'mute_upon_entry' => $request['mute_upon_entry'],
            'waiting_room' => $request['waiting_room'],
            'audio' => $request['audio'],
            'auto_recording' => $request->has('auto_recording') ? $request['auto_recording'] : 'none',
            'approval_type' => $request['approval_type'],

            'is_recurring' =>  $request['is_recurring'],
            'recurring_type' =>   $request['is_recurring'] == 1 ? $request['recurring_type'] : null,
            'recurring_repect_day' =>   $request['is_recurring'] == 1 ? $request['recurring_repect_day'] : null,
            'recurring_end_date' =>  $request['is_recurring'] == 1 ?  $request['recurring_end_date'] : null,

            'password' =>  $meeting_details->password,
            'start_time' =>  Carbon::parse($start_date)->toDateTimeString(),
            'end_time' =>  Carbon::parse($start_date)->addMinute($request['durration'])->toDateTimeString(),
        ]);

        if ($request->file('attached_file') != "") {
            if (file_exists($system_meeting->attached_file)) {
                unlink($system_meeting->attached_file);
            }
            $file = $request->file('attached_file');
            $fileName = $request['topic'] . time() . "." . $file->getClientOriginalExtension();
            $file->move('public/uploads/zoom-meeting/', $fileName);
            $fileName = 'public/uploads/zoom-meeting/' . $fileName;
            $system_meeting->update([
                'attached_file' =>  $fileName
            ]);
        }

        if (auth()->user()->role_id == 1) {
            $system_meeting->participates()->detach();
            $system_meeting->participates()->attach($request['participate_ids']);
        }
        $this->setNotificaiton($request['participate_ids'], $request['member_type'], $updateStatus = 1);

        DB::commit();
        Toastr::success('Meeting updated successful', 'Success');
        return redirect()->back();

        // } catch (\Exception $e) {
        //     Toastr::error('Operation Failed', 'Failed');
        //     return redirect()->back();
        // }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $localMeeting = ZoomMeeting::findOrFail($id);
            $meeting = Zoom::meeting();
            $meeting->find($localMeeting->meeting_id);
            $meeting->delete(true);
            if (file_exists($localMeeting->attached_file)) {
                unlink($localMeeting->attached_file);
            }
            $localMeeting->delete();
            Toastr::success('Meeting deleted successful', 'Success');
            return redirect()->route('zoom.meetings.index');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    private function setTrueFalseStatus($value)
    {
        if ($value == 1) {
            return true;
        }
        return false;
    }

    public function userWiseUserList(Request $request)
    {
        if ($request->has('user_type')) {
            $userList =  User::where('role_id', $request['user_type'])
                ->where('school_id', Auth::user()->school_id)
                ->select('id', 'full_name', 'school_id')->get();
            return response()->json([
                'users' => $userList
            ]);
        }
    }

    private function setNotificaiton($users, $role_id, $updateStatus)
    {
        $now = Carbon::now('utc')->toDateTimeString();
        $school_id = Auth::user()->school_id;
        $notification_datas = [];

        if ($updateStatus == 1) {
            foreach ($users as $key => $user) {
                array_push(
                    $notification_datas,
                    [
                        'user_id'       => $user,
                        'role_id'       => $role_id,
                        'school_id'     => $school_id,
                        'date'          => date('Y-m-d'),
                        'message'       => 'Zoom meeting is updated by ' . Auth::user()->full_name . '',
                        'url'           => route('zoom.meetings.index'),
                        'created_at'    => $now,
                        'updated_at'    => $now
                    ]
                );
            };
        } else {
            foreach ($users as $key => $user) {
                array_push(
                    $notification_datas,
                    [
                        'user_id'       => $user,
                        'role_id'       => $role_id,
                        'school_id'     => $school_id,
                        'date'          => date('Y-m-d'),
                        'message'       => 'Zoom meeting is created by ' . Auth::user()->full_name . ' with you',
                        'url'           => route('zoom.meetings.index'),
                        'created_at'    => $now,
                        'updated_at'    => $now
                    ]
                );
            };
        }
        SmNotification::insert($notification_datas);
    }

    private function isTimeAvailableForMeeting($request, $id)
    {
        if (isset($request['participate_ids'])) {
            $teacherList = $request['participate_ids'];
        } else {
            $teacherList = [Auth::user()->id];
        }

        if ($id != 0) {
            $meetings = ZoomMeeting::where('date_of_meeting', Carbon::parse($request['date'])->format("m/d/Y"))
                ->where('id', '!=', $id)
                ->where('school_id', Auth::user()->school_id)
                ->whereHas('participates', function ($q) use ($teacherList) {
                    $q->whereIn('user_id', $teacherList);
                })
                ->get();
        } else {
            $meetings = ZoomMeeting::where('date_of_meeting', Carbon::parse($request['date'])->format("m/d/Y"))
                ->where('school_id', Auth::user()->school_id)
                ->whereHas('participates', function ($q) use ($teacherList) {
                    $q->whereIn('user_id', $teacherList);
                })
                ->get();
        }
        if ($meetings->count() == 0) {
            return false;
        }
        $checkList = [];

        foreach ($meetings as $key => $meeting) {
            $new_time   = Carbon::parse($request['date'] . ' ' . date("H:i:s", strtotime($request['time'])));
            if ($new_time->between(Carbon::parse($meeting->start_time), Carbon::parse($meeting->end_time))) {
                array_push($checkList, $meeting->time_of_meeting);
            }
        }
        if (count($checkList) > 0) {
            return true;
        } else {
            return false;
        }
    }

    private function defaultPageData()
    {
        $data['default_settings'] =  ZoomSetting::first()->makeHidden('api_key', 'secret_key', 'created_at', 'updated_at');
        $data['roles'] = InfixRole::where(function ($q) {
            $q->where('school_id', Auth::user()->school_id)->orWhere('type', 'System');
        })->whereNotIn('id', [1, 2])->get();

        if (Auth::user()->role_id == 4) {
            $data['default_settings'] = ZoomSetting::first();
            $data['meetings'] = ZoomMeeting::orderBy('id', 'DESC')->whereHas('participates', function ($query) {
                return $query->where('user_id', Auth::user()->id);
            })
                ->orWhere('created_by', Auth::user()->id)
                ->where('status', 1)
                ->get();
        } elseif (Auth::user()->role_id == 1) {
            $data['meetings'] = ZoomMeeting::orderBy('id', 'DESC')->get();
        } else {
            $data['meetings'] = ZoomMeeting::orderBy('id', 'DESC')->whereHas('participates', function ($query) {
                return  $query->where('user_id', Auth::user()->id);
            })
                ->where('status', 1)
                ->get();
        }
        return $data;
    }
}
