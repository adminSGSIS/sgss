<?php

namespace Modules\Zoom\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Modules\Zoom\Entities\ZoomSetting;
use Illuminate\Support\Facades\Artisan;

class SettingController extends Controller
{
    /* Tran Thanh Phu start */
    public function settings()
    {
        try{
            $host_video = ZoomSetting::all()->first()->host_video;
            $participant_video = ZoomSetting::all()->first()->participant_video;
            $join_before_host = ZoomSetting::all()->first()->join_before_host;
            $mute_upon_entry = ZoomSetting::all()->first()->mute_upon_entry;
            $waiting_room = ZoomSetting::all()->first()->waiting_room;
            $audio = ZoomSetting::all()->first()->audio;
            $auto_recording = ZoomSetting::all()->first()->auto_recording;
            $approval_type = ZoomSetting::all()->first()->approval_type;
            return view('zoom::zoomMeeting.zoomSetting', compact('host_video', 'participant_video', 'join_before_host', 'mute_upon_entry', 'waiting_room', 'audio', 'auto_recording', 'approval_type'));
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }

    public function updateSettings(Request $request)
    {
        try{
            $zoomSetting = ZoomSetting::all()->first();

            $zoomSetting->host_video = $request->has('host_video') ? true : false;
            $zoomSetting->participant_video = $request->has('participant_video') ? true : false;
            $zoomSetting->join_before_host = $request->has('join_before_host') ? true : false;
            $zoomSetting->mute_upon_entry = $request->has('mute_upon_entry') ? true : false;
            $zoomSetting->waiting_room = $request->has('waiting_room') ? true : false;
            $zoomSetting->audio = $request->audio;
            $zoomSetting->auto_recording = $request->auto_recording;
            $zoomSetting->approval_type = $request->approval_type;

            $zoomSetting->save();
            return redirect('/zoom/meetings');
        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back(); 
        }
    }

    /* Tran Thanh Phu end */

    private function putEnvConfigration($key, $value)
    {
        $path = base_path('.env');
        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                $key . '=' . env($key),
                $key . '=' . $value,
                file_get_contents($path)
            ));
        }
    }
}
