<?php

namespace Modules\Zoom\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use MacsiDigital\Zoom\Facades\Zoom;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Modules\Zoom\Entities\ZoomSetting;
use Modules\Zoom\Entities\VirtualClass;
use App\SmStudent;
use Redirect;
use Jenssegers\Agent\Agent;
class VirtualClassFrontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $list=VirtualClass::all();
        return view('zoom::virtualClass.frontEnd.ListClass',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('zoom::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('zoom::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('zoom::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }


    public function join($id,$type)
    {

        $vclass = VirtualClass::find($id);
        $meeting_id = $vclass ->meeting_id;
        $meeting = Zoom::meeting()->find($meeting_id);
        try {
            if (!$vclass->currentStatus == 'started') {
                Toastr::error('Class not yet start, try later', 'Failed');
                return redirect()->back();
            }
            if (!$vclass->currentStatus == 'closed') {
                Toastr::error('Class are closed', 'Failed');
                return redirect()->back();
            }
            if($type=='web_url')
            {
                return redirect(url('https://success.zoom.us/wc/join/'.$meeting_id));
            }          
            return redirect(url($meeting->join_url));
        } catch (\Exception $e) {
            Toastr::error($e->getMessage(), 'Failed');
            return redirect()->back();
        }
        
    }


    public function postVclassLogin(Request $request)
    { /*Xử lý login vào zoom app cho học sinh*/
        $agent = new Agent();
        $request->validate([
                'student_id' => 'required',
                'password' => 'required',
                
            ]);
        $VirtualClass=VirtualClass::where('meeting_id',$request->meeting_id)->first();
        $student = SmStudent::where('class_id',$VirtualClass->class_id)->where('admission_no',$request->student_id)->first(); /*Kiểm tra học sinh có thuộc lớp này không*/
        if(isset($student))
        {   /*Nếu có thuộc lớp này*/
            if($VirtualClass->password == $request->password)/*Kiểm tra meeting passord*/
            {   /*Đúng*/
                if ($request->type =='web') { /*Kiểm tra loại yêu cầu*/
                    /*Học qua web*/
                    
                        return redirect(url('https://zoom.us/wc/'.$request->meeting_id.'/join?prefer=1&un='.base64_encode ($student->full_name)));
                    
                }
                elseif ($request->type == 'app') {
                    /*Học qua app*/
                    if($agent->isDesktop())
                    {
                        return redirect::away('zoommtg://zoom.us/join?confno='.$request->meeting_id.'&pwd='.$VirtualClass->password.'&uname='.$student->full_name);
                    }
                    elseif($agent->isMobile()||$agent->isTablet()){
                        return redirect::away('zoomus://zoom.us/join?confno='.$request->meeting_id.'&pwd='.$VirtualClass->password.'&uname='.$student->full_name);
                    }
                }
            }
            else{/*Sai*/
                return redirect()->back()->with('thongbao','Mật khẩu đăng nhập buổi học không chính xác!!!');
            }
        }
        else 
        {   /*Nếu không thuộc lớp này*/
            return redirect()->back()->with('thongbao','Học sinh không thuộc lớp này không thể truy cập!!!');
        }
    }

    public function getVclassLogin($id,$type)
    {
        $meeting_id=VirtualClass::find($id)->meeting_id;
        return view('zoom::virtualClass.frontEnd.login',compact('meeting_id','type'));
    }
}
