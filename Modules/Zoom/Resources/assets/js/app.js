(function ($) {
    "use strict";

    $(".default-settings").hide();
    $(document).ready(function () { 

        $(document).on('click', '.recurring-type', function () {
            if ($("input[name='is_recurring']:checked").val() == 0) {
                $(".recurrence-section-hide").hide();
            } else {
                $(".recurrence-section-hide").show();
            }
        })

        $(document).on('click', '.chnage-default-settings', function () {
            if ($(this).val() == 0) {
                $(".default-settings").hide();
            } else {
                $(".default-settings").show();
            }
        })
    })
})(jQuery);
