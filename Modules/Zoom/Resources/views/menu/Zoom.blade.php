
@if(@in_array(554, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
    <li>
        <a href="#zoomMenu" data-toggle="collapse" aria-expanded="false"
        class="dropdown-toggle">
            <span class="flaticon-reading"></span>
        @lang('lang.virtual_class')
        </a>
        <ul class="collapse list-unstyled" id="zoomMenu">
            @if(@in_array(555, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                <li>
                    <a href="{{ url('zoom/virtual-class')}}">@lang('lang.virtual_class')</a>
                </li>
            @endif
            @if(@in_array(560, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                <li>
                    <a href="{{ url('zoom/meetings') }}">@lang('lang.virtual_meeting')</a>
                </li>
            @endif
            @if(@in_array(565, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                <li>
                    <a href="{{ url('zoom/virtual-class-reports') }}">@lang('lang.class_reports')</a>
                </li>
            @endif

            @if(@in_array(567, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                <li>
                    <a href="{{ url('zoom/meeting-reports') }}">@lang('lang.meeting_reports')</a>
                </li>
            @endif
            @if(@in_array(569, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                <li>
                    <a href="{{ url('zoom/settings') }}">@lang('lang.settings')</a>
                </li>
            @endif
        </ul>
    </li>
    <!-- Zoom Menu  -->
@endif
