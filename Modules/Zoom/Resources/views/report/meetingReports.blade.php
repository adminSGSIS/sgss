@extends('backEnd.master')


@section('css')
    <link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/zoom.css')}}"/>
@endsection


@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('lang.meetings') @lang('lang.reports') </h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.meetings')</a>
                <a href="#"> @lang('lang.reports') </a>
            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area">
    <div class="container-fluid p-0">

        <div class="row">
            <div class="col-lg-10">
                <h3 class="mb-30">
                    @lang('lang.meetings') @lang('lang.reports')
                </h3>
            </div>
        </div>
        <div class="row mb-20">
            <div class="col-lg-12">
                @if(session()->has('message-success') != "")
                    @if(session()->has('message-success'))
                    <div class="alert alert-success">
                        {{ session()->get('message-success') }}
                    </div>
                    @endif
                @endif
                <div class="white-box">
                    @if(in_array(567, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                        <form action="{{ route('zoom.meeting.reports.show') }}" method="GET">
                    @endif
                            <div class="row">
                                <div class="col-lg-4 mt-30-md">
                                    <select class="niceSelect w-100 bb user_type form-control" name="member_type">
                                        <option data-display=" @lang('lang.member_type') *" value="">@lang('lang.member_type') *</option>
                                        @foreach($roles as $value)
                                            @if(isset($member_type))
                                                <option value="{{$value->id}}" {{ $value->id == $member_type ? 'selected' : '' }}>{{ $value->name }}</option>
                                            @else
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4 mt-30-md" id="select_section_div">
                                    <div class="row">
                                        <div class="col-lg-5 d-flex">
                                            <p class="text-uppercase fw-500 mb-10">@lang('lang.select_member')</p>
                                        </div>
                                        <div class="col-lg-7">
                                    <select multiple id="selectSectionss" {{ @$errors->has('member_ids') ? ' is-invalid' : '' }} name="member_ids[]" style="width:300px">
                                        @if(isset($editdata))
                                            @foreach ($userList as $value)
                                                <option value="{{$value->id}}" {{ $value->role_id == $user_type ? 'selected' : '' }} >{{$value->full_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                                </div>
                                <div class="col-lg-2 mt-30-md">
                                    <input data-display="@lang('lang.from_date')" placeholder="@lang('lang.from_date')" class="primary-input date form-control" type="text" name="from_time" value="{{ isset($from_time) ? Carbon\Carbon::parse($from_time)->format('m/d/Y') : '' }}">
                                </div>
                                <div class="col-lg-2 mt-30-md">
                                    <input data-display="@lang('lang.to_date')" placeholder="@lang('lang.to_date')" class="primary-input date form-control" type="text" name="to_time" value="{{ isset($to_time) ? Carbon\Carbon::parse($to_time)->format('m/d/Y') : '' }}">
                                </div>

                                @php
                                    $tooltip = "";
                                        if(in_array(568, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                        {
                                            $tooltip = "";
                                        }else{
                                            $tooltip = "You have no permission to search";
                                        }
                                @endphp

                                <div class="col-lg-12 mt-20 text-right">
                                    <button type="submit" class="primary-btn small fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">
                                        <span class="ti-search pr-2"></span>
                                        @lang('lang.search')
                                    </button>
                                </div>

                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area" style="display:  {{ isset($meetings) ? 'block' : 'none'  }} ">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <table id="" class="display school-table school-table-style" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('lang.meeting_id')</th>
                                    <th>@lang('lang.password')</th>
                                    <th>@lang('lang.topic')</th>
                                    <th>@lang('lang.participans')</th>
                                    <th>@lang('lang.date')</th>
                                    <th>@lang('lang.time')</th>
                                    <th>@lang('lang.duration')</th>
                                </tr>
                        </thead>

                        <tbody>
                            @if (isset($meetings))
                                @foreach($meetings as $key => $meeting )
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $meeting->meeting_id }}</td>
                                    <td>{{ $meeting->password }}</td>
                                    <td>{{ $meeting->topic }}  </td>
                                    <td>{{ $meeting->participatesName }}</td>
                                    <td>{{ $meeting->date_of_meeting }}</td>
                                    <td>{{ $meeting->time_of_meeting }}</td>
                                    <td>{{ $meeting->meeting_duration }} Min</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            </div>

        </div>
    </div>
</section>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $(document).on('change','.user_type',function(){
                let userType = $(this).val();
                $("#selectSectionss").select2().empty()
                $.get('{{ route('zoom.user.list.user.type.wise') }}',{ user_type: userType },function(res){
                    $("#selectSectionss").select2().empty()
                    $.each(res.users, function( index, item ) {
                        $('#selectSectionss').append(new Option(item.full_name, item.id))
                    });
                })
            })
        })
    </script>
@stop
