@extends('backEnd.master')
@section('mainContent')

    <link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/zoom.css')}}"/> 
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('lang.manage') @lang('lang.zoom_setting')</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.virtual_class')</a>
                <a href="#">@lang('lang.settings')</a>
            </div>
        </div>
    </div>
</section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{ route('zoom.settings.update') }}" method="POST">
                        @csrf
                        <div class="white-box">
                                <div class="row p-0">
                                    <div class="col-lg-12">
                                        <h3 class="text-center">@lang('lang.zoom_setting')</h3>
                                        <hr>


                                        <div class="row mb-40 mt-40">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10">@lang('lang.meeting_approval')</p>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <select class="w-100 bb niceSelect form-control {{ @$errors->has('approval_type') ? ' is-invalid' : '' }}" name="approval_type">
                                                            <option data-display="@lang('lang.select') *" value="">@lang('lang.select') *</option>
                                                            <option value="0" {{ old('approval_type',$setting->approval_type) == 0? 'selected' : ''}} >Automatically</option>
                                                            <option value="1" {{ old('approval_type',$setting->approval_type) == 1? 'selected' : ''}} >Manually Approve</option>
                                                            <option value="2" {{ old('approval_type',$setting->approval_type) == 2? 'selected' : ''}} >No Registration Required</option>
                                                        </select>
                                                        @if ($errors->has('approval_type'))
                                                            <span class="invalid-feedback invalid-select" role="alert">
                                                                <strong>{{ @$errors->first('approval_type') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10">@lang('lang.host_video') </p>
                                                    </div>
                                                    <div class="col-lg-7">
                                                            <div class="radio-btn-flex ml-20">
                                                                <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="host_video" id="host_video_on" value="1" class="common-radio relationButton" {{ old('host_video',$setting->host_video) == 1 ? 'checked': ''}}>
                                                                        <label for="host_video_on">@lang('lang.enable')</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="host_video" id="host_video" value="0" class="common-radio relationButton" {{ old('host_video',$setting->host_video) == '0' ? 'checked': ''}}>
                                                                        <label for="host_video">@lang('lang.disable')</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row mb-40 mt-40">

                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10"> @lang('lang.auto_recording') ( @lang('lang.for_paid_package') )</p>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <select class="w-100 bb niceSelect form-control {{ @$errors->has('auto_recording') ? ' is-invalid' : '' }}" name="auto_recording">
                                                            <option data-display="@lang('lang.select') *" value="">@lang('lang.select') *</option>
                                                            <option value="none" {{ old('auto_recording',$setting->auto_recording) == 'none'? 'selected' : ''}} >None</option>
                                                            <option value="local" {{ old('auto_recording',$setting->auto_recording) == 'local'? 'selected' : ''}} >Local</option>
                                                            <option value="cloud" {{ old('auto_recording',$setting->auto_recording) == 'cloud'? 'selected' : ''}} >Cloud</option>
                                                        </select>
                                                        @if ($errors->has('auto_recording'))
                                                        <span class="invalid-feedback invalid-select" role="alert">
                                                            <strong>{{ @$errors->first('auto_recording') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10">@lang('lang.participant_video') </p>
                                                    </div>
                                                    <div class="col-lg-7">
                                                            <div class="radio-btn-flex ml-20">
                                                                <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="participant_video" id="participant_video_on" value="1" class="common-radio relationButton" {{ old('participant_video',$setting->participant_video) == 1? 'checked': ''}}>
                                                                        <label for="participant_video_on">@lang('lang.enable')</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="participant_video" id="participant_video" value="0" class="common-radio relationButton" {{ old('participant_video',$setting->participant_video) == 0? 'checked': ''}}>
                                                                        <label for="participant_video">@lang('lang.disable')</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row mb-40 mt-40">

                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10">@lang('lang.audio_options')</p>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <select class="w-100 bb niceSelect form-control {{ @$errors->has('audio') ? ' is-invalid' : '' }}" name="audio">
                                                            <option data-display="@lang('lang.select') *" value="">@lang('lang.select') *</option>
                                                            <option value="both" {{ old('audio',$setting->audio) == 'both' ? 'selected' : ''}} >Both</option>
                                                            <option value="telephony"  {{ old('audio',$setting->audio) == 'telephony'? 'selected' : ''}}>Telephony</option>
                                                            <option value="voip"  {{ old('audio',$setting->audio) == 'voip'? 'selected' : ''}} >Voip</option>

                                                        </select>
                                                        @if ($errors->has('audio'))
                                                        <span class="invalid-feedback invalid-select" role="alert">
                                                            <strong>{{ @$errors->first('audio') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10">@lang('lang.join_before_host') </p>
                                                    </div>
                                                    <div class="col-lg-7">
                                                            <div class=" radio-btn-flex ml-20">
                                                                <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="join_before_host" id="join_before_host_on" value="1" class="common-radio relationButton"  {{  old('join_before_host',$setting->join_before_host) == 1? 'checked': '' }}>
                                                                        <label for="join_before_host_on">@lang('lang.enable')</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="join_before_host" id="join_before_host" value="0" class="common-radio relationButton"  {{ old('join_before_host',$setting->join_before_host) == 0? 'checked': '' }}>
                                                                        <label for="join_before_host">@lang('lang.disable')</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mb-40 mt-40">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10">@lang('lang.pakage')</p>
                                                    </div>
                                                    <div class="col-lg-7">
                                                        <select class="w-100 bb niceSelect form-control {{ @$errors->has('package_id') ? ' is-invalid' : '' }}" name="package_id">
                                                            <option data-display="@lang('lang.select') *" value="">@lang('lang.select') *</option>
                                                            <option value="1" {{ old('package_id',$setting->package_id) == 1 ? 'selected' : ''}} >Basic (Free)</option>
                                                            <option value="2" {{ old('package_id',$setting->package_id) == 2 ? 'selected' : ''}} >Pro</option>
                                                            <option value="3" {{ old('package_id',$setting->package_id) == 3 ? 'selected' : ''}} >Business</option>
                                                            <option value="4" {{ old('package_id',$setting->package_id) == 4 ? 'selected' : ''}} >Enterprise</option>
                                                        </select>
                                                        @if ($errors->has('package_id'))
                                                        <span class="invalid-feedback invalid-select" role="alert">
                                                            <strong>{{ @$errors->first('package_id') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10">@lang('lang.waiting_room')</p>
                                                    </div>
                                                    <div class="col-lg-7">
                                                            <div class=" radio-btn-flex ml-20">
                                                                <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="waiting_room" id="waiting_room_on" value="1" class="common-radio relationButton"  {{ old('waiting_room',$setting->waiting_room) == 1? 'checked': '' }}>
                                                                        <label for="waiting_room_on">@lang('lang.enable')</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="waiting_room" id="waiting_room" value="0" class="common-radio relationButton"  {{ old('waiting_room',$setting->waiting_room) == 0? 'checked': '' }}>
                                                                        <label for="waiting_room">@lang('lang.disable')</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="row mb-40 mt-40">
                                            <div class="col-lg-6">
                                                <div class="input-effect sm2_mb_20 md_mb_20">
                                                    <input class="primary-input form-control{{ $errors->has('api_key') ? ' is-invalid' : '' }}" type="text" name="api_key" value="{{ old('api_key',$setting->api_key) }}">
                                                    <label>@lang('lang.api_key')<span>*</span> </label>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('api_key'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('api_key') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-5 d-flex">
                                                        <p class="text-uppercase fw-500 mb-10"> @lang('lang.mute_upon_entry') </p>
                                                    </div>
                                                    <div class="col-lg-7">

                                                            <div class="radio-btn-flex ml-20">
                                                                <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="mute_upon_entry" id="mute_upon_entr_on" value="1" class="common-radio relationButton" {{ old('mute_upon_entry',$setting->mute_upon_entry) == 1? 'checked': ''}}>
                                                                        <label for="mute_upon_entr_on">@lang('enable')</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="">
                                                                        <input type="radio" name="mute_upon_entry" id="mute_upon_entry" value="0" class="common-radio relationButton"  {{ old('mute_upon_entry',$setting->mute_upon_entry) == 0? 'checked': ''}}>
                                                                        <label for="mute_upon_entry">@lang('disable')</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="row mb-40 mt-40">
                                            <div class="col-lg-6">
                                                <div class="input-effect sm2_mb_20 md_mb_20">
                                                    <input class="primary-input form-control{{ $errors->has('secret_key') ? ' is-invalid' : '' }}" type="text" name="secret_key" value="{{ old('secret_key',$setting->secret_key) }}">
                                                    <label>@lang('serect_key')<span>*</span></label>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('secret_key'))
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong>{{ $errors->first('secret_key') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                         </div>

                                        @if(@in_array(570, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                                            <div class="row mt-40">
                                                <div class="col-lg-12 text-center">
                                                <button class="primary-btn fix-gr-bg" id="_submit_btn_admission">
                                                        <span class="ti-check"></span>
                                                        @lang('lang.update')
                                                    </button>
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
