@extends('backEnd.master')
@section('mainContent')

<link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/zoom.css')}}"/>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('lang.virtual_class') @lang('lang.room')</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.virtual_class')</a>
                <a href="#">@lang('lang.room')</a>
            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-8">
                <h3 class="mb-30">@lang('lang.virtual_class') @lang('lang.room')</h3>
            </div>
            <div class="col-lg-4 text-right col-md-12 mb-20">
                @lang('lang.topic') : {{ $topic }} & @lang('lang.password') : {{ $password }}
            </div>
        </div>
        <div class="row">
            <div class="iframe-container" style="overflow: hidden; padding-top: 56.25%; position: relative;">
                <iframe allow="microphone; camera" style="border: 0; height: 100%; left: 0; position: absolute; top: 0; width: 100%;" src="https://success.zoom.us/wc/join/{meeting-id}" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')

@stop



