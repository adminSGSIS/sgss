@extends('frontEnd.home.layout.front_master')
@section('main_content')
<div class="container">
	<div class="iframe-container" style="overflow: hidden; padding-top: 56.25%; position: relative;">
                <iframe src=“https://zoom.us/wc/join/{{$meeting_id}} sandbox=“allow-same-origin allow-forms allow-scripts” allowfullscreen=“allowfullscreen”></iframe>
    </div>
</div>
@endsection