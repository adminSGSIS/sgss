@extends('frontEnd.home.layout.front_master')
@section('main_content')
<link rel="stylesheet" type="text/css" href="{{url('/Modules/Zoom/public/css/datatables.min.css')}}"/>
<div class="banner-inner">
            <img width="100%" src="{{asset('public/images/banner/banner-du-an.jpg')}}">
        </div>

<div class="container">
  <h3>List virtual classroom</h3>
  
	<table id="example" class="table table-striped table-bordered nowrap display responsive" style="width:100%">
  <thead>
    <tr>
      <th >#</th>
      <th >Class</th>
      <th >Section</th>
      <th >VClass_ID</th>
      <th >Teacher</th>
      <th >Topic</th>
      <th >Date</th>
      <th >Time</th>
      <th >Join</th>
    </tr>
  </thead>
  <tbody>
  	@php $i=1; @endphp
  	@foreach($list as $list)
    <tr>
      	<td scope="row">{{$i++}}</td>
      	<td>{{ $list->class->class_name}}</td>
      	<td>{{ $list->section->section_name}}</td>
      	<td>{{ $list->meeting_id }}</td> 
        <td>@foreach($list->staff as $classteacher) {{$classteacher->full_name}}</br> @endforeach</td>      	
        <td>{{ $list->topic }}</td>
        <td>{{ $list->date_of_meeting }}</td>
        <td>{{ $list->time_of_meeting }}</td>
        <td>
        	<a href="{{url('zoom/login-zoom/'.$list->id)}}/web"  type="button" class="btn btn-info btn-sm join-class">Web</a>
          <a href="{{url('zoom/login-zoom/'.$list->id)}}/app" type="button" class="btn btn-primary btn-sm join-class">App</a>        	      	
        </td>
    </tr>
    @endforeach 
  </tbody>
</table>
</div>

@endsection
@section('script')
<script type="text/javascript" src="{{url('/Modules/Zoom/public/js/datatables.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
    responsive: true,
    columnDefs: [
        { responsivePriority: 1, targets: 1 },
        { responsivePriority: 2, targets: 3 },
        { responsivePriority: 5, targets: 6 },
        { responsivePriority: 2, targets: -1 }
        
    ]
} );

} );
</script>
@endsection
