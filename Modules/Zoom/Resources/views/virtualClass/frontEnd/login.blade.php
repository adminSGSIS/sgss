@extends('frontEnd.home.layout.front_master')
@section('main_content') 
<div class="banner-inner">
            <img width="100%" src="{{asset('public/images/banner/banner-du-an.jpg')}}">
        </div>
<div class="container">
    <div class="row">
         <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <h1 style="color: black">LOGIN</h1>
            <form method="POST" action="{{url('/zoom/')}}/login-zoom" accept-charset="UTF-8" class="form-horizontal">
            @csrf
                 @if(session('thongbao'))
                    <div class="alert alert-danger">
                    {{session('thongbao')}}
                    </div>
                    @endif 
              <div class="add-visitor">
                    <input type="hidden" name="meeting_id" id="meeting_id" value="{{$meeting_id}}">
                    <input type="hidden" name="type" value="{{$type}}">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="input-effect">
                                                                <label>Student id<span>*</span></label>
                                                                <input class=" form-control" type="text" name="student_id" autocomplete="off" value="" required="">
                                                                
                                                                
                                                                <span class="focus-border"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-35">
                                                        <div class="col-lg-12">
                                                            <div class="input-effect">
                                                                <label>Meeting password<span>*</span></label>
                                                                <input class="form-control" type="password" name="password" autocomplete="off" maxlength="8" value="" required="">
                                                                
                                                                <span class="focus-border"></span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                                                                                                                                                                 
                                                   <div class="row mt-40">
                                                        <div class="col-lg-12 text-center">
                                                           <button class="primary-btn fix-gr-bg btn-booktour" data-toggle="tooltip" title="" data-original-title="">
                                                                <span class="ti-check">Login</span>
                                                            </button>
                                                        </div>
                                                    </div>
              </div>
    
          </form>
        </div>
    </div>
          
</div>
@endsection