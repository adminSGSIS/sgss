@extends('backEnd.master')

@section('css')
    <link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/zoom.css')}}"/>
@endsection

@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1> @lang('lang.virtual_class')  @lang('lang.list')</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.virtual_class')</a>
                <a href="#">@lang('lang.list')</a>
            </div>
        </div>
    </div>
</section>


<section class="admin-visitor-area">
    <div class="container-fluid p-0">
        <div class="row">
                @include('zoom::virtualClass.includes.form')
                @include('zoom::virtualClass.includes.list')
        </div>
    </div>
</section>
@endsection

@section('script')
    @if(isset($editdata))
        @if ( old('is_recurring',$editdata->is_recurring) == 1)
            <script>$(".recurrence-section-hide").show();</script>
        @else
            <script>$(".recurrence-section-hide").hide();</script>
        @endif
    @elseif( old('is_recurring') == 1)
        <script>$(".recurrence-section-hide").show();</script>
    @else
        <script>$(".recurrence-section-hide").hide();</script>
    @endif
    <script>
    (function($) { 
        "use strict"; 
        $(document).ready(function(){

            $(".default-settings").hide();

            $(document).on('click','.recurring-type',function(){
                if($("input[name='is_recurring']:checked").val() == 0){
                    $(".recurrence-section-hide").hide();
                }else{
                    $(".recurrence-section-hide").show();
                }
            })
            $(document).on('click','.chnage-default-settings',function(){
                if($(this).val() == 0){
                    $(".default-settings").hide();
                }else{
                    $(".default-settings").show();
                }
            })
        })
    })(jQuery);
    </script>
@stop
