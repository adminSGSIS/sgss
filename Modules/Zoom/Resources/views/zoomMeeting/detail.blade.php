{{-- /* Tran Thanh Phu start - detail participants */ --}}
@extends('backEnd.master')
@section('css')
    <link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/meetingList.css')}}"/>
@endsection
@section('mainContent')

<div class="limiter">
    <div class="container-table100">
        <div class="wrap-table100">
            <div class="table100">
                <table>
                    <thead>
                        <tr class="table100-head">
                            <th class="column1">Meeting Id</th>
                            <th class="column2">Name</th>
                            <th class="column3">Email</th>
                            @if ($zoomMeeting->created_by == $userId || Auth::user()->id == 1)
                                <th class="column6">Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($participants as $item)
                                <tr>
                                    <td class="column1">{{$zoomMeetingUsers[0]->meeting_id}}</td>
                                    <td class="column2">{{$item->full_name}}</td>
                                    <td class="column3">{{$item->email}}</td>
                                    @if ($zoomMeeting->created_by == $userId || Auth::user()->id == 1)
                                        <form action="/zoom/detail/{{$zoomMeeting->meeting_id}}/{{$item->id}}" method="POST">
                                            @csrf
                                            <td class="column6">
                                                <button class="btn btn-danger" style="border-radius: 20px; font-size: 14px">Delete</button>
                                            </td>
                                        </form>
                                    @endif
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
{{-- /* Tran Thanh Phu end - detail participants */ --}}