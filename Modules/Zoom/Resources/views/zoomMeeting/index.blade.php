{{-- /* Tran Thanh Phu start - create a meeting */ --}}
@extends('backEnd.master')
@section('css')
    <link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/zoomMeeting.css')}}"/>
@endsection
@section('mainContent')
<style>
    .list{
        padding-top: 1px !important; 
    }
</style>
<div class="container-contact100">
    <div class="wrap-contact100">
        <div class="contact100-form-title" style="background-image: url(https://res.cloudinary.com/dtwtxfuug/image/upload/v1597766074/bg-01_lospre.jpg);">
            <span class="contact100-form-title-1">
                CREATE A MEETING
            </span>

        </div>
        @if (session('staffsError'))
            <div class="alert alert-warning" role="alert">{{session('staffsError')}}</div>
        @endif
        <form action="/zoom/meetings" method="POST" class="contact100-form validate-form">
            @csrf
            <div class="wrap-input100 validate-input input-effect">
                <span class="label-input100">Topic:</span>
                <input class="primary-input  form-control" type="text" name="topic" required autocomplete="off">
                <label>Enter Topic</label>
                <span class="focus-border"></span>
            </div>
 

            <div class="wrap-input100 validate-input" style="border: 0" >
                <span class="label-input100">Role:</span>
                <select class="niceSelect w-100 bb form-control mb-3" onchange="addStaffFunc()" name="role_id" id="role_id">
                    <option data-display="Role" value=""> @lang('lang.select') </option>
                    @foreach($departments as $key=>$value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
                <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="addStaff" onclick="addStaffButton()" >
                    <img style="margin-top: -3px" width="20px" src="https://static.thenounproject.com/png/261370-200.png" alt="">
                    ADD STAFF
                </button>
                <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="closeAddStaff" onclick="closeAddStaffButton()">
                    <img style="margin-top: -3px" width="20px" src="https://static.thenounproject.com/png/261368-200.png" alt="">
                    ADD STAFF
                </button>
                @foreach ($departments as $department)
                <div style="display: none" id="{{$department->id}}">
                    @foreach ($staffs as $staff)
                        @if ($staff->department_id == $department->id)
                            <div class="mt-2 {{ $errors->has('staffs') ? ' is-invalid' : '' }}" style="color: #828bb2;">
                                <input type="checkbox" class="uncheck" name="staffs[]" value="{{$staff->id}}" id="{{$staff->email}}">
                                <label for="{{$staff->email}}">{{$staff->full_name}}</label>
                            </div>
                        @endif
                    @endforeach
                </div>
                @endforeach
            </div>

            <div style="border: none" class="wrap-input100 validate-input">
                <span class="label-input100">Date:</span>
                <div class="no-gutters input-right-icon">
                    <div class="col">
                        <div class="input-effect sm2_mb_20 md_mb_20">
                            <input class="primary-input date form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" id="startDate" type="text"
                                name="date" autocomplete="off" required="">
                            <label>Enter Date</label>
                            <span class="focus-border"></span>
                            @if ($errors->has('date'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('date') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-auto">
                        <button class="" type="button">
                            <i class="ti-calendar" id="start-date-icon"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div style="border: none" class="wrap-input100 validate-input">
                <span class="label-input100">Time:</span>
                <div class="no-gutters input-right-icon">
                    <div class="col">
                        <div class="input-effect">
                            <input class="primary-input time form-control is-invalid" type="text" name="time" required="">
                            <label>Enter Time</label>
                            <span class="focus-border"></span>
                            @if (session('timeError'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{session('timeError')}}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-auto">
                        <button class="" type="button">
                            <i class="ti-timer"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="wrap-input100 validate-input input-effect">
                <span class="label-input100">Description:</span>
                <textarea class="primary-input form-control" type="text" cols="0" rows="4" name="description" required autocomplete="off"></textarea>
                <label>Enter Description</label>
                <span class="focus-border"></span>
            </div>

            <div class="wrap-input100 validate-input input-effect">
                <span class="label-input100">Meeting Duration:</span>
                <input class="primary-input form-control" type="number" name="duration" required autocomplete="off">
                <label>Enter Duration (minutes)</label>
                <span class="focus-border"></span>
            </div>
            
            <div class="wrap-input100 validate-input input-effect">
                <span class="label-input100">Password:</span>
                <input class="primary-input form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" required autocomplete="off">
                <label>Enter Password</label>
                <span class="focus-border"></span>
                @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            
            <div class="container-contact100-form-btn">
                <button class="contact100-form-btn">
                    <span>
                        Submit
                        <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
                    </span>
                </button>
            </div>
        </form>
        <div style="float: right; padding: 20px">
            <a href="/zoom/settings">Advanced Setting</a>
            <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
        </div>
    </div>
</div>
<script>
    var id, oldId;
    function addStaffFunc(){
        var x = document.getElementById("role_id").value;
        id = x;
        console.log(x);
        document.getElementById("addStaff").style.display = 'block';
        document.getElementById(oldId || x).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
        var inputs = document.querySelectorAll('.uncheck'); 
        for (var i = 0; i < inputs.length; i++) { 
            inputs[i].checked = false; 
        } 
    }
    function addStaffButton(){
        oldId = id;
        document.getElementById(id).style.display = 'block';
        document.getElementById('closeAddStaff').style.display = 'block';
        document.getElementById('addStaff').style.display = 'none';
    }
    function closeAddStaffButton(){
        document.getElementById(id).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
        document.getElementById('addStaff').style.display = 'block';
    }
</script>
@endsection
{{-- /* Tran Thanh Phu end - create a meeting */ --}}