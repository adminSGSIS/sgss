{{-- /* Tran Thanh Phu start - start meeting  */ --}}
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Saigon Star International School</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{url('/')}}/public/uploads/settings/409298f520de7111a06a2bf060f3ca6f.png" type="image/png"/>
    <link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/meetingList.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/backEnd/')}}/vendors/css/bootstrap.css"/>
    <script src="https://source.zoom.us/1.7.9/lib/vendor/jquery.min.js"></script>
</head>
<body>
    <h3 style="text-align: center">Loading...</h3>
    <div id="app"></div>
    <p style="display: none" id="zk">{{env('ZOOM_CLIENT_KEY')}}{{env('ZOOM_CLIENT_SECRET')}}</p>
    @if (Auth::check() == true)
        <script src="{{ URL::asset('Modules/Zoom/public/js/elearning.js') }}" type="text/javascript"></script>
    @else
        <script>
            alert("You are not in this meeting");
            window.location.replace("http://sgstar.asia");
        </script>
    @endif
</body>
</html>
{{-- /* Tran Thanh Phu end - start meeting  */ --}}