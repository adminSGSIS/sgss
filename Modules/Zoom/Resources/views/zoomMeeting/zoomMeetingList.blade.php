{{-- /* Tran Thanh Phu start - meetings list  */ --}}
@extends('backEnd.master')
@section('css')
    <link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/meetingList.css')}}"/>
@endsection
@section('mainContent')

@if (count($zoomMeetings) == 0)
    <div class="alert alert-warning" role="alert">
        You don't have any meeting
    </div> 
@else
<div class="limiter">
    <div class="container-table100">
        <div class="wrap-table100">
            <div class="table100">
                <table>
                    <thead>
                        <tr class="table100-head">
                            <th class="column1">Date</th>
                            <th class="column2">Meeting Id</th>
                            <th class="column3">Topic</th>
                            <th class="column4">Duration</th>
                            <th class="column5">Participants</th>
                            <th class="column6">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($zoomMeetings as $item)
                                <tr>
                                    
                                    <td class="column1">{{$item->start_time}}</td>
                                    <td class="column2">{{$item->meeting_id}}</td>
                                    <td class="column3">{{$item->topic}}</td>
                                    <td class="column4">{{$item->meeting_duration}}</td>
                                    <td class="column5 edit_A_Tag">
                                        <a href="/zoom/detail/{{$item->meeting_id}}">{{$participants[$loop->index]}}</a>
                                    </td>
                                    <td class="column6">
                                        <div class="edit_A_Tag">
                                            @if ($item->created_by == $userId || Auth::user()->id == 1)
                                            <a href="/meeting?nickname={{$username}}&meetingId={{$item->meeting_id}}&password={{$item->password}}&role=1" target="_blank">Join</a>
                                            /
                                            <a style="color: rgb(206, 114, 114) !important" href="/zoom/delete/{{$item->meeting_id}}">Delete</a>
                                            @else
                                            <a href="/meeting?nickname={{$username}}&meetingId={{$item->meeting_id}}&password={{$item->password}}&role=0" target="_blank">Join</a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
{{-- /* Tran Thanh Phu end - meetings list  */ --}}