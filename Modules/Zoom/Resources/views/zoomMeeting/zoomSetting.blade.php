{{-- /* Tran Thanh Phu start - edit setting zoom meeting  */ --}}
@extends('backEnd.master')
@section('css')
    <link rel="stylesheet" href="{{url('Modules/Zoom/Resources/assets/css/zoomMeeting.css')}}"/>
@endsection
@section('mainContent')

<div style="min-height: 0;" class="container-contact100">
    <div class="wrap-contact100">
        <div class="contact100-form-title" style="background-image: url(https://res.cloudinary.com/dtwtxfuug/image/upload/v1597766074/bg-01_lospre.jpg);">
            <span class="contact100-form-title-1">
                ZOOM SETTING
            </span>
        </div>

        <form action="/zoom/settings" method="POST" >
            @csrf
            <li class="list-group-item">
                Start video when the host joins the meeting.
                <label class="switch">
                <input type="checkbox" @if($host_video == 1) checked @endif value="{{!($host_video)}}" name="host_video" class="success">
                <span class="slider round"></span>
                </label>
            </li>
            <li class="list-group-item">
                Start video when participants join the meeting.
                <label class="switch ">
                <input type="checkbox" @if($participant_video == 1) checked @endif name="participant_video" value="{{!($participant_video)}}" class="success">
                <span class="slider round"></span>
                </label>
            </li>
            <li class="list-group-item">
                Allow participants to join the meeting before the host starts the meeting.
                <label class="switch ">
                <input type="checkbox" @if($join_before_host == 1) checked @endif name="join_before_host" value="{{!($join_before_host)}}" class="success">
                <span class="slider round"></span>
                </label>
            </li>
            <li class="list-group-item">
                Mute participants upon entry.
                <label class="switch ">
                <input type="checkbox" @if($mute_upon_entry == 1) checked @endif name="mute_upon_entry" value="{{!($mute_upon_entry)}}" class="success">
                <span class="slider round"></span>
                </label>
            </li>
            <li class="list-group-item">
                Enable waiting room.
                <label class="switch ">
                <input type="checkbox" @if($waiting_room == 1) checked @endif name="waiting_room" value="{{!($waiting_room)}}" class="success">
                <span class="slider round"></span>
                </label>
            </li>
            <li class="list-group-item">
                Determine how participants can join the audio portion of the meeting
                <div style="display: flex; flex-direction: column;">
                    <label class="radio">
                        <input type="radio" name="audio" value="both" @if($audio == 'both') checked @endif>
                        <span>Both Telephony and VoIP.</span>
                    </label>
                    <label class="radio">
                        <input type="radio" name="audio" value="telephony" @if($audio == 'telephony') checked @endif>
                        <span>Telephony only.</span>
                    </label>
                    <label class="radio">
                        <input type="radio" name="audio" value="voip" @if($audio == 'voip') checked @endif>
                        <span>VoIP only.</span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                Automatic recording
                <div style="display: flex; flex-direction: column;">
                    <label class="radios">
                        <input type="radio" name="auto_recording" value="local" @if($auto_recording == 'local') checked @endif>
                        <span>Record on local.</span>
                    </label>
                    <label class="radio">
                        <input type="radio" name="auto_recording" value="cloud" @if($auto_recording == 'cloud') checked @endif>
                        <span>Record on cloud.</span>
                    </label>
                    <label class="radio">
                        <input type="radio" name="auto_recording" value="none" @if($auto_recording == 'none') checked @endif>
                        <span>None.</span>
                    </label>
                </div>
            </li>
            <li class="list-group-item">
                Approval Type
                <div style="display: flex; flex-direction: column;">
                    <label class="radios">
                        <input type="radio" name="approval_type" value=0 @if($approval_type == 0) checked @endif>
                        <span>Automatically approve.</span>
                    </label>
                    <label class="radio">
                        <input type="radio" name="approval_type" value=1 @if($approval_type == 1) checked @endif>
                        <span>Manually approve.</span>
                    </label>
                    <label class="radio">
                        <input type="radio" name="approval_type" value=2 @if($approval_type == 2) checked @endif>
                        <span>No registration required.</span>
                    </label>
                </div>
            </li>
            
            <div  class="container-contact100-form-btn mb-2" style="width: 110px; margin: auto">
                <button class="contact100-form-btn">
                    <span>
                        Save
                        <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
                    </span>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
{{-- /* Tran Thanh Phu end - edit setting zoom meeting  */ --}}