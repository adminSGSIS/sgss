<?php

// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('zoom')->group(function() {
//     Route::get('/', 'ZoomController@index');
// });

Route::prefix('zoom')->group(function () {
    Route::name('zoom.')->group(function () {
        Route::get('about', 'MeetingController@about');

        /*Tran Thanh Phu - meeting online*/
        Route::get('meetings', 'MeetingController@index');
        Route::post('meetings', 'MeetingController@createMeeting');
        Route::get('delete/{id}', 'MeetingController@deleteMeeting');
        Route::get('meetings-list', 'MeetingController@meetingList');
        Route::get('detail/{id}', 'MeetingController@detailParticipants');
        Route::post('/detail/{meeting_id}/{id}', 'MeetingController@deleteParticipant');
        Route::get('settings', 'SettingController@settings')->name('settings');
        Route::post('settings', 'SettingController@updateSettings')->name('settings.update');
        /*Tran Thanh Phu - meeting online*/
        
        Route::get('meeting-room/{id}', 'VirtualClassController@meetingStart')->name('virtual-class.join');
        Route::get('virtual-class-room/{id}', 'MeetingController@meetingStart')->name('meeting.join');
        Route::get('user-list-user-type-wise', 'MeetingController@userWiseUserList')->name('user.list.user.type.wise');
        Route::get('virtual-class-reports', 'ReportController@report')->name('virtual.class.reports.show');
        Route::get('meeting-reports', 'ReportController@meetingReport')->name('meeting.reports.show');

        /*Pham Trong Hai*/
        Route::resource('virtual-class', 'VirtualClassController');
        Route::get('meeting-start/{id}','VirtualClassController@meetingStart');
        Route::get('list-virtual-class','VirtualClassFrontEndController@index');
        Route::get('join-virtual-class/{id}/{type}','VirtualClassFrontEndController@join');
        /*Pham Trong Hai*/

        /*Hải thêm*/
        Route::get('list-virtual-class','VirtualClassFrontEndController@index');
        //Route::get('join-virtual-class/{id}/{type}','VirtualClassFrontEndController@join');
        Route::get('login-zoom/{id}/{type}','VirtualClassFrontEndController@getVclassLogin');
        Route::post('login-zoom','VirtualClassFrontEndController@postVclassLogin');
        /*Hải thêm*/
    });
});
Route::get('/meeting', function () {
    return view('zoom::zoomMeeting.startMeeting');
});
