<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcceptUsePolicy extends Model
{
    protected $table = 'accept_use_policy';

    protected $guarded = [];

    public function class()
    {
        return $this->belongsTo('App\SmClass');
    }
}
