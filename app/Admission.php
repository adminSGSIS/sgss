<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    protected $fillable = [
        "student_id",
        "parent_id",
        "particular_interests",
        "talents_or_abilities",
        "trial_from_date",
        "trial_to_date",
        "trial_class_id",
        "trial_teacher",
        "school_history_name",
        "school_history_city",
        "school_history_from_date",
        "school_history_to_date",
        "school_history_language_instruction",
        "school_history_withdrawal_reason",
        "health_information_1",
        "health_information_2",
        "health_information_3",
        "health_information_4",
        "health_information_5",
        "additional_needs",
        "bus_service_request",
        "lunch_request",
        "eal_support",
        "sen_support",
        "payer_name",
        "payer_company",
        "payer_address",
        "payer_email",
        "payer_mobile",
        "payer_require_vat_invoice",
        "emergency_contact_name",
        "emergency_contact_family_name",
        "emergency_relationship",
        "emergency_contact_mobile_1",
        "emergency_contact_mobile_2",
        "emergency_email",
        "emergency_contact_address",
        'home_adress_in_hcm'
    ];

    public function class()
    {
        return $this->belongsTo('App\SmClass', 'trial_class_id');
    }

    public function student()
    {
        return $this->belongsTo('App\SmStudent');
    }

    public function teacher()
    {
        return $this->belongsTo('App\User', 'trial_teacher', 'id');
    }
}
