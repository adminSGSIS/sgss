<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use Modules\CRM\Entities\CustomerFacebook;

class customerInformation extends Conversation
{
    protected $phone_number;

    protected $child_age;

    public function askPhoneNumber()
    {
        $this->ask('Enter phone number', function(Answer $answer) {
            $this->phone_number = $answer->getText();
            if(!preg_match("/^[0-9]{8,11}$/", $this->phone_number)){
                $this->say('Please enter phone number again');
                $this->askPhoneNumber();
                return;
            }
            $this->askChildrenAge();
        });
    }
    
    public function askChildrenAge()
    {
        $this->ask('One more thing - how old is your child?', function(Answer $answer) {
            $this->child_age = $answer->getText();
            if(!preg_match("/^[0-9]+$/", $this->child_age)){
                $this->say('Please enter age again');
                $this->askChildrenAge();
                return;
            }
            $customer = CustomerFacebook::where('fb_id', session('Fb'))->first();
            $customer->child_age = $this->child_age;
            $customer->phone_number = $this->phone_number;
            $customer->save();
            $this->say('Thanks, that is all we need, we will contact you soon');
        });
    }

    public function run()
    {
        $this->say('Hey, let me ask you something');
        $this->say('First I wanna ask for your phone number');
        $this->askPhoneNumber();
    }
}