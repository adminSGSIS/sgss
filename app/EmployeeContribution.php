<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeContribution extends Model
{
    public $table = "employees_contribution";
}
