<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployerExpense extends Model
{
    public $table = "employer_expenses";
}
