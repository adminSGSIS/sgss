<?php

namespace App\Exports;

use App\MedicalDeclarationDomestic;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MedicalDomesticExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return MedicalDeclarationDomestic::select(
            'full_name',
            'date_of_birth',
            'nationality',
            'email',
            'traveled',
        )->latest()->get();
    }

    public function headings(): array
    {
        return [
            'FULL NAME', 'DATE OF BIRTH', 'NATIONALITY', 'EMAIL', 'LAST 14 DAYS'
        ];
    }
}
