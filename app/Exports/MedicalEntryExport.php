<?php

namespace App\Exports;

use App\MedicalDeclarationEntry;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MedicalEntryExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return MedicalDeclarationEntry::select(
            'full_name',
            'date_of_birth',
            'nationality',
            'email',
            'past_14_days',
        )->latest()->get();
    }

    public function headings(): array
    {
        return [
            'FULL NAME', 'DATE OF BIRTH', 'NATIONALITY', 'EMAIL', 'LAST 14 DAYS'
        ];
    }
}
