<?php

namespace App\Exports;

use App\MedicalDeclarationEntry;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\Medical\Entities\Patient;

class patientExportExcel implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Patient::select(
            'id',
            'patient_id',
            'patient_name',
            'date_of_birth',
            'gender',
            'address',
            'date',
            'case',
            'temperaturte',
            'respiratory',
            'symtome_type',
            'symtome_describe',
            'old_patient',
            'consultant',
            'hospital',
            'patient_type'
        )->latest()->get();
    }

    public function headings(): array
    {
        return [
            'Id', 'Patient id','Patient name', 'Date of birth', 'Gender', 'Address','Date','Case','Temperaturte',
            'Respiratory','symtome Type','Symtome Describe','Old Patient',' consultant','hospital','patient_type'
        ];
    }
}
