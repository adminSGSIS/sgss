<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class FbCustomer extends Model {
  protected $fillable = [
    "fb_id",
    "name",
    "nickname",
    "email",
    "avatar",
    "profile"
  ];
}
