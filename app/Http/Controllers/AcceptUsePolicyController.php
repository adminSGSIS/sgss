<?php

namespace App\Http\Controllers;

use App\SmClass;
use App\AcceptUsePolicy;
use Barryvdh\DomPDF\Facade as PDF;


class AcceptUsePolicyController extends Controller
{
    public function create()
    {
        $classes = SmClass::all()->sortBy('stt');
        return view('frontEnd.home.acceptable_use_policy', compact('classes'));
    }

    public function store()
    {
        request()->validate(['class_id' => 'required']);
        $data = AcceptUsePolicy::create(request()->all());
        $pdf = PDF::loadView('pdf.acceptable_use_policy_pdf', compact('data'));
        return $pdf->download('acceptable_use_policy_pdf.pdf');
    }
}
