<?php

namespace App\Http\Controllers;

use App\User;
use App\SmClass;
use App\SmRoute;
use App\SmParent;
use App\Admission;
use App\SmStudent;
use App\SmBaseSetup;
use App\SmAcademicYear;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Assessment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\SmStaff;
use App\SmGeneralSettings;

class AdmissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create()
    {
        $proficient = ['Native', 'Fluent', 'Confidence', 'Gaining Confidence', 'Beginer'];
        $bus_service = SmRoute::all();
        $_count = SmStudent::count();
        $admission_no = substr(date('Y'), 2, 2).'01'.str_pad($_count + 1, 4, '0', STR_PAD_LEFT);
        $roll_id = SmStudent::max('roll_no') + 1;
        $classes = SmClass::all();
        $sessions = SmAcademicYear::where('year', 'like', '%' . date("Y") . '%')->get();
        $parents = User::where('role_id', 3)->get();
        $genders = SmBaseSetup::where('base_group_id', '=', '1')->get();
        return view('backEnd.studentInformation.student_admission', compact('classes', 'sessions', 'admission_no', 'roll_id', 'proficient', 'bus_service', 'parents', 'genders'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'academic_id' => 'required',
            'class_id' => 'required',
            'section_id' => 'required',
            'student_photo' => 'required|image',
            'emergency_email' => 'required|unique:admissions',
        ]);

        DB::transaction(function () {
            if (request('check_parent') == 'find') {
                $parent = SmParent::where('user_id', request('choose_parent_id'))->first();
            } else {
                $user_parent = User::create([
                    'role_id' => 3,
                    'full_name' => request('emergency_contact_name'),
                    'email' => request('emergency_email'),
                    'username' => request('emergency_email'),
                    'password' => Hash::make(123456),
                ]);
                
                $parent = SmParent::create(request()->all() + ['user_id' => $user_parent->id]);
            }

            $user_student = User::create([
                'role_id' => 2,
                'full_name' => request('first_name') . " " . request('last_name'),
                'email' => request('admission_no') . '@sgstar.edu.vn',
                'username' => request('admission_no') . '@sgstar.edu.vn',
                'password' => Hash::make(123456),
            ]);

            $student = SmStudent::create(request()->all() + [
                'user_id' => $user_student->id,
                'parent_id' => $parent->id,
                'full_name' => request('first_name') . " " . request('last_name'),
                'role_id' => 2,
                'session_id' => request('academic_id')
            ]);

            Admission::create(request()->all() + [
                'student_id' => $student->id,
                'parent_id' => $parent->id
            ]);
        });
        Toastr::success('Operation successful', 'Success');
        return back();
    }

    public function edit(SmStudent $student)
    {
        $proficient = ['Native', 'Fluent', 'Confidence', 'Gaining Confidence', 'Beginer'];
        $bus_service = SmRoute::all();
        $genders = SmBaseSetup::where('base_group_id', '=', '1')->get();
        $_count = SmStudent::count();
        $admission_no = substr(date('Y'), 2, 2).'01'.str_pad($_count + 1, 4, '0', STR_PAD_LEFT);
        $roll_id = SmStudent::max('roll_no') + 1;
        $classes = SmClass::all();
        $sessions = SmAcademicYear::where('year', 'like', '%' . date("Y") . '%')->get();
        return view('backEnd.studentInformation.student_edit', compact('classes', 'sessions', 'admission_no', 'roll_id', 'proficient', 'student', 'bus_service', 'genders'));
    }

    public function update(Request $request)
    {
        //dd($request);
        request()->validate([
            'academic_id' => 'required',
            'class_id' => 'required',
            'section_id' => 'required',
        ]);

        $student = SmStudent::find($request->student_id);
        
        $student->route_list_id = $request->bus_service_request;
        $student->lunch_request = $request->have_lunch_request;
        if ($request->file('student_img') != "") {
            $file = $request->file('student_img');
            $image = $student->id.'-'.$student->admission_no ."." . $file->getClientOriginalExtension();
            $file->move('public/uploads/student/', $image);
            $image = 'public/uploads/student/' . $image;
            $student->student_photo = $image;
        }
        $student->save();
        
        
        /* if (request('student_photo') != "" && file_exists($request->student_photo)) {
             unlink($student->student_photo);
         }*/

        $student->update(request()->all() + [
            'full_name' => request('first_name') . " " . request('last_name'),
            'session_id' => request('academic_id'),
           
        ]);
        if($student->admission != null) /* if student have admission */
        {
            $student->admission->update(request()->all());
        }
        
        $student->parent->update(request()->all());
        Toastr::success('Operation successful', 'Success');
        return back();
    }

    public function trialList()
    {
        
        $studentIds = Admission::where('trial_teacher_id', auth()->user()->staff->id)->pluck('student_id');
        
        if(Auth::user()->staff->designations->id == 13 || Auth::user()->staff->designations->id == 14 || Auth::user()->staff->designations->id == 45 || Auth::user()->id == 1||Auth::user()->staff->designations->id == 4)
        {
            $students =  SmStudent::where('is_trial', 1)->get();
        }
        else
            $students =  SmStudent::where('is_trial', 1)->whereIn('id', $studentIds)->get();
        return view('backEnd.studentInformation.trial-list', compact('students'));
    }
    
    public function feedback(SmStudent $student)
    {
        $teacher_name = null;
        if($student->admission->trial_teacher_id == "0")
        {
            $teacher_name = "";
        }
        else{
            $staff=SmStaff::find($student->admission->trial_teacher_id);
            $teacher_name = $staff->full_name;
        }
        
        $check = $student->admission->trial_teacher_id == auth()->user()->staff->id || auth()->user()->staff->designation_id == 13||auth()->user()->staff->designation_id == 14||auth()->user()->staff->designation_id == 45||auth()->user()->staff->designation_id == 1||auth()->user()->staff->designation_id == 1||auth()->user()->staff->designation_id ==4;
        abort_if($student->is_trial == 0, 404);
        if(!$check){
            return redirect()->route('dashboard');
        };
        $check_academic=$student->class_id==1||$student->class_id==21||$student->class_id==23||$student->class_id==22||$student->class_id==32;
        $staffId=SmStaff::findOrFail($student->admission->trial_teacher_id);
        $parent=SmParent::where('id',$student->parent_id)->first();
        if(!$check_academic)
            return view('backEnd.studentInformation.feedback', compact('student','teacher_name'),compact('parent'));
            else
            return view('backEnd.studentInformation.feedback_v1',compact('student','teacher_name'),compact('parent')); 
                 
    }

    public function feedbackStore(SmStudent $student)
    {
        $teacher_name = null;
        
        if($student->admission->trial_teacher_id == "0")
        {
            $teacher_name = "";
        }
        else{
            $staff=SmStaff::find($student->admission->trial_teacher_id);
            $teacher_name = $staff->full_name;
        }
        $parent=SmParent::where('id',$student->parent_id)->first();
        $student->assessment->update(request()->all());
        $check_academic=$student->class_id==1||$student->class_id==21||$student->class_id==22||$student->class_id==23||$student->class_id==32;

        Toastr::success('Operation successful', 'Success');
        if(request()->all()!=Null)
        {
            $this->isPass($student);
        }
        
        if(auth()->user()->staff->id==17)
        {
            $data=url("feedback").'/'.$student->id;
                    Mail::send('frontEnd.mails.feedback_mail', compact('data'), function ($message) {
                        $message->to('admissions@sgstar.edu.vn')->subject('Feedback');
                    });               
        }       
        return back();
    }

    public function isPass(SmStudent $student)
    {
        if(($student->assessment->quantitative<85||$student->assessment->verbal<85||$student->assessment->non_verbal<85||$student->assessment->spatia_l<85||
        $student->assessment->listen<85||$student->assessment->speaking<85||$student->assessment->reading<85||$student->assessment->writing<85)
        && $student->assessment->eal_score!=Null && auth()->user()->staff->designation_id == 14)
        {
            $student->assessment->mail_status=1;                               
            $data = url("feedback").'/'.$student->id;
            Mail::send('frontEnd.mails.sen_mail', compact('data'), function ($message) {
                $setting = SmGeneralSettings::find(1);
                $email = $setting->email;
                $message->to($email)->subject('Feedback');
            });
            $student->assessment->save();
        }
    }

    public function isTrialStatus(SmStudent $student)
    {
        $student->update(['is_trial' => ($student->is_trial == 1) ? 0 : 1]);
        return response()->json([$student]);
    }
    
    
}
