<?php

namespace App\Http\Controllers;

use App\SmClass;
use App\SmClassRoom;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use App\Conversations\customerInformation;

class BotManController extends Controller
{
  public function handle()
  {
    $botman = app('botman');

    $botman->hears('I want ([0-9]+)', function ($bot, $number) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $bot->reply('You will get: ' . $number);
    });

    $botman->hears('.*class|classes.*', function ($bot) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $bot->reply('Here, list of classes');
      
      // getting classes
      $reply = "";
      foreach (SmClass::get() as $i) {
        $reply .= $i->class_name."<br><br>";
      }
      $bot->reply($reply);
    });

    $botman->hears('logo', function ($bot) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $attachment = new Image('/public/landing/images/logo.png');
      $message = OutgoingMessage::create('Saigon star logo')->withAttachment($attachment);
      $bot->reply($message);
    });

    $botman->hears('about school|school', function ($bot) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $attachment = new Image('/public/images/img_mission.png');
      $message = OutgoingMessage::create('Saigon star logo')->withAttachment($attachment);
      $bot->reply($message);
    });

    $botman->hears('contact|phone|phone number', function ($bot) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $bot->reply('Contact number: +84 888 006 996');
    });

    $botman->fallback(function ($bot) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $bot->reply('Sorry, I did not understand these messages');
      $bot->startConversation(new customerInformation);
    });

    $botman->hears('image attachment', function ($bot) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $attachment = new Image('https://botman.io/img/logo.png');
      $message = OutgoingMessage::create('This is my text')
      ->withAttachment($attachment);
      $bot->reply($message);
    });

    $botman->hears('hello|hi', function ($bot) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $bot->reply("Hi, nice to meet you!");
      $bot->startConversation(new customerInformation);
    });

    $botman->hears('room|rooms|class room', function ($bot) {
      if (!session('Fb')) {
        $bot->reply('<a class="btn btn-primarytei" target="_blank" href="/auth/fb">Login with Facebook</a>');
        return;
      }
      $bot->reply('Here, list of rooms');
      
      $reply = "";
      foreach (SmClassRoom::get() as $i) {
        $reply .= $i->room_no."<br><br>";
      }
      $bot->reply($reply);
    });

    $botman->listen();
  }
}
