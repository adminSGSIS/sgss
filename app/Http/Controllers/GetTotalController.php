<?php

namespace App\Http\Controllers;

use App\SmParent;
use App\SmStaff;
use App\SmStudent;

class GetTotalController extends Controller
{
    public function students()
    {
        $students = SmStudent::latest()->get();
        return view('backEnd.get_total.students', compact('students'));
    }

    public function parents()
    {
        $parents = SmParent::latest()->get();
        return view('backEnd.get_total.parents', compact('parents'));
    }

    public function teachers()
    {
        $teachers = SmStaff::where('role_id', 4)->latest()->get();
        return view('backEnd.get_total.teachers', compact('teachers'));
    }
}
