<?php

namespace App\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use App\SmContacts; // Anh 17-09
use App\SmStrength;
use App\SmTestimonial;
use Illuminate\Http\Request;

use App\YearCheck;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\SmGeneralSettings;
class LandingController extends Controller
{

    public function __construct()
	{
        $this->middleware('PM');
        // User::checkAuth();
	}
    // public function LandingPage()
    // {

    //     try {
    //         return view('frontEnd.landing.index');
    //     } catch (\Exception $e) {
    //         Toastr::error('Operation Failed', 'Failed');
    //         return redirect()->back();
    //     }
    // }
     /* >>>>>>>>>>>>>>>>>>> Cao Văn Anh Start <<<<<<<<<<<<<<<<<<< */
   public function LandingPage(){
        $strength = SmStrength::get();
        $testimonials = SmTestimonial::get();
        return view('frontEnd.landing.index',compact('strength','testimonials'));
   }
    public function LandingPageStore(Request $request){
       
        try {
            $contact_message = new SmContacts();
            $contact_message->name = $request->name;
            $contact_message->email = $request->email;
            $contact_message->phone_number = $request->phone;
            $result = $contact_message->save();
           
              
              
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['phone_number'] = $request->phone_number;
            $data['subject'] = 'Registration';

            Mail::send('frontEnd.contact_mail', compact('data'), function ($message) use ($request) {

                $setting = SmGeneralSettings::find(1);
                $email = $setting->email;
                $school_name = $setting->school_name;
                $message->from($email, $school_name);
                $message->to('admissions@sgstar.edu.vn')->subject('Registration');
                
            });
            if($result){
                Toastr::success('Operation successful', 'Success');
                return redirect()->back()->with('message-success', 'Message send successfully');
            }else{
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }
    public function LandingPageVi(){
        $strength = SmStrength::get();
        $testimonials = SmTestimonial::get();
        return view('frontEnd.landing.index_ver2',compact('strength','testimonials'));
   }
    public function index()
    {
        try{
            $strength = SmStrength::get();
            return view('backEnd.landing.strength',compact('strength'));

        }catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'title_vn' => 'required',
            'content' => 'required',
            'content_vn' => 'required',
           
        ]);
        try {
            $strength = new SmStrength();

            $strength->title = $request->title;
            $strength->title_vn = $request->title_vn;
            $strength->content = $request->content;
            $strength->content_vn = $request->content_vn;
            $result = $strength->save();
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function edit($id)
    {

        try {
            $strength = SmStrength::get();
            $add_strength = SmStrength::find($id);
            return view('backEnd.landing.strength', compact('strength', 'add_strength'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }


    public function update(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'title_vn' => 'required',
            'content' => 'required',
            'content_vn' => 'required',
            
        ]);
        try {
            $strength = SmStrength::find($request->id);   
            
            $strength->title = $request->title;
            $strength->title_vn = $request->title_vn;
            $strength->content = $request->content;
            $strength->content_vn = $request->content_vn;
         
            $result = $strength->save();
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function forDeleteStrength($id)
    {
        try {
            return view('backEnd.landing.delete_modal', compact('id'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        try {
            $strength = SmStrength::find($id);
            $result = $strength->delete();
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back();
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
}
