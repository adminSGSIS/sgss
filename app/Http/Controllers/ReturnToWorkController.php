<?php

namespace App\Http\Controllers;

use App\ReturnToWork;
use Illuminate\Support\Str;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;

class ReturnToWorkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $list = ReturnToWork::all();
        return view('backEnd.humanResource.return-to-work.index', compact('list'));
    }

    public function create()
    {
        $token = Str::random(100);
        return view('backEnd.humanResource.return-to-work.create', compact('token'));
    }

    public function store()
    {
        try {
            $returnToWork = ReturnToWork::create(request()->all());
            $data = [
                'name' => request('name'),
                'url' => url('/return-to-work/approve').'/'.$returnToWork->id.'?token='.request('token'),
            ];
            Mail::send('backEnd.humanResource.return-to-work.manager_mail', compact('data'), function ($message) {
                $message->to(request('manager_email'))->subject('Please check form return from '.request('name'));
            });
            Toastr::success('Operation successful', 'Success');
            return back();
        } catch (\Exception $th) {
            Toastr::error('Operation error', 'Error');
            return back();
        }
    }

    public function show(ReturnToWork $returnToWork)
    {
        return view('backEnd.humanResource.return-to-work.show', compact('returnToWork'));
    }

    public function update(ReturnToWork $returnToWork)
    {
        try {
            $returnToWork->update(request()->all() + ['approve_status' => 0]);
            $data = [
                'name' => request('name'),
                'url' => url('/return-to-work/approve').'/'.$returnToWork->id.'?token='.request('token'),
            ];
            $email = request('manager_email');
            Mail::send('backEnd.humanResource.return-to-work.manager_mail', compact('data'), function ($message) use ($email) {
                $message->to(request('manager_email'))->subject('Please check form return from '.request('name'));
            });
            Toastr::success('Operation successful', 'Success');
            return back();
        } catch (\Exception $th) {
            Toastr::error('Operation error', 'Error');
            return back();
        }
    }
    
    public function delete(ReturnToWork $returnToWork)
    {
        $returnToWork->delete();
        return back();
    }

    public function approve(ReturnToWork $returnToWork)
    {
        if ($returnToWork->token == request('token')) {
            return view('backEnd.humanResource.return-to-work.approve', compact('returnToWork'));
        }
        abort(401);
    }

    public function approved(ReturnToWork $returnToWork)
    {
        $returnToWork->update(request()->all() + ['approve_status' => 1]);

        $data = [
            'start_date' => now()->format('d/m/Y')
        ];

        $email = $returnToWork->email;
        Mail::send('backEnd.humanResource.return-to-work.employee_mail', compact('data'), function ($message) use ($email) {
            $message->to($email)->subject('Saigon Star Notification');
        });

        Toastr::success('Operation successful', 'Success');
        return redirect('/return-to-work');
    }
}
