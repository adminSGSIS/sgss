<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use App\SmStaff;
use App\SmPayrollVn;
use App\SmPayrollForeigner;
use App\SmDeduct;
use App\EmployeeContribution;
use App\EmployerExpense;
use Carbon;
use App\GlobalVariable;
use App\SmGeneralSettings;
use App\SmEmailSetting;
use Illuminate\Contracts\Mail\Mailer;

class SalaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('PM');
    }

    public function salaryVN()
    {
        try {
            if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
            $except = SmPayrollVn::where('month_year', Carbon::now()->format('F Y'))->pluck('staff_id')->toArray();
            $except[] = 1;
            $staffs = SmStaff::all()->except($except);
            return view('backEnd.accounts.salary_vn', compact('staffs'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function storeSalaryVN(Mailer $mailer)
    {
        $employer_expense = new EmployerExpense();
        $employer_expense->social_ins = request('social_ins_employer_expense');
        $employer_expense->health_ins = request('health_ins_employer_expense');
        $employer_expense->unemployee_ins = request('unemployee_ins_employer_expense');
        $employer_expense->trade_union = request('trade_union_employer_expense');
        $employer_expense->total = request('total_employer_expense');
        $employer_expense->save();

        $employee_contribution = new EmployeeContribution();
        $employee_contribution->social_ins = request('social_ins_employees_contribute');
        $employee_contribution->health_ins = request('health_ins_employees_contribute');
        $employee_contribution->unemployee_ins = request('unemployee_ins_employees_contribute');
        $employee_contribution->union_fees = request('union_fees_employees_contribute');
        $employee_contribution->total = request('total_employees_contribute');
        $employee_contribution->save();

        $deduct = new SmDeduct();
        $deduct->personal = request('personal');
        $deduct->dependent = request('dependent');
        $deduct->deduction_dependent = request('deduction_dependent');
        $deduct->total = request('total_deduction');
        $deduct->save();

        $payroll_vn = new SmPayrollVn();
        $payroll_vn->staff_id = request('staff_id');
        $payroll_vn->employer_expense_id = $employer_expense->id;
        $payroll_vn->employee_contribution_id = $employee_contribution->id;
        $payroll_vn->deduction_id = $deduct->id;
        $payroll_vn->basic_salary = request('basic_salary');
        $payroll_vn->responsible_allowance = request('responsible_allowance');
        $payroll_vn->allowance = request('allowance');
        $payroll_vn->bus_duty_allowance = request('bus_duty_allowance');
        $payroll_vn->pre_sen_eca = request('pre_sen_eca');
        $payroll_vn->other_allowance = request('other_allowance');
        $payroll_vn->total_salary = request('total_salary');
        $payroll_vn->working_day = request('working_day');
        $payroll_vn->hours_before_convert = request('hours_before_convert');
        $payroll_vn->hours_after_convert = request('hours_after_convert');
        $payroll_vn->month_salary = request('month_salary');
        $payroll_vn->contribute_insurance_salary = request('contribute_insurance_salary');
        $payroll_vn->assessable_income = request('assessable_income');
        $payroll_vn->pit = request('pit');
        $payroll_vn->net_salary = request('net_salary');
        $payroll_vn->month_year = Carbon::now()->format('F Y');
        $payroll_vn->save();

        if (request()->has('send')) {
            $systemSetting = SmGeneralSettings::select('school_name', 'email')->where('school_id', Auth::user()->school_id)->find(1);
            $systemEmail = SmEmailSetting::find(1);

            $system_email = $systemEmail->from_email;
            $school_name = $systemSetting->school_name;

            $data['email_sms_title'] = "New Payroll Information";
            $data['system_email'] = $system_email;
            $data['school_name'] = $school_name;
            $data['from'] = Auth::user()->full_name;
            $data['description'] = "Please click ".url('salary/me/search')." to see";
            $data['files'] = [];
            $details = $payroll_vn->staff->email;

            $mailer->send('backEnd.emails.mail', ['data'=> $data], function ($message) use ($data, $details) {
                $message->from($data['system_email'], $data['school_name']);
                    
                $message->to($details)->subject($data['email_sms_title']);
            });
        }
            
        return redirect()->back();
        try {
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function salaryForeigner()
    {
        try {
            if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
            $except = SmPayrollForeigner::where('month_year', Carbon::now()->format('F Y'))->pluck('staff_id')->toArray();
            $except[] = 1;
            $staffs = SmStaff::where('role_id', '4')->get()->except($except);
            return view('backEnd.accounts.salary_foreigner', compact('staffs'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function storeSalaryForeigner(Mailer $mailer)
    {
        try {
            $deduct = new SmDeduct();
            $deduct->personal = request('personal');
            $deduct->dependent = request('dependent');
            $deduct->deduction_dependent = request('deduction_dependent');
            $deduct->total = request('total_deduction');
            $deduct->save();

            $payroll_foreigner = new SmPayrollForeigner();
            $payroll_foreigner->staff_id = request('staff_id');
            $payroll_foreigner->deduction_id = $deduct->id;
            $payroll_foreigner->basic_salary = request('basic_salary');
            $payroll_foreigner->allowance = request('allowance');
            $payroll_foreigner->total_salary = request('total_salary');
            $payroll_foreigner->working_day = request('working_day');
            $payroll_foreigner->month_salary = request('month_salary');
            $payroll_foreigner->contribute_insurance_salary = request('contribute_insurance_salary');
            $payroll_foreigner->assessable_income = request('assessable_income');
            $payroll_foreigner->health_ins_employer = request('health_ins_employer');
            $payroll_foreigner->health_ins_employee = request('health_ins_employee');
            $payroll_foreigner->pit = request('pit');
            $payroll_foreigner->net_salary = request('net_salary');
            $payroll_foreigner->month_year = Carbon::now()->format('F Y');
            $payroll_foreigner->save();

            if (request()->has('send')) {
                $systemSetting = SmGeneralSettings::select('school_name', 'email')->where('school_id', Auth::user()->school_id)->find(1);
                $systemEmail = SmEmailSetting::find(1);
    
                $system_email = $systemEmail->from_email;
                $school_name = $systemSetting->school_name;
    
                $data['email_sms_title'] = "New Payroll Information";
                $data['system_email'] = $system_email;
                $data['school_name'] = $school_name;
                $data['from'] = Auth::user()->full_name;
                $data['description'] = "Please click ".url('salary/me/search')." to see";
                $data['files'] = [];
                $details = $payroll_foreigner->staff->email;
                $details = "thanhphu2410@gmail.com";
    
                $mailer->send('backEnd.emails.mail', ['data'=> $data], function ($message) use ($data, $details) {
                    $message->from($data['system_email'], $data['school_name']);
                        
                    $message->to($details)->subject($data['email_sms_title']);
                });
            }
            return redirect()->back();
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function listSalaryOption()
    {
        if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
        return view('backEnd.accounts.salary_list_option');
    }

    public function listSalarySearch()
    {
        if (request('vn_foreigner') == 'select' || request('month_year') == 'select') {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
        if (request('vn_foreigner') == '1') {
            return redirect('/salary/list/vn/'.request('month_year'));
        }
        if (request('vn_foreigner') == '2') {
            return redirect('/salary/list/foreigner/'.request('month_year'));
        }
    }

    public function listSalaryVn($month_year)
    {
        try {
            if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
            $paginate = SmPayrollVn::where('month_year', $month_year)->paginate(10);
            $payroll_vn = $paginate->sortBy('staff.role_id');
            $td_length = $payroll_vn->count();
            $temp = 0;
            foreach ($payroll_vn as $value) {
                if ($value->staff->role_id != $temp) {
                    $td_length += 1;
                }
                $temp = $value->staff->role_id;
            }
            $temp = 0;
            return view('backEnd.accounts.salary_list_vn', compact('payroll_vn', 'temp', 'td_length', 'paginate'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function listSalaryForeigner($month_year)
    {
        try {
            if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
            $paginate = SmPayrollForeigner::where('month_year', request('month_year'))->paginate(10);
            $payroll_foreigner = $paginate->sortBy('staff.role_id');
            $td_length = $payroll_foreigner->count();
            return view('backEnd.accounts.salary_list_foreigner', compact('payroll_foreigner', 'td_length', 'paginate'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function editSalaryVn($id)
    {
        try {
            if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
            $payroll_vn = SmPayrollVn::findOrFail($id);
            return view('backEnd.accounts.edit_salary_vn', compact('payroll_vn'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function updateSalaryVn($id)
    {
        try {
            $payroll_vn = SmPayrollVn::find($id);

            $employer_expense =  $payroll_vn->employer_expense;
            $employer_expense->social_ins = request('social_ins_employer_expense');
            $employer_expense->health_ins = request('health_ins_employer_expense');
            $employer_expense->unemployee_ins = request('unemployee_ins_employer_expense');
            $employer_expense->trade_union = request('trade_union_employer_expense');
            $employer_expense->total = request('total_employer_expense');
            $employer_expense->save();

            $employee_contribution = $payroll_vn->employee_contribution;
            $employee_contribution->social_ins = request('social_ins_employees_contribute');
            $employee_contribution->health_ins = request('health_ins_employees_contribute');
            $employee_contribution->unemployee_ins = request('unemployee_ins_employees_contribute');
            $employee_contribution->union_fees = request('union_fees_employees_contribute');
            $employee_contribution->total = request('total_employees_contribute');
            $employee_contribution->save();

            $deduct = $payroll_vn->deduction;
            $deduct->personal = request('personal');
            $deduct->dependent = request('dependent');
            $deduct->deduction_dependent = request('deduction_dependent');
            $deduct->total = request('total_deduction');
            $deduct->save();

            $payroll_vn->basic_salary = request('basic_salary');
            $payroll_vn->responsible_allowance = request('responsible_allowance');
            $payroll_vn->allowance = request('allowance');
            $payroll_vn->bus_duty_allowance = request('bus_duty_allowance');
            $payroll_vn->pre_sen_eca = request('pre_sen_eca');
            $payroll_vn->other_allowance = request('other_allowance');
            $payroll_vn->total_salary = request('total_salary');
            $payroll_vn->working_day = request('working_day');
            $payroll_vn->hours_before_convert = request('hours_before_convert');
            $payroll_vn->hours_after_convert = request('hours_after_convert');
            $payroll_vn->month_salary = request('month_salary');
            $payroll_vn->contribute_insurance_salary = request('contribute_insurance_salary');
            $payroll_vn->assessable_income = request('assessable_income');
            $payroll_vn->pit = request('pit');
            $payroll_vn->net_salary = request('net_salary');
            $payroll_vn->save();
            return redirect()->back();
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function deleteSalaryVn($id)
    {
        try {
            if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
            $payroll_vn = SmPayrollVn::findOrFail($id);
            $payroll_vn->deduction->delete();
            $payroll_vn->employee_contribution->delete();
            $payroll_vn->employer_expense->delete();
            $payroll_vn->delete();
            return redirect('/salary/vn');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function editSalaryForeigner($id)
    {
        try {
            if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
            $payroll_foreigner = SmPayrollForeigner::findOrFail($id);
            return view('backEnd.accounts.edit_salary_foreigner', compact('payroll_foreigner'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function updateSalaryForeigner($id)
    {
        try {
            $payroll_foreigner = SmPayrollForeigner::find($id);
            $deduct = $payroll_foreigner->deduction;
            $deduct->personal = request('personal');
            $deduct->dependent = request('dependent');
            $deduct->deduction_dependent = request('deduction_dependent');
            $deduct->total = request('total_deduction');
            $deduct->save();

            $payroll_foreigner->basic_salary = request('basic_salary');
            $payroll_foreigner->allowance = request('allowance');
            $payroll_foreigner->total_salary = request('total_salary');
            $payroll_foreigner->working_day = request('working_day');
            $payroll_foreigner->month_salary = request('month_salary');
            $payroll_foreigner->contribute_insurance_salary = request('contribute_insurance_salary');
            $payroll_foreigner->assessable_income = request('assessable_income');
            $payroll_foreigner->health_ins_employer = request('health_ins_employer');
            $payroll_foreigner->health_ins_employee = request('health_ins_employee');
            $payroll_foreigner->pit = request('pit');
            $payroll_foreigner->net_salary = request('net_salary');
            $payroll_foreigner->save();
            return redirect()->back();
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function deleteSalaryForeigner($id)
    {
        try {
            if (!in_array(137, GlobalVariable::GlobarModuleLinks()) && !(Auth::user()->role_id == 1)) {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
            $payroll_foreigner = SmPayrollForeigner::findOrFail($id);
            $payroll_foreigner->deduction->delete();
            $payroll_foreigner->delete();
            return redirect('/salary/foreigner');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function myPayrollOption()
    {
        try {
            $staff_id = Auth::user()->staff->id;
            $my_payroll = SmPayrollForeigner::where('staff_id', $staff_id)->get();
            $vn_or_foreigner = "foreigner";
            if ($my_payroll->count() == 0) {
                $my_payroll = SmPayrollVn::where('staff_id', $staff_id)->get();
                $vn_or_foreigner = "vn";
            }
            if ($my_payroll->count() == 0) {
                Toastr::error("You don't have any payroll", 'Failed');
                return redirect()->back();
            }
            return view('backEnd.accounts.my_payroll_search', compact('my_payroll', 'vn_or_foreigner'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function myPayrollSearch()
    {
        
        try {
            $staff_id = Auth::user()->staff->id;
            if (request('vn_or_foreigner') == "vn") {
                $payroll_vn = SmPayrollVn::where('staff_id', $staff_id)->where('month_year', request('month_year'))->first();
                
                return view('backEnd.accounts.edit_salary_vn', compact('payroll_vn'));
            }
            else{
                 $payroll_foreigner = SmPayrollForeigner::where('staff_id', $staff_id)->where('month_year', request('month_year'))->first();
                 
                 return view('backEnd.accounts.edit_salary_foreigner', compact('payroll_foreigner'));
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function exportSalary ($staff_id,$month_year,$type)
    {
        $payroll = null;
        if($type == 'vn')
        {
            $payroll = SmPayrollVn::where('staff_id', $staff_id)->where('month_year',$month_year)->first();
            
        }
        else
        {
            $payroll = SmPayrollForeigner::where('staff_id', $staff_id)->where('month_year',$month_year)->first();
            
        }
        
       
        return view('backEnd.accounts.payslip', compact('payroll'));
        
    }

    public function getStaffInformation()
    {
        $staff = SmStaff::find(request()->id);
        return response()->json([
            'role' => $staff->roles->name,
            'staff_no' => $staff->staff_no,
        ]);
    }

    public function getMonthYear()
    {
        $data = SmPayrollForeigner::all()->pluck('month_year')->unique();
        if (request('vn_foreigner') == '1') {
            $data = SmPayrollVn::all()->pluck('month_year')->unique();
        }
        return response()->json([
            'data' => $data,
        ]);
    }
}
