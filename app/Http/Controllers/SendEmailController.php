<?php

namespace App\Http\Controllers;

use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;
use Modules\RolePermission\Entities\InfixRole;

class SendEmailController extends Controller
{
    public function create()
    {
        $roles = InfixRole::all();
        return view('backEnd.sendEmail.create', compact('roles'));
    }

    public function store()
    {
        $data['content'] = request('body');
        $role_emails = [];
        foreach (request('roles', []) as $role) {
            $parents = User::where('role_id', $role)->pluck('email');
            $role_emails = $this->checkEmails($parents);
        }
        
        $individual_emails = $this->checkEmails(request('individual_emails', []));
        
        $emails = array_unique(array_merge($role_emails, $individual_emails));
        
        // $emails = ['tvdkhoa1801@gmail.com', 'thanhphu2410@gmail.com', 'caovananhdn@gmail.com'];
        $email_template = request()->exists('summercamp_template') 
                ? 'frontEnd.mails.summercamp' 
                : 'frontEnd.mails.send_email';
        Mail::send($email_template, compact('data'), function ($message) use ($emails) {
            $message->to('caovananhdn@gmail.com')
                ->bcc($emails)
                ->subject(request('title'));
        });
        Toastr::success('Operation successful', 'Success');
        return back();
    }
    
    private function checkEmails($emails)
    {
        $filtered = [];
        foreach ($emails as $value) {
            if (strpos($value, '@') !== false && strpos($value, '.') !== false) {
                $filtered[] = $value;
            }
        }
        return $filtered;
    }
}
