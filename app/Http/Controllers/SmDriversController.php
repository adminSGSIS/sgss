<?php

namespace App\Http\Controllers;

use App\SmStaff;
use App\SmVehicle;
use App\YearCheck;
use App\ApiBaseMethod;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\SmRoute;
use App\SmAssignVehicle;
use DB;


class SmDriversController extends Controller
{
    public function listdrivers()
    {
    	$routes = SmRoute::all();
        
         $drivers = SmStaff::where([['active_status', 1], ['role_id', 9]])->get();
            foreach ($drivers as $driver) {
                    if($driver ->route){
                        $route_title = SmRoute::find($driver->route->route_id);
                        $driver['route_title']= $route_title->title;
                        $driver['route_id'] = $route_title->id;
                    }
                    else
                    {
                        $driver['route_title']= "";
                        $driver['route_id'] = "";
                    }
                    if($driver->vehicle)
                    {
                        $vehicle_no = SmVehicle::find($driver->vehicle->id)->vehicle_no;
                        $driver['vehicle_no'] = $vehicle_no;
                    }
                    else{
                         $driver['vehicle_no'] = "";                   
                    }                                       
               }   
        $listdrivers = $drivers;
    	return view('backEnd.transport.list_driver',compact('routes','listdrivers'));       
    }  
}
