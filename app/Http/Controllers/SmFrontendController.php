<?php

namespace App\Http\Controllers;

use App\User;
use App\SmExam;
use App\SmNews;
use App\SmClass;
use App\SmEvent;
use App\SmStaff;
use App\SmCourse;
use App\SmSection;
use App\ApiBaseMethod;
use App\SmStudent;
use App\SmSubject;
use App\YearCheck;
use App\SmExamType;
use App\SmNewsPage;
use App\Candidate; //anh
use App\SmTestOnline;//anh
use App\SmAboutPage;
use App\SmCategoriesImagesAlbum;
use App\SmCategoriesImagesDefaultAlbum;
use App\SmCategoriesImagesPhoto;
use App\SmCoursePage;
use App\SmCustomLink;
use App\SmContactPage;
use App\SmNoticeBoard;
use App\SmTestimonial;
use App\SmContactMessage;
use App\SmSocialMediaIcon;
use App\SmGeneralSettings;
use App\SmVisitor;
use App\SmHomePageSetting;
use Illuminate\Http\Request;
use App\SmFrontendPersmission;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\FeesGroup;
use App\SmFeesGroup;
use App\sm_fees;
use App\Form1;
use App\SmPaymentMethhod;
use Dotenv\Validator;
use App\SmRoute;
use App\SmFeesDiscount;
use App\late_enrolment;
use App\school_grades;
use App\withdraw;
use App\born_between;
use App\born_between_year;
use App\SmFeesNotes;
use \Carbon\Carbon;
use App\SmCareers;
use File;
use App\SmParent;
use App\SmNewsSubCategory;
use App\SmNewsCategory;
use App\SmAcademicYear; // Long 31/08
use Intervention\Image\Facades\Image; // Long 04/09
use App\SmFoodType;
use App\SmFood;
use App\SmFoodWeekday;
use Illuminate\Support\Str;
use App\MedicalDeclarationEntry;
use App\MedicalDeclarationDomestic;

class SmFrontendController extends Controller
{
    public function __construct()
    {
        $this->middleware('PM');
        // User::checkAuth();
    }
    /**
     * Display a caretogy images.
     *
     * @return \Illuminate\Http\Response
     */

    public function category()
    {
        try {
            $album = SmCategoriesImagesAlbum::get()->map(function ($i) {
                return [
                    'album' => $i,
                    'gallery' => SmCategoriesImagesPhoto::where('album_id', $i->id)->get()
                ];
            });

            return view('frontEnd.home.light_category', compact('album'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            if (Schema::hasTable('users')) {
                $testInstalled = DB::table('users')->get();
                if (count($testInstalled) < 1) {
                    return view('install.welcome_to_infix');
                } else {
                    $exams = SmExam::all();
                    $news = SmNews::orderBy('order', 'asc')->limit(3)->get();
                    $testimonial = SmTestimonial::all();
                    $academics = SmCourse::orderBy('id', 'asc')->limit(3)->get();
                    $exams_types = SmExamType::all();
                    $events = SmEvent::all();
                    $a = 2;
                    $b = 3;
                    $c = 9;
                    $notice_board = SmNoticeBoard::where('is_published', 1)->orderBy('created_at', 'DESC')->take(3)->get();
                    $classes = SmClass::where('active_status', 1)->get();
                    $subjects = SmSubject::where('active_status', 1)->get();
                    $sections = SmSection::where('active_status', 1)->get();
                    $links = SmCustomLink::find(1);
                    $homePage = SmHomePageSetting::find(1);
                    $permisions = SmFrontendPersmission::where([['parent_id', 1], ['is_published', 1]])->get();
                    $per = [];
                    foreach ($permisions as $permision) {
                        $per[$permision->name] = 1;
                    }
                    $button_settings = SmGeneralSettings::find(1);

                    $url = explode('/', $button_settings->website_url);
                    if ($button_settings->website_btn == 0) {
                        return redirect('login');
                    } else {
                        if (!SmCategoriesImagesDefaultAlbum::first()) {
                            $photo = [];
                        } else {
                            $photo = SmCategoriesImagesPhoto::where('album_id', SmCategoriesImagesDefaultAlbum::first()->album_id)->get();
                        }

                        if ($button_settings->website_url == '') {
                            if (SmGeneralSettings::isModule('Saas') == true) {
                                $contact_info = SmContactPage::first();
                                if (SmGeneralSettings::isModule('SaasSubscription') == true) {
                                    $packages = \Modules\SaasSubscription\Entities\SmPackagePlan::where('active_status', 1)->get();
                                } else {
                                    $packages = [];
                                }

                                return view('saas::auth.saas_landing', compact('contact_info', 'packages'));
                            } else {
                                return view('frontEnd.home.layout.body', compact('exams', 'classes', 'subjects', 'exams_types', 'sections', 'news', 'testimonial', 'notice_board', 'events', 'academics', 'links', 'homePage', 'per', 'photo'));
                            }
                        } elseif ($url[max(array_keys($url))] == 'home') {
                            if (SmGeneralSettings::isModule('Saas') == true) {
                                $contact_info = SmContactPage::first();
                                return view('saas::auth.saas_landing', compact('contact_info'));
                            } else {
                                return view('frontEnd.home.layout.body', compact('exams', 'classes', 'subjects', 'exams_types', 'sections', 'news', 'testimonial', 'notice_board', 'events', 'academics', 'links', 'homePage', 'per', 'photo'));
                            }
                        } else {
                            $url = $button_settings->website_url;
                            return Redirect::to($url);
                        }
                    }
                }
            } else {
                return view('install.welcome_to_infix');
            }
        } catch (\Exception $e) {
            // dd($e);
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function about()
    {
        try {
            $exams = SmExam::all();
            $exams_types = SmExamType::all();
            $classes = SmClass::where('active_status', 1)->get();
            $subjects = SmSubject::where('active_status', 1)->get();
            $sections = SmSection::where('active_status', 1)->get();
            $about = SmAboutPage::first();
            $testimonial = SmTestimonial::all();
            $totalStudents = SmStudent::where('active_status', 1)->get();
            $totalTeachers = SmStaff::where('active_status', 1)->where('role_id', 4)->get();
            $history = SmNews::where('category_id', 2)->limit(3)->get();
            $mission = SmNews::where('category_id', 3)->limit(3)->get();

            /* Tran Thanh Phu */
            $categories = SmNewsCategory::where('category_name', 'About')->first();
            $subCategories = SmNewsSubCategory::where([
                ['category_id', $categories->id],
                ['sub_category_name', 'Our Shared Vision & Mission']
            ])->first();
            $cate_post = SmNews::where('sub_category_id', $subCategories->id)->first();
            /* Tran Thanh Phu */

            return view('frontEnd.home.light_about', compact('exams', 'classes', 'subjects', 'exams_types', 'cate_post', 'sections', 'about', 'testimonial', 'totalStudents', 'totalTeachers', 'history', 'mission'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function news()
    {
        try {
            $exams = SmExam::all();
            $exams_types = SmExamType::all();
            $classes = SmClass::where('active_status', 1)->get();
            $subjects = SmSubject::where('active_status', 1)->get();
            $sections = SmSection::where('active_status', 1)->get();
            return view('frontEnd.home.light_news', compact('exams', 'classes', 'subjects', 'exams_types', 'sections'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function contact()
    {
        try {
            $exams = SmExam::all();
            $exams_types = SmExamType::all();
            $classes = SmClass::where('active_status', 1)->get();
            $subjects = SmSubject::where('active_status', 1)->get();
            $sections = SmSection::where('active_status', 1)->get();

            $contact_info = SmContactPage::first();
            return view('frontEnd.home.light_contact', compact('exams', 'classes', 'subjects', 'exams_types', 'sections', 'contact_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function institutionPrivacyPolicy()
    {
        try {
            $exams = SmExam::all();
            $exams_types = SmExamType::all();
            $classes = SmClass::where('active_status', 1)->get();
            $subjects = SmSubject::where('active_status', 1)->get();
            $sections = SmSection::where('active_status', 1)->get();

            $contact_info = SmContactPage::first();
            return view('frontEnd.home.institutionPrivacyPolicy', compact('exams', 'classes', 'subjects', 'exams_types', 'sections', 'contact_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    
    public function institutionTermServices()
    {
        try {
            $exams = SmExam::all();
            $exams_types = SmExamType::all();
            $classes = SmClass::where('active_status', 1)->get();
            $subjects = SmSubject::where('active_status', 1)->get();
            $sections = SmSection::where('active_status', 1)->get();

            $contact_info = SmContactPage::first();
            return view('frontEnd.home.institutionTermServices', compact('exams', 'classes', 'subjects', 'exams_types', 'sections', 'contact_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function newsDetails($id)
    {
        try {
            $news = SmNews::find($id);
            return view('frontEnd.home.light_news_details', compact('news'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function newsPage()
    {
        try {
            $hotnews = SmNews::where('category_id', SmNewsCategory::where('category_name', 'news & event')->first()->id)->orderBy('created_at', 'desc')->first();
            $newslist = '';
            if ($hotnews) {
                $newslist = SmNews::where('category_id', SmNewsCategory::where('category_name', 'news & event')->first()->id)
                    ->where('news_body', '!=', $hotnews->news_body)->orderBy('created_at', 'desc')->take(4)->get();
            }
            
            $list_cate = SmNewsSubCategory::where('category_id', SmNewsCategory::where('category_name', 'news & event')->first()->id)->get();
            
            return view('frontEnd.home.light_news_category', compact('newslist', 'hotnews', 'list_cate'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function subcate($id)
    {
        try {
            $subcate = SmNewsSubCategory::where('category_id', $id)->paginate(6);
            $name_cate = SmNewsCategory::find($id);
            return view('frontEnd.home.light_news_sub_category', compact('subcate', 'name_cate'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function category_items($id)
    {
        try {
            $name_subcate=SmNewsSubCategory::find($id);
            $items=SmNews::where('sub_category_id', $id)->orderBy('created_at', 'desc')->get();
            
            return view('frontEnd.home.category_items', compact('items', 'name_subcate'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function conpactPage()
    {
        try {
            $contact_us = SmContactPage::first();
            return view('frontEnd.contact_us', compact('contact_us'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function contactPageEdit()
    {
        try {
            $contact_us = SmContactPage::first();
            $update = "";

            return view('frontEnd.contact_us', compact('contact_us', 'update'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function contactPageStore(Request $request)
    {
        if ($request->file('image') == "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'google_map_address' => 'required',
            ]);
        } else {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'google_map_address' => 'required',
                'image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        }

        try {
            $fileName = "";
            if ($request->file('image') != "") {
                $contact = SmContactPage::find(1);
                if ($contact != "") {
                    if ($contact->image != "") {
                        if (file_exists($contact->image)) {
                            unlink($contact->image);
                        }
                    }
                }

                $file = $request->file('image');
                $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/contactPage/', $fileName);
                $fileName = 'public/uploads/contactPage/' . $fileName;
            }

            $contact = SmContactPage::first();
            if ($contact == "") {
                $contact = new SmContactPage();
            }
            $contact->title = $request->title;
            $contact->description = $request->description;
            $contact->button_text = $request->button_text;
            $contact->button_url = $request->button_url;

            $contact->address = $request->address;
            $contact->address_text = $request->address_text;
            $contact->phone = $request->phone;
            $contact->phone_text = $request->phone_text;
            $contact->email = $request->email;
            $contact->email_text = $request->email_text;
            $contact->latitude = $request->latitude;
            $contact->longitude = $request->longitude;
            $contact->google_map_address = $request->google_map_address;
            if ($fileName != "") {
                $contact->image = $fileName;
            }

            $result = $contact->save();

            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('contact-page');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function aboutPage()
    {
        try {
            $about_us = SmAboutPage::first();
            return view('frontEnd.about_us', compact('about_us'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function aboutPageEdit()
    {
        try {
            $about_us = SmAboutPage::first();
            $update = "";

            return view('frontEnd.about_us', compact('about_us', 'update'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function newsHeading()
    {
        try {
            $SmNewsPage = SmNewsPage::first();
            $update = "";

            return view('backEnd.news.newsHeadingUpdate', compact('SmNewsPage', 'update'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function newsHeadingUpdate(Request $request)
    {
        if ($request->file('image') == "" && $request->file('main_image') == "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
            ]);
        } elseif ($request->file('image') != "" && $request->file('main_image') != "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'image' => 'dimensions:min_width=1420,min_height=450',
                'main_image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        } elseif ($request->file('image') != "" && $request->file('main_image') == "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        } elseif ($request->file('image') == "" && $request->file('main_image') != "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'main_image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        }

        try {
            $fileName = "";
            if ($request->file('image') != "") {
                $about = SmNewsPage::find(1);
                if ($about != "") {
                    if ($about->image != "") {
                        if (file_exists($about->image)) {
                            unlink($about->image);
                        }
                    }
                }

                $file = $request->file('image');
                $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/about_page/', $fileName);
                $fileName = 'public/uploads/about_page/' . $fileName;
            }

            $mainfileName = "";
            if ($request->file('main_image') != "") {
                $about = SmNewsPage::find(1);
                if ($about != "") {
                    if ($about->main_image != "") {
                        if (file_exists($about->main_image)) {
                            unlink($about->main_image);
                        }
                    }
                }

                $file = $request->file('main_image');
                $mainfileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/about_page/', $mainfileName);
                $mainfileName = 'public/uploads/about_page/' . $mainfileName;
            }

            $about = SmNewsPage::first();
            if ($about == "") {
                $about = new SmNewsPage();
            }
            $about->title = $request->title;
            $about->description = $request->description;
            $about->main_title = $request->main_title;
            $about->main_description = $request->main_description;
            $about->button_text = $request->button_text;
            $about->button_url = $request->button_url;
            if ($fileName != "") {
                $about->image = $fileName;
            }
            if ($mainfileName != "") {
                $about->main_image = $mainfileName;
            }
            $result = $about->save();

            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('news-heading-update');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect('news-heading-update');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    // end heading update

    public function courseHeading()
    {
        try {
            $SmCoursePage = SmCoursePage::first();
            $update = "";

            return view('backEnd.course.courseHeadingUpdate', compact('SmCoursePage', 'update'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function courseHeadingUpdate(Request $request)
    {
        if ($request->file('image') == "" && $request->file('main_image') == "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
            ]);
        } elseif ($request->file('image') != "" && $request->file('main_image') != "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'image' => 'dimensions:min_width=1420,min_height=450',
                'main_image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        } elseif ($request->file('image') != "" && $request->file('main_image') == "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        } elseif ($request->file('image') == "" && $request->file('main_image') != "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'main_image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        }

        try {
            $fileName = "";
            if ($request->file('image') != "") {
                $about = SmCoursePage::find(1);
                if ($about != "") {
                    if ($about->image != "") {
                        if (file_exists($about->image)) {
                            unlink($about->image);
                        }
                    }
                }

                $file = $request->file('image');
                $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/about_page/', $fileName);
                $fileName = 'public/uploads/about_page/' . $fileName;
            }

            $mainfileName = "";
            if ($request->file('main_image') != "") {
                $about = SmCoursePage::find(1);
                if ($about != "") {
                    if ($about->main_image != "") {
                        if (file_exists($about->main_image)) {
                            unlink($about->main_image);
                        }
                    }
                }

                $file = $request->file('main_image');
                $mainfileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/about_page/', $mainfileName);
                $mainfileName = 'public/uploads/about_page/' . $mainfileName;
            }

            $about = SmCoursePage::first();
            if ($about == "") {
                $about = new SmCoursePage();
            }
            $about->title = $request->title;
            $about->description = $request->description;
            $about->main_title = $request->main_title;
            $about->main_description = $request->main_description;
            $about->button_text = $request->button_text;
            $about->button_url = $request->button_url;
            if ($fileName != "") {
                $about->image = $fileName;
            }
            if ($mainfileName != "") {
                $about->main_image = $mainfileName;
            }
            $result = $about->save();

            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('course-heading-update');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect('course-heading-update');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function aboutPageStore(Request $request)
    {
        if ($request->file('image') == "" && $request->file('main_image') == "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
            ]);
        } elseif ($request->file('image') != "" && $request->file('main_image') != "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'image' => 'dimensions:min_width=1420,min_height=450',
                'main_image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        } elseif ($request->file('image') != "" && $request->file('main_image') == "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        } elseif ($request->file('image') == "" && $request->file('main_image') != "") {
            $request->validate([
                'title' => 'required',
                'description' => 'required',
                'main_title' => 'required',
                'main_description' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'main_image' => 'dimensions:min_width=1420,min_height=450',
            ]);
        }

        try {
            $fileName = "";
            if ($request->file('image') != "") {
                $about = SmAboutPage::find(1);
                if ($about != "") {
                    if ($about->image != "") {
                        if (file_exists($about->image)) {
                            unlink($about->image);
                        }
                    }
                }

                $file = $request->file('image');
                $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/about_page/', $fileName);
                $fileName = 'public/uploads/about_page/' . $fileName;
            }

            $mainfileName = "";
            if ($request->file('main_image') != "") {
                $about = SmAboutPage::find(1);
                if ($about != "") {
                    if ($about->main_image != "") {
                        if (file_exists($about->main_image)) {
                            unlink($about->main_image);
                        }
                    }
                }

                $file = $request->file('main_image');
                $mainfileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move('public/uploads/about_page/', $mainfileName);
                $mainfileName = 'public/uploads/about_page/' . $mainfileName;
            }

            $about = SmAboutPage::first();
            if ($about == "") {
                $about = new SmAboutPage();
            }
            $about->title = $request->title;
            $about->description = $request->description;
            $about->main_title = $request->main_title;
            $about->main_description = $request->main_description;
            $about->button_text = $request->button_text;
            $about->button_url = $request->button_url;
            if ($fileName != "") {
                $about->image = $fileName;
            }
            if ($mainfileName != "") {
                $about->main_image = $mainfileName;
            }
            $result = $about->save();

            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('about-page');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function sendMessage(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['subject'] = $request->subject;
        $data['message'] = $request->message;

        DB::beginTransaction();
        try {
            $contact_message = new SmContactMessage();
            $contact_message->name = $request->name;
            $contact_message->email = $request->email;
            $contact_message->subject = $request->subject;
            $contact_message->message = $request->message;
            $result = $contact_message->save();

            Mail::send('frontEnd.contact_mail', compact('data'), function ($message) use ($request) {
                $setting = SmGeneralSettings::find(1);
                $email = $setting->email;
                $school_name = $setting->school_name;
                $message->to($email, $school_name)->subject($request->subject);
                $message->from($email, $school_name);
            });

            DB::commit();
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect()->back()->with('message-success', 'Message send successfully');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
            // Toastr::success('Operation successful', 'Success');
            // return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }

    public function contactMessage(Request $request)
    {
        try {
            $contact_messages = SmContactMessage::orderBy('id', 'desc')->get();

            return view('frontEnd.contact_message', compact('contact_messages'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    //user register method start
    public function register()
    {
        try {
            $schools = SmSchool::where('active_status', 1)->get();
            return view('auth.registerCodeCanyon', compact('schools'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function customer_register(Request $request)
    {
        $request->validate([
            'fullname' => 'required|min:3|max:100',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
        ]);

        try {
            //insert data into user table
            $s = new User();
            $s->role_id = 4;
            $s->full_name = $request->fullname;
            $s->username = $request->email;
            $s->email = $request->email;
            $s->active_status = 0;
            $s->access_status = 0;
            $s->password = Hash::make($request->password);
            $s->save();
            $result = $s->toArray();
            $last_id = $s->id; //last id of user table

            //insert data into staff table
            $st = new SmStaff();
            $st->school_id = 1;
            $st->user_id = $last_id;
            $st->role_id = 4;
            $st->first_name = $request->fullname;
            $st->full_name = $request->fullname;
            $st->last_name = '';
            $st->staff_no = 10;
            $st->email = $request->email;
            $st->active_status = 0;
            $st->save();

            $result = $st->toArray();
            if (!empty($result)) {
                Toastr::success('Operation successful', 'Success');
                return redirect('login');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            Toastr::error('Operation Failed,' . $e->getMessage(), 'Failed');
            return redirect()->back();
        }
    }

    public function course()
    {
        try {
            $exams = SmExam::all();
            $course = SmCourse::all();
            $news = SmNews::orderBy('order', 'asc')->limit(3)->get();
            $exams_types = SmExamType::all();
            $coursePage = SmCoursePage::first();
            $classes = SmClass::where('active_status', 1)->get();
            $subjects = SmSubject::where('active_status', 1)->get();
            $sections = SmSection::where('active_status', 1)->get();
            return view('frontEnd.home.light_course', compact('exams', 'classes', 'coursePage', 'subjects', 'exams_types', 'sections', 'course', 'news'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function courseDetails($id)
    {
        try {
            $course = SmCourse::find($id);
            $courses = SmCourse::orderBy('id', 'asc')->whereNotIn('id', [$id])->limit(3)->get();
            //return view('frontEnd.home.light_course_details', compact('course', 'courses'));
            $categories = SmNewsCategory::where('category_name', "School Life")->first();

            /* Tran Thanh Phu start */
            $numbers = range(0, 6);
            shuffle($numbers);
            $randoms = array_slice($numbers, 0, 3);
            for ($i = 0; $i < 3; $i ++) {
                $cate_post[] = SmNews::where('category_id', $categories->id)->get()[$randoms[$i]];
            }
            /* Tran Thanh Phu end */
            return view('frontEnd.home.light_course_details', compact('course', 'courses', 'cate_post'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function socialMedia()
    {
        $visitors = SmSocialMediaIcon::all();

        return view('frontEnd.socialMedia', compact('visitors'));
    }

    public function socialMediaStore(Request $request)
    {
        $request->validate([
            'url' => 'required',
            'icon' => 'required',
            // 'icon' => 'required|dimensions:min_width=24,max_width=24',
            'status' => 'required',
        ]);

        try {

            // $fileName = "";
            // if ($request->file('icon') != "") {
            //     $file = $request->file('icon');
            //     $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            //     $file->move('public/uploads/socialIcon/', $fileName);
            //     $fileName = 'public/uploads/socialIcon/' . $fileName;
            // }

            $visitor = new SmSocialMediaIcon();
            $visitor->url = $request->url;
            $visitor->icon = $request->icon;
            $visitor->status = $request->status;
            $result = $visitor->save();

            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                if ($result) {
                    return ApiBaseMethod::sendResponse(null, 'Created successfully.');
                }
                return ApiBaseMethod::sendError('Something went wrong, please try again.');
            } else {
                if ($result) {
                    Toastr::success('Operation successful', 'Success');
                    return redirect()->back();
                }
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function socialMediaEdit($id)
    {
        $visitors = SmSocialMediaIcon::all();
        $visitor = SmSocialMediaIcon::find($id);

        return view('frontEnd.socialMedia', compact('visitors', 'visitor'));
    }


    public function socialMediaUpdate(Request $request)
    {
        $request->validate([
            'url' => 'required',
            'icon' => 'required',
            // 'icon' => 'dimensions:min_width=24,max_width=24',
            'status' => 'required',
        ]);

        try {
            $visitor = SmSocialMediaIcon::find($request->id);
            $visitor->url = $request->url;
            $visitor->icon = $request->icon;
            $visitor->status = $request->status;
            $result = $visitor->save();

            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                if ($result) {
                    return ApiBaseMethod::sendResponse(null, 'Updated successfully.');
                }
                return ApiBaseMethod::sendError('Something went wrong, please try again.');
            } else {
                if ($result) {
                    Toastr::success('Operation successful', 'Success');
                    return redirect('social-media');
                }
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    /* start Cao văn anh */
    public function teachers(){
            $leaderships = SmStaff::where('role_id',4)->whereIn('designation_id',[13,6,7,5])->get();
            // dd($leaderships);
            $early_years_teachers = SmStaff::where('role_id',4)->where('department_id',2)->get();
            $primary_years_teachers = SmStaff::where('role_id',4)->where('department_id',10)->get();
            $middle_years_teachers = SmStaff::where('role_id',4)->where('department_id',9)->get();
            foreach($primary_years_teachers as $primary_years_teacher)
            {
                $primary_years_teacher->sort = $primary_years_teacher->designations->id;
            }
            $primary_years_teachers = $primary_years_teachers->sortBy('sort');
            
            $specialist_teachers =  SmStaff::where('role_id',4)->where('department_id',12)->get();
            $teacher_info = SmStaff::where('role_id',4)->get();
            return view('frontEnd.home.teachers',compact('leaderships','early_years_teachers','primary_years_teachers','middle_years_teachers','specialist_teachers','teacher_info'));
    }
     /* end Cao văn anh */


    public function socialMediaDelete(Request $request, $id)
    {
        try {
            $visitor = SmSocialMediaIcon::find($id);
            // if ($visitor->icon != "") {
            //     if (file_exists($visitor->icon)) {
            //         unlink($visitor->icon);
            //     }
            // }
            $result = $visitor->delete();

            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                if ($result) {
                    return ApiBaseMethod::sendResponse(null, 'Deleted successfully.');
                } else {
                    return ApiBaseMethod::sendError('Something went wrong, please try again.');
                }
            } else {
                if ($result) {
                    Toastr::success('Operation successful', 'Success');
                    return redirect('social-media');
                } else {
                    Toastr::error('Operation Failed', 'Failed');
                    return redirect()->back();
                }
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    /* start Enrolment Danh Phi Long */
    public function enrolment_index()
    {
        return redirect('enrolment/step_1');
    }

    public function enrolment_1()
    {
        try {
            return view('frontEnd.home.light_enrolment');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function enrolment_1s(Request $request)
    {
        $valid = $request->validate([
            'student_full_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'date_of_birth' => 'required|date_format:m/d/Y',
            'gender' => 'required',
            'session' => 'required',
            'first_nationality' => 'required|regex:/^[\pL\s\-]+$/u',
            'residential_address_in_vietnam' => 'required|min:5|max:150',
            'student_lives_with' => 'required',
            'fathers_full_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'fathers_nationality' => 'required|regex:/^[\pL\s\-]+$/u',
            'fathers_contact_phone_number' => 'required|numeric',
            'fathers_email_address' => 'required|min:5|max:150',
            'fathers_occupation' => 'required|regex:/^[\pL\s\-]+$/u', // NGHE NGHIEP PHU HUYNH
            'mothers_occupation' => 'required|regex:/^[\pL\s\-]+$/u', // NGHE NGHIEP PHU HUYNH
            'mothers_full_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'mothers_nationality' => 'required|regex:/^[\pL\s\-]+$/u',
            'mothers_contact_phone_number' => 'required|numeric',
            'mothers_email_address' => 'required|min:5|max:150',
            'emergency_full_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'emergency_relationship_to_parents' => 'required|regex:/^[\pL\s\-]+$/u',
            'emergency_contact_phone_number' => 'required|numeric',
            'school_name' => 'required',
            'location_or_country' => 'required|regex:/^[\pL\s\-]+$/u',
            'language_of_instruction' => 'required|regex:/^[\pL\s\-]+$/u',
            'from' => 'required|numeric',
            'to' => 'required',
            'childs_first_language' => 'required|regex:/^[\pL\s\-]+$/u',
            'level_of_E_language_experience' => 'required|numeric',
            'answer1' => 'required|max:150',
            'answer2' => 'required|max:150',
            'answer3' => 'required|max:150',
            'answer4' => 'required|max:150',
            'answer5' => 'required|max:150',
            'health_permissions' => 'required',
            'lunch_requested' => 'required',
            'school_bus' => 'required',
            'payment_by' => 'required',
            'VAT' => 'required',
            'other_permissions' => 'required'
        ]);
        try {
            $request->flash();
            return redirect('enrolment/step_2');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function enrolment_2(Request $request) // SmFrontendController
    {
        $input = $request->old();
        if (!$input) {
            return redirect('enrolment');
        }
        $fees = FeesGroup::where("fees_group", $input["session"])->get();
        $tuition_fees = SmAcademicYear::where("id_group", $input["session"])->get();
        // $classes = DB::table('sm_class_sections')->where('class_id', '=', $input['session'])
        //    ->join('sm_classes','sm_class_sections.class_id','=','sm_classes.id')
        //    ->get();
        $classes = DB::table("sm_academic_years")->where("id_group", $input["session"])->get();
        return view('frontEnd.home.light_enrolment_2', compact('fees', 'tuition_fees', 'classes'));
    }

    public function enrolment_2s(Request $request)
    {
        $valid = $request->validate([
            'class' => 'required'
        ]);
        try {
            // upload
            if ($request->file('avt')) {
                $max_id = SmStudent::max('id');
                $max_admission_id = 'sgstar'.str_pad($max_id + 1, 3, '0', STR_PAD_LEFT);
                $file_name = 'tmp_'.md5($max_admission_id).'.png';
                $resize_img = Image::make($request->file('avt'))->fit(100, 100);
                $resize_img->save(public_path('uploads/student/' . $file_name));
                session(['avt' => $file_name]);
            }

            $request->all();
            $request->flash();
            return redirect("/enrolment/step_3");
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function enrolment_3(Request $request)
    {
        try {
            $input = $request->old();
            if (!$input) {
                return redirect('enrolment');
            }
            $payment_methods = SmPaymentMethhod::get();
            return view('frontEnd.home.light_enrolment_3', compact("payment_methods"));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function enrolment_3s(Request $request)
    {
        $valid = $request->validate([
            'price' => 'required|numeric|min:0',
            'method' => 'required'
        ]);
        try {
            $request->all();
            $request->flash();
            return redirect("/enrolment/finished");
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function enrolment_finished(Request $request)
    {
        // try {
        $input = $request->old();
        if (!$input) {
            return redirect('enrolment');
        }

        // user
            $tmp_email = 'sgstar'.str_pad(SmStudent::max('id') + 1, 3, '0', STR_PAD_LEFT).'@sgstar.edu.vn'; // 2708_Long

            $user_stu = new User();
        $user_stu->role_id = 2;
        $user_stu->full_name = $input['student_full_name'];
        $user_stu->username = $tmp_email;
        $user_stu->email = $tmp_email;
        $user_stu->password = Hash::make(123456);
        $user_stu->save();

        // user_parents
        $user_parent = new User();
        $user_parent->role_id = 3;
        $user_parent->full_name = $input['fathers_full_name'];
        $user_parent->username  = $input['fathers_email_address'];
        $user_parent->email = $input['fathers_email_address'];
        $user_parent->password = Hash::make(123456);
        $user_parent->save();

        // parents
        $parent = new SmParent();
        $parent->user_id = $user_parent->id;
        $parent->fathers_name = $input['fathers_full_name'];
        $parent->fathers_mobile = $input['fathers_contact_phone_number'];
        $parent->fathers_occupation = $input['fathers_occupation'];
        $parent->mothers_name = $input['mothers_full_name'];
        $parent->mothers_mobile = $input['mothers_contact_phone_number'];
        $parent->mothers_occupation = $input['fathers_occupation'];
        $parent->guardians_name = $input['emergency_full_name'];
        $parent->guardians_mobile = $input['emergency_contact_phone_number'];
        // $parent->guardians_occupation = $value->guardian_occupation;
        /* Tran Thanh Phu start */
        if (strpos(strtolower($input['emergency_relationship_to_parents']), 'father') !== false) {
            $guardians_relation = 'F';
        } elseif (strpos(strtolower($input['emergency_relationship_to_parents']), 'mother') !== false) {
            $guardians_relation = 'M';
        } else {
            $guardians_relation = 'O';
        }
        $parent->guardians_relation = $guardians_relation;
        $parent->guardians_email = $input['fathers_email_address'];
        /* Tran Thanh Phu end */

        // $parent->relation = $value->relationButton;
        // $parent->guardians_address = $value->guardian_address;
        $parent->save();

        // store student
        $student = new SmStudent();
        $student->admission_no = 'sgstar'.str_pad(SmStudent::max('id') + 1, 3, '0', STR_PAD_LEFT); // 2708_Long
        $student->roll_no = SmStudent::max('roll_no') + 1;
        $student->class_id = $input['class'];
        $student->section_id = $input['session'];
        $student->student_category_id = $input['session'];
        $student->user_id = $user_stu->id;
        $student->parent_id = $parent->id;
        $student->mobile = $input['fathers_contact_phone_number'];

        /* Tran Thanh Phu start*/
        $splitName = explode(" ", $input['student_full_name']);
        $firstname = '';
        for ($i = 0; $i < count($splitName); $i++) {
            if ($i > 0) {
                $firstname = $firstname." ".$splitName[$i];
            }
        }
        $student->first_name  = $firstname;
        $student->last_name  = $splitName[0];
        /* Tran Thanh Phu end*/
            
        $student->full_name  = $input['student_full_name'];
        $student->gender_id = $input['gender'];
        $student->date_of_birth = date('Y-m-d', strtotime($input['date_of_birth']));
            
        $student->session_id = 1;
        $student->role_id = 1;
            
        // avatar
        if ($input['avt']) {
            $student->student_photo = "public/uploads/student/".session('avt');
        }
            
        $student->save();
            
        return view('frontEnd.home.light_enrolment_finished', compact('student'));
        // } catch (\Exception $e) {
        //     Toastr::error('Operation Failed', 'Failed');
        //     return redirect()->back();
        // }
    }
    /* Phạm Trọng hải*/
    public function admissions()
    {
        ///lấy danh sách các group
        $early= SmAcademicYear::where('id_group', 1)->get();
        $primary = SmAcademicYear::where('id_group', 2)->get();
        $middle=SmAcademicYear::where('id_group', 3)->get();

        $early_fees=sm_fees::where('fees_group', 1)->get();
        $primary_fees=sm_fees::where('fees_group', 2)->get();
        $middle_fees=sm_fees::where('fees_group', 3)->get();

        $withdraw=withdraw::all();
        $bus = smroute::all(); //lấy tất cả phí xe buýt
        $discount=SmFeesDiscount::where('type', null)->get(); //lấy tất cả discount
        $late_enrolment=late_enrolment::all(); //lấy tất cả phí ghi danh muộn
        $notes=SmFeesNotes::all();

        $thisyear=Carbon::now()->year;                          //lấy năm hiện tại
        $previousSchoolYear=($thisyear-1)."-".$thisyear;        //Lấy niên khóa trước
        $thisSchoolYear=$thisyear."-".($thisyear+1);            //lấy niên khóa hiện tại
        $nextSchoolYear=($thisyear+1)."-".($thisyear+2);        //lấy niên khóa tiếp theo
              //lấy niên khóa kế sau

        $schoolyears=[];                                        //tạo mảng schoolyears trống
        array_push($schoolyears, $previousSchoolYear, $thisSchoolYear, $nextSchoolYear);      //đẩy các niên khóa đã lấy được vào mảng schoolyears

        $born_between=DB::table('born_between')->orderby('id', 'desc')->limit(14)->get(); //lấy 14 niên khóa gần nhất
        $grades=[];
        $fromto=[];

        for ($i=0; $i < $born_between->count() ; $i++) {
            $grades[$i]=born_between_year::where('id_born', $born_between[$i]->id)
                                                    ->whereIn('school_year', $schoolyears) //gọi mảng schoolyears làm điều kiện truy vấn
                                                    ->orderby('id', 'asc')
                                                    ->get();
            $from=$born_between[$i]->from;  //lấy ngày sinh bắt đầu ở mảng born_between thứ i
            $to=$born_between[$i]->to;      //lấy ngày sinh kết thúc ở mảng born_between  thứ i
            $fromto[$i]=$from." to ".$to ;  //nối 2 mảng lại
        }
 
        $list=array_combine($fromto, $grades); //lấy tất cả lớp học
        

        
        return view('frontEnd.home.admissions', compact('notes', 'early', 'primary', 'middle', 'bus', 'discount', 'late_enrolment', 'withdraw', 'list', 'schoolyears', 'early_fees', 'primary_fees', 'middle_fees')); //gửi danh sách vè view
    }
    /* Phạm Trọng hải   */
    /* Phạm Trọng hải   */
    public function careers()
    {
        try {
            $careers = SmCareers::all()->where('status',1);
            return view('frontEnd.careers', compact('careers'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function viewcareer($id)
    {
        try {
            $career = SmCareers::find($id);
            return view('frontEnd.view_career', compact('career'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function job_apply(Request $request)
    {
        request()->validate([

            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
            'name' => 'required|max:100|min:1',
            'email' => 'required|max:100|min:1',
            'phone' => 'required|max:11|min:1',
            'gender' => 'required',
            'url_file' => 'required',

            ]);
              
        try {
            $candidate = new Candidate();
              
            $candidate->name = $request->name;
            $candidate->email = $request->email;
            $candidate->phone = $request->phone;
            $candidate->date_of_birth = $request->date;
            $candidate->gender = $request->gender;
            $candidate->address = $request->address;
            $candidate->school = $request->school;
            $candidate->graduation_year = $request->graduation_year;
            $candidate->about_me = $request->about;
            $avatar = $request->avatar;
            $url_avatar = $avatar->getClientOriginalName();
            $candidate->avatar = '/public/tmp_image/'.$url_avatar;
            $avatar->move(base_path('\public\tmp_image'), $candidate->avatar);

            $file = $request->url_file;
               
            $url_file = $file->getClientOriginalName();
                
            $candidate->attached_files = '/public/candidates/'.$url_file;
            $file->move(base_path('/public/candidates'), $url_file);
              
            $candidate->experience = $request->experience;
            $candidate->qualifications = $request->qualifications;
            $code = str_random(15);
            $candidate->code = $code;
                
            $result=$candidate->save();
                  
              
            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('testonline/'.$code);
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
        }
    }
    public function getlistsubcate($id)
    {
        try {
            $list_sub_cate = SmNewsSubCategory::where('category_id', $id)->get();

            foreach ($list_sub_cate as $list) {
                echo "<option data-display='".$list->sub_category_name."' value='".$list->id."'>".$list->sub_category_name."</option>";
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function getcategory($id)
    {
        try {
            $list_cate = SmNewsSubCategory::find($id);
            $id_cate = $list_cate->category_id;
            return $id_cate;
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    /*Phạm Trọng Hải end  28/7/2020*/

  

    public function lifeSchool()
    {
        try {
            $categories = SmNewsCategory::where('category_name', "School Life")->first();
            foreach (SmNewsSubCategory::where('category_id', $categories->id)->get() as $i) {
                $subNameCategories[] = strtolower($i->sub_category_name);
            }
            for ($i = 0; $i < count(SmCategoriesImagesAlbum::all()); $i++) {
                if (in_array(strtolower(SmCategoriesImagesAlbum::all()[$i]->title), $subNameCategories)) {
                    $filterCategories[] = SmCategoriesImagesAlbum::all()[$i];
                }
            }
            $album = array_map(function ($i) {
                return [
                   'album' => $i,
                   'gallery' => SmCategoriesImagesPhoto::where('album_id', $i->id)->get()
               ];
            }, $filterCategories);
            return view('frontEnd.home.light_lifeSchool', compact('album'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function lifeSchoolDetail($id)
    {
        try {
            $imagesAlbum = SmCategoriesImagesAlbum::where('title', ucwords(str_replace("-", " ", $id)))->first();
            $categories = SmNewsCategory::where('category_name', "School Life")->first();
            $cate_post = SmNews::where([
               ['category_id', $categories->id],
               ['sub_category_id', SmNewsSubCategory::where('sub_category_name', str_replace("-", " ", $id))->first()->id]
           ])->first();
            if ($imagesAlbum) {
                $imagesPhoto = SmCategoriesImagesPhoto::where('album_id', $imagesAlbum->id)->get();
                return view('frontEnd.home.light_lifeSchoolDetail', compact('imagesAlbum', 'imagesPhoto', 'cate_post'));
            } else {
                if ($cate_post) {
                    $imagesPhoto = false;
                    return view('frontEnd.home.light_lifeSchoolDetail', compact('imagesPhoto', 'cate_post'));
                }
                return view('errors.404');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function ourSchoolWelcome()
    {
        try {
            return view('frontEnd.home.light_ourSchoolWelcome');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
   
    public function ourSchoolCampus()
    {
        try {
            $images =SmCategoriesImagesPhoto::where('album_id',10)->get();
            return view('frontEnd.home.light_ourSchoolCampus',compact('images'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function ourSchoolIntroduction()
    {
        try {
            return view('frontEnd.home.light_ourSchoolIntroduction');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
   
    public function ourSchoolOverview()
    {
        try {
            return view('frontEnd.home.light_ourSchoolOverview');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function winterCamp()
    {
        try {
            return view('frontEnd.home.winter-camp');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function summerCamp()
    {
        try {
            return view('frontEnd.home.summer-camp');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function staffWorkshop()
    {
        try {
            return view('frontEnd.home.staff-workshop');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function parentsHandbook()
    {
        try {
            return view('frontEnd.home.parents-handbook');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function renderCatePost($id)
    {
        try {
            if ($id == 'admissions-process') {
                $cate_post = [];
                return view('frontEnd.home.renderCatePost', compact('cate_post'));
            }

            $categories = SmNewsCategory::where('category_name', str_replace("-", " ", $id))->first();
            $categories_as = SmNewsCategory::where('category_name', str_replace("-", ", ", $id))->first(); //another solution
            if ($categories || $categories_as) {
                $cate_post = SmNews::where('category_id', $categories ? $categories->id : $categories_as->id)->latest()->first();
                $cate_name = $categories ? $categories->category_name : $categories_as->category_name;
                return view('frontEnd.home.renderCatePost', compact("cate_post", "cate_name"));
            }
            $subCategories = SmNewsSubCategory::where('sub_category_name', str_replace("-", " ", $id))->first();
            $subCategories_as = SmNewsSubCategory::where('sub_category_name', str_replace("-", ", ", $id))->first(); //another solution
            $cate_post = SmNews::where('sub_category_id', $subCategories ? $subCategories->id : $subCategories_as->id)->latest()->first();
            $cate_name = $subCategories ? $subCategories->sub_category_name : $subCategories_as->sub_category_name;
            if ($cate_post) {
                return view('frontEnd.home.renderCatePost', compact("cate_post", "cate_name"));
            } else {
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    /* end - Tran Thanh Phu */
    /* ============== Cao Van Anh Start Here (26/11/2020)=========*/
    public function cvonline($code)
    {
        try {
            $data = Candidate::where('code', $code)->get();
            return view('frontEnd.home.cv_online', compact("data"));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function testonline($code)
    {
        try {
            return view('frontEnd.home.test_online', compact('code'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function test_result(Request $request)
    {
        try {
            $test_online = new SmTestOnline;
            $code = $request->code;
            $test_online->code = $code;
            $test_online->answer_1 = $request->question1;
            $test_online->answer_2 = $request->question2;
            $test_online->answer_3 = $request->question3;
            $test_online->answer_4 = $request->question4;
            $test_online->answer_5 = $request->question5;
            $test_online->answer_6 = $request->question6;
            $test_online->answer_7 = $request->question7;
            $test_online->answer_8 = $request->question8;
            $test_online->answer_9 = $request->question9;
            $test_online->answer_10 = $request->question10;
            $test_online->answer_11 = $request->question11;
            $test_online->answer_12 = $request->question12;
            $test_online->answer_13 = $request->question13;
            $test_online->answer_14 = $request->question14;
            $test_online->answer_15 = $request->question15;
            $test_online->answer_16 = $request->question16;
            $test_online->answer_17= $request->question17;

            $result=$test_online->save();

            $candidates = Candidate::where('code', $code)->get();
            foreach ($candidates as $candidate) {
                $name =$candidate->name;
                $phone = $candidate->phone;
                $email = $candidate->email;
                $file_attached = $candidate->attached_files;
            }
           
            $data['name'] = $name;
            $data['email'] = $email;
            $data['phone_number'] = $phone;
            $data['url_cv'] = url('/').'/'.'cvonline/'.$code;
            $data['url_result'] = url('/').'/'.'view_testonline/'.$code;
            $data['subject'] = 'Candidate';
            $data['attached_file'] = url('/').$file_attached;
           
            
            Mail::send('frontEnd.job_apply_email', compact('data'), function ($message) use ($request) {
                $code = $request->code;
                $setting = SmGeneralSettings::find(1);
                $email = $setting->email;
                $school_name = $setting->school_name;
                $message->from($email, $school_name);
                $message->to(Candidate::where('code', $code)->first()->email)->subject('Congratulations');
                $message->to('admin@sgstar.edu.vn')->subject('New Candidate');
            });
         

            if ($result) {
                Toastr::success('Operation successful', 'Success');
                return redirect('home');
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back()->with('message-danger', 'Something went wrong, please try again');
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function view_testonline($code)
    {
        try {
            $data = SmTestOnline::where('code', $code)->get();
           
            return view('frontEnd.home.view_testonline', compact("data"));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    /*::::::::::: 14-01-2021 ::::::::::::::::::::::::*/


    public function leadershipTeam()
    {
        try {
            $teacher_info = SmStaff::where('role_id', 4)->get();
            $leaderships = SmStaff::where('role_id',4)->whereIn('designation_id',[13,6,7,5])->orderBy('updated_at','asc')->get();
            return view('frontEnd.home.staff.leadership', compact('leaderships', 'teacher_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function admissionTeam()
    {
        try {
            $teacher_info = SmStaff::whereIn('role_id', [7,10])->get();
            $admissions = SmStaff::whereIn('user_id', [68,264,71,69])->orderBy('created_at', 'asc')->get();
            return view('frontEnd.home.staff.admission', compact('admissions', 'teacher_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function operationTeam()
    {
        try {
            $teacher_info = SmStaff::whereIn('role_id', [10,12,17,19])->get();
            $operations = SmStaff::whereIn('user_id', [71,279,292,277])->orderBy('created_at', 'asc')->get();
            return view('frontEnd.home.staff.operations', compact('operations', 'teacher_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function marketingTeam()
    {
        try {
            $teacher_info = SmStaff::whereIn('role_id', [5,16])->get();
            $marketings = SmStaff::where('department_id', 26)->get();
            return view('frontEnd.home.staff.marketing', compact('marketings', 'teacher_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function academicTeam()
    {
        $teacher_info = SmStaff::where('role_id', 4)->get();
        // $academics = SmStaff::where('role_id', 4)->orderBy('created_at', 'asc')->get()->except([18,16,17,57,92]);
        $earlys = SmStaff::where('department_id',2)->where('active_status',1)->get()->except(28);
        foreach($earlys as $early)
        {
            $early->sort = $early->designations->id;
        }
        $earlys = $earlys->sortBy('sort');
        $primarys = SmStaff::where('department_id',10)->where('active_status',1)->get()->except([18,17]);
        foreach($primarys as $primary)
        {
            $primary->sort = $primary->designations->id;
        }
        $primarys = $primarys->sortBy('sort');
        $middles = SmStaff::where('department_id',9)->where('active_status',1)->get()->except(18);
        $specialists = SmStaff::where('department_id',12)->where('active_status',1)->get();
        return view('frontEnd.home.staff.academic', compact('earlys', 'primarys','specialists','teacher_info','middles'));
        try {
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
   
    public function assistantTeam()
    {
         try {
            $teacher_info = SmStaff::where('role_id', 13)->get();
            $assistants = SmStaff::whereIn('department_id', [15,16,17,18])->get();
            foreach($assistants as $assistant)
            {
                $assistant->sort = $assistant->designations->id;
            }
            $assistants = $assistants->sortBy('sort');
            return view('frontEnd.home.staff.teacher-assistant', compact('assistants', 'teacher_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function itTeam()
    {
        try {
            $teacher_info = SmStaff::where('role_id', 16)->get();
            $its = SmStaff::where('department_id', 5)->orderBy('created_at', 'asc')->get();
            return view('frontEnd.home.staff.itTeam', compact('its', 'teacher_info'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function reasonSgstar()
    {
        try {
            return view('frontEnd.home.why-sgstarschool');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
     public function curriculumOverview()
    {
        try {
            return view('frontEnd.home.curriculum-overview');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
     public function earlyYear()
    {
        try {
            return view('frontEnd.home.early-year');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
     public function primaryYear()
    {
        try {
            return view('frontEnd.home.primary-year');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function studentCouncil()
    {
        try {
            return view('frontEnd.home.student-council');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function bookTour()
    {
        try {
            return view('frontEnd.home.bookTourSgstar');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
   
    public function scholarship()
    {
        try {
            return view('frontEnd.home.scholarship');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
     public function studentOfMonth()
    {
        try {
            return view('frontEnd.home.student-of-the-month');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
     public function parentsEssentials()
    {
        try {
            return view('frontEnd.home.parents-essentials');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    
    public function sgsisTeam()
    {
        try {
            return view('frontEnd.home.sgsisTeam');
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    //Pham Trong Hai 22/1/2021 start
    public function timetable()
    {
        return view('frontEnd.home.layout.timetable');
    }

    public function sgstv()
    {
        return view('frontEnd.home.layout.sgstv');
    }

    public function sgsjournal()
    {
        return view('frontEnd.home.layout.sgsjournal');
    }

    public function schoolcalendar()
    {
        return view('frontEnd.home.layout.schoolcalendar');
    }
    public function workshop()
    {
        return view('frontEnd.home.layout.workshop');
    }
    public function busschedule()
    {
        return view('frontEnd.home.layout.bus_schedule');
    }
    public function parentTeacherGroup()
    {
        return view('frontEnd.home.layout.parent_teacher_group');
    }
    public function menu()
    {
        $dayOfWeek = Carbon::today()->format('l');
        $menu = SmFoodWeekday::where('weekday', $dayOfWeek)->get();
        return view('frontEnd.home.layout.menu', compact('menu', 'dayOfWeek'));
    }
    public function weekdaymenu($weekday)
    {
        $dayOfWeek = $weekday;
        $menu = SmFoodWeekday::where('weekday', $dayOfWeek)->get();
        return view('frontEnd.home.layout.menu', compact('menu', 'dayOfWeek'));
    }
    public function qualityCycle()
    {
        return view('frontEnd.home.layout.qualityCycle');
    }
    //Pham Trong Hai 22/1/2021 end
    public function medicalDeclarationEntry()
    {
        return view('frontEnd.home.medical_declaration_entry');
    }

    public function medicalDeclarationEntryStore()
    {
        request()->validate([
            'gender' => "required",
        ]);
        try {
            $entry = MedicalDeclarationEntry::create(request()->all());
            return redirect('/declare-success/entry?id=' . $entry->id);
        } catch (\Exception $e) {
            return back();
        }
    }
    
    public function declareSuccessEntry()
    {
        $entry = MedicalDeclarationEntry::findOrFail(request('id'));
        return view('frontEnd.home.declaration_success_entry', compact('entry'));
    }

    public function medicalDeclaratioDomestic()
    {
        return view('frontEnd.home.medical_declaration_domestic');
    }

    public function medicalDeclaratioDomesticStore()
    {
        request()->validate([
            'gender' => "required",
        ]);
        try {
            $domestic = MedicalDeclarationDomestic::create(request()->all());
            return redirect('/declare-success/domestic?id=' . $domestic->id);
        } catch (\Exception $e) {
            return back();
        }
    }
    
    public function declareSuccessDomestic()
    {
        $domestic = MedicalDeclarationDomestic::findOrFail(request('id'));
        return view('frontEnd.home.declaration_success_domestic', compact('domestic'));
    }

    public function events()
    {
        try {
            $thismonth = Carbon::today()->month;
            $images = SmCategoriesImagesPhoto :: where('album_id',17)->get();
        
            $academic_id = YearCheck::getAcademicId();
            $events = SmEvent::whereMonth('from_date', $thismonth)->where('academic_id', $academic_id)->get();
            return view('frontEnd.home.layout.events', compact('events', 'thismonth' , 'images'));
        } catch (\Exception $e) {
            return view('errors.404');
        }
    }
    public function monthevents($id)
    {
        try {
            $thisyear = Carbon::today()->year;
            $academic_id = YearCheck::getAcademicId();
            $events = SmEvent::whereMonth('from_date', $id)->where('academic_id', $academic_id)->get();
            return response()->json([
                'data' => $events,
            ]);
        } catch (\Exception $e) {
            return view('errors.404');
        }
    }

    public function eventDetail($id)
    {
        try {
            $event = SmEvent::find($id);
            $slug = Str::slug($event->event_title);
            return redirect('event-'.$event->id.'/'.$slug);
        } catch (\Exception $e) {
            return view('errors.404');
        }
    }

    public function detailEvent($id, $slug)
    {
        try {
            $event = SmEvent::find($id);
            return view('frontEnd.home.layout.event-detail', compact('event'));
        } catch (\Exception $e) {
            return view('errors.404');
        }
    }

    public function subscribeEmail()
    {
        $data = [
            'booking' => url('/').'/book-a-tour',
            'fees'    => url('/').'/admissions'
        ];
        Mail::send('frontEnd.mails.subscribe_mail', compact('data'), function ($message) {
            $message->to(request('email'))->subject('Thanks for your subscribe');
        });
        return back();
    }
    
}
