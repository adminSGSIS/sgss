<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SmFoodType;
use App\SmFood;
use App\SmFoodWeekday;
Use \Carbon\Carbon;
use Brian2694\Toastr\Facades\Toastr;

class SmLunchMenuController extends Controller
{
    public function menu ()
    {
        $foods = SmFood::all();
        $foodtypes = SmFoodType::all();
    	return view('backEnd.kitchen.lunchmenu',compact('foods','foodtypes'));
    }

    public function menuweekday ($weekday)
    {
    	$foods = SmFood::all();
    	foreach ($foods as $food) {
    		$check = SmFoodWeekday::where('weekday',$weekday)->where('food_id',$food->id)->first();
    		if($check != null)
    		{
    			$food->isSelected = 1;
    		}
    		else
    		{
    			$food->isSelected = 0;
    		}
    	}

    	return view('backEnd.kitchen.weekdaymenu',compact('weekday','foods'));
    }

    public function savemenu (Request $request)
    {
    	try {
            $delete = SmFoodWeekday::where('weekday' , $request->weekday)->delete();
            
            	if(isset($request->hotdishes))
	            {
		            foreach ($request->hotdishes as $hotdish) {
		            	$add = new SmFoodWeekday();
		            	$add->weekday = $request->weekday;
		            	$add->food_id = $hotdish;
		            	$add->save();
		            }
	            }
	            if(isset($request->vegetarians))
	            {
		            foreach ($request->vegetarians as $vegetarian) {
		            	$add = new SmFoodWeekday();
		            	$add->weekday = $request->weekday;
		            	$add->food_id = $vegetarian;
		            	$add->save();
		            }
	            }
	            if(isset($request->salads))
	            {
		            foreach ($request->salads as $salad) {
		            	$add = new SmFoodWeekday();
		            	$add->weekday = $request->weekday;
		            	$add->food_id = $salad;
		            	$add->save();
		            }
	            }
	            if(isset($request->hams))
	            {
		            foreach ($request->hams as $ham) {
		            	$add = new SmFoodWeekday();
		            	$add->weekday = $request->weekday;
		            	$add->food_id = $ham;
		            	$add->save();
		            }
	            }
	            if(isset($request->snacks))
	            {
		            foreach ($request->snacks as $snack) {
		            	$add = new SmFoodWeekday();
		            	$add->weekday = $request->weekday;
		            	$add->food_id = $snack;
		            	$add->save();
		            }
	            }
            
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();

        } catch (\Exception $e) {
            Toastr::error('Operation Failed, Setup page is not complete', 'Failed');
            return redirect()->back();
        }
    }
    public function addfood (Request $request)
    {
        try {
            	$food = new SmFood();
            	$food->name = $request->name;
            	$food->type = $request->type;
            	$food->save();
            	return $food->id;

        } catch (\Exception $e) {
            Toastr::error('Operation Failed, Setup page is not complete', 'Failed');
            return redirect()->back();
        }
    
    }

    public function addfood2 (Request $request)
    {
        try {
            	$food = new SmFood();
            	$food->name = $request->name;
            	$food->type = $request->type;
            	$food->save();
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();

        } catch (\Exception $e) {
            Toastr::error('Operation Failed, Setup page is not complete', 'Failed');
            return redirect()->back();
        }
    
    }
    public function deletefood ($id)
    {
        //try {
            	$food = SmFood::find($id);
            
                $assignedfood = SmFoodWeekday::where('food_id',$id)->get();
                if($food != null)
                {
                    $del = $food->delete();
                }
                if($assignedfood->isNotEmpty() && $del)
                {
                     SmFoodWeekday::where('food_id',$id)->delete();
                }
            Toastr::success('Operation successful', 'Success');
            return redirect()->back();

        // } catch (\Exception $e) {
        //     Toastr::error('Operation Failed, Setup page is not complete', 'Failed');
        //     return redirect()->back();
        // }
    
    }
}
