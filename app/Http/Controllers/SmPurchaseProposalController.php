<?php

namespace App\Http\Controllers;

use App\SmItem;
use App\YearCheck;
use App\ApiBaseMethod;
use App\SmItemCategory;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\SmInventoryPayment;
use App\SmSupplier;
use App\SmStaff;

class SmPurchaseProposalController extends Controller
{
    /* Tran Thanh Phu - start */
    public function getSupplier(Request $request)
    {
        try{
            $itemCategory = SmItemCategory::find($request->id);
            $suppliers = SmSupplier::where('school_id',Auth::user()->school_id)->where('id', $itemCategory->supplier_id)->get();
            return response()->json([$suppliers]);
        } catch (\Exception $e) {
            return response()->json("", 404);
        }
    }

    public function getItem(Request $request)
    {
        try{
            $items = SmItem::where('school_id',Auth::user()->school_id)->where('item_category_id', $request->id)->get();
            return response()->json([$items]);
        } catch (\Exception $e) {
            return response()->json("", 404);
        }
    }

    public function getEditItem(Request $request)
    {
        try{
            $inventoryPayment = SmInventoryPayment::find($request->id);
            $items = SmItem::where('school_id',Auth::user()->school_id)->where('id', $inventoryPayment->item_id)->get();
            return response()->json([$items]);
        } catch (\Exception $e) {
            return response()->json("", 404);
        }
    }

    public function getEditSupplier(Request $request)
    {
        try{
            $inventoryPayment = SmInventoryPayment::find($request->id);
            $items = SmSupplier::where('school_id',Auth::user()->school_id)->where('id', $inventoryPayment->supplier_id)->get();
            return response()->json([$items]);
        } catch (\Exception $e) {
            return response()->json("", 404);
        }
    }

    public function changeActiveStatus(Request $request)
    {
        try{
            $inventoryPayment = SmInventoryPayment::find($request->id);
            if($inventoryPayment->active_status == 0){
                $inventoryPayment->active_status = 1;
            }
            else{
                $inventoryPayment->active_status = 0;
            }
            $inventoryPayment->update();
            return response()->json($inventoryPayment);
        } catch (\Exception $e) {
            return response()->json("", 404);
        }
    }

    public function index()
    {
        
        try{
            if(Auth::user()->role_id == 1){
            $inventoryPayment = SmInventoryPayment::where('school_id',Auth::user()->school_id)->get();
        }
        else{
            $inventoryPayment = SmInventoryPayment::where('school_id',Auth::user()->school_id)->where('staff_id', SmStaff::where('email', Auth::user()->email)->first()->id)->get();
            $inventoryPaymentAccepted = SmInventoryPayment::where('school_id',Auth::user()->school_id)->where('staff_id', SmStaff::where('email', Auth::user()->email)->first()->id)->where('active_status', 1)->get();
            foreach($inventoryPaymentAccepted as $i=>$item){
                if($item->active_status == 1){
                    Toastr::success('Your purchase proposal(' . SmItem::find($item->item_id)->item_name .') is accepted', 'Congratulation');
                    $item->active_status = 2;
                    $item->update();
                }
                if($i == $inventoryPaymentAccepted->count() - 1){
                    return redirect('/purchase-proposal');
                }
            }
        }
        $suppliers = SmSupplier::where('school_id',Auth::user()->school_id)->get();
        $items = SmItem::where('school_id',Auth::user()->school_id)->get();
        $itemCategories = SmItemCategory::where('school_id',Auth::user()->school_id)->get();
        return view('backEnd.inventory.purchaseProposal', compact('items', 'itemCategories', 'inventoryPayment', 'suppliers'));
        
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'category_id' => "required",
            'item_id' => "required",
            'supplier' => "required"
        ]);

        if ($validator->fails()) {
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                return ApiBaseMethod::sendError('Validation Error.', $validator->errors());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try{
            $inventoryPayment = new SmInventoryPayment();
            $inventoryPayment->amount = $request->quantity;
            $inventoryPayment->notes = $request->description;
            $inventoryPayment->active_status = 0;
            $inventoryPayment->supplier_id = $request->supplier;
            $inventoryPayment->school_id = Auth::user()->school_id;
            $inventoryPayment->academic_id = YearCheck::getAcademicId();
            $inventoryPayment->item_id = $request->item_id;
            $inventoryPayment->item_category_id = $request->category_id;
            $inventoryPayment->staff_id = SmStaff::where('email', Auth::user()->email)->first()->id;
            $results = $inventoryPayment->save();

            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                if ($results) {
                    return ApiBaseMethod::sendResponse(null, 'Added successfully');
                } else {
                    return ApiBaseMethod::sendError('Something went wrong, please try again');
                }
            } else {
                if ($results) {
                    Toastr::success('Operation successful', 'Success');
                    return redirect()->back();
                } else {
                    Toastr::error('Operation Failed', 'Failed');
                    return redirect()->back();
                }
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function view($id)
    {
        try {
            $inventoryPayment = SmInventoryPayment::find($id);
            $staffDetails = SmStaff::find($inventoryPayment->staff_id);
            $itemCategories = SmItemCategory::find($inventoryPayment->item_category_id);
            $suppliers = SmSupplier::find($inventoryPayment->supplier_id);
            $items = SmItem::find($inventoryPayment->item_id);
            return view('backEnd.inventory.viewPurchaseProposal', compact('staffDetails', 'itemCategories', 'suppliers', 'items', 'inventoryPayment'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function edit(Request $request, $id)
    {
        try{
            $inventoryPayment = SmInventoryPayment::where('school_id',Auth::user()->school_id)->get();
            $suppliers = SmSupplier::where('school_id',Auth::user()->school_id)->get();
            $items = SmItem::where('school_id',Auth::user()->school_id)->get();
            $itemCategories = SmItemCategory::where('school_id',Auth::user()->school_id)->get();
            $editData = SmInventoryPayment::find($id);
            return view('backEnd.inventory.purchaseProposal', compact('editData', 'items', 'itemCategories', 'inventoryPayment', 'suppliers'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $inventoryPayment = SmInventoryPayment::find($id);
            if(SmItemCategory::find($request->category_id)->id != $inventoryPayment->item_category_id && $request->item_id == "" 
                || SmItemCategory::find($request->category_id)->id != $inventoryPayment->item_category_id && $request->supplier == "")
            {
                Toastr::error('The item & supplier field is required', 'Failed');
                return redirect()->back();
            }
            $inventoryPayment->amount = $request->quantity;
            $inventoryPayment->notes = $request->description;
            $inventoryPayment->supplier_id = $request->supplier ? $request->supplier : $inventoryPayment->supplier_id;
            $inventoryPayment->school_id = Auth::user()->school_id;
            $inventoryPayment->academic_id = YearCheck::getAcademicId();
            $inventoryPayment->item_id = $request->item_id ? $request->item_id : $inventoryPayment->item_id;
            $inventoryPayment->item_category_id = $request->category_id;
            $results = $inventoryPayment->update();

            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                if ($results) {
                    return ApiBaseMethod::sendResponse(null, 'Category has been updated successfully');
                } else {
                    return ApiBaseMethod::sendError('Something went wrong, please try again');
                }
            } else {
                if ($results) {
                    Toastr::success('Operation successful', 'Success');
                    return redirect('purchase-proposal');
                } else {
                    Toastr::error('Operation Failed', 'Failed');
                    return redirect()->back();
                }
            }
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function deleteView(Request $request, $id)
    {
        try{
            $title = "Are you sure to detete this Purchase Proposal?";
            $url = url('purchase-proposal/' . $id . '/delete');
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                return ApiBaseMethod::sendResponse($id, null);
            }
            return view('backEnd.modal.delete', compact('id', 'title', 'url'));
        }catch (\Exception $e) {
           Toastr::error('Operation Failed', 'Failed');
           return redirect()->back();
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            $result = SmInventoryPayment::destroy($id);
            if ($result) {
                if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                    if ($result) {
                        return ApiBaseMethod::sendResponse(null, 'Item Category has been deleted successfully');
                    } else {
                        return ApiBaseMethod::sendError('Something went wrong, please try again.');
                    }
                } else {
                    if ($result) {
                        Toastr::success('Operation successful', 'Success');
                        return redirect()->back();
                    } else {
                        Toastr::error('Operation Failed', 'Failed');
                        return redirect()->back();
                    }
                }
            } else {
                Toastr::error('Operation Failed', 'Failed');
                return redirect()->back();
            }
        } catch (\Illuminate\Database\QueryException $e) {

            $msg = 'This data already used in  : ' . $tables . ' Please remove those data first';
            Toastr::error($msg, 'Failed');
            return redirect()->back();
          }

    }
    /* Tran Thanh Phu - end */
}
