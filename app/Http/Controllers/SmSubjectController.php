<?php

namespace App\Http\Controllers;

use App\SmSubject;
use App\tableList;
use App\YearCheck;
use App\ApiBaseMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\SmClass;
use Modules\StudyInformation\Entities\SmGrade;
use App\SmAcademicYear;
use Modules\StudyInformation\Entities\TestSchedule;

class SmSubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('PM');
        // User::checkAuth();
    }

    public function index(Request $request)
    {
        try {
            $subjects = SmSubject::where('active_status', 1)->orderBy('id', 'DESC')->where('school_id', Auth::user()->school_id)->get();
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                return ApiBaseMethod::sendResponse($subjects, null);
            }
            return view('backEnd.academics.subject', compact('subjects'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function store(Request $request)
    {
        $input = $request->all();
        if (ApiBaseMethod::checkUrl($request->fullUrl())) {
            $validator = Validator::make($input, [
                'subject_name' => "required|max:200",
                'subject_type' => "required",
                'subject_code' => "sometimes|nullable|max:30",
            ]);
        } else {
            $validator = Validator::make($input, [
                'subject_name' => "required|max:200",
                'subject_type' => "required",
                'subject_code' => "required|nullable|max:30",
            ]);
        }

        $is_duplicate = SmSubject::where('school_id', Auth::user()->school_id)->where('subject_name', $request->subject_name)->first();
        if ($is_duplicate) {
            Toastr::error('Duplicate name found!', 'Failed');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($validator->fails()) {
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                return ApiBaseMethod::sendError('Validation Error.', $validator->errors());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $find = SmSubject::where('school_id', Auth::user()->school_id)->where('subject_code', $request->subject_code)->first();
        if ($find != "") {
            Toastr::error('Duplicate subject code, Please try again', 'Failed');
            return redirect()->back()->withInput();
        }

        try {
            $subject = new SmSubject();
            $subject->subject_name = $request->subject_name;
            $subject->subject_type = $request->subject_type;
            $subject->subject_code = $request->subject_code;
            $subject->school_id = Auth::user()->school_id;
            $subject->academic_id = YearCheck::getAcademicId();
            $result = $subject->save();

            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                if ($result) {
                    return ApiBaseMethod::sendResponse(null, 'Subject has been created successfully');
                } else {
                    return ApiBaseMethod::sendError('Something went wrong, please try again.');
                }
            } else {
                if ($result) {
                    Toastr::success('Operation successful', 'Success');

                    return redirect()->back();
                } else {
                    Toastr::error('Operation Failed', 'Failed');
                    return redirect()->back();
                }
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function edit(Request $request, $id)
    {
        // try {
            $subject = SmSubject::find($id);
            $subjects = SmSubject::where('active_status', 1)->orderBy('id', 'DESC')->where('school_id', Auth::user()->school_id)->get();
            $belongToClasses = $subject->classes->pluck('id')->toArray();
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                $data = [];
                $data['subject'] = $subject->toArray();
                $data['subjects'] = $subjects->toArray();
                return ApiBaseMethod::sendResponse($data, null);
            }
            return view('backEnd.academics.subject', compact('subject', 'subjects', 'belongToClasses'));
        // } catch (\Exception $e) {
        //     Toastr::error('Operation Failed', 'Failed');
        //     return redirect()->back();
        // }
    }
    public function update(Request $request)
    {
        $input = $request->all();

        if (ApiBaseMethod::checkUrl($request->fullUrl())) {
            $validator = Validator::make($input, [
                'subject_name' => "required|max:200",
                'subject_type' => "required",
                'subject_code' => "required|nullable|max:30",
            ]);
        } else {
            $validator = Validator::make($input, [
                'subject_name' => "required|max:200",
                'subject_type' => "required",
                'subject_code' => "required|nullable|max:30",
            ]);
        }

        $is_duplicate = SmSubject::where('school_id', Auth::user()->school_id)->where('id', '!=', $request->id)->where('subject_name', $request->subject_name)->first();
        if ($is_duplicate) {
            Toastr::error('Duplicate name found!', 'Failed');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($validator->fails()) {
            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                return ApiBaseMethod::sendError('Validation Error.', $validator->errors());
            }
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $subject = SmSubject::find($request->id);
            $subject->subject_name = $request->subject_name;
            $subject->subject_type = $request->subject_type;
            $subject->subject_code = $request->subject_code;
            $result = $subject->save();

            $subject->classes()->detach();
            $subject->classes()->attach($request->classes);

            $lastestAcademic = SmAcademicYear::getLastestYear();
            foreach ($request->classes as $value) {
                SmGrade::whereClassId($value)->whereAcademic_year($lastestAcademic)->destroy();
                $students = SmGrade::whereClassId($value)->whereAcademic_year($lastestAcademic)->pluck('student_id')->unique()->toArray();
                foreach ($students as $student) {
                    for ($i = 1; $i <= 2; $i++) {
                        $sm_grade = new SmGrade();
                        $sm_grade->student_id = $student;
                        $sm_grade->class_id = $value;
                        $sm_grade->term = $i;
                        $sm_grade->subject_id = $subject->id;
                        $sm_grade->academic_year = $lastestAcademic;
                        $sm_grade->save();
                    }
                }
            }

            if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                if ($result) {
                    return ApiBaseMethod::sendResponse(null, 'Subject has been updated successfully');
                } else {
                    return ApiBaseMethod::sendError('Something went wrong, please try again.');
                }
            } else {
                if ($result) {
                    Toastr::success('Operation successful', 'Success');

                    return redirect()->back();
                } else {
                    Toastr::error('Operation Failed', 'Failed');
                    return redirect()->back();
                }
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
    public function delete(Request $request, $id)
    {
        try {
            $tables = tableList::getTableList('subject_id', $id);
            try {
                if ($tables == null) {
                    $delete_query = $section = SmSubject::destroy($request->id);
                    if (ApiBaseMethod::checkUrl($request->fullUrl())) {
                        if ($section) {
                            return ApiBaseMethod::sendResponse(null, 'Subject has been deleted successfully');
                        } else {
                            return ApiBaseMethod::sendError('Something went wrong, please try again.');
                        }
                    } else {
                        if ($delete_query) {
                            Toastr::success('Operation successful', 'Success');
                            return redirect('subject');
                        } else {
                            Toastr::error('Operation Failed', 'Failed');
                            return redirect()->back();
                        }
                    }
                } else {
                    $msg = 'This data already used in  : ' . $tables . ' Please remove those data first';
                    Toastr::error($msg, 'Failed');
                    return redirect()->back();
                }
            } catch (\Illuminate\Database\QueryException $e) {
                $msg = 'This data already used in  : ' . $tables . ' Please remove those data first';
                Toastr::error($msg, 'Failed');
                return redirect()->back();
            }
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function assignToClassesView($id)
    {
        try {
            $assignedClasses = SmSubject::find($id)->classes->modelKeys();
            $classes = SmClass::all()->except($assignedClasses);
            return view('backEnd.academics.assign_to_classes', compact('id', 'classes'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function assignToClasses($id, Request $request)
    {
        try {
            $subject = SmSubject::find($id);
            $subject->classes()->attach($request->classes);
        
            $lastestAcademic = SmAcademicYear::getLastestYear();
            foreach ($request->classes as $value) {
                $students = SmGrade::whereClassId($value)->whereAcademic_year($lastestAcademic)->pluck('student_id')->unique()->toArray();
                foreach ($students as $student) {
                    for ($i = 1; $i <= 2; $i++) {
                        $sm_grade = new SmGrade();
                        $sm_grade->student_id = $student;
                        $sm_grade->class_id = $value;
                        $sm_grade->term = $i;
                        $sm_grade->subject_id = $subject->id;
                        $sm_grade->academic_year = $lastestAcademic;
                        $sm_grade->save();
                    }
                }

                for ($i = 1; $i <= 2; $i++) {
                    $schedule = new TestSchedule();
                    $schedule->class_id = $value;
                    $schedule->subject_id = $subject->id;
                    $schedule->term = $i;
                    $schedule->save();
                }
            }
            Toastr::success('Assign Success', 'Success');
            return redirect()->back();
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function removeInClassesView($id)
    {
        try {
            $assignedClasses = SmSubject::find($id)->classes;
            return view('backEnd.academics.remove_in_classes', compact('id', 'assignedClasses'));
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }

    public function removeInClasses($id, Request $request)
    {
        try {
            $lastestAcademic = SmAcademicYear::getLastestYear();
            $subject = SmSubject::find($id);
            $subject->classes()->detach($request->classes);
            foreach ($request->classes as $class) {
                SmGrade::whereClass_id($class)->whereSubject_id($id)->whereAcademic_year($lastestAcademic)->delete();
                TestSchedule::whereClass_id($class)->whereSubject_id($id)->delete();
            }
            Toastr::success('Remove Success', 'Success');
            return redirect()->back();
        } catch (\Exception $e) {
            Toastr::error('Operation Failed', 'Failed');
            return redirect()->back();
        }
    }
}
