<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\FbCustomer;
use Session;
use Modules\CRM\Entities\CustomerFacebook;

class SocialAuthController extends Controller {
  public function redirect() {
    return Socialite::driver('facebook')->redirect();
  }

  public function callback() {
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $user = Socialite::driver('facebook')->stateless()->user();
    $findUser = FbCustomer::where('fb_id', $user->id)->first();
    if (!$findUser) {
        $newUser = FbCustomer::create([
          "name" => $user->name,
          "nickname" => $user->nickname,
          "avatar" => $user->avatar,
          "email" => $user->email,
          "fb_id" => $user->id,
          "profile" => $user->profileUrl
        ]);
      
        $customer = new CustomerFacebook();
        $customer->staff_id = 0;
        $customer->customer_name = $user->name;
        $customer->email = $user->email;
        $customer->year = date('Y');
        $customer->school_id = 1;
        $customer->fb_id = $user->id;
        $customer->save();
      
        Session::put('Fb', $newUser->fb_id);
    } else {
        $customer = CustomerFacebook::where('fb_id', $user->id)->first();
        $customer->updated_at = date('Y-m-d G:i:s');
        $customer->save();
        Session::put('Fb', $findUser->fb_id);
    }
    return redirect()->to('home');
  }
}

