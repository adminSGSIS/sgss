<?php

namespace App\Http\Controllers;

use App\SmStaff;
use Illuminate\Http\Request;

class UpdateInfo extends Controller {
  function skip(Request $request) {
    $staff = SmStaff::find($request->staff_id);
    $staff->updated_profile = (int)date("Y");
    $staff->update();
    return redirect()->back();
  }

  function update(Request $request) {
    $staff = SmStaff::find($request->staff_id);
    $staff->updated_profile = (int)date("Y");
    $staff->update();
    return redirect("/edit-staff/".$request->staff_id);
  }
}
