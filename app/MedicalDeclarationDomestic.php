<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalDeclarationDomestic extends Model
{
    protected $table = 'medical_declaration_domestic';
    protected $guarded = [];
}
