<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalDeclarationEntry extends Model
{
    protected $table = 'medical_declaration_entry';
    protected $guarded = [];
}
