<?php 
namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Builder;
Use App\SmNewsSubCategory;
Use App\SmNewsCategory;
use Modules\ChatBox\Entities\conversation;
use Modules\ChatBox\Entities\conversation_users;
use Modules\ChatBox\Entities\conversationDetails;
use Auth;
use App\User;
use App\SmStaff;
use Modules\ChatBox\Entities\MessageOneOne;
use Modules\ChatBox\Entities\MessageOneOneDetail;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
   public function boot()
    {
        Builder::defaultStringLength(191);
        view()->composer('frontEnd.home.layout.front_master',function($view){
            $categories = SmNewsCategory::where('category_name', "School Life")->first();
            $view->with('subCategories', []);
            if($categories){
                $view->with('subCategories', SmNewsSubCategory::where('category_id', $categories->id)->get());
            }
            $categories = SmNewsCategory::all();
            $bulletin = SmNewsSubCategory::where('category_id','16')->get();
            $view->with('categories',$categories,'bulletin',$bulletin);
            $view->with('bulletin',$bulletin);
        });
        // view()->composer('chatbox::minibox',function($view){
        //     $cons = User::find(Auth::user()->id)->Conversation;   

        //     $finds = MessageOneOne::where('id_from',Auth::user()->id)->orWhere('id_to',Auth::user()->id)->orderBy('updated_at','desc')->get();
        //     $recents = collect();
        //     $listid = [];
        //     foreach ($finds as $find) {
        //         if($find->id_from == Auth::user()->id)
        //         {
        //             $staff_photo = SmStaff::where('user_id',$find->id_to)->first();
        //             if(isset($staff_photo) && $staff_photo->staff_photo != null)
        //             {
        //                 $photo = $staff_photo->staff_photo;
        //             }
        //             else
        //             {
        //                 $photo = 'public/sg_star_logo.png';
        //             }
        //             $data =[
        //                 'full_name'=>User::find($find->id_to)->full_name,
        //                 'staff_photo'=> $photo,
        //                 'id' => $find->id_to,
        //                 'status' =>$find->status,
        //                 'lastest_sender'=>$find->lastest_sender,
        //             ];
        //             $recents->push($data);
        //             $listid [] =  $find->id_to;
        //         }
        //         elseif ($find->id_to == Auth::user()->id) 
        //         {
        //             $staff_photo = SmStaff::where('user_id',$find->id_from)->first();
        //             if(isset($staff_photo) && $staff_photo->staff_photo != null)
        //             {
        //                 $photo = $staff_photo->staff_photo;
        //             }
        //             else
        //             {
        //                 $photo = 'public/sg_star_logo.png';
        //             }
        //             $data =[
        //                 'full_name'=>User::find($find->id_from)->full_name ?? "",
        //                 'staff_photo'=> $photo,
        //                 'id' => $find->id_from,
        //                 'status' =>$find->status,
        //                 'lastest_sender'=>$find->lastest_sender,
        //             ];
        //             $recents->push($data);
        //             $listid [] =  $find->id_from;
        //         }


        //     }
             
        //     //dd($recents);
        //     $users = User::all()->where('id','!=',Auth::user()->id)->where('role_id','!=',2)->whereNotIn('id',$listid);

        //     foreach ($users as $user) {
        //         $find = SmStaff::where('user_id',$user->id)->first();
        //         if(isset($find) && $find->staff_photo != null)
        //             {
        //                 $user->image = $find->staff_photo;
        //             }
        //         else
        //             {
        //                 $user->image = '/public/sg_star_logo.png';
        //             }               
        //     }

        //     $cons = $cons->sortByDesc('updated_at');
        //     $view->with('cons',$cons);
        //     $view->with('users',$users);
        //     $view->with('recents',$recents);
        // });
    }

    public function register()
    {
        //
    }
}
