<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnToWork extends Model
{
    protected $table = 'return_to_work';

    protected $guarded = [];
}
