<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmAcademicYear extends Model
{
    public static function getLastestYear()
    {
        return SmAcademicYear::latest('created_at')->first()->year;
    }

    public static function getAcademicYears()
    {
        return SmAcademicYear::all()->pluck('year')->unique()->toArray();
    }
}
