<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class SmAssignClassTeacher extends Model
{
    //kiem tra giao vien chu nhiem
    public function checkRoleUser($class_id)
    {
        $check = SmAssignClassTeacher::whereClass_id($class_id)->first();
        if ($check && $check->classTeachers->first()->teacher_id == Auth::user()->id) {
            return 1;
        }
        return 0;
    }

    public function className()
    {
        return $this->belongsTo('App\SmClass', 'class_id', 'id');
    }
    public function section()
    {
        return $this->belongsTo('App\SmSection', 'section_id', 'id');
    }

    public function classTeachers()
    {
        return $this->hasMany('App\SmClassTeacher', 'assign_class_teacher_id', 'id');
    }
}
