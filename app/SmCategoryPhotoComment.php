<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmCategoryPhotoComment extends Model
{
    protected $table="sm_category_photo_comment";
}
