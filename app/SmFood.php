<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmFood extends Model
{
    protected $table = 'sm_foods';

    public function food_type ()
    {
    	return $this->BelongsTo('App\SmFoodType','type','id');
    }
}
