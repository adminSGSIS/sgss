<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmFoodType extends Model
{
    protected $table = 'sm_food_types';
}
