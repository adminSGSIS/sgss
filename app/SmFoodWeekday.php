<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmFoodWeekday extends Model
{
    protected $table = 'sm_assign_food_weekday';

    public function food ()
    {
    	return $this->BelongsTo('App\SmFood','food_id','id');
    }
}
