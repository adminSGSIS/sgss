<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class SmParent extends Model
{
    protected $fillable = [
        "user_id",
        "fathers_name",
        "fathers_nationality",
        "fathers_occupation",
        "fathers_company",
        "fathers_mobile",
        "fathers_email",
        "fathers_address",
        "fathers_work_address",
        "fathers_first_language",
        "fathers_language_level",
        "fathers_photo",
        "mothers_name",
        "mothers_nationality",
        "mothers_occupation",
        "mothers_company",
        "mothers_mobile",
        "mothers_email",
        "mothers_work_address",
        "mothers_work_address",
        "mothers_first_language",
        "mothers_language_level",
        "mothers_photo",
    ];
    public function parent_user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function childrens()
    {
        return $this->hasMany('App\SmStudent', 'parent_id', 'id');
    }
    
    public static function myChildrens()
    {
        try {
            if (Session::get('childrens') == "") {
                $user = Auth::user();
                $parent = SmParent::where('user_id', $user->id)->first();
                $childrens = SmStudent::where('parent_id', $parent->id)->where('active_status', 1)->get();
                Session::put('childrens', $childrens);
            }
            return Session::get('childrens');
        } catch (\Exception $e) {
            $data=[];
            return $data;
        }
    }
}
