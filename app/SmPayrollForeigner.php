<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmPayrollForeigner extends Model
{
    public $table = "sm_payroll_foreigner";
    public function staff()
    {
        return $this->belongsTo('App\SmStaff', 'staff_id', 'id');
    }
    public function deduction()
    {
        return $this->belongsTo('App\SmDeduct', 'deduction_id', 'id');
    }
}
