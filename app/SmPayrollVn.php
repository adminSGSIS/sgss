<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmPayrollVn extends Model
{
    public $table = "sm_payroll_vn";
    public function staff()
    {
        return $this->belongsTo('App\SmStaff', 'staff_id', 'id');
    }
    public function employer_expense()
    {
        return $this->belongsTo('App\EmployerExpense', 'employer_expense_id', 'id');
    }
    public function employee_contribution()
    {
        return $this->belongsTo('App\EmployeeContribution', 'employee_contribution_id', 'id');
    }
    public function deduction()
    {
        return $this->belongsTo('App\SmDeduct', 'deduction_id', 'id');
    }
}
