<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmSubject extends Model
{
    public function classes()
    {
        return $this->belongsToMany('App\SmClass');
    }
}
