<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFbCustomersTable extends Migration {
  public function up() {
    Schema::create('fb_customers', function (Blueprint $table) {
        $table->id();
        $table->timestamps();
        $table->string('fb_id');
        $table->string('name');
        $table->string('nickname')->nullable();
        $table->string('email');
        $table->string('avatar')->nullable();
        $table->string('profile')->nullable();
        });
  }

  public function down() {
    Schema::dropIfExists('fb_customers');
  }
}
