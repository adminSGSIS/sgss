<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmTestOnlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_test_onlines', function (Blueprint $table) {
            $table->id();
            $table->string('code',100);
            $table->longText('answer_1');
            $table->longText('answer_2');
            $table->longText('answer_3');
            $table->longText('answer_4');
            $table->longText('answer_5');
            $table->longText('answer_6');
            $table->longText('answer_7');
            $table->longText('answer_8');
            $table->longText('answer_9');
            $table->longText('answer_10');
            $table->longText('answer_11');
            $table->longText('answer_12');
            $table->longText('answer_13');
            $table->longText('answer_14');
            $table->longText('answer_15');
            $table->longText('answer_16');
            $table->longText('answer_17');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_test_onlines');
    }
}
