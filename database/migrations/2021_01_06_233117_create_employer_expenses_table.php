<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployerExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('social_ins')->nullable();
            $table->string('health_ins')->nullable();
            $table->string('unemployment_ins')->nullable();
            $table->string('trade_union')->nullable();
            $table->string('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_expenses');
    }
}
