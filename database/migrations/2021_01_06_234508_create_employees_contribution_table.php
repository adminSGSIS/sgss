<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesContributionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_contribution', function (Blueprint $table) {
            $table->increments('id');
            $table->string('social_ins')->nullable();
            $table->string('health_ins')->nullable();
            $table->string('unemployment_ins')->nullable();
            $table->string('union_fees')->nullable();
            $table->string('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_contribution');
    }
}
