<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmPayrollVnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_payroll_vn', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id')->unsigned()->index();
            $table->foreign('staff_id')->references('id')->on('sm_staffs')->onDelete('cascade');

            $table->integer('employer_expense_id')->unsigned()->index();
            $table->foreign('employer_expense_id')->references('id')->on('employer_expenses')->onDelete('cascade');

            $table->integer('employee_contribution_id')->unsigned()->index();
            $table->foreign('employee_contribution_id')->references('id')->on('employees_contribution')->onDelete('cascade');

            $table->integer('deduction_id')->unsigned()->index();
            $table->foreign('deduction_id')->references('id')->on('sm_deducts')->onDelete('cascade');

            $table->string('basic_salary')->nullable();
            $table->string('responsible_allowance')->nullable();
            $table->string('allowance')->nullable();
            $table->string('bus_duty_allowance')->nullable();
            $table->string('pre_sen_eca')->nullable();
            $table->string('other_allowance')->nullable();
            $table->string('total_salary')->nullable();
            $table->string('working_day')->nullable();
            $table->string('hours_before_convert')->nullable();
            $table->string('hours_after_convert')->nullable();
            $table->string('month_salary')->nullable();
            $table->string('contribute_insurance_salary')->nullable();
            $table->string('assessable_income')->nullable();
            $table->string('pit')->nullable();
            $table->string('net_salary')->nullable();
            $table->string('month_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_payroll_vn');
    }
}
