<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmPayrollForeignerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_payroll_foreigner', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id')->unsigned()->index();
            $table->foreign('staff_id')->references('id')->on('sm_staffs')->onDelete('cascade');

            $table->integer('deduction_id')->unsigned()->index();
            $table->foreign('deduction_id')->references('id')->on('sm_deducts')->onDelete('cascade');

            $table->string('basic_salary')->nullable();
            $table->string('allowance')->nullable();
            $table->string('total_salary')->nullable();
            $table->string('working_day')->nullable();
            $table->string('month_salary')->nullable();
            $table->string('contribute_insurance_salary')->nullable();
            $table->string('assessable_income')->nullable();
            $table->string('health_ins_employer')->nullable();
            $table->string('health_ins_employee')->nullable();
            $table->string('pit')->nullable();
            $table->string('net_salary')->nullable();
            $table->string('month_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_payroll_foreigner');
    }
}
