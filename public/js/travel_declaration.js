(function () {
    window.requestAnimFrame = (function (callback) {
        return (
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimaitonFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            }
        );
    })();

    var canvas = document.getElementById("sig-canvas");
    var ctx = canvas.getContext("2d");
    ctx.strokeStyle = "#222222";
    ctx.lineWidth = 4;

    var drawing = false;
    var mousePos = {
        x: 0,
        y: 0,
    };
    var lastPos = mousePos;

    canvas.addEventListener(
        "mousedown",
        function (e) {
            drawing = true;
            lastPos = getMousePos(canvas, e);
        },
        false
    );

    canvas.addEventListener(
        "mouseup",
        function (e) {
            drawing = false;
        },
        false
    );

    canvas.addEventListener(
        "mousemove",
        function (e) {
            mousePos = getMousePos(canvas, e);
        },
        false
    );

    // Add touch event support for mobile
    canvas.addEventListener("touchstart", function (e) {}, false);

    canvas.addEventListener(
        "touchmove",
        function (e) {
            var touch = e.touches[0];
            var me = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY,
            });
            canvas.dispatchEvent(me);
        },
        false
    );

    canvas.addEventListener(
        "touchstart",
        function (e) {
            mousePos = getTouchPos(canvas, e);
            var touch = e.touches[0];
            var me = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY,
            });
            canvas.dispatchEvent(me);
        },
        false
    );

    canvas.addEventListener(
        "touchend",
        function (e) {
            var me = new MouseEvent("mouseup", {});
            canvas.dispatchEvent(me);
        },
        false
    );

    function getMousePos(canvasDom, mouseEvent) {
        var rect = canvasDom.getBoundingClientRect();
        return {
            x: mouseEvent.clientX - rect.left,
            y: mouseEvent.clientY - rect.top,
        };
    }

    function getTouchPos(canvasDom, touchEvent) {
        var rect = canvasDom.getBoundingClientRect();
        return {
            x: touchEvent.touches[0].clientX - rect.left,
            y: touchEvent.touches[0].clientY - rect.top,
        };
    }

    function renderCanvas() {
        if (drawing) {
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
            lastPos = mousePos;
        }
    }

    // Prevent scrolling when touching the canvas
    document.body.addEventListener(
        "touchstart",
        function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        },
        false
    );
    document.body.addEventListener(
        "touchend",
        function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        },
        false
    );
    document.body.addEventListener(
        "touchmove",
        function (e) {
            if (e.target == canvas) {
                e.preventDefault();
            }
        },
        false
    );

    (function drawLoop() {
        requestAnimFrame(drawLoop);
        renderCanvas();
    })();

    function clearCanvas() {
        canvas.width = canvas.width;
    }

    // Set up the UI
    var sigText = document.getElementById("sig-dataUrl");
    var clearBtn = document.getElementById("sig-clearBtn");
    var submitBtn = document.getElementById("sig-submitBtn");
    var sig_img = document.querySelector("#sig_img");
    clearBtn.addEventListener(
        "click",
        function (e) {
            clearCanvas();
            sigText.value = "";
            sig_img.srcset = "";
        },
        false
    );
    submitBtn.addEventListener(
        "click",
        function (e) {
            var dataUrl = canvas.toDataURL();
            sigText.value = dataUrl;
            sig_img.srcset = dataUrl;
        },
        false
    );
})();

var no = document.querySelector("#no");
var yes = document.querySelector("#yes");
var sub_component = document.querySelector(".sub_component");
yes.addEventListener(
    "click",
    function (e) {
        sub_component.style.display = "none";
        var length = document.querySelector("#form").childElementCount;
        if (length >= 1) {
            for (let index = 0; index < length - 1; index++) {
                document.querySelector("#form").children[1].remove();
            }
        }
    }
);

no.addEventListener(
    "click",
    function (e) {
        sub_component.style.display = "block";
        if (document.querySelector("#form").childElementCount <= 1) {
            $('#form').append('<div class="form-row"><div class="col-3"><input required class="form-control form-control-sm" type="text" name="country_traveled[]" autocomplete="off"></div><div class="col-2"><input required class="form-control form-control-sm" type="text" autocomplete="off" name="city_traveled[]"></div><div class="col-2"><input required class="primary-input form-control form-control-sm date" type="date" value="Date" name="from[]"></div><div class="col-2"><input required class="primary-input form-control form-control-sm date" type="date" value="Date" name="to[]"></div><div class="col-3"><input required class="form-control form-control-sm" type="text" name="purpose[]" autocomplete="off"></div></div>');
        }
    }
);

var addnewrow = document.querySelector('#addnewrow');
addnewrow.addEventListener(
    "click",
    function (e) {
        $('#form').append('<hr><div class="form-row"><div class="col-3"><input required class="form-control form-control-sm" type="text" name="country_traveled[]" autocomplete="off"></div><div class="col-2"><input required class="form-control form-control-sm" type="text" autocomplete="off" name="city_traveled[]"></div><div class="col-2"><input required class="primary-input form-control form-control-sm date" type="date" value="Date" name="from[]"></div><div class="col-2"><input required class="primary-input form-control form-control-sm date" type="date" value="Date" name="to[]"></div><div class="col-3"><input required class="form-control form-control-sm" type="text" name="purpose[]" autocomplete="off"></div></div>');
    }
);