<div style="height: 450px">
    <form method="POST" action="subject/remove-in-classes/{{ $id }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        @csrf
        <div style="margin: auto; width: 35%;">
            <input style="width: 15px; height: 15px; margin-top: 5px" type="checkbox" onclick="checkAll(this)" class="common-checkbox" id="check_all">
            <label style="margin-top: 10px" class="ml-4" for="check_all">Check All</label>
        </div>
        
        <div style="overflow-y: scroll; height: 280px">
            @foreach ($assignedClasses as $index=>$value)
                <div class="mt-2 mb-4 class-name">
                    <input type="checkbox" class="mr-2 common-checkbox" name="classes[]"
                    value="{{$value->id}}" id="class{{ $index }}{{$value->id}}">
                    <label for="class{{ $index }}{{$value->id}}">{{$value->class_name}}</label>
                </div>
            @endforeach 
        </div>
        <button class="primary-btn fix-gr-bg mt-3 optionBtn" style="margin-left: 150px" type="submit">Apply</button>  
    </form>
</div>
<script>
    function checkAll(x){
        if(x.checked == true){
            for (var i = 0; i < document.getElementsByClassName('class-name').length; i++){
                document.getElementsByClassName('class-name')[i].children[0].checked = true;
            }
        }else{
            for (var i = 0; i < document.getElementsByClassName('class-name').length; i++){
                document.getElementsByClassName('class-name')[i].children[0].checked = false;
            }
        }
    }
</script>