@extends('backEnd.master')
@section('css')
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 12px !important;
        vertical-align: middle !important;
    }
    #table{
        display: none;
    }
    .uppercase{
        text-transform: uppercase;
    }
    @media print {
        .print{
            display: none;
        }
        footer{
            display: none;
        }
        #message_box{
            display: none !important;
        }
        #table{
            display: revert;
        }
    }
</style>
@endsection
@section('mainContent')
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box print">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Salary (Vietnamese)</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                @if(@in_array(137, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                <a href="/salary/list/option">List Salary</a>
                @endif
            </div>
        </div>
    </div>
</section>

@if(@in_array(137, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
<div class="row print">
    <div class="col-lg-8 col-md-6">
        <div class="main-title">
            <h3 class="mb-30">DELETE</h3>
        </div>
    </div>
    <div class="col-lg-4 text-right">
        <a href="/salary/vn/delete/{{ $payroll_vn->id }}" class="primary-btn small fix-gr-bg">
            <span class="ti-plus pr-2"></span>
            DELETE
        </a>
    </div>
</div>
<hr class="print">
@endif

<div class="row print">
    <div class="col-lg-8 col-md-6">
        <div class="main-title">
            <h3 class="mb-30">EXPORT</h3>
        </div>
    </div>
    <div class="col-lg-4 text-right">
        <a href="{{url('salary/export-salary')}}/{{$payroll_vn->staff_id}}/{{$payroll_vn->month_year}}/vn" class="primary-btn small fix-gr-bg" target="_blank">
            <span class="ti-plus pr-2"></span>
            PDF
        </a>
        <a href="#" onclick="exportTableToExcel('table', 'Payroll-Vn {{ $payroll_vn->month_year }}')" class="primary-btn small fix-gr-bg">
            <span class="ti-plus pr-2"></span>
            EXCEL
        </a>
    </div>
</div>

<table id="table">
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="8">
            <img src="{{ asset('public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png') }}" width="250" alt="">
        </th>
        <th colspan="6" class="uppercase">PAYROLL {{ $payroll_vn->month_year }}</th>
    </tr>
    <tr>
        <th colspan="11"></th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="11">STAFF INFORMATION</th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Staff Name</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->staff->full_name) ? $payroll_vn->staff->full_name : ""}} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Role</th>
        <td colspan="6" style="border: 1px solid black;">{{isset($payroll_vn->staff->roles->name) ? $payroll_vn->staff->roles->name : ""}} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Staff No</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->staff->staff_no) ? $payroll_vn->staff->staff_no : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="11">ALLOWANCES</th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Responsible/Allowance</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->responsible_allowance) ? $payroll_vn->responsible_allowance : ""  }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Allowance</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->allowance) ? $payroll_vn->allowance : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Bus Duty/Allowance</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->bus_duty_allowance) ? $payroll_vn->bus_duty_allowance : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">PRE | SEN | ECA</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->pre_sen_eca) ? $payroll_vn->pre_sen_eca : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Other Allowance</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->other_allowance) ? $payroll_vn->other_allowance : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="11">SALARY</th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Basic Salary</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->basic_salary) ? $payroll_vn->basic_salary : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Total Salary</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->total_salary) ? $payroll_vn->total_salary : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Working Day</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->working_day) ? $payroll_vn->working_day : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Month/Salary</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->month_salary) ? $payroll_vn->month_salary : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Salary(Contribute Insurance)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->contribute_insurance_salary) ? $payroll_vn->contribute_insurance_salary : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Assessable Income</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->assessable_income) ? $payroll_vn->assessable_income : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="11">OVER TIME</th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Hours Before Convert</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->hours_before_convert) ? $payroll_vn->hours_before_convert : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Hours After Convert</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->hours_after_convert) ? $payroll_vn->hours_after_convert : ""}} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="11">EMPLOYER EXPENSE (23.5%)</th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Social Ins (17.5%)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employer_expense->social_ins) ? $payroll_vn->employer_expense->social_ins : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Health Ins (3%)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employer_expense->health_ins) ? $payroll_vn->employer_expense->health_ins : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Unemployee Ins (1%)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employer_expense->unemployee_ins) ? $payroll_vn->employer_expense->unemployee_ins : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Trade Union (2%)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employer_expense->trade_union) ? $payroll_vn->employer_expense->trade_union : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Total</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employer_expense->total) ? $payroll_vn->employer_expense->total : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="11">EMPLOYEES CONTRIBUTE (11.5%)</th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Social Ins (8%)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employee_contribution->social_ins) ? $payroll_vn->employee_contribution->social_ins : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Health Ins (1.5%)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employee_contribution->health_ins) ? $payroll_vn->employee_contribution->health_ins : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Unemployee Ins (1%)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employee_contribution->unemployee_ins) ? $payroll_vn->employee_contribution->unemployee_ins : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Union Fees (2%)</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employee_contribution->union_fees) ? $payroll_vn->employee_contribution->union_fees : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Total</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->employee_contribution->total) ? $payroll_vn->employee_contribution->total : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="11">DEDUCTION</th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Personal</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->deduction->personal) ? $payroll_vn->deduction->personal : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Dependent</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->deduction->dependent) ? $payroll_vn->deduction->dependent : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Deduction Dependent</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->deduction->deduction_dependent) ? $payroll_vn->deduction->deduction_dependent : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Total</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->deduction->total) ? $payroll_vn->deduction->total : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th colspan="11">OTHERS</th>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">PIT</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->pit) ? $payroll_vn->pit : "" }} &nbsp;</td>
    </tr>
    <tr>
        <th colspan="8" class="print"></th>
        <th style="text-align: left; border: 1px solid black;" colspan="6">Net Salary</th>
        <td colspan="6" style="border: 1px solid black;">{{ isset($payroll_vn->net_salary) ? $payroll_vn->net_salary : "" }} &nbsp;</td>
    </tr>
</table>

<section class="admin-visitor-area up_st_admin_visitor print">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="main-title xs_mt_0 mt_0_sm">
                    <h3 class="mb-3">Update Salary [{{ $payroll_vn->month_year }}]</h3>
                </div>
            </div>
        </div>
        @if(@in_array(137, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
        <form action="/salary/vn/edit/{{ $payroll_vn->id }}" method="post">
            @csrf
        @endif
            <div class="row">
                <div class="col-lg-12">
                    @if(session()->has('message-success'))
                    <div class="alert alert-success">
                        {{ session()->get('message-success') }}
                    </div>
                    @elseif(session()->has('message-danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('message-danger') }}
                    </div>
                    @endif
                    <div class="white-box">
                        <div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Staff information</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" type="text" readonly value="{{ isset($payroll_vn->staff->full_name) ? $payroll_vn->staff->full_name : ""}}">
                                        <label>Staff Name</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" type="text" readonly value="{{isset($payroll_vn->staff->roles->name) ? $payroll_vn->staff->full_name : ""}}">
                                        <label>role</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" type="text" readonly value="{{ isset($payroll_vn->staff->staff_no) ? $payroll_vn->staff->staff_no : "" }}">
                                        <label>staff no</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Allowances</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="responsible_allowance" type="text" value="{{ isset($payroll_vn->responsible_allowance) ? $payroll_vn->responsible_allowance : ""  }}">
                                        <label>Responsible/Allowance</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="allowance" type="text" value="{{ isset($payroll_vn->allowance) ? $payroll_vn->allowance : "" }}">
                                        <label>Allowance</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="bus_duty_allowance" type="text" value="{{ isset($payroll_vn->bus_duty_allowance) ? $payroll_vn->bus_duty_allowance : "" }}">
                                        <label>Bus duty/Allowance</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="pre_sen_eca" type="text" value="{{ isset($payroll_vn->pre_sen_eca) ? $payroll_vn->pre_sen_eca : "" }}">
                                        <label>PRE | SEN | ECA</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="other_allowance" type="text" value="{{ isset($payroll_vn->other_allowance) ? $payroll_vn->other_allowance : "" }}">
                                        <label>Other Allowance</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Salary</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="basic_salary" type="text" value="{{ isset($payroll_vn->basic_salary) ? $payroll_vn->basic_salary : "" }}">
                                        <label>Basic Salary</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="total_salary" type="text" value="{{ isset($payroll_vn->total_salary) ? $payroll_vn->total_salary : "" }}">
                                        <label>Total Salary</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="working_day" type="text" value="{{ isset($payroll_vn->working_day) ? $payroll_vn->working_day : "" }}">
                                        <label>Working Day</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="month_salary" type="text" value="{{ isset($payroll_vn->month_salary) ? $payroll_vn->month_salary : "" }}">
                                        <label>Month/Salary</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="contribute_insurance_salary" type="text" value="{{ isset($payroll_vn->contribute_insurance_salary) ? $payroll_vn->contribute_insurance_salary : "" }}">
                                        <label>Salary(Contribute insurance)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="assessable_income" type="text" value="{{ isset($payroll_vn->assessable_income) ? $payroll_vn->assessable_income : "" }}">
                                        <label>Assessable Income</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Over time</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="hours_before_convert" type="text" value="{{ isset($payroll_vn->hours_before_convert) ? $payroll_vn->hours_before_convert : "" }}">
                                        <label>Hours before convert</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="hours_after_convert" type="text" value="{{ isset($payroll_vn->hours_after_convert) ? $payroll_vn->hours_after_convert : ""}}">
                                        <label>Hours after convert</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Employer expense (23.5%)</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="social_ins_employer_expense" type="text" value="{{ isset($payroll_vn->employer_expense->social_ins) ? $payroll_vn->employer_expense->social_ins : "" }}">
                                        <label>Social Ins (17.5%)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="health_ins_employer_expense" type="text" value="{{ isset($payroll_vn->employer_expense->health_ins) ? $payroll_vn->employer_expense->health_ins : "" }}">
                                        <label>Health Ins (3%)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="unemployee_ins_employer_expense" type="text" value="{{ isset($payroll_vn->employer_expense->unemployee_ins) ? $payroll_vn->employer_expense->unemployee_ins : "" }}">
                                        <label>Unemployee ins (1%)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="trade_union_employer_expense" type="text" value="{{ isset($payroll_vn->employer_expense->trade_union) ? $payroll_vn->employer_expense->trade_union : "" }}">
                                        <label>Trade union (2%)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="total_employer_expense" type="text" value="{{ isset($payroll_vn->employer_expense->total) ? $payroll_vn->employer_expense->total : "" }}">
                                        <label>Total</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Employees Contribute (11.5%)</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="social_ins_employees_contribute" type="text" value="{{ isset($payroll_vn->employee_contribution->social_ins) ? $payroll_vn->employee_contribution->social_ins : "" }}">
                                        <label>Social Ins (8%)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="health_ins_employees_contribute" type="text" value="{{ isset($payroll_vn->employee_contribution->health_ins) ? $payroll_vn->employee_contribution->health_ins : "" }}">
                                        <label>Health Ins (1.5%)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="unemployee_ins_employees_contribute" type="text" value="{{ isset($payroll_vn->employee_contribution->unemployee_ins) ? $payroll_vn->employee_contribution->unemployee_ins : "" }}">
                                        <label>unemployee ins (1%)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="union_fees_employees_contribute" type="text" value="{{ isset($payroll_vn->employee_contribution->union_fees) ? $payroll_vn->employee_contribution->union_fees : "" }}">
                                        <label>Union fees (1%)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="total_employees_contribute" type="text" value="{{ isset($payroll_vn->employee_contribution->total) ? $payroll_vn->employee_contribution->total : "" }}">
                                        <label>Total</label>
                                        <span class="focus-border"></span>) ? 
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Deduction</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="personal" type="text" value="{{ isset($payroll_vn->deduction->personal) ? $payroll_vn->deduction->personal : "" }}">
                                        <label>personal</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="dependent" type="text" value="{{ isset($payroll_vn->deduction->dependent) ? $payroll_vn->deduction->dependent : "" }}">
                                        <label>dependent</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="deduction_dependent" type="text" value="{{ isset($payroll_vn->deduction->deduction_dependent) ? $payroll_vn->deduction->deduction_dependent : "" }}">
                                        <label>Deduction dependent</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="total_deduction" type="text" value="{{ isset($payroll_vn->deduction->total) ? $payroll_vn->deduction->total : "" }}">
                                        <label>total</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Others</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="pit" type="text" value="{{ isset($payroll_vn->pit) ? $payroll_vn->pit : "" }}">
                                        <label>PIT</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="net_salary" type="text" value="{{ isset($payroll_vn->net_salary) ? $payroll_vn->net_salary : "" }}">
                                        <label>net salary</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>
                            @if(@in_array(137, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                            <div class="row mt-40">
                                <div class="col-lg-12 text-center">
                                    <button class="primary-btn fix-gr-bg">
                                        <span class="ti-check"></span>
                                        Save
                                    </button>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @if(@in_array(137, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
        </form>
        @endif
    </div>
</section>
@endsection

@section('script')
<script>
    // function exportPdf() {
    //     window.print();
    // }

    function exportTableToExcel(tableID, filename = ''){
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
            
            // Specify file name
            filename = filename?filename+'.xls':'excel_data.xls';
            
            // Create download link element
            downloadLink = document.createElement("a");
            
            document.body.appendChild(downloadLink);
            
            if(navigator.msSaveOrOpenBlob){
                var blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob( blob, filename);
            }else{
                // Create a link to the file
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
            
                // Setting the file name
                downloadLink.download = filename;
                
                //triggering the function
                downloadLink.click();
            }
        }
</script>
@endsection