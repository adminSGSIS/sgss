@extends('backEnd.master')
@section('mainContent')
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-8 col-md-6">
                <div class="main-title">
                    <h3 class="mb-30">@lang('lang.select_criteria')</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <form action="/salary/me/search" method="post">
                        @csrf
                        <input type="hidden" name="vn_or_foreigner" value="{{ $vn_or_foreigner }}">
                        <div class="row">
                            <div class="col-lg-12">
                                <select class="niceSelect w-100 bb" name="month_year">
                                    <option data-display="Select">select</option>
                                    @foreach ($my_payroll as $item)
                                        <option value="{{ $item->month_year }}">Payroll {{ $item->month_year }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-lg-12 mt-20 text-right">
                                <button type="submit" class="primary-btn small fix-gr-bg">
                                    <span class="ti-search pr-2"></span>
                                    search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
