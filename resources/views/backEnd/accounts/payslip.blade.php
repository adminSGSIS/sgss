<!DOCTYPE html>
<html>
<head>
	<title>Export Salary</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">

		<div style="text-align: center;">

			<img width="35%" src="https://sgstar.edu.vn/public/images/logo.png">
			<h2>Payslip {{$payroll->month_year}}</h2>
		</div>
		
		<table class="table table-bordered table-responsive">
		    <thead>
		      <tr>
		        <th>Information</th>
		        <th>Detail</th>
		        
		        
		      </tr>
		    </thead>
		    <tbody>
		      <tr>
		        <td width="50%">Full Name</td>
		        <td>{{isset($payroll->staff->full_name) ? $payroll->staff->full_name : ""}}</td>
		        
		      </tr>
		      <tr>
		        <td>Employee Code</td>
		        <td>{{isset($payroll->staff->staff_no) ? $payroll->staff->staff_no : ""}}</td>
		        
		      </tr>
		      <tr>
		        <td>Department</td>
		        <td>{{ isset($payroll->staff->roles->name) ? $payroll->staff->roles->name : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td>Standard working days</td>
		        <td>{{ isset($payroll->working_day) ? $payroll->working_day : "" }}</td>
		       
		      </tr>
		      <tr>
		        <td>Actual working days</td>
		        <td>{{ isset($payroll->working_day) ? $payroll->working_day : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td>Overtime</td>
		        <td>{{ isset($payroll->hours_after_convert) ? $payroll->hours_after_convert : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td>Absent with pay</td>
		        <td>Absent with pay</td>
		        
		      </tr>
		      <tr>
		        <td>Absent without pay</td>
		        <td>Absent without pay</td>
		        
		      </tr>
		      <tr>
		        <td>Basic salary</td>
		        <td>{{ isset($payroll->month_salary) ? $payroll->month_salary : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td>Actual salary</td>
		        <td>{{ isset($payroll->total_salary) ? $payroll->total_salary : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td><b>ECA</b></td>
		        <td>{{ isset($payroll->pre_sen_eca) ? $payroll->pre_sen_eca : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td>Bus</td>
		        <td>{{ isset($payroll->bus_duty_allowance) ? $payroll->bus_duty_allowance : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td><b>Deduction</b></td>
		        <td>{{ isset($payroll->deduction->total_deduction) ? $payroll->deduction->total_deduction : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td>Insurance(Social,Health & Unemployment)</td>
		        <td>{{ isset($payroll->employer_expense->total) ? $payroll->employer_expense->total : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td>Trade Union (if any)</td>
		        <td>{{ isset($payroll->employer_expense->trade_union) ? $payroll->employer_expense->trade_union : "" }}</td>
		        
		      </tr>
		      <tr>
		        <td>Personal Incone tax</td>
		        <td>Tax</td>
		        
		      </tr>
		      <tr>
		        <td>Net Income</td>
		        <td>{{isset($payroll->net_salary) ? $payroll->net_salary : ""}}</td>
		        
		      </tr>
		      <tr>
		        <td>Deduction Salary (as Labour law)</td>
		        <td>{{ isset($payroll->deduction->personal) ? $payroll->deduction->personal : "" }}</td>
		        
		      </tr>
		    </tbody>
		  </table>
		  <div class="row">
	    <div class="col-lg-4"></div>
	    <div class="col-lg-4"></div>
	    <div class="col-lg-4">
	        <b>Tổng giám đốc (General Director)</b><br>
            Ký,đóng dấu,Ghi rõ họ tên (Sign,Stamp,fullname)
	    </div>
	</div>
	</div>
	

</body>
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
	})
</script>
</html>