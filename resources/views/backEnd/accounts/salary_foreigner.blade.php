@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Salary (Foreigner)</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="/salary/list/option">List Salary</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="main-title xs_mt_0 mt_0_sm">
                    <h3 class="mb-3">Input Salary [{{ Carbon::now()->format('F Y') }}]</h3>
                </div>
            </div>
        </div>
        <form action="/salary/foreigner" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    @if(session()->has('message-success'))
                    <div class="alert alert-success">
                        {{ session()->get('message-success') }}
                    </div>
                    @elseif(session()->has('message-danger'))
                    <div class="alert alert-danger">
                        {{ session()->get('message-danger') }}
                    </div>
                    @endif
                    <div class="white-box">
                        <div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Staff information</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('staff_id') ? ' is-invalid' : '' }}" name="staff_id" id="staff_id">
                                            <option data-display="Staff Name *" value="">Staff Name *</option>
                                            @foreach($staffs as $staff)
                                            <option value="{{$staff->id}}" {{old('staff') == $staff->id? 'selected': ''}}>{{ $staff->full_name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('staff_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('staff_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" type="text" id="role" readonly value=" ">
                                        <label>role</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" type="text" id="staff_no" readonly value=" ">
                                        <label>staff no</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Allowances</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="input-effect mb-4 mt-4">
                                        <input class="primary-input read-only-input" name="allowance" type="text" required>
                                        <label>Allowance</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Salary</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="basic_salary" type="text" required>
                                        <label>Basic Salary</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="working_day" type="text" required>
                                        <label>working day</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="total_salary" type="text" required>
                                        <label>Total Salary</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="month_salary" type="text" required>
                                        <label>Month/Salary</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="contribute_insurance_salary" type="text" required>
                                        <label>Salary(Contribute insurance)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="assessable_income" type="text" required>
                                        <label>Assessable Income</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Insurance</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="health_ins_employer" type="text" required>
                                        <label>Health Ins (employer)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="health_ins_employee" type="text" required>
                                        <label>Health Ins (employee)</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Deduction</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="personal" type="text" required>
                                        <label>personal</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="dependent" type="text" required>
                                        <label>dependent</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="deduction_dependent" type="text" required>
                                        <label>Deduction dependent</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="total_deduction" type="text" required>
                                        <label>total</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4 class="stu-sub-head">Others</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30">
                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="pit" type="text" required>
                                        <label>PIT</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input class="primary-input read-only-input" name="net_salary" type="text" required>
                                        <label>net salary</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-40 mt-30 ml-2">
                                <input type="checkbox" class="common-checkbox" id="checkbox" name="send">
                                <label for="checkbox">Send his/her email (maybe take a minute)</label>
                            </div>
                            
                            <div class="row mt-40">
                                <div class="col-lg-12 text-center">
                                    <button class="primary-btn fix-gr-bg">
                                        <span class="ti-check"></span>
                                        @lang('lang.save')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection