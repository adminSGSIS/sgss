@extends('backEnd.master')
@section('css')
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 12px !important;
        vertical-align: middle !important;
    }

    .table{
        width: 100%;
        overflow-x: scroll;
    }
    ::-webkit-scrollbar {
        height: 5px !important; 
    }
    footer{
        display: none;
    }
    .uppercase{
        text-transform: uppercase;
    }
    .sign{
        display: none;
    }
    @media print {
        .table{
            transform:rotate(270deg);
            overflow: visible;
            margin-left: -20%; 
        }
        #message_box{
            display: none !important;
        }
        .print{
            display: none;
        }
        .sign{
            display: revert;
        }
        .detail{
            display: none;
        }
    }
</style>
@endsection
@section('mainContent')
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box print">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Salary (Foreigner) {{ $payroll_foreigner[0]->month_year }}</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="/salary/foreigner">ADD SALARY</a>
            </div>
        </div>
    </div>
</section>
<div class="row print">
    <div class="col-lg-8 col-md-6">
        <div class="main-title">
            <h3 class="mb-30">EXPORT</h3>
        </div>
    </div>
    <div class="col-lg-4 text-right">
        <a href="#" onclick="exportPdf()" class="primary-btn small fix-gr-bg">
            <span class="ti-plus pr-2"></span>
            PDF
        </a>
        <a href="#" onclick="exportTableToExcel('table', 'Payroll-Foreigner {{ $payroll_foreigner[0]->month_year }}')" class="primary-btn small fix-gr-bg">
            <span class="ti-plus pr-2"></span>
            EXCEL
        </a>
    </div>
</div>
    <div class="table">
        <table id="table">
            <tr>
                <th>
                    <img src="{{ asset('public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png') }}" width="110" alt="">
                </th>
                <th colspan="16" class="uppercase">PAYROLL {{ $payroll_foreigner[0]->month_year }}</th>
            </tr>
            <tr>
                <th class="detail" rowspan="2">Details</th>
                <th rowspan="2">S/N</th>
                <th rowspan="2">Full Name</th>
                <th rowspan="2">Basic Salary</th>
                <th rowspan="2">Allowances</th>
                <th rowspan="2">Total Salary</th>
                <th rowspan="2">Working Days</th>
                <th rowspan="2">Month Salary</th>
                <th rowspan="2">Salary Contribute Insurance</th>
                <th rowspan="2">Assessable Income</th>
                <th rowspan="2">Health Ins (Employer)</th>
                <th rowspan="2">Health Ins (Employee)</th>
                <th colspan="4">Deduction</th>
                <th rowspan="2">PIT</th>
                <th rowspan="2">Net Salary</th>
            </tr>
            <tr>
                <td>Personal</td>
                <td>Dependent</td>
                <td>Deduction Dependent</td>
                <td>Total</td>
            </tr>
            @foreach ($payroll_foreigner as $item)
                <tr>
                    <td class="detail">
                        <a href="/salary/foreigner/edit/{{ $item->id }}" class="btn btn-outline-dark">View</a>
                    </td>
                    <td>{{ $loop->index + 1}}</td>
                    <td>{{ $item->staff->full_name }}</td>
                    <td>{{ $item->basic_salary }}</td>
                    <td>{{ $item->allowance }}</td>
                    <td>{{ $item->total_salary }}</td>
                    <td>{{ $item->working_day }}</td>
                    <td>{{ $item->month_salary }}</td>
                    <td>{{ $item->contribute_insurance_salary }}</td>
                    <td>{{ $item->assessable_income }}</td>
                    <td>{{ $item->health_ins_employer }}</td>
                    <td>{{ $item->health_ins_employee }}</td>
                    <td>{{ $item->deduction->personal }}</td>
                    <td>{{ $item->deduction->dependent }}</td>
                    <td>{{ $item->deduction->deduction_dependent }}</td>
                    <td>{{ $item->deduction->total }}</td>
                    <td>{{ $item->pit }}</td>
                    <td>{{ $item->net_salary }}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="17" style="text-align: left">TOTAL</td>
            </tr>
            <tr class="sign">
                <td colspan="3">General Director</td>
                <td colspan="11"></td>
                <td colspan="3">Prepared By</td>
            </tr>
            <tr class="sign"><td colspan="32" >&nbsp;</td></tr>
            <tr class="sign">
                <td colspan="3">VO THI PHUONG THAO</td>
                <td colspan="11"></td>
                <td colspan="3">MAI THI THU THANH</td>
            </tr>
        </table>
    </div>
    {{ $paginate->links() }}
@endsection


@section('script')
<script>
    function exportPdf() {
        var td_length = @json($td_length);
        var percent = 65.5 - (td_length * 2.5);
        $('head').append('<style type="text/css" media="print">.table{margin-top:' + percent + '%} </style>');
        window.print();
    }

    function exportTableToExcel(tableID, filename = ''){
        var detail = document.getElementsByClassName("detail").length;
        for (let index = 0; index < detail; index++) {
            document.getElementsByClassName("detail")[0].remove();
        }

        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(tableID);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
        
        // Specify file name
        filename = filename?filename+'.xls':'excel_data.xls';
        
        // Create download link element
        downloadLink = document.createElement("a");
        
        document.body.appendChild(downloadLink);
        
        if(navigator.msSaveOrOpenBlob){
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob( blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        
            // Setting the file name
            downloadLink.download = filename;
            
            //triggering the function
            downloadLink.click();
        }
        location.reload();
    }
</script>
@endsection