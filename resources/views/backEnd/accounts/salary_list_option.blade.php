@extends('backEnd.master')
@section('mainContent')
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-8 col-md-6">
                <div class="main-title">
                    <h3 class="mb-30">@lang('lang.select_criteria')</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <form action="/salary/list/search" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <select class="niceSelect w-100 bb" name="vn_foreigner" id="vn_foreigner">
                                    <option data-display="Select">select</option>
                                    <option value="1">Vietnamese</option>
                                    <option value="2">Foreigner</option>
                                </select>
                            </div>

                            <div class="col-lg-6" id="month_year_div">
                                <select class="niceSelect w-100 bb" name="month_year" id="month_year">
                                    <option data-display="Select">select</option>
                                </select>
                            </div>

                            <div class="col-lg-12 mt-20 text-right">
                                <button type="submit" class="primary-btn small fix-gr-bg">
                                    <span class="ti-search pr-2"></span>
                                    search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
