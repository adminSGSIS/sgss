@extends('backEnd.master')
<?php require_once('resources\views\backEnd\function\convertToNumber.blade.php') ?>
@section('mainContent')

<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('lang.add_expense') </h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard') </a>
                <a href="{{url('add-expense')}}">@lang('lang.accounts')</a>
                <a href="{{url('add-expense')}}">@lang('lang.add_expense')</a>
            </div>
        </div>
    </div>
</section>
<div class="d-flex align-items-end flex-column" style="padding:10px">
    <button id="print_btn" onClick="window.print()" type="button" class="btn btn-primary">Print</button>
</div>
<style type="text/css">
    @media print {
        #print_btn {
            display :  none;
        }
        footer{
            display : none;
        }
        .sms-breadcrumb {
            display :  none;
        } 
    }
    td{
        width: 80%;
        border-bottom: 1px dashed black;
        padding-left:5px;
     }
     .section {
        background-color: white;
        padding: 50px;
    }

</style>
<section class="section" id="print_content">
    <div class="container-fluid p-0">
        @if(isset($add_income))
        @if(in_array(140, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )

        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('add-income')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')
                </a>
            </div>
        </div>
        @endif
        @endif
      
        <div class="row">
            <div class="col-lg-12">
                    <tbody>
                        <td>
                        <section style="padding:0 50px 100px 50px">
  
                            <div class="d-flex justify-content-between">
                                <div class="d-flex flex-column" style="max-width:400px">
                                    <span style="font-weight: bold">
                                        <img src="{{url('public/images/favicon.png')}}" alt="logo" style="width:60px;height:50px" >
                                       <div style="float:right;padding-right:120px">
                                        <a>SAIGON STAR </a><br>
                                        <a>INTERNATIONAL SCHOOL</a> 
                                    </div>
                                      
                                       
                                    </span>
                                    <span style="font-weight: bold">CÔNG TY TNHH QUỐC TẾ NGÔI SAO SÀI GÒN</span>
                                    <span>Địa chỉ: Khu dân cư số 5, P.Thạnh Mỹ Lợi, Quận 2,TPHCM</span>
                                    <span>Mã số thuế : 030 414 5048</span>
                                    <span>Điện thoại: (08) 3742 7827  - 3742 3222</span>
                                    <span>Fax: (08) 3742 5436</span>
                                </div>

                                <div class="d-flex flex-column " style="text-align:center">
                                    <span style="font-weight: bold" >Mẫu số C31-BB</span> 
                                    <span>(Ban hành theo QĐ số 19/2006/QĐ-BTC</span>
                                    <span>Ngày 30/3/2006 của Bộ trưởng Bộ Tài Chính)</span>
                                </div>

                            </div>

                            @foreach($expense_detail as $expense_detail)
                            
                            <div class="d-flex align-items-start flex-column" style="padding-left:80%">
                                <span>Quyển số: </span>
                                <span style="color:red">Số PC: {{sprintf("%'.06d\n",$expense_detail->id)}}</span>
                                <span>Nợ:</span>
                                <span>Có: </span>
                            </div>

                            <div class="d-flex align-items-center flex-column">
                                <h1>PHIẾU CHI</h1>
                                <span>{{@$expense_detail->created_at != ""? App\SmGeneralSettings::DateConvater(@$expense_detail->created_at):''}}</span>
                            </div>
                            <br><br>
                            <table style="margin-left:50px">
                                <tr>
                                    <th>Họ tên người nhận tiền:</th>
                                    <td>{{$expense_detail->name}}</td>   
                                </tr>
                                <tr>
                                    <th>Địa chỉ:</th>
                                    <td></td>    
                                </tr>
                                <tr>
                                    <th>Lý do chi:</th>
                                    <td>{{$expense_detail->description}}</td>    
                                </tr>
                                <tr>
                                    <th>Số tiền:</th>
                                    <td>{{$expense_detail->amount}}</td>    
                                </tr>
                                <tr>
                                    <th>Số tiền bằng chữ:</th>
                                    <td>{{convertNumberToWord($expense_detail->amount)}} dollars</td>
                                </tr>
                                <tr>
                                    <th>Kèm theo:</th>
                                    <td></td>
                                </tr>
                            </table>
                           
                            <br><br><br>
                            <div class="d-flex justify-content-between " style="padding:0 50px 0 30px;text-align:center">
                                <div class="d-flex flex-column">
                                    <h3>Thủ trưởng đơn vị</h3>
                                    <span>(Ký, họ tên, đóng dấu)</span>
                                    
                                </div>
                                <div>
                               
                                    <h3>Kế toán</h3>
                                    <span>(Ký, họ tên)</span>
                                    <br><br><br><br>
                                    <span>{{isset($expense_detail->ACHead->head)? $expense_detail->ACHead->head: ''}}</span>
                                
                                </div class="d-flex flex-column">
                                <div>
                                    <h3>Người nhận tiền</h3>
                                    <span>(Ký, họ tên)</span>
                                </div>

                            </div>
                        @endforeach
                    </section>
                        
                        
                        </td>
                    </tbody>
         
            </div>
        </div>
    </div>
</section>



@endsection