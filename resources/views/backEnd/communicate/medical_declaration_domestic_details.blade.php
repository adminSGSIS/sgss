@extends('backEnd.master')
@section('css')
    <style>
        .centerTd td,
        .centerTh th {
            text-align: center;
        }
        #table{
            display: none;
        }
        @media print {
            footer,
            .print {
                display: none;
            }

            #message_box {
                display: none !important;
            }
        }

    </style>
@endsection
@section('mainContent')
    <table id="table">
        <tr>
            <th colspan="6"></th>
            <th colspan="4">
                <img src="{{ asset('public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png') }}" width="200" alt="">
            </th>
            <th colspan="6" class="uppercase">MEDICAL DECLARATION</th>
        </tr>
        <tr></tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">MEDICAL DECLARATION</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Full Name</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->full_name }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Date Of Birth</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->date_of_birth }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Gender</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->gender }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Nationality</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->nationality }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Passport / ID Card</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->id_card }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">CONTACT INFORMATION IN VIETNAM</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Province</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->province }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">District</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->district }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Ward</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->ward }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Address</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->address }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Phone number</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->phone_number }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Email</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->email }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">SYMPTOMS</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Fever</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_1 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Cough</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_2 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Shortness of breath</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_3 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Pneumonia</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_4 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Sore throat</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_5 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Tired</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_6 }} &nbsp;</td>
        </tr>
        
        <tr>
            <th colspan="6"></th>
            <th colspan="10">During the past 14 days, you were in contact with</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Sick or suspected person, infected with COVID-19</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->contact_with_1 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">People from countries with COVID-19 disease</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->contact_with_2 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">People with manifestations (fever, cough, shortness of breath, pneumonia)</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->contact_with_3 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">Which of the following diseases do you currently have?</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Chronic liver disease</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_1 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Chronic blood disease</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_2 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Chronic lung disease</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_3 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Chronic kidney disease</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_4 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Heart-related diseaes</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_5 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">High Blood Pressure</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_6 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">HIV</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_7 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Transplant recipients, Mercury bone</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_8 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Diabetes</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_9 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Cancer</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->diseases_10 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Are you pregnant?</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->pregnant }} &nbsp;</td>
        </tr>
    </table>
    <section class="sms-breadcrumb mb-40 white-box print">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>MEDICAL DECLARATION DOMESTIC</h1>
                <div class="bc-pages">
                    <a href="{{ url('dashboard') }}">@lang('lang.dashboard')</a>
                </div>
            </div>
        </div>
    </section>
    <div class="row print">
        <div class="col-lg-8 col-md-6">
            <div class="main-title">
                <h3 class="mb-30">EXPORT</h3>
            </div>
        </div>
        <div class="col-lg-4 text-right">
            <a href="#" onclick="exportPdf()" class="primary-btn small fix-gr-bg">
                <span class="ti-plus pr-2"></span>
                PDF
            </a>
            <a href="#" onclick="exportTableToExcel('table', 'Medical Declaration Domestic')" class="primary-btn small fix-gr-bg">
                <span class="ti-plus pr-2"></span>
                EXCEL
            </a>
        </div>
    </div>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->full_name }}" readonly>
                                            <label>Full Name</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->date_of_birth }}" readonly>
                                            <label>Date Of Birth</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->gender }}" readonly>
                                            <label>Gender</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->nationality }}" readonly>
                                            <label>Nationality</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->id_card }}" readonly>
                                            <label>Passport / Id Card</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">Contact address in Vietnam</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->province }}" readonly>
                                            <label>Province</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->district }}" readonly>
                                            <label>District</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->ward }}" readonly>
                                            <label>Ward</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->address }}" readonly>
                                            <label>Address</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->phone_number }}" readonly>
                                            <label>Phone number</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->email }}" readonly>
                                            <label>Email</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">Symptoms</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_1 }}" readonly>
                                            <label>Fever</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_2 }}" readonly>
                                            <label>Cough</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_3 }}" readonly>
                                            <label>Shortness of breath</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_4 }}" readonly>
                                            <label>Pneumonia</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_5 }}" readonly>
                                            <label>Sore throat</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_6 }}" readonly>
                                            <label>Tired</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">In the last 14 days, which regions/ countries/ territories have you traveled to</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->traveled }}" readonly>
                                            <span class="focus-border"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">During the past 14 days, you were in contact with</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->contact_with_1 }}" readonly>
                                            <label>Sick or suspected person, infected with COVID-19</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->contact_with_2 }}" readonly>
                                            <label>People from countries with COVID-19 disease</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->contact_with_3 }}" readonly>
                                            <label>People with manifestations (fever, cough, shortness of breath, pneumonia)</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">During the past 14 days, you were in contact with</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_1 }}" readonly>
                                            <label>Chronic liver disease</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_2 }}" readonly>
                                            <label>Chronic blood disease</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_3 }}" readonly>
                                            <label>Chronic lung disease</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_4 }}" readonly>
                                            <label>Chronic kidney disease</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_5 }}" readonly>
                                            <label>Heart-related diseaes</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_5 }}" readonly>
                                            <label>High Blood Pressure</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_6 }}" readonly>
                                            <label>HIV</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_7 }}" readonly>
                                            <label>Transplant recipients, Mercury bone</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_8 }}" readonly>
                                            <label>Diabetes</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->diseases_9 }}" readonly>
                                            <label>Cancer</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->pregnant }}" readonly>
                                            <label>Are you pregnant?</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
<script>
    function exportPdf() {
        window.print();
    }

    function exportTableToExcel(tableID, filename = ''){
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
            
            // Specify file name
            filename = filename?filename+'.xls':'excel_data.xls';
            
            // Create download link element
            downloadLink = document.createElement("a");
            
            document.body.appendChild(downloadLink);
            
            if(navigator.msSaveOrOpenBlob){
                var blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob( blob, filename);
            }else{
                // Create a link to the file
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
            
                // Setting the file name
                downloadLink.download = filename;
                
                //triggering the function
                downloadLink.click();
            }
        }
</script>
@endsection