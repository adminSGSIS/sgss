@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
   #table{
       display: none;
   }
</style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>MEDICAL DECLARATION ENTRY</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                </div>
            </div>
        </div>
    </section>
    
    <div class="row">
        <div class="col-lg-8 col-md-6 col-sm-6">
            <div class="main-title mt_0_sm mt_0_md">
                <h3 class="mb-30">Export excel</h3>
            </div>
        </div>

        <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg col-sm-6 text_sm_right">
            <a href="{{ url('admin/medical-declaration/entry/export') }}" class="primary-btn small fix-gr-bg">
                <span class="ti-plus pr-2"></span>
                EXPORT </a>
        </div>
    </div>

    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Data List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success-delete') }}
                                            </div>
                                        @elseif(session()->has('message-danger-delete'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger-delete') }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            <tr class="centerTh">
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Nationality</th>
                                <th>Date of birth</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($all_items as $value)
                                    <tr class="centerTd">
                                        <td>{{$value->full_name}}</td>
                                        <td>{{$value->email}}</td>
                                        <td>{{$value->nationality}}</td>
                                        <td>{{$value->date_of_birth}}</td>
                                        <td>
                                            <a href="{{url('admin/medical-declaration/entry/details'.'/'.$value->id)}}" class="dropdown-item" title="Travel Declaration Details" data-modal-size="modal-md">
                                                View
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    
@endsection