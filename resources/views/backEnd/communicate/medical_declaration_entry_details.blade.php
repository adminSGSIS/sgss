@extends('backEnd.master')
@section('css')
    <style>
        .centerTd td,
        .centerTh th {
            text-align: center;
        }
        #table{
            display: none;
        }
        @media print {
            footer,
            .print {
                display: none;
            }

            #message_box {
                display: none !important;
            }
        }

    </style>
@endsection
@section('mainContent')
    <table id="table">
        <tr>
            <th colspan="6"></th>
            <th colspan="4">
                <img src="{{ asset('public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png') }}" width="200" alt="">
            </th>
            <th colspan="6" class="uppercase">MEDICAL DECLARATION</th>
        </tr>
        <tr></tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">MEDICAL DECLARATION</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Gate</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->gate }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Full Name</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->full_name }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Date Of Birth</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->date_of_birth }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Gender</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->gender }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Nationality</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->nationality }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Passport</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->passport }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Tarvel Information</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->travel_information }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Transportation No</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->transportation_no }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Seat No</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->seat_no }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Departure date</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->departure_date }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Immigration date</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->immigration_date }}&nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">PLACE OF DEPARTURE</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Country</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->departure_country }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Province</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->departure_province }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">PLACE OF DESTINATION</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Country</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->destination_country }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Province</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->destination_province }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">In the past 14 days, which province / city / territory / country have you been to?</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->past_14_days }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">PROPERTY AFTER QUARANTINE</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Province</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->after_quarantine_province }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">District</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->after_quarantine_district }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Ward</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->after_quarantine_ward }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Quarantine place</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->after_quarantine_place }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">CONTACT INFORMATION IN VIETNAM</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Province</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->information_in_vn_province }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">District</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->information_in_vn_district }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Ward</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->information_in_vn_ward }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Staying address</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->information_in_vn_staying_address }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Phone number</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->phone_number }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Email</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->email }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">SYMPTOMS</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Fever</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_1 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Cough</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_2 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Difficulty of breathing</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_3 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Sore throat</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_5 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Vomiting</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_6 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Diarrhea</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_7 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Skin haemorrhage</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->symptoms_8 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">List of vaccines or biologicals used</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->vaccines }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th colspan="10">HISTORY OF EXPOSURE</th>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">History of exposure</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->history_of_exposure_1 }} &nbsp;</td>
        </tr>
        <tr>
            <th colspan="6"></th>
            <th style="text-align: left; border: 1px solid black;" colspan="5">Gate</th>
            <td colspan="5" style="border: 1px solid black;">{{ $medical->history_of_exposure_2 }} &nbsp;</td>
        </tr>
    </table>
    <section class="sms-breadcrumb mb-40 white-box print">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>MEDICAL DECLARATION ENTRY</h1>
                <div class="bc-pages">
                    <a href="{{ url('dashboard') }}">@lang('lang.dashboard')</a>
                </div>
            </div>
        </div>
    </section>
    <div class="row print">
        <div class="col-lg-8 col-md-6">
            <div class="main-title">
                <h3 class="mb-30">EXPORT</h3>
            </div>
        </div>
        <div class="col-lg-4 text-right">
            <a href="#" onclick="exportPdf()" class="primary-btn small fix-gr-bg">
                <span class="ti-plus pr-2"></span>
                PDF
            </a>
            <a href="#" onclick="exportTableToExcel('table', 'Medical Declaration Entry')" class="primary-btn small fix-gr-bg">
                <span class="ti-plus pr-2"></span>
                EXCEL
            </a>
        </div>
    </div>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-effect">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->gate }}" readonly>
                                            <label>Gate</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->full_name }}" readonly>
                                            <label>Full Name</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->date_of_birth }}" readonly>
                                            <label>Date Of Birth</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->gender }}" readonly>
                                            <label>Gender</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->nationality }}" readonly>
                                            <label>Nationality</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->passport }}" readonly>
                                            <label>Passport</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->travel_information }}" readonly>
                                            <label>Travel Information</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->transportation_no }}" readonly>
                                            <label>Transportation no</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->seat_no }}" readonly>
                                            <label>Seat no</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->departure_date }}" readonly>
                                            <label>Departure date</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->immigration_date }}" readonly>
                                            <label>Immigration date</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">Place of departure</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->departure_country }}" readonly>
                                            <label>Country</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->departure_province }}" readonly>
                                            <label>Province</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">Place of destination</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->destination_country }}" readonly>
                                            <label>Country</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->destination_province }}" readonly>
                                            <label>Province</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->past_14_days }}" readonly>
                                            <label>In the past 14 days, which province / city / territory / country have you
                                                been to?</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">Property after quarantine</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->after_quarantine_province }}" readonly>
                                            <label>Province</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->after_quarantine_district }}" readonly>
                                            <label>District</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->after_quarantine_ward }}" readonly>
                                            <label>Ward</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->after_quarantine_place }}" readonly>
                                            <label>Quarantine place</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">Contact information in VietNam</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->information_in_vn_province }}" readonly>
                                            <label>Province</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->information_in_vn_district }}" readonly>
                                            <label>District</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->information_in_vn_ward }}" readonly>
                                            <label>Ward</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->information_in_vn_staying_address }}" readonly>
                                            <label>Staying address</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->phone_number }}" readonly>
                                            <label>Phone number</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->email }}" readonly>
                                            <label>Email</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">Symptoms</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_1 }}" readonly>
                                            <label>Fever</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_2 }}" readonly>
                                            <label>Cough</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_3 }}" readonly>
                                            <label>Difficulty of breathing</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_4 }}" readonly>
                                            <label>Sore throat</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_5 }}" readonly>
                                            <label>Vomiting</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_6 }}" readonly>
                                            <label>Diarrhea</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_7 }}" readonly>
                                            <label>Skin haemorrhage</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->symptoms_8 }}" readonly>
                                            <label>Rash</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->vaccines }}" readonly>
                                            <label>List of vaccines or biologicals used</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="main-title mb-3">
                                            <h4 class="stu-sub-head">History of exposure: During the last 14 days</h4>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->history_of_exposure_1 }}" readonly>
                                            <label>Visit any poultry farm / living animal market / slaughter house / contact to animal </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                        <div class="input-effect mt-3">
                                            <input class="primary-input form-control" type="text"
                                                value="{{ $medical->history_of_exposure_2 }}" readonly>
                                            <label>Care for a sick person of communicables diseases</label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
<script>
    function exportPdf() {
        window.print();
    }

    function exportTableToExcel(tableID, filename = ''){
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
            
            // Specify file name
            filename = filename?filename+'.xls':'excel_data.xls';
            
            // Create download link element
            downloadLink = document.createElement("a");
            
            document.body.appendChild(downloadLink);
            
            if(navigator.msSaveOrOpenBlob){
                var blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob( blob, filename);
            }else{
                // Create a link to the file
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
            
                // Setting the file name
                downloadLink.download = filename;
                
                //triggering the function
                downloadLink.click();
            }
        }
</script>
@endsection