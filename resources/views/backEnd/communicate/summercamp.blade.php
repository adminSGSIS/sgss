@extends('backEnd.master')
@section('css')
    <style>
        .centerTd td,
        .centerTh th {
            text-align: center;
        }

    </style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>SENDING SUMMERCAMP MAIL</h1>
                <div class="bc-pages">
                    <a href="{{ url('dashboard') }}">@lang('lang.dashboard')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                SENDING MAIL
                            </h3>
                        </div>
                        <form action="/send-summercamp-mail" method="POST">
                            @csrf
                            <div class="white-box">
                                <div class="add-visitor">
                                    <div class="row p-2">
                                        <div class="col-12">
                                            <div class="input-effect">
                                                <input class="primary-input form-control" type="text" name="title"
                                                    autocomplete="off" required>
                                                <label>Title *<span></span> </label>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-30">
                                            <div class="d-flex">
                                                <p class="text-uppercase fw-500 mr-4">Send To</p>
                                                <div class="mr-30">
                                                    <input type="checkbox" name="subject_type" id="send_to"
                                                        value="parents" checked required>
                                                    <label for="send_to">Parents</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-40">
                                <div class="col-lg-12 text-center">
                                    <button type="submit" class="primary-btn fix-gr-bg">
                                        <span class="ti-check"></span>
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
