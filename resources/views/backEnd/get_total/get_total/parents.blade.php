@extends('backEnd.master')
@section('mainContent')
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
            <div class="row mt-40">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 no-gutters">
                            <div class="main-title">
                                <h3 class="mb-0">Parent List</h3>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Father Name</th>
                                        <th>Father Mobile</th>
                                        <th>Mother Name</th>
                                        <th>Mother Mobile</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($parents as $parent)
                                    <tr>
                                        <td>{{ $parent->fathers_name != null ? $parent->fathers_name : ''}}</td>
                                        <td>{{ $parent->fathers_mobile != null ? $parent->fathers_mobile : '' }}</td>
                                        <td>{{ $parent->mothers_name != null ? $parent->mothers_name : '' }}</td>
                                        <td>{{ $parent->mothers_mobile != null ? $parent->mothers_mobile : '' }}</td>
                                        <td>{{ $parent->guardians_email != null ? $parent->guardians_email : '' }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
@endsection
