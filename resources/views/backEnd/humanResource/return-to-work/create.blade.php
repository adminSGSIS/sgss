@extends('backEnd.master')
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>RETURN TO WORK FORM</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('return-to-work/list')}}">List</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <form action="/return-to-work" method="POST">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h3 class="mb-30">
                                        Self-Certification (to be completed by employee)
                                    </h3>
                                </div>
                            
                                <div class="white-box">
                                    <div class="add-visitor">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 mb-3">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" name="name" autocomplete="off" value="{{ old('name') }}" required>
                                                    <label>Name<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 mb-3">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="email" name="email" autocomplete="off" value="{{ old('email') }}" required>
                                                    <label>Email<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 mb-3">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" name="job_title" autocomplete="off" value="{{ old('job_title') }}" required>
                                                    <label>Job title<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 mb-3">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control date"
                                                    type="text" name="first_day_of_absence" autocomplete="off" value="{{ old('first_day_of_absence') }}" required>
                                                    <label>First Day of Absence<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 mb-3">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control date"
                                                    type="text" name="last_day_of_absence" autocomplete="off" value="{{ old('last_day_of_absence') }}" required>
                                                    <label>Last Day of Absence<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-12 mb-3">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="number" name="number_of_working_days_absent" autocomplete="off" value="{{ old('number_of_working_days_absent') }}" required>
                                                    <label>Number of working days absent<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-12 mb-3">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" name="report_absence_to" autocomplete="off" value="{{ old('report_absence_to') }}" required>
                                                    <label>I reported my absence to<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-12 mb-3">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control date"
                                                    type="text" name="time_of_call" autocomplete="off" value="{{ old('time_of_call') }}" required>
                                                    <label>Time of phone call<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-12 mb-3">
                                                <div class="input-effect">
                                                    <label class="d-block">State briefly why you were unfit for work (specify the nature of illness or injury)<span></span> </label>
                                                    <textarea name="specify_injury" class="form-control" rows="4"></textarea>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                            <div class="col-12 mb-3">
                                                <div class="input-effect">
                                                    <label class="d-block">If absent for two or more days, which doctor did you visit?<span></span> </label>
                                                    <textarea name="which_doctor" class="form-control" rows="4"></textarea>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            <div class="row mt-4">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-title">
                                <h3 class="mb-30">
                                    Return To Work Discussion (to be completed by line manager)
                                </h3>
                            </div>
                            <div class="white-box">
                                <div class="add-visitor">
                                    <div class="row">
                                        <div class="col-6 mb-3">
                                            <div class="input-effect">
                                                <input class="primary-input form-control"
                                                type="text" name="manager_name" autocomplete="off" value="{{ old('manager_name') }}" required>
                                                <label>Manager’s Name<span></span> </label>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 mb-3">
                                            <div class="input-effect">
                                                <input class="primary-input form-control"
                                                type="email" name="manager_email" autocomplete="off" value="{{ old('manager_email') }}" required>
                                                <label>Email<span></span> </label>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <div class="input-effect">
                                                <input class="primary-input form-control date"
                                                type="text" name="date_of_discussion" autocomplete="off" value="{{ old('date_of_discussion') }}" required>
                                                <label>Date of RTW Discussion<span></span> </label>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>
                                        <div class="col-12 mt-3 mb-3">
                                            <label>Has the employee confirmed that they are fit for work?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q1_yes" type="radio" name="q1" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q1_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q1_no" type="radio" name="q1" value="No" class="common-radio relationButton" >
                                                    <label for="q1_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Has the employee been updated on the issues which have arisen during their
                                                absence?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q2_yes" type="radio" name="q2" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q2_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q2_no" type="radio" name="q2" value="No" class="common-radio relationButton" >
                                                    <label for="q2_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Did the employee follow the reporting procedure?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q3_yes" type="radio" name="q3" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q3_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q3_no" type="radio" name="q3" value="No" class="common-radio relationButton" >
                                                    <label for="q3_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Is the employee entitled to sick pay? (a doctor’s certificate must be presented for
                                                absences of more than one day)</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q4_yes" type="radio" name="q4" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q4_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q4_no" type="radio" name="q4" value="No" class="common-radio relationButton" >
                                                    <label for="q4_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Is the absence related to a work related accident? (If yes, has the appropriate
                                                documentation been completed?)</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q5_yes" type="radio" name="q5" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q5_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q5_no" type="radio" name="q5" value="No" class="common-radio relationButton" >
                                                    <label for="q5_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Is a phased return appropriate?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q6_yes" type="radio" name="q6" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q6_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q6_no" type="radio" name="q6" value="No" class="common-radio relationButton" >
                                                    <label for="q6_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mb-3">
                                            <div class="input-effect">
                                                <label class="d-block">Details of phased return/restrictions<span></span> </label>
                                                <textarea name="q6_sub" class="form-control" rows="4"></textarea>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Is there an underlying reason for the absence?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q7_yes" type="radio" name="q7" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q7_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q7_no" type="radio" name="q7" value="No" class="common-radio relationButton" >
                                                    <label for="q7_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mb-3">
                                            <div class="input-effect">
                                                <label class="d-block">Reason<span></span> </label>
                                                <textarea name="q7_sub" class="form-control" rows="4"></textarea>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Has there been a change in the employee’s circumstances?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q8_yes" type="radio" name="q8" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q8_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q8_no" type="radio" name="q8" value="No" class="common-radio relationButton" >
                                                    <label for="q8_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mb-3">
                                            <div class="input-effect">
                                                <label class="d-block">Summary of change<span></span> </label>
                                                <textarea name="q8_sub" class="form-control" rows="4"></textarea>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Is the absence related to the employee’s disability or pregnancy?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q9_yes" type="radio" name="q9" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q9_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q9_no" type="radio" name="q9" value="No" class="common-radio relationButton" >
                                                    <label for="q9_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Does the employee have a temporary or permanent mobility issue that would
                                                require a Personal Emergency Evacuation Plan?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q10_yes" type="radio" name="q10" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q10_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q10_no" type="radio" name="q10" value="No" class="common-radio relationButton" >
                                                    <label for="q10_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mb-3">
                                            <div class="input-effect">
                                                <input class="primary-input form-control"
                                                type="text" name="q10_sub" autocomplete="off" value="{{ old('q10_sub') }}">
                                                <label>Total number of working days lost due to absence in the last 12 months<span></span> </label>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Has the employee reached the monitoring absence trigger (3 occasions in a 12
                                                month period)?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q11_yes" type="radio" name="q11" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q11_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q11_no" type="radio" name="q11" value="No" class="common-radio relationButton" >
                                                    <label for="q11_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Is there concern about the level of sickness absence?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q12_yes" type="radio" name="q12" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q12_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q12_no" type="radio" name="q12" value="No" class="common-radio relationButton" >
                                                    <label for="q12_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Has the employee been informed about the importance of regular attendance and
                                                the fact that poor attendance can lead to informal and formal monitoring under the
                                                School’s Managing Absence Policy?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q13_yes" type="radio" name="q13" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q12_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q13_no" type="radio" name="q13" value="No" class="common-radio relationButton" >
                                                    <label for="q13_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mt-3 mb-3">
                                            <label>Is there a need for follow up action such as an Attendance Review Meeting?</label>
                                            <div class="d-flex">
                                                <div class="mr-30">
                                                    <input id="q14_yes" type="radio" name="q14" value="Yes" class="common-radio relationButton" checked>
                                                    <label for="q14_yes">Yes</label>
                                                </div>
                                                <div class="mr-30">
                                                    <input id="q14_no" type="radio" name="q14" value="No" class="common-radio relationButton" >
                                                    <label for="q14_no">No</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 mb-3">
                                            <div class="input-effect">
                                                <label class="d-block">Note of any other comments or issues raised, and any further action agreed<span></span> </label>
                                                <textarea name="q14_sub" class="form-control" rows="4"></textarea>
                                                <span class="focus-border textarea"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-40">
                <div class="col-lg-12 text-center">
                    <button class="primary-btn fix-gr-bg">
                        <span class="ti-check"></span>
                            @lang('lang.save')
                    </button>
                </div>
            </div>
        </form>
    </section>
    
@endsection