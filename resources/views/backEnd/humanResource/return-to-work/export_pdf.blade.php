<div class="pdf">
    <img src="/public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png" alt="" width="200">
    <h3 class="text-center">RETURN TO WORK FORM</h3>
    <p>This form is to be completed on the first day back after any period of sickness and
        will be kept on file.</p>
    <h1>Part 1: Self-Certification (to be completed by employee)</h1>
    <table class="table">
        <tbody>
            <tr>
                <td>Name: {{ $returnToWork->name }}</td>
                <td>Job title: {{ $returnToWork->job_title }}</td>
            </tr>
            <tr>
                <td>First Day of Absence: {{ $returnToWork->first_day_of_absence }}</td>
                <td>Last Day of Absence: {{ $returnToWork->last_day_of_absence }}</td>
            </tr>
            <tr>
                <td>First Day of Absence: {{ $returnToWork->first_day_of_absence }}</td>
                <td>Last Day of Absence: {{ $returnToWork->last_day_of_absence }}</td>
            </tr>
            <tr>
                <td>Number of working days absent: {{ $returnToWork->number_of_working_days_absent }}</td>
                <td>I reported my absence to: {{ $returnToWork->report_absence_to }}</td>
            </tr>
            <tr>
                <td></td>
                <td>Time of phone call: {{ $returnToWork->time_of_call }}</td>
            </tr>
            <tr>
                <td colspan="2">State briefly why you were unfit for work (specify the nature of illness or injury)</td>
            </tr>
            <tr>
                <td colspan="2">{{ $returnToWork->specify_injury }}s</td>
            </tr>
            <tr>
                <td colspan="2">If absent for two or more days, which doctor did you visit?</td>
            </tr>
            <tr>
                <td colspan="2">{{ $returnToWork->which_doctor }}s</td>
            </tr>
            <tr>
                <td>First Day of Absence: {{ $returnToWork->first_day_of_absence }}</td>
                <td>Last Day of Absence: {{ $returnToWork->last_day_of_absence }}</td>
            </tr>
            <tr>
                <td>Signed(Employee)</td>
                <td>Date: </td>
            </tr>
        </tbody>
    </table>
    <h1>Part 2: Return To Work Discussion (to be completed by line manager)</h1>
    <table class="table">
        <tbody>
            <tr>
                <td>Manager’s Name: {{ $returnToWork->manager_name }}</td>
                <td>Date of RTW Discussion: {{ $returnToWork->date_of_discussion }}</td>
            </tr>
            <tr>
                <td>Has the employee confirmed that they are fit for work?</td>
                <td>{{ $returnToWork->q1 }}</td>
            </tr>
            <tr>
                <td>Has the employee been updated on the issues which have arisen during their
                    absence?</td>
                <td>{{ $returnToWork->q2 }}</td>
            </tr>
            <tr>
                <td>Did the employee follow the reporting procedure?</td>
                <td>{{ $returnToWork->q3 }}</td>
            </tr>
            <tr>
                <td>Is the employee entitled to sick pay? (a doctor’s certificate must be presented for
                    absences of more than one day)</td>
                <td>{{ $returnToWork->q4 }}</td>
            </tr>
            <tr>
                <td>Is the absence related to a work related accident? (If yes, has the appropriate
                    documentation been completed?)</td>
                <td>{{ $returnToWork->q5 }}</td>
            </tr>
            <tr>
                <td>Is a phased return appropriate?</td>
                <td>{{ $returnToWork->q6 }}</td>
            </tr>
            <tr>
                <td colspan="2">Details of phased return/restrictions</td>
            </tr>
            <tr>
                <td colspan="2">{{ $returnToWork->q6_sub }}s</td>
            </tr>
            <tr>
                <td>Is there an underlying reason for the absence?</td>
                <td>{{ $returnToWork->q7 }}</td>
            </tr>
            <tr>
                <td colspan="2">Reason</td>
            </tr>
            <tr>
                <td colspan="2">{{ $returnToWork->q7_sub }}s</td>
            </tr>
            <tr>
                <td>Has there been a change in the employee’s circumstances?</td>
                <td>{{ $returnToWork->q8 }}</td>
            </tr>
            <tr>
                <td colspan="2">Summary of change</td>
            </tr>
            <tr>
                <td colspan="2">{{ $returnToWork->q8_sub }}s</td>
            </tr>
            <tr>
                <td>Is the absence related to the employee’s disability or pregnancy?</td>
                <td>{{ $returnToWork->q9 }}</td>
            </tr>
            <tr>
                <td>Does the employee have a temporary or permanent mobility issue that would
                    require a Personal Emergency Evacuation Plan?</td>
                <td>{{ $returnToWork->q10 }}</td>
            </tr>
            <tr>
                <td colspan="2">Total number of working days lost due to absence in the last 12 months</td>
            </tr>
            <tr>
                <td colspan="2">{{ $returnToWork->q10_sub }}s</td>
            </tr>
            <tr>
                <td>Has the employee reached the monitoring absence trigger (3 occasions in a 12
                    month period)?</td>
                <td>{{ $returnToWork->q11 }}</td>
            </tr>
            <tr>
                <td>Is there concern about the level of sickness absence?</td>
                <td>{{ $returnToWork->q12 }}</td>
            </tr>
            <tr>
                <td>Has the employee been informed about the importance of regular attendance and
                    the fact that poor attendance can lead to informal and formal monitoring under the
                    School’s Managing Absence Policy?</td>
                <td>{{ $returnToWork->q13 }}</td>
            </tr>
            <tr>
                <td>Is there a need for follow up action such as an Attendance Review Meeting?</td>
                <td>{{ $returnToWork->q14 }}</td>
            </tr>
            <tr>
                <td colspan="2">Note of any other comments or issues raised, and any further action agreed</td>
            </tr>
            <tr>
                <td colspan="2">{{ $returnToWork->q14_sub }}s</td>
            </tr>
            <tr>
                <td>Signed(Manager)</td>
                <td>Date: </td>
            </tr>
            <tr>
                <td>Signed(Employee)</td>
                <td>Date: </td>
            </tr>
        </tbody>
    </table>
</div>