@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
   .hight-light td{
        color: red !important;
   }
</style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>RETURN TO WORK FORM</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('return-to-work/list')}}">List</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>Employee name</th>
                                    <th>Job title</th>
                                    <th>Manager name</th>
                                    <th>Approved</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($list as $value)
                                    <tr class="centerTd @if(request('id') == $value->id && $value->approve_status == 1) hight-light @endif">
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->job_title }}</td>
                                        <td>{{ $value->manager_name }}</td>
                                        <td>{{ $value->approve_status == 1 ? 'Yes' : 'No' }}</td>
                                        <td>{{ $value->created_at->format('m/d/Y') }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="/return-to-work/{{ $value->id }}">view</a>
                                                    <a class="dropdown-item" href="/return-to-work/{{ $value->id }}/delete">delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection