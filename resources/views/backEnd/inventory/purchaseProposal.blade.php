 {{-- Author: Tran Thanh Phu --}}
@extends('backEnd.master')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/css')}}/purchase-proposal.css">
@endsection
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Purchase Proposal</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        @if(isset($editData))
        @if(in_array(321, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
           
        <div class="row">
            <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                <a href="{{url('purchase-proposal')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.add')
                </a>
            </div>
        </div>
        @endif
        @endif
      <div class="row">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">@if(isset($editData))
                                    @lang('lang.edit')
                                @else
                                    @lang('lang.add')
                                @endif
                                Purchase Proposal
                            </h3>
                        </div>
                        @if(isset($editData))
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'purchase-proposal/'.$editData->id . '/edit', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @else
                        @if(in_array(321, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
           
                        {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'purchase-proposal',
                        'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
                        @endif
                        @endif
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    @if(session()->has('message-success'))
                                    <div class="alert alert-success mb-20">
                                        {{ session()->get('message-success') }}
                                    </div>
                                    @elseif(session()->has('message-danger'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('message-danger') }}
                                    </div>
                                    @endif
                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect" id="getabc">
                                            <select class="niceSelect w-100 bb form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" name="category_id" id="category_name">
                                                <option data-display="@lang('lang.select_item_category') *" value="">Category Item</option>
                                                @foreach($itemCategories as $key=>$value)
                                                <option  value="{{$value->id}}"
                                                @if(isset($editData))
                                                @if($editData->item_category_id == $value->id)
                                                    @lang('lang.selected')
                                                @endif
                                                @endif
                                                >{{$value->category_name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('category_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong>{{ $errors->first('category_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect" id="item_name_div">
                                            <select class="niceSelect w-100 bb form-control{{ $errors->has('item_id') ? ' is-invalid' : '' }}" name="item_id" id="item_name">
                                                <option data-display="select item name *" value="">@lang('lang.select')</option>
                                                
                                            </select>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('item_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong>{{ $errors->first('item_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect" id="supplierDiv">
                                            <select class="niceSelect w-100 bb form-control{{ $errors->has('supplier') ? ' is-invalid' : '' }}" name="supplier" id="supplier">
                                                <option data-display="select supplier *" value="">@lang('lang.select')</option>
                                                
                                            </select>
                                            <span class="focus-border"></span>
                                            @if ($errors->has('supplier'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong>{{ $errors->first('supplier') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <input class="primary-input form-control"
                                            type="number" min="1" name="quantity" autocomplete="off" value="{{isset($editData)? $editData->amount : '' }}" required>
                                            <label>quantity<span></span> </label>
                                            <span class="focus-border textarea"></span>
        
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control" cols="0" rows="4" name="description" id="description">{{isset($editData) ? $editData->notes : ''}}</textarea>
                                            <label>@lang('lang.description') <span></span> </label>
                                            <span class="focus-border textarea"></span>

                                        </div>
                                    </div>
                             </div>
                  				@php 
                                  $tooltip = "";
                                  if(in_array(321, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                                        $tooltip = "";
                                    }else{
                                        $tooltip = "You have no permission to add";
                                    }
                                @endphp
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                       <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">

                                            <span class="ti-check"></span>
                                            @if(isset($editData))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.save')
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
               
          <div class="row">
            <div class="col-lg-4 no-gutters">
                <div class="main-title">
                    <h3 class="mb-0">Purchase Proposal List</h3>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">
                <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                    <thead>
                        @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="6">
                                         @if(session()->has('message-success-delete'))
                                          <div class="alert alert-success">
                                              {{ session()->get('message-success-delete') }}
                                          </div>
                                        @elseif(session()->has('message-danger-delete'))
                                          <div class="alert alert-danger">
                                              {{ session()->get('message-danger-delete') }}
                                          </div>
                                        @endif
                                    </td>
                                </tr>
                             @endif
                        <tr>
                            <th>@lang('lang.staff') @lang('lang.name')</th>
                            <th>@lang('lang.date') </th>
                            <th>Item Name</th>
                            <th>@lang('lang.quantity')</th>
                            <th>@lang('lang.action')</th>
                            @if (Auth::user()->role_id == 1)
                                <th>approve</th>
                            @endif
                        </tr>
                    </thead>

                    <tbody>
                        @if(isset($inventoryPayment))
                        @foreach($inventoryPayment as $value)
                        <tr>
                            <td>{{App\SmStaff::find($value->staff_id)->full_name}}</td>
                            <td>{{$value->created_at}}</td>
                            <td style="text-align: center">{{App\SmItem::find($value->item_id)->item_name}}</td>
                            <td style="text-align: center">{{$value->amount}}</td>
                            <td>
                                <div class="dropdown">
                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                        @lang('lang.select')
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        @if(in_array(322, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            <a class="dropdown-item" href="{{url('purchase-proposal/'.$value->id)}}">@lang('lang.view')</a>
                                        @endif
                                        @if(in_array(322, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            <a class="dropdown-item" href="{{url('purchase-proposal/'.$value->id.'/edit')}}">@lang('lang.edit')</a>
                                        @endif
                                        @if(in_array(323, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Item" href="{{url('purchase-proposal/'.$value->id.'/delete-view')}}">@lang('lang.delete')</a>
                                        @endif
                                    </div>
                                </div>
                            </td>
                            @if (Auth::user()->role_id == 1)
                                <td style="text-align: center;">
                                    <div class="cbx">
                                        <input @if($value->active_status != 0) checked @endif value="{{$value->id}}" id="cbx" class="changeApprove" type="checkbox"/>
                                        <label for="cbx"></label>
                                        <svg width="15" height="14" viewbox="0 0 15 14" fill="none">
                                        <path d="M2 8.36364L6.23077 12L13 2"></path>
                                        </svg>
                                    </div>
                                </td>
                            @endif
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection