@extends('backEnd.master')
@section('mainContent')
@php  $setting = App\SmGeneralSettings::find(1); if(!empty($setting->currency_symbol)){ $currency = $setting->currency_symbol; }else{ $currency = '$'; } @endphp
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Purchase Proposal Details</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
            </div>
        </div>
    </div>
</section>
<section class="mb-40 student-details">
    @if(session()->has('message-success'))
    <div class="alert alert-success">
        {{ session()->get('message-success') }}
    </div>
    @elseif(session()->has('message-danger'))
    <div class="alert alert-danger">
        {{ session()->get('message-danger') }}
    </div>
    @endif
    <div class="container-fluid p-0">
        <div class="row">
         <div class="col-lg-3">
            <!-- Start Student Meta Information -->
            <div class="main-title">
                <h3 class="mb-20">Proposer Details</h3>
            </div>
            <div class="student-meta-box">
                <div class="student-meta-top"></div>

                {{-- @if(!empty($staffDetails->staff_photo))
                    <img class="student-meta-img img-100" src="{{asset($staffDetails->staff_photo)}}"  alt="">
                @else
                    <img class="student-meta-img img-100" src="{{ file_exists(@$staffDetails->staff_photo) ? asset($staffDetails->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"  alt="">
                @endif --}}
                <img class="student-meta-img img-100" src="{{ file_exists(@$staffDetails->staff_photo) ? asset($staffDetails->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"  alt="">
                <div class="white-box">
                    <div class="single-meta mt-10">
                        <div class="d-flex justify-content-between">
                            <div class="name">
                                @lang('lang.staff_name')
                            </div>
                            <div class="value">

                                @if(isset($staffDetails)){{$staffDetails->full_name}}@endif

                            </div>
                        </div>
                    </div>
                    <div class="single-meta">
                        <div class="d-flex justify-content-between">
                            <div class="name">
                                @lang('lang.role') 
                            </div>
                            <div class="value">
                               @if(isset($staffDetails)){{$staffDetails->roles->name}}@endif
                           </div>
                       </div>
                   </div>
                   <div class="single-meta">
                    <div class="d-flex justify-content-between">
                        <div class="name">
                            @lang('lang.designation')
                        </div>
                        <div class="value">
                           @if(isset($staffDetails)){{ !empty($staffDetails->designations)?$staffDetails->designations->title:''}}@endif
                            
                       </div>
                   </div>
               </div>
               <div class="single-meta">
                <div class="d-flex justify-content-between">
                    <div class="name">
                        @lang('lang.department')
                    </div>
                    <div class="value">
                        
                           @if(isset($staffDetails)){{ !empty($staffDetails->departments)?$staffDetails->departments->name:''}}@endif 

                    </div>
                </div>
            </div>
            <div class="single-meta">
                <div class="d-flex justify-content-between">
                    <div class="name">
                        @lang('lang.epf_no')
                    </div>
                    <div class="value">
                       @if(isset($staffDetails)){{$staffDetails->epf_no}}@endif
                   </div>
               </div>
           </div>
           <div class="single-meta">
            <div class="d-flex justify-content-between">
                <div class="name">
                    @lang('lang.basic_salary')
                </div>
                <div class="value">
                    ({{$currency}}) @if(isset($staffDetails)){{$staffDetails->basic_salary}}@endif
                </div>
            </div>
        </div>
        <div class="single-meta">
            <div class="d-flex justify-content-between">
                <div class="name">
                    @lang('lang.contract_type')
                </div>
                <div class="value">
                   @if(isset($staffDetails)){{$staffDetails->contract_type}}@endif
               </div>
           </div>
       </div>
       <div class="single-meta">
        <div class="d-flex justify-content-between">
            <div class="name">
                @lang('lang.date_of_joining')
            </div>
            <div class="value">
                @if(isset($staffDetails))
               
{{$staffDetails->date_of_joining != ""? App\SmGeneralSettings::DateConvater($staffDetails->date_of_joining):''}}


                @endif
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- End Student Meta Information -->

<!-- Start Student Details -->
<div class="col-lg-9 staff-details">
    <ul class="nav nav-tabs tabs_scroll_nav" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="#studentProfile" role="tab" data-toggle="tab">Information</a>
        </li>
    </ul>

<div class="tab-content">
    <!-- Start Profile Tab -->
    <div role="tabpanel" class="tab-pane fade show active" id="studentProfile">
        <div class="white-box">
            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-5">
                        <div class="">
                            Company Name
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-6">
                        <div class="">
                            @if(isset($suppliers)){{$suppliers->company_name}}@endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-info">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="">
                           Item Category Name
                       </div>
                   </div>

                   <div class="col-lg-7 col-md-7">
                        <div class="">
                                @if(isset($itemCategories)){{$itemCategories->category_name}}@endif
                        </div>
                    </div>
                </div>
            </div>

     <div class="single-info">
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <div class="">
                    Item Name
                </div>
            </div>

            <div class="col-lg-7 col-md-7">
                <div class="">
                    @if(isset($items)){{$items->item_name}}@endif
                </div>
            </div>
        </div>
    </div>

    <div class="single-info">
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <div class="">
                    @lang('lang.amount')
                </div>
            </div>

            <div class="col-lg-7 col-md-7">
                <div class="">

                    @if(isset($inventoryPayment)) {{$inventoryPayment->amount}} @endif 
                    
                </div>
            </div>
        </div>
    </div>

    <div class="single-info">
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <div class="">
                    Proposal Date
                </div>
            </div>

            <div class="col-lg-7 col-md-7">
                <div class="">
                    @if(isset($inventoryPayment)) {{$inventoryPayment->created_at}} @endif
                </div>
            </div>
        </div>
    </div>
    <div class="single-info">
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <div class="">
                    Discription
                </div>
            </div>

            <div class="col-lg-7 col-md-7">
                <div class="">
                    @if(isset($inventoryPayment)) {{$inventoryPayment->notes}} @endif
                </div>
            </div>
        </div>
    </div>

    <div class="single-info">
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <div class="">
                    Approved
                </div>
            </div>

            <div class="col-lg-7 col-md-7">
                <div class="">
                    @if(isset($inventoryPayment)) @if($inventoryPayment->active_status == 0) No @else Yes @endif @endif
                </div>
            </div>
        </div>
    </div>
    
</div>

</div>
</div>

</section>
@endsection