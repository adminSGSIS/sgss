@extends('backEnd.master')
@section('mainContent')
<h2>MENU SETTING</h2>
	<div class="row">
        
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('lunch-menu/monday')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Monday</h3>
                            <p class="mb-0">Monday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                        
                                                    </h1>
                    </div>
                </div>
            </a>
        </div>
                
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('lunch-menu/tuesday')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Tuesday</h3>
                            <p class="mb-0">Tuesday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                        
                        </h1>
                    </div>
                </div>
            </a>
        </div>
                        
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('lunch-menu/wednesday')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Wednesday</h3>
                            <p class="mb-0">Wednesday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                        
                        </h1>
                    </div>
                </div>
            </a>
        </div>
                
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('lunch-menu/thursday')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Thursday</h3>
                            <p class="mb-0">Thursday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                      
                                                  </h1>
                   </div>
               </div>
           </a>
       </div>
       <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="{{url('lunch-menu/friday')}}" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Friday</h3>
                            <p class="mb-0">Friday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                      
                                                  </h1>
                   </div>
               </div>
           </a>
       </div>
          </div>
<br><br>
<h2>FOOD SETTING</h2>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="white-box mt-30">
                        <form action="{{url('add-food-2')}}" method="POST">
                            @csrf
                        
                            <div class="add-visitor">
                                <div class="row  mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <input class="primary-input form-control" type="text" name="name" value="" maxlength="100">
                                            <label>Food name<span>*</span></label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row  mt-25">
                                    <div class="col-lg-12">
                                        <select class="niceSelect w-100 bb form-control" name="type" style="display: none;">
                                            <option data-display="Select Food type*" value="">Food type</option>
                                            @foreach($foodtypes as $foodtype)
                                            <option data-display="{{$foodtype->name}}" value="{{$foodtype->id}}">{{$foodtype->name}}</option>
                                            @endforeach
                                        </select>
                                     </div>  
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="" data-original-title="">
                                            <span class="ti-check"></span>
                                            Save                                        </button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <div class="white-box mt-30">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message-success-delete') }}
                                        </div>
                                        @elseif(session()->has('message-danger-delete'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('message-danger-delete') }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <th width="40%">@lang('lang.name')</th>
                                    <th>@lang('lang.type')</th>
                                    <th>@lang('lang.action')</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($foods as $food)
                                <tr>
                                    <td>{{$food->name}}</td>
                                    <td>{{$food->food_type->name}}</td>
                                    
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                @lang('lang.select')
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a onclick="return confirm('Delete food?')" class="dropdown-item deleteAddIncomeModal" href="/delete-food/{{$food->id}}" >@lang('lang.delete')</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
    </div>
</div>
@endsection