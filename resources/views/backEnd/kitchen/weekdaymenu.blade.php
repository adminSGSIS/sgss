@extends('backEnd.master')
@section('mainContent')
	<h2 style="text-align: center;">Thực đơn / {{$weekday}} menu</h2>
	<form method="POST" action="{{url('save-menu')}}">
		@csrf
		<input type="hidden" name="weekday" value="{{$weekday}}">
		<div class="white-box">
		<h3>Các món nóng / Hot Dishes  <button id="add-hotdish" class="btn btn-primary btn-sm" style="float: right;"><i class="fa fa-plus"></i></button></h3> 
		<div class="row  mt-25" id="hotdishes-input" style="display: none;">
            <div class="col-lg-9">
                <div class="input-effect">
                    <input class="primary-input form-control" type="text" id="hotdish-text-input" value="" maxlength="100">
                    <label>food name <span>*</span></label>
                    <span class="focus-border"></span>
                                                            </div>
            </div>
            <div class="col-lg-3">
                <button class="primary-btn fix-gr-bg" id="btn-add-hotdish">
	                <span class="ti-check"></span>
	                save
	            </button>
            </div>
        </div>
        <br>
		<select class="foodselect" name="hotdishes[]" multiple="multiple" id="hotdishes">
			@foreach($foods as $food)
				@if($food->type == 1)
				<option value="{{$food->id}}" @if($food->isSelected == 1) selected @endif >{{$food->name}}</option>
				@endif
			@endforeach
		</select>
		<br><br><br>
		<h3>Các món chay / Vegetarian  <button id="add-vegetarian" class="btn btn-info btn-sm" style="float: right;"><i class="fa fa-plus"></i></button></h3>
		<div class="row  mt-25" id="vegetarian-input" style="display: none;">
            <div class="col-lg-9">
                <div class="input-effect">
                    <input class="primary-input form-control" type="text" id="vegetarian-text-input" value="" maxlength="100">
                    <label>food name <span>*</span></label>
                    <span class="focus-border"></span>
                                                            </div>
            </div>
            <div class="col-lg-3">
                <button class="primary-btn fix-gr-bg" id="btn-add-vegetarian">
	                <span class="ti-check"></span>
	                save
	            </button>
            </div>
        </div>
        <br>
		<select class="foodselect" name="vegetarians[]" multiple="multiple" id="vegetarians">
			@foreach($foods as $food)
				@if($food->type == 2)
				<option value="{{$food->id}}" @if($food->isSelected == 1) selected @endif>{{$food->name}}</option>
				@endif
			@endforeach
		</select><br><br><br>
		<h3>Salad và rau củ / Salad and vegetable  <button id="add-salad" class="btn btn-danger btn-sm" style="float: right;"><i class="fa fa-plus"></i></button></h3>
		<div class="row  mt-25" id="salad-input" style="display: none;">
            <div class="col-lg-9">
                <div class="input-effect">
                    <input class="primary-input form-control" type="text" id="salad-text-input" value="" maxlength="100">
                    <label>food name <span>*</span></label>
                    <span class="focus-border"></span>
                                                            </div>
            </div>
            <div class="col-lg-3">
                <button class="primary-btn fix-gr-bg" id="btn-add-salad">
	                <span class="ti-check"></span>
	                save
	            </button>
            </div>
        </div>
        <br>
		<select class="foodselect" name="salads[]" multiple="multiple" id="salad">
			@foreach($foods as $food)
				@if($food->type == 3)
				<option value="{{$food->id}}" @if($food->isSelected == 1) selected @endif>{{$food->name}}</option>
				@endif
			@endforeach
		</select><br><br><br>
		<h3>Các loại thịt nguội / Hams  <button id="add-hams" class="btn btn-warning btn-sm" style="float: right;"><i class="fa fa-plus"></i></button></h3>
		<div class="row  mt-25" id="hams-input" style="display: none;">
            <div class="col-lg-9">
                <div class="input-effect">
                    <input class="primary-input form-control" type="text" id="hams-text-input" value="" maxlength="100">
                    <label>food name <span>*</span></label>
                    <span class="focus-border"></span>
                                                            </div>
            </div>
            <div class="col-lg-3">
                <button class="primary-btn fix-gr-bg" id="btn-add-hams">
	                <span class="ti-check"></span>
	                save
	            </button>
            </div>
        </div>
        <br>
		<select class="foodselect" name="hams[]" multiple="multiple" id="hams">
			@foreach($foods as $food)
				@if($food->type == 4)
				<option value="{{$food->id}}" @if($food->isSelected == 1) selected @endif>{{$food->name}}</option>
				@endif
			@endforeach
		</select><br><br><br>
		<h3>Ăn nhẹ buổi chiều / Snack  <button id="add-snack" class="btn btn-secondary btn-sm" style="float: right;"><i class="fa fa-plus"></i></button></h3>
		<div class="row  mt-25" id="snack-input" style="display: none;">
            <div class="col-lg-9">
                <div class="input-effect">
                    <input class="primary-input form-control" type="text" id="snack-text-input" value="" maxlength="100">
                    <label>food name <span>*</span></label>
                    <span class="focus-border"></span>
                                                            </div>
            </div>
            <div class="col-lg-3">
                <button class="primary-btn fix-gr-bg" id="btn-add-snack">
	                <span class="ti-check"></span>
	                save
	            </button>
            </div>
        </div>
        <br>
		<select class="foodselect" name="snacks[]" multiple="multiple" id="snacks">
			@foreach($foods as $food)
				@if($food->type == 5)
				<option value="{{$food->id}}" @if($food->isSelected == 1) selected @endif>{{$food->name}}</option>
				@endif
			@endforeach
		</select><br><br><br>
			<div class="col-lg-12 text-center">
	            <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="" data-original-title="">
	                <span class="ti-check"></span>
	                save Menu
	            </button>
	        </div>
		</div>

	</form>
@endsection
@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
		    $(".foodselect").select2();

		    $('#add-hotdish').click(function(e){
		    	e.preventDefault();
		    	$('#hotdishes-input').toggle();
		    });
		    $('#add-vegetarian').click(function(e){
		    	e.preventDefault();
		    	$('#vegetarian-input').toggle();
		    });
		    $('#add-salad').click(function(e){
		    	e.preventDefault();
		    	$('#salad-input').toggle();
		    });
		    $('#add-hams').click(function(e){
		    	e.preventDefault();
		    	$('#hams-input').toggle();
		    });
		    $('#add-snack').click(function(e){
		    	e.preventDefault();
		    	$('#snack-input').toggle();
		    });
///////////////////////////////////////////////////////////////////////

			$('#btn-add-hotdish').click(function(e){
				e.preventDefault();
				var name = $('#hotdish-text-input').val();
				var type = 1;
				if(name != ''){
					$.ajax({
	                    url : '/add-food',
	                    type : "post",
	                    dataType:"html",
	                    data : {
	                    	 _token : "{{ csrf_token() }}",
	                         name : name,
	                         type : type
	                    },
	                    success : function (result){
	                    	
	                    	$('#hotdish-text-input').val('');
	                        $('#hotdishes').append('<option value="'+(result)+'">'+(name)+'</option>')
	                    }
	                });
	            }
			});	
			$('#btn-add-vegetarian').click(function(e){
				e.preventDefault();
				var name = $('#vegetarian-text-input').val();
				var type = 2;
				if(name != ''){
					$.ajax({
	                    url : '/add-food',
	                    type : "post",
	                    dataType:"html",
	                    data : {
	                    	 _token : "{{ csrf_token() }}",
	                         name : name,
	                         type : type
	                    },
	                    success : function (result){
	                    	$('#vegetarian-text-input').val('');
	                        $('#vegetarians').append('<option value="'+(result)+'">'+(name)+'</option>')
	                    }
	                });
	            }
			});	

			$('#btn-add-salad').click(function(e){
				e.preventDefault();
				var name = $('#salad-text-input').val();
				var type = 3;
				if(name != ''){
					$.ajax({
	                    url : '/add-food',
	                    type : "post",
	                    dataType:"html",
	                    data : {
	                    	 _token : "{{ csrf_token() }}",
	                         name : name,
	                         type : type
	                    },
	                    success : function (result){
	                    	$('#salad-text-input').val('');
	                        $('#salad').append('<option value="'+(result)+'">'+(name)+'</option>')
	                    }
	                });
	            }
			});	

			$('#btn-add-hams').click(function(e){
				e.preventDefault();
				var name = $('#hams-text-input').val();
				var type = 4;
				if(name != ''){
					$.ajax({
	                    url : '/add-food',
	                    type : "post",
	                    dataType:"html",
	                    data : {
	                    	 _token : "{{ csrf_token() }}",
	                         name : name,
	                         type : type
	                    },
	                    success : function (result){
	                    	$('#hams-text-input').val('');
	                        $('#hams').append('<option value="'+(result)+'">'+(name)+'</option>')
	                    }
	                });
	            }
			});	

			$('#btn-add-snack').click(function(e){
				e.preventDefault();
				var name = $('#snack-text-input').val();
				var type = 5;
				if(name != ''){
					$.ajax({
	                    url : '/add-food',
	                    type : "post",
	                    dataType:"html",
	                    data : {
	                    	 _token : "{{ csrf_token() }}",
	                         name : name,
	                         type : type
	                    },
	                    success : function (result){
	                    	$('#snack-text-input').val('');
	                        $('#snacks').append('<option value="'+(result)+'">'+(name)+'</option>')
	                    }
	                });
	            }
			});	

		});
		
	</script>
@endsection