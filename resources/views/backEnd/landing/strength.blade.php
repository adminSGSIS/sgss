@extends('backEnd.master')
@section('mainContent')
    @php
        function showPicName($data){
            $name = explode('/', $data);
            return $name[3];
        }
    @endphp
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>@lang('lang.add') @lang('strength')</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="#">@lang('strength')</a>
                    <a href="/strength-landing">@lang('lang.add') @lang('strength')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">
            @if(isset($add_course))
            @if(in_array(511, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                <div class="row">
                    <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                        <a href="{{url('course-list')}}" class="primary-btn small fix-gr-bg">
                            <span class="ti-plus pr-2"></span>
                            @lang('lang.add')
                        </a>
                    </div>
                </div>
            @endif
            @endif
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                @if(isset($add_strength))
                                    @lang('lang.edit')
                                @else
                                    @lang('lang.add')
                                @endif
                                @lang('strength')
                            </h3>
                        </div>
                        @if(isset($add_strength))
                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'update_strength',
                            'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'add-strength-update']) }}
                        @else
                        @if(in_array(511, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                            {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'route' => 'store_strength',
                            'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'add-strength']) }}
                        @endif
                        @endif
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(session()->has('message-success'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success') }}
                                            </div>
                                        @elseif(session()->has('message-danger'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger') }}
                                            </div>
                                        @endif

                                        @if ($errors->any())
                                            @foreach ($errors->all() as $error)
                                                <p class="error text-danger">{{$error}}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-lg-12 mt-40">
                                        <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                           type="text" name="title" autocomplete="off"
                                                           value="{{isset($add_strength)? $add_strength->title: old('title')}}">
                                                    <input type="hidden" name="id"
                                                           value="{{isset($add_strength)? $add_strength->id: ''}}">
                                                    <label>@lang('lang.title') (english)<span>*</span></label>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('title'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('title') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            </br> </br>
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control{{ $errors->has('title_vn') ? ' is-invalid' : '' }}"
                                                           type="text" name="title_vn" autocomplete="off"
                                                           value="{{isset($add_strength)? $add_strength->title_vn: old('title_vn')}}">
                                                    <input type="hidden" name="id"
                                                           value="{{isset($add_strength)? $add_strength->id: ''}}">
                                                    <label>@lang('lang.title') (vietnamese)<span>*</span></label>
                                                    <span class="focus-border"></span>
                                                    @if ($errors->has('title_vn'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('title_vn') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 ">
                                        <div class="row mt-20">
                                            <div class="col-md-12 col-lg-12 mt-20">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="input-effect">
                                                        <label>@lang('Content(english)') *<span></span></label>
                                                        <textarea class="primary-input form-control" cols="0" rows="5" name="content" id="summernote">
                                                         {{isset($add_strength) ? $add_strength->content : ''}}</textarea>
                                                            
                                                        <span class="focus-border textarea"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 ">
                                        <div class="row mt-20">
                                            <div class="col-md-12 col-lg-12 mt-20">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="input-effect">
                                                        <label>@lang('Content(vietnamese)') *<span></span></label>
                                                        <textarea class="primary-input form-control" cols="0" rows="5" name="content_vn" id="summernotetwo">
                                                         {{isset($add_strength) ? $add_strength->content_vn : ''}}</textarea>
                                                            
                                                        <span class="focus-border textarea"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $tooltip = "";
                                    if(in_array(511, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                                            $tooltip = "";
                                        }else{
                                            $tooltip = "You have no permission to add";
                                        }
                                @endphp
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{@$tooltip}}">
                                            <span class="ti-check"></span>
                                            @if(isset($add_strength))
                                                @lang('lang.update')
                                            @else
                                                @lang('lang.add')
                                            @endif
                                            @lang('strength')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mt-40">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">@lang('Strength list')</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            @if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="4">
                                        @if(session()->has('message-success-delete'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message-success-delete') }}
                                            </div>
                                        @elseif(session()->has('message-danger-delete'))
                                            <div class="alert alert-danger">
                                                {{ session()->get('message-danger-delete') }}
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <th>@lang('lang.title')</th>
                             
                                <th>@lang('lang.action')</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if(isset($strength))
                                @foreach($strength as $value)
                                    <tr>
                                        <td>{{@$value->title}}</td>
                                       
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    @if(in_array(512, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                                    <a class="dropdown-item"
                                                       href="{{url('edit-strength/'.$value->id)}}">@lang('lang.edit')</a>
                                                    @endif
                                                       @if(in_array(513, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
                                                    <a href="{{url('for-delete-strength/'.$value->id)}}"
                                                       class="dropdown-item small fix-gr-bg modalLink"
                                                       title="Delete Strength" data-modal-size="modal-md">
                                                        @lang('lang.delete')
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
<script type="text/javascript">
    $('#summernote').summernote({

       toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['fontname', ['fontname']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'video']],
    ['view', ['fullscreen', 'codeview', 'help']],
    ['height', ['height']]
],
});
$('#summernotetwo').summernote({

toolbar: [
['style', ['bold', 'italic', 'underline', 'clear']],
['font', ['strikethrough', 'superscript', 'subscript']],
['fontsize', ['fontsize']],
['fontname', ['fontname']],
['color', ['color']],
['para', ['ul', 'ol', 'paragraph']],
['table', ['table']],
['insert', ['link', 'picture', 'video']],
['view', ['fullscreen', 'codeview', 'help']],
['height', ['height']]
],
});
</script>


@endsection

