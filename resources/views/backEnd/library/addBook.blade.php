@extends('backEnd.master')
@section('mainContent')
<style type="text/css">
    .input{
        width: 80%;

    }
</style>
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>@lang('lang.manage') @lang('lang.book')</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                    <a href="{{url('library')}}">@lang('lang.library')</a>
                    @if(isset($editData))
                        <a href="#">@lang('lang.edit_book')</a>
                    @else
                        <a href="#">@lang('lang.add_book')</a>
                    @endif

                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area">
          <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-6">
                    <div class="main-title ">
                        <h3 class="mb-30">
                            @if(isset($editData))
                                @lang('lang.edit')
                            @else
                                @lang('lang.add')
                            @endif
                            @lang('lang.book')
                            @lang('cover')</h3>

                    </div>
                </div>
            </div>
            @if(isset($editData))
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'update-book-data/'.$editData->id, 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
            @else
            @if(in_array(300, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
       
                {{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'save-book-data',
                'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
            @endif
            @endif

            <div class="row">
                <div class="col-lg-12">
                    @include('backEnd.partials.alertMessage')
                    <div class="white-box">
                        <div class="">
                            <input type="hidden" name="url" id="url" value="{{URL::to('/')}}">
                            <div class="row mb-30">
                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input
                                            class="primary-input form-control{{ $errors->has('book_title') ? ' is-invalid' : '' }}"
                                            type="text" name="book_title" autocomplete="off"
                                            value="{{isset($editData)? $editData->book_title :(old('book_title')!=''? old('book_title'):'')}}">
                                        <label>@lang('lang.book_title') <span>*</span> </label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('book_title'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('book_title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <select
                                            class="niceSelect w-100 bb form-control{{ $errors->has('book_category_id') ? ' is-invalid' : '' }}"
                                            name="book_category_id" id="book_category_id">
                                            <option data-display="@lang('lang.select_book_category') *"
                                                    value="">@lang('lang.select')</option>
                                            @foreach($categories as $key=>$value)
                                                @if(isset($editData))
                                                    <option
                                                        value="{{$value->id}}" {{$value->id == $editData->book_category_id? 'selected':''}}>{{$value->category_name}}</option>
                                                @else
                                                    <option
                                                        value="{{$value->id}}" {{old('book_category_id')!=''? (old('book_category_id') == $value->id? 'selected':''):''}} >{{$value->category_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('book_category_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                        <strong>{{ $errors->first('book_category_id') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <select
                                            class="niceSelect w-100 bb form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}"
                                            name="subject" id="subject">
                                            <option data-display="@lang('lang.select_subjects')"
                                                    value="">@lang('lang.select')</option>
                                            @foreach($subjects as $key=>$value)
                                                @if(isset($editData))
                                                    <option value="{{$value->id}}" {{$value->id == $editData->subject_id? 'selected':''}}>{{$value->subject_name}}</option>
                                                    @else
                                                    <option value="{{$value->id}}" {{old('subject')!=''? (old('subject') == $value->id? 'selected':''):''}} >{{$value->subject_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('subject'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input
                                            class="primary-input form-control{{ $errors->has('type') ? ' is-invalid' : '' }}"
                                            type="text" name="book_number" autocomplete="off"
                                            value="{{isset($editData)? $editData->book_number: old('book_number')}}">
                                        <label>@lang('lang.book') @lang('lang.no')</label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('book_number'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('book_number') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                            </div>

                            <div class="row mb-30">
                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input
                                            class="primary-input form-control{{ $errors->has('isbn_no') ? ' is-invalid' : '' }}"
                                            type="number" name="isbn_no" autocomplete="off"
                                            value="{{isset($editData)? $editData->isbn_no: old('isbn_no')}}">
                                        <label>@lang('lang.isbn') @lang('lang.no')</label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('isbn_no'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('isbn_no') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input
                                            class="primary-input form-control{{ $errors->has('publisher_name') ? ' is-invalid' : '' }}"
                                            type="text" name="publisher_name" autocomplete="off" 
                                            value="{{isset($editData)? $editData->publisher_name: old('publisher_name')}}">
                                        <label>@lang('lang.publisher') @lang('lang.name')</label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('publisher_name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('publisher_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input
                                            class="primary-input form-control{{ $errors->has('author_name') ? ' is-invalid' : '' }}"
                                            type="text" name="author_name" autocomplete="off"
                                            value="{{isset($editData)? $editData->author_name: old('author_name')}}">
                                        <label>@lang('lang.author_name')</label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('author_name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('author_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            

                            <div class="row mb-30">

                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input

                                            class="primary-input form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}"
                                            type="number" min="0" name="quantity" autocomplete="off" readonly=""
                                            value="{{isset($editData)? $editData->quantity : '0'}}">
                                        <label>@lang('lang.quantity')</label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('quantity'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <input
                                            class="primary-input form-control{{ $errors->has('book_price') ? ' is-invalid' : '' }}"
                                            type="number" min="0" name="book_price" autocomplete="off"
                                            value="{{isset($editData)? $editData->book_price : old('book_price')}}">
                                        <label>@lang('lang.book_price')</label>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('book_price'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('book_price') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="row md-20">
                                <div class="col-lg-12">
                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                        <textarea class="primary-input form-control" cols="0" rows="4" name="details"
                                                  id="details">{{isset($editData) ? $editData->details : old('details')}}</textarea>
                                        <label>@lang('lang.description') <span></span> </label>
                                        <span class="focus-border textarea"></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-40">
                            @php 
                              $tooltip = "";
                              if(in_array(300, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                                    $tooltip = "";
                                }else{
                                    $tooltip = "You have no permission to add";
                                }
                            @endphp
                            <div class="col-lg-12 text-center">
                                <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="{{$tooltip}}">
                                    <span class="ti-check"></span>
                                    @if(isset($editData))
                                        @lang('lang.update')
                                    @else
                                        @lang('lang.save')
                                    @endif

                                    @lang('lang.book')
                                </button>
                            </div>
                        
                    </div>
                        {{ Form::close() }}
                        </div>
                       @if(isset($editData))
                        <br>
                          <h2>Books within this cover <button id="add_btn" type="button"  class="btn btn-primary btn-sm">Add</button></h2>
                          <input type="hidden" id="count" value="{{$editData->Book->count()}}">            
                          <table class="table" id="table_id">
                            <thead>
                              <tr>
                                <th width="10%">Stt</th>
                                <th width="20%">Barcode</th>
                                <th width="20%">Bookshelf</th>
                                <th width="20%">Status</th>
                                <th width="20%">Available</th>
                                <th width="15%">Delete</th>
                              </tr>
                            </thead>
                            <tbody id="list_books">
                                
                                @php $stt=1; @endphp
                                
                                @foreach($editData->Book as $book)
                                
                                <tr >
                                    <td>{{$stt++}}</td>
                                    <td>{{$book->barcode}}</td>
                                    <td>{{$book->Shelf->title}}</td>
                                    <td>{{$book->status}}</td>
                                    <td>{{$book->borrowed ==1 || $book->status == 'lost'? 'No' :'Yes'}}</td>
                                    <td>
                                        <i class="fa fa-trash fa-2x" id="{{$book->id}}"></i>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                            <tbody>
                                <tr id="add_book" style="display: none;">
                                    <td>Add book</td>
                                    <td><div class="input-effect input"><input class="primary-input  " type="text" id="bardcode"   ><span class="focus-border"></span></div></td>
                                    <td>
                                        <select id="rack">
                                            @foreach($shelves as $shelf)
                                            <option value="{{$shelf->id}}">{{$shelf->title}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select id="status">
                                            <option value="good">good</option>
                                            <option value="damaged">damaged</option>
                                        </select>
                                    </td>
                                    <td>Yes</td>
                                    <td><button id="confirm_add" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button></td>
                                </tr>
                            </tbody>
                          </table>
                        @endif
                        </div>
                          
                        
                </div>
            </div>
        </div>
        
      <input type="hidden" id="title_id" value="{{isset($editData)? $editData->id : ''}}">
    </section>
@endsection
@section('script')
<script type="text/javascript">
    $( document ).ready(function() {
        $('#books').DataTable();
        count = $('#count').val();
        
        title_id=$('#title_id').val();
        $('#add_btn').click(function(){
            $('#add_book').toggle();
            $('html, body').animate({scrollTop:$(document).height()}, 'slow');
        });

        $('#confirm_add').click(function(e){
            e.preventDefault();
            
            barcode = $('#bardcode').val();
            rack = $('#rack').val();
            rack_title = $('#rack :selected').text();
            status = $('#status').val();
            if (barcode == "" || rack ==null) { return false;}
            $.ajax({
                type: "POST",  
                url: "/library/add-book",
                dataType: "json",
                data :{
                    barcode : barcode,
                    rack : rack,
                    status : status,
                    title_id : title_id
                },
                success:function(data){
                    if(count == 0)
                    {
                        $('#list_books').empty();
                    }
                    if(data.error != "none")
                    {
                        alert(data.error);
                    }
                    else
                    {
                        if(data.success != "none")
                        {
                            count++;
                            $('#list_books').append('<tr><td>'+(count)+'</td><td>'+(barcode)+'</td><td>'+(rack_title)+'</td><td>'+(status)+'</td><td>Yes</td><td><i class="fa fa-trash fa-2x" id="'+(data.id)+'"></i></td></tr>');
                            $('#bardcode').val("");
                            
                            $('#add_book').toggle();
                            
                        }
                    }
                    
                }                  
            });      
        });

        $ (document).on('click','.fa-trash',function(){
                id = $(this).attr('id');
                var r = confirm("Delete?!");
                if(r==true)
                {
                    $.ajax({
                    type: "GET",  
                    url: "/library/delete-book/"+(id)+"",
                    dataType: "json",
                    
                    success:function(data){
                        if(data.error == "none")
                        {
                            $('#'+(id)+'').parent().parent().remove();
                            alert('remove success !!!');
                        }
                        else
                        {
                            alert('The book cannot be removed yet!!!');
                        }
                        
                    }                  
                    }); 
                    count--;     
                }
        });
    });
</script>
@endsection