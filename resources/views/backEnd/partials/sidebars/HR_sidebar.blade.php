@php
    use App\SmGeneralSettings; 
    $school_settings= SmGeneralSettings::where('school_id',Auth::user()->school_id)->first();
    if (Auth::user() == "") { header('location:' . url('/login')); exit(); }
    Session::put('permission', App\GlobalVariable::GlobarModuleLinks());
@endphp
@if(SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes" && Session::get('isSchoolAdmin')==FALSE )
    @include('backEnd/partials/saas_menu')
{{--  @elseif(Session::get('isSchoolAdmin')==TRUE && SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes")  --}}


@else




<input type="hidden" name="url" id="url" value="{{url('/')}}">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="{{url('/')}}">
          <img  src="{{ file_exists(@$school_settings->logo) ? asset($school_settings->logo) : asset('public/uploads/settings/logo.png') }}" alt="logo">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    {{-- {{ Auth::user()->role_id }} --}}
    <ul class="list-unstyled components">
        @if(Auth::user()->role_id != 2 && Auth::user()->role_id != 3 )
            
            <li>
                <a href="#subMenuSalary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-wallet"></span>
                    Payroll
                </a>
                <ul class="collapse list-unstyled" id="subMenuSalary">
                    <li>
                        <a href="{{url('salary/me/search')}}">My Payroll</a>
                    </li>
                    <li>
                        <a href="{{url('workday').'/'.Auth::user()->id.'/'.date('Y')}}">Work day</a>
                    </li>
                </ul>
            </li>
                
            <li>
                <a href="#subMenuDevices" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span>
                        <img style="margin-left: 18px" src="/public/images/device.png" width="17px" alt="">
                    </span>
                    Devices Management
                </a>
                <ul class="collapse list-unstyled" id="subMenuDevices">
                    <li>
                        <a href="#" onclick="qrcheck()">QRCODE Checking</a>
                    </li>
                    <li>
                        <a href="{{url('devices')}}">In Stock</a>
                    </li>
                
                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                    <li>
                        <a href="{{url('devices/repairing')}}">Repairing Devices</a>
                    </li>
                    @endif
               
                
                    <li>
                        <a href="{{url('devices/equipments-handover')}}">Equipments Handover</a>
                    </li>
                </ul>
            </li>

                <li>
                    <a href="#subMenuInventory" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-inventory"></span>
                        @lang('lang.purchase')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuInventory">
                        
                            <li>
                                <a href="{{url('purchase-proposal')}}"> purchase proposal</a>
                            </li>
                        
                            <li>
                                <a href="{{url('item-category')}}"> @lang('lang.item_category')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('item-list')}}"> @lang('lang.item_list')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('item-store')}}"> @lang('lang.item_store')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('suppliers')}}"> @lang('lang.supplier')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('item-receive')}}"> @lang('lang.item_receive')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('item-receive-list')}}"> @lang('lang.item_receive_list')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('item-sell-list')}}"> @lang('lang.item_sell')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('item-issue')}}"> @lang('lang.item_issue')</a>
                            </li>
                        
                    </ul>
                </li>
            
            
            <li>
                <a href="#onlineForms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="fa fa-globe" style="padding-left: 20px"></span>
                    Online forms
                </a>
                <ul class="collapse list-unstyled" id="onlineForms">
                    <li>
                        <a href="{{url('return-to-work')}}">Form return to work</a>
                    </li>

                    <li>
                        <a href="{{url('admin/medical-declaration/entry')}}">Medical Declaration Entry</a>
                    </li>
                    
                    <li>
                        <a href="{{url('admin/medical-declaration/domestic')}}">Medical Declaration Domestic</a>
                    </li>
                </ul>
            </li>

            
                <li>
                    <a href="#subMenuHumanResource" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">
                        <span class="flaticon-consultation"></span>
                        @lang('lang.human_resource')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuHumanResource">
                       
                        
                            <li>
                                <a href="{{url('designation')}}"> @lang('lang.designation')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('department')}}"> @lang('lang.department')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('staff_directory')}}"> @lang('lang.staff_directory')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('staff_attendance')}}"> @lang('lang.staff_attendance')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('staff_attendance_report')}}"> @lang('lang.staff_attendance_report')</a>
                            </li>
                        
                         <!--Pham Trong Hai start -->
                         
                            <li>
                                <a href="{{url('careers-list')}}"> @lang('careers')</a>
                            </li>
                        
                        <!--Pham Trong Hai end -->
                        
                    </ul>
                </li>
            

            {{-- @if(@in_array(188, App\GlobalVariable::GlobarModuleLinks())) --}}
                <li>
                <a href="#subMenuLeaveManagement" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">
                    <span class="flaticon-slumber"></span>
                    @lang('lang.leave')
                </a>
                <ul class="collapse list-unstyled" id="subMenuLeaveManagement">
                        <li>
                            <a href="{{url('approve-leave')}}">@lang('approve request')</a>
                            
                            <a href="{{url('pending-leave')}}">@lang('pending request')</a>
                        </li>
                    
                        <li>
                            <a href="{{url('approve-leave')}}">@lang('your approve request')</a>
                            
                            <a href="{{url('pending-leave')}}">@lang('your pending request')</a>
                            
                        </li>

                        <li>
                            <a href="{{url('staff-leave-request')}}">@lang('Staff leave request')</a>
                        </li>
                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 4)
                        <li>
                            <a href="{{url('pending-leave-student')}}">@lang('student leave request')</a>
                        </li>
                    @endif
                    {{-- @if(@in_array(193, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1) --}}
                        <li>
                            <a href="{{url('apply-leave')}}">@lang('lang.apply_leave')</a>
                        </li>
                    {{-- @endif --}}
                </ul>
            </li>
            {{-- @endif --}}

            

            
                <li>
                    <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-email"></span>
                        @lang('lang.communicate')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuCommunicate">
                        
                            <li>
                                <a href="{{url('notice-list')}}">@lang('lang.notice_board')</a>
                            </li>
                        
                        @if (@$config->Saas == 1 && Auth::user()->is_administrator != "yes" )
                            
                            <li>
                                <a href="{{url('administrator-notice')}}">@lang('lang.administrator') @lang('lang.notice')</a>
                            </li>

                        @endif
                        {{--
                        @if(@in_array(73, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                             <a href="{{url('send-message')}}">@lang('lang.send_message')</a>
                        </li>
                        @endif
                        --}}
                        
                            <li>
                                <a href="{{url('send-email')}}">Send Email</a>
                            </li>
                        
                            <li>
                                <a href="{{url('email-sms-log')}}">@lang('lang.email_sms_log')</a>
                            </li>
                    
                    </ul>
                </li>
            

            

           
            
            <li>
                <a href="#subCustomerFacebook" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    CRM
                </a>
                <ul class="collapse list-unstyled" id="subCustomerFacebook">
                    @if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('customer-facebook/add')}}">add customer facebook</a>
                        </li>
                    @endif
                    @if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('customer-facebook/search-customer')}}">search customer facebook</a>
                        </li>
                    @endif
                     <li>
                        <a href="{{url('tasks/add')}}">Work Flow</a>
                    </li>
                    <li>
                        <a href="{{url('tasks/disabled')}}">Disabled Task</a>
                    </li>
                </ul>
            </li>

            
                <li>
                    <a href="#subMenuTransport" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-bus"></span>
                        @lang('lang.transport')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuTransport">
                        
                            <li>
                                <a href="{{url('transport-route')}}"> @lang('lang.routes')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('vehicle')}}"> @lang('lang.vehicle')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('list-drivers')}}"> @lang('driver')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('assign-vehicle')}}"> @lang('lang.assign_vehicle')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_transport_report')}}"> @lang('lang.student_transport_report')</a>
                            </li>
                        
                    </ul>
                </li>
            

            
                <li>
                    <a href="#subMenuDormitory" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-hotel"></span>
                        @lang('lang.dormitory')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuDormitory">
                        
                            <li>
                                <a href="{{url('room-type')}}"> @lang('lang.room_type')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('dormitory-list')}}"> @lang('lang.dormitory')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('room-list')}}"> @lang('lang.dormitory_rooms')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_dormitory_report')}}"> @lang('lang.student_dormitory_report')</a>
                            </li>
                        
                    </ul>
                </li>
            

           
                <li>
                    <a href="#subMenusystemReports" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">
                        <span class="flaticon-analysis"></span>
                        @lang('lang.reports')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenusystemReports">
                        

                            <li>
                                <a href="{{route('student_report')}}">@lang('lang.student_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('guardian_report')}}">@lang('lang.guardian_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_history')}}">@lang('lang.student_history')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_login_report')}}">@lang('lang.student_login_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('fees_statement')}}">@lang('lang.fees_statement')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('balance_fees_report')}}">@lang('lang.balance_fees_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('transaction_report')}}">@lang('lang.transaction_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('class_report')}}">@lang('lang.class_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('class_routine_report')}}">@lang('lang.class_routine')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('exam_routine_report')}}">@lang('lang.exam_routine')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('teacher_class_routine_report')}}">@lang('lang.teacher') @lang('lang.class_routine')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('merit_list_report')}}">@lang('lang.merit_list_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('custom-merit-list')}}">@lang('custom') @lang('lang.merit_list_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('online_exam_report')}}">@lang('lang.online_exam_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('mark_sheet_report_student')}}">@lang('lang.mark_sheet_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('tabulation_sheet_report')}}">@lang('lang.tabulation_sheet_report')</a>
                            </li>
                    
                            <li>
                                <a href="{{route('progress_card_report')}}">@lang('lang.progress_card_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('custom-progress-card')}}"> @lang('lang.custom') @lang('lang.progress_card_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_fine_report')}}">@lang('lang.student_fine_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('user_log')}}">@lang('lang.user_log')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('previous-class-results')}}">@lang('lang.previous') @lang('lang.result') </a>
                            </li>
                        
                            <li>
                                <a href="{{url('previous-record')}}">@lang('lang.previous') @lang('lang.record') </a>
                            </li>
                        
                            {{-- New Client report start --}}

                    
                       
                         @if(SmGeneralSettings::isModule('ResultReports')== TRUE)
                        {{-- ResultReports --}}
                            <li>
                                <a href="{{url('resultreports/cumulative-sheet-report')}}">@lang('lang.cumulative') @lang('lang.sheet') @lang('lang.report')</a>
                            </li> 

                            <li>
                                <a href="{{url('resultreports/continuous-assessment-report')}}">@lang('lang.contonuous') @lang('lang.assessment') @lang('lang.report')</a>
                            </li>
                            <li>

                                <a href="{{url('resultreports/termly-academic-report')}}">@lang('lang.termly') @lang('lang.academic') @lang('lang.report')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/academic-performance-report')}}">@lang('lang.academic') @lang('lang.performance') @lang('lang.report')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/terminal-report-sheet')}}">@lang('lang.terminal') @lang('lang.report') @lang('lang.sheet')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/continuous-assessment-sheet')}}">@lang('lang.continuous') @lang('lang.assessment') @lang('lang.sheet')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/result-version-two')}}">@lang('lang.result') @lang('lang.version') V2</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/result-version-three')}}">@lang('lang.result') @lang('lang.version') V3</a>
                            </li>
                             {{--End New result result report --}}
                        @endif 


                    </ul>
                </li>
            @endif
            {{-- @if(App\SmGeneralSettings::isModule('Saas')== TRUE)

            @else

            @endif --}}
 
            @if(SmGeneralSettings::isModule('InfixBiometrics')== TRUE )
                <li>
                    <a href="#BioSettings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-hotel"></span> 
                        @lang('lang.biometrics')  
                    </a>
                    <ul class="collapse list-unstyled" id="BioSettings">
                       
                        <li>
                            <a href="{{url('infixbiometrics/bio-settings')}}">@lang('lang.biometrics') @lang('lang.settings')</a>
                        </li>
                        
                        <li>
                            <a href="{{url('infixbiometrics/bio-attendance')}}">@lang('lang.attendance')</a>
                        </li>
                        <li>
                            <a href="{{url('infixbiometrics/bio-attendance-reports')}}">@lang('lang.staff') @lang('lang.attendance') @lang('lang.reports')</a>
                        </li>
                        <li>
                            <a href="{{url('infixbiometrics/student-bio-attendance-reports')}}">@lang('lang.student') @lang('lang.attendance') @lang('lang.reports')</a>
                        </li>
                    </ul>
                </li>
            @endif

            
                <li>
                    <a href="#subMenusystemStyle" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">
                        <span class="flaticon-settings"></span>
                        @lang('lang.style')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenusystemStyle">

                        

                            <li>
                                <a href="{{url('background-setting')}}">@lang('lang.background_settings')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('color-style')}}">@lang('lang.color') @lang('lang.theme')</a>
                            </li>
                        
                    </ul>
                </li>
            
            

           {{-- <!-- @if(in_array(20, $main_modules))

            @if(in_array(18, $modules) || Auth::user()->role_id == 1)
                <li>
                    <a href="#subMenuApi" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">
                        <span class="flaticon-settings"></span>
                        @lang('lang.api')
                        @lang('lang.permission')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuApi">
                        @if(in_array(117, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                            <li>
                                <a href="{{url('api/permission')}}">@lang('lang.api') @lang('lang.permission') </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            @endif --> --}}

            @if(SmGeneralSettings::isModule('Saas')== TRUE  && Auth::user()->is_administrator != "yes" )
          
                <li>
                    <a href="#Ticket" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-settings"></span>
                        @lang('lang.ticket_system')
                    </a>
                    <ul class="collapse list-unstyled" id="Ticket">
                        <li><a href="{{ url('school/ticket-view') }}">@lang('lang.ticket_list')</a>
                        </li>
                    </ul>
                    </li>

            @endif
        

        
    </ul>
</nav>
@endif
