@php
    use App\SmGeneralSettings; 
    $school_settings= SmGeneralSettings::where('school_id',Auth::user()->school_id)->first();
    if (Auth::user() == "") { header('location:' . url('/login')); exit(); }
    Session::put('permission', App\GlobalVariable::GlobarModuleLinks());
@endphp
@if(SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes" && Session::get('isSchoolAdmin')==FALSE )
    @include('backEnd/partials/saas_menu')
{{--  @elseif(Session::get('isSchoolAdmin')==TRUE && SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes")  --}}


@else




<input type="hidden" name="url" id="url" value="{{url('/')}}">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="{{url('/')}}">
          <img  src="{{ file_exists(@$school_settings->logo) ? asset($school_settings->logo) : asset('public/uploads/settings/logo.png') }}" alt="logo">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    {{-- {{ Auth::user()->role_id }} --}}
    <ul class="list-unstyled components">
        @if(Auth::user()->role_id != 2 && Auth::user()->role_id != 3 )

            
    
                <li>
                    <a href="{{url('/admin-dashboard')}}" id="admin-dashboard">
                        <span class="flaticon-speedometer"></span>
                        @lang('lang.dashboard')
                    </a>
                </li>
                <li>
                    <a href="{{url('/admin-category')}}">

                        <span class="flaticon-inventory"></span>
                        categories images
                    </a>
                 </li>

                <li>
                    <a href="{{url('/docs')}}">
                        <span class="flaticon-book-1"></span>
                        document
                    </a>
                </li>
                <li>
                        <a href="#subMenuFeesCollection" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">
                            <span class="flaticon-wallet"></span>
                            @lang('Fee and payment')
                        </a>
                        <ul class="collapse list-unstyled" id="subMenuFeesCollection">
                           
                                <li>
                                    <a href="{{url('fee-group')}}"> @lang('lang.fees_group')</a>
                                </li>
                            
                                <li>
                                    <a href="{{url('fee')}}"> fee</a>
                                </li>
                            
                                <li>
                                    <a href="{{url('feescollection/pay-slip-search')}}"> Create quotation</a>
                                </li>
                            
                                <li>
                                    <a href="{{url('feescollection/list-invoice')}}"> payment</a>
                                </li>
                            
                        </ul>
                    </li>

            @endif
            {{-- check module link permission --}}
            
                <li>
                    <a href="#subMenuAccount" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-accounting"></span>
                        @lang('lang.accounts')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuAccount">
                        
                            <li>
                                <a href="{{url('chart-of-account')}}"> @lang('lang.chart_of_account')</a>
                            </li>
                        
                       
                        
                            <li>
                                <a href="{{url('bank-account')}}"> @lang('lang.bank_account')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('add_income')}}"> @lang('lang.income')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('profit')}}"> @lang('lang.profit')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('add-expense')}}"> @lang('lang.expense')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('search_account')}}"> @lang('lang.search')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('payroll')}}"> @lang('lang.payroll')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('payroll-report')}}"> @lang('lang.payroll_report')</a>
                            </li>
                        
                    </ul>
                </li>
            
            
            <li>
                <a href="#subMenuSalary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-wallet"></span>
                    Payroll
                </a>
                <ul class="collapse list-unstyled" id="subMenuSalary">
                    <li>
                        <a href="{{url('salary/me/search')}}">My Payroll</a>
                    </li>
                    
                    
                    <li>
                        <a href="{{url('salary/vn')}}">Payroll Vn</a>
                    </li>
                    <li>
                        <a href="{{url('salary/foreigner')}}">Payroll Foreigner</a>
                    </li>
                    <li>
                        <a href="{{url('salary/list/option')}}">List Payroll</a>
                    </li>
                    
                    <li>
                        <a href="{{url('workday').'/'.Auth::user()->id.'/'.date('Y')}}">Work day</a>
                    </li>
                </ul>
            </li>
            

            
            
            <li>
                <a href="#onlineForms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="fa fa-globe" style="padding-left: 20px"></span>
                    Online forms
                </a>
                <ul class="collapse list-unstyled" id="onlineForms">
                    <li>
                        <a href="{{url('return-to-work')}}">Form return to work</a>
                    </li>

                    <li>
                        <a href="{{url('admin/medical-declaration/entry')}}">Medical Declaration Entry</a>
                    </li>
                    
                    <li>
                        <a href="{{url('admin/medical-declaration/domestic')}}">Medical Declaration Domestic</a>
                    </li>
                </ul>
            </li>

            {{-- @if(@in_array(188, App\GlobalVariable::GlobarModuleLinks())) --}}
                <li>
                <a href="#subMenuLeaveManagement" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">
                    <span class="flaticon-slumber"></span>
                    @lang('lang.leave')
                </a>
                <ul class="collapse list-unstyled" id="subMenuLeaveManagement">
                   
                        <li>
                            
                            <a href="{{url('approve-leave')}}">@lang('your approve request')</a>
                            
                            <a href="{{url('pending-leave')}}">@lang('your pending request')</a>
                            
                        </li>
                    
                   
                        <li>
                            <a href="{{url('apply-leave')}}">@lang('lang.apply_leave')</a>
                        </li>
                    {{-- @endif --}}
                </ul>
            </li>
            {{-- @endif --}}

           

            

            <li>
                    <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-email"></span>
                        @lang('lang.communicate')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuCommunicate">
                        
                            <li>
                                <a href="{{url('notice-list')}}">@lang('lang.notice_board')</a>
                            </li>
                        
                        @if (@$config->Saas == 1 && Auth::user()->is_administrator != "yes" )
                            
                            <li>
                                <a href="{{url('administrator-notice')}}">@lang('lang.administrator') @lang('lang.notice')</a>
                            </li>

                        @endif
                        {{--
                       
                        <li>
                             <a href="{{url('send-message')}}">@lang('lang.send_message')</a>
                        </li>
                        @endif
                        --}}
                        
                            <li>
                                <a href="{{url('send-email')}}">Send Email</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('email-sms-log')}}">@lang('lang.email_sms_log')</a>
                            </li>
                        
                    </ul>
                </li>

            
            
            <li>
                <a href="#subCustomerFacebook" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    CRM
                </a>
                <ul class="collapse list-unstyled" id="subCustomerFacebook">
                    @if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('customer-facebook/add')}}">add customer facebook</a>
                        </li>
                    @endif
                    @if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('customer-facebook/search-customer')}}">search customer facebook</a>
                        </li>
                    @endif
                     <li>
                        <a href="{{url('tasks/add')}}">Work Flow</a>
                    </li>
                    <li>
                        <a href="{{url('tasks/disabled')}}">Disabled Task</a>
                    </li>
                </ul>
            </li>

            

            <li>
                    <a href="#subMenuDormitory" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-hotel"></span>
                        @lang('lang.dormitory')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuDormitory">
                        
                            <li>
                                <a href="{{url('room-type')}}"> @lang('lang.room_type')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('dormitory-list')}}"> @lang('lang.dormitory')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('room-list')}}"> @lang('lang.dormitory_rooms')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_dormitory_report')}}"> @lang('lang.student_dormitory_report')</a>
                            </li>
                        
                    </ul>
                </li>

            
                <li>
                    <a href="#subMenusystemReports" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">
                        <span class="flaticon-analysis"></span>
                        @lang('lang.reports')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenusystemReports">
                        

                            <li>
                                <a href="{{route('student_report')}}">@lang('lang.student_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('guardian_report')}}">@lang('lang.guardian_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_history')}}">@lang('lang.student_history')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_login_report')}}">@lang('lang.student_login_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('fees_statement')}}">@lang('lang.fees_statement')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('balance_fees_report')}}">@lang('lang.balance_fees_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('transaction_report')}}">@lang('lang.transaction_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('class_report')}}">@lang('lang.class_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('class_routine_report')}}">@lang('lang.class_routine')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('exam_routine_report')}}">@lang('lang.exam_routine')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('teacher_class_routine_report')}}">@lang('lang.teacher') @lang('lang.class_routine')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('merit_list_report')}}">@lang('lang.merit_list_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('custom-merit-list')}}">@lang('custom') @lang('lang.merit_list_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('online_exam_report')}}">@lang('lang.online_exam_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('mark_sheet_report_student')}}">@lang('lang.mark_sheet_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('tabulation_sheet_report')}}">@lang('lang.tabulation_sheet_report')</a>
                            </li>
                    
                            <li>
                                <a href="{{route('progress_card_report')}}">@lang('lang.progress_card_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('custom-progress-card')}}"> @lang('lang.custom') @lang('lang.progress_card_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_fine_report')}}">@lang('lang.student_fine_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('user_log')}}">@lang('lang.user_log')</a>
                            </li>
                        
                            <li>
                                <a href="{{url('previous-class-results')}}">@lang('lang.previous') @lang('lang.result') </a>
                            </li>
                        
                            <li>
                                <a href="{{url('previous-record')}}">@lang('lang.previous') @lang('lang.record') </a>
                            </li>
                        
                            {{-- New Client report start --}}

                    
                       
                         @if(SmGeneralSettings::isModule('ResultReports')== TRUE)
                        {{-- ResultReports --}}
                            <li>
                                <a href="{{url('resultreports/cumulative-sheet-report')}}">@lang('lang.cumulative') @lang('lang.sheet') @lang('lang.report')</a>
                            </li> 

                            <li>
                                <a href="{{url('resultreports/continuous-assessment-report')}}">@lang('lang.contonuous') @lang('lang.assessment') @lang('lang.report')</a>
                            </li>
                            <li>

                                <a href="{{url('resultreports/termly-academic-report')}}">@lang('lang.termly') @lang('lang.academic') @lang('lang.report')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/academic-performance-report')}}">@lang('lang.academic') @lang('lang.performance') @lang('lang.report')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/terminal-report-sheet')}}">@lang('lang.terminal') @lang('lang.report') @lang('lang.sheet')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/continuous-assessment-sheet')}}">@lang('lang.continuous') @lang('lang.assessment') @lang('lang.sheet')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/result-version-two')}}">@lang('lang.result') @lang('lang.version') V2</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/result-version-three')}}">@lang('lang.result') @lang('lang.version') V3</a>
                            </li>
                             {{--End New result result report --}}
                        @endif 


                    </ul>
                </li>
            
       
        <!-- Student Panel --> 
       

    </ul>
</nav>
@endif
