@php
    use App\SmGeneralSettings; 
    $school_settings= SmGeneralSettings::where('school_id',Auth::user()->school_id)->first();
    if (Auth::user() == "") { header('location:' . url('/login')); exit(); }
    Session::put('permission', App\GlobalVariable::GlobarModuleLinks());
@endphp
@if(SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes" && Session::get('isSchoolAdmin')==FALSE )
    @include('backEnd/partials/saas_menu')
{{--  @elseif(Session::get('isSchoolAdmin')==TRUE && SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes")  --}}


@else




<input type="hidden" name="url" id="url" value="{{url('/')}}">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="{{url('/')}}">
          <img  src="{{ file_exists(@$school_settings->logo) ? asset($school_settings->logo) : asset('public/uploads/settings/logo.png') }}" alt="logo">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    {{-- {{ Auth::user()->role_id }} --}}
    <ul class="list-unstyled components">
        @if(Auth::user()->role_id != 2 && Auth::user()->role_id != 3 )

            
    
                <li>
                    <a href="{{url('/admin-dashboard')}}" id="admin-dashboard">
                        <span class="flaticon-speedometer"></span>
                        @lang('lang.dashboard')
                    </a>
                </li>
                <li>
                    <a href="{{url('/admin-category')}}">

                        <span class="flaticon-inventory"></span>
                        categories images
                    </a>
                 </li>

            
            
                
            
            
             
            
            <li>
                <a href="#subMenuDevices" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span>
                        <img style="margin-left: 18px" src="/public/images/device.png" width="17px" alt="">
                    </span>
                    Devices Management
                </a>
                <ul class="collapse list-unstyled" id="subMenuDevices">
                    <li>
                        <a href="#" onclick="qrcheck()">QRCODE Checking</a>
                    </li>
                    <li>
                        <a href="{{url('devices')}}">In Stock</a>
                    </li>
                
                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                    <li>
                        <a href="{{url('devices/repairing')}}">Repairing Devices</a>
                    </li>
                    @endif
               
                
                    <li>
                        <a href="{{url('devices/equipments-handover')}}">Equipments Handover</a>
                    </li>
                </ul>
            </li>

            

            @if(SmGeneralSettings::isModule('ParentRegistration')== TRUE )
            @if(@in_array(542, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
             <li>
                <a href="#subMenuStudentRegistration" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-reading"></span>
                    @lang('lang.registration')
                </a>
                <ul class="collapse list-unstyled" id="subMenuStudentRegistration">
                        @if(@in_array(543, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('parentregistration/student-list')}}"> @lang('lang.student_list')</a>
                        </li>
                        @endif
                        @if(SmGeneralSettings::isModule('Saas') != TRUE)
                            @if(@in_array(547, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                            <li>
                                <a href="{{url('parentregistration/settings')}}"> @lang('lang.settings')</a>
                            </li>
                            @endif
                        @endif
                    </ul>
            </li>

            @endif
            @endif




            <li>
                    <a href="#subMenuStudent" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-reading"></span>
                        @lang('lang.student_information')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuStudent">
                        @if(@in_array(193, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->staff->designations->id == 1 || Auth::user()->staff->designations->id == 14 || Auth::user()->staff->designations->id == 45)
                            <li>
                                <a href="{{url('student-trial-list')}}">@lang('student trial list')</a>
                            </li>
                        @endif
                            <li>
                                <a href="{{route('student_category')}}"> @lang('lang.student_category')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_list')}}"> @lang('lang.student_list')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_attendance')}}"> @lang('lang.student_attendance')</a>
                            </li>
                       
                            <li>
                                <a href="{{route('student_attendance_report')}}"> @lang('lang.student_attendance_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('subject-wise-attendance')}}"> @lang('lang.subject') @lang('lang.wise') @lang('lang.attendance') </a>
                            </li>
                        
                            <li>
                                <a href="{{url('subject-attendance-report')}}"> @lang('lang.subject_attendance_report') </a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_admission')}}">@lang('lang.student_admission')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_group')}}">@lang('lang.student_group')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_promote')}}">@lang('lang.student_promote')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('disabled_student')}}">@lang('lang.disabled_student')</a>
                            </li>
                    </ul>
                </li>
                 <li>
                <a href="#subMenuStudyResult" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-reading"></span>
                    Study Information
                </a>
                <ul class="collapse list-unstyled" id="subMenuStudyResult">
                    <li><a href="{{url('student-list')}}">Learning Outcomes</a></li>
                    <li><a href="{{url('study-information/exam-schedule/0')}}">Exam Schedule</a></li>
                </ul>
            </li>
            
            <li>
                <a href="#subMenuKitchen" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-reading"></span>
                    Kitchen
                </a>
                <ul class="collapse list-unstyled" id="subMenuKitchen">
                    <li><a href="{{url('lunch-menu')}}">Lunch menu</a></li>
                </ul>
            </li>

                


            
            <li>
                    <a href="#subMenuTeacher" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-professor"></span>
                        @lang('lang.study_material')
                    </a>

                    <ul class="collapse list-unstyled" id="subMenuTeacher">
                       
                            <li>
                                <a href="{{url('upload-content')}}"> @lang('lang.upload_content')</a>
                            </li>
                        

                       
                            <li>
                                <a href="{{url('assignment-list')}}">@lang('lang.assignment')</a>
                            </li>
                        

                        
                            <li>
                                <a href="{{url('syllabus-list')}}">@lang('lang.syllabus')</a>
                            </li>
                        

                        
                            <li>
                                <a href="{{url('other-download-list')}}">@lang('lang.other_download')</a>
                            </li>
                        
                    </ul>
                </li>

            
                    <li>
                        <a href="#subMenuFeesCollection" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">
                            <span class="flaticon-wallet"></span>
                            @lang('Fee and payment')
                        </a>
                        <ul class="collapse list-unstyled" id="subMenuFeesCollection">
                           
                                <li>
                                    <a href="{{url('fee-group')}}"> @lang('lang.fees_group')</a>
                                </li>
                            
                                <li>
                                    <a href="{{url('fee')}}"> fee</a>
                                </li>
                            
                            @if(@in_array(131, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 || Auth::user()->role_id == 5 || Auth::user()->role_id == 6 || Auth::user()->role_id == 10)
                                <li>
                                    <a href="{{url('feescollection/pay-slip-search')}}"> Create quotation</a>
                                </li>
                            @endif
                            @if(@in_array(118, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 || Auth::user()->role_id == 5 || Auth::user()->role_id == 6 || Auth::user()->role_id == 10)
                                <li>
                                    <a href="{{url('feescollection/list-invoice')}}"> payment</a>
                                </li>
                            @endif
                            
                        </ul>
                    </li>
                
            {{-- check module link permission --}}
            
            
            
            <li>
                <a href="#subMenuSalary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-wallet"></span>
                    Payroll
                </a>
                <ul class="collapse list-unstyled" id="subMenuSalary">
                    <li>
                        <a href="{{url('salary/me/search')}}">My Payroll</a>
                    </li>
                    
                    
                    <li>
                        <a href="{{url('workday').'/'.Auth::user()->id.'/'.date('Y')}}">Work day</a>
                    </li>
                </ul>
            </li>
            

            
            <li>
                <a href="#onlineForms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="fa fa-globe" style="padding-left: 20px"></span>
                    Online forms
                </a>
                <ul class="collapse list-unstyled" id="onlineForms">
                    <li>
                        <a href="{{url('return-to-work')}}">Form return to work</a>
                    </li>

                    <li>
                        <a href="{{url('admin/medical-declaration/entry')}}">Medical Declaration Entry</a>
                    </li>
                    
                    <li>
                        <a href="{{url('admin/medical-declaration/domestic')}}">Medical Declaration Domestic</a>
                    </li>
                </ul>
            </li>

            

            {{-- @if(@in_array(188, App\GlobalVariable::GlobarModuleLinks())) --}}
                <li>
                <a href="#subMenuLeaveManagement" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">
                    <span class="flaticon-slumber"></span>
                    @lang('lang.leave')
                </a>
                <ul class="collapse list-unstyled" id="subMenuLeaveManagement">
                    
                        <li>
                            
                            <a href="{{url('approve-leave')}}">@lang('your approve request')</a>
                            
                            <a href="{{url('pending-leave')}}">@lang('your pending request')</a>
                            
                        </li>
                    
                        <li>
                            <a href="{{url('pending-leave-student')}}">@lang('student leave request')</a>
                        </li>
                    
                    {{-- @if(@in_array(193, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1) --}}
                        <li>
                            <a href="{{url('apply-leave')}}">@lang('lang.apply_leave')</a>
                        </li>
                    {{-- @endif --}}
                </ul>
            </li>
            {{-- @endif --}}


            <li>
                    <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-email"></span>
                        @lang('lang.communicate')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuCommunicate">
                        
                            <li>
                                <a href="{{url('notice-list')}}">@lang('lang.notice_board')</a>
                            </li>
                        
                        @if (@$config->Saas == 1 && Auth::user()->is_administrator != "yes" )
                            
                            <li>
                                <a href="{{url('administrator-notice')}}">@lang('lang.administrator') @lang('lang.notice')</a>
                            </li>

                        @endif
                        {{--
                       
                        <li>
                             <a href="{{url('send-message')}}">@lang('lang.send_message')</a>
                        </li>
                        @endif
                        --}}
                        
                            <li>
                                <a href="{{url('send-email')}}">Send Email</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('email-sms-log')}}">@lang('lang.email_sms_log')</a>
                            </li>
                        
                    </ul>
                </li>

            
            
           <li>
                <a href="#subCustomerFacebook" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    CRM
                </a>
                <ul class="collapse list-unstyled" id="subCustomerFacebook">
                    @if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('customer-facebook/add')}}">add customer facebook</a>
                        </li>
                    @endif
                    @if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('customer-facebook/search-customer')}}">search customer facebook</a>
                        </li>
                    @endif
                     <li>
                        <a href="{{url('tasks/add')}}">Work Flow</a>
                    </li>
                    <li>
                        <a href="{{url('tasks/disabled')}}">Disabled Task</a>
                    </li>
                </ul>
            </li>

            <li>
                    <a href="#subMenuTransport" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-bus"></span>
                        @lang('lang.transport')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuTransport">
                        
                            <li>
                                <a href="{{url('transport-route')}}"> @lang('lang.routes')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('vehicle')}}"> @lang('lang.vehicle')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('list-drivers')}}"> @lang('driver')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('assign-vehicle')}}"> @lang('lang.assign_vehicle')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('student_transport_report')}}"> @lang('lang.student_transport_report')</a>
                            </li>
                        
                    </ul>
                </li>

           

            
                <li>
                    <a href="#subMenuEvent" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-hotel"></span>
                        @lang('lang.event')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuEvent">
                        
                            <li>
                                <a href="{{url('event-list')}}"> @lang('event list')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('event-participation')}}"> @lang('event participation')</a>
                            </li>
                        
                        
                    </ul>
                </li>
           

            <li>
                    <a href="#subMenusystemReports" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">
                        <span class="flaticon-analysis"></span>
                        @lang('lang.reports')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenusystemReports">
                        

                            <li>
                                <a href="{{route('student_report')}}">@lang('lang.student_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('guardian_report')}}">@lang('lang.guardian_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('student_history')}}">@lang('lang.student_history')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('student_login_report')}}">@lang('lang.student_login_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('fees_statement')}}">@lang('lang.fees_statement')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('balance_fees_report')}}">@lang('lang.balance_fees_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('transaction_report')}}">@lang('lang.transaction_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('class_report')}}">@lang('lang.class_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('class_routine_report')}}">@lang('lang.class_routine')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('exam_routine_report')}}">@lang('lang.exam_routine')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('teacher_class_routine_report')}}">@lang('lang.teacher') @lang('lang.class_routine')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('merit_list_report')}}">@lang('lang.merit_list_report')</a>
                            </li>

                            <li>
                                <a href="{{url('custom-merit-list')}}">@lang('custom') @lang('lang.merit_list_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('online_exam_report')}}">@lang('lang.online_exam_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('mark_sheet_report_student')}}">@lang('lang.mark_sheet_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('tabulation_sheet_report')}}">@lang('lang.tabulation_sheet_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('progress_card_report')}}">@lang('lang.progress_card_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('custom-progress-card')}}"> @lang('lang.custom') @lang('lang.progress_card_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('student_fine_report')}}">@lang('lang.student_fine_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('user_log')}}">@lang('lang.user_log')</a>
                            </li>
                         
                        
                            <li>
                                <a href="{{url('previous-class-results')}}">@lang('lang.previous') @lang('lang.result') </a>
                            </li>
                         
                        
                            <li>
                                <a href="{{url('previous-record')}}">@lang('lang.previous') @lang('lang.record') </a>
                            </li>
                         
                            {{-- New Client report start --}}

                    
                       
                         @if(SmGeneralSettings::isModule('ResultReports')== TRUE)
                        {{-- ResultReports --}}
                            <li>
                                <a href="{{url('resultreports/cumulative-sheet-report')}}">@lang('lang.cumulative') @lang('lang.sheet') @lang('lang.report')</a>
                            </li> 

                            <li>
                                <a href="{{url('resultreports/continuous-assessment-report')}}">@lang('lang.contonuous') @lang('lang.assessment') @lang('lang.report')</a>
                            </li>
                            <li>

                                <a href="{{url('resultreports/termly-academic-report')}}">@lang('lang.termly') @lang('lang.academic') @lang('lang.report')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/academic-performance-report')}}">@lang('lang.academic') @lang('lang.performance') @lang('lang.report')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/terminal-report-sheet')}}">@lang('lang.terminal') @lang('lang.report') @lang('lang.sheet')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/continuous-assessment-sheet')}}">@lang('lang.continuous') @lang('lang.assessment') @lang('lang.sheet')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/result-version-two')}}">@lang('lang.result') @lang('lang.version') V2</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/result-version-three')}}">@lang('lang.result') @lang('lang.version') V3</a>
                            </li>
                             {{--End New result result report --}}
                        @endif 

                    
                    </ul>
                </li>
 
        
       @endif


    </ul>
</nav>
@endif
