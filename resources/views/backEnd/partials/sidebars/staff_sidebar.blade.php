@php
    use App\SmGeneralSettings; 
    $school_settings= SmGeneralSettings::where('school_id',Auth::user()->school_id)->first();
    if (Auth::user() == "") { header('location:' . url('/login')); exit(); }
    Session::put('permission', App\GlobalVariable::GlobarModuleLinks());
@endphp
@if(SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes" && Session::get('isSchoolAdmin')==FALSE )
    @include('backEnd/partials/saas_menu')
{{--  @elseif(Session::get('isSchoolAdmin')==TRUE && SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes")  --}}


@else




<input type="hidden" name="url" id="url" value="{{url('/')}}">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="{{url('/')}}">
          <img  src="{{ file_exists(@$school_settings->logo) ? asset($school_settings->logo) : asset('public/uploads/settings/logo.png') }}" alt="logo">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    {{-- {{ Auth::user()->role_id }} --}}
    <ul class="list-unstyled components">
        @if(Auth::user()->role_id != 2 && Auth::user()->role_id != 3 )

            
    
                <li>
                    <a href="{{url('/admin-dashboard')}}" id="admin-dashboard">
                        <span class="flaticon-speedometer"></span>
                        @lang('lang.dashboard')
                    </a>
                </li>
                <li>
                    <a href="{{url('/admin-category')}}">

                        <span class="flaticon-inventory"></span>
                        categories images
                    </a>
                 </li>

                <li>
                    <a href="{{url('/docs')}}">
                        <span class="flaticon-book-1"></span>
                        document
                    </a>
                </li>
                
            @endif
            {{-- check module link permission --}}
            
                
            <li>
                <a href="#subMenuDevices" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span>
                        <img style="margin-left: 18px" src="/public/images/device.png" width="17px" alt="">
                    </span>
                    Devices Management
                </a>
                <ul class="collapse list-unstyled" id="subMenuDevices">
                    <li>
                        <a href="#" onclick="qrcheck()">QRCODE Checking</a>
                    </li>
                    <li>
                        <a href="{{url('devices')}}">In Stock</a>
                    </li>
                
                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16)
                    <li>
                        <a href="{{url('devices/repairing')}}">Repairing Devices</a>
                    </li>
                    @endif
               
                
                    <li>
                        <a href="{{url('devices/equipments-handover')}}">Equipments Handover</a>
                    </li>
                </ul>
            </li>
            
            <li>
                <a href="#subMenuSalary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-wallet"></span>
                    Payroll
                </a>
                <ul class="collapse list-unstyled" id="subMenuSalary">
                    <li>
                        <a href="{{url('salary/me/search')}}">My Payroll</a>
                    </li>
                    <li>
                        <a href="{{url('workday').'/'.Auth::user()->id.'/'.date('Y')}}">Work day</a>
                    </li>
                </ul>
            </li>
            

            
            
            <li>
                <a href="#onlineForms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="fa fa-globe" style="padding-left: 20px"></span>
                    Online forms
                </a>
                <ul class="collapse list-unstyled" id="onlineForms">
                    <li>
                        <a href="{{url('return-to-work')}}">Form return to work</a>
                    </li>

                    <li>
                        <a href="{{url('admin/medical-declaration/entry')}}">Medical Declaration Entry</a>
                    </li>
                    
                    <li>
                        <a href="{{url('admin/medical-declaration/domestic')}}">Medical Declaration Domestic</a>
                    </li>
                </ul>
            </li>

            {{-- @if(@in_array(188, App\GlobalVariable::GlobarModuleLinks())) --}}
                <li>
                <a href="#subMenuLeaveManagement" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">
                    <span class="flaticon-slumber"></span>
                    @lang('lang.leave')
                </a>
                <ul class="collapse list-unstyled" id="subMenuLeaveManagement">
                   
                        <li>
                            
                            <a href="{{url('approve-leave')}}">@lang('your approve request')</a>
                            
                            <a href="{{url('pending-leave')}}">@lang('your pending request')</a>
                            
                        </li>
                    
                   
                        <li>
                            <a href="{{url('apply-leave')}}">@lang('lang.apply_leave')</a>
                        </li>
                    {{-- @endif --}}
                </ul>
            </li>
            {{-- @endif --}}

           

            

            <li>
                    <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-email"></span>
                        @lang('lang.communicate')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuCommunicate">
                        
                            <li>
                                <a href="{{url('notice-list')}}">@lang('lang.notice_board')</a>
                            </li>
                        
                        @if (@$config->Saas == 1 && Auth::user()->is_administrator != "yes" )
                            
                            <li>
                                <a href="{{url('administrator-notice')}}">@lang('lang.administrator') @lang('lang.notice')</a>
                            </li>

                        @endif
                        {{--
                       
                        <li>
                             <a href="{{url('send-message')}}">@lang('lang.send_message')</a>
                        </li>
                        @endif
                        --}}
                        
                            <li>
                                <a href="{{url('send-email')}}">Send Email</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('email-sms-log')}}">@lang('lang.email_sms_log')</a>
                            </li>
                        
                    </ul>
                </li>

            
            
            <li>
                <a href="#subCustomerFacebook" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    CRM
                </a>
                <ul class="collapse list-unstyled" id="subCustomerFacebook">
                    @if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('customer-facebook/add')}}">add customer facebook</a>
                        </li>
                    @endif
                    @if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1)
                        <li>
                            <a href="{{url('customer-facebook/search-customer')}}">search customer facebook</a>
                        </li>
                    @endif
                     <li>
                        <a href="{{url('tasks/add')}}">Work Flow</a>
                    </li>
                    <li>
                        <a href="{{url('tasks/disabled')}}">Disabled Task</a>
                    </li>
                </ul>
            </li>

            

            

            
                
            
       
        <!-- Student Panel --> 
       

    </ul>
</nav>
@endif
