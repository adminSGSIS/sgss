@php
    use App\SmGeneralSettings; 
    $school_settings= SmGeneralSettings::where('school_id',Auth::user()->school_id)->first();
    if (Auth::user() == "") { header('location:' . url('/login')); exit(); }
    Session::put('permission', App\GlobalVariable::GlobarModuleLinks());
@endphp
@if(SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes" && Session::get('isSchoolAdmin')==FALSE )
    @include('backEnd/partials/saas_menu')
{{--  @elseif(Session::get('isSchoolAdmin')==TRUE && SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes")  --}}


@else

<input type="hidden" name="url" id="url" value="{{url('/')}}">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="{{url('/')}}">
          <img  src="{{ file_exists(@$school_settings->logo) ? asset($school_settings->logo) : asset('public/uploads/settings/logo.png') }}" alt="logo">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    {{-- {{ Auth::user()->role_id }} --}}
    <ul class="list-unstyled components">
            <li>
                <a href="{{url('/admin-dashboard')}}" id="admin-dashboard">
                    <span class="flaticon-speedometer"></span>
                    @lang('lang.dashboard')
                </a>
            </li>
            <li>
                <a href="{{url('/admin-category')}}">

                    <span class="flaticon-inventory"></span>
                    categories images
                </a>
             </li>

            
            <li>
                <a href="{{url('/docs')}}">
                    <span class="flaticon-book-1"></span>
                    document
                </a>
            </li>
            
            
            <li>
                <a href="#subMenuDevices" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span>
                        <img style="margin-left: 18px" src="/public/images/device.png" width="17px" alt="">
                    </span>
                    Devices Management
                </a>
                <ul class="collapse list-unstyled" id="subMenuDevices">
                    <li>
                        <a href="#" onclick="qrcheck()">QRCODE Checking</a>
                    </li>
                    <li>
                        <a href="{{url('devices')}}">In Stock</a>
                    </li>

                    <li>
                        <a href="{{url('devices/repairing')}}">Repairing Devices</a>
                    </li>
                    
                    <li>
                        <a href="{{url('devices/equipments-handover')}}">Equipments Handover</a>
                    </li>
                </ul>
            </li>

                <li>
                    <a href="#subMenuStudent" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-reading"></span>
                        @lang('lang.student_information')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuStudent">
                            <li>
                                <a href="{{route('student_list')}}"> @lang('lang.student_list')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_attendance')}}"> @lang('lang.student_attendance')</a>
                            </li>
                       
                            <li>
                                <a href="{{route('student_attendance_report')}}"> @lang('lang.student_attendance_report')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('subject-wise-attendance')}}"> @lang('lang.subject') @lang('lang.wise') @lang('lang.attendance') </a>
                            </li>
                        
                            <li>
                                <a href="{{url('subject-attendance-report')}}"> @lang('lang.subject_attendance_report') </a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_admission')}}">@lang('lang.student_admission')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_group')}}">@lang('lang.student_group')</a>
                            </li>
                        
                            <li>
                                <a href="{{route('student_promote')}}">@lang('lang.student_promote')</a>
                            </li>
                        
                            
                    </ul>
                </li>
            <li>
                <a href="#subMenuStudyResult" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-reading"></span>
                    Study Information
                </a>
                <ul class="collapse list-unstyled" id="subMenuStudyResult">
                    <li><a href="{{url('student-list')}}">Learning Outcomes</a></li>
                    <li><a href="{{url('study-information/exam-schedule/0')}}">Exam Schedule</a></li>
                </ul>
            </li>
            
            
                <li>
                    <a href="#subMenuAcademic" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-graduated-student"></span>
                        @lang('lang.academics')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuAcademic"> 
                            <li>
                                <a href="{{route('optional-subject')}}"> @lang('lang.optional') @lang('lang.subject') </a>
                            </li>
                        <!--LONG-->
                         <li>
                            <a href="{{url('/schedule')}}">
                                School timetable
                            </a>
                         </li>
                        <!--END-->
                        
                            <li>
                                <a href="{{route('section')}}"> @lang('lang.section')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('class')}}"> @lang('lang.class')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('subject')}}"> @lang('lang.subjects')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('class-room')}}"> @lang('lang.class_room')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('class-time')}}"> @lang('lang.cl_ex_time_setup')</a>
                            </li>
                        
                         
                            <li>
                                <a href="{{url('assign-class-teacher')}}"> @lang('lang.assign_class_teacher')</a>
                            </li>
                        
                         
                            <li>
                                <a href="{{route('assign_subject')}}"> @lang('lang.assign_subject')</a>
                            </li>
                        
                         
                            <li>
                                <a href="{{route('class_routine_new')}}"> @lang('lang.class_routine')</a>

                            </li>
                        

                    <!-- only for teacher -->
                        
                            <li>
                                <a href="{{url('view-teacher-routine')}}">@lang('lang.view') @lang('lang.class_routine')</a>
                            </li>
                        
                    </ul>
                </li>
            


            
           
                <li>
                    <a href="#subMenuTeacher" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-professor"></span>
                        @lang('lang.study_material')
                    </a>

                    <ul class="collapse list-unstyled" id="subMenuTeacher">
                       
                            <li>
                                <a href="{{url('upload-content')}}"> @lang('lang.upload_content')</a>
                            </li>
                        

                       
                            <li>
                                <a href="{{url('assignment-list')}}">@lang('lang.assignment')</a>
                            </li>
                        

                        
                            <li>
                                <a href="{{url('syllabus-list')}}">@lang('lang.syllabus')</a>
                            </li>
                        

                        
                            <li>
                                <a href="{{url('other-download-list')}}">@lang('lang.other_download')</a>
                            </li>
                        
                    </ul>
                </li>
            

            
            <li>
                <a href="#subMenuSalary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-wallet"></span>
                    Payroll
                </a>
                <ul class="collapse list-unstyled" id="subMenuSalary">
                    <li>
                        <a href="{{url('salary/me/search')}}">My Payroll</a>
                    </li>
                    <li>
                        <a href="{{url('workday').'/'.Auth::user()->id.'/'.date('Y')}}">Work day</a>
                    </li>
                </ul>
            </li>
            

            
            <li>
                <a href="#onlineForms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="fa fa-globe" style="padding-left: 20px"></span>
                    Online forms
                </a>
                <ul class="collapse list-unstyled" id="onlineForms">
                    <li>
                        <a href="{{url('return-to-work')}}">Form return to work</a>
                    </li>

                    <li>
                        <a href="{{url('admin/medical-declaration/entry')}}">Medical Declaration Entry</a>
                    </li>
                    
                    <li>
                        <a href="{{url('admin/medical-declaration/domestic')}}">Medical Declaration Domestic</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#subMenuLeaveManagement" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">
                    <span class="flaticon-slumber"></span>
                    @lang('lang.leave')
                </a>
                <ul class="collapse list-unstyled" id="subMenuLeaveManagement">
                    <li>
                        
                        <a href="{{url('approve-leave')}}">@lang('your approve request')</a>
                        
                        <a href="{{url('pending-leave')}}">@lang('your pending request')</a>
                        
                    </li>
                    
                    
                    <li>
                        <a href="{{url('pending-leave-student')}}">@lang('student leave request')</a>
                    </li>
                
                    <li>
                        <a href="{{url('apply-leave')}}">@lang('lang.apply_leave')</a>
                    </li>
                   
                </ul>
            </li>

            
                <li>
                    <a href="#subMenuExam" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-test"></span>
                        @lang('lang.examination')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuExam">
                       
                            <li>
                                <a href="{{url('marks-grade')}}"> @lang('lang.marks_grade')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('exam-type')}}"> @lang('lang.add_exam_type')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('exam')}}"> @lang('lang.exam_setup')</a>
                            </li>
                        

                        
                            <li>
                                <a href="{{route('exam_schedule')}}"> @lang('lang.exam_schedule')</a>
                            </li>
                        
                        

                        
                            <li>
                                <a href="{{route('exam_attendance')}}"> @lang('lang.exam_attendance')</a>
                            </li>
                        

                        
                            <li>
                                <a href="{{route('marks_register')}}"> @lang('lang.marks_register')</a>
                            </li>
                        

                        
                        
                            <li>
                                <a href="{{route('send_marks_by_sms')}}"> @lang('lang.send_marks_by_sms')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('question-group')}}">@lang('lang.question_group')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('question-bank')}}">@lang('lang.question_bank')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('online-exam')}}">@lang('lang.online_exam')</a>
                            </li>
                        

                    </ul>
                </li>
            

       

            

                <li>
                    <a href="#subMenuHomework" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-book"></span>
                        @lang('lang.home_work')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuHomework">
                        
                            <li>
                                <a href="{{route('add-homeworks')}}"> @lang('lang.add_homework')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('homework-list')}}"> @lang('lang.homework_list')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('evaluation-report')}}"> @lang('lang.evaluation_report')</a>
                            </li>
                        
                    </ul>
                </li>

            

            
                <li>
                    <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-email"></span>
                        @lang('lang.communicate')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuCommunicate">
                        
                            <li>
                                <a href="{{url('notice-list')}}">@lang('lang.notice_board')</a>
                            </li>
                        
                        @if (@$config->Saas == 1 && Auth::user()->is_administrator != "yes" )
                            
                            <li>
                                <a href="{{url('administrator-notice')}}">@lang('lang.administrator') @lang('lang.notice')</a>
                            </li>

                        @endif
                        {{--
                       
                        <li>
                             <a href="{{url('send-message')}}">@lang('lang.send_message')</a>
                        </li>
                        @endif
                        --}}
                        
                            <li>
                                <a href="{{url('send-email')}}">Send Email</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('email-sms-log')}}">@lang('lang.email_sms_log')</a>
                            </li>
                        
                    </ul>
                </li>
            

            
                <li>
                    <a href="#subMenulibrary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-book-1"></span>
                        @lang('lang.library')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenulibrary">
                        
                            <li>
                                <a href="{{url('library')}}"> @lang('Library Manage')</a>
                            </li>
                        
                       
                            <li>
                                <a href="{{url('book-category-list')}}"> @lang('lang.book_category')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('add-book')}}"> @lang('lang.add_book')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('book-list')}}"> @lang('lang.book_list')</a>
                            </li>
                        
                        
                        
                            <li>
                                <a href="{{url('library-member')}}"> @lang('lang.library_member')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('member-list')}}"> @lang('lang.member_list')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('all-issed-book')}}"> @lang('lang.all_issued_book')</a>
                            </li>
                        
                    </ul>
                </li>
            

            
            <li>
                <a href="#subCustomerFacebook" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    CRM
                </a>
                <ul class="collapse list-unstyled" id="subCustomerFacebook">
                    
                        <li>
                            <a href="{{url('customer-facebook/add')}}">add customer facebook</a>
                        </li>
                    
                    
                        <li>
                            <a href="{{url('customer-facebook/search-customer')}}">search customer facebook</a>
                        </li>
                    
                     <li>
                        <a href="{{url('tasks/add')}}">Work Flow</a>
                    </li>
                    <li>
                        <a href="{{url('tasks/disabled')}}">Disabled Task</a>
                    </li>
                </ul>
            </li>

            
                <li>
                    <a href="#subMenuTransport" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-bus"></span>
                        @lang('lang.transport')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuTransport">
                        
                            <li>
                                <a href="{{url('transport-route')}}"> @lang('lang.routes')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('vehicle')}}"> @lang('lang.vehicle')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('list-drivers')}}"> @lang('driver')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('assign-vehicle')}}"> @lang('lang.assign_vehicle')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('student_transport_report')}}"> @lang('lang.student_transport_report')</a>
                            </li>
                        
                    </ul>
                </li>
            

            
                <li>
                    <a href="#subMenuDormitory" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-hotel"></span>
                        @lang('lang.dormitory')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuDormitory">
                        
                            <li>
                                <a href="{{url('room-type')}}"> @lang('lang.room_type')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('dormitory-list')}}"> @lang('lang.dormitory')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('room-list')}}"> @lang('lang.dormitory_rooms')</a>
                            </li>
                        
                        
                        
                        
                            <li>
                                <a href="{{route('student_dormitory_report')}}"> @lang('lang.student_dormitory_report')</a>
                            </li>
                        
                    </ul>
                </li>
            

            
                <li>
                    <a href="#subMenusystemReports" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">
                        <span class="flaticon-analysis"></span>
                        @lang('lang.reports')
                    </a>
                    <ul class="collapse list-unstyled" id="subMenusystemReports">
                        

                            <li>
                                <a href="{{route('student_report')}}">@lang('lang.student_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('guardian_report')}}">@lang('lang.guardian_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('student_history')}}">@lang('lang.student_history')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('student_login_report')}}">@lang('lang.student_login_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('fees_statement')}}">@lang('lang.fees_statement')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('balance_fees_report')}}">@lang('lang.balance_fees_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('transaction_report')}}">@lang('lang.transaction_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('class_report')}}">@lang('lang.class_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('class_routine_report')}}">@lang('lang.class_routine')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('exam_routine_report')}}">@lang('lang.exam_routine')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('teacher_class_routine_report')}}">@lang('lang.teacher') @lang('lang.class_routine')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('merit_list_report')}}">@lang('lang.merit_list_report')</a>
                            </li>

                            <li>
                                <a href="{{url('custom-merit-list')}}">@lang('custom') @lang('lang.merit_list_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('online_exam_report')}}">@lang('lang.online_exam_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('mark_sheet_report_student')}}">@lang('lang.mark_sheet_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('tabulation_sheet_report')}}">@lang('lang.tabulation_sheet_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('progress_card_report')}}">@lang('lang.progress_card_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{url('custom-progress-card')}}"> @lang('lang.custom') @lang('lang.progress_card_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('student_fine_report')}}">@lang('lang.student_fine_report')</a>
                            </li>
                        
                        
                            <li>
                                <a href="{{route('user_log')}}">@lang('lang.user_log')</a>
                            </li>
                         
                        
                            <li>
                                <a href="{{url('previous-class-results')}}">@lang('lang.previous') @lang('lang.result') </a>
                            </li>
                         
                        
                            <li>
                                <a href="{{url('previous-record')}}">@lang('lang.previous') @lang('lang.record') </a>
                            </li>
                         
                            {{-- New Client report start --}}

                    
                       
                         @if(SmGeneralSettings::isModule('ResultReports')== TRUE)
                        {{-- ResultReports --}}
                            <li>
                                <a href="{{url('resultreports/cumulative-sheet-report')}}">@lang('lang.cumulative') @lang('lang.sheet') @lang('lang.report')</a>
                            </li> 

                            <li>
                                <a href="{{url('resultreports/continuous-assessment-report')}}">@lang('lang.contonuous') @lang('lang.assessment') @lang('lang.report')</a>
                            </li>
                            <li>

                                <a href="{{url('resultreports/termly-academic-report')}}">@lang('lang.termly') @lang('lang.academic') @lang('lang.report')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/academic-performance-report')}}">@lang('lang.academic') @lang('lang.performance') @lang('lang.report')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/terminal-report-sheet')}}">@lang('lang.terminal') @lang('lang.report') @lang('lang.sheet')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/continuous-assessment-sheet')}}">@lang('lang.continuous') @lang('lang.assessment') @lang('lang.sheet')</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/result-version-two')}}">@lang('lang.result') @lang('lang.version') V2</a>
                                </li>
                            <li>

                                <a href="{{url('resultreports/result-version-three')}}">@lang('lang.result') @lang('lang.version') V3</a>
                            </li>
                             {{--End New result result report --}}
                        @endif 

@endif
                    </ul>
                </li>
    </ul>
</nav>

