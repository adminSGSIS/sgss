@extends('backEnd.master')
@section('css')
    <style>
        .centerTd td,
        .centerTh th {
            text-align: center;
        }

        .plus-btn,
        .plus-btn:hover, 
        .minus-btn, 
        .minus-btn:hover {
            background: none;
            border: none;
            cursor: pointer;
        }

        .plus-btn:focus
        .minus-btn:focus {
            outline: none;
        }
        
        .minus-btn {
            font-size: 16px;
        }

    </style>
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>SENDING SUMMERCAMP MAIL</h1>
                <div class="bc-pages">
                    <a href="{{ url('dashboard') }}">@lang('lang.dashboard')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                SENDING MAIL
                            </h3>
                        </div>
                        <form action="/send-email" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-7">
                                    <div class="white-box">
                                        <div class="add-visitor">
                                            <div class="row p-2">
                                                <div class="col-12">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control" type="text" name="title"
                                                            autocomplete="off" required>
                                                        <label>Title *<span></span> </label>
                                                        <span class="focus-border textarea"></span>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-12 mt-30">
                                                    <input type="checkbox" id="summercamp_template"
                                                        class="common-checkbox" value="yes"
                                                        name="summercamp_template">
                                                    <label
                                                        for="summercamp_template">Use summercamp template ?</label>
                                                </div>


                                                {{-- <div class="col-12">
                                                    <input class="primary-input form-control" type="file" id="file_attach"
                                                        name="file">
                                                </div> --}}

                                                <div class="col-12 mt-30" id="summernote-wrapper">
                                                    <textarea name="body" id="summernote"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-5">
                                    <div class="row student-details mt_0_sm">
                                        <div class="col-lg-12">
                                            <ul class="nav nav-tabs mt_0_sm mb-20 justify-content-center" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#group_email_sms" selectTab="G"
                                                        role="tab" data-toggle="tab">@lang('lang.group')</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" selectTab="I" href="#indivitual_email_sms"
                                                        role="tab" data-toggle="tab">@lang('lang.individual')</a>
                                                </li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <input type="hidden" name="selectTab" id="selectTab">
                                                <div role="tabpanel" class="tab-pane fade show active" id="group_email_sms">
                                                    <div class="white-box">
                                                        <div class="col-lg-12">
                                                            <label>@lang('lang.message') @lang('lang.to') </label>
                                                            <br>
                                                            @foreach ($roles as $role)
                                                                <div>
                                                                    <input type="checkbox" id="role_{{ $role->id }}"
                                                                        class="common-checkbox" value="{{ $role->id }}"
                                                                        name="roles[]">
                                                                    <label
                                                                        for="role_{{ $role->id }}">{{ $role->name }}</label>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>

                                                <div role="tabpanel" class="tab-pane fade" id="indivitual_email_sms">
                                                    <div class="white-box">
                                                        <div class="row mb-35">
                                                            <div class="col-12" id="wrapper">
                                                                <div>
                                                                    <label>@lang('lang.message') @lang('lang.to') </label>
                                                                    <button class="plus-btn" type="button"><i
                                                                            class="fa fa-plus-circle"></i></button>
                                                                </div>
                                                                <br>
                                                                <div class="input-effect mb-4">
                                                                    <input class="primary-input form-control has-content" type="email"
                                                                        name="individual_emails[]" autocomplete="off" value="&nbsp;">
                                                                    <label>
                                                                        Email<span></span> 
                                                                        <button class="minus-btn" type="button">
                                                                            <i class="fa fa-minus-circle"></i>
                                                                        </button>
                                                                    </label>
                                                                    <span class="focus-border textarea"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="white-box mt-30">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span> @lang('lang.send')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['codeview']],
                ],
                callbacks: {
                    onImageUpload: function(files, editor, welEditable) {
                        sendFile(files[0], editor, welEditable);
                    }
                },
                popover: {
                    image: [
                        ['custom', ['imageAttributes']],
                        // ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                        // ['float', ['floatLeft', 'floatRight', 'floatNone']],
                        ['remove', ['removeMedia']]
                    ],
                },
                lang: 'en-US', // Change to your chosen language
                imageAttributes: {
                    icon: '<i class="note-icon-pencil"/>',
                    removeEmpty: false, // true = remove attributes | false = leave empty if present
                    disableUpload: false // true = don't display Upload Options | Display Upload Options
                }
            });

            function sendFile(file, editor, welEditable) {
                data = new FormData();
                data.append("file", file);
                $.ajax({
                    data: data,
                    type: "POST",
                    url: '/summernote/imgs_upload',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        var image = $('<img>').attr('src', 'http://sgstar.edu.vn/' + data.url);
                        $('#summernote').summernote("insertNode", image[0]);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
            $("input[name=files]").prop("multiple", false);

            $(".plus-btn").on("click", function(e) {
                e.preventDefault();
                $("#wrapper").append(
                    '<div class="input-effect mb-4">' +
                    '<input class="primary-input form-control has-content" type="email"' +
                    'name="individual_emails[]" autocomplete="off">' +
                    '<label>Email<span></span> <button class="minus-btn" type="button">' +
                    '<i class="fa fa-minus-circle"></i>' +
                    '</button></label>' +
                    '<span class="focus-border textarea"></span>' +
                    '</div>'
                );
            })
            
            $("#summercamp_template").on("click", function(e) {
                if ($(this).is(":checked")) {
                    $('#summernote-wrapper').css("display", "none");
                } else {
                    $('#summernote-wrapper').css("display", "block");
                }
            })
            
           $(document).on("click", ".minus-btn", function(e) {
                e.preventDefault();
                $(this).parents(".input-effect").remove();
            })
        });

    </script>
@endsection
