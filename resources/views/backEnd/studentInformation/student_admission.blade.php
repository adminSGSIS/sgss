@extends('backEnd.master')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/')}}/css/croppie.css">
<style>
    .infix_session {
        display: none;
    }
</style>
@endsection
@section('mainContent')
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('lang.student_admission')</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.student_information')</a>
                <a href="#">@lang('lang.student_admission')</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="main-title xs_mt_0 mt_0_sm">
                    <h3 class="mb-0">@lang('lang.add') @lang('lang.student')</h3>
                </div>
            </div>
            @if(in_array(63, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
            <div class="offset-lg-3 col-lg-3 text-right mb-20 col-sm-6">
                <a href="{{route('import_student')}}" class="primary-btn small fix-gr-bg">
                    <span class="ti-plus pr-2"></span>
                    @lang('lang.import') @lang('lang.student')
                </a>
            </div>
            @endif
        </div>
        @if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
        {{ Form::open(['class' => 'form-horizontal studentadmission', 'files' => true, 'route' => 'student_store', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form']) }}
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <div>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                @if($errors->any())
                                    @foreach ($errors->all() as $error)
                                        @if($error == "The email address has already been taken.")
                                            <div class="error text-danger ">
                                                {{ 'The email address has already been taken, You can find out in student list or disabled student list' }}
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                                @if ($errors->any())
                                <div class="error text-danger ">{{ 'Something went wrong, please try again' }}</div>
                                @endif
                            </div>
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">@lang('lang.personal') @lang('lang.info')</h4>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="url" value="{{ URL::to('/') }}">
                        <div class="row mb-40 mt-30">
                            <div class="col-lg-3">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('academic_id') ? ' is-invalid' : '' }}" 
                                        name="academic_id" id="academic_year">
                                        <option data-display="@lang('lang.academic_year') *" value="">@lang('lang.academic_year') *</option>
                                        @foreach($sessions as $session)
                                            <option value="{{$session->id}}" {{old('academic_id') == $session->id? 'selected': ''}}>
                                                {{$session->year}}[{{$session->title}}]
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('academic_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('session') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            @if(old('academic_id'))
                                @php
                                    $classes = App\SmClass::where('academic_id', old('academic_id'))->get();
                                @endphp
                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20" id="class-div">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('class_id') ? ' is-invalid' : '' }}"
                                            name="class_id" id="classSelectStudent">
                                            <option data-display="@lang('lang.class') *" value="">@lang('lang.class') *</option>
                                            @foreach ($classes as $item)
                                            <option data-display="{{ $item ->class_name }}" 
                                                value="{{ $item ->id }}" @if(old('class_id') == $item->id) selected @endif>
                                                {{ $item ->class_name }}
                                            </option>
                                            @endforeach
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('class_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('class_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            @else
                                <div class="col-lg-3">
                                    <div class="input-effect sm2_mb_20 md_mb_20" id="class-div">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('class_id') ? ' is-invalid' : '' }}"
                                            name="class_id" id="classSelectStudent">
                                            <option data-display="@lang('lang.class') *" value="">@lang('lang.class') *</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('class_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('class_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            @if(old('class_id'))
                                @php
                                $sections = DB::table('sm_class_sections')->where('class_id', '=', old('class_id'))
                                ->join('sm_sections','sm_class_sections.section_id','=','sm_sections.id')
                                ->get();
                                @endphp
                                <div class="col-lg-2">
                                    <div class="input-effect sm2_mb_20 md_mb_20" id="sectionStudentDiv">
                                        <select class="niceSelect w-100 bb form-control {{ $errors->has('section_id') ? ' is-invalid' : '' }}" 
                                            name="section_id" id="sectionSelectStudent">
                                            <option data-display="@lang('lang.section') *" value="">@lang('lang.section') *</option>
                                            @foreach ($sections as $section)
                                            <option value="{{ $section->id }}" @if(old('section_id') == $section->id) selected @endif>
                                                {{ $section->section_name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('section_id'))
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong>{{ $errors->first('section_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            @else
                                <div class="col-lg-2">
                                    <div class="input-effect sm2_mb_20 md_mb_20" id="sectionStudentDiv">
                                        <select class="niceSelect w-100 bb form-control{{ $errors->has('section_id') ? ' is-invalid' : '' }}" 
                                            name="section_id" id="sectionSelectStudent">
                                            <option data-display="@lang('lang.section') *" value="">@lang('lang.section') *</option>
                                        </select>
                                        <span class="focus-border"></span>
                                        @if ($errors->has('section_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('section_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input form-control" 
                                    type="text" name="admission_no" readonly value="{{ $admission_no }}">
                                    <label>@lang('lang.admission') @lang('lang.number')</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input read-only-input" type="number" id="roll_no" 
                                    name="roll_no" value="{{ $roll_id }}" readonly>
                                    <label>@lang('lang.roll') @lang('lang.number')</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" name="first_name"
                                     value="{{old('first_name')}}" required>
                                    <label>@lang('lang.first') @lang('lang.name') <span>*</span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" name="middle_name"
                                     value="{{old('middle_name')}}">
                                    <label>middle name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" name="last_name"
                                    value="{{old('last_name')}}" required>
                                    <label>@lang('lang.last') @lang('lang.name') *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input" type="text" id="placeholderPhoto" 
                                            placeholder="@lang('lang.student_photo')" readonly="">
                                            <span class="focus-border"></span>
                                            @if ($errors->has('student_photo'))
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong>{{ @$errors->first('student_photo') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="photo">@lang('lang.browse')</label>
                                            <input type="file" class="d-none" value="{{ old('student_photo') }}" name="student_photo" id="photo" accept="image/*">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" required
                                    name="nickname" value="{{old('nickname')}}">
                                    <label>Student’s preferred name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text"
                                             name="date_of_birth" value="{{old('date_of_birth')}}">
                                            <label>@lang('lang.date_of_birth')</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control" name="gender_id">
                                        <option data-display="@lang('lang.gender')" value="">@lang('lang.gender')</option>
                                        @foreach ($genders as $item)
                                        <option value="{{ $item->id }}" {{ old('gender_id') == $item->id ? 'selected': '' }}>{{ $item->base_setup_name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" required
                                    name="nationality_1" value="{{old('nationality_1')}}">
                                    <label>First Nationality *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                    name="nationality_2" value="{{old('nationality_2')}}">
                                    <label>Second Nationality</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                     name="enroll_in" value="{{old('enroll_in')}}">
                                    <label>enroll in</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">student interests</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12 mb-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                     name="particular_interests" value="{{old('particular_interests')}}">
                                    <label>Student's particular interests</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                     name="talents_or_abilities" value="{{old('talents_or_abilities')}}">
                                    <label>talents or abilities </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">language</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" required
                                    name="first_language" value="{{old('first_language')}}">
                                    <label>Child's first language *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mb-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="second_language" value="{{old('second_language')}}">
                                    <label>Child's second language</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="other_language" value="{{old('other_language')}}">
                                    <label>Other spoken language</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            
                            <div class="col-6 mt-4">
                                <fieldset>
                                    <p>Other language level</p>
                                    <div>
                                        <input type="radio" id="other_language_proficient_1" class="common-radio relationButton"
                                        name="other_language_proficient" value="{{ $proficient[0] }}" @if(old('other_language_proficient') == $proficient[0]) checked @endif>
                                        <label for="other_language_proficient_1">{{ $proficient[0] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="other_language_proficient_2" class="common-radio relationButton" name="other_language_proficient" 
                                        value="{{ $proficient[1] }}" @if(old('other_language_proficient') == $proficient[1]) checked @endif>
                                        <label for="other_language_proficient_2">{{ $proficient[1] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="other_language_proficient_3" class="common-radio relationButton" name="other_language_proficient" 
                                        value="{{ $proficient[2] }}" @if(old('other_language_proficient') == $proficient[2]) checked @endif>
                                        <label for="other_language_proficient_3">{{ $proficient[2] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="other_language_proficient_4" class="common-radio relationButton" name="other_language_proficient" 
                                        value="{{ $proficient[3] }}" @if(old('other_language_proficient') == $proficient[3]) checked @endif>
                                        <label for="other_language_proficient_4">{{ $proficient[3] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="other_language_proficient_5" class="common-radio relationButton" name="other_language_proficient" 
                                        value="{{ $proficient[4] }}" @if(old('other_language_proficient') == $proficient[4]) checked @endif>
                                        <label for="other_language_proficient_5">{{ $proficient[4] }}</label>
                                        <br>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;">{{ $errors->first('other_language_proficient') }}</small>
                            </div>

                            <div class="col-6 mt-4">
                                <fieldset>
                                    <p>English level</p>
                                    <div>
                                        <input type="radio" id="english_proficient_1" class="common-radio relationButton" name="english_proficient"
                                        value="{{ $proficient[0] }}" @if(old('english_proficient') == $proficient[0]) checked @endif>
                                        <label for="english_proficient_1">{{ $proficient[0] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="english_proficient_2" class="common-radio relationButton" name="english_proficient" 
                                        value="{{ $proficient[1] }}" @if(old('english_proficient') == $proficient[1]) checked @endif>
                                        <label for="english_proficient_2">{{ $proficient[1] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="english_proficient_3" class="common-radio relationButton" name="english_proficient" 
                                        value="{{ $proficient[2] }}" @if(old('english_proficient') == $proficient[2]) checked @endif>
                                        <label for="english_proficient_3">{{ $proficient[2] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="english_proficient_4" class="common-radio relationButton" name="english_proficient" 
                                        value="{{ $proficient[3] }}" @if(old('english_proficient') == $proficient[3]) checked @endif>
                                        <label for="english_proficient_4">{{ $proficient[3] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="english_proficient_5" class="common-radio relationButton" name="english_proficient" 
                                        value="{{ $proficient[4] }}" @if(old('english_proficient') == $proficient[4]) checked @endif>
                                        <label for="english_proficient_5">{{ $proficient[4] }}</label>
                                        <br>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;">{{ $errors->first('english_proficient') }}</small>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">trial date</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-3">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text" 
                                            name="trial_from_date" value="{{old('trial_from_date')}}">
                                            <label>From</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text"  
                                            name="trial_to_date" value="{{old('trial_to_date')}}">
                                            <label>To</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control{{ $errors->has('trial_class_id') ? ' is-invalid' : '' }}"
                                         name="trial_class_id">
                                        <option data-display="@lang('lang.class')" value="">@lang('lang.class')</option>
                                        @foreach ($classes as $item)
                                            <option data-display="{{ $item->class_name }}" 
                                                value="{{ $item->id }}" @if($item->id == old('trial_class_id')) selected @endif>
                                                {{ $item->class_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="focus-border"></span>
                                    @if ($errors->has('trial_class_id'))
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong>{{ $errors->first('trial_class_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="trial_teacher" value="{{old('trial_teacher')}}">
                                    <label>Teacher</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">school history</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="school_history_name" value="{{old('school_history_name')}}">
                                    <label>Current school name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="school_history_city" value="{{old('school_history_city')}}">
                                    <label>City, country</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text"  
                                            name="school_history_from_date" value="{{old('school_history_from_date')}}">
                                            <label>Date from</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text" 
                                            name="school_history_to_date" value="{{old('school_history_to_date')}}">
                                            <label>Date to</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                    name="school_history_language_instruction" value="{{old('school_history_language_instruction')}}">
                                    <label>Language of instruction</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                    name="school_history_withdrawal_reason" value="{{old('school_history_withdrawal_reason')}}">
                                    <label>Reason for withdrawal</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">health information</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12 mb-40">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="health_information_1" value="{{old('health_information_1')}}">
                                    <label>Allergies to medications and/or foods</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mb-40">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="health_information_2" value="{{old('health_information_2')}}">
                                    <label>Medications taken on a regular basis</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mb-40">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="health_information_3" value="{{old('health_information_3')}}">
                                    <label>If your child has any other medical condition which may affect his/her daily routine, please describe it here</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mb-2">
                                <fieldset>
                                    <div>
                                        <input type="checkbox" id="health_information_4" class="common-checkbox relationButton" 
                                        name="health_information_4" value="yes" @if(old('health_information_4') == 'yes') checked @endif>
                                        <label for="health_information_4">
                                            We consent to the school nurse administering over-the-counter 
                                            medications for symptom relief of minor illnesses
                                        </label>
                                        <br>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-12">
                                <fieldset>
                                    <div>
                                        <input type="checkbox" id="health_information_5" class="common-checkbox relationButton" 
                                        name="health_information_5" value="yes" @if(old('health_information_5') == 'yes') checked @endif>
                                        <label for="health_information_5">
                                            We agree to provide any professional reports relevant to my child's education 
                                            history 
                                        </label>
                                        <br>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">additional needs</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control mt-3" name="additional_needs"
                                    type="text">{{old('additional_needs')}}</textarea>
                                    <label>
                                        PLease describe any other Special educational need that 
                                        Saigon Star should be aware of in order to make your child's learning is positive experience
                                    </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">School bus service requested</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            @foreach ($bus_service as $item)
                            <div class="col-12 mb-2">
                                <fieldset>
                                    <div>
                                        <input type="radio" id="bus_service_request_{{ $item->id }}" class="common-radio relationButton" 
                                        name="bus_service_request" value="{{ $item->id }}" 
                                        @if(old('bus_service_request') == $item->id) checked @endif>
                                        <label for="bus_service_request_{{ $item->id }}">
                                            {{ $item->title }}
                                        </label>
                                        <br>
                                    </div>
                                </fieldset>
                            </div>
                            @endforeach
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">lunch requested</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control mt-3" name="lunch_request"
                                    type="text" value="{{old('lunch_request')}}">
                                    <label>
                                        Any special request for lunch ?
                                    </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">support</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="eal_support"
                                    type="text" value="{{old('eal_support')}}">
                                    <label>EAL support (hours/week)</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="sen_support"
                                    type="text" value="{{old('sen_support')}}">
                                    <label>SEN support (hours/week)</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">payment by</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_name" required
                                    type="text" value="{{old('payer_name')}}">
                                    <label>Contact Name *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_company"
                                    type="text" value="{{old('payer_company')}}">
                                    <label>Company Name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_address" required
                                    type="text" value="{{old('payer_address')}}">
                                    <label>Address *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_email" required
                                    type="email" value="{{old('payer_email')}}">
                                    <label>Email *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_mobile" required
                                    type="text" value="{{old('payer_mobile')}}">
                                    <label>Phone number *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6 mt-4">
                                <fieldset>
                                    <p>Find or add new parents</p>
                                    <div>
                                        <input type="radio" id="parent_registered_yes" class="common-radio relationButton" name="check_parent"
                                        value="find">
                                        <label for="parent_registered_yes">Find</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="parent_registered_no" class="common-radio relationButton" name="check_parent"
                                        value="new" checked>
                                        <label for="parent_registered_no">New</label>
                                        <br>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;">{{ $errors->first('fathers_language_level') }}</small>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">family information</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30" style="display: none" id="choose_parent">
                            <div class="col-lg-12 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control" name="choose_parent_id">
                                        <option data-display="Choose parent" value="">Choose parent</option>
                                        @foreach ($parents as $item)
                                            <option value={{ $item->id }}>{{ $item->full_name }} / {{ $item->email }}</option>
                                        @endforeach
                                    </select>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30" id="father_info">
                            <div class="col-6">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text"
                                    name="fathers_name" value="{{old('fathers_name')}}">
                                    <label>@lang('lang.father') @lang('lang.name')<span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                    name="fathers_nationality" value="{{old('fathers_nationality')}}">
                                    <label>nationality</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" name="fathers_occupation" value="{{old('fathers_occupation')}}">
                                    <label>@lang('lang.occupation')</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" name="fathers_company" value="{{old('fathers_company')}}">
                                    <label>Company</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            
                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text"
                                    name="fathers_mobile" value="{{old('fathers_mobile')}}">
                                    <label>@lang('lang.father') @lang('lang.phone') <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                           
                            <div class="col-6 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="email" name="fathers_email"
                                     value="{{old('fathers_email')}}">
                                    <label>Father's email</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="fathers_work_address"
                                     value="{{old('fathers_work_address')}}">
                                    <label>Father's work address</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="fathers_first_language"
                                     value="{{old('fathers_first_language')}}">
                                    <label>Father's first language</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            
                            <div class="col-6 mt-4">
                                <fieldset>
                                    <p>English level</p>
                                    <div>
                                        <input type="radio" id="fathers_language_level_1" class="common-radio relationButton" name="fathers_language_level"
                                        value="{{ $proficient[0] }}" @if(old('fathers_language_level') == $proficient[0]) checked @endif>
                                        <label for="fathers_language_level_1">{{ $proficient[0] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="fathers_language_level_2" class="common-radio relationButton" name="fathers_language_level" 
                                        value="{{ $proficient[1] }}" @if(old('fathers_language_level') == $proficient[1]) checked @endif>
                                        <label for="fathers_language_level_2">{{ $proficient[1] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="fathers_language_level_3" class="common-radio relationButton" name="fathers_language_level" 
                                        value="{{ $proficient[2] }}" @if(old('fathers_language_level') == $proficient[2]) checked @endif>
                                        <label for="fathers_language_level_3">{{ $proficient[2] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="fathers_language_level_4" class="common-radio relationButton" name="fathers_language_level" 
                                        value="{{ $proficient[3] }}" @if(old('fathers_language_level') == $proficient[3]) checked @endif>
                                        <label for="fathers_language_level_4">{{ $proficient[3] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="fathers_language_level_5" class="common-radio relationButton" name="fathers_language_level" 
                                        value="{{ $proficient[4] }}" @if(old('fathers_language_level') == $proficient[4]) checked @endif>
                                        <label for="fathers_language_level_5">{{ $proficient[4] }}</label>
                                        <br>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;">{{ $errors->first('fathers_language_level') }}</small>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30" id="mother_info">
                            <div class="col-6">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" name="mothers_name"
                                     value="{{old('mothers_name')}}">
                                    <label>@lang('lang.mother') @lang('lang.name') <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" 
                                    type="text" name="mothers_nationality" value="{{old('mothers_nationality')}}">
                                    <label>nationality</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" name="mothers_occupation" value="{{old('mothers_occupation')}}">
                                    <label>@lang('lang.occupation')</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" name="mothers_company" value="{{old('mothers_company')}}">
                                    <label>Company</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            
                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" 
                                    type="text" name="mothers_mobile" value="{{old('mothers_mobile')}}">
                                    <label>@lang('lang.mother') @lang('lang.phone') <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                           
                            <div class="col-6 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="email" name="mothers_email"
                                     value="{{old('mothers_email')}}">
                                    <label>Mother's email</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="mothers_work_address"
                                     value="{{old('mothers_work_address')}}">
                                    <label>Mother's work address</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="mothers_first_language"
                                     value="{{old('mothers_first_language')}}">
                                    <label>Mother's first language</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <fieldset>
                                    <p>English level</p>
                                    <div>
                                        <input type="radio" id="mothers_language_level_1" class="common-radio relationButton" name="mothers_language_level"
                                        value="{{ $proficient[0] }}" @if(old('mothers_language_level') == $proficient[0]) checked @endif>
                                        <label for="mothers_language_level_1">{{ $proficient[0] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="mothers_language_level_2" class="common-radio relationButton" name="mothers_language_level" 
                                        value="{{ $proficient[1] }}" @if(old('mothers_language_level') == $proficient[1]) checked @endif>
                                        <label for="mothers_language_level_2">{{ $proficient[1] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="mothers_language_level_3" class="common-radio relationButton" name="mothers_language_level" 
                                        value="{{ $proficient[2] }}" @if(old('mothers_language_level') == $proficient[2]) checked @endif>
                                        <label for="mothers_language_level_3">{{ $proficient[2] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="mothers_language_level_4" class="common-radio relationButton" name="mothers_language_level" 
                                        value="{{ $proficient[3] }}" @if(old('mothers_language_level') == $proficient[3]) checked @endif>
                                        <label for="mothers_language_level_4">{{ $proficient[3] }}</label>
                                        <br>
                                    </div>
                                    <div>
                                        <input type="radio" id="mothers_language_level_5" class="common-radio relationButton" name="mothers_language_level" 
                                        value="{{ $proficient[4] }}" @if(old('mothers_language_level') == $proficient[4]) checked @endif>
                                        <label for="mothers_language_level_5">{{ $proficient[4] }}</label>
                                        <br>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;">{{ $errors->first('mothers_language_level') }}</small>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30">
                            <div class="col-6">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" required
                                    name="emergency_contact_name" value="{{old('emergency_contact_name')}}">
                                    <label>emergency name *<span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                    name="emergency_contact_family_name" value="{{old('emergency_contact_family_name')}}">
                                    <label>emergency family name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-12 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text"
                                    name="emergency_relationship" value="{{old('emergency_relationship')}}">
                                    <label>Relationship to Child</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" required
                                    name="emergency_contact_mobile_1" value="{{old('emergency_contact_mobile_1')}}">
                                    <label>Emergency phone number 1 *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" required
                                    name="emergency_contact_mobile_2" value="{{old('emergency_contact_mobile_2')}}">
                                    <label>Emergency phone number 2</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="email" required
                                    name="emergency_email" value="{{old('emergency_email')}}">
                                    <label>Emergency email *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" 
                                    name="emergency_contact_address" value="{{old('emergency_contact_address')}}">
                                    <label>Emergency address</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="home_adress_in_hcm"
                                    value="{{old('home_adress_in_hcm')}}">
                                    <label>Home address in Ho Chi Minh City</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @php
                    $tooltip = "";
                    if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                        $tooltip = "";
                    }else{
                        $tooltip = "You have no permission to add";
                    }
                @endphp


                <div class="row mt-40">
                    <div class="col-lg-12 text-center">
                        <button class="primary-btn fix-gr-bg" id="_submit_btn_admission" data-toggle="tooltip" title="{{$tooltip}}">
                            <span class="ti-check"></span>
                            @lang('lang.save') @lang('lang.student')
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection

@section('script')
<script>
    $("#parent_registered_yes").on("click", function(){
        $("#mother_info").css("display", "none");
        $("#father_info").css("display", "none");
        $("#choose_parent").css("display", "flex");
    });

    $("#parent_registered_no").on("click", function(){
        $("#mother_info").css("display", "none");
        $("#father_info").css("display", "flex");
        $("#choose_parent").css("display", "none");
    });
</script>
<script src="{{asset('public/backEnd/')}}/js/croppie.js"></script>
<script src="{{asset('public/backEnd/')}}/js/st_addmision.js"></script>
@endsection