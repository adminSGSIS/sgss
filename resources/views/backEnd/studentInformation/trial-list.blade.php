@extends('backEnd.master')
@section('css')
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
<link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/css')}}/purchase-proposal.css">
@endsection
@section('mainContent')
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Trial List</h1>
                <div class="bc-pages">
                    <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>Student name</th>
                                    <th>Class</th>
                                    <th>Date of birth</th>
                                    <th>Gender</th>
                                    <th>Is Trial</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($students as $value)
                                    <tr class="centerTd">
                                        <td>{{ $value->full_name }}</td>
                                        <td>{{ $value->admission->class->class_name ?? '' }}</td>
                                        <td>{{ $value->date_of_birth }}</td>
                                        <td>{{ $value->gender->base_setup_name }}</td>
                                        <td>
                                            <div class="cbx">
                                                <input @if($value->is_trial == 1) checked @endif value="{{$value->id}}" id="cbx" class="isTrialStatus" type="checkbox"/>
                                                <label for="cbx"></label>
                                                <svg width="15" height="14" viewbox="0 0 15 14" fill="none">
                                                <path d="M2 8.36364L6.23077 12L13 2"></path>
                                                </svg>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    @lang('lang.select')
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="/feedback/{{ $value->id }}">view</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(".isTrialStatus").click(function(){
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "/is-trial-status/" + $(this).val(),
                success: function (data) {
                    console.log(data);
                }
            });
        });
    </script>
@endsection