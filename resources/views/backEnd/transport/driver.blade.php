@extends('backEnd.master')
@section('mainContent')
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>@lang('')</h1>
            <div class="bc-pages">
                <a href="{{url('dashboard')}}">@lang('lang.dashboard')</a>
                <a href="#">@lang('lang.inventory')</a>
                <a href="#">@lang('lang.item_list')</a>
            </div>
        </div>
    </div>
</section>
@if(isset($editData))
{{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'files-update', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
@else
@if(in_array(321, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )
{{ Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'create-drivers',
'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
@endif
@endif
<div id="driver_info">
	<h2>Base Information</h2>
	<div class="row">
		<div class="col-lg-4 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('drivers_no') ? ' is-invalid' : '' }}" type="text"  name="drivers_no" 
                value="@if(isset($editData)) {{$editData->drivers_no}} @else sm_driver_no_{{$next_id}}  @endif" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.drivers_no') <span>*</span> </label>
                        @if ($errors->has('drivers_no'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('drivers_no') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		
		
	</div>

	<div class="row">
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" type="text"  name="first_name" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.first_name') <span>*</span> </label>
                        @if ($errors->has('first_name'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('first_name') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" type="text"  name="last_name" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.last_name') <span>*</span> </label>
                        @if ($errors->has('last_name'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('last_name') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>

		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('full_name') ? ' is-invalid' : '' }}" type="text"  name="full_name" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.full_name') <span>*</span> </label>
                        @if ($errors->has('full_name'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('full_name') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-3 mt-30">
			<select class="niceSelect w-100 bb form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender">
                <option data-display="@lang('lang.gender') *" value="">@lang('lang.select_gender') *</option>
                <option data-display="@lang('lang.gender') *" value="0">@lang('lang.male') </option>
                <option data-display="@lang('lang.gender') *" value="1">@lang('lang.female') </option>
                <option data-display="@lang('lang.gender') *" value="2">@lang('lang.other') </option>
                                                  
            </select>
		</div>
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('mother_name') ? ' is-invalid' : '' }}" type="text"  name="mothers_name" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('mothers name') <span>*</span> </label>
                        @if ($errors->has('mothers_name'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('mothers_name') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('fathers_name') ? ' is-invalid' : '' }}" type="text"  name="fathers_name" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('fathers name') <span>*</span> </label>
                        @if ($errors->has('fathers_name'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('fathers_name') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-3 mt-30">
            <div class="input-effect">
                <input
					class="primary-input date form-control{{ $errors->has('date_of_joining') ? ' is-invalid' : '' }}"
					id="startDate" type="text" placeholder="@lang('lang.date_of_joining') *"
					name="date_of_joining"
					value="">
					<span class="focus-border"></span>
					@if ($errors->has('date'))
					<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('date_of_joining') }}</strong>
                                            </span>
					@endif
				</div>
		</div>
		<div class="col-lg-3 mt-30">
            <div class="input-effect">
                <input
					class="primary-input date form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}"
					id="startDate" type="text" placeholder="@lang('lang.date_of_birth') *"
					name="date_of_birth"
					value="">
					<span class="focus-border"></span>
					@if ($errors->has('date'))
					<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('date_of_birth') }}</strong>
                                            </span>
					@endif
			</div>
		</div>
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="text"  name="email" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.email') <span>*</span> </label>
                        @if ($errors->has('email'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('email') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" type="text"  name="mobile" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.mobile') <span>*</span> </label>
                        @if ($errors->has('mobile'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('mobile') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('emergency_mobile') ? ' is-invalid' : '' }}" type="text"  name="emergence_mobile" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('emergence mobile') <span>*</span> </label>
                        @if ($errors->has('emergence_mobile'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('emergence_mobile') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-3 mt-30">
			<select class="niceSelect w-100 bb form-control{{ $errors->has('martial_status') ? ' is-invalid' : '' }}" name="marital_status">
                <option data-display="@lang('marital status') *" value="">@lang('marital status') *</option>
                <option data-display="@lang('lang.unmarried') *" value="0">@lang('lang.unmarried') </option>
                <option data-display="@lang('lang.married') *" value="1">@lang('lang.married') </option>                                                  
            </select>
		</div>
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('driving_license') ? ' is-invalid' : '' }}" type="text"  name="driving_license" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('driving license') <span>*</span> </label>
                        @if ($errors->has('driving_license'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('driving_license') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>

		<div class="col-lg-3 mt-30">
			<div class="row no-gutters input-right-icon">
                    <div class="col">
                         <div class="input-effect sm2_mb_20 md_mb_20">
                         <input class="primary-input" type="text" id="placeholderPhoto" placeholder="@lang('drivers photo')">
			
			<span class="focus-border"></span>

				@if ($errors->has('drivers_photo'))
				<span class="invalid-feedback d-block" role="alert">
				<strong>{{ @$errors->first('drivers_photo') }}</strong>
				</span>
				@endif
						</div>
				</div>
				<div class="col-auto">
					<button class="primary-btn-small-input" type="button">
						<label class="primary-btn small fix-gr-bg" for="photo">@lang('lang.browse')</label>
						<input type="file" class="d-none" value="{{ old('drivers_photo') }}" name="photo" id="photo">
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" type="text"  name="address" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.address') <span>*</span> </label>
                        @if ($errors->has('address'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('address') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-6 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('emergence_address') ? ' is-invalid' : '' }}" type="text"  name="emergence_address" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('emergence address') <span>*</span> </label>
                        @if ($errors->has('emergence_address'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('emergence_address') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-6 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('qualification') ? ' is-invalid' : '' }}" type="text"  name="qualification" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('qualification') <span>*</span> </label>
                        @if ($errors->has('qualification'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('qualification') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-6 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('experience') ? ' is-invalid' : '' }}" type="text"  name="experience" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.experience') <span>*</span> </label>
                        @if ($errors->has('experience'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('experience') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
	</div>

	<br>
	<h2>Payroll Details</h2>
	<div class="row">
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('ept_no') ? ' is-invalid' : '' }}" type="text"  name="ept_no" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.ept_no') <span>*</span> </label>
                        @if ($errors->has('ept_no'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('ept_no') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('base_salary') ? ' is-invalid' : '' }}" type="text"  name="base_salary" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.base_salary') <span>*</span> </label>
                        @if ($errors->has('base_salary'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('base_salary') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>

		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('contact_type') ? ' is-invalid' : '' }}" type="text"  name="contact_type" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.contact_type') <span>*</span> </label>
                        @if ($errors->has('contact_type'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('contact_type') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>

		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" type="text"  name="location" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.location') <span>*</span> </label>
                        @if ($errors->has('location'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('location') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
	</div>
	<br>
	<h2>Bank info Details</h2>
	<div class="row">
		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('bank_account_name') ? ' is-invalid' : '' }}" type="text"  name="bank_account_name" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.bank_account_name') <span>*</span> </label>
                        @if ($errors->has('bank_account_name'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('bank_account_name') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>

		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('bank_account_number') ? ' is-invalid' : '' }}" type="text"  name="bank_account_number" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.bank_account_number') <span>*</span> </label>
                        @if ($errors->has('bank_account_number'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('bank_account_number') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>

		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('bank_name') ? ' is-invalid' : '' }}" type="text"  name="bank_name" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.bank_name') <span>*</span> </label>
                        @if ($errors->has('bank_name'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('bank_name') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>

		<div class="col-lg-3 mt-30">		
          <div class="input-effect">
                <input class="primary-input form-control{{ $errors->has('branch_name') ? ' is-invalid' : '' }}" type="text"  name="branch_name" value="" >
                    <span class="focus-border"></span>
                     <label>@lang('lang.branch_name') <span>*</span> </label>
                        @if ($errors->has('branch_name'))
	                        <span class="invalid-feedback" role="alert">
	                            <strong>{{ $errors->first('branch_name') }}</strong>
	                        </span>
                         @endif
          </div>                       
		</div>
	</div>
</div>
<div class="row mt-40">
<div class="col-lg-12 text-center">
<button class="primary-btn fix-gr-bg" data-toggle="tooltip" >
<span class="ti-check"></span>
@if(isset($editData))
@lang('lang.update')
@else
@lang('lang.save')
@endif
</button>
</div>
</div>
</div>
</div>
{{ Form::close() }}
@endsection


