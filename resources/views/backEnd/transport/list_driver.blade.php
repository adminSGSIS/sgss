@extends('backEnd.master')
@section('mainContent')
<div class="container-fluid p-0">
		<div class="row">
                <div class="col-lg-8 col-md-6">
                    <div class="main-title">
                        <h3 class="mb-30">@lang('lang.select_criteria')</h3>
                    </div>
                </div>
                <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg">
                    <a href="{{url('add-staff')}}" class="primary-btn small fix-gr-bg">
                        <span class="ti-plus pr-2"></span>
                        @lang('lang.add') @lang('lang.driver')
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                        <form action="driver-search" method="POST">
                        	@csrf
                            <div class="row">
                            	<div class="col-lg-4">
                                    <select class="w-100 bb niceSelect form-control" id="route" name="route">
                                        <option  data-display="Select route" value="">@lang('lang.select_route')</option>
                                        <option  data-display="Select route" value="">@lang('lang.select_route')</option>
	                                    @foreach($routes as $route)
	                                    <option value="{{$route->title}}"  data-display="{{$route->title}}" {{isset($route_id)? ($route->id == $route_id? 'selected' : ''):"" }}>{{$route->title}}</option>	
	                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                	<div class="input-effect">
						                <input id="vehicle" class="primary-input form-control{{ $errors->has('vehicle') ? ' is-invalid' : '' }}" type="text"  name="vehicle" value="@if(isset($vehicle_id)) {{$vehicle_id}} @endif" >
						                    <span class="focus-border"></span>
						                     <label>@lang('lang.vehicle') <span>*</span> </label>
						                        @if ($errors->has('vehicle'))
							                        <span class="invalid-feedback" role="alert">
							                            <strong>{{ $errors->first('vehicle') }}</strong>
							                        </span>
						                         @endif
						          	</div>              
                                </div>

                                <div class="col-lg-4 mt-30-md">
                                	<div class="input-effect">
						                <input id="drivername" class="primary-input form-control{{ $errors->has('drivername') ? ' is-invalid' : '' }}" type="text"  name="drivername" value="@if(isset($drivername)) {{$drivername}} @endif" >
						                    <span class="focus-border"></span>
						                     <label>@lang('Driver Name') <span>*</span> </label>
						                        @if ($errors->has('drivername'))
							                        <span class="invalid-feedback" role="alert">
							                            <strong>{{ $errors->first('drivername') }}</strong>
							                        </span>
						                         @endif
						          	</div>             


                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
<br><br><br>
<div class="row">
                    <div class="col-lg-12">

                        <table id="example" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                                @if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != "")
                                <tr>
                                    <td colspan="7">
                                        @if(session()->has('message-success-delete'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message-success-delete') }}
                                        </div>
                                        @elseif(session()->has('message-danger-delete'))
                                        <div class="alert alert-danger">
                                            {{ session()->get('message-danger-delete') }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                <tr>
                                    <th> @lang('lang.driver')  @lang('lang.no')</th>
                                    <th> @lang('lang.driver')  @lang('lang.name')</th>
                                    <th> @lang('lang.contract')  @lang('lang.type')</th>
                                   	<th> @lang('lang.route') </th>
                                   	<th> @lang('lang.vehicle')  @lang('lang.number')</th>                                   
                                    <th> @lang('lang.email')</th>
                                    <th> @lang('lang.mobile')</th>
                                    <th> @lang('lang.driver')  @lang('lang.license')</th>                                  
                                    <th> @lang('lang.action')</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @if(isset($listdrivers))
                            @foreach($listdrivers as $driver)
                                <tr>
                                    <td>{{@$driver->staff_no}}</td>
                                    <td>{{@$driver->full_name}}</td>
                                    <td>{{@$driver->contract_type}}</td>
                                    <td>{{@$driver->route_title}}</td>
                                    <td>{{@$driver->vehicle_no}}</td>
                                    <td>{{@$driver->email}}</td>
                                    <td>{{@$driver->mobile}}</td>
                                    <td>{{@$driver->driving_license}}</td>
                                    <td>
                                        <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            @lang('lang.select')
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="{{route('viewStaff', $driver->id)}}">@lang('lang.view')</a>
                                           @if(in_array(163, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )

                                            <a class="dropdown-item" href="{{route('editStaff', $driver->id)}}">@lang('lang.edit')</a>
                                           @endif
                                           @if(in_array(164, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 )

                                            @if ($driver->role_id != Auth::user()->role_id )
                                           
                                            {{-- <a class="dropdown-item modalLink" title="Delete Staff" data-modal-size="modal-md" href="{{route('deleteStaffView', $value->id)}}">@lang('lang.delete')</a> --}}
                                            
                                               
                                            @endif
                                            @endif
                                       
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                               
                            </tbody>
                            <tfoot>
                               
                            </tfoot>
                        </table>
                    </div>
                </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
   var table = $('#example').DataTable();
 
// #myInput is a <input type="text"> element
    $('#route').on( 'change', function () {
    table
        .columns( 3 )
        .search( this.value )
        .draw();
    } );
    $('#vehicle').on( 'keyup', function () {
    table
        .columns( 4 )
        .search( this.value )
        .draw();
    } );
    $('#drivername').on( 'keyup', function () {
    table
        .columns( 1 )
        .search( this.value )
        .draw();
    } );
</script>
@endsection