@extends('frontEnd.home.layout.front_master')
@section('main_content')

<div class="banner">
    	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>Working at Saigon Star </h3>
            </div>
    	</div>
    	
    </div>
<div class="c-color" >
<div class="container ">
        
	   <br>
        
        <br>

		<div class="col">
		   
            <div class="row">
            	@foreach($careers as $career)
                <div class="col-12 col-md-6 col-lg-4 item" >
                	<a href="view-career/{{$career->id}}" title="View Details">
                    <div class="card news-card" >
                    	<div class="ItemImage">
                        <img class="card-img-top news-card-img" src="{{url('')}}/{{$career->image}}" alt="Card image cap">
                        </div>
                        <div class="card-body news-card-body">
                            <h5 class="card-title news-card-title">{{Illuminate\Support\Str::limit($career->name,50)}}</h5>
                           
                        </div>
                    </div>
                    </a>
                </div>
                @endforeach
            </div>    
		</div>
</div>
<div class="container">
              <img class="banner" width="100%" src="{{asset('public/')}}/images/CHILDREN-01.png" alt=""> 
        </div>
</div>

@endsection