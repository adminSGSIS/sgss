@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
@endpush
@section('main_content')
<section class="contact "style="margin-bottom:-50px" >
  <div class="contact-image-background"  >
      <img src="{{ url('public') }}/images/09-min.png" alt=""> 
      <div class="booktour_mobile">
            <a href="#" data-toggle="modal" data-target="#booktournew" data-whatever="@mdo" id="booktour_mobile">
                <img src ="{{url('public/frontEnd')}}/assets/img/logo/booktour_icon.gif">
            </a>
      </div>
  </div>
  

  <form id="contact-form" class="contact-form-2" method="POST" action="{{url('/book-tour')}}" >
         @csrf
       <div class="booktour_row">
            <input type="text" id="parent" class="form-control" name="name" placeholder="*Parent's name "
                required>
            <input type="text" id="address" class="form-control" name="address" placeholder="*Address "
                required>
        </div>
        <div class="booktour_row">
            <input type="text" id="email" class="form-control" name="email" placeholder="*Email" required>
            <input type="text" id="telephone1" class="form-control" name="phone" placeholder="*Phone" required>
        </div>
        <div class="booktour_row fontcalender" >
            <input type="text" id="visitday" class="form-control" name="date"
                placeholder="dd/mm/yyyy" required autocomplete="off">
            <label for="visitday"><i class="fa fa-calendar fa-lg calendar" aria-hidden="true" ></i></label>
        </div>
        <div class="booktour_row">
            <textarea rows="4" cols="50" name="description" placeholder="*Message to school..."></textarea>
        </div>
       
      <button class="btn_submit">SUBMIT</button>
  </form>

    
</section>
    
   
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        var windowSize = $(window).width();
        
        if (windowSize <= 800) {
            
           $('#booktour_mobile').trigger('click');
           
        }
    });
</script>
@endsection