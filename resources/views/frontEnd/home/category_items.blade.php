@extends('frontEnd.home.layout.front_master')
@section('main_content')


<img width="100%" src="{{asset('public/')}}/images/banner-du-an.jpeg">
<div class="container mt-4">
        <h3 class="titleNews mb-3" style="text-align: center">{{$name_subcate->sub_category_name}}</h3>
        <div class="col">
            <div class="row" style="width: 90%; margin: auto">
                @foreach($items as $lastest)                 
                    <div class="col-lg-4 col-sm-6 col-12 item">
                        <a href="{{url('news-details/'.$lastest->id)}}" title="View Details">
                            <div style="height: 500px; "  class="card news-card" >
                                <div class="zoom img-hover-zoom">
                                <img class="card-img-top" src="{{asset($lastest->image)}}" height="auto" width="auto" alt="Card image cap">
                                </div>
                                <div class="card-body">
                                    <div class="card-title">
                                        <h5>{{App\SmGeneralSettings::DateConvater($lastest->publish_date)}}</h5>
                                        <h4>{{\Illuminate\Support\Str::limit($lastest->news_title,70)}}</h4>
                                    </div>      
                                    <br>
                                    <div class="card-description">
                                        @if (strlen($lastest->description) > 10)
                                            <p class="card-text">{{\Illuminate\Support\Str::limit($lastest->description,150)}}</p>
                                        @else
                                            <a href="{{url('news-details/'.$lastest->id)}}">Read More</a>
                                        @endif
                                    </div>    
                                </div>
                            </div>
                        </a>
                    </div>                                      
                @endforeach
            </div>
        </div>
</div>


@endsection