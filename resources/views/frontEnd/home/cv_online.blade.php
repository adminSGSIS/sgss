@extends('frontEnd.home.layout.front_master')
@section('main_content')
   <style>
        .logo_form{
            width: 22%;
            margin: 1%;
            margin-left: 5%;
        }
        .profile-pic{
            width: 200px;
            height: 200px;
            border: 3px solid lightblue;
            border-radius: 50%;
        }
        .card-body{
            display: flex;
        }
        .form-group{
            display: flex;
        }
        .contact h3{
            font-size: 18px;
            color: lightseagreen;
            font-style: normal;
        }
        .contact h4{
            font-size: 17px;
            color: lightseagreen;
            margin-left: 10%;
            max-width: 550px;
            font-style: normal;
        }
        .contact label{
            font-size: 18px;
            width: 200px;
            color: lightseagreen;
            font-style: normal;
        }
        .pt-100{
            padding-top: 100px;
        }
        .copy-right p{
            text-align: center;
            font-weight: 600;
            color: #0c4da2;
            font-size: 16px;
        }
        @media print
        {    
            button{
                display: none !important;
            }
            footer{
                display: none !important;
            }
            header{
                display: none !important;
            }
            .social_open{
                display: none !important;
            }
            .action_footer{
                display: none !important;
            }
            #botmanWidgetRoot{
                display: none !important;
            }
        }
   </style>
   <section>
        @foreach($data as $candidate)
        <div class="row d-flex justify-content-center">
            <div class="col-10">
                <div class="card"> 
                    <div class="logo_form">
                        <img src="{{ url('public') }}/images/logo.png" alt="logo" class="img-responsive">
                    </div>
                    <div class="card-body col-12">		
                        <div class="avatar col-4">
                            <img class="profile-pic"  src="{{$candidate->avatar}}">
                        </div>
                        <div class="contact col-8">
                            <div class="form-group">
                                <label>Full Name: </label>
                                <h3>{{$candidate->name}}</h3>
                            </div>
                            <div class="form-group">
                                <label>Gender: </label>
                                <h3>{{$candidate->gender}}</h3>
                            </div>
                            <div class="form-group">
                                <label>Day of Birth: </label>
                                <h3>{{$candidate->date_of_birth}}</h3>
                            </div>
                            <div class="form-group">
                                <label>Phone Number:</label>
                                <h3>{{$candidate->phone}}</h3>
                            </div>
                            <div class="form-group">
                                <label>Email:: </label>
                                <h3>{{$candidate->email}}</h3>
                            </div>
                        </div>
                   </div> 
                   <div class="contact col-12">
                        <div class="form-group">
                            <label>Address: </label>
                            <h4>{{$candidate->address}}</h4>
                        </div>
                        <div class="form-group">
                            <label>Graduated from school: </label>
                            <h4>{{$candidate->school}}</h4>
                        </div>
                        <div class="form-group">
                            <label>Graduation year: </label>
                            <h4>{{$candidate->graduation_year}}</h4>
                        </div>
                        <div class="form-group">
                            <label>About me: </label>
                            <h4>{{$candidate->about_me}}</h4>
                        </div>
                        <div class="form-group">
                            <label>Experience: </label>
                            <h4>{{$candidate->experience}}</h4>
                        </div>
                        <div class="form-group">
                            <label>Qualifications: </label>
                            <h4>{{$candidate->qualifications}}</h4>
                        </div>
                   </div>
                   <div class=" col-12 d-flex justify-content-start pt-100">
                        <button  onclick="window.print()" type="button" class="btn btn-info">Print CV</button>
                   </div>
                   <div class="copy-right">
                        <p>©2020 Saigon Star International School - Imprint</p>
                   </div>
                </div>
            </div>
        </div>
        @endforeach
   </section>
@endsection