<!--domestic-->
@extends('frontEnd.home.layout.front_master')
@section('main_content')
    <br><br><br><br>
    <section class="container mt-5">
        <div class="text-center">
            <h3>MEDICAL DECLARATION INFORMATION</h3>
            <h4>{{ $domestic->full_name }}</h4>
            <div style="color: green;" class="mb-2">Declared information was created at {{ $domestic->created_at->format('d-m-Y') }}</div>
            <p class="mb-0">Please take a screenshot for proof</p>
            <div>
                {{ SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)
                    ->generate(url('admin/medical-declaration/domestic/details') . '/'. $domestic->id) }}
            </div>
            <a href="/">
                <button class="btn btn-primary mt-3">GO HOME</button>
            </a>
        </div>
    </section>
@endsection