@extends('frontEnd.home.layout.front_master')
@section('main_content')
    @push('css')
        <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
        <link rel="stylesheet" href="{{ url('public') }}/frontEnd/css/newcss.css" />
    @endpush

    <div class="banner">
        <img class="banner" width="100%" src="{{ asset('public/') }}/images/banner-du-an1.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>BUS SCHEDULE</h3>
            </div>
        </div>
    </div>
    <div class="main_content">
        <div class="c-color pt-30 pb-30 ">
                <a target="_blank" class="d-flex justify-content-center" href="{{ url('public') }}/tailieu/03_SSIS_WEB_PARENT_Bus schedule.pdf" >
                    <div class="early-year col-md-8 col-12">
                        <img width="65%" src="{{ asset('public/') }}/images/Bus-schedule.png" id="EYTimetable">
                    </div>
                </a>         
        </div>
    </div>
@endsection
