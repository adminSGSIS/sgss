@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
	<link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
    <link rel="stylesheet" href="{{url('public')}}/frontEnd/css/newcss.css"/> 
@endpush

<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>Event Detail</h3>
        </div>
	</div>
</div>
<div class="main_content">
	<div class="background-content">
		<div class="container">
			<br>
			<h2 class="event-detail-title">{{$event->event_title}}</h2>
			@if($event->uplad_image_file != '')
			<img src="{{url('')}}/{{$event->uplad_image_file}}"><br><br>
			@endif	
			Start date : {{date('M j Y', strtotime($event->from_date))}}<br>
			End date   : {{date('M j Y', strtotime($event->to_date))}}<br>
			Target     : {{$event->for_whom}}<br>
			Location   : {{$event->event_location}}<br>
			Description: {{$event->event_des}}<br><br>
			From<br>
			Saigon Star Team~<br><br><br>
		</div>
		</div>
		
		
	</div>
	
</div>
@endsection