@extends('frontEnd.home.layout.front_master')
@section('main_content')
    @push('css')
        <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
        <link rel="stylesheet" href="{{ url('public') }}/frontEnd/css/newcss.css" />
        <link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/css')}}/lightbox.css">
    @endpush

    <div class="banner">
        <img class="banner" width="100%" src="{{ asset('public/') }}/images/banner-du-an1.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>quality cycle</h3>
            </div>
        </div>
    </div>
    <div class="main_content" style="background:#fafaf4">
        <div class="container d-flex justify-content-center pt-30">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                @foreach($images as $image)
                <div class="carousel-item @if ($loop->first) active @endif">
                  <a href="/public/uploads/category/{{$image->path}}" data-lightbox="2">
                        <img class="img-slider video_1aa w-100" src="/public/uploads/category/{{$image->path}}">
                  </a>
                </div>
                @endforeach
              </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick="event.preventDefault();">
                    <span class="fa fa-chevron-left"  aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" onclick="event.preventDefault();">
            <span class="fa fa-chevron-right"  aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
            
        </div>
        <div class="container">
              <img class="banner" width="100%" src="{{asset('public/')}}/images/CHILDREN-01.png" alt=""> 
        </div>
       
    </div>
@endsection

