<!DOCTYPE html>
<html>
<head>
	@include('frontEnd.home.layout.header')
</head>
<body>
	 @yield('main_content')
	
</body>
	@include('frontEnd.home.layout.footer')
	
</html>