<meta charset="utf-8" />
<!-- Mobile Meta -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1" />
<link rel="canonical" href="https://sgstar.edu.vn/" itemprop="url" />

<!-- Favicon -->
<link rel="shortcut icon" type="image/ico" href="{{ url('public/frontEnd') }}/assets/img/favicon.png" />

<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple Touch Icon / Startup Screen -->
<!--<link rel="apple-touch-icon-precomposed" href="/img/apple-touch-icon-144x144.png" />-->
<!--link rel="apple-touch-startup-image"       href="/sites/all/themes/hedu2015/assets/img/startup.png" /> -->


<!-- Tile icon for Win8 (144x144 + tile color) __doesn't validate__-->
<meta name="msapplication-TileImage" content="{{ url('public/frontEnd') }}/assets/img/apple-touch-icon-144x144.png" />
<meta name="msapplication-TileColor" content="#222222" />

<title itemprop="name">Saigon Star International School</title>

<meta name="robots" content="index,follow,NOODP" />
<meta name="description"
    Content="Saigon Star is devoted to excellence in teaching, learning, and research, and to developing leaders in many disciplines who make a difference globally. Saigon Star International School is an International School in Ho Chi Minh City, permitted to operate under Ho Chi Minh City Department of Education" />
<meta name="keywords" Content="Saigon Star, Education, Innovation, Teaching, Learning" />

<meta name="node_id" Content="60293" />

<meta property="fb:admins" content="593072576,599799799,662979311,613602264" />
<meta property="fb:page_id" content="105930651606" />
<meta property="fb:app_id" content="135898969862440" />
<meta property="twitter:account_id" content="1491443782" />
<meta name="twitter:site" content="@SaigonStar">
<meta name="twitter:creator" content="@SaigonStar">
<meta name="twitter:card" content="summary_large_image">
<meta property="og:site_name" content="Saigon Star" />
<meta property="og:type" content="university" />

<meta property="og:title" content="Saigon Star" />
<meta property="og:url" content="https://sgstar.edu.vn" />
<meta property="og:description"
    content="Saigon Star International School is an International School in Ho Chi Minh City, permitted to operate under Ho Chi Minh City Department of Education" />
<meta property="og:image" content="https://sgstar.edu.vn/" />

<!--<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">-->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i' rel='stylesheet'>
<!-- Styles -->
<link rel="stylesheet" href="{{ url('public/frontEnd') }}/assets/css/sgstar.min.css?v=20180820" />


<link rel="stylesheet" href="\public\css\backend\bootstrap.css" />
<link href="{{ url('public/frontEnd') }}/assets/css/print.css" rel="stylesheet" media="print" type="text/css" />

<link href="{{ url('public/frontEnd') }}/assets/css/style2.css" rel="stylesheet">
<link href="{{ url('public') }}/css/style2.css" rel="stylesheet">


<link href="{{ url('public/frontEnd') }}/assets/gallery_slider/css/style.css" rel="stylesheet">

<link rel="stylesheet" href="{{ url('public/frontEnd') }}/css/news.css" />
<link href="{{ url('public') }}/css/swiper.min.css" rel="stylesheet">
<link href="{{ url('public') }}/css/font-awesome.min.css" rel="stylesheet">

<link href="{{ asset('public/build/css/intlTelInput.css') }}" rel="stylesheet">

<link rel="stylesheet" href="{{ url('public/frontEnd') }}/css/general.css" />
<link rel="stylesheet" href="{{ url('public') }}/css/backend/style.css" />
<link rel="stylesheet" href="{{ url('public') }}/css/backend/bootstrap-datepicker.min.css" />
<link rel="stylesheet" href="{{ url('public') }}/backEnd/vendors/css/themify-icons.css" />
<link rel="stylesheet" href="{{ url('public') }}/backEnd/vendors/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="{{ url('public') }}/frontEnd/css/floating-sidebar.css" />
<link rel="stylesheet" href="{{ url('public') }}/frontEnd/css/ripple.min.css" />
<!--Start of Tawk.to Script-->

<!--End of Tawk.to Script-->
@stack('css')
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162129580-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-162129580-1');
</script>
</head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFKSVRT');</script>
<!-- End Google Tag Manager -->
 <!--Google Tag Manager -->
    <!--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':-->
    <!--new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],-->
    <!--j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=-->
    <!--'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);-->
    <!--})(window,document,'script','dataLayer','GTM-TJQF6T3');</script>-->
 <!--End Google Tag Manager -->

@include('frontEnd.modals_form.book_tour_new')

@include('frontEnd.modals_form.job_apply')
@include('frontEnd.modals_form.early_year_timetable')
@include('frontEnd.modals_form.primary_year_timetable')

<body id="body" class="homepage">

    <!-- Google Tag Manager (noscript) -->

    <!-- End Google Tag Manager (noscript) -->

    <div id="page" class="page">

        <!-- include keyboard jump links -->
        <ul class="jump-links">
            <li>
                <a href="#mainArea" id="jumpToMain">
                    Jump to main content
                </a>
            </li>
            <li>
                <a href="#searchArea" id="jumpToSearch">
                    Jump to search
                </a>
            </li>
            <li>
                <a href="#footerArea">
                    Jump to the footer
                </a>
            </li>
        </ul>


        <!--

    Header Area

  -->

        <header role="banner" id="headerArea" class="header-base ">

            <!-- include h1 > link > text combo -->
            <h1 class="harvard-heading">
                <a href="/" class="harvard-heading__link">
                    <img style="width: 33%;margin-top:-2%;margin-left:-4%" src="{{ asset('/public/frontEnd/assets/img/logo/harvard-logo.png') }}" alt="">
                </a>
            </h1>

            <!-- include main navigation component -->
            <a href="siteNavigation"  class="btn--menu" id="btn_menu" data-state="off"
                data-id="nav" aria-label="Reveal Navigation">
                <span class="btn--menu__icon"></span>
                 
            </a>

            <nav role="navigation" class="main-nav" id="siteNavigation">

                <div class="nav-drawer">

                    <div class="nav-group nav-group--consistent" id="mid-nav">
                        <ul class="clearfix">
                            <li class="nav-left-half">
                                <ul role="presentation" class="clearfix">
                                    <li id="home-menu" >
                                        <a href="/home"><i class="fa fa-home"></i></a>
                                    </li>
                                    <li class="has-drop-nav">
                                        <a onclick="event.preventDefault();" href="{{ url('our-school/overview') }}"><span class="menu-txt">Our School <i class="fa fa-caret-down"
                                                    data-toggle="collapse" href="#OurSchool-collapse-items"
                                                    role="button" aria-expanded="false"
                                                    aria-controls="collapseExample"></i></span></a>
                                        <ul class="list--drop-nav">
                                            <li class="first"><a href="{{ url('our-school/welcome') }}"
                                                        class="">Welcome message</a></li>
                                            <li class="first"><a href="{{ url('our-school/overview') }}"
                                                    class="first">Introduction</a></li>
                                            <li class="first"><a href="{{ url('teachers') }}"
                                                    class="first">Teachers</a></li>
                                            <li class="first"><a href="{{ url('our-school/campus') }}" class="">Campus</a>
                                            </li>
                                           
                                        </ul>
                                        <div class="collapse" id="OurSchool-collapse-items">
                                            <ul class="mobile-navbar">
                                                <li class="first"><a href="{{ url('our-school/welcome') }}"
                                                        class="">Welcome</a></li>
                                                    <li class="first"><a href="{{ url('our-school/overview') }}"
                                                        class="last">Introduction</a></li>
                                                
                                                <li class="first"><a href="{{ url('our-school/campus') }}" 
                                                    class="">Campus</a>
                                                </li>
                                                <li class="first"><a href="{{ url('teachers') }}" 
                                                    class="">Teacher</a>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-drop-nav">
                                        <a onclick="event.preventDefault();" href="/curriculum"><span class="menu-txt">Curriculum <i class="fa fa-caret-down"
                                                    data-toggle="collapse" href="#Curriculum-collapse-items"
                                                    role="button" aria-expanded="false"
                                                    aria-controls="collapseExample"></i></span></a>
                                        <ul class="list--drop-nav">
                                            <li class="first"><a href="{{ url('curriculum') }}"
                                                    class="first">Overview</a></li>
                                            <li class="first"><a href="{{ url('early-year') }}"
                                                    class="">Early Years (age 2-5)</a></li>
                                            <li class="first"><a href="{{ url('primary-year') }}" class="">Primary
                                                    Years
                                                    (age 5-11)</a></li>
                                            
                                        </ul>
                                        <div class="collapse" id="Curriculum-collapse-items">
                                            <ul class="mobile-navbar">
                                                <li class="first"><a href="/curriculum"
                                                    class="first">Overview</a></li>
                                                <li class="first"><a href="{{ url('early-year') }}"
                                                        class="">Early Years (age 2-5)</a></li>
                                                <li class="first"><a href="{{ url('primary-year') }}" class="">Primary
                                                        Years
                                                        (age 5-11)</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-drop-nav">
                                        <a onclick="event.preventDefault();" href="{{ url('post/admissions-process') }}"><span class="menu-txt">Admissions <i class="fa fa-caret-down"
                                                    data-toggle="collapse" href="#Admissions-collapse-items"
                                                    role="button" aria-expanded="false"
                                                    aria-controls="collapseExample"></i></span></a>
                                        <ul class="list--drop-nav">
                                            <li class="first"><a href="/why-us" class="">Why SGSIS</a></li>
                                            <li class="first"><a href="{{ url('post/admissions-process') }}"
                                                    class="first">Admissions Process</a></li>
                                           
                                            <li class="first"><a href="{{ url('admissions') }}" class="">Fees and
                                                    Policy</a>
                                            </li>
                                            <li class="first"><a href="{{ url('form/enrolment-form') }}" class="">How to apply?</a>
                                            </li>
                                            <li class="first"><a href="{{ url('scholarship') }}" class="">Scholarship</a>
                                            </li>
                                            <li class="first"><a href="{{ url('book-a-tour') }}" class="">Book a tour</a>
                                            </li>
                                            <li class="first"><a href="{{ url('admission-team') }}"
                                                    class="last">Admissions team</a></li>
                                        </ul>

                                        <div class="collapse" id="Admissions-collapse-items">
                                            <ul class="mobile-navbar">
                                                <li class="first"><a href="/why-us" class="">Why SGSIS</a></li>
                                                <li class="first"><a href="{{ url('post/admissions-process') }}"
                                                        class="first">Admissions Process</a></li>
                                               
                                                <li class="first"><a href="{{ url('admissions') }}" class="">Fees and
                                                        Policy</a>
                                                </li>
                                                <li class="first"><a href="{{ url('form/enrolment-form') }}" class="">How to apply?</a>
                                                </li>
                                                <li class="first"><a href="{{ url('scholarship') }}" class="">Scholarship</a>
                                                </li>
                                                <li class="first"><a href="{{ url('book-a-tour') }}" class="">Book a tour</a>
                                                </li>
                                                <li class="first"><a href="{{ url('admission-team') }}"
                                                        class="last">Admissions team</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                  

                                   
                                  
                                    
                                </ul>
                            </li>
                            <li class="nav-right-half">
                                <ul role="presentation" class="clearfix">
                                   
                                    <li class="has-drop-nav">
                                        <a onclick="event.preventDefault();" href="{{ url('/parents-essentials') }}"><span class="menu-txt">Parents <i class="fa fa-caret-down"
                                                    data-toggle="collapse" href="#Parents-collapse-items" role="button"
                                                    aria-expanded="false"
                                                    aria-controls="collapseExample"></i></span></a>
                                        <ul class="list--drop-nav">
                                            <li class="first"><a href="{{ url('parents/school-calendar') }}"
                                                    class="">School calendar</a></li>
                                          
                                            <li class="has-sub-menu first"><a href="{{ url('parents/timetable') }}" class="">Timetable</a>
                                                
                                            </li>
                                            <li class="first"><a href="{{ url('parents/menu') }}" class="">Menu</a></li>
                                            <li class="first"><a href="{{ url('parents/bus-schedule') }}" class="">Bus
                                                    Schedule</a></li>
                                            <li class="first"><a href="{{ url('parents/workshop') }}"
                                                    class="">Workshop</a>
                                            </li>
                                            <li class="first"><a href="{{ url('/parents-essentials') }}" class="">Parent essentials</a></li>
                                            <!--<li class="first"><a href="parents-handbook" class="">Parents' handbook</a></li>-->
                                     
                                            <li class="first"><a href="{{ url('parents/parent-teacher-group') }}"
                                                    class="">Parent Teacher Group</a></li>
                                            

                                        </ul>

                                        <div class="collapse" id="Parents-collapse-items">
                                            <ul class="mobile-navbar">
                                                <li class=""><a href="{{ url('parents/school-calendar') }}"
                                                    class="">School calendar</a></li>
                                                <li class=""><a href="{{ url('parents/timetable') }}"
                                                        class="">Timetable</a></li>
                                                <li class=""><a href="{{ url('parents/menu') }}" class="">Menu</a></li>
                                                <li class=""><a href="{{ url('parents/bus-schedule') }}" class="">Bus
                                                        Schedule</a></li>
                                                <li class=""><a href="{{ url('parents/workshop') }}"
                                                        class="">Workshop</a>
                                                </li>
                                                <li class=""><a href="{{ url('/parents-essentials') }}" class="">Parent essentials</a></li>
                                                <!--<li class=""><a href="parents-handbook" class="">Parents' handbook</a></li>-->
                                               
                                                
                                                
                                                
                                                <li class=""><a href="{{ url('parents/parent-teacher-group') }}"
                                                        class="">Parent Teacher Group</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                   
                                    <li class="has-drop-nav">
                                        <a onclick="event.preventDefault();" href="{{ url('student-council') }}"><span class="menu-txt">Students <i class="fa fa-caret-down"
                                                    data-toggle="collapse" href="#Student-collapse-items" role="button"
                                                    aria-expanded="false"
                                                    aria-controls="collapseExample"></i></span></a>
                                        <ul class="list--drop-nav">
                                            <li class="first"><a href="{{ url('student-council') }}"
                                                    class="">Student Council</a></li>
                                            <li class="first"><a href="{{ url('sgsis-team') }}" class="">SGSIS News
                                                    Team</a>
                                            </li>
                                            <li class="first"><a href="{{ url('winter-camp') }}"
                                                    class="">Winter
                                                    Camp</a></li>
                                            <li class="first"><a href="{{ url('summer-camp') }}"
                                                    class="">Summer
                                                    Camp</a></li>
                                            <li class="first"><a href="{{url('student-of-the-month')}}"
                                            class="">Community success</a></li>
                                            
                                        </ul>
                                        <div class="collapse" id="Student-collapse-items">
                                            <ul class="mobile-navbar">
                                                 <li class=""><a href="{{ url('student-council') }}"
                                                    class="">Student Council</a></li>
                                            <li class=""><a href="{{ url('sgsis-team') }}" class="">SGSIS News
                                                    Team</a>
                                            </li>
                                            <li class=""><a href="{{ url('winter-camp') }}"
                                                    class="">Winter
                                                    Camp</a></li>
                                            <li class=""><a href="{{ url('summer-camp') }}"
                                                    class="">Summer
                                                    Camp</a></li>
                                            <li class=""><a href="{{url('student-of-the-month')}}"
                                            class="">Community success</a></li>
                                           
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-drop-nav" >
                                        <a onclick="event.preventDefault();" href="{{ url('leadership-team') }}"><span class="menu-txt">Staff <i class="fa fa-caret-down"
                                                    data-toggle="collapse" href="#Staff-collapse-items" role="button"
                                                    aria-expanded="false"
                                                    aria-controls="collapseExample"></i></span></a>
                                        <ul class="list--drop-nav">
                                            {{-- <li class=""><a href="{{ url('leadership-team') }}" class="">Leadership
                                                    Team</a></li>
                                            <li class="first"><a href="{{ url('academic-team') }}" class="">Academic
                                                    Team</a></li>
                                            <li class="first"><a href="{{ url('TA-team') }}" class="">LTA Team</a></li>
                                            <li class="first"><a href="{{ url('admission-team') }}" class="">Admission
                                                    Team</a></li>
                                            <li class="first"><a href="{{ url('operation-team') }}"
                                                    class="">Operations Team</a>
                                            </li>
                                            <li class="first"><a href="{{ url('marketing') }}" class="">Marketing & Event Team</a>
                                                    
                                            </li>
                                            <li class="first"><a href="{{ url('it-team') }}" class="">IT Team</a></li> --}}
                                            <li class="first has-sub-menu"><a onclick="event.preventDefault();" href="#" class="">Meet our team</a>
                                                <ul class="sub-menu-2nd-level list--drop-nav">
                                                    <li class="first"><a href="{{ url('leadership-team') }}" class="">Leadership
                                                    Team</a></li>
                                                    <li class="first"><a href="{{ url('academic-team') }}" class="">Academic
                                                            Team</a></li>
                                                    <li class="first"><a href="{{ url('TA-team') }}" class="">LTA Team</a></li>
                                                    <li class="first"><a href="{{ url('admission-team') }}" class="">Admissions
                                                            Team</a></li>
                                                    <li class="first"><a href="{{ url('operation-team') }}"
                                                            class="">Operations Team</a>
                                                    </li>
                                                    <li class="first"><a href="{{ url('marketing') }}" class="">Marketing
                                                            & Event Team</a>
                                                    </li>
                                                    <li class=""><a href="{{ url('it-team') }}" class="">IT Team</a></li>
                                                </ul>
                                            </li>
                                            <li class="first"><a href="{{ url('staff-workshop') }}" class="">Workshop</a></li>
                                            <li class="first"><a href="{{ url('/careers') }}" class="">Join us</a></li>
                                        </ul>
                                        <div class="collapse" id="Staff-collapse-items">
                                            <ul class="mobile-navbar">
                                                <li class=""><a href="{{ url('leadership-team') }}" class="">Leadership
                                                    Team</a></li>
                                                <li class=""><a href="{{ url('academic-team') }}" class="">Academic
                                                        Team</a></li>
                                                <li class=""><a href="{{ url('TA-team') }}" class="">LTA Team</a></li>
                                                <li class=""><a href="{{ url('admission-team') }}" class="">Admissions
                                                        Team</a></li>
                                                <li class=""><a href="{{ url('operation-team') }}"
                                                        class="">Operations Team</a>
                                                </li>
                                                <li class=""><a href="{{ url('marketing') }}" class="">Marketing
                                                        Event Team</a>
                                                </li>
                                                <li class=""><a href="{{ url('it-team') }}" class="">IT Team</a></li>
                                                <li class=""><a href="#" class="">Meet our team</a>
                                                </li>
                                                <li class=""><a href="staff-workshop" class="">Workshop</a></li>
                                                <li class=""><a href="{{ url('/careers') }}" class="">Join us</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-drop-nav">

                                            <a onclick="event.preventDefault();" href="{{ url('/events') }}" class="">
                                                <span class="menu-txt"> News & Events <i class="fa fa-caret-down"
                                                        data-toggle="collapse" href="#about-collapse-items" role="button"
                                                        aria-expanded="false" aria-controls="collapseExample"></i></span>
                                            </a>
                                            <ul class="list--drop-nav">
                                                <li class="first"><a href="{{ url('/events') }}" class="">Quality cycle</a></li>
                                                <li class="first"><a href="{{ url('parents/the-sgs-journal') }}" class="">The
                                                        SGSIS Journal</a></li>
                                                <li class="first"><a href="{{ url('parents/the-sgs-tv') }}" class="">The SGSIS
                                                        TV</a></li>
                                            </ul>
                                            <div class="collapse" id="about-collapse-items">
                                                <ul class="mobile-navbar">
                                                <li class=""><a href="{{ url('/events') }}" class="">Quality cycle</a></li>
                                                <li class=""><a href="{{ url('parents/the-sgs-journal') }}" class="">The
                                                        SGSIS Journal</a></li>
                                                <li class=""><a href="{{ url('parents/the-sgs-tv') }}" class="">The SGSIS
                                                        TV</a></li>
                                                </ul>
                                            </div>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                 


                    <div class="list-clean">

                        <ul class="nav-group nav-group--top-bar">
                            <!--<li class="top-nav-1"><a href="#">Feed back</a></li>-->
                            <!--<li class="top-nav-1"><a href="/contact">Contact</a></li>-->
                            <!--<li class="top-nav-1"><a href="/news-page">Bulletin</a></li>-->
                            <!--<li class="top-nav-1"><a href="#">Visitors</a></li>-->
                            <li class="top-nav-1 "><a href="#" class="media" style="color:#191970">.</a></li>
                            <li class="top-nav-1 "><a href="#" id="document_mobile">Document</a></li>
                            <li class="top-nav-1 last"><a href="{{ url('/login') }}" id="login_mobile">Login</a></li>
                            
                        </ul>


                    </div>


                    <input type="hidden" id="scroll-count" name="">
                </div> <!-- end .nav-drawer -->

            </nav>
            <div class="phone-contact">
                <a href="tel: (+84)8 8800 6996"> <i class="fa fa-phone"></i> 08 8800 6996</a>
            </div>
            <!--<form role="search" id="searchArea" class="search-area" method="get" action="/searches/">-->
            <!--    <label for="siteSearch">-->
            <!--        <span class="is-visually-hidden">-->
            <!--            Search:-->
            <!--        </span>-->
            <!--    </label>-->

            <!--    <input type="search" name="searchtext" id="searchSite" class="input-search" aria-label="search"-->
            <!--        placeholder="What can we help you find?" />-->
            <!--    <button type="button" class="btn--mobile-site-search">-->
            <!--        <span class="is-visually-hidden">-->
            <!--            Open Search-->
            <!--        </span>-->
            <!--    </button>-->
            <!--</form>-->
            
            <!--<div id="show-hide" >-->
            <!--    <a href="#"><span id="document_button" >DOCUMENT</span></a>-->
            <!--    <a href="{{url('/login')}}"><span id="login_button" >LOGIN</span></a>-->
                <!--<a href="" onclick="event.preventDefault()" id="search"><span id="search_button"> <i class="fa fa-search"></i> </span></a>-->
                <!--<form class="example" action="/action_page.php" >-->
                <!--  <input type="text" placeholder="Search.." name="search2">-->
                <!--  <button type="submit"><i class="fa fa-arrow-right"></i></button>-->
                <!--</form>-->
            <!--</div>-->
            <div class="container menu-bar" onclick="myFunction(this)">
              <div class="bar1"></div>
              <div class="bar2"></div>
              <div class="bar3"></div>
            </div>
            
            <div class="tablet-menu">
                <div class="row">
                    <div class="col-md-3">
                      <div class="list-tag"><h3 class="list-tag-text">Our School</h3></div>
                      <div class="list-lg">
                            <ul class="list-item-footer">
                               <a href="/our-school/welcome"><li class="list-item-footer-item">Welcome Message</li></a>
                               <a href="/our-school/overview"><li class="list-item-footer-item">Overview</li></a>
                               <a href="/teachers"><li class="list-item-footer-item">Teacher</li></a>
                               <a href="/our-school/campus"><li class="list-item-footer-item">Campus</li></a>
                               
                          </ul>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="list-tag"><h3 class="list-tag-text">Curriculum</h3></div>
                      <div class="list-lg">
                            <ul class="list-item-footer">
                               <a href="/curriculum"><li class="list-item-footer-item">Overview</li></a>
                               <a href="/early-years"><li class="list-item-footer-item">Early Years</li></a>
                               <a href="/primary-years"><li class="list-item-footer-item">Primary Years</li></a>
                               
                          </ul>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="list-tag"><h3 class="list-tag-text">Admissions</h3></div>
                      <div class="list-lg">
                            <ul class="list-item-footer">
                               <a href="/why-us"><li class="list-item-footer-item">Why Choose SaigonStar?</li></a>
                               <a href="/post/admissions-process"><li class="list-item-footer-item">Admissions Process</li></a>
                               <a href="/admissions"><li class="list-item-footer-item">Fees and Policy</li></a>
                               <a href="{{ url('form/enrolment-form') }}"><li class="list-item-footer-item">How to apply?</li></a>
                               <a href="/scholarship"><li class="list-item-footer-item">Scholarship</li></a>
                               <a href="/book-a-tour"><li class="list-item-footer-item">Book a Tour</li></a>
                               <a href="/admission-team"><li class="list-item-footer-item">Admissions Team</li></a>
                          </ul>
                      </div>
                    </div>
                    
                  <div class="col-md-3">
                      <div class="list-tag"><h3 class="list-tag-text">Our Parents</h3></div>
                      <div class="list-lg">
                            <ul class="list-item-footer">
                              <a href="/parents/school-calendar"><li class="list-item-footer-item">Calendar</li></a>
                              <a href="/parents/timetable"><li class="list-item-footer-item">Timetable</li></a>
                              <a href="/parents/menu"><li class="list-item-footer-item">Menu</li></a>
                              <a href="/parents/bus-schedule"><li class="list-item-footer-item">Parent Essentials</li></a>
                              <!--<a href="/parents/workshop"><li class="list-item-footer-item">Handbook</li></a>-->
                              <a href="/parents"><li class="list-item-footer-item">Workshop</li></a>
                              <a href="#"><li class="list-item-footer-item">Bus schedule</li></a>
                              <a href="/parents/parent-teacher-group"><li class="list-item-footer-item">Testimonial</li></a>
                          </ul>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="list-tag"><h3 class="list-tag-text">Our Student</h3></div>
                      <div class="list-lg">
                            <ul class="list-item-footer">
                              <a href="{{ url('student-council') }}"><li class="list-item-footer-item">Student Council</li></a>
                              <a href="/sgsis-team"><li class="list-item-footer-item">SGSIS News Team</li></a>
                              <a href="{{ url('winter-camp') }}"><li class="list-item-footer-item">Winter Camp</li></a>
                              <a href="{{ url('summer-camp') }}"><li class="list-item-footer-item">Summer Camp</li></a>
                              <a href="{{url('student-of-the-month')}}"><li class="list-item-footer-item">Student of The Month</br>Community success/celebration</li></a>
                              
                              
                          </ul>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="list-tag"><h3 class="list-tag-text">Our Staff</h3></div>
                      <div class="list-lg">
                            <ul class="list-item-footer">
                              <a href="/leadership-team"><li class="list-item-footer-item">Leadership Team</li></a>
                              <a href="/academic-team"><li class="list-item-footer-item">Academic Team</li></a>
                              <a href="/TA-team"><li class="list-item-footer-item">TA Team</li></a>
                              <a href="/admission-team"><li class="list-item-footer-item">Admissions Team</li></a>
                              <a href="/operation-team"><li class="list-item-footer-item">Operations Team</li></a>
                              <a href="/marketing"><li class="list-item-footer-item">Marketing & Event Team</li></a>
                              <a href="/it-team"><li class="list-item-footer-item">IT Team</li></a>
                              <a href="staff-workshop"><li class="list-item-footer-item">Workshop</li></a>
                              <a href="/careers"><li class="list-item-footer-item">Join Us</li></a>
                              
                          </ul>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="list-tag"><h3 class="list-tag-text">News And Event</h3></div>
                      <div class="list-lg">
                            <ul class="list-item-footer">
                              <a href="/events"><li class="list-item-footer-item">Quality cycle</li></a>
                              <a href="/parents/the-sgs-journal"><li class="list-item-footer-item">The SGSIS Journal</li></a>
                              <a href="/parents/the-sgs-tv"><li class="list-item-footer-item">The SGSIS TV</li></a>
                          </ul>
                      </div>
                  </div>
                  
                </div>

            </div>
            <div class="test_button">
                <a href="#" id=documen_bt>DOCUMENT</a>
                <a href="/login" id=login_bt>LOGIN</a>
            </div>
            <nav class="floating-menu">
                <ul class="main-menu">
                    <li>
                        <a href="{{url('/')}}" class="ripple sidebar-group">
                            <i class="fa fa-home fa-icon"></i>
                            <!--<br><b class="sidebar-title">home</b>-->
                        </a>
                    </li>
                    <li>
                        <a href="https://zalo.me/0888944455" target="_blank" class="ripple sidebar-group">
                            <img src="https://stc-zaloprofile.zdn.vn/pc/v1/images/zalo_sharelogo.png" width="25px" height="25px">
                        </a>
                    </li>
                    
                    <li>
                        <a href="{{url('book-a-tour')}}" class="ripple sidebar-group">
                            <i class="fa fa-clipboard fa-icon"></i>
                            <!--<br><b class="sidebar-title">visit</b>-->
                        </a>
                    </li>
                    <li>
                        <a href="tel:(+84)8 8800 6996" class="ripple sidebar-group">
                            <i class="fa fa-phone fa-icon"></i>
                            <!--<br><b class="sidebar-title">phone</b>-->
                        </a>
                    </li>
                </ul>
                <div class="menu-bg"></div>
            </nav>
                   
                  
