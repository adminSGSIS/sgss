@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
<link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
    <link rel="stylesheet" href="{{url('public')}}/frontEnd/css/newcss.css"/> 
@endpush
<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>{{strtoupper($dayOfWeek)}} MENU</h3>
        </div>
	</div>
</div>
<div class="main">
		<div class="week-first-half content menu-food">
			<div class="container" style="padding-bottom: 30px;text-align:center">
				<a href="{{url('parents/menu/monday')}}" type="button" class="btn-menu  btn-sm @if(strtolower($dayOfWeek) == 'monday') select @endif">Monday</a>
				<a href="{{url('parents/menu/tuesday')}}" type="button" class="btn-menu   btn-sm @if(strtolower($dayOfWeek) == 'tuesday') select @endif">Tuesday</a>
				<a href="{{url('parents/menu/wednesday')}}" type="button" class="btn-menu   btn-sm @if(strtolower($dayOfWeek) == 'wednesday') select @endif">Wednesday</a>
				<a href="{{url('parents/menu/thursday')}}" type="button" class="btn-menu   btn-sm @if(strtolower($dayOfWeek) == 'thursday') select @endif">Thursday</a>
				<a href="{{url('parents/menu/friday')}}" type="button" class="btn-menu   btn-sm @if(strtolower($dayOfWeek) == 'friday') select @endif">Friday</a>
			</div>
			<div class="table-responsive">
			<table class="table container table-bordered">
			  <thead class="table-primary">
			    <tr>
			      <td scope="row"><h5>Hot Dishes / Các món nóng</h5></td>
			    </tr>
			  </thead>
			  <tbody>
			  	@foreach($menu as $hotdishes)
			  	@if($hotdishes->food->id == 1)
			    <tr>
			      <th scope="row">{{$hotdishes->food->name}}</th>
			    </tr>
			    @endif
			    @endforeach
			  </tbody>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Vegetarian / Các món chay </h5></td>
			    </tr>
			  </thead>
			  <tbody>
			    @foreach($menu as $hotdishes)
			  	@if($hotdishes->food->id == 2)
			    <tr>
			      <th scope="row">{{$hotdishes->food->name}}</th>
			    </tr>
			    @endif
			    @endforeach
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Salad & vegetables / Salad và rau củ </h5></td>
			    </tr>
			  </thead>
			  <tbody>
			    @foreach($menu as $hotdishes)
			  	@if($hotdishes->food->id == 3)
			    <tr>
			      <th scope="row">{{$hotdishes->food->name}}</th>
			    </tr>
			    @endif
			    @endforeach
			  </tbody>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Hams / Các loại thịt nguội</h5></td>
			    </tr>
			  </thead>
			  <tbody>
			    @foreach($menu as $hotdishes)
			  	@if($hotdishes->food->id == 4)
			    <tr>
			      <th scope="row">{{$hotdishes->food->name}}</th>
			    </tr>
			    @endif
			    @endforeach
			  </tbody>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Dessert / Tráng miệng</h5></td>
			    </tr>
			  </thead>
			  <tbody>
			  	<tr>
			  		<th class="table-primary" scope="row">FRUITS / TRÁI CÂY - FRUIT JUICE / NƯỚC ÉP - PASTRIES / BÁNH</th>
			  	</tr>
			  </tbody>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Snack / Ăn nhẹ buổi chiều</h5></td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      @foreach($menu as $hotdishes)
				  	@if($hotdishes->food->id == 5)
				    <tr>
				      <th scope="row">{{$hotdishes->food->name}}</th>
				    </tr>
				    @endif
				  @endforeach

			    </tr>
			  </tbody>
			  
			
			</table>
		</div>
		</div>
</div>
@endsection