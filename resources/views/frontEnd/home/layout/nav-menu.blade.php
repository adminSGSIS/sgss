 <header role="banner" id="headerArea" class="header-base ">

  <!-- include h1 > link > text combo -->
  <h1 class="harvard-heading">
    <a href="/" class="harvard-heading__link">
      <span class="is-visually-hidden">
        Harvard University
      </span>
    </a>
  </h1>

  <!-- include main navigation component -->
  <a href="#siteNavigation"
      class="btn--menu"
      id="btn_menu"
      data-state="off"
      data-id="nav"
      aria-label="Reveal Navigation">
    <span class="btn--menu__icon"></span>
    Menu
  </a>

<nav role="navigation" class="main-nav" id="siteNavigation">

  <div class="nav-drawer">

    <div class="nav-group nav-group--consistent" id="mid-nav">
      <ul class="clearfix">
        <li class="nav-left-half">
          <ul role="presentation" class="clearfix">
            <li class="has-drop-nav">
              <a href="#" class="">
               <span class="menu-txt"> About</span>
              </a>
                  <ul  class="list--drop-nav">
                    <li class="first"><a href="#" class="first">Harvard at a Glance</a></li>
                    <li class=""><a href="#" class="">Harvard's President</a></li>
                    <li class=""><a href="#" class="">Harvard's Leadership</a></li>
                    <li class=""><a href="#" class="">Academic Experience</a></li>
                    <li class=""><a href="#" class="">Administrative Offices</a></li>
                    <li class=""><a href="#" class="">Directories</a></li>
                    <li class=""><a href="#" class="">Harvard in the Community</a></li>
                    <li class=""><a href="#" class="">News</a></li>
                  </ul>
            </li>
            <li class="has-drop-nav">
                  <a href="/admissions-aid"><span class="menu-txt"> Curriculum</span></a>
                  <ul  class="list--drop-nav">
                    <li class="first"><a href="#" class="first">Undergraduate</a></li>
                    <li class=""><a href="#" class="">Graduate & Professional Schools</a></li>
                    <li class=""><a href="#" class="">Continuing Education</a></li>
                    <li class="last"><a href="#" class="last">Summer Programs</a></li>
                  </ul>
            </li>
            <li class="has-drop-nav">
              <a href="/admissions-aid"><span class="menu-txt"> Our School</span></a>
              <ul  class="list--drop-nav">
                <li class="first"><a href="#" class="first">Undergraduate</a></li>
                <li class=""><a href="#" class="">Graduate & Professional Schools</a></li>
                <li class=""><a href="#" class="">Continuing Education</a></li>
                <li class="last"><a href="#" class="last">Summer Programs</a></li>
              </ul>
             </li>
          </ul>
        </li>
        <li class="nav-right-half">
          <ul role="presentation" class="clearfix">
            <li class="has-drop-nav">
                      <a href="/schools"><span class="menu-txt">Admissions</span></a>              
                      <ul  class="list--drop-nav">
                        <li class="first"><a href="#" class="first">Business</a></li>
                        <li class=""><a href="#" class="">College</a></li>
                        <li class=""><a href="#" class="">Continuing Education</a></li>
                        <li class=""><a href="#" class="">Dental</a></li>
                        <li class=""><a href="#" class="">Design</a></li>
                        <li class=""><a href="#" class="">Divinity</a></li>
                        <li class=""><a href="#" class="">Education</a></li>
                        <li class=""><a href="#" class="">Engineering</a></li>
                        <li class=""><a href="#" class="">Faculty of Arts & Sciences</a></li>
                        <li class=""><a href="#" class="">Government</a></li>
                        <li class=""><a href="#" class="">Graduate School</a></li>
                        <li class=""><a href="#" class="">Law</a></li>
                        <li class=""><a href="#" class="">Medical</a></li>
                        <li class=""><a href="#" class="">Public Health</a></li>
                        <li class="last"><a href="#" class="last">Radcliffe Institute</a></li>
                      </ul>
              </li>
              <li class="has-drop-nav">
                          <a href="/on-campus"><span class="menu-txt">On Campus</span></a>
                          <ul  class="list--drop-nav">
                            <li class="first"><a href="#" class="first">Arts and Humanities</a></li>
                            <li class=""><a href="#" class="">Athletics</a></li>
                            <li class=""><a href="#" class="">Commencement</a></li>
                            <li class=""><a href="#" class="">Employment</a></li>
                            <li class=""><a href="#" class="">Events</a></li>
                            <li class=""><a href="#" class="">HarvardX</a></li>
                            <li class=""><a href="#" class="">Library</a></li>
                            <li class=""><a href="#" class="">Museums</a></li>
                            <li class=""><a href="#" class="">Research</a></li>
                            <li class=""><a href="#" class="">Science</a></li>
                            <li class="last"><a href="#" class="last">Visit Harvard</a></li>
                          </ul>
                </li>
                <li class="has-drop-nav">
                  <a href="/admissions-aid"><span class="menu-txt">Contact</span></a>
                  <ul  class="list--drop-nav">
                    <li class="first"><a href="#" class="first">Undergraduate</a></li>
                    <li class=""><a href="#" class="">Graduate & Professional Schools</a></li>
                    <li class=""><a href="#" class="">Continuing Education</a></li>
                    <li class="last"><a href="#" class="last">Summer Programs</a></li>
                  </ul>
                 </li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="nav-group nav-group--callouts" style="background-color: #e6007e">
      <ul class="clearfix">
        <li class="nav-left-half">
          <ul role="presentation" class="clearfix">
            <li>
              <a href="#" style="color: #fff">
                 News
              </a>
            </li>
            <li>
              <a href="#s" style="color: #fff">
                Events
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-right-half">
          <ul role="presentation" class="clearfix">
            <li>
              <a href="#" style="color: #fff">
                Visit
              </a>
            </li>
            <li>
              <a href="#" style="color: #fff">
                Give
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    
    
			<div class="list-clean">
				
  <ul  class="nav-group nav-group--top-bar">
  
		<li class="first"><a href="/faculty" class="first">Faculty</a></li>
		<li class=""><a href="/staff" class="">Staff</a></li>
		<li class=""><a href="/students" class="">Students</a></li>
		<li class=""><a href="http://alumni.harvard.edu" class="">Alumni</a></li>
		<li class=""><a href="/parents" class="">Parents</a></li>
		<li class=""><a href="/on-campus/visit-harvard" class="">Visitors</a></li>
		<li class="last"><a href="/media-relations" class="last">Media</a></li>
  </ul>
  
			</div>
			
  </div> <!-- end .nav-drawer -->

</nav>

<form role="search" id="searchArea" class="search-area" method="get" action="/searches/">
  <label for="siteSearch">
    <span class="is-visually-hidden">
      Search:
    </span>
  </label>

  <input type="search"
         name="searchtext"
         id="searchSite"
         class="input-search"
         aria-label="search"
         placeholder="What can we help you find?" />
  <button type="button" class="btn--mobile-site-search">
    <span class="is-visually-hidden">
      Open Search
    </span>
  </button>
</form>


  </header>