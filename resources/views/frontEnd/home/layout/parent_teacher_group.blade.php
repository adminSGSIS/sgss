@extends('frontEnd.home.layout.front_master')
@section('main_content')
    @push('css')
        <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
        <link rel="stylesheet" href="{{ url('public') }}/frontEnd/css/newcss.css" />
    @endpush

    <div class="banner">
        <img class="banner" width="100%" src="{{ asset('public/') }}/images/banner-du-an1.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>Parent teacher group</h3>
            </div>
        </div>
    </div>
    <div class="main_content" style="background:#fafaf4">
        <div class="container d-flex justify-content-center">
            <div class="col-lg-10 col-sm-12 pt-30">
                <img width="100%" style="border-radius:15px" src="{{ asset('public/') }}/images/parent-teacher-group.jpg" alt="">
            </div>
        </div>
        <div class="container">
              <img class="banner" width="100%" src="{{asset('public/')}}/images/CHILDREN-01.png" alt=""> 
        </div>
       
    </div>
@endsection

