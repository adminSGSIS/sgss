@extends('frontEnd.home.layout.front_master')
@section('main_content')
    @push('css')
        <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
        <link rel="stylesheet" href="{{ url('public') }}/frontEnd/css/newcss.css" />
    @endpush

    <div class="banner">
        <img class="banner" width="100%" src="{{ asset('public/') }}/images/banner-du-an.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>
                    <div id="canhide">QUALITY CYCLE
                </h3>
            </div>
        </div>
    </div>
    <div class="main_content">
        <div class="background-content">
            <img class="banner" width="100%" src="{{ asset('public/') }}/images/BG_03.jpeg" alt="">

            <div class="early-year">
                <img width="70%" src="{{ asset('public/') }}/images/Quality Cycle 2020-2021-01.png">
            </div>
        </div>

    </div>
@endsection
