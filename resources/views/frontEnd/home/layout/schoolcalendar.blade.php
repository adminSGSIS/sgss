@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
<link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
<link rel="stylesheet" href="{{url('public')}}/frontEnd/css/newcss.css"/> 
@endpush

<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>SCHOOL CALENDAR</h3>
        </div>
	</div>
	
	
</div>
<div class="main_content">
	<div class="c-color">
		<div class="container d-flex justify-content-center pb-30 pt-30">
			<img width="100%" src="{{asset('public/')}}/images/SCHOOL CALENDAR_20-21.png">
		</div>		
	</div>
	
</div>
@endsection