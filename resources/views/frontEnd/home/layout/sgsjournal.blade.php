@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
<link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
    <link rel="stylesheet" href="{{url('public')}}/frontEnd/css/newcss.css"/> 
@endpush

<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>SGSIS JOURNAL</h3>
        </div>
	</div>
	
	
</div>

<div class="main_content c-color">
    <br>
    <div class="container">
        <div class="text-center pb-10">
            <a type="button" class="title-mobile">VOL 1</a>
        </div>
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner d-flex justify-content-center">
                <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item active">
                    <a target="_blank" href="/public/tailieu/THE SAIGON JOURNAL_10112020.pdf">
                        <img src="{{asset('public/')}}/images/sgsis-journal.jpg" alt="">
                    </a>
                </div>
                
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick="event.preventDefault();">
                    <span class="fa fa-chevron-left"  aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" onclick="event.preventDefault();">
            <span class="fa fa-chevron-right"  aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
	</div>
	
	<div class="container">
	    <img class="banner" width="100%" src="{{asset('public/')}}/images/CHILDREN-01.png" alt="">
	</div>
</div>
@endsection
