@extends('frontEnd.home.layout.front_master')
@section('main_content')
    @push('css')
        <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
        <link rel="stylesheet" href="{{ url('public') }}/frontEnd/css/newcss.css" />
    @endpush

    <div class="banner">
        <img class="banner" width="100%" src="{{ asset('public/') }}/images/banner-du-an1.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>TIMETABLE</h3>
            </div>
        </div>
    </div>
    <div class="main_content">
        <div class="c-color pt-30 d-flex justify-content-center">
            <div class="col-md-8 col-12">
                <a target="_blank" href="/public/tailieu/02_SSIS_WEB_PARENT_Timetable_EY.pdf" >
                    <div class="early-year">
                        <img width="50%" src="{{ asset('public/') }}/images/LOGO_EY-01.png" id="EYTimetable">
                    </div>
                </a>
                <a target="_blank" href="/public/tailieu/02_SSIS_WEB_PARENT_Timetable_PY.pdf"  >
                    <div class="primary-year">
                        <img width="60%" src="{{ asset('public/') }}/images/LOGO_PY-01.png" id="PYTimetable">
                    </div>
                </a>
            </div>
        </div>

    </div>
@endsection
