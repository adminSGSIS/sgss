@php $page_title="All about Infix School management system; School management software"; @endphp
@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public')}}/frontEnd/css/new_style.css"/>
    
@endpush
@section('main_content')

    <img width="100%" src="{{asset('public/')}}/images/banner-du-an.jpeg" alt="">
    <!--================ End Facts Area =================-->

    <!--================ Tran Thanh Phu start =================-->
    <div class="header">
        <h4>Home / About us</h4>
    </div>
    <div class="wrapper">
        <h3 class="mission">1. Our Mission</h3>
        <div class="missionContent">
            <i class="fa fa-quote-left mr-5" aria-hidden="true"></i>
            <h3>
                <span style="font-family: Arial, Helvetica, sans-serif;">
                    <span style="color: rgb(22, 160, 133);">"</span>
                </span>
                To provide a safe and supportive learning environment, and every opportunity for children to be successful
                both as learners and as contributors in a changing world
                <span style="font-family: Arial, Helvetica, sans-serif;">
                    <span style="color: rgb(22, 160, 133);">"</span>
                </span>
            </h3>
        </div>
        <h3 class="mission mt-3">2. Our Shared Vision & Mission Explained</h3>
        <div class="ourShared">
            <h4>Responsible global citizens</h4>
            <div class="image">
                <img src="public/images/vision-150x150.jpg" alt="">
            </div>
            <p>{!!$cate_post->news_body!!}</p>
        </div>
    </div>
    <!--================ Tran Thanh Phu end =================-->
    
@endsection

