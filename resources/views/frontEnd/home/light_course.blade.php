@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public/')}}/frontEnd/css/new_style.css"/>
@endpush
@section('main_content')
<style type="text/css">
.collapsing {
  position: relative;
  height: 0;
  overflow: hidden;
  -webkit-transition-property: height, visibility;
  transition-property: height, visibility;
  -webkit-transition-duration: 1.05s;
  transition-duration: 1.05s;
  -webkit-transition-timing-function: ease;
  transition-timing-function: ease;
}
</style>
	 <!--================ Cao Văn Anh =================-->
    <!--================ Home Banner Area =================-->
    
    <section class="banner">
        <div class ="alt-banner" >
            <div class="banner-overlay"></div>
            <div class="container">
                <div class="banner-content">
                    <h2>{{$coursePage->title}}</h2>
                    <p style="color:#fff;font-weight:bold">{{$coursePage->description}}</p>
                    <a class="primary-btn fix-gr-bg semi-large" href="#">{{$coursePage->button_text}}</a>
                </div><!-- banner-content -->
            </div>
        </div>

    </section>

    <!--================ End Home Banner Area =================-->

    <!--================ Course List Area =================-->
    <section class="academics-area section-gap-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="row">

                        <div class="col-lg-12 col-sm-12">
                            <div class="topnav">
                                <div class="d-flex justify-content-center">
                                    <a href="/home">Home</a><a href="">/ An International Curriculum</a> 
                                </div>
                            </div>
                        </div>
                        <br><br><br>
                        <!-- Internaltional Early Year Curriculum -->
                        <div class="col-lg-12 pb-10">
                            <div id="accordion">
                                <div class="card">

                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" id="btn-early" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                International Early Years Curriculum
                                            </button>
                                        </h5>
                                    </div >

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">		
                                            <div class="early row">
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div id="carouselExampleSlidesOnly" class="carousel slide pt-30" data-ride="carousel">
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <img class="d-block w-100 " src="/public/images/fun02.jpg" alt="First slide">
                                                            </div>
                                                            <div class="carousel-item">
                                                                <img class="d-block w-100 " src="/public/images/fun03.jpg" alt="Second slide">
                                                            </div>
                                                            <div class="carousel-item">
                                                                <img class="d-block w-100 " src="/public/images/fun04.jpg" alt="Third slide">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12" style="height : 400px;overflow-y : scroll;">  
                                                    <div class="topnav">
                                                        <div class="d-flex justify-content-center">
                                                            <a style="color: #FFF;text-align:center" >International Early Years Curriculum</a>
                                                        </div>
                                                    </div>
                                                    <div class="content-course">
                                                    {!!$coursePage->early_description!!}
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of  Internaltional Early Year Curriculum --> 

                        <!-- Internaltional Primary Year Curriculum -->
                        <div class="col-lg-12 pb-10">
                            <div id="accordion">
                                <div class="card">

                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" id="btn-early" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                                International Primary Years Curriculum
                                            </button>
                                        </h5>
                                    </div >

                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">		
                                        <div class="primary row" >
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div id="carouselExampleSlidesOnly" class="carousel slide pt-30" data-ride="carousel">
                                                    <div class="carousel-inner">
                                                        <div class="carousel-item active">
                                                            <img class="d-block w-100 " src="/public/images/fun02.jpg" alt="First slide">
                                                        </div>
                                                        <div class="carousel-item">
                                                            <img class="d-block w-100 " src="/public/images/fun03.jpg" alt="Second slide">
                                                        </div>
                                                        <div class="carousel-item">
                                                            <img class="d-block w-100 " src="/public/images/fun04.jpg" alt="Third slide">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12" style="height : 400px;overflow-y : scroll;">
                                                <div class="topnav">
                                                    <div class="d-flex justify-content-center">
                                                        <a style="color: #FFF" >International Primary Years Curriculum</a>
                                                    </div>
                                                </div>
                                                <div class="content-course">
                                                {!!$coursePage->primary_description!!}
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of  Internaltional Primary Year Curriculum --> 

                         <!-- Internaltional Middle Year Curriculum -->
                         <div class="col-lg-12 pb-10">
                            <div id="accordion">
                                <div class="card">

                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" id="btn-early" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                                                International Middle Years Curriculum
                                            </button>
                                        </h5>
                                    </div >

                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">		
                                            <div class="middle row" >
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div id="carouselExampleSlidesOnly" class="carousel slide pt-30" data-ride="carousel">
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <img class="d-block w-100 " src="/public/images/fun02.jpg" alt="First slide">
                                                            </div>
                                                            <div class="carousel-item">
                                                                <img class="d-block w-100 " src="/public/images/fun03.jpg" alt="Second slide">
                                                            </div>
                                                            <div class="carousel-item">
                                                                <img class="d-block w-100 " src="/public/images/fun04.jpg" alt="Third slide">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12" style="height : 400px;overflow-y : scroll;" >
                                                    <div class="topnav">
                                                        <div class="d-flex justify-content-center">
                                                            <a style="color: #FFF" >International Middle Years Curriculum</a>
                                                        </div>
                                                    </div>
                                                    <div class="content-course">
                                                    {!!$coursePage->middle_description!!}
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of  Internaltional Middle Year Curriculum --> 
                    
                        <div class="col-lg-12">
                            <div class="topnav">
                                <div class="d-flex justify-content-center">
                                    <a style="color: white;">Course List</a>
                                </div>
                            </div>
                            <br>
                        <div>  
                        
                    </div>
                    <div class="row">
                        @foreach($course as $value)
                        <div class="col-lg-4 col-md-6">
                            <div class="academic-item">
                                <div class="academic-img">
                                    <img class="img-fluid" src="{{asset($value->image)}}" alt="">
                                </div>
                                <div class="academic-text">
                                    <h4>
                                        <a style="color:blue;font-size : 25px;" href="{{url('course-Details/'.$value->id)}}" style="font-style: normal">{{$value->title}}</a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="row text-center mt-30">
                <!-- <div class="col-lg-12">
                    <a class="primary-btn fix-gr-bg semi-large" href="#">Load More Courses</a>
                </div> -->
            </div>
        </div>
    </section>
    <!--================ End Course List Area =================-->

    <!--================ News Area =================-->
    <section class="news-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="title" style="color:#007bff;font-family:sans-serif;font-style: normal">Latest News</h3>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom:50px">
                        @foreach($news as $value)
                        <div class="col-lg-4 col-md-6">
                            <div class="news-item">
                                <div class="news-img">
                                    <img class="img-fluid w-100 h-220" src="{{asset($value->image)}}" alt="">
                                </div>
                                <div class="news-text">
                                    <p class="date">                                                                            
                                        {{$value->publish_date != ""? App\SmGeneralSettings::DateConvater($value->publish_date):''}}
                                    </p>
                                    <h4>
                                        <a href="{{url('news-details/'.$value->id)}}">
                                           <p style="font-style: normal;color:blue;font-size : 25px;">{{$value->news_title}}</p> 
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End News Area =================-->
@endsection
