@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
    <link href="/public/frontend/css/course.css" rel="stylesheet">
@endpush
 <!--================ Cao Văn Anh =================-->
    <!--================ Home Banner Area =================-->
    <!-- <section class="container box-1420">
        <div class="banner-area">
            <div class="banner-inner">
                <div class="banner-content">
                    <h2>Course Details</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a class="primary-btn fix-gr-bg semi-large" href="#">Learn More About Us</a>
                </div>
            </div>
        </div>
    </section> -->
    <!--================ End Home Banner Area =================-->

    <!--================ Course Overview Area =================-->
    <img width="100%" src="{{asset('public/')}}/images/banner-du-an.jpeg" alt="">
    <section class="overview-area student-details section-gap-top">
        <div class="container">

            <div class="col-lg-12">
                <br>
                <div class="topnav">
                    <div class="d-flex justify-content-center">
                        <a href="/home">Home / {{$course->title}}</a>
                    </div>
                </div>
                <br>
            <div>

            <div class="d-flex justify-content-center">
                <img class="img-fluid"  src="{{asset($course->image)}}" alt="">
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="d-flex justify-content-center">
                     <div style="display: none;"  class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Dropdown button
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" onclick="removeActive()"  href="#overviewTab" role="tab" data-toggle="tab">Content</a>
                                <a class="dropdown-item" onclick="removeActive()" href="#outlineTab" role="tab" data-toggle="tab">Calendar</a>
                                @foreach($cate_post as $item)
                                <a class="dropdown-item" onclick="removeActive()" href="#id{{$item->id}}" role="tab" data-toggle="tab">{{$item->news_title}}</a>
                                @endforeach
                            </div>
                         </div>  
                        <ul class="nav nav-tabs mb-50 " role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#overviewTab" role="tab" data-toggle="tab">Content</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#outlineTab" role="tab" data-toggle="tab">Calendar</a>
                            </li>
                            @foreach($cate_post as $item)
                                 <li class="nav-item">
                                    <a class="nav-link" href="#id{{$item->id}}" role="tab" data-toggle="tab">{{$item->news_title}}</a>
                                 </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- Start Overview Tab -->
                        <div role="tabpanel" class="tab-pane fade show active" id="overviewTab">
                            <p>
                               {!!$course->overview!!}
                            </p>
                        </div>
                        <!-- End Overview Tab -->

                        <!-- Start Outline Tab -->
                        <div role="tabpanel" class="tab-pane fade" id="outlineTab">
                           <img src="{{asset($course->outline)}}" alt="">
                        </div>
                        <!-- End Outline Tab -->
                        
                        @foreach($cate_post as $item)
                            <div role="tabpanel" class="tab-pane fade" id="id{{$item->id}}">
                                <h1 style="text-align: center; ">{{$item->news_title}}</h1>
                                <p>
                                    {!!$item->news_body!!}
                                </p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Course Overview Area =================-->

    <!--================ Related Course Area =================-->
    <section class="academics-area section-gap-top mt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="title" style="color:#007bff;font-family:sans-serif;font-style: normal">Related Courses</h3>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($courses as $course)
                        <div class="col-lg-4 col-md-6 col-6">
                            <div class="academic-item">
                                <div class="academic-img">
                                    <img class="img-fluid-two" src="{{asset($course->image)}}" alt="">
                                </div>
                                <div class="academic-text">
                                    <h4>
                                        <a href="{{url('course-Details/'.$course->id)}}" style="font-style: normal">{{$course->title}}</a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Related Course Area =================-->
<script>
    function removeActive(){
        for(var i = 0; i <  document.getElementsByClassName('dropdown-item').length; i++){
            document.getElementsByClassName('dropdown-item')[i].className = "dropdown-item";
        }
    }   
</script>
@endsection

