{{-- Tran Thanh Phu --}}
@extends('frontEnd.home.layout.front_master')
@if ($cate_post)
    <title>{{ $cate_post->news_title }}</title>
@endif
@section('main_content')
    @push('css')
        <link rel="stylesheet" type="text/css" href="{{ asset('public/backEnd/css') }}/lightbox.css">
        <link href="{{ asset('public') }}/frontEnd/css/lifeSchool.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
        <link rel="stylesheet" href="{{ asset('public/frontEnd/css/newcss.css') }}" />
    @endpush


    @if ($imagesPhoto)
        <div class="banner">
            <img class="banner" width="100%" src="{{asset('public/images/banner/banner_03.jpg')}}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>{{ $imagesAlbum->title }}</h3>
                </div>
            </div>
        </div>
        @if ($cate_post)
            <div class="catePost c-color">
                <div class="paragraph">
                    <p>{!! $cate_post->news_body !!}</p>
                </div>
            </div>
        @endif
    @else
        <div class="banner">
            <img class="banner" width="100%" src="{{asset('public/images/banner/banner_03.jpg')}}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>{{ $cate_post->news_title }}</h3>
                </div>
            </div>
        </div>
        <div class="catePost c-color">

            <div class="paragraph">
                <p>{!! $cate_post->news_body !!}</p>
            </div>
        </div>
    @endif

@endsection

{{-- Tran Thanh Phu --}}
