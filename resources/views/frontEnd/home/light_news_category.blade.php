@extends('frontEnd.home.layout.front_master')
@section('css')
    <link href="{{asset('public/frontend/css/course.css')}}" rel="stylesheet">
@endsection
@section('main_content')
<img width="100%" src="{{asset('public/')}}/images/banner-du-an.jpeg" alt="">

<section class="container news-lastest">
    
    

<!-- Modal -->

    <h4 class="mt-4 titleNews"><a href="{{url('public/uploads/THE SAIGON JOURNAL_10112020.pdf')}}"target="_blank">THE SAIGON JOURNAL (click to open)</a></h4>
    
    <br>
    <div style="margin-left: 20px" class="col nen">
        <div class="row">
            @if ($hotnews)
                <div class="col-lg-5 item">
                    <a href="{{url('news-details/'.$hotnews->id)}}" title="View Details">
                    <div class="card news-card h-100" >
                        <div class="zoom img-hover-zoom">
                            <img class="card-img-top" src="{{asset($hotnews->image)}}"  alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h5>{{App\SmGeneralSettings::DateConvater($hotnews->publish_date)}}</h5>
                                <h4>{{\Illuminate\Support\Str::limit($hotnews->news_title,70)}}</h4>
                            </div>
                            <br>
                            <div class="card-description">
                                @if (strlen($hotnews->description) > 10)
                                    <p class="card-text">{{\Illuminate\Support\Str::limit($hotnews->description,150)}}</p>
                                @else
                                    <a href="{{url('news-details/'.$hotnews->id)}}">Read More</a>
                                @endif
                            </div>                
                        </div>
                    </div>
                    </a>
                </div>

                <div class="col-lg-7">
                    <div class="row">
                        @foreach($newslist as $news)
                        <div class="col-lg-6 item" >
                            <a href="{{url('news-details/'.$news->id)}}" title="View Details">
                            <div class="card news-card h-100" >
                                <div class="zoom img-hover-zoom">
                                <img class="card-img-top" src="{{asset($news->image)}}" height="200px" width="200px" alt="Card image cap">
                                </div>
                                <div class="card-body">
                                    <div class="card-title">
                                        <h5>{{App\SmGeneralSettings::DateConvater($news->publish_date)}}</h5>
                                        <h4>{{\Illuminate\Support\Str::limit($news->news_title,30)}}</h4>
                                    </div>                      
                                </div>
                                
                            </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
    <br>
    
</section>
<section class="student-details section-gap-top mt-4">
    <div class="d-flex justify-content-center">
        <div style="display: none;"  class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown button
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" onclick="removeActive()"  href="#all" role="tab" data-toggle="tab">All</a>
                @foreach($list_cate as $cate)
                <a class="dropdown-item" onclick="removeActive()" href="#id{{$cate->id}}" role="tab" data-toggle="tab">{{$cate->sub_category_name}}</a>
                @endforeach
            </div>
        </div>
        <ul class="nav nav-tabs mb-3" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#all" role="tab" data-toggle="tab">All</a>
            </li>
            @foreach($list_cate as $cate)
                <li class="nav-item">
                    <a class="nav-link" href="#id{{$cate->id}}" @if($cate->id == 38) id="bullentin" @endif role="tab" data-toggle="tab">{{$cate->sub_category_name}}</a>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="tab-content">
        <!-- Start Overview Tab -->
        <div role="tabpanel" class="tab-pane fade show active" id="all">
            @foreach($list_cate as $cate)
                @php
                    $latest_news = App\SmNews::where('sub_category_id', $cate->id)->orderBy('updated_at', 'desc')->take(3)->get();
                @endphp
                @if (count($latest_news) > 0)
                    <h1 style="text-align:center; color: cadetblue;">{{$cate->sub_category_name}}</h1>
                @endif
                <div class="row" style="width: 90%; margin: auto">
                    @foreach($latest_news as $index=>$lastest)                 
                        <div class="col-lg-4 col-sm-6 col-12 item">
                            <a href="{{url('news-details/'.$lastest->id)}}" title="View Details">
                                <div style="height: 500px; "  class="card news-card" >
                                    <div class="zoom img-hover-zoom">
                                    <img class="card-img-top" src="{{asset($lastest->image)}}" height="auto" width="auto" alt="Card image cap">
                                    </div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h5>{{App\SmGeneralSettings::DateConvater($lastest->publish_date)}}</h5>
                                            <h4>{{\Illuminate\Support\Str::limit($lastest->news_title,70)}}</h4>
                                        </div>      
                                        <br>
                                        <div class="card-description">
                                            @if (strlen($lastest->description) > 10)
                                                <p class="card-text">{{\Illuminate\Support\Str::limit($lastest->description,150)}}</p>
                                            @else
                                                <a href="{{url('news-details/'.$lastest->id)}}">Read More</a>
                                            @endif
                                        </div>    
                                    </div>
                                </div>
                            </a>
                        </div>                                      
                    @endforeach
                </div>
            @endforeach
        </div>

        @foreach($list_cate as $cate)
            <div role="tabpanel" class="tab-pane fade" id="id{{$cate->id}}">
                <div class="row" style="width: 90%; margin: auto">
                    @foreach(App\SmNews::where('sub_category_id', $cate->id)->orderBy('updated_at', 'desc')->get() as $lastest)                 
                        <div class="col-lg-4 col-sm-6 col-12 item">
                            <a href="{{url('news-details/'.$lastest->id)}}" title="View Details">
                                <div style="height: 500px; "  class="card news-card" >
                                    <div class="zoom img-hover-zoom">
                                    <img class="card-img-top" src="{{asset($lastest->image)}}" height="auto" width="auto" alt="Card image cap">
                                    </div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h5>{{App\SmGeneralSettings::DateConvater($lastest->publish_date)}}</h5>
                                            <h4>{{\Illuminate\Support\Str::limit($lastest->news_title,70)}}</h4>
                                        </div>      
                                        <br>
                                        <div class="card-description">
                                            @if (strlen($lastest->description) > 10)
                                                <p class="card-text">{{\Illuminate\Support\Str::limit($lastest->description,150)}}</p>
                                            @else
                                                <a href="{{url('news-details/'.$lastest->id)}}">Read More</a>
                                            @endif
                                        </div>    
                                    </div>
                                </div>
                            </a>
                        </div>                                      
                    @endforeach
                </div>
            </div>
        @endforeach

    </div>
</section>

<script>
    if(window.location.href.indexOf('/bulletin') != -1){
        for(var i = 0; i <  document.getElementsByClassName('nav-link').length; i++){
            document.getElementsByClassName('nav-link')[i].className = document.getElementsByClassName('nav-link')[i].className.replace(" active", "");
        }
        for(var i = 0; i <  document.getElementsByClassName('tab-pane').length; i++){
            document.getElementsByClassName('tab-pane')[i].className = document.getElementsByClassName('tab-pane')[i].className.replace(" show active", "");
        }
        document.getElementById('bullentin').className += " active";
        document.getElementById('id38').className += " show active";
    }
    function removeActive(){
        for(var i = 0; i <  document.getElementsByClassName('dropdown-item').length; i++){
            document.getElementsByClassName('dropdown-item')[i].className = "dropdown-item";
        }
    }   
</script>
@endsection