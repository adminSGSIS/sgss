@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('public/backEnd/css')}}/lightbox.css">
@endpush
@section('script')
<script src="{{asset('public/backEnd/js/')}}/lightbox-plus-jquery.js"></script>
@endsection
@section('main_content')
    <div class="banner">
    	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>campus</h3>
            </div>
    	</div>
    	
    </div>
    <section class="c-color">
        <div class="container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          
          <div class="carousel-inner">
             @foreach($images as $image)
            <div class="carousel-item @if ($loop->first) active @endif">
                <a href="/public/uploads/category/{{$image->path}}" class="d-flex justify-content-center mt-30"data-lightbox="1">
                            <img class="img-slider video_1aa rounded" width="80%" src="/public/uploads/category/{{$image->path}}">
                            
                </a>
              
            </div>
            @endforeach
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick="event.preventDefault();">
                    <span class="fa fa-chevron-left"  aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" onclick="event.preventDefault();">
            <span class="fa fa-chevron-right"  aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    		
    	</div>
                 
    </section>
@endsection