@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
@endpush
@section('main_content')
   
    <div class="banner">
        <img class="banner" width="100%" src="{{asset('public/images/banner/banner_03.jpg')}}" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>an introduction of saigon </br>star international school</h3>
            </div>
        </div>
    </div>
    <div class="main-ourschool">
        
        <div class="content-wc">
            <div class="left">
                <div>
                    <img src="/public/images/left.png" alt="">
                </div>
            </div>
            <p class="col-lg-8 col-sm-8">
            Saigon Star International School is a boutique educational setting with lush and green facilities
            in a quiet area of District 2. We specialise in, and deliver, the UK National Curriculum which is
            supported by the holistic and progressive IPC curriculum. We were the first school in Vietnam
            to be accredited with this and attain categories of 'Mastering' in 2018.</br></br>
            Our amazing, dedicated and caring teachers are fully qualified/licensed. mainly from the JK,
            and carry at least three years experience from their home countries. Supported by our
            professional development opportunities, they model the attributes Of lifelong learning and our
            reflective practices to our student body and in doing so, help us try to develop those 'the ways
            Of thinking' (Academic and International goals) and 'the ways Of being'
            (our Personal Learning Goals).</br></br>
            Our approach to the IPC curriculum, the associated Personal Learning Goals and the overarch-
            inq 'umbrella' Of International Mindedness goes beyond just words on our walls and we aim to
            provide meaningful opportunities, across a plethora of platforms, for our learners to improve
            and showcase their Knowledge, Skills and Understanding in a safe, purposeful and nurturing
            environment. We aim to ensure that whilst we help to foster an intrinsic motivation for learning,
            we also equip our students with the ability to collaborate and cooperate, think cognitively and
            gain strong resilience for life's challenges.</br></br>
            Aside from the academic expectations of our school, we focus strongly on providing creative
            opportunities with the Sciences, Arts and Sports all core to our provision. Our learning
            opportunities for these include STEM, World Languages. Music and Dance, Art and many
            more that foster passion. creativity and enrich young lives.</br></br>
            By supplementing our high-quality education With various community events throughout the
            academic year, our hope is that we celebrate the rich tapestry Of our families, their backgrounds
            and beliefs. and Offer motivating learning Opportunities no matter the age.
            </p>
            <div class="right">
                <img src="/public/images/right.png" alt="">
            </div>
        </div>
    </div>
    
@endsection