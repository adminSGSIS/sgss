@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
@endpush
@section('main_content')
   <div class="banner">
    	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>our vision</h3>
            </div>
    	</div>
    	
    </div>
    <div class="main-ourschool c-color">
        <div class="content-overview d-flex justify-content-center">
            <p class="col-lg-5 col-12">All children are helped to become globally-competent, 
                well-rounded, lifelong learners.
            </p>
        </div>
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>our mission</button>
    </div>
    <div class="main-ourschool c-color">
        <div class="content-overview d-flex justify-content-center">
            <p class="col-lg-5 col-12">We provide motivating and engaging learning experiences,
                within a safe and nurturing environment, that enable every child to experience daily success in learning.
            </p>
        </div>
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    
    <div class="title c-color" style="padding-bottom:5%;">
        <button>philosophy</button>
    </div>
    <section class="py-5 d-flex justify-content-center c-color">
        <img width="55%" src="{{url('public/images/our belief and philosophy-01.png')}}">
    </section>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>school name</button>
    </div>
    <div class="title c-color">
        <h2 class="mb-0">saigon star </br>international school</h2>
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>acronym</button>
    </div>
    <div class="title c-color">
        <h2 class="mb-0">SGSIS</h2>
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>slogan</button>
    </div>
    <div class="title c-color">
        <h4 class="mb-0">Inspired Learners.<br> Global Thinkers.</h4>
        
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>values</button>
    </div>
    <div class="c-color" id="value-web">
        <div class="container d-flex justify-content-center value">
            <img src="/public/images/value.png" alt="">
        </div>
    </div>

    <div class="row c-color pt-10" id="value-mobile">
        <div class="col-4">
            <img src="/public/images/values/ethical.png" alt="">
        </div>
        <div class="col-4">
            <img src="/public/images/values/thinker.png" alt="">
        </div>
        <div class="col-4">
            <img src="/public/images/values/adapt.png" alt="">
        </div>
        <div class="col-4">
            <img src="/public/images/values/empath.png" alt="">
        </div>
        <div class="col-4">
            <img src="/public/images/values/respectful.png" alt="">
        </div>
        <div class="col-4">
            <img src="/public/images/values/resilent.png" alt="">
        </div>
        <div class="col-6">
            <img src="/public/images/values/communi.png" alt="">
        </div>
        <div class="col-6">
            <img src="/public/images/values/collab.png" alt="">
        </div>
    </div>
    
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>History</button>
    </div>
    <div class="c-color" id="value-web">
        <br>
        <div class="container d-flex justify-content-center value ">
            <img src="/public/images/history.png" alt="">
        </div>
    </div>
    <div class="c-color col-12 pt-5" id="history-mobile">
        <div class="col-12 card-year-history">
            <a>2005</a>
        </div>
        <div class="card-history">
            <span class="">
                Saigon Star International School was established in 2005 at
                127 Nguyen Van Thu, Dakao Ward, District 1. The school
                was founded by an Indian businessman who lived and
                worked in Vietnam.
            </span>
        </div>
    </div>
    <div class="c-color col-12 pt-5" id="history-mobile">
        <div class="col-12 card-year-history">
            <a>2008</a>
        </div>
        <div class="card-history">
            <span class="">
            After being established for 3 years, the number of pupils
            grew to include students up to Year 6.
            2011
            </span>
        </div>
    </div>
    <div class="c-color col-12 pt-5" id="history-mobile">
        <div class="col-12 card-year-history">
            <a>2011</a>
        </div>
        <div class="card-history">
            <span class="">
            In October 2010, the school moved to a purpose-built
            facility in Su Hy Nhan Street, Residential Area No. 5, Thanh
            My Loi Ward, D2, HCMC, a major turning point in the
            school's history which created new challenges and
            opportunities for the school's development.
            </span>
        </div>
    </div>
    <div class="c-color col-12 pt-5" id="history-mobile">
        <div class="col-12 card-year-history">
            <a>2013</a>
        </div>
        <div class="card-history">
            <span class="">
            In order to further strengthen the school's
            legitimacy as an international school, Saigon Star adopted
            the International Primary Curriculum (IPC).
            </span>
        </div>
    </div>
    <div class="c-color col-12 pt-5" id="history-mobile">
        <div class="col-12 card-year-history">
            <a>2016</a>
        </div>
        <div class="card-history">
            <span class="">
            Saigon Star became one of the first schools in the world to
            adopt the brand new International Early Years Curriculum
            (IEYC)
            </span>
        </div>
    </div>
    <div class="c-color col-12 pt-5" id="history-mobile">
        <div class="col-12 card-year-history">
            <a>2018</a>
        </div>
        <div class="card-history">
            <span class="">
            Saigon Star International School was honored to be
            recognised as the first and only IPC-accredited school in
            Vietnam, one of only 18 worldwide.
            </span>
        </div>
    </div>
    <div class="c-color col-12 pt-5 pb-5" id="history-mobile">
        <div class="col-12 card-year-history">
            <a>2021</a>
        </div>
        <div class="card-history">
            <span class="">
            SGSIS develops into a boutique educational setting
            with lush & green facilities.
            </span>
        </div>
    </div>
    
@endsection
