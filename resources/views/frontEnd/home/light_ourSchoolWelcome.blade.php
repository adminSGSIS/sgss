@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
@endpush
@section('main_content')
    <div>
        <img style="width:100%" src="/public/images/banner/banner_03.jpg" alt="">
    </div>
    <div class="main-ourschool" style="background: #fafaf4;">
        <img style="margin-top:-25%;" src="/public/images/welcome.png" alt="">
        <div class="content-wc">
            <div class="row welcome-content">
                <div class="col-lg-3 col-sm-12 col-md-3">
                    <img src="/public/images/James.jpg" alt="">
                    <h3>JAMES QUANTRILL</h3>
                    <h4>HEAD OF SCHOOL/HEAD OF PRIMARY</h4>
                </div>
                <div class="col-lg-9 col-sm-12 col-md-9" >
                    <p class="new-text-color">     I would like to extend a heartfelt and sincere welcome to Saigon Star International School
                            a boutique international school with a real sense of family at its heart. 
                            Having been here for five fantastic years, I feel incredibly proud of the journey we have been on together
                            with every single member of our community contributing to our growth, development and improvement, 
                            establishing us as a diverse, inclusive and welcoming school that is fully focussed on learning and enrichment.
                            <br><br>
                            We are proud to be the first and only international school that is accredited with the International Primary Curriculum (IPC) 
                            in Vietnam a curriculum which we believe helps us to achieve our Shared Vision, 
                            embrace our community’s wide diversity and foster a love for lifelong learning. 
                            If you haven’t already, we would encourage you to come and enjoy a visit to our peaceful,
                            green and tranquil surroundings of District 2, HCMC, and see first-hand the tangible development 
                            of our learner’s intrinsic personal values, balanced with their academic goals. Our caring, 
                            supportive and internationally qualified teachers demonstrate their capacity to provide more personalized learning 
                            (our maximum class size of 20 is one of the best learner/teacher ratios in the city) 
                            and a strong commitment to ensuring your child is healthy, happy and learning focused.
                            <br><br>
                            At Saigon Star, we supplement the IEYC (International Early Years Curriculum), 
                            the IPC and the IMYC (International Middle Years Curriculum) with the English National Curriculum. 
                            We provide specialist teachers with expertise in Sports and Swimming, Music, 
                            Performing Arts and Mandarin. All of our teachers arrive fully qualified or licensed, 
                            with at least 3-year experience from their home country. Our aim is to provide all of our teachers with consistent, 
                            high-quality professional development opportunities that positively impact our learners and ensures longevity in our journey together.
                            <br><br>
                            We hope that you will join our fantastic family environment and support us in becoming the strong 
                            foundation for your child on their long educational journey ahead.<br>
                            Thank you for your interest in Saigon Star. I look forward to our strong partnership together.</br></br>
                            Mr. James Quantrill<br>
                            Headteacher
                    </p>
                </div>   
                                 
            </div> 
                       
        </div>
        <!--<div style="padding-bottom: 27%; width:100%">
            <div class="left col-lg-6 col-sm-6 col-md-6" >
                <img  src="/public/images/left.png" alt="" >
            </div> 
            <div class="right col-lg-6 col-sm-6 col-md-6">                  
                <img src="/public/images/right.png" alt="" > 
            </div> 
        </div>-->
    </div>
    <div class="main-ourschool" style="background: #fafaf4;">
        <div style="width:100%" class="botpad">
            <div class="left col-lg-6 col-sm-6 col-md-6" >
                <img  src="/public/images/left.png" alt="" >
            </div> 
            <div class="right col-lg-6 col-sm-6 col-md-6">                  
                <img src="/public/images/right.png" alt="" > 
            </div> 
        </div>
    </div>
@endsection

