@extends('frontEnd.home.layout.front_master')
@section('main_content')
    @push('css')
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontEnd/css/travel_declaration.css') }}">
    @endpush
    <br><br><br><br>
    
    <section class="container mt-5">
        <div class="row title">
            <div class="col-12 text-center">
                <br>
                <h2>GENERAL HEALTH DECLARATION INFORMATION</h2>

                <p>( COVID-19 EPIDEMIC PREVENTION )</p>

                <span class="font-weight-bold">Warning: Declaring false information is a violation of Vietnamese law and may be subject to criminal handling</span>
            </div>
        </div>
        
        <form action="/medical-declaration/domestic" method="POST">
            @csrf
            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">Full name</label>
                    <input type="text" class="form-control" name="full_name" required value="{{ old('full_name') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-sm-12 form-group">
                    <label class="red-star">Date of Birth1</label>
                    <input type="text" autocomplete="off" class="form-control date datepicker" name="date_of_birth" placeholder="dd/mm/yyyy" required value="{{ old('date_of_birth') }}">
                </div>

                <div class="col-lg-4 col-sm-12 form-group">
                    <label class="red-star">Gender</label>
                    <select class="form-control {{ $errors->has('gender') ? 'is-invalid' : '' }}" name="gender">
                        <option data-display="Select" value="">Select</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                    </select>
                </div>

                <div class="col-lg-4 col-sm-12 form-group">
                    <label class="red-star">Nationality</label>
                    <input type="text" class="form-control" name="nationality" required value="{{ old('nationality') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">Passport number / ID card</label>
                    <input type="text" class="form-control" name="id_card" required value="{{ old('id_card') }}">
                </div>
            </div>

            <p class="font-weight-bold">Contact address in VietNam</p>

            <div class="row">
                <div class="col-lg-4  col-sm-12 form-group">
                    <label class="red-star">Province</label>
                    <input type="text" class="form-control" name="province" required value="{{ old('province') }}">
                </div>

                <div class="col-lg-4  col-sm-12form-group">
                    <label class="red-star">District</label>
                    <input type="text" class="form-control" name="district" required value="{{ old('district') }}">
                </div>

                <div class="col-lg-4  col-sm-12 form-group">
                    <label class="red-star">Ward</label>
                    <input type="text" class="form-control" name="ward" required value="{{ old('ward') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">Address</label>
                    <input type="text" class="form-control" name="address" required value="{{ old('address') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6  col-sm-12 form-group">
                    <label class="red-star">Tel./Mob</label>
                    <input type="text" class="form-control" name="phone_number" required value="{{ old('phone_number') }}">
                </div>

                <div class="col-lg-6  col-sm-12 form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star font-weight-bold">In the last 14 days, which regions/ countries/ territories have you traveled to (may travel across multiple places)</label>
                    <textarea class="form-control" name="traveled" rows="3"></textarea>
                </div>
            </div>

            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Symptoms</th>
                        <th scope="col">Yes</th>
                        <th scope="col">No</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="red-star">Fever</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_1" value="yes" required></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_1" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Cough</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_2" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_2" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Shortness of breath</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_3" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_3" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Pneumonia</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_4" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_4" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Sore throat</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_5" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_5" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Tired</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_6" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_6" value="no" checked></td>
                    </tr>
                </tbody>
            </table>
            
            <p class="font-weight-bold">During the past 14 days, you were in contact with</p>

            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Yes</th>
                        <th scope="col">No</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="red-star">Sick or suspected person, infected with COVID-19</td>
                        <td><input class="form-check-input" type="radio" name="contact_with_1" value="yes" ></td>
                        <td><input class="form-check-input" type="radio" name="contact_with_1" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">People from countries with COVID-19 disease</td>
                        <td><input class="form-check-input" type="radio" name="contact_with_2" value="yes" ></td>
                        <td><input class="form-check-input" type="radio" name="contact_with_2" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">People with manifestations (fever, cough, shortness of breath, pneumonia)</td>
                        <td><input class="form-check-input" type="radio" name="contact_with_3" value="yes" ></td>
                        <td><input class="form-check-input" type="radio" name="contact_with_3" value="no" checked></td>
                    </tr>
                </tbody>
            </table>

            <p class="font-weight-bold">Which of the following diseases do you currently have?</p>

            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Name of illness</th>
                        <th scope="col">Yes</th>
                        <th scope="col">No</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="red-star">Chronic liver disease</td>
                        <td><input class="form-check-input" type="radio" name="diseases_1" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_1" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Chronic blood disease</td>
                        <td><input class="form-check-input" type="radio" name="diseases_2" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_2" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Chronic lung disease</td>
                        <td><input class="form-check-input" type="radio" name="diseases_3" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_3" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Chronic kidney disease</td>
                        <td><input class="form-check-input" type="radio" name="diseases_4" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_4" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Heart-related diseaes</td>
                        <td><input class="form-check-input" type="radio" name="diseases_5" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_5" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">High Blood Pressure</td>
                        <td><input class="form-check-input" type="radio" name="diseases_6" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_6" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">HIV</td>
                        <td><input class="form-check-input" type="radio" name="diseases_7" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_7" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Transplant recipients, Mercury bone</td>
                        <td><input class="form-check-input" type="radio" name="diseases_8" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_8" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Diabetes</td>
                        <td><input class="form-check-input" type="radio" name="diseases_9" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_9" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Cancer</td>
                        <td><input class="form-check-input" type="radio" name="diseases_10" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="diseases_10" value="no" checked></td>
                    </tr>
                </tbody>
            </table>

            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Yes</th>
                        <th scope="col">No</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="red-star">Are you pregnant?</td>
                        <td><input class="form-check-input" type="radio" name="pregnant" value="yes" ></td>
                        <td><input class="form-check-input" type="radio" name="pregnant" value="no" checked></td>
                    </tr>
                </tbody>
            </table>
            
            <div class="component_4 mt-4">
                <p  class="red-star">Signature</p>
                <div class="row">
                    <div class="col-md-8">
                        <canvas id="sig-canvas" width="620" height="160">
                            
                        </canvas>
                    </div>
                    <div class="col-md-4">
                        <img id="sig_img" src="" alt="">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-outline-danger btn-sm" id="sig-clearBtn">Reset</button>
                        <button type="button" class="btn btn-outline-success btn-sm" id="sig-submitBtn">Accept</button>
                    </div>
                </div>
                <br />
                <input required name="signature" id="sig-dataUrl">
            </div>
            <div class="text-justify">
                <label>How do I create an electronic signature?</label>
                <p>-Draw your signature using your finger or a stylus. If you have access to a touchscreen, you can use your
                    finger to create an electronic
                    signature directly in your document. This is particularly helpful for when you’re signing on a mobile
                    device or tablet!
                </p>
                <p>-Use your cursor to draw your signature. Using your mouse or your touchpad, you can drag your cursor
                    along the signature line to create
                    a unique electronic signature.</p>

                </ul>
            </div>

            <div class="text-center mt-4">
                <button class="primary-btn fix-gr-bg">
                    <span class="ti-check"></span>
                    Save
                </button>
            </div>
        </form>
    </section>
@endsection

@section('script')
    <script src="{{ asset('public/js/travel_declaration.js') }}"></script>
    <script>
        $('.datepicker').datepicker({format: 'dd-mm-yyyy' }).val();
    </script>
@endsection
