@extends('frontEnd.home.layout.front_master')
@section('main_content')
    @push('css')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/frontEnd/css/travel_declaration.css') }}">
    @endpush
    <br><br><br><br>
    
    <section class="container mt-5">
        <div class="row title">
            <div class="col-12 text-center">
                <br>
                <h2>MEDICAL DECLARATION</h2>

                <p>THIS IS IMPORTANT DOCUMENT, YOUR INFORMATION IS VITAL TO ALLOW HEALTH AUTHORITIES CONTACT YOU TO PREVENT
                    COMMUNICABLE DISEASES</p>

                <span class="font-weight-bold">Warning: Declaring false information is a violation of Vietnamese law and may be subject to criminal
                    handling</span>
            </div>
        </div>
        
        <form action="/medical-declaration/entry" method="POST">
            @csrf
            <div class="row">
                <div class="form-group col-12">
                    <i class="bi bi-alarm-fill"></i>
                    <label class="red-star">Gate</label>
                    <input type="text" class="form-control" name="gate" required value="{{ old('gate') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">Full name</label>
                    <input type="text" class="form-control" name="full_name" required value="{{ old('full_name') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-sm-12 form-group">
                    <label class="red-star">Date of Birth</label>
                    <input type="text" class="form-control date datepicker" placeholder="dd-mm-yyyy" name="date_of_birth" 
                    required value="{{ old('date_of_birth') }}" autocomplete="off">
                </div>

                <div class="col-lg-4 col-sm-12 form-group">
                    <label class="red-star">Gender</label>
                    <select class="form-control {{ $errors->has('gender') ? 'is-invalid' : '' }}" name="gender">
                        <option data-display="Select" value="">Select</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                    </select>
                </div>

                <div class="col-lg-4 col-sm-12 form-group">
                    <label class="red-star">Nationality</label>
                    <input type="text" class="form-control" name="nationality" required value="{{ old('nationality') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">Passport number or other legal document</label>
                    <input type="text" class="form-control" name="passport" required value="{{ old('passport') }}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">Travel information (Car, Ship, Plane, ...)</label>
                    <input type="text" class="form-control" name="travel_information" required value="{{ old('travel_information') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-sm-12 form-group">
                    <label>Transportation No</label>
                    <input type="text" class="form-control" name="transportation_no" value="{{ old('transportation_no') }}">
                </div>

                <div class="col-lg-6 col-sm-12 form-group">
                    <label>Seat No</label>
                    <input type="text" class="form-control" name="seat_no" value="{{ old('seat_no') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-sm-12 form-group">
                    <label class="red-star">Departure date</label>
                    <input type="text" class="form-control datepicker" name="departure_date" autocomplete="off" required value="{{ old('departure_date') }}">
                </div>

                <div class="col-lg-6 col-sm-12 form-group">
                    <label class="red-star">Immigration date</label>
                    <input type="text" class="form-control datepicker" name="immigration_date" autocomplete="off" required value="{{ old('immigration_date') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-sm-12 form-group">
                    <label class="red-star">Place of departure (Country)</label>
                    <input type="text" class="form-control " name="departure_country"  required value="{{ old('departure_country') }}">
                </div>

                <div class="col-lg-6 col-sm-12 form-group">
                    <label class="red-star">Place of departure (Province)</label>
                    <input type="text" class="form-control" name="departure_province" required value="{{ old('departure_province') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-sm-12 form-group">
                    <label class="red-star">Place of destination (Country)</label>
                    <input type="text" class="form-control" name="destination_country" required value="{{ old('destination_country') }}">
                </div>

                <div class="col-lg-6 col-sm-12 form-group">
                    <label class="red-star">Place of destination (Province)</label>
                    <input type="text" class="form-control" name="destination_province" required value="{{ old('destination_province') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">In the past 14 days, which province / city / territory / country have you been to?</label>
                    <input type="text" class="form-control" name="past_14_days" required value="{{ old('past_14_days') }}">
                </div>
            </div>

            <p class="font-weight-bold">Select a property after quarantine</p>

            <div class="row">
                <div class="col-lg-4  col-sm-12 form-group">
                    <label class="red-star">Province</label>
                    <input type="text" class="form-control" name="after_quarantine_province" required value="{{ old('after_quarantine_province') }}">
                </div>

                <div class="col-lg-4  col-sm-12 form-group">
                    <label class="red-star">District</label>
                    <input type="text" class="form-control" name="after_quarantine_district" required value="{{ old('after_quarantine_district') }}">
                </div>

                <div class="col-lg-4  col-sm-12 form-group">
                    <label class="red-star">Ward</label>
                    <input type="text" class="form-control" name="after_quarantine_ward" required value="{{ old('after_quarantine_ward') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">Quarantine place</label>
                    <input type="text" name="after_quarantine_place" class="form-control" required value="{{ old('after_quarantine_place') }}">
                </div>
            </div>
        

            <p class="font-weight-bold">Contact information in VietNam</p>

            <div class="row">
                <div class="col-lg-4  col-sm-12 form-group">
                    <label class="red-star">Province</label>
                    <input type="text" class="form-control" name="information_in_vn_province" required value="{{ old('information_in_vn_province') }}">
                </div>

                <div class="col-lg-4  col-sm-12form-group">
                    <label class="red-star">District</label>
                    <input type="text" class="form-control" name="information_in_vn_district" required value="{{ old('information_in_vn_district') }}">
                </div>

                <div class="col-lg-4  col-sm-12 form-group">
                    <label class="red-star">Ward</label>
                    <input type="text" class="form-control" name="information_in_vn_ward" required value="{{ old('information_in_vn_ward') }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-12">
                    <label class="red-star">Staying address</label>
                    <input type="text" class="form-control" name="information_in_vn_staying_address" required value="{{ old('information_in_vn_staying_address') }}">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6  col-sm-12 form-group">
                    <label class="red-star">Tel./Mob</label>
                    <input type="text" class="form-control" name="phone_number" required value="{{ old('phone_number') }}">
                </div>

                <div class="col-lg-6  col-sm-12 form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>
            </div>

          

            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Symptoms</th>
                        <th scope="col">Yes</th>
                        <th scope="col">No</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="red-star">Fever</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_1" value="yes" required></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_1" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Cough</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_2" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_2" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Difficulty of breathing</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_3" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_3" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Sore throat</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_4" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_4" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Vomiting</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_5" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_5" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Diarrhea</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_6" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_6" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Skin haemorrhage</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_7" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_7" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Rash</td>
                        <td><input class="form-check-input" type="radio" name="symptoms_8" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="symptoms_8" value="no" checked></td>
                    </tr>
                </tbody>
            </table>

            <div class="form-group">
                <label>List of vaccines or biologicals used</label>
                <input type="text" class="form-control" name="vaccines" value="{{ old('vaccines') }}">
            </div>
            
            <p class="font-weight-bold">History of exposure: During the last 14 days, did you</p>

            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Yes</th>
                        <th scope="col">No</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="red-star">Visit any poultry farm / living animal market / slaughter house / contact to animal</td>
                        <td><input class="form-check-input" type="radio" name="history_of_exposure_1" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="history_of_exposure_1" value="no" checked></td>
                    </tr>
                    <tr>
                        <td class="red-star">Care for a sick person of communicables diseases</td>
                        <td><input class="form-check-input" type="radio" name="history_of_exposure_2" value="yes"></td>
                        <td><input class="form-check-input" type="radio" name="history_of_exposure_2" value="no" checked></td>
                    </tr>
                </tbody>
            </table>
            
            <div class="component_4 mt-4">
                <p  class="red-star">Signature</p>
                <div class="row">
                    <div class="col-md-8">
                        <canvas id="sig-canvas" width="620" height="160">
                            
                        </canvas>
                    </div>
                    <div class="col-md-4">
                        <img id="sig_img" src="" alt="">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-outline-danger btn-sm" id="sig-clearBtn">Reset</button>
                        <button type="button" class="btn btn-outline-success btn-sm" id="sig-submitBtn">Accept</button>
                    </div>
                </div>
                <br />
                <input required name="signature" id="sig-dataUrl">
            </div>
            <div class="text-justify">
                <label>How do I create an electronic signature?</label>
                <p>-Draw your signature using your finger or a stylus. If you have access to a touchscreen, you can use your
                    finger to create an electronic
                    signature directly in your document. This is particularly helpful for when you’re signing on a mobile
                    device or tablet!
                </p>
                <p>-Use your cursor to draw your signature. Using your mouse or your touchpad, you can drag your cursor
                    along the signature line to create
                    a unique electronic signature.</p>

                </ul>
            </div>

            <div class="text-center mt-4">
                <button class="primary-btn fix-gr-bg">
                    <span class="ti-check"></span>
                    Save
                </button>
            </div>
        </form>
    </section>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <!--<script>-->
    <!--    $(document).ready(function () {-->
    <!--        $(".date").flatpickr();-->
    <!--        $(".date").removeAttr("readonly");-->
    <!--    });-->
    <!--</script>-->
    <script>
        $('.datepicker').datepicker({format: 'dd-mm-yyyy' }).val();
    </script>
    <script src="{{ asset('public/js/travel_declaration.js') }}"></script>
@endsection
