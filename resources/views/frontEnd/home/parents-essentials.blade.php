@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
<link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
    <link rel="stylesheet" href="{{url('public')}}/frontEnd/css/newcss.css"/> 
@endpush

<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>parent essentials</h3>
        </div>
	</div>
</div>
<div class="main_content" style="background:#fafaf4">
    <div class="parents-essentials curriculum-page row">
		<div class="col-lg-4 col-md-12 col-sm-12 sub-menu">          
			<a href="/curriculum" style="border-bottom: 2px solid  #7dd3f7">Table of contents</a></br>
			<a class="pl-2" href="#1">Health policy</a></br>
		    <a class="pl-2" href="#2">Important Health Notice</a></br>
		    <a class="pl-2" href="#3">Sickness Policy</a></br>
		    <a class="pl-2" href="#4">Uniform</a></br>
		    <a class="pl-2" href="#5">P.E Attire</a></br>
		    <a class="pl-2" href="#6">Parent Communication</a></br>
		    <a class="pl-2" href="#7">Home-Learning</a></br>
		    <a class="pl-2" href="#8">How To Support Learning At Home</a>
		</div>  
		<div class="col-lg-8 col-md-12 col-sm-12 content-parent">
			<div id="1">
				<div >
					<h3>1. Health policy</h3>
					<p class="new-text-color">
						If a student becomes unwell at school, they will be accompanied to the school clinic room 
						(next to the Headteacher’s room). The School Nurse (or a nominated member of staff in the 
						case of their unavailability) will evaluate the student’s medical condition and make a
						decision about whether the student should go home. If necessary, the school will contact 
						the student’s parents to collect the child. If a student is sick, they should stay home for 
						a period of 48 hours in order to prevent the spread of the illness to others.</br>

						In emergencies, the school will transport the child to an accident and emergency hospital
						immediately.</p>
				</div>
			</div>
		     <div id="2">
		         <h3>2. Important Health Notice</h3>
		         <p class="new-text-color">This year we have members of our school community who have a severe allergy to NATURAL
		            RUBBER LATEX. This allergy can result in a life-threatening reaction known as ANAPHYLAXIS.</br>
                    Anaphylaxis is a serious allergic reaction that is rapid in onset and if left untreated,
                    will result in death. It typically causes more than one of the following: an itchy rash, 
                    throat or tongue swelling, shortness of breath, vomiting, lightheadedness, and low blood pressure. 
                    These symptoms typically come on over minutes to hours.</br>
                    For this reason, we are asking all students to refrain from bringing any products containing latex
                    into school. In a school environment. Latex is most likely going to be found in balloons, 
                    rubber gloves and other toys/articles containing rubber.</br>
                    I am sure you will all understand our requirement to safeguard staff, as well as pupils, and 
                    ask that these items are not brought to school in the future. Thank you for your cooperation.</p>
		     </div>
		     <div id="3">
		         <h3>3. Sickness Policy</h3>
		         <p class="new-text-color">If your child is unwell, please keep them at home to aid a fast and full recovery.
		            School is not the best place for a sick child. Pupils with a contagious illness must 
		            be kept at home until the contagious stage has passed. A medical certificate (not to be 
		            confused with a prescription for medication) which declares your child fit and healthy 
		            to return to school, must be provided (see below).</br>
		            If your child will be absent from school due to sickness, parents are asked to inform 
		            the bus supervisor at least 15 minutes before your child’s pick-up time.</br>
		            If your child does not use the bus service, parents are asked to phone the school office 
		            (+84 8 8800 6996) between 8:15-8.45 am to provide a reason for your child’s absence and 
		            provide an anticipated length of absence. If you are unable to contact someone on the 
		            first attempt, please keep trying.
		            </p>
		     </div>
		     <div id="4">
		        <h3>4. Uniform</h3>
		        <p class="new-text-color">Children are required to come to school in a name-labelled school uniform, which is available 
		            from the school office. Students should also wear footwear with closed-toes, that fit securely
		            and allow children to move freely and safely.</br>
		            Elsewhere, please ensure your child has a hat in school each day. We enforce a strict ‘no hat,
		            no play’ rule. We also recommend that students apply/bring anti-mosquito spray/lotion every day.</br>
                    At key times, pupils will be required to wear a hygiene mask to avoid cross contamination of 
                    communicable diseases.
                </p>
		     </div>
		     <div id="5">
		        <h3>5. P.E. Attire</h3>
		        <p class="new-text-color">If your child has P.E. in the morning, we recommend that they wear their land-based Physical 
		            Education uniform to school and change into their regular uniform afterwards.</br>
                    If your child has land based P.E. in the afternoon, we recommend that children wear 
                    their regular uniform to school in the morning and change into their Physical Education 
                    uniform at lunchtime.</br>
                    A complete swimming kit consists of: swimming costume, towel, goggles and a swimming cap.
                    All these items are compulsory.</br>
                    In preparation for swimming classes, students aged 4+ and above should be able to dress 
                    themselves independently. As a stepping stone to this, students in Nursery should be encouraged
                    to put shoes and hats on independently. Parents’ support with this at home is kindly appreciated.
                </p>
		     </div>
		     <div id="6">
		         <h3>6. Parent Communication</h3>
		         <p class="new-text-color">At Saigon Star, we truly value the important role that parents and carers play in their child’s 
		            education. As such, we strive to develop a close-working partnership through high-quality, 
		            timely communications.</br>
		            Daily - Seesaw is a communication tool that enables parents to view photos and videos of children’s 
		            learning on a regular basis. Parents can expect at least 3 posts per week and a maximum of TWO posts per day.</br>
		            Weekly - Parents receive an email every Friday afternoon between 1-4pm with a brief summary of upcoming events,
		            reminders, and information about what children will be learning the following week.</br>
		            Per Topic - At the beginning of each new IPC topic, class teachers will send home an overview.
		            This will also include a home-learning menu that will last for the duration of the topic.</br>
		            Periodically - As and when changes and improvements are made to the school curriculum, 
		            these are shared with parents via a bulletin on the school’s website: https://www.sgstar.edu.vn/bulletin</br>
		            Events - Saigon Star organises various opportunities for parents to visit school throughout the school year.
		            These include but are not limited to: parent workshops, learning showcases, and celebration days e.g. Mid-Autumn Festival.
		         </p>
		     </div>
		     <div id="7">
		         <h3>7. Home-Learning</h3>
		         <p class="new-text-color">The following suggestions are offered as guidance to teachers and parents.</p>
		         </br>
				 <div class="table-responsive">
		         <table class="table table-bordered tab new-text-color">
                      <thead >
                            <tr  class="table-primary">
                              
                              <th scope="col">Age Group</th>
                              <th scope="col">Recommended Amount</th>
                              <th scope="col">Suggested Activities</th>
                            </tr>
                      </thead>
                      <tbody >
                        <tr>
                          
                          <td> Milepost 1</br>(Year 1 & 2)</td>
                          <td>10-20 minutes per</br> night</td>
                          <td>  
                                <ul style="text-align:left">
                                    <li>Reading to an adult</li>
                                    <li>Spelling</li>
                                    <li>IXL</li>
                                    <li>IPC Home Learning Menu tasks</li>
                                </ul>
                          </td>
                        </tr>
                        <tr>
                        
                          <td>Milepost 2</br>(Year 3 & 4)</td>
                          <td>20-25 minutes per</br> night</td>
                          <td>
                               <ul style="text-align:left">
                                    <li>Reading (independent reading and reading to an adult)</li>
                                    <li>Spelling</li>
                                    <li>IXL</li>
                                    <li>Times Tables</li>
                                    <li>IPC Home Learning Menu tasks</li>
                                    <li>Any additional English/Maths/World Languages allocated by the teacher</li>
                                </ul>
                          </td>
                        </tr>
                        <tr>
                          <td>Milepost 3</br>(Year 5 & 6)</th>
                          <td>30 minutes per</br> night</td>
                          <td>
                              <ul style="text-align:left">
                                    <li>Reading (independent reading and reading to an adult)</li>
                                    <li>Spelling</li>
                                    <li>IXL</li>
                                    <li>Times Tables</li>
                                    <li>IPC Home Learning Menu tasks</li>
                                    <li>Any additional English/Maths/World Languages allocated by the teacher</li>
                                </ul>
                          </td>
                        </tr>
                      </tbody>
                </table>
				 </div>
		     </div>
		     <div id="8">
		         <h3>8. How to Support Learning at Home</h3>
		         <p class="new-text-color">The following provide useful guidance and activities for learning at home.</br>
		            Show-me-Tell-me-Talk-to-me
                    The ten things you can say to your child
                    Understand-your-place-in-the-world
                    Parents Guide How To Help Your Child Read Using-Phonics (for parents with children in Year 1 & 2)</br></br>
                    <a class="link-blank" target="_blank" href="{{ asset('public/tailieu/Parents_ Handbook.pdf') }}">Parent's Handbook</a><br>
                    <a class="link-blank" target="_blank" href="{{ asset('public/tailieu/JueEs1y_Early_Years_PLG_Menu.pdf') }}">Early Years PLG Menu</a><br>
                    <a class="link-blank" target="_blank" href="{{ asset('public/tailieu/cDHL90a_Milepost_1_PLG_Menu.pdf') }}">Milepost_1_PLG_Menu</br>
                    <a class="link-blank" target="_blank" href="{{ asset('public/tailieu/QaSAxZD_Milepost_2_PLG_Menu.pdf') }}">Milepost_2_PLG_Menu</br>
                    <a class="link-blank" target="_blank" href="{{ asset('public/tailieu/Eo7PGKP_Milepost_3_PLG_Menu_.pdf') }}">Milepost_3_PLG_Menu</br>
                </p>
		     </div>
		</div> 
	</div>
</div>
@endsection
@section('script')
    <script>
		$("a").on('click', function(e) {
			$("HTML, BODY").animate({ scrollTop: $($(this).attr('href')).offset().top - 80}, 1000);
		});
    </script>
@endsection