@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
<link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
    <link rel="stylesheet" href="{{url('public')}}/frontEnd/css/newcss.css"/> 
@endpush

<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>parent's handbook</h3>
        </div>
	</div>
	
	
</div>
<div class="main_content" style="background:#fafaf4">
    <div class="container parents-essentials">
		<div class="col-lg-3 col-md-12 col-sm-12 sub-menu-parent">
		    <h3>Table of contents</h3>
		    <a href="#1">1.Learning</a></br>
		    <a href="#2">2.School Calendar</a></br>
		    <a href="#3">3.Parent-Teacher Conferences</a></br>
		    <a href="#4">4.Visiting School & Campus Security</a></br>
		    <a href="#5">5.Safeguarding Concerns</a></br>
		    <a href="#6">6.Morning Supervision</a></br>
		    <a href="#7">7.Attendance</a></br>
		    <a href="#8">8.Late Arrival</a></br>
		    <a href="#9">9.After School Collection</a></br>
		    <a href="#10">10.Early Dismissal</a></br>
		    <a href="#11">11.School Transport</a></br>
		    <a href="#12">12.School Lunch & Snacks</a></br>
		    <a href="#13">13.Uniform</a></br>
		    <a href="#14">14.P.E. Attire</a></br>
		    <a href="#15">15.Stationery</a></br>
		    <a href="#16">16.Electronic Communication Devices</a></br>
		    <a href="#17">17.Lost and Found</a></br>
		    <a href="#18">18.Communication</a></br>
		    <a href="#19">19.Personal Information</a></br>
		    <a href="#20">20.Medical Information</a></br>
		    <a href="#21">21.Important Health Notice</a></br>
		    <a href="#22">22.Educational Trips</a></br>
		    <a href="#23">23.Birthday Celebrations</a></br>
		    <a href="#24">24.Parent Teacher Group (PTG)</a></br>
		    <a href="#25">25.Parent What’sApp Group</a></br>
		    <a href="#26">26.Library Agreement (Primary and Middle Years parents only) </a></br>
		    <a href="#27">27.Book Care Rules: </a></br>
		    <a href="#28">28.Library Policies: </a></br>
		    <a href="#29">29.Parent/Carer Code of Conduct</a></br>
		    <a href="#30">30.Behaviour that is expected:</a></br>
		    <a href="#31">31.If parents have any concerns about their child in relation to the points described above, they should:</a></br>
		    <a href="#32">32.Behaviour that will not be tolerated:</a></br>
		    <a href="#33">33.What happens if someone ignores or breaks the code?</a></br>
		    <a href="#34">34.Online activity which we consider inappropriate:</a></br>
		    <a href="#35">35.PARENT HANDBOOK DECLARATION</a></br>
		    
		</div>    
		<div class="col-lg-9 col-md-12 col-sm-12 content-parent">
		     <div id="1">
		         <h3>1.Learning</h3>
		         <p>Parents are expected to support the learning in school in numerous ways;</p>
                 <ul>
                     <li>Giving positive feedback to pupils is more effective than negative, with regard to learning.</li>
                     <li>Pupils should arrive in class, well fed, not tired and dressed smartly.</li>
                     <li>Develop in children, a respectful attitude towards others</li>
                     <li>Give encouragement and practical support  to undertake Home Learning tasks.</li>
                     <li>Support learning in class by giving children a quiet, calm place in which to read and study.</li>
                     <li>Develop stress-free conditions to complete Home-Learning tasks, such as practising multiplication tables.</li>
                     <li>Share books with children and monitor their daily reading tasks at home.</li>
                 </ul>
		     </div>
		     <div id="2">
		         <h3>2.School Calendar</h3>
		         <p>The annual school calendar is available in the Parent Essentials section of the school website. 
		         This is typically published in January for the following academic year.</p>
		     </div>
		     <div id="3">
		         <h3>3.Parent-Teacher Conferences</h3>
		         <p>Conferences take place three times a year. Parents are able to sign up for a 10 minute meeting, 
		         however, should you wish to speak with your child’s teacher at any other time, we have an open door policy, 
		         therefore, please contact them directly to schedule an appointment. 
		            </p>
		     </div>
		     <div id="4">
		        <h3>4.Visiting School & Campus Security</h3>
		        <p>The school campus has two entrances, which are both manned by security at all times. Parents are to enter
		        the campus using the gate opposite of the park.</br>
		        If visiting during the school day, parents are discouraged from observing classes from the corridor and/or
		        entering classrooms during lessons to ensure that both learners and teachers remain 100% focused.</br>
		        All visitors are required to sign in and out by security and are required to wear a visitor badge at 
		        all times. These must be returned to the security office upon your departure. </br>
		        All adults must use the disabled toilet located on the ground floor corridor of the main building.
		        Parents and other visitors are not permitted to enter student toilets at any time.</br>
		        Visitors are welcome to take photographs of the school during their visit, however, photographs may
		        not be taken of individual children other than your own. Group photographs are permitted but must 
		        not be posted on social media.</br>
		        
                </p>
		     </div>
		     <div id="5">
		        <h3>5.Safeguarding Concerns</h3>
		        <p>If you are worried about a pupil, please make a written record of your concern (including the day, time,
		        concern and your signature) and contact our Designated Safety Lead, Mr. James (below).</br>
                </p>
		     </div>
		     <div id="6">
		         <h3>6.Morning Supervision</h3>
		         <p>Playground supervision begins once the first bus supervisor arrives, which is expected to be around 8am.
		         For any parents who drop their children off at school, we kindly ask you to wait with your child outside the
		         school gates until the first bus arrives. Parents must not leave children unattended.
		         </p>
		     </div>
		     <div id="7">
		         <h3>7.Attendance</h3>
		         <p>Parents are kindly asked to ensure students maintain a good attendance. The expected level is between 96-98%.
		         Above this is considered ‘exceeding expectations’. Below 96% is considered as ‘requires improvement’.</br>
		         We request that families plan any holidays with respect to the school calendar. As much as possible, 
		         parents are also asked to schedule doctor, dentist and other appointments either after school hours,
		         or during school holidays.
		         </p>
		         </br>
		       
		     </div>
		     <div id="8">
		         <h3>8.Late Arrival</h3>
		         <p>If a student arrives late and a parent or carer is present, they should take the child to the classroom
		         and pupils should report directly to the class teacher before joining the class.
                </p>
		     </div>
		     <div id="9">
		         <h3>9.After School Collection</h3>
		         <p>The school day finishes at 3pm, or 4pm if your child takes part in the ECAs. If you are collecting your 
		         child from school, we require all parents to wait outside the Primary library on the ground floor of the main
		         building rather than their child’s classroom so that learning can continue and any end-of-day tasks can be 
		         completed without distraction. </br>
		         Students should be picked up on time everyday. If you know you are going to be late, please inform the school 
		         in advance by calling: (+84) 8 8800 6996.</br>
		         If your child is to be collected by someone who is not known to the school, the school office must be notified 
		         a minimum of four hours ahead of time, and a photo of the new person must be shared in advance so that we can 
		         ensure that your child goes home with the correct person.
                </p>
		     </div>
		     <div id="10">
		         <h3>10.Early Dismissal</h3>
		         <p>If for any reason you need to collect your child before the end of the school day, we kindly ask that you
		         inform your child’s class teacher at least one day before. Otherwise, please call a member of staff in the 
		         school office, who will pass the message on.
                </p>
		     </div>
		     <div id="11">
		         <h3>11.School Transport</h3>
		         <ul>
		             <li>Students should be at the pick-up point in good time in order to respect other users of the bus service.</li>
		             <li>If a student does not arrive within 5 minutes of their designated pick-up time, please understand that the 
		             bus will need to continue on its route in order to arrive at school on time. If a child misses the school bus, 
		             parents will become responsible for organising alternative transportation to school on that day.</li>
		             <li>Students must be in the company of an adult while waiting for the morning bus and must be met at the bus
		             stop in the afternoon by a parent/guardian. If a designated adult is not present, please note that your 
		             child will be returned to school. For the safety of all children, this includes adults whom we do not recognise. </li>
		             <li>For any parent who wishes their child to make their own way home from the drop-off point, 
		             the school must be informed in writing.</li>
		             <li>Children wishing to take a different bus than usual e.g. to visit a friend after school, may do so only if
		             space is available and permission has been granted ahead of time. A request to the office should be made at 
		             least one day in advance.</li>
		             <li>In the interest of positive behaviour and safety, all students are expected to follow the bus rules. Students 
		             who are not respectful of the bus rules will be warned first and will, if necessary, be spoken to by their class
		             teacher and/or the Headteacher.</li>
		             <li>Students who visit the Headteacher three times due to inappropriate behaviour on the bus will no longer be 
		             permitted to use school transport</li>
		             <li>Whilst we do our very best to ensure buses arrive on time, at times, traffic in Ho Chi Minh can be very 
		             heavy and congested and cause delays. For this reason, we ask that parents remain patient, especially with
		             bus supervisors.</li>
		         </ul>
		     </div>
		     <div id="12">
		         <h3>12.School Lunch & Snacks</h3>
		         <p>Students have a choice of bringing their own food or having lunch supplied by the school (ordered in advance 
		            for the whole term). A menu will be sent out to parents prior to each week. 
                    For children bringing food from home, we encourage students to eat healthy, balanced lunches that should 
                    include fruits or vegetables. Students should not be sent to school with chocolates, fizzy drinks, sweets or candies.</br>
                    The school will provide your child with a healthy snack mid-morning and mid-afternoon. Students should also have a reusable 
                    and refillable water bottle at all times, which needs cleaning every evening to prevent mould. Water is available 
                    from the water bottles situated throughout the school. Lunch boxes should be labelled clearly with your child’s 
                    name and class.</br>
                </p>
		     </div><div id="13">
		         <h3>13.Uniform</h3>
		         <p>Children are required to come to school in a name-labelled school uniform, which is available 
		         from the school office. Students should also wear footwear with closed-toes, that fit securely 
		         and allow children to move freely and safely</br>
		         Elsewhere, please ensure your child has a hat in school each day. We enforce a strict 
		         ‘no hat, no play’ rule. We also recommend that students apply/bring anti-mosquito spray/lotion every day. </br>
		         At key times, pupils will be required to wear a hygiene mask to avoid cross contamination of communicable diseases.
                </p>
		     </div>
		     <div id="14">
		         <h3>14.P.E. Attire</h3>
		         <p>If your child has P.E. in the morning, we recommend that they wear their land-based sports uniform to
		         school and change into their regular uniform afterwards.</br>
		         If your child has land based P.E. in the afternoon, we recommend that children wear their regular 
		         uniform to school in the morning and change into their sports uniform at lunchtime.</br>
		         A complete swimming kit consists of: swimming costume, towel, goggles and a swimming cap. All these items are compulsory. </br>
		         In preparation for swimming classes, students aged 4+ and above should be able to dress themselves independently.
		         As a stepping stone to this, students in Nursery should be encouraged to put shoes and hats on independently.
		         Parents’ support with this at home is kindly appreciated.
                </p>
		     </div>
		     <div id="15">
		         <h3>15.Stationery</h3>
		         <p>Materials such as pencils, crayons, erasers, scissors are supplied by the 
		         school but some students may wish to bring their own pencil case to school.</br>
		         From Year 2 to Year 6, parents are required to supply their child with an iPad,
		         which children are to bring to school as directed by their child’s class teacher.
                </p>
		     </div>
		     <div id="16">
		         <h3>16.Electronic Communication Devices</h3>
		         <p>
		             In order to ensure the safety of our students, as well as facilitate a learning-focussed
		             environment, electronic communication and listening devices, such as mobile phones & smart
		             watches, may not be used while on campus. iPads are only to be used for the purpose of
		             supporting the learning process and to the discretion of our trusted practitioners. 
		             If you feel your child must have a cell phone, it must remain in their backpack, TURNED OFF
		             or SILENT while at school and while on the bus.</br>
		             Texting, taking photos, making and watching videos, and playing online games are not permitted 
		             anytime during the school day unless authorised by your child’s teacher. Failure to follow these 
		             guidelines will result in disciplinary action and/or confiscation of the phone/device. The mobile
		             phone/device may be retrieved at the end of the day. This policy is in effect throughout the entire
		             school day, including on the bus and school trips.</br>
		             If parents need to contact their child, they may do so by phoning the school during school hours, 
		             or the adult on the child’s bus enroute to school/home.
                </p>
		     </div>
		     <div id="17">
		         <h3>17.Lost and Found</h3>
		         <p>To minimise the number of lost items, we ask that all your child’s belongings be clearly labeled
		         with his/her name. If your child loses a specific item, please contact your child’s class teacher
		         who will do their best to help locate the item. At periodic times during the school year, any uncollected
		         items are sent to a charitable organisation. The school does not take responsibility for lost belongings.
                </p>
		     </div>
		     <div id="18">
		         <h3>18.Communication</h3>
		         <p>As part of our effort to minimise our carbon footprint, we use paperless communication whenever possible, 
		         therefore, it is very important that parents check their email account regularly and update the school 
		         with any change of email address. Teachers will also post regular photos on the Seesaw app. Your child’s 
		         teacher will be able to help you to connect to this. </br>
		         Should you need to contact your child’s teacher by email, please note that teachers may take 24-48 
		         hours to respond due to their busy schedules. Should parents wish to discuss their child’s progress, 
		         attainment and targets, parents can request meetings outside of the regular school hours. For any urgent issues,
		         parents should phone the school office.
                </p>
		     </div>
    	      <div id="19">
    	         <h3>19.Personal Information</h3>
    	         <p>It is very important for the school to have complete and up-to-date information about each student, 
    	         including people we may need to contact in case of emergency. Please ensure that the school is informed of 
    	         any changes by sending an email to admissions@sgstar.edu.vn.This includes change to:
                </p>
                <ul>
                    <li>Home address or telephone number</li>
                    <li>Parents’ and others’ contact details, especially mobile phone numbers and email addresses</li>
                    <li>Medical information including vaccinations, operations, allergies and anything else of relevance.
                        For reasons of health and safety, parents should inform the school of any medication that their child is taking</li>
                    <li>Permissions, including who is approved to collect your child from school and/or the school bus</li>
                </ul>
    	     </div>
		     <div id="20">
		         <h3>20.Medical Information</h3>
		         <p>If a student becomes unwell at school they will be accompanied to the school clinic room (next to the Headteacher’s room).
		         The School Nurse (or a nominated member of staff in the case of their unavailability) will evaluate the student’s medical 
		         condition and make a decision about whether the student should go home. If necessary, the school will contact the student’s
		         parents to collect the child. If a student is sick, they should stay home for a period of 48 hours in order to prevent the 
		         spread of the illness to others.</br>
		         In emergencies, the school will transport the child to an accident and emergency hospital immediately.
                </p>
		     </div>
		     <div id="21">
		         <h3>21.Important Health Notice</h3>
		         <p>This year we have members of our school community who have a severe allergy to NATURAL RUBBER LATEX.
		         This allergy can result in a life-threatening reaction, known as ANAPHYLAXIS. </br>
		         Anaphylaxis is a serious allergic reaction that is rapid in onset and if left untreated, will result in death. 
		         It typically causes more than one of the following: an itchy rash, throat or tongue swelling, shortness of breath,
		         vomiting, lightheadedness, and low blood pressure. These symptoms typically come on over minutes to hours.</br>
		         For this reason, we are asking all students to refrain from bringing any products containing latex into school. 
		         In a school environment, latex is most likely going to be found in balloons, rubber gloves and other toys/articles 
		         containing rubber. </br>
		         I am sure you will all understand our requirement to safeguard staff, as well as pupils and ask that these items
		         are not brought to school in the future. Thank you for your cooperation. 
		         </p>
		     </div>
		     <div id="22">
		         <h3>22.Educational Trips</h3>
		         <p>Field trips offer students unique learning opportunities that are not possible at school. They, therefore, 
		         form an integral part of the wider curriculum at Saigon Star. All trips are organised and led by teachers but may,
		         on occasion, include external professional guides to provide specialist knowledge or skills.</br>
		         Teachers will inform parents in advance of any off-site learning experiences, however, if you do not wish your
		         child to participate for any reason, please inform your child’s class teacher. In such an event, please note 
		         that children will not be permitted to join another class. Students would, therefore, need to stay home on this day. </br>
		         Students and parents can expect Educational Day Trips approximately once per term. Year 3 students have a residential 
		         at school towards the end of the school year, whilst older groups take part in off-site residential trips for a
		         period of time that increases with age. </br>
		         To reduce paper consumption, the school will assume we have consent for your child(ren) to participate in these trips
		         rather than administer permission slips for each and every one. Please contact the school should this not be the case. </br>
		         </p>
		     </div>
		     <div id="23">
		         <h3>23.Birthday Celebrations</h3>
		         <p>Many parents request to hold birthday parties during school time. Saigon Star welcomes this but reminds parents to arrange 
		         these at a time that is appropriate to the teacher and will not interfere with learners’ learning. </br>
		         The School Council has worked hard to drive a reduction of our ‘Single-Use Plastics’ and so community members
		         are reminded of this prior to bringing anything in.
		         </p>
		     </div>
		     <div id="24">
		         <h3>24.Parent Teacher Group (PTG)</h3>
		         <p>Should anyone wish to get more involved in school life by volunteering some of their time and/or expertise to the PTG, 
		         you are encouraged to get in touch with Eva Mehrudeen, who can tell you more about any upcoming events. She is contactable 
		         via phone (0933 435946) or by email: eva_mehrudeen@yahoo.co.id</p>
		     </div>
		     <div id="25">
		         <h3>25.Parent What’sApp Group</h3>
		         <p>To keep in touch with other parents from your child’s class, parents can ‘opt-in’ to a What’sApp group by sending your 
		         details to the leader of the PTG, mentioned above. Posts to this group are monitored. </p>
		     </div>
		     <div id="26">
		         <h3>26.Library Agreement (Primary and Middle Years parents only) </h3>
		         <p>Our school library collection has almost 10,000 books to inspire a life-long love for reading. Good books are 
		         difficult to obtain and replace in Vietnam. To help others enjoy the library resources, responsible book care is
		         important and we encourage you to teach your child the importance of respecting such shared resources. 
		         All Primary and Middle Years students are welcome to borrow two books at a time, with the Year 1 children
		         beginning to loan one book at a time. </br>
		         Please read and discuss the expectations below with your child
		         </p>
		     </div>
		     <div id="27">
		         <h3>27.Book Care Rules: </h3>
		         <ul>
		             <li>Keep books clean and dry; make sure hands are clean before touching; keep books away from food and liquids.</li>
		             <li>Keep books in a safe place, away from babies, younger siblings and pets.  </li>
		             <li>Find a good place and time where you and your child can read the book. A desk is recommended but a comfortable space is also suitable.</li>
		             <li>Turn pages carefully from the corner, not folding it. Use a bookmark to mark your place. Never write or draw in a book or earmark pages.</li>
		             <li>When you are not reading your library books, keep them in the plastic envelope and in a designated place for storage so the book will not be misplaced.</li>
		             <li>Remember your child’s library day and place the library envelope with the book in the school bag the night before. 
		                (For Banded Readers, teachers often change these more frequently. Check with the teacher on frequency of change.
		                Banded books must be returned in the library folder).</li>
		         </ul>
		     </div>
		     <div id="28">
		         <h3>28.Library Policies: </h3>
		         <p>1.Borrowing books from the library is free. There is no charge for checking out books unless a title is lost or damaged.</br>
		            2.When a student checks out a book, that student should look through the book for any damage or concerns. If found, 
		            they need to notify the teacher immediately</br>
		            3.Classes will visit the library each week. If students have not returned their previously checked out books, 
		            they will not be allowed to borrow more books. </br>
		            4.If a book has not been returned by the end of each term, the book will be considered lost.</br>
		            5.If a book is lost or damaged, the parent must pay to replace it and the student may be asked to
		            contribute to the library and support our librarian for a short period. The majority of our books cost
		            between US$4-10 to replace.</br>
		            6.If a book is damaged or lost, this should be brought to the attention of the teacher. Never try to repair 
		            a book as this may cause more damage.
		            
		            </p>
		     </div>
		     <div id="29">
		         <h3>29.Parent/Carer Code of Conduct</h3>
		         <p>Here at Saigon Star International School, we are very proud and fortunate to have a very dedicated and supportive
		         school community. At our school the staff, parents and carers all recognise that the education of our children 
		         is a partnership between us all.</br>
		         We expect our school community to respect our school’s ethos and set a good example of their own behaviour on school 
		         premises, when accompanying classes on school visits, and in the community. The purpose of this code of conduct is to 
		         provide the expectations around the conduct of all parents, carers and visitors connected to our school.</p>
		     </div>
		     <div id="30">
		         <h3>30.Behaviour that is expected:</h3>
		         <ul>
		             <li>All members of staff and our school community should be treated with respect.</li>
		             <li>Motorbikes are to be dismounted and walked through the security gate when dropping off and picking up students.</li>
		             <li>All students should be wearing high-quality helmets.</li>
		             <li>No student should be dropped off prior to 8:00 am. Adults must wait with their students until campus is open as 
		             there are no adults on the playground supervising at this time.</li>
		             <li>All parents must sign-in and obtain a visitors badge when attending events during school hours.</li>
		             <li>Please respect the working hours of all employees. Staff are not expected to return emails, calls, 
		             or messages before or after the work day (7.45am to 4.15pm).</li>
		             <li>LTAs should only be contacted regarding concerns with their bus register. Classroom LTAs should not 
		             be contacted instead of the class teacher to resolve issues or answer questions not related to the bus. 
		             All classroom queries should be directed to the classroom teacher. </li>
		         </ul>
		         <p>We are committed to resolving difficulties in a constructive manner, through an open and positive dialogue.
		            However we understand that everyday misunderstandings can cause frustrations and have a negative impact on our 
		            relationships.
		          </p>
		     </div>
		     <div id="31">
		         <h3>31.If parents have any concerns about their child in relation to the points described above, they should:</h3>
		         <p>1.Initially contact the class teacher</br>
		            2.If the concern remains, they should contact the Headteacher
		         </p>
		         <p>Should any of the following occur on school premises or in connection with school, we may feel it is 
		         necessary to take action by contacting the appropriate authorities or consider banning the offending adult
		         from entering the school premises.</p>
		     </div>
		     <div id="32">
		         <h3>32.Behaviour that will not be tolerated:</h3>
		            <ul>
        		         <li>Disruptive or inappropriate behaviour which interferes or threatens to interfere with any of the school’s 
        		         normal operation or activities anywhere on the school premises.</li>
        		         <li>Using loud or offensive language or displaying temper.</li>
        		         <li>Threatening in any way, a member of staff, visitor, fellow parent/carer or child.</li>
        		         <li>Damaging or destroying school property</li>
        		         <li>Sending abusive or threatening emails or text/voicemail/phone messages or other written 
        		         communications (including social media) to anyone within the school community.</li>
        		         <li>Defamatory, offensive or derogatory comments regarding the school or any of the 
        		         pupils/parents/staff/governors at the school on social media or other sites</li>
        		         <li>The use of physical, verbal or written aggression towards another adult or child.
        		         This includes physical punishment of your own child on or off school premises.</li>
        		         <li>Approaching someone else’s child in order to discuss or chastise them because of 
        		         the actions of this child towards their own child (such an approach to a child may be seen to be an assault)</li>
        		         <li>Smoking or intoxication by any substance on school premises or school outin</li>
        		    </ul>
        		    <p>It is important for parents and carers to make sure any persons collecting their children are aware of this policy. </br>
        		        Thank you for abiding by this code in our school. Together we create a positive and uplifting environment not only 
        		        for the children but also all who work and visit our school.
        		    </p>
		     </div>
		     <div id="33">
		         <h3>33.What happens if someone ignores or breaks the code?</h3>
		         <p>In cases where unacceptable behaviour is considered to be a serious and potentially criminal matter, the concerns will 
		         in the first instance be referred to the Senior Leadership Team and/or possibly the local police. This will include any or
		         all cases of threats or violence and actual violence to any child, staff, or community member in the school. This will also 
		         include anything that could be seen as a sign of harassment of any member of the school community, such as any form of 
		         insulting social media post or any form of social media cyber bullying. In cases where the code of conduct has been broken
		         but the breach was not libellous, slanderous or criminal matter, then the school will invite the parent/carer
		         to attend a meeting.</br>
		         If the parent/carer refuses to attend the meeting then the school will write to the parent/carer and ask them to stop the
		         behaviour causing the concern and warn that if they do not, they may be banned from the school premises. If after this 
		         behaviour continues, the parent/carer will again be written to and informed that a ban is now in place.</br>
		         Note:</br>
		         (1) a ban from the school can be introduced without having to go through all the steps offered above in more serious cases.</br>
		         (2) Site bans will normally be limited in the first instance.
		         </p>
		     </div>
		     <div id="34">
		         <h3>34.Online activity which we consider inappropriate:</h3>
		         <ul>
		             <li>Identifying or posting images/videos of children</li>
		             <li>Abusive or personal comments about staff, children or other parents</li>
		             <li>Using social media as a medium to air any concerns or grievances</li>
		             <li>Bringing the school in disrepute</li>
		             <li>Posting defamatory or libellous comments</li>
		             <li>Emails circulated or sent directly with abusive or personal comments about staff or children </li>
		             <li>Using social media to publicly challenge school policies or discuss issues about individual 
		             children or members of staff</li>
		             <li>Threatening behaviour, such as verbally intimidating staff, or using bad language</li>
		             <li>Breaching school security procedures</li>
		         </ul>
		         <p>At our school we take our safeguarding responsibilities seriously and will deal with any reported
		            incidents appropriately in line with the actions outlined above.
		          </p>
		     </div>
		     <div id="35">
		         <h3>35.PARENT HANDBOOK DECLARATION</h3>
		         <p>Thank you for taking the time to review this document. If there is anything in this handbook that you 
		            do not understand, you should speak to the Headteacher in the first instance. </br>
                    We kindly ask that parents sign below and return the slip to the school office.</br>
                    I have read and understood the contents within the 2020-21 Parent Handbook.
                </p>
		     </div>
		</div> 
	</div>
	
</div>
@endsection
@section('script')
    <script>
		$(document).ready(function(){
		// Add smooth scrolling to all links
		$("a").on('click', function(event) {

			// Make sure this.hash has a value before overriding default behavior
			if (this.hash !== "") {
			// Prevent default anchor click behavior
			event.preventDefault();

			// Store hash
			var hash = this.hash;

			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 1200, function(){

				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
			});
			} // End if
		});
		});
    </script>
@endsection
