<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img class="d-block w-100" src="{{ url('public') }}/images/banner/banner007.png" alt="First slide">
		</div>
		<div class="carousel-item">
			<img class="d-block w-100" src="{{ url('public') }}/images/banner/banner_03.jpg" alt="Second slide">
		</div>
	</div>
</div>