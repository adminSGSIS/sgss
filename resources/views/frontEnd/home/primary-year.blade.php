@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
@endpush
@section('main_content')
    <div class="banner">
    	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>curriculum</h3>
            </div>
    	</div>
    	
    </div>
    <div class="c-color">
        <div class="curriculum-page row">
            <div class="col-lg-3 col-md-12 col-sm-12 sub-menu">          
                <a href="/curriculum" class="font-weight-bold">OVERVIEW <i class="fa fa-caret-down"></i></a></br>
                <a onclick="event.preventDefault();" href="#" class="font-weight-bold">ACADEMIC PROGRAMS <i class="fa fa-caret-down"></i></a></br>
                <a class="pl-2" href="/early-year">EARLY YEARS</a></br>
                <a class="pl-2 active" href="/primary-year">PRIMARY YEARS</a>
            </div> 
            <div class="col-lg-9 col-md-12 col-sm-12">
                <h3>ACADEMIC PROGRAMS</h3>
                <h4>primary years</h4>
                <p class="new-text-color">
                    The IPC is our International Primary Curriculum for children aged 5 -11 years old.
                    It is used by over 1000 schools in over 90 countries worldwide. </br></br>
                    It is a comprehensive, thematic, creative curriculum, with a clear process
                    of learning and specific learning goals for every subject. It also develops 
                    international mindedness and encourages personal learning. </br></br>
                    At Saigon Star, our aim is to provide a truly International Education. 
                    This means providing an education that encourages learners to be globally competent 
                    (Boix Mansilla and Jackson, 2013), future ready, socially conscious and motivated to
                    positively contribute within a local and/or global context. </br></br>
                    We combine the rigorous National Curriculum for England and Wales with International 
                    Primary Curriculum by Fieldwork Education, enriched with STEM, World languages, 
                    Extracurricular Activities, News and SGTV clubs. 
                
                </p>
                <img src="/public/images/pic5.jpg" width="100%">
                
                <div style="padding-bottom:10%" class="curriculum-link">
                    <br>
                    <a target="_blank" href="/public/tailieu/02_SSIS_WEB_CURRICULUM_PY.pdf">Download the 2020-2021 Primary Year Program Overview Kit</a>
                </div>
            </div>
        </div>
    </div>  
@endsection