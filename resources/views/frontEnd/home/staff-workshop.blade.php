@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
@endpush
@section('main_content')
    <div class="banner">
    	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>training</h3>
            </div>
    	</div>
    	
    </div>
   

    <section style="background:#fafaf4">
        <div class="container">
           
            <ul id="flexiselDemo3" style="background:#fafaf4">
                <li class="wrapper_1">
                    <img class="img-slider " src="/public/images/staff_workshop/01.jpg" />
                </li>
                <li class="wrapper_2">
                    <img class="img-slider " style="max-height:253px;width:100%" src="/public/images/staff_workshop/02.jpg" />
                </li>
                <li class="wrapper_3">
                    <img class="img-slider " src="/public/images/staff_workshop/03.jpg" />
                </li>
            </ul>
        </div>
       <div class="container">
            <img  width="100%" src="{{asset('public/')}}/images/CHILDREN-01.png" alt=""> 
       </div>
    </section>
@endsection