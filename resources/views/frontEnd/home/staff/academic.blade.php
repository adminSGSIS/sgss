@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
@endpush
@section('main_content')
    <div >
        <div class="banner">
            <img class="banner" width="100%" src="{{ asset('public/images/banner/banner_03.jpg') }}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>academic team</h3>
                </div>
            </div>
        </div>
        
        <div class="staff d-flex justify-content-center c-color">
            @foreach ($earlys as $early)
                <div class="profile2 collumn">
                    <img src="{{ file_exists(@$early->staff_photo) ? asset($early->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $early->user_id }}">
                        {{ $early->first_name }}
                        {{ substr($early->last_name, strrpos($early->last_name, ' ')) }}

                    </a><br>
                    <p class="staff-title">{!! $early->designations->title !!}</p>
                </div>

            @endforeach
            @foreach ($primarys as $primary)
                <div class="profile2 collumn">
                    <img src="{{ file_exists(@$primary->staff_photo) ? asset($primary->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $primary->user_id }}">
                        {{ $primary->first_name }}
                        {{ substr($primary->last_name, strrpos($primary->last_name, ' ')) }}

                    </a><br>
                    <p class="staff-title">{!! $primary->designations->title !!}</p>
                </div>

            @endforeach
            
            @foreach ($middles as $middle)
                <div class="profile2 collumn">
                    <img src="{{ file_exists(@$middle->staff_photo) ? asset($middle->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $primary->user_id }}">
                        {{ $middle->first_name }}
                        {{ substr($middle->last_name, strrpos($middle->last_name, ' ')) }}

                    </a><br>
                    <p class="staff-title">{!! $middle->designations->title !!}</p>
                </div>

            @endforeach
            
            @foreach ($specialists as $specialist)
                <div class="profile2 collumn">
                    <img src="{{ file_exists(@$specialist->staff_photo) ? asset($specialist->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $specialist->user_id }}">
                        {{ $specialist->first_name }}
                        {{ substr($specialist->last_name, strrpos($specialist->last_name, ' ')) }}

                    </a><br>
                    <p class="staff-title">{!! $specialist->designations->title !!}</p>
                </div>

            @endforeach
        </div>
    </div>
    
    <div class="c-color">
        <div class="container">
              <img width="100%" src="{{ url('public') }}/images/CHILDREN-01.png" alt="">
        </div>
    </div>
    @include('frontEnd.home.partials.info-teachers')
@endsection
