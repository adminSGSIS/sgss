@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
@endpush
@section('main_content')
    <div>
        <div class="banner">
            <img class="banner" width="100%" src="{{ asset('public/images/banner/banner_03.jpg') }}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>IT team</h3>
                </div>
            </div>
        </div>
        
        <div class="staff d-flex justify-content-center c-color">
            @foreach ($its as $key => $it)
                @if($key != 0)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$it->staff_photo) ? asset($it->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <p class="btn-profile">
                        {{ $it->full_name }}
                    </p>
                    <p class="staff-title">{{ $it->designations->title }}</p>
                </div>
                @endif
            @endforeach
            @foreach ($its as $key => $it)
                @if($key == 0)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$it->staff_photo) ? asset($it->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <p class="btn-profile">
                        {{ $it->full_name }}
                    </p>
                    <p class="staff-title">{{ $it->designations->title }}</p>
                </div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="c-color">
        <div class="container">
              <img width="100%" src="{{ url('public') }}/images/CHILDREN-01.png" alt="">
        </div>
    </div>
    <!--<div class="rainbow">-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--</div>-->
    @include('frontEnd.home.partials.info-teachers')
@endsection
