@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
@endpush
@section('main_content')
    <div>
        <div class="banner">
            <img class="banner" width="100%" src="{{ asset('public/images/banner/banner_03.jpg') }}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>leadership team</h3>
                </div>
            </div>
        </div>
        
        <div class="staff d-flex justify-content-center c-color pt-30">
            @foreach ($leaderships as $key=>$leadership)
                @if($leadership->designation_id == 13)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$leadership->staff_photo) ? asset($leadership->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $leadership->user_id }}">
                        {{ $leadership->full_name }}
                    </a>
                    <p class="staff-title">{{$leadership->designations->title}}</p>
                </div>
                @endif
            @endforeach
            @foreach ($leaderships as $key=>$leadership)
                @if($leadership->designation_id == 7)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$leadership->staff_photo) ? asset($leadership->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $leadership->user_id }}">
                        {{ $leadership->full_name }}
                    </a>
                    <p class="staff-title">{{$leadership->designations->title}}</p>
                </div>
                @endif
            @endforeach
            @foreach ($leaderships as $key=>$leadership)
                @if($leadership->designation_id == 6)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$leadership->staff_photo) ? asset($leadership->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $leadership->user_id }}">
                        {{ $leadership->full_name }}
                    </a>
                    <p class="staff-title">{{$leadership->designations->title}}</p>
                </div>
                @endif
            @endforeach
            @foreach ($leaderships as $key=>$leadership)
                @if($leadership->designation_id == 5)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$leadership->staff_photo) ? asset($leadership->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $leadership->user_id }}">
                        {{ $leadership->full_name }}
                    </a>
                    <p class="staff-title">{{$leadership->designations->title}}</p>
                </div>
                @endif
            @endforeach
        </div>
        <div class="c-color">
        <div class="container">
              <img width="100%" src="{{ url('public') }}/images/CHILDREN-01.png" alt="">
        </div>
    </div>
            @include('frontEnd.home.partials.info-teachers')
        @endsection
