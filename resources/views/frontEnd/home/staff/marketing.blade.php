@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
@endpush
@section('main_content')
    <div>
        <div class="banner">
            <img class="banner" width="100%" src="{{ asset('public/images/banner/banner_03.jpg') }}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>marketing & event team</h3>
                </div>
            </div>
        </div>
        
        <div class="staff d-flex justify-content-center c-color">
            @foreach ($marketings as $marketing)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$marketing->staff_photo) ? asset($marketing->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <p class="btn-profile" >
                        {{ $marketing->full_name }}
                    </p>
                    <p class="staff-title">{{$marketing->designations->title}}</p>
                </div>

            @endforeach
            <div class="profile collumn">
                    <img src="https://sgstar.asia/public/uploads/staff/4457d678560b045af37465c6c6dcb3d0.png" alt=""><br>
                    <p class="btn-profile">
                        NGUYEN NGOC HIEN
                    </p>
                    
                    <p class="staff-title">Event Coordinator</p>
                </div>
        </div>
    </div>
   <div class="c-color">
        <div class="container">
              <img width="100%" src="{{ url('public') }}/images/CHILDREN-01.png" alt="">
        </div>
    </div>
    @include('frontEnd.home.partials.info-teachers')
@endsection
