@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
@endpush
@section('main_content')
    <div >
        <div class="banner">
            <img class="banner" width="100%" src="{{ asset('public/images/banner/banner_03.jpg') }}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>operations team</h3>
                </div>
            </div>
        </div>
        
        <div class="staff d-flex justify-content-center c-color">
            @foreach ($operations as $key=>$operation)
                @if($key ==2)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$operation->staff_photo) ? asset($operation->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <button type="button" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal{{ $operation->user_id }}">
                        {{ $operation->full_name }}
                    </button>
                    <p class="staff-title">{{$operation->designations->title}}</p>
                </div>
                @endif
            @endforeach
            @foreach ($operations as $key=>$operation)
                @if($key != 2)
                <div class="profile collumn">
                    <img src="{{ file_exists(@$operation->staff_photo) ? asset($operation->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <p class="btn-profile" >
                        {{ $operation->full_name }}
                    </p>
                    <p class="staff-title">{{$operation->designations->title}}</p>
                </div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="c-color">
        <div class="container">
              <img width="100%" src="{{ url('public') }}/images/CHILDREN-01.png" alt="">
        </div>
    </div>
    @include('frontEnd.home.partials.info-teachers')
@endsection
