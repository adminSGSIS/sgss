@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/style2.css" />
@endpush
@section('main_content')
    <div>
        <div class="banner">
            <img class="banner" width="100%" src="{{ asset('public/images/banner/banner_03.jpg') }}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>learning & teaching assistant team</h3>
                </div>
            </div>
        </div>
        
        <div class="staff d-flex justify-content-center c-color pt-30">
            @foreach ($assistants as $assistant)
                <div class="profile2 collumn">
                    <img src="{{ file_exists(@$assistant->staff_photo) ? asset($assistant->staff_photo) : asset('public/uploads/staff/demo/staff.jpg') }}"
                        alt=""></br>
                    <p class="btn-profile" >
                        {{ strtoupper ($assistant->full_name) }}
                    </p>
                    <p class="staff-title">{{$assistant->designations->title}}</p>
                </div>

            @endforeach
        </div>
    </div>
    <div class="c-color">
        <div class="container">
              <img width="100%" src="{{ url('public') }}/images/CHILDREN-01.png" alt="">
        </div>
    </div>
    @include('frontEnd.home.partials.info-teachers')
@endsection
