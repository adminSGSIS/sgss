@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
<link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
    <link rel="stylesheet" href="{{url('public')}}/frontEnd/css/newcss.css"/> 
@endpush
<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner/banner_03.jpg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>STUDENT COUNCIL</h3>
        </div>
	</div>
</div>

<div class="main_content" style="background:#fafaf4">
	<div class="container" id="student-council">
		<div class="col-12 student-council">
		    <div class="col-lg-6 col-sm-12">
		        <h1>year 3</h1>
		        <div class="group1">
    		        <div class="avatar">
    		            <img src="{{asset('public/')}}/images/student-council/TOM.png" alt="">
    		            <h3>Tom</h3>
    		        </div>
    		        <div class="avatar">
    		            <img src="{{asset('public/')}}/images/student-council/PEPSI.png" alt="">
    		            <h3>Pepsi</h3>
    		        </div>
    		     </div>
		    </div>
		    <div class="col-lg-6 col-sm-12">
		        <h1>year 4</h1>
		        <div class="group1">
    		        <div class="avatar">
    		            <img src="{{asset('public/')}}/images/student-council/ADI.png" alt="">
    		            <h3>Adi</h3>
    		        </div>
    		        <div class="avatar">
    		            <img src="{{asset('public/')}}/images/student-council/LIAM.png" alt="">
    		            <h3>Liam</h3>
    		        </div>
    		     </div>
		    </div>
		</div>
		<div class="col-12 student-council">
		    <div class="col-lg-6 col-sm-12">
		        <h1>year 5</h1>
		        <div class="group1">
    		        <div class="avatar">
    		            <img src="{{asset('public/')}}/images/student-council/KIRIN.png" alt="">
    		            <h3>Kirin</h3>
    		        </div>
    		        <div class="avatar">
    		            <img src="{{asset('public/')}}/images/student-council/TIMOFEY.png" alt="">
    		            <h3>Timofey</h3>
    		        </div>
    		     </div>
		    </div>
		    <div class="col-lg-6 col-sm-12">
		        <h1>year 6</h1>
		        <div class="group1">
    		        <div class="avatar">
    		            <img src="{{asset('public/')}}/images/student-council/KY-ANH.png" alt="">
    		            <h3>Ky Anh</h3>
    		        </div>
    		        <div class="avatar">
    		            <img src="{{asset('public/')}}/images/student-council/ORI.png" alt="">
    		            <h3>Ori</h3>
    		        </div>
    		     </div>
		    </div>
		</div>
		
		
	</div>
	<div id="carouselExampleControls" class="carousel slide pt-30" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<div class="student-council">
					<h1>year 3</h1>
					<div class="">
						<div class="col-12 text-center">
							<img src="{{asset('public/')}}/images/student-council/TOM.png" width="80%" alt="">
							<h3>Tom</h3>
						</div>
						<div class="col-12 text-center">
							<img src="{{asset('public/')}}/images/student-council/PEPSI.png" width="80%" alt="">
							<h3>Pepsi</h3>
						</div>
					</div>
				</div>
			</div>

			<div class="carousel-item">
				<div class="student-council">
					<h1>year 4</h1>
					<div class="">
						<div class="col-12 text-center">
							<img src="{{asset('public/')}}/images/student-council/ADI.png" width="80%" alt="">
							<h3>Adi</h3>
						</div>
						<div class="col-12 text-center">
							<img src="{{asset('public/')}}/images/student-council/LIAM.png" width="80%" alt="">
							<h3>Liam</h3>
						</div>
					</div>
				</div>
			</div>

			<div class="carousel-item">
				<div class="student-council">
					<h1>year 5</h1>
					<div class="">
						<div class="col-12 text-center">
							<img src="{{asset('public/')}}/images/student-council/KIRIN.png" width="80%" alt="">
							<h3>Kirin</h3>
						</div>
						<div class="col-12 text-center">
							<img src="{{asset('public/')}}/images/student-council/TIMOFEY.png" width="80%" alt="">
							<h3>Timofey</h3>
						</div>
					</div>
				</div>
			</div>

			<div class="carousel-item">
				<div class="student-council">
					<h1>year 6</h1>
					<div class="">
						<div class="col-12 text-center">
							<img src="{{asset('public/')}}/images/student-council/KY-ANH.png" width="80%" alt="">
							<h3>Ky Anh</h3>
						</div>
						<div class="col-12 text-center">
							<img src="{{asset('public/')}}/images/student-council/ORI.png" width="80%" alt="">
							<h3>Ori</h3>
						</div>
					</div>
				</div>
			</div>
		
		</div>
		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" onclick="event.preventDefault();">
			<span class="fa fa-chevron-left"  aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next" onclick="event.preventDefault();">
			<span class="fa fa-chevron-right"  aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
@endsection