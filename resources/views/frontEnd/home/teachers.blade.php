@extends('frontEnd.home.layout.front_master')
@push('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/teacher.css" />
@endpush
@section('main_content')

    <!-- CSS Library-->
    <!-- Style -->
    <div class="banner">
    	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>OUR TEACHERS</h3>
            </div>
    	</div>
    	
    </div>
    <div class="c-color">
        <div class="main-teacher">
        
            <div id="myBtnContainer" class="text-center pt-30">
                <button class="btn md-txt active" onclick="filterSelection('leadership')"> Leadership Team</button>
                <button class="btn md-txt" onclick="filterSelection('Early')"> Early years</button>
                <button class="btn md-txt" onclick="filterSelection('primary')"> Primary years</button>
                <!--<button class="btn md-txt" onclick="filterSelection('Middle')"> Middle years</button>-->
                <button class="btn md-txt" onclick="filterSelection('Specialist')"> Specialists</button>
            </div>

            <!-- Portfolio Gallery Grid -->
            <div class="row-people clearfix d-flex justify-content-center">
                            @foreach($leaderships as $key=>$leadership)
                            @if($leadership->designation_id == 13)
                            <div class="column-teacher leadership">
                                <div class="content-teacher">
                                    <img src="{{asset($leadership->staff_photo)}}" alt="Lights" style="width:100%">
                                    <h4>{{$leadership->full_name}}</h4>
                                    <p>{{$leadership->designations->title}}</p> 
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$leadership->user_id}}">
                                        Read more
                                    </button>

                                </div>
                            </div>
                            @endif
                            @endforeach
                            @foreach($leaderships as $key=>$leadership)
                        @if($leadership->designation_id == 7)
                            <div class="column-teacher leadership">
                                <div class="content-teacher">
                                    <img src="{{asset($leadership->staff_photo)}}" alt="Lights" style="width:100%">
                                    <h4>{{$leadership->full_name}}</h4>
                                    <p>{{$leadership->designations->title}}</p> 
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$leadership->user_id}}">
                                        Read more
                                    </button>

                                </div>
                            </div>
                            @endif
                            
                            @endforeach
                            @foreach($leaderships as $key=>$leadership)
                            @if($leadership->designation_id == 6)
                            <div class="column-teacher leadership">
                                <div class="content-teacher">
                                    <img src="{{asset($leadership->staff_photo)}}" alt="Lights" style="width:100%">
                                    <h4>{{$leadership->full_name}}</h4>
                                    <p>{{$leadership->designations->title}}</p> 
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$leadership->user_id}}">
                                        Read more
                                    </button>

                                </div>
                            </div>
                            @endif
                            @endforeach
                            @foreach($leaderships as $key=>$leadership)
                            @if($leadership->designation_id == 5)
                            <div class="column-teacher leadership">
                                <div class="content-teacher">
                                    <img src="{{asset($leadership->staff_photo)}}" alt="Lights" style="width:100%">
                                    <h4>{{$leadership->full_name}}</h4>
                                    <p>{{$leadership->designations->title}}</p> 
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$leadership->user_id}}">
                                        Read more
                                    </button>

                                </div>
                            </div>
                            @endif
                            @endforeach
                            @foreach($primary_years_teachers as $key=>$primary_years_teacher)
                            <div class="column-teacher primary">
                                <div class="content-teacher">
                                <img src="{{asset($primary_years_teacher->staff_photo)}}" alt="Car" style="width:100%">
                                <h4>{{$primary_years_teacher->full_name}}</h4>
                                <p>{{$primary_years_teacher->designations->title}}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$primary_years_teacher->user_id}}">
                                    Read more
                                </button>  
                                </div>
                            </div>
                            @endforeach
                    
                            @foreach($early_years_teachers as $key=>$early_years_teacher)
                            @if($key == 1)
                            <div class="column-teacher Early">
                                <div class="content-teacher">
                                <img src="{{asset($early_years_teacher->staff_photo)}}" alt="Car" style="width:100%">
                                <h4>{{$early_years_teacher->full_name}}</h4>
                                <p>{{$early_years_teacher->designations->title}}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$early_years_teacher->user_id}}">
                                    Read more
                                </button>  
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @foreach($early_years_teachers as $key=>$early_years_teacher)
                            @if($key == 2)
                            <div class="column-teacher Early">
                                <div class="content-teacher">
                                <img src="{{asset($early_years_teacher->staff_photo)}}" alt="Car" style="width:100%">
                                <h4>{{$early_years_teacher->full_name}}</h4>
                                <p>{{$early_years_teacher->designations->title}}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$early_years_teacher->user_id}}">
                                    Read more
                                </button>  
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @foreach($early_years_teachers as $key=>$early_years_teacher)
                            @if($key ==3)
                            <div class="column-teacher Early">
                                <div class="content-teacher">
                                <img src="{{asset($early_years_teacher->staff_photo)}}" alt="Car" style="width:100%">
                                <h4>{{$early_years_teacher->full_name}}</h4>
                                <p>{{$early_years_teacher->designations->title}}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$early_years_teacher->user_id}}">
                                    Read more
                                </button>  
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @foreach($early_years_teachers as $key=>$early_years_teacher)
                            @if($key == 0)
                            <div class="column-teacher Early">
                                <div class="content-teacher">
                                <img src="{{asset($early_years_teacher->staff_photo)}}" alt="Car" style="width:100%">
                                <h4>{{$early_years_teacher->full_name}}</h4>
                                <p>{{$early_years_teacher->designations->title}}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$early_years_teacher->user_id}}">
                                    Read more
                                </button>  
                                </div>
                            </div>
                            @endif
                            @endforeach
                            
                            <!--@foreach($middle_years_teachers as $middle_years_teacher)-->
                            <!--<div class="column-teacher Middle">-->
                            <!--    <div class="content-teacher">-->
                            <!--    <img src="{{asset($middle_years_teacher->staff_photo)}}" alt="Car" style="width:100%">-->
                            <!--    <h4>{{$middle_years_teacher->full_name}}</h4>-->
                            <!--    <p>{{$middle_years_teacher->designations->title}}</p>-->
                            <!--    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$middle_years_teacher->user_id}}">-->
                            <!--        Read more-->
                            <!--    </button>  -->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--@endforeach-->
                            
                            @foreach($specialist_teachers as $specialist_teacher)
                            <div class="column-teacher Specialist">
                                <div class="content-teacher">
                                <img src="{{asset($specialist_teacher->staff_photo)}}" alt="Car" style="width:100%">
                                <h4>{{$specialist_teacher->full_name}}</h4>
                                <p>{{$specialist_teacher->designations->title}}</p>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$specialist_teacher->user_id}}">
                                    Read more
                                </button>  
                                </div>
                            </div>
                            @endforeach
            <!-- END GRID -->
            
            </div>

        @include('frontEnd.home.partials.info-teachers') 
        </div>
    </div>
@endsection
@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        filterSelection("leadership")
        function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("column-teacher");
        
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
        }

        function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
        }
        }

        function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);     
            }
        }
        element.className = arr1.join(" ");
        }


        // Add active class to the current button (highlight it)
        var btnContainer = document.getElementById("myBtnContainer");
        var btns = btnContainer.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function(){
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
        }
    </script>

    <script type="text/javascript">
        jQuery(function($){  
        $("#bt_close").on("click", function() {
            $(".social_group").addClass("hidden");
            $("#bt_open").show();
        });
        $("#bt_open").on("click", function() {
            $(this).hide();
            $(".social_group").show();
            $(".social_group").removeClass("hidden");
        });

        $(".social_group").hover(            
                function() {
                    $(this).toggleClass('open');        
                },
                function() {
                    $(this).toggleClass('open');       
                }
            );
        });  

        $( document ).ready(function() {    
            $("#bs-example-navbar-collapse-1 .dropdown-menu li.active").parent().parent().addClass('active');
        $('#menutop li .i_mobile_ex').click(function(){
            console.log('ok');
        })
        });
    </script>
@endsection