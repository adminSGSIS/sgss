@extends('frontEnd.home.layout.front_master')
@section('main_content')
<style>
    .logo_form{
        width: 22%;
        margin-top: 2%;
        margin-left: 2%;
    }
    .top{
        display: flex;
    }
    .title{
        font-size: 20px;
        font-weight: 700;
        color: #87ceeb;
    }
    .title-top{
        margin-top: 2%;
        margin-left: 23%;
    }
    .title-top h3{
        color: #0c4da2;
        font-style: normal;
        font-size: 20px;
    }
    textarea{
        width: 100%;
        border-radius: 10px;
        border: 1px solid #e9ecef;
        padding: 5px 5px 5px 15px;
    }
    textarea:focus{
        outline: none;
    }
    .title-top h4{
        font-size: 12px;
        color: #0c4da2;
        font-style: normal;
    }
    .form-group{
        display: flex;
        margin-left: -30px;
    }
    .card-body label{
        color: #87ceeb;
    }
    .copy-right p{
        text-align: center;
        font-weight: 600;
        color: #0c4da2;
        font-size: 16px;
    }
    
</style>
<div class="row d-flex justify-content-center pt-20">
    <div class="col-10">
        <div class="card"> 
            <div class="top">
                <div class="logo_form">
                    <img src="/public/images/logo.png" alt="logo" class="img-responsive">
                </div>
                <div class="title-top">
                    <h3>Saigon Star International School</h3>
                    <h4> <i class="fa fa-map-marker"></i> Su Hy Nhan Street, Residential Area No.5, Thanh My Loi Ward, District 2, Ho Chi Minh City</h4>
                    <h4> <i class="fa fa-globe"></i> www.sgstar.edu.vn  &nbsp &nbsp   <i class="fa fa-envelope-square"></i>  info@sgstar.edu.vn  &nbsp &nbsp  <i class="fa fa-phone"></i> 028 342 3222   &nbsp &nbsp <i class="fa fa-phone"></i> 08 8800 6996</h4>
                </div>
            </div>
            
            <div class="card-body col-12">		
                    <p class="d-flex justify-content-center pt-20 title">SCREENING INTERVIEW QUESTIONS</p>
                    @foreach($data as $answer)
                        <p>1. Tell us a little about yourself/your background.</br>
                            Purpose: To evaluate the skills and attitude required for the job.</p>
                        <label for="question1">Answer:</label>
                        <textarea  id="question1"  rows="5" readonly>{{$answer->answer_1}}</textarea>

                        <p>2. What are your biggest weaknesses?</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question2" id="question2"  rows="5" readonly>{{$answer->answer_2}}</textarea>
                        
                        <p> 3. What are your key strengths?</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question3" id="question2"  rows="5" readonly>{{$answer->answer_3}}</textarea>

                        <p> 4. What about this job interests you?
                            Purpose: To see whether or not the candidate is serious and have a genuine interest in
                            pursuing the position.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question4" id="question2"  rows="5" readonly>{{$answer->answer_4}}</textarea>

                        <p> 5. Tell us what you know about the role.
                            Purpose: To see how much a candidate knows about the basics of the position he/she is
                            applying for, whether or not he/she has taken the time to carefully read the job description
                            and research as much as possible before.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question5" id="question2"  rows="5" readonly>{{$answer->answer_5}}</textarea>

                        <p> 6. Why do you want to work here?
                            Purpose: To get an idea of whether or not the candidate has researched the company, what
                            motivates them and whether their values align with those of the business or they might be
                            inspired by the company mission or excited about the company growth in the industry.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question6" id="question2"  rows="5" readonly>{{$answer->answer_6}}</textarea>

                        <p> 7. What are you passionate about?
                            Purpose: To understanding what the candidate is passionate about or what motivates them
                            might help the company decide whether the candidate might be a good fit for the position.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question7" id="question2"  rows="5" readonly>{{$answer->answer_7}}</textarea>

                        <p> 8. What do you consider to be your biggest professional achievement (or proudest moment)?</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question8" id="question2"  rows="5" readonly>{{$answer->answer_8}}</textarea>

                        <p>9. What type of management style do you prefer?
                            Purpose: To see whether or not a candidate would fit well with the supervisor that will be
                            managing him/her.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question9" id="question2"  rows="5" readonly>{{$answer->answer_9}}</textarea>

                        <p> 10. Tell us about the last time a co-worker or customer got angry with you. What happened?
                            Purpose: To choose candidates who focus not on blame but on addressing and fixing the
                            problem and, most important, learn from the experience.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question10" id="question2"  rows="5" readonly>{{$answer->answer_10}}</textarea>

                        <p>11. What business would you love to start?
                            Purpose: The business a candidate would love to start tells you about his/her hopes and
                            dreams, interests and passions, the work he/she likes to do, the people he/she likes to work
                            with. The HR interviewer is able to see how well candidates can sell themselves.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question11" id="question2"  rows="5" readonly>{{$answer->answer_11}}</textarea>

                        <p> 12. Why do you want to leave your current job?</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question12" id="question2"  rows="5" readonly>{{$answer->answer_12}}</textarea>

                        <p> 13. What kind of work environment do you like best?
                                Purpose: To see how well a candidate would fit with the company’s culture.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question13" id="question2"  rows="5" readonly>{{$answer->answer_13}}</textarea>

                        <p> 14. Tell us about the toughest decision you had to make in the last six months.
                        Purpose: The goal of this question is to evaluate a candidate&#39;s reasoning ability, problem-
                        solving skills, judgment, and possibly even willingness to take intelligent risks.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question14" id="question2"  rows="5" readonly>{{$answer->answer_14}}</textarea>

                        <p> 15. What is your leadership style?</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question15" id="question2"  rows="5" readonly>{{$answer->answer_15}}</textarea>

                        <p> 16. Tell us about a time you disagreed with a decision. What did you do?
                            Purpose: The company wants employees willing to be honest and forthright, to share
                            concerns and issues, but to also get behind a decision and support it as if they agreed, even if
                            they didn&#39;t. To see how professional and emotional a candidate may show through their
                            story.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question16" id="question2"  rows="5" readonly>{{$answer->answer_16}}</textarea>
                        
                        <p> 17. What do you like to do outside of work?
                            Purpose: Cultural fit is extremely important and using outside interests is a way to
                            determine/re-test how a candidate will fit into a team.</p>
                        <label for="question2">Answer:</label>
                        <textarea name="question17" id="question2"  rows="5" readonly>{{$answer->answer_17}}</textarea>
                    @endforeach
                        
                   
            </div> 
            <div class="copy-right">
                <p class="d-flex justify-content-center">©2020 Saigon Star International School - Imprint</p>
            </div>
        </div>
    </div>
</div>
@endsection