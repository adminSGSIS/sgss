@extends('frontEnd.home.layout.front_master')
@section('main_content')
@push('css')
<link rel="stylesheet" href="{{asset('public/')}}/css/style2.css"/>
    <link rel="stylesheet" href="{{url('public')}}/frontEnd/css/newcss.css"/> 
@endpush

<div class="banner">
	<img class="banner" width="100%" src="{{asset('public/')}}/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>WINTER CAMP</h3>
        </div>
	</div>
	
	
</div>
<div class="main_content" style="background:#fafaf4">
    <div class="container winter-camp">
        <div class="col-lg-5 col-md-12 col-sm-12">
            <img src="{{asset('public/')}}/images/winter-camp.jpg" alt="">
        </div>
        <div class="col-lg-7 col-md-12 col-sm-12">
            <p class="new-text-color">Open to students from all schools (age 2-13)
            Theme: Adaptability to the changing world<br>
            We are living in the changing world and we are facing even greater changes in the near future. We are approaching a population of nine billion by 2050, the necessity of ceasing to burn fossil fuels, and a rapidly accelerating climate shift that will raise sea levels and flood coastal cities and low-lying areas where millions live. Agricultural patterns will change, species will be stressed, forest fires will be more common and widespread, and storms more intense. Disease patterns will change. Water shortages will cause conflicts. 
            In order to mitigate and adapt to the negative impacts of these changes we will need to find huge resources and change the way we learn, live and do everything.<br>
            
            Knowledge, Skills and Attitudes<br>
            Confidence, Physical development, Motor skills, Creativity<br>
            English, Research, Deep Thinking, Presentation<br>
            Field trips: Golf, Horse riding, Movies<br>
            Student grouping by age <br>
            -	2-5 years old<br>
            -	6-8 years old<br>
            -	 9+ years old
            </p>
            
            <iframe class="video-wintercamp" src="https://www.youtube.com/embed/5FoDjBInxLg" 
            frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media;
            gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="text-center">
                <a target="_blank" href="winter-camp-register" class="btn_register_online" type="button" >
                    REGISTER ONLINE
                </a>
            </div>
        </div>
    </div>  
    
    <div class="container">
        <img class="banner" width="100%" src="{{asset('public/')}}/images/CHILDREN-01.png" alt="">
    </div>
</div>
@endsection
