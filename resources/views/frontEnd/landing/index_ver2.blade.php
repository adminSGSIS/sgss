<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SgStar International</title>
    <link href="/public/landing/images/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon"/>

    <!-- Google fonts -->
    <!--<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">-->
	<link href='https://fonts.googleapis.com/css?family=Nunito:400,400i,700,700i,800,800i,900,900i' rel='stylesheet'>
    <!-- Bootstrap -->
    <link href="/public/landing/assets/css/bootstrap.min.css" rel="stylesheet">
  
    <!-- Font-awesome -->
    <link href="/public/landing/assets/css/font-awesome.min.css" rel="stylesheet">
  
    <!-- Flaticon -->
    <link href="/public/landing/assets/flaticon/flaticon.css" rel="stylesheet">
  
    <!-- Style -->
    <link href="public/landing/assets/css/style.css" rel="stylesheet">
  
    <!-- Responsive -->
    <link href="/public/landing/assets/css/responsive.css" rel="stylesheet">
  </head>
   @include('frontEnd.modals_form.registration')
  <body id="scroll-top">

    <!-- Preloader start here -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>

  	<header>
  		<div class="main-menu">
		  	<div class="container">
			 
		  		<div class="row no-gutters">
		  			<nav class="main-menu-area w-100">
					    <div class="logo-area">
						<a class="set_logo" href="/"></a>
							<!--<button type="button" class="navbar-toggle collapsed d-md-none" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>-->
							<li class="contact_banner_mb">
									<button><a href="tel:08 8800 6996"><i class="fa fa-phone" style="color: #fff;font-size: 25px;margin-top: 20%" aria-hidden="true"></i></a></button><span>08 8800 6996</span> 
							</li>
					    </div>
					    	

						<div class="menu-area" >
							<ul class="menu">
								<li><a href="#about">VỀ CHÚNG TÔI</a></li>
								<li><a href="#course">CÁC KHÓA HỌC</a></li>
								<li><a href="#strengths">THẾ MẠNH</a></li>
								<li><a href="#contact">LIÊN HỆ</a></li>
								<li class="contact_banner">
									<button><a href="tel:028 3742 3222"><i class="fa fa-phone" style="color: #fff;font-size: 25px;margin-left:-5px" aria-hidden="true"></i></a></button><span>028 3742 3222</span> 
								</li>
							</ul>
							
						</div>
					</nav>
				</div>
			</div>
  		</div>
  	</header>
  	<!-- header End here -->


	<!-- Banner Start here -->
	<section class="banner">
		<div class="padding-0">
			<img  style="width:100%" src="/public/landing/images/banner/homepage.png" alt="">
		</div>
	</section>
  	<!-- Banner End here -->


  	<!-- About Us Start here -->
  	<section class="about" >
  		<div class="padding-70">
	  		<div class="container_1">
	  			<div class="row">
	  				<div class="col-lg-12 ">
						<iframe class="video-sgss"
							src = "https://www.youtube.com/embed/0O7An6JUdy8">
						</iframe>
						
	  				</div>
	  				
	  			</div><!-- row -->
	  		</div><!-- container -->
  		</div><!-- overlay -->
  	</section><!-- about -->
  	<!-- About Us End here -->


	<!-- Registration Star Start here -->
	<section class="container_4 col-lg-12" style="display:flex" id="registration">	
		<div class="left-gallery">
			<img src="public/landing/images/gallery/gallery_03.png" alt="">
		</div>
		<div class="right-gallery">
			<img  src="public/landing/images/gallery/gallery_01.png" alt="">
			<img  style="padding-top:15px"src="public/landing/images/gallery/gallery_02.png" alt="">
		</div>
		
	</section>
	<!-- Registration Star End here -->
	


	<!-- Location Start here -->
	<section  id="about">	
		<div class="padding-70">
		<div class="main_content">
			<div class="left-logo">
				<img src="public/landing/images/items/left_icon.png" alt="">
			</div>
			<div class="content col-lg-9">Mang đến cho con bạn một tuổi thơ vui vẻ, khỏe mạnh có thể giúp chúng thành công trong cuộc sống.
			Nhưng nhiều bậc cha mẹ tự hỏi, chính xác thì làm cách nào để nuôi dạy những đứa trẻ hạnh phúc trong thế giới ngày nay? 
			Bạn có thể giúp con bạn phát triển những kỹ năng đó bằng cách áp dụng thói quen lành mạnh, trong suốt một cuộc sống lâu dài. </br></br>
			
			Tại Trường Quốc tế Ngôi Sao Sài Gòn, chúng tôi chọn lọc các chương trình giảng dạy quốc tế tốt nhất dành cho trẻ từ 2-5 tuổi (IEYC)
			nhằm xây dựng nhân cách tuyệt vời và giúp trẻ trở thành những người học thành công suốt cuộc đời.</div>
			<div class="right-logo">
				<img src="public/landing/images/items/right_icon.png" alt="">
			</div>
		</div>	
		</div><!-- overlay -->
	</section><!-- about -->
	<!-- Location End here -->
	<div class="foot_animation">
			<img src="public/landing/images/items/foot.png" class="star foot_1" />
			<img src="public/landing/images/items/foot.png" class="star foot_2" />		
			<img src="public/landing/images/items/foot.png" class="star foot_3" />		
			<img src="public/landing/images/items/foot.png" class="star foot_4" />
		
	</div>

	<!-- Gallery Start here -->
	<section class="gallery padding-70" id="course">
		<div class="container_2">
			<div class="gallery_center">
				<img src="public/landing/images/classes/students.png" alt="">
			</div>
			<div class="gallery_frog">
				<img src="public/landing/images/classes/06.png" alt="">
			</div>
			<div class="preschool">
				<p class="class_title">PRESCHOOL </br>2-3 TUỔI</p>
			</div>
			<div class="preschool_img">
				<img src="public/landing/images/classes/05.png" alt="">
			</div>
			<div class="nursery">
				<p class="class_title">NURSERY </br>3-4 TUỔI</p>
			</div>
			<div class="nursery_img">
				<img src="public/landing/images/classes/04.png" alt="">
			</div>
			<div class="reception">
				<p class="class_title">RECEPTION </br>4-5 TUỔI</p>
				
			</div>
			<div class="reception_img">
				<img src="public/landing/images/classes/07.png" alt="">
			</div>
		</div><!-- container -->
	</section>
	<!-- Gallery End here -->



  	<!-- Courses Start here -->
  	<section class="classes" id="course">
		<div class="classes_img">
			<img src="public/landing/images/gallery/two_students.png" alt="">
		</div>
		<div class="square_content">
					<p class="small">Một không gian đẹp và thân thiện</br>với thiên nhiên rộng <a class="small_a">7.000</a>m² </br>với thảm cỏ tự nhiên, sân chơi ngoài trời, sân chơi cát, xe trượt tuyết, ziplines,….
					</p>
	    </div>	
  	</section><!-- classes -->
  	<!-- Course End here -->

	<!-- Testimonial Start here -->
  	<section class="testimonial" id="strengths">
		<div class="left_one">
			<p><a class= "small_a">SĨ SỐ LỚP NHỎ</a> </br>với sự chăm sóc tận tình từ </br>
			đội ngũ giáo viên </br>có bằng cấp giảng dạy ,</br> cùng sự trợ giảng và</br> bảo mẫu nhiều kinh nghiệm.
			</p>
			<div class="foot_animation_2">
        		<img src="public/landing/images/items/foot.png" class="star foot_1" />
        		<img src="public/landing/images/items/foot.png" class="star foot_2" />		
        		<img src="public/landing/images/items/foot.png" class="star foot_3" />		
        		<img src="public/landing/images/items/foot.png" class="star foot_4" />
		
	        </div>
		</div>
		<div class="right_one">
			<img src="public/landing/images/strengths/10.png" alt="">
		</div>
	</section><!-- testimonial -->

	<section class="strengths_two col-lg-12">
		<div class="left_two">
			<img src="public/landing/images/strengths/11.png" alt="">
		</div>
		<div class="right_two">
		    
			<img src="public/landing/images/strengths/12.png" alt="">
			<div class="foot_animation_3">
        		<img src="public/landing/images/items/foot.png" class="star foot_1" />
        		<img src="public/landing/images/items/foot.png" class="star foot_2" />		
        		<img src="public/landing/images/items/foot.png" class="star foot_3" />		
        		<img src="public/landing/images/items/foot.png" class="star foot_4" />
		
	        </div>
			<p>Chương trình International Early</br>
				 Years Curriculum được áp dụng  </br>
				ở hơn 60 nước trên thế giới.</p>
		</div>
	</section>
	<!-- Testimonial End here --> 
	<section class="strengths_two">
		<div class="left_three">
		    <div class="foot_animation_4">
    			<img src="public/landing/images/items/foot.png" class="star foot_1" />
    			<img src="public/landing/images/items/foot.png" class="star foot_2" />		
    			<img src="public/landing/images/items/foot.png" class="star foot_3" />		
    			<img src="public/landing/images/items/foot.png" class="star foot_4" />
        	</div>
			<p><a class= "small_a">CỐ VẤN VÀ HỖ TRỢ</a> </br>và 
			   miễn phí cho học sinh cần </br> chương trình giáo dục đặc biệt
			</p>
		</div>
		<div class="right_one">
			<img src="public/landing/images/strengths/13.png" alt="">
		</div>
	
	</section>

	<section class="strengths_two col-lg-12">
		<div class="left_four">
			<img src="public/landing/images/strengths/14.png" alt="">
		</div>
		<div class="right_four">
		     <div class="foot_animation_5">
    			<img src="public/landing/images/items/foot.png" class="star foot_1" />
    			<img src="public/landing/images/items/foot.png" class="star foot_2" />		
    			<img src="public/landing/images/items/foot.png" class="star foot_3" />		
    			<img src="public/landing/images/items/foot.png" class="star foot_4" />
        	</div>
			<p  class= "small_a">DỊCH VỤ </br>BUS</p>
		</div>
	</section>
	<section class="strengths_two">
		<div class="left_five">
		    <div class="foot_animation_6">
    			<img src="public/landing/images/items/foot.png" class="star foot_1" />
    			<img src="public/landing/images/items/foot.png" class="star foot_2" />		
    			<img src="public/landing/images/items/foot.png" class="star foot_3" />		
    			<img src="public/landing/images/items/foot.png" class="star foot_4" />
        	</div>
			<p><a class= "small_a">INSPIRED LEARNERS.</a> </br>
			   GLOBAL THINKERS.</p>
		</div>
		<div class="right_one">
			<img src="public/landing/images/strengths/15.png" alt="">
		</div>
	
	</section>
	<section class="enrolment" id="gallery">
		<div class="container1">
			<div class="rainbow">
				<img src="public/landing/images/enrolment/16.png" alt="">
			</div>
			<div class="enrolment_content">
				CÁC ĐẶC ĐIỂM </br>CẦN THIẾT CHO </br>  CUỘC SỐNG & HỌC TẬP Ở </br>
				<a class="small_a">THẾ KỶ 21</a>
			</div>
		</div><!-- container -->
	</section>


  	<!-- Footer Start here -->
  	<footer id="contact">
  		<div class="footer-top padding-70">
  			<div class="container_3">
  				<div class="row">
  					<div class="col-lg-6 col-md-6 col-xs-12">
  						<div class="footer-item">
						  <div class="title"><img src="/public/landing/images/footer/logo.png" alt="logo" class="img-responsive"></div>
  							<div class="footer-about">
  								<ul>
  									<li><span><i class="fa fa-home" aria-hidden="true"></i></span>Đường Sử Hy Nhan, Khu dân cư số 5,</br>
									&nbsp &nbsp &nbsp &nbsp Phường Thạnh Mỹ Lợi, Quận  2, TP.Ho Chi Minh</li>
									<li><span><i class="fa fa-globe" aria-hidden="true"></i></span>sgstar.edu.vn &nbsp  &nbsp &nbsp  &nbsp &nbsp  &nbsp</li>
									<li><span><i class="fa fa-envelope-square" aria-hidden="true"></i></span>admissions@sgstar.edu.vn</li>
  									<li><span><i class="fa fa-phone" aria-hidden="true"></i></span>(+84)28 3742 3222   </li>
									<li><span><i class="fa fa-phone" aria-hidden="true"></i></span>(+84)8 8800 6996  </li>
									<div class="link_us">
										<h3>LINK US</h3>
										<a href="https://fb.com/saigonstarschool"><img src="public/landing/images/footer/bt-facebook.png" alt=""></a>
										<a href="https://www.youtube.com/channel/UCcKlFmxYK5C6piLNe18hhRg"><img src="/public/landing/images/footer/bt-youtube.png" alt=""></a>
									</div>
  								</ul>
  							</div>
							
  						</div>
					</div> 
					<div class="col-lg-6 col-md-6 col-xs-12">
						<div class="form_register">
							<img src="/public/landing/images/footer/form.png" alt="">
							<form action="{{url('/send-contact')}}" method="post">
							    @csrf
    							<div class="form_text">
    								<input type="text" autocomplete="off" required  placeholder= "Nhập tên của bạn*" name="name" id="">
    								<input type="email" autocomplete="off" required  placeholder= "Địa chỉ email*" name="email">
    								<input type="text" autocomplete="off" required placeholder= "Nhập số điện thoại*" name="phone">
    							</div>
    							<button class="btn_submit">Submit</button>
    						</form>
						</div>
					</div>  
  				</div><!-- row -->
  			</div><!-- container -->
			
  		</div><!-- footer top -->
  		<div class="bottom_footer">
				<p>Bản quyền thuộc về Sai Gon Star Internaltional School 2020</p>
		</div>
  	</footer>
  	<a class="page-scroll scroll-top" href="#scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
     <a href="#"  class="book_tour_mb"  data-toggle="modal" data-target="#registration" data-whatever="@mdo">
        <img src="public/landing/images/items/booktour_new.png" >
    </a>
  	<!-- Footer End here -->

	  </body>


    <!-- jquery -->
        <script src="/public/landing/assets/js/jquery-1.12.4.min.js"></script>
        <!-- Bootstrap -->
        <script src="/public/landing/assets/js/bootstrap.min.js"></script>
        <!-- Isotope -->
        <script src="public/landing/assets/js/isotope.min.js"></script>
      
        <!-- lightcase -->
        <script src="/public/landing/assets/js/lightcase.js"></script>
      
        <!-- counterup -->
        <script src="/public/landing/assets/js/jquery.counterup.min.js"></script>
      
        <!-- Swiper -->
        <script src="/public/landing/assets/js/swiper.jquery.min.js"></script>

        <!--progress-->
        <script src="/public/landing/assets/js/circle-progress.min.js"></script>

        <!--nstSlider-->
        <script src="/public/landing/assets/js/jquery.nstSlider.js"></script>

        <!--flexslider-->
        <script src="/public/landing/assets/js/flexslider-min.js"></script>
        <!--easing-->
        <script src="/public/landing/assets/js/jquery.easing.min.js"></script>
        <!-- custom -->
	    <script src="/public/landing/assets/js/custom.js"></script>
	
	<script>
		$(document).ready(function(){
		// Add smooth scrolling to all links
		$("a").on('click', function(event) {

			// Make sure this.hash has a value before overriding default behavior
			if (this.hash !== "") {
			// Prevent default anchor click behavior
			event.preventDefault();

			// Store hash
			var hash = this.hash;

			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 1200, function(){

				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
			});
			} // End if
		});
		});
    </script>
	 <!----------------- 16-10-2020 --------------------->
	 <script>
        $(document).ready(function() { var logo = $(".set_logo"); $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        
            if (scroll >= 200 && $(document).width()>900) {
              if(!logo.hasClass("set_logo_hide")) {
                logo.hide();
                logo.removeClass('set_logo').addClass("set_logo_hide").fadeIn( "slow");
              }
            } else {
                  if(!logo.hasClass("set_logo")) {
                    logo.hide();
                    logo.removeClass("set_logo_hide").addClass('set_logo').fadeIn( "slow");
                  }
                }
        
        });
        });
    </script>
	<script>
        $(document).ready(function() { var logo = $(".menu-area"); $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        
            if ($(document).width() >= 900 && scroll >= 200) {
                if(!logo.hasClass("menu-area_second")) {
                    logo.hide();
                    logo.removeClass('.menu-area').addClass("menu-area_second").fadeIn( "slow");
                }
            } if ($(document).width() >= 900 && scroll < 200){
                  if(!logo.hasClass(".menu-area")) {
                    logo.hide();
                    logo.removeClass("menu-area_second").addClass('.menu-area').fadeIn( "slow");
                }
            } if ($(document).width() < 900) {
                logo.hide();
            }
                
        
        });
        });
    </script>
    <!---------------------- 13.11.2020 ------------------------>
    <script>
        function showImages(el) {
            var windowHeight = jQuery( window ).height();
            $(el).each(function(){
                var thisPos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (topOfWindow + windowHeight - 200 > thisPos ) {
                    $(this).addClass("fadeIn");
                }
            });
        }

        // if the image in the window of browser when the page is loaded, show that image
        $(document).ready(function(){
            showImages('.star');
        });

        // if the image in the window of browser when scrolling the page, show that image
        $(window).scroll(function() {
            showImages('.star');
        });
    </script>
    <script>
        $(window).load(function(){        
          if($(window).width()<550){    
           		$('#registration').modal('show');
        	}
        }); 
    </script>
 
</html>