@extends('frontEnd/mails/mail_template')
@section('mail-content')
  <!--Content-->
    <div style="text-align:center">
        <h1 style="text-align:center">FEEDBACK FORM</h1>
        <h2>Teacher's Name: {{ $data1['teacher-name'] }}</h2>
    </div>
    <table class="table" style="text-align: center">
        <tbody>
            <tr>
                <td colspan="6">
                    Student's name: <b>{{ $data1['studentName'] }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Father's name: <b>{{ $data1['studentFather'] }}</b>
                </td>
                <td colspan="3">
                    Father's phone: <b>{{ $data1['fatherPhone'] }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Mother's name: <b>{{ $data1['studentMother'] }}</b>
                </td>
                <td colspan="3">
                    Mother's phone: <b>{{ $data1['motherPhone'] }}</b>
                </td>
            </tr>
            <tr>
                <td>
                    Listening: <b>{{ $data1['listen'] }}</b>.
                </td>
                <td>
                   Speaking:  <b>{{ $data1['speaking'] }}</b>.
                </td>
                <td>
                    Reading: <b>{{ $data1['reading'] }}</b>.
                </td>
                <td colspan="3">
                    Writing: <b>{{ $data1['writing'] }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    EAL Assessments:  <b>{{ $data1['ela-assessments'] }}</b>
                 </td>
                <td colspan="3">
                    EAL Support: <b>{{ $data1['eal-support'] }}</b> (hour/week)
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    SEN Assessments: <b>{{ $data1['sen'] }}</b>
                </td>
                <td colspan="3">
                    SEN Support: <b>{{ $data1['sen-support'] }}</b> (hour/week)
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    CAT4 Assessment: <b>{{ $data1['cat4-assessment'] }}</b>
                </td>
                <td>
                    Quantitative: <b>{{ $data1['quantitative'] }}</b>
                </td>
                <td>
                    Verbal: <b>{{ $data1['verbal'] }}</b>
                </td>
                <td>
                    Non-verbal: <b>{{ $data1['non-verbal'] }}</b>
                </td>
                <td>
                    Spatial: <b>{{ $data1['spatia_l'] }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Class teacher: <b>{{ $data1['class-teacher'] }}</b>
                </td>
                <td colspan="3">
                    Head teacher recommendation: <b>{{ $data1['head-teacher'] }}</b>
                </td>
            </tr>
        </tbody>
    </table>
    <!--Content End-->
@endsection