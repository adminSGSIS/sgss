
<div class="modal fade" id="booktournew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background: transparent;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="padding:0">
          <form method="POST" action="{{url('/book-tour')}}" accept-charset="UTF-8" class="form-horizontal">
            @csrf
                 
              <div class="add-visitor">
                <div class="row mt-25">
                    <div class="row-bookatour">
                        <div class="input-effect">                                                                                                    
                        <input style="font-family:Roboto" class="form-control" type="text" name="name" autocomplete="off" value="" required="" placeholder="*Name">
                            <span class="focus-border textarea"></span>
                          
                        </div>
                    </div>
                </div>
                <div class="row mt-25">
                    <div class="row-bookatour">
                        <div class="input-effect">                                                                                                    
                        <input style="font-family:Roboto" class="form-control" type="text" name="address" autocomplete="off" value="" required="" placeholder="*Address">
                            <span class="focus-border textarea"></span>
                          
                        </div>
                    </div>
                </div>
                <!--<div class="row mt-25">-->
                <!--    <div class="row-bookatour">-->
                <!--        <div class="input-effect">                                                                                                    -->
                <!--        <input style="font-family:Roboto" class="form-control" type="text" name="purpose" autocomplete="off" value="" required="" placeholder="*Purpose">-->
                <!--            <span class="focus-border textarea"></span>-->
                          
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->
                <!--<div class="row mt-25">-->
                <!--    <div class="row-bookatour">-->
                <!--        <div class="input-effect">                                                                                                    -->
                <!--        <input style="font-family:Roboto" class="form-control" type="text" name="no_of_person" autocomplete="off" value="" required="" placeholder="*Number of visitor">-->
                <!--            <span class="focus-border textarea"></span>-->
                          
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->

                <div class="row mt-25">
                    <div class="row-bookatour">
                        <div class="input-effect">                                                                                                    
                        <input style="font-family:Roboto" class="form-control" type="text" name="email" autocomplete="off" value="" required="" placeholder="*Email">
                            <span class="focus-border textarea"></span>
                          
                        </div>
                    </div>
                </div>
                <div class="row mt-25">
                    <div class="row-bookatour">
                        
                            <input style="font-family:Roboto" class="form-control" id="telephone2" class="telephone" type="tel" name="phone" value="" required="" placeholder="*Phone">
                            <input type="hidden" name="dial_code" id="dial2">
                            <input type="hidden" name="country_name" id="country2">
                        
                    </div>
                </div>
                <div class="row mt-25">
                    <div class="row-bookatour">
                        <div class="input-effect">                                                                                                    
                        <input id="visitday_modal" style="font-family:Roboto" class="form-control" type="text" name="date" autocomplete="off" value="" required="" placeholder="*Date of visit">
                            <span class="focus-border textarea"></span>
                          
                        </div>
                    </div>
                </div>
                <div class="row mt-25">
                    <div class="row-bookatour">
                        <div class="input-effect">                                                                                                    
                        <textarea style="font-family:Roboto" class="form-control" type="text" name="description" autocomplete="off" value="" required="" placeholder="Message to school"></textarea>
                            <span class="focus-border textarea"></span>
                          
                        </div>
                    </div>
                </div>
                            
                <div class="row mt-25">
                    <div class="col-lg-12 text-center">
                        <button class="submit-btn fix-gr-bg " data-toggle="tooltip" title="" data-original-title="">
                            <span class="ti-check1">Submit</span>
                        </button>
                    </div>
                </div>
              </div>
    
          </form>
        </div>
        
      </div>
    </div>
  </div>