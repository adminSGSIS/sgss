<style>
    body {
        background-color: #efefef;
    }

    .profile-pic {
        background: #f3f3f3;
        display: block;
    }

    .avatar {
        padding-left: 6%;
    }

    .file-upload {
        display: none;
    }

    #job_apply .modal-content .modal-body {
        padding: 10px;
    }

    #job_apply .modal-dialog {
        max-width: 70%;
    }

    #job_apply .form-group {
        display: flex;
    }

    #job_apply .form-group input {
        margin-left: 5%;
        max-width: 80%;
        border-radius: 12px;
    }

    #job_apply .form-group textarea {
        margin-left: 5%;
        max-width: 80%;
        border-radius: 12px;
    }

    #job_apply .form-group label {
        width: 25%;
    }

    #job_apply .form-group2 {
        display: flex;
        margin-bottom: 2%;
        margin-left: 2%;
        margin-right: 2%;
    }

    #job_apply .form-group2 input {
        margin-left: 10%;
        max-width: 80%;
        border-radius: 12px;
    }

    #job_apply .form-group2 textarea {
        margin-left: 10%;
        max-width: 80%;
        border-radius: 12px;
    }

    #job_apply .form-group2 label {
        width: 28%;
    }

    .pt-70 {
        padding-top: 70px;
    }

    .logo_form {
        width: 65%;
    }

    .circle {
        border-radius: 1000px !important;
        overflow: hidden;
        width: 160px;
        height: 160px;
        border: 5px solid rgba(42, 184, 18, 0.3);
        position: absolute;
        top: 72px;
    }

    img {
        max-width: 100%;
        height: auto;
    }

    .p-image {
        position: absolute;
        bottom: 46%;
        left: 18%;
        color: #666666;
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .p-image:hover {
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .title h3 {
        color: darkturquoise;
        font-style: initial;
        margin-left: 10%;

    }

    .modal-header h4 {
        font-size: 22px;
        font-style: normal;
    }

    .main_content {
        margin-left: auto;
        margin-right: auto;
    }

    .upload-button {
        font-size: 1.2em;
    }

    .upload-button:hover {
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
        color: #999;
    }

</style>
<div class="modal fade" id="job_apply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Apply For A Job</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('apply') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-12" style="display:flex">
                        <div col-4>
                            <div class="logo_form">
                                <img src="/public/images/logo.png" alt="logo" class="img-responsive">
                            </div>

                            <div class="avatar">
                                <div class="small-12 medium-2 large-2 columns">
                                    <div class="circle">
                                        <!-- User Profile Image -->
                                        <img class="profile-pic" src="">

                                        <!-- Default Image -->
                                        <!-- <i class="fa fa-user fa-5x"></i> -->
                                    </div>
                                    <div class="p-image">
                                        <i class="fa fa-camera upload-button"></i>
                                        <input required="" class="file-upload" name="avatar" type="file"
                                            accept="image/*" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-8 pt-40">
                            <div class="title">
                                <h3>CV ONLINE</h3>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" id="name" name="name" required="">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Gender:</label><br>
                                <input type="radio" name="gender" checked value="male"> Male
                                <input type="radio" name="gender" value="female"> Female
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Date of Birth</label>
                                <input type="date" class="form-control" name="date" value="{{ date('d/m/Y') }}" required="">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Phone:</label>
                                <input type="tel" class="form-control" id="phone" name="phone" required="">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Email:</label>
                                <input type="email" class="form-control" id="email" name="email" required="">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Address:</label>
                                <textarea class="form-control" id="message" name="address"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-11 main_content">
                        <div class="form-group2">
                            <label for="message-text" class="col-form-label">Graduated school:</label>
                            <input type="text" class="form-control" id="message" name="school" required=""></input>
                        </div>
                        <div class="form-group2">
                            <label for="message-text" class="col-form-label">Graduation year:</label>
                            <input type="text" class="form-control" id="message" name="graduation_year"
                                required=""></input>
                        </div>
                        <div class="form-group2">
                            <label for="message-text" class="col-form-label">Qualifications:</label>
                            <textarea class="form-control" id="message" name="qualifications" required=""></textarea>
                        </div>
                        <div class="form-group2">
                            <label for="message-text" class="col-form-label">Experience:</label>
                            <textarea class="form-control" id="message" name="experience" required=""></textarea>
                        </div>
                        <div class="form-group2">
                            <label for="message-text" class="col-form-label">About yourself:</label>
                            <textarea class="form-control" id="message" name="about" required=""></textarea>
                        </div>
                        <label for="recipient-name" class="col-form-label">Attachments:</label>
                        <div class="custom-file">
                            <input type="file" id="customFile" class="custom-file-input" name="url_file" required="">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Apply</button>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
