<div class="modal fade" id="registration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{url('/send-contact')}}" accept-charset="UTF-8" class="form-horizontal">
            @csrf
                 
              <div class="add-visitor">
                                            
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-effect">
                                <input style="font-family:Nunito"class="primary-input form-control" type="text" name="name" autocomplete="off" value="" required="" placeholder="Enter your name*" >
                              
                                <span class="focus-border"></span>
                            </div>

                        </div>
                    </div>  

                <div class="row mt-25">
                    <div class="col-lg-12">
                        <div class="input-effect">                                                                                                    
                        <input style="font-family:Nunito" class="primary-input form-control" type="text" name="email" autocomplete="off" value="" required="" placeholder="Enter your email*">
                            <span class="focus-border textarea"></span>
                          
                        </div>
                    </div>
                </div>
                <div class="row mt-25">
                        <div class="col-lg-12">
                            
                                <input style="font-family:Nunito" class="primary-input form-control" id="telephone2" class="telephone" type="tel" name="phone" value="" required="" placeholder="Enter your phone number*">
                                <input type="hidden" name="dial_code" id="dial2">
                                <input type="hidden" name="country_name" id="country2">
                            
                        </div>
                    </div>
                            
                    <div class="row mt-40">
                        <div class="col-lg-12 text-center">
                            <button class="submit-btn fix-gr-bg " data-toggle="tooltip" title="" data-original-title="">
                                <span class="ti-check">Submit</span>
                            </button>
                        </div>
                    </div>
              </div>
    
          </form>
        </div>
        
      </div>
    </div>
  </div>