@extends('frontEnd.home.layout.front_master')
@section('main_content')
<style type="text/css">
    
    
    
    
</style>
<div class="banner">
            <img class="banner" width="100%" src="{{ asset('public/images/banner/banner_03.jpg') }}" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>{{$career->name}}</h3>
                </div>
            </div>
        </div>
<div class="c-color">
<div class="container hidden-print">
        <div class="row">
            <div class="col-lg-9 left-col new-text-color">                    
					<br>
					{!!$career->description!!}
            </div>
            
            <div class="col-lg-3 sub_content">
             
                <div class="right-col">
                	
                    <a href="#" title="View Details" data-toggle="modal" data-target="#job_apply" data-whatever="@mdo">
                    <div>
                        <div class="ItemImage">
                        <img class="card-img" src="https://2.bp.blogspot.com/-YE3MJa5l1hg/V0GK8bkPNYI/AAAAAAAAAU8/MeX2uuOWGeMXtYTQMkfDufWYIyd7_OOIwCLcB/s1600/Job-Offer-350x287.jpg" alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Apply for our job opportunities ?</h5>
                            <p class="card-text">Want to join us? click here!</p>
                            <div class="row">                                
                            </div>
                        </div>
                    </div>
                    </a>
                </div>         
            </div>              
        </div>		
</div>
</div>
@include('frontEnd.modals_form.job_apply')
@endsection
@section('script')
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<script>
  $(document).ready(function() {

        
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    $(".file-upload").on('change', function(){
        readURL(this);
    });

    $(".upload-button").on('click', function() {
      $(".file-upload").click();
    });
    });
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection
