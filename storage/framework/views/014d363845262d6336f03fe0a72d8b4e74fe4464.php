
<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Medicine import Manager</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>

            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="white-box">
                    <?php if(isset($editData)): ?>
                    <form action="<?php echo e(route('medicine-management-update',$editData->id)); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <?php else: ?>
                        <form action="<?php echo e(route('medicine-management-store')); ?>" method="POST">
                            <?php echo csrf_field(); ?>
                            <?php endif; ?>

                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="input-effect">
                                            <input class="primary-input form-control" type="text" name="name"
                                                value="<?php echo date("Y/m/d");?>" readonly>
                                            <label><?php echo app('translator')->get('lang.date'); ?> <span>*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-effect">
                                            <select name="staff" id="" class="niceSelect w-100 bb form-control">
                                                <option disabled selected><label><?php echo app('translator')->get('employee'); ?>
                                                        <span>*</span></label>
                                                </option>
                                                <?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($staff->id); ?>"
                                                    <?php if( !empty($editData)) {if($editData->staff==$staff->id){echo 'selected';} }  ?>>
                                                    <?php echo e($staff->full_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-effect">
                                            <select name="department" id="" class="niceSelect w-100 bb form-control">
                                                <option disabled selected><label><?php echo app('translator')->get('department'); ?>
                                                        <span>*</span></label>
                                                </option>
                                                <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($department->id); ?>"
                                                    <?php if( !empty($editData)) {if($editData->deparment==$department->id){echo 'selected';} }  ?>>
                                                    <?php echo e($department->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-effect">
                                            <select name="supplier" id="" class="niceSelect w-100 bb form-control">
                                                <option disabled selected><label><?php echo app('translator')->get('supplier'); ?>
                                                        <span>*</span></label>
                                                </option>
                                                <?php $__currentLoopData = $suppliers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supplier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($supplier->id); ?>"
                                                    <?php if( !empty($editData)) {if($editData->supplier==$supplier->id){echo 'selected';} }  ?>>
                                                    <?php echo e($supplier->company_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-5" style="padding-top: 25px">
                                    <h3><?php echo app('translator')->get('Medicine detail'); ?></h3>
                                    <div class="container">
                                        <div class="row mb-5">
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <label><?php echo app('translator')->get('Medicine Code'); ?> <span>*</span></label>
                                                    <select id="abc" name="medicinecode"
                                                        class="niceSelect w-100 bb form-control">
                                                        <?php $__currentLoopData = $medicines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $medicine): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option  value="<?php echo e($medicine->id); ?>"
                                                            <?php if( !empty($editData)) {if($editData->medicine_code==$medicine->id){echo 'selected';} }  ?>>
                                                            <?php echo e($medicine->medicine_code); ?>

                                                        </option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">

                                                
                                                <div class="input-effect">
                                                    <label><?php echo app('translator')->get('Medicine type'); ?> <span>*</span></label>
                                                    <select name="medicine_type" id=""
                                                        class="niceSelect w-100 bb form-control">
                                                        <?php $__currentLoopData = $medicine_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $medicine_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($medicine_type->id); ?>"
                                                            <?php if( !empty($editData)) {if($editData->medicine_type==$medicine_type->id){echo 'selected';} }  ?>>
                                                            <?php echo e($medicine_type->name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                    <span class="focus-border"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-12 mb-5">
                                                        <div class="no-gutters input-right-icon">
                                                            <div class="input-effect">
                                                                <input class="primary-input form-control date"
                                                                    type="text" name="importdate" id="apply_date"
                                                                    required
                                                                    value="<?php echo e(!empty($editData)? $editData->import_date: ''); ?>">
                                                                <label><?php echo app('translator')->get('import Date'); ?> <span>*</span></label>
                                                                <span class="focus-border"></span>
                                                            </div>
                                                            <div class="col-auto">
                                                                <button class="" type="button">
                                                                    <i class="ti-calendar" id="apply_date_icon"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="input-effect">
                                                            <input class="primary-input form-control" type="text"
                                                                name="amount"
                                                                value="<?php echo e(isset($editData)? $editData->amount: ''); ?>"
                                                                required>
                                                            <label><?php echo app('translator')->get('amount'); ?> <span>*</span></label>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="col-lg-6">
                                                <div class="input-effect sm2_mb_20 md_mb_20">
                                                    <textarea required class="primary-input form-control" name="note"
                                                        autocomplete="off" id="" cols="30"
                                                        rows="8"><?php echo e(!empty($editData) ? $editData->note:''); ?></textarea>
                                                    <label><?php echo app('translator')->get('note'); ?><span> *</span> </label>
                                                    <span class="focus-border"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip"
                                            title="<?php echo e(@$tooltip); ?>">
                                            <span class="ti-check"></span>
                                            <?php echo e(!isset($editData)? "save":"update"); ?>

                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Medicine <?php echo app('translator')->get('lang.list'); ?></h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                                <?php if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(session()->get('message-success-delete')); ?>

                                        </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                        <div class="alert alert-danger">
                                            <?php echo e(session()->get('message-danger-delete')); ?>

                                        </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>No.</th>
                                    <th><?php echo app('translator')->get('staff '); ?></th>
                                    <th><?php echo app('translator')->get('deparment '); ?></th>
                                    <th><?php echo app('translator')->get('supplier'); ?></th>
                                    <th><?php echo app('translator')->get('medicine_code'); ?></th>
                                    <th><?php echo app('translator')->get('amount'); ?></th>
                                    <th><?php echo app('translator')->get('import_date'); ?></th>
                                    <th><?php echo app('translator')->get('medicine_type'); ?></th>
                                    <th><?php echo app('translator')->get('note'); ?></th>
                                    <th><?php echo app('translator')->get('lang.action'); ?></th>
                                </tr>
                            </thead>
                            <?php $count=1; ?>
                            <?php $__currentLoopData = $medicineManagements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tbody>
                                <th><?php echo e($count++); ?></th>
                                <th><?php echo e($value->staffRelation->full_name); ?></th>
                                <th><?php echo e($value->departmentRelation->name); ?></th>
                                <th><?php echo e($value->supplierRelation->company_name); ?></th>
                                <th><?php echo e($value->medicineRelation->medicine_code); ?></th>
                                <th><?php echo e($value->amount); ?></th>
                                <th><?php echo e($value->import_date); ?></th>
                                <th><?php echo e($value->medicineTypeRelation->name); ?></th>
                                <th><?php echo e($value->note); ?></th>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            <?php echo app('translator')->get('lang.select'); ?>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">

                                            <a class="dropdown-item"
                                                href="<?php echo e(url('medical/medicine-management-edit/'.$value->id)); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                            <a href="<?php echo e(url('medical/medicine-delete/'.$value->id)); ?>"
                                                class="dropdown-item"><?php echo app('translator')->get('lang.delete'); ?></a>
                                        </div>
                                    </div>
                                </td>
                            </tbody>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/Medical\Resources/views/Medicine/medicine_management.blade.php ENDPATH**/ ?>