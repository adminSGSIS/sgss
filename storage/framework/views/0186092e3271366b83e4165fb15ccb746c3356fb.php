<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Customer Facebook</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-8 col-md-6">
                <div class="main-title">
                    <h3 class="mb-30">Customer <?php echo app('translator')->get('lang.information'); ?> </h3>
                </div>
            </div>
            <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg">
                <a href="/customer-facebook/search-customer" class="primary-btn small fix-gr-bg">
                    <?php echo app('translator')->get('lang.all'); ?> Customer List
                </a>
            </div>
        </div>
        <?php if(isset($editData)): ?>
            <?php if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'customer-facebook/'. $editData->id . '/edit', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

            <?php endif; ?>
        <?php elseif(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'customer-facebook/add', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

        <?php endif; ?>
        <div class="row">
            <div class="col-lg-12">
                <?php if(session()->has('message-success')): ?>
                <div class="alert alert-success">
                  <?php echo e(session()->get('message-success')); ?>

              </div>
              <?php elseif(session()->has('message-danger')): ?>
              <div class="alert alert-danger">
                  <?php echo e(session()->get('message-danger')); ?>

              </div>
              <?php endif; ?>
              <div class="white-box">
                <div class="">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-title">
                                <h4><?php echo app('translator')->get('lang.basic_info'); ?></h4>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-20">
                        <div class="col-lg-12">
                            <hr>
                        </div>
                    </div>

                    <input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>"> 

                    <div class="row mb-30">
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="text" required name="customer_name" value="<?php echo e(isset($editData) ? $editData->customer_name : ''); ?>">
                                <span class="focus-border"></span>
                                <label>Customer Name <span>*</span> </label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="email" required name="email" value="<?php echo e(isset($editData) ? $editData->email : ''); ?>">
                                <span class="focus-border"></span>
                                <label><?php echo app('translator')->get('lang.email'); ?> <span>*</span> </label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="number" min="7" required name="phone_number" value="<?php echo e(isset($editData) ? $editData->phone_number : ''); ?>">
                                <span class="focus-border"></span>
                                <label><?php echo app('translator')->get('lang.phone_number'); ?></label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-effect">
                                <input class="primary-input form-control" type="text" required name="expected_class" value="<?php echo e(isset($editData) ? $editData->expected_class : ''); ?>">
                                <span class="focus-border"></span>
                                <label>Expected Class</label>
                            </div>
                        </div>

                        <div class="col-lg-12 mt-3">
                            <div class="input-effect">
                                <textarea class="primary-input form-control" cols="0" rows="4" name="notes"><?php echo e(isset($editData) ? $editData->notes : ''); ?></textarea>
                                <label>notes <span>*</span> </label>
                                <span class="focus-border textarea"></span>
                            </div>
                        </div>
                        
                    </div>
            <div class="row mt-40">
                <div class="col-lg-12 text-center">
                    <button class="primary-btn fix-gr-bg">
                        <span class="ti-check"></span>
                        <?php if(isset($editData)): ?>
                            <?php echo app('translator')->get('lang.update'); ?> customer
                        <?php else: ?>
                            <?php echo app('translator')->get('lang.save'); ?> customer
                        <?php endif; ?>
                    </button>
                </div>
            </div>
    </div>
    <?php echo e(Form::close()); ?>

    </div>
</section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/CRM/Resources/views/CustomerFacebook/createCustomer.blade.php ENDPATH**/ ?>