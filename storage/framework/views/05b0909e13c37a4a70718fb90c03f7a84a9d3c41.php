<?php $__env->startSection('css'); ?>
<style>
    .pdf {
        display: none;
        font-family: 'Roboto', sans-serif;
        color: rgba(66, 64, 64, 0.822);
    }
    
    body {
        -webkit-print-color-adjust: exact !important;
        height: 100%;
        width: 100%;
    }
    .logan{
        background-color: #31b6f0;
        width:100%;
        height:125px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 0px 0px 30px 30px;
        font-size: 80px;
    }
    .logan h1{
        font-size: 70px;
        color: #fff;
        font-weight: bold;
    }
    .left-icon{
        position: absolute;
        left: 85px;
    }
    .right-icon{
        position: absolute;
        right: -135px;
    }
    .letter{
        padding-left: 70px!important;
        padding-right:70px!important;
        font-size:22px;
        padding:8px;
        line-height:1.3;
    }
    .ft200 {
        font-size: 200% !important;
    }

    .p-2>h1 {
        font-size: 100px;
        color: #fff;
    }
    div > h1{

    }
    .footer{
        bottom: 0;position: absolute;width: 100%;
    }
    @page  {
        margin-bottom: 0;
        margin-top: 0;
    }
    
    @media  print {
        #message_box,
        footer,
        .print {
            display: none !important;
        }

        .pdf {
            display: block;
        }

        #view-slip {
            display: none;
        }

        #pay-slip {
            display: none;
        }
    }
</style>
<?php $__env->stopSection(); ?>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
<div class="pdf">
        <img src="<?php echo e(asset('public/header_letter.png')); ?>" width="100%">
        <div style="padding-top:30px">
            <div class="letter" style="text-align: justify">
                <div style="padding-bottom: 55px;padding-top:45px">
                    <div class="logan">
                        <div class="left-icon">
                            <img src="<?php echo e(asset('public/tmp_image/left_icon.png')); ?>" alt="" width="22%">
                        </div>
                        <h1>WELCOME LETTER</h1>
                        <div class="right-icon">
                            <img src="<?php echo e(asset('public/tmp_image/right_icon.png')); ?>" alt="" width="22%">
                        </div>
                    </div>
                </div>
                
                <p>Dear parent of <?php echo e($slip->student->full_name); ?>,</p>
                <br>
                Welcome to Saigon Star International School; a boutique international school with a real sense of family 
                at its heart. Having been at Saigon Star since 2014, I feel incredibly proud of the journey we have been 
                on together; with every single member of our community contributing to our growth, development and 
                improvement, establishing us as a diverse, inclusive and welcoming school that is fully focused on learning 
                and enrichment. <br><br>
                We are proud to be the first and only international school that is accredited with the International Primary 
                Curriculum (IPC) in Vietnam; a curriculum which we believe helps us to achieve our Shared Vision, embrace 
                our community’s wide diversity and foster a love for lifelong learning. If you haven’t already, we would 
                encourage you to come and enjoy a visit to our peaceful, green and tranquil surroundings of Thu Duc City, 
                HCMC, and see first-hand the tangible development of our learner’s intrinsic values, balanced with their 
                academic goals. Our caring, supportive and internationally qualified teachers demonstrate their capacity 
                to provide more personalized learning and a strong commitment to ensuring your child is healthy, happy 
                and learning focused. <br><br>
                At Saigon Star, we supplement the IEYC (International Early Years Curriculum), the IPC with the English 
                National Curriculum. The IEYC is integrated with components of the child-centered approach, and we 
                provide specialist teachers with expertise in Sports, Music, Performing Arts and Languages. <br><br>
                We hope that you will join our fantastic family environment and support us in becoming the strong 
                foundation for your child on their long educational journey ahead. <br><br>
                Thank you for your interest in Saigon Star; I look forward to our strong partnership together.<br><br>

                Yours faithfully,<br><br><br>

                Mr. James Quantrill
                <br>
                Headteacher
            </div>
        </div>
            <div>
                <img src="<?php echo e(asset('public/footer_letter.png')); ?>" width="100%">
            </div>
</div><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/letter.blade.php ENDPATH**/ ?>