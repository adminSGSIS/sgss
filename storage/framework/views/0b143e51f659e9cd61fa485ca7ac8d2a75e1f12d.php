
<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Add Medicine Type</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>

            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30"><?php if(isset($editData)): ?>
                                <?php echo app('translator')->get('lang.edit'); ?>
                                <?php else: ?>
                                <?php echo app('translator')->get('lang.add'); ?>
                                <?php endif; ?>
                                Medicine Type
                            </h3>
                        </div>
                        <?php if(isset($editData)): ?>
                        <form action="<?php echo e(route('medicine_type-update',$editData->id)); ?>" method="post">
                            <!--Update-->
                            <?php echo csrf_field(); ?>
                            <?php else: ?>
                            <form action="<?php echo e(route('medicine_type-store')); ?>" method="POST">
                                <!--Create-->
                                <?php echo csrf_field(); ?>
                                <?php endif; ?>
                                <div class="white-box">
                                    <div class="add-visitor">

                                        <div class="row  mt-25">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="name" required
                                                        value="<?php echo e(!empty($editData)? $editData->name: ''); ?>"
                                                        maxlength="300">
                                                    <label><?php echo app('translator')->get('lang.name'); ?> <span>*</span></label>

                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="<?php echo e(!empty($editData)? $editData->id: ''); ?>">
                                        <div class="row mt-40">
                                            <div class="col-lg-12 text-center">
                                                <button class="primary-btn fix-gr-bg" data-toggle="tooltip"
                                                    title="<?php echo e(@$tooltip); ?>">
                                                    <span class="ti-check"></span>
                                                    <?php echo e(!isset($editData)? "save":"update"); ?>

                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Medicine Type <?php echo app('translator')->get('lang.list'); ?></h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                                <?php if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(session()->get('message-success-delete')); ?>

                                        </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                        <div class="alert alert-danger">
                                            <?php echo e(session()->get('message-danger-delete')); ?>

                                        </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>No.</th>
                                    <th><?php echo app('translator')->get('lang.name'); ?></th>
                                    <th><?php echo app('translator')->get('lang.action'); ?></th>
                                </tr>
                            </thead>
                            <?php $count=1; ?>
                            <?php $__currentLoopData = $medicine_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tbody>
                                <th><?php echo e($count++); ?></th>
                                <th><?php echo e($value->name); ?></th>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            <?php echo app('translator')->get('lang.select'); ?>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">

                                            <a class="dropdown-item"
                                                href="<?php echo e(url('medical/medicine_type-edit/'.$value->id)); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                                <a href="<?php echo e(url('medical/medicine_type-delete/'.$value->id)); ?>" class="dropdown-item"><?php echo app('translator')->get('lang.delete'); ?></a>
                                            

                                        </div>
                                    </div>
                                </td>
                            </tbody>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/Medical\Resources/views/Medicine/medicine_type.blade.php ENDPATH**/ ?>