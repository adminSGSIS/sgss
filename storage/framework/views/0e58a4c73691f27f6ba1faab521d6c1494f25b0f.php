<?php $__env->startSection('css'); ?>
 <style>
    .centerTd td, .centerTh th{
        text-align: center;
    }
 </style>
 <?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1><?php echo app('translator')->get('lang.list'); ?> Customers</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-8 col-md-6 col-sm-6">
                    <div class="main-title mt_0_sm mt_0_md">
                        <h3 class="mb-30  "><?php echo app('translator')->get('lang.select_criteria'); ?></h3>
                    </div>
                </div>

                <?php if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                 <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg col-sm-6 text_sm_right">
                    <a href="/customer-facebook/add" class="primary-btn small fix-gr-bg">
                        <span class="ti-plus pr-2"></span>
                        <?php echo app('translator')->get('lang.add'); ?> customer
                    </a>
                </div>
            <?php endif; ?>
            </div>
            <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => '/customer-facebook/search-customer', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'infix_form'])); ?>

            <div class="row">
                <div class="col-lg-12">
                <div class="white-box">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <input class="primary-input" type="text" name="year">
                                <label>search by year</label>
                                <span class="focus-border"></span>
                            </div>
                        </div>

                        
                        
                        <div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <input class="primary-input" type="text" name="customer_name">
                                <label>search by customers name</label>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                       
                        <div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <input class="primary-input" type="text" name="creator_name">
                                <label>search by creator name</label>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 mt-20 text-right">
                            <button type="submit" class="primary-btn small fix-gr-bg" id="btnsubmit">
                                <span class="ti-search pr-2"></span>
                                <?php echo app('translator')->get('lang.search'); ?>
                            </button>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            <?php echo e(Form::close()); ?>


            <?php if(@$customers): ?>
            <div class="row mt-40 full_wide_table">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 no-gutters">
                            <div class="main-title">
                                <h3 class="mb-0">Customers List </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row  ">
                        <div class="col-lg-12">
                            <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                                <thead>
                                    <?php if(session()->has('message-success') != "" ||
                                    session()->get('message-danger') != ""): ?>
                                    <tr>
                                        <td colspan="10">
                                            <?php if(session()->has('message-success')): ?>
                                            <div class="alert alert-success">
                                                <?php echo e(session()->get('message-success')); ?>

                                            </div>
                                            <?php elseif(session()->has('message-danger')): ?>
                                            <div class="alert alert-danger">
                                                <?php echo e(session()->get('message-danger')); ?>

                                            </div>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <?php endif; ?>
                                    <tr class="centerTh">
                                        <th><?php echo app('translator')->get('lang.staff'); ?> <?php echo app('translator')->get('lang.name'); ?></th>
                                        <th>name</th>
                                        <th>phone number</th>
                                        <th>email</th>
                                        <th>child age</th>
                                        <th>action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php $__currentLoopData = @$customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $__currentLoopData = $customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    
                                    <tr class="centerTd">
                                        <?php if($item->staff_id == 0): ?>
                                            <td>Bot</td>
                                        <?php else: ?>
                                            <td><?php echo e(App\SmStaff::find($item->staff_id)->full_name); ?></td>
                                        <?php endif; ?>
                                        <td><?php echo e($item->customer_name); ?></td>
                                        <td><?php echo e($item->phone_number); ?></td> 
                                        <td><?php echo e($item->email); ?></td>
                                        <td><?php echo e($item->child_age); ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <?php echo app('translator')->get('lang.select'); ?>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <?php if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                                                        <a class="dropdown-item" href="<?php echo e(url('customer-facebook/'.$item->id)); ?>"><?php echo app('translator')->get('lang.view'); ?></a>
                                                    <?php endif; ?>
                                                    <?php if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                                                        <a class="dropdown-item" href="<?php echo e(url('customer-facebook/'.$item->id.'/edit')); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                                    <?php endif; ?>
                                                    <?php if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
                                                        <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Item" href="<?php echo e(url('customer-facebook/'.$item->id.'/delete-view')); ?>"><?php echo app('translator')->get('lang.delete'); ?></a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/CRM/Resources/views/CustomerFacebook/searchCustomer.blade.php ENDPATH**/ ?>