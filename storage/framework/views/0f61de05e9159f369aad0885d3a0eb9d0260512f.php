
<?php $__env->startSection('main_content'); ?>
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
    <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css"/> 
<?php $__env->stopPush(); ?>

<div class="banner">
	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>SCHOOL CALENDAR</h3>
        </div>
	</div>
	
	
</div>
<div class="main_content">
	<div class="background-content">
		<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/BG_03.jpeg" alt="">
		
		<div class="early-year">
			<img width="70%" src="<?php echo e(asset('public/')); ?>/images/SCHOOL CALENDAR_20-21.png">
		</div>
		
		
	</div>
	
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/layout/schoolcalendar.blade.php ENDPATH**/ ?>