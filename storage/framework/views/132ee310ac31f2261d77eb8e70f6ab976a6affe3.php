<?php
    use App\SmGeneralSettings; 
    $school_settings= SmGeneralSettings::where('school_id',Auth::user()->school_id)->first();
    if (Auth::user() == "") { header('location:' . url('/login')); exit(); }
    Session::put('permission', App\GlobalVariable::GlobarModuleLinks());
?>
<?php if(SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes" && Session::get('isSchoolAdmin')==FALSE ): ?>
    <?php echo $__env->make('backEnd/partials/saas_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



<?php else: ?>

<input type="hidden" name="url" id="url" value="<?php echo e(url('/')); ?>">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="<?php echo e(url('/')); ?>">
          <img  src="<?php echo e(file_exists(@$school_settings->logo) ? asset($school_settings->logo) : asset('public/uploads/settings/logo.png')); ?>" alt="logo">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    
    <ul class="list-unstyled components">
            <li>
                <a href="<?php echo e(url('/admin-dashboard')); ?>" id="admin-dashboard">
                    <span class="flaticon-speedometer"></span>
                    <?php echo app('translator')->get('lang.dashboard'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo e(url('/admin-category')); ?>">

                    <span class="flaticon-inventory"></span>
                    categories images
                </a>
             </li>

            
            <li>
                <a href="<?php echo e(url('/docs')); ?>">
                    <span class="flaticon-book-1"></span>
                    document
                </a>
            </li>
            
            
            <li>
                <a href="#subMenuDevices" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span>
                        <img style="margin-left: 18px" src="/public/images/device.png" width="17px" alt="">
                    </span>
                    Devices Management
                </a>
                <ul class="collapse list-unstyled" id="subMenuDevices">
                    <li>
                        <a href="#" onclick="qrcheck()">QRCODE Checking</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('devices')); ?>">In Stock</a>
                    </li>

                    <li>
                        <a href="<?php echo e(url('devices/repairing')); ?>">Repairing Devices</a>
                    </li>
                    
                    <li>
                        <a href="<?php echo e(url('devices/equipments-handover')); ?>">Equipments Handover</a>
                    </li>
                </ul>
            </li>

                <li>
                    <a href="#subMenuStudent" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-reading"></span>
                        <?php echo app('translator')->get('lang.student_information'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuStudent">
                            <li>
                                <a href="<?php echo e(route('student_list')); ?>"> <?php echo app('translator')->get('lang.student_list'); ?></a>
                            </li>
                        
                            <li>
                                <a href="<?php echo e(route('student_attendance')); ?>"> <?php echo app('translator')->get('lang.student_attendance'); ?></a>
                            </li>
                       
                            <li>
                                <a href="<?php echo e(route('student_attendance_report')); ?>"> <?php echo app('translator')->get('lang.student_attendance_report'); ?></a>
                            </li>
                        
                            <li>
                                <a href="<?php echo e(route('subject-wise-attendance')); ?>"> <?php echo app('translator')->get('lang.subject'); ?> <?php echo app('translator')->get('lang.wise'); ?> <?php echo app('translator')->get('lang.attendance'); ?> </a>
                            </li>
                        
                            <li>
                                <a href="<?php echo e(url('subject-attendance-report')); ?>"> <?php echo app('translator')->get('lang.subject_attendance_report'); ?> </a>
                            </li>
                        
                            <li>
                                <a href="<?php echo e(route('student_admission')); ?>"><?php echo app('translator')->get('lang.student_admission'); ?></a>
                            </li>
                        
                            <li>
                                <a href="<?php echo e(route('student_group')); ?>"><?php echo app('translator')->get('lang.student_group'); ?></a>
                            </li>
                        
                            <li>
                                <a href="<?php echo e(route('student_promote')); ?>"><?php echo app('translator')->get('lang.student_promote'); ?></a>
                            </li>
                        
                            
                    </ul>
                </li>
            <li>
                <a href="#subMenuStudyResult" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-reading"></span>
                    Study Information
                </a>
                <ul class="collapse list-unstyled" id="subMenuStudyResult">
                    <li><a href="<?php echo e(url('student-list')); ?>">Learning Outcomes</a></li>
                    <li><a href="<?php echo e(url('study-information/exam-schedule/0')); ?>">Exam Schedule</a></li>
                </ul>
            </li>
            
            
                <li>
                    <a href="#subMenuAcademic" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-graduated-student"></span>
                        <?php echo app('translator')->get('lang.academics'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuAcademic"> 
                            <li>
                                <a href="<?php echo e(route('optional-subject')); ?>"> <?php echo app('translator')->get('lang.optional'); ?> <?php echo app('translator')->get('lang.subject'); ?> </a>
                            </li>
                        <!--LONG-->
                         <li>
                            <a href="<?php echo e(url('/schedule')); ?>">
                                School timetable
                            </a>
                         </li>
                        <!--END-->
                        
                            <li>
                                <a href="<?php echo e(route('section')); ?>"> <?php echo app('translator')->get('lang.section'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('class')); ?>"> <?php echo app('translator')->get('lang.class'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('subject')); ?>"> <?php echo app('translator')->get('lang.subjects'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('class-room')); ?>"> <?php echo app('translator')->get('lang.class_room'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('class-time')); ?>"> <?php echo app('translator')->get('lang.cl_ex_time_setup'); ?></a>
                            </li>
                        
                         
                            <li>
                                <a href="<?php echo e(url('assign-class-teacher')); ?>"> <?php echo app('translator')->get('lang.assign_class_teacher'); ?></a>
                            </li>
                        
                         
                            <li>
                                <a href="<?php echo e(route('assign_subject')); ?>"> <?php echo app('translator')->get('lang.assign_subject'); ?></a>
                            </li>
                        
                         
                            <li>
                                <a href="<?php echo e(route('class_routine_new')); ?>"> <?php echo app('translator')->get('lang.class_routine'); ?></a>

                            </li>
                        

                    <!-- only for teacher -->
                        
                            <li>
                                <a href="<?php echo e(url('view-teacher-routine')); ?>"><?php echo app('translator')->get('lang.view'); ?> <?php echo app('translator')->get('lang.class_routine'); ?></a>
                            </li>
                        
                    </ul>
                </li>
            


            
           
                <li>
                    <a href="#subMenuTeacher" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-professor"></span>
                        <?php echo app('translator')->get('lang.study_material'); ?>
                    </a>

                    <ul class="collapse list-unstyled" id="subMenuTeacher">
                       
                            <li>
                                <a href="<?php echo e(url('upload-content')); ?>"> <?php echo app('translator')->get('lang.upload_content'); ?></a>
                            </li>
                        

                       
                            <li>
                                <a href="<?php echo e(url('assignment-list')); ?>"><?php echo app('translator')->get('lang.assignment'); ?></a>
                            </li>
                        

                        
                            <li>
                                <a href="<?php echo e(url('syllabus-list')); ?>"><?php echo app('translator')->get('lang.syllabus'); ?></a>
                            </li>
                        

                        
                            <li>
                                <a href="<?php echo e(url('other-download-list')); ?>"><?php echo app('translator')->get('lang.other_download'); ?></a>
                            </li>
                        
                    </ul>
                </li>
            

            
            <li>
                <a href="#subMenuSalary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-wallet"></span>
                    Payroll
                </a>
                <ul class="collapse list-unstyled" id="subMenuSalary">
                    <li>
                        <a href="<?php echo e(url('salary/me/search')); ?>">My Payroll</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('workday').'/'.Auth::user()->id.'/'.date('Y')); ?>">Work day</a>
                    </li>
                </ul>
            </li>
            

            
            <li>
                <a href="#onlineForms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="fa fa-globe" style="padding-left: 20px"></span>
                    Online forms
                </a>
                <ul class="collapse list-unstyled" id="onlineForms">
                    <li>
                        <a href="<?php echo e(url('return-to-work')); ?>">Form return to work</a>
                    </li>

                    <li>
                        <a href="<?php echo e(url('admin/medical-declaration/entry')); ?>">Medical Declaration Entry</a>
                    </li>
                    
                    <li>
                        <a href="<?php echo e(url('admin/medical-declaration/domestic')); ?>">Medical Declaration Domestic</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#subMenuLeaveManagement" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">
                    <span class="flaticon-slumber"></span>
                    <?php echo app('translator')->get('lang.leave'); ?>
                </a>
                <ul class="collapse list-unstyled" id="subMenuLeaveManagement">
                    <li>
                        
                        <a href="<?php echo e(url('approve-leave')); ?>"><?php echo app('translator')->get('your approve request'); ?></a>
                        
                        <a href="<?php echo e(url('pending-leave')); ?>"><?php echo app('translator')->get('your pending request'); ?></a>
                        
                    </li>
                    
                    
                    <li>
                        <a href="<?php echo e(url('pending-leave-student')); ?>"><?php echo app('translator')->get('student leave request'); ?></a>
                    </li>
                
                    <li>
                        <a href="<?php echo e(url('apply-leave')); ?>"><?php echo app('translator')->get('lang.apply_leave'); ?></a>
                    </li>
                   
                </ul>
            </li>

            
                <li>
                    <a href="#subMenuExam" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-test"></span>
                        <?php echo app('translator')->get('lang.examination'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuExam">
                       
                            <li>
                                <a href="<?php echo e(url('marks-grade')); ?>"> <?php echo app('translator')->get('lang.marks_grade'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('exam-type')); ?>"> <?php echo app('translator')->get('lang.add_exam_type'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('exam')); ?>"> <?php echo app('translator')->get('lang.exam_setup'); ?></a>
                            </li>
                        

                        
                            <li>
                                <a href="<?php echo e(route('exam_schedule')); ?>"> <?php echo app('translator')->get('lang.exam_schedule'); ?></a>
                            </li>
                        
                        

                        
                            <li>
                                <a href="<?php echo e(route('exam_attendance')); ?>"> <?php echo app('translator')->get('lang.exam_attendance'); ?></a>
                            </li>
                        

                        
                            <li>
                                <a href="<?php echo e(route('marks_register')); ?>"> <?php echo app('translator')->get('lang.marks_register'); ?></a>
                            </li>
                        

                        
                        
                            <li>
                                <a href="<?php echo e(route('send_marks_by_sms')); ?>"> <?php echo app('translator')->get('lang.send_marks_by_sms'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('question-group')); ?>"><?php echo app('translator')->get('lang.question_group'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('question-bank')); ?>"><?php echo app('translator')->get('lang.question_bank'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('online-exam')); ?>"><?php echo app('translator')->get('lang.online_exam'); ?></a>
                            </li>
                        

                    </ul>
                </li>
            

       

            

                <li>
                    <a href="#subMenuHomework" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-book"></span>
                        <?php echo app('translator')->get('lang.home_work'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuHomework">
                        
                            <li>
                                <a href="<?php echo e(route('add-homeworks')); ?>"> <?php echo app('translator')->get('lang.add_homework'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('homework-list')); ?>"> <?php echo app('translator')->get('lang.homework_list'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('evaluation-report')); ?>"> <?php echo app('translator')->get('lang.evaluation_report'); ?></a>
                            </li>
                        
                    </ul>
                </li>

            

            
                <li>
                    <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-email"></span>
                        <?php echo app('translator')->get('lang.communicate'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuCommunicate">
                        
                            <li>
                                <a href="<?php echo e(url('notice-list')); ?>"><?php echo app('translator')->get('lang.notice_board'); ?></a>
                            </li>
                        
                        <?php if(@$config->Saas == 1 && Auth::user()->is_administrator != "yes" ): ?>
                            
                            <li>
                                <a href="<?php echo e(url('administrator-notice')); ?>"><?php echo app('translator')->get('lang.administrator'); ?> <?php echo app('translator')->get('lang.notice'); ?></a>
                            </li>

                        <?php endif; ?>
                        
                        
                            <li>
                                <a href="<?php echo e(url('send-email')); ?>">Send Email</a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('email-sms-log')); ?>"><?php echo app('translator')->get('lang.email_sms_log'); ?></a>
                            </li>
                        
                    </ul>
                </li>
            

            
                <li>
                    <a href="#subMenulibrary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-book-1"></span>
                        <?php echo app('translator')->get('lang.library'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenulibrary">
                        
                            <li>
                                <a href="<?php echo e(url('library')); ?>"> <?php echo app('translator')->get('Library Manage'); ?></a>
                            </li>
                        
                       
                            <li>
                                <a href="<?php echo e(url('book-category-list')); ?>"> <?php echo app('translator')->get('lang.book_category'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('add-book')); ?>"> <?php echo app('translator')->get('lang.add_book'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('book-list')); ?>"> <?php echo app('translator')->get('lang.book_list'); ?></a>
                            </li>
                        
                        
                        
                            <li>
                                <a href="<?php echo e(url('library-member')); ?>"> <?php echo app('translator')->get('lang.library_member'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('member-list')); ?>"> <?php echo app('translator')->get('lang.member_list'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('all-issed-book')); ?>"> <?php echo app('translator')->get('lang.all_issued_book'); ?></a>
                            </li>
                        
                    </ul>
                </li>
            

            
            <li>
                <a href="#subCustomerFacebook" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    CRM
                </a>
                <ul class="collapse list-unstyled" id="subCustomerFacebook">
                    
                        <li>
                            <a href="<?php echo e(url('customer-facebook/add')); ?>">add customer facebook</a>
                        </li>
                    
                    
                        <li>
                            <a href="<?php echo e(url('customer-facebook/search-customer')); ?>">search customer facebook</a>
                        </li>
                    
                     <li>
                        <a href="<?php echo e(url('tasks/add')); ?>">Work Flow</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('tasks/disabled')); ?>">Disabled Task</a>
                    </li>
                </ul>
            </li>

            
                <li>
                    <a href="#subMenuTransport" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-bus"></span>
                        <?php echo app('translator')->get('lang.transport'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuTransport">
                        
                            <li>
                                <a href="<?php echo e(url('transport-route')); ?>"> <?php echo app('translator')->get('lang.routes'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('vehicle')); ?>"> <?php echo app('translator')->get('lang.vehicle'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('list-drivers')); ?>"> <?php echo app('translator')->get('driver'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('assign-vehicle')); ?>"> <?php echo app('translator')->get('lang.assign_vehicle'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('student_transport_report')); ?>"> <?php echo app('translator')->get('lang.student_transport_report'); ?></a>
                            </li>
                        
                    </ul>
                </li>
            

            
                <li>
                    <a href="#subMenuDormitory" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-hotel"></span>
                        <?php echo app('translator')->get('lang.dormitory'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuDormitory">
                        
                            <li>
                                <a href="<?php echo e(url('room-type')); ?>"> <?php echo app('translator')->get('lang.room_type'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('dormitory-list')); ?>"> <?php echo app('translator')->get('lang.dormitory'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('room-list')); ?>"> <?php echo app('translator')->get('lang.dormitory_rooms'); ?></a>
                            </li>
                        
                        
                        
                        
                            <li>
                                <a href="<?php echo e(route('student_dormitory_report')); ?>"> <?php echo app('translator')->get('lang.student_dormitory_report'); ?></a>
                            </li>
                        
                    </ul>
                </li>
            

            
                <li>
                    <a href="#subMenusystemReports" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle">
                        <span class="flaticon-analysis"></span>
                        <?php echo app('translator')->get('lang.reports'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenusystemReports">
                        

                            <li>
                                <a href="<?php echo e(route('student_report')); ?>"><?php echo app('translator')->get('lang.student_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('guardian_report')); ?>"><?php echo app('translator')->get('lang.guardian_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('student_history')); ?>"><?php echo app('translator')->get('lang.student_history'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('student_login_report')); ?>"><?php echo app('translator')->get('lang.student_login_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('fees_statement')); ?>"><?php echo app('translator')->get('lang.fees_statement'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('balance_fees_report')); ?>"><?php echo app('translator')->get('lang.balance_fees_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('transaction_report')); ?>"><?php echo app('translator')->get('lang.transaction_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('class_report')); ?>"><?php echo app('translator')->get('lang.class_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('class_routine_report')); ?>"><?php echo app('translator')->get('lang.class_routine'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('exam_routine_report')); ?>"><?php echo app('translator')->get('lang.exam_routine'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('teacher_class_routine_report')); ?>"><?php echo app('translator')->get('lang.teacher'); ?> <?php echo app('translator')->get('lang.class_routine'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('merit_list_report')); ?>"><?php echo app('translator')->get('lang.merit_list_report'); ?></a>
                            </li>

                            <li>
                                <a href="<?php echo e(url('custom-merit-list')); ?>"><?php echo app('translator')->get('custom'); ?> <?php echo app('translator')->get('lang.merit_list_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('online_exam_report')); ?>"><?php echo app('translator')->get('lang.online_exam_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('mark_sheet_report_student')); ?>"><?php echo app('translator')->get('lang.mark_sheet_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('tabulation_sheet_report')); ?>"><?php echo app('translator')->get('lang.tabulation_sheet_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('progress_card_report')); ?>"><?php echo app('translator')->get('lang.progress_card_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('custom-progress-card')); ?>"> <?php echo app('translator')->get('lang.custom'); ?> <?php echo app('translator')->get('lang.progress_card_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('student_fine_report')); ?>"><?php echo app('translator')->get('lang.student_fine_report'); ?></a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(route('user_log')); ?>"><?php echo app('translator')->get('lang.user_log'); ?></a>
                            </li>
                         
                        
                            <li>
                                <a href="<?php echo e(url('previous-class-results')); ?>"><?php echo app('translator')->get('lang.previous'); ?> <?php echo app('translator')->get('lang.result'); ?> </a>
                            </li>
                         
                        
                            <li>
                                <a href="<?php echo e(url('previous-record')); ?>"><?php echo app('translator')->get('lang.previous'); ?> <?php echo app('translator')->get('lang.record'); ?> </a>
                            </li>
                         
                            

                    
                       
                         <?php if(SmGeneralSettings::isModule('ResultReports')== TRUE): ?>
                        
                            <li>
                                <a href="<?php echo e(url('resultreports/cumulative-sheet-report')); ?>"><?php echo app('translator')->get('lang.cumulative'); ?> <?php echo app('translator')->get('lang.sheet'); ?> <?php echo app('translator')->get('lang.report'); ?></a>
                            </li> 

                            <li>
                                <a href="<?php echo e(url('resultreports/continuous-assessment-report')); ?>"><?php echo app('translator')->get('lang.contonuous'); ?> <?php echo app('translator')->get('lang.assessment'); ?> <?php echo app('translator')->get('lang.report'); ?></a>
                            </li>
                            <li>

                                <a href="<?php echo e(url('resultreports/termly-academic-report')); ?>"><?php echo app('translator')->get('lang.termly'); ?> <?php echo app('translator')->get('lang.academic'); ?> <?php echo app('translator')->get('lang.report'); ?></a>
                                </li>
                            <li>

                                <a href="<?php echo e(url('resultreports/academic-performance-report')); ?>"><?php echo app('translator')->get('lang.academic'); ?> <?php echo app('translator')->get('lang.performance'); ?> <?php echo app('translator')->get('lang.report'); ?></a>
                                </li>
                            <li>

                                <a href="<?php echo e(url('resultreports/terminal-report-sheet')); ?>"><?php echo app('translator')->get('lang.terminal'); ?> <?php echo app('translator')->get('lang.report'); ?> <?php echo app('translator')->get('lang.sheet'); ?></a>
                                </li>
                            <li>

                                <a href="<?php echo e(url('resultreports/continuous-assessment-sheet')); ?>"><?php echo app('translator')->get('lang.continuous'); ?> <?php echo app('translator')->get('lang.assessment'); ?> <?php echo app('translator')->get('lang.sheet'); ?></a>
                                </li>
                            <li>

                                <a href="<?php echo e(url('resultreports/result-version-two')); ?>"><?php echo app('translator')->get('lang.result'); ?> <?php echo app('translator')->get('lang.version'); ?> V2</a>
                                </li>
                            <li>

                                <a href="<?php echo e(url('resultreports/result-version-three')); ?>"><?php echo app('translator')->get('lang.result'); ?> <?php echo app('translator')->get('lang.version'); ?> V3</a>
                            </li>
                             
                        <?php endif; ?> 

<?php endif; ?>
                    </ul>
                </li>
    </ul>
</nav>

<?php /**PATH D:\WWW\SGS\sgss\resources\views/backEnd/partials/sidebars/teacher_sidebar.blade.php ENDPATH**/ ?>