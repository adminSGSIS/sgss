<div class="row mb-40 mt-30 justify-content-around">
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('emergency_full_name') ? ' is-invalid' : ''); ?>" type="text" name="emergency_full_name" value="<?php echo e(old('emergency_full_name')); ?>">

      <label>Full name *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_full_name_feedback" role="alert"></span>
      <?php if($errors->has('emergency_full_name')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('emergency_full_name')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('emergency_relationship_to_parents') ? ' is-invalid' : ''); ?>" type="text" name="emergency_relationship_to_parents" value="<?php echo e(old('emergency_relationship_to_parents')); ?>">

      <label>Relationship to parents *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_relationship_to_parents_feedback" role="alert"></span>
      <?php if($errors->has('emergency_relationship_to_parents')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('emergency_relationship_to_parents')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('emergency_contact_phone_number') ? ' is-invalid' : ''); ?>" type="text" name="emergency_contact_phone_number" value="<?php echo e(old('emergency_contact_phone_number')); ?>">

      <label>Contact phone number *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_contact_phone_number_feedback" role="alert"></span>
      <?php if($errors->has('emergency_contact_phone_number')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('emergency_contact_phone_number')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

</div>

<div class="row justify-content-around mb-40">
  <div class="col-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('emergency_occupation') ? ' is-invalid' : ''); ?>" type="text" name="emergency_occupation" value="<?php echo e(old('emergency_occupation')); ?>">
      <label>Emergency's occupation</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_occupation_feedback	" role="alert"></span>
      <?php if($errors->has('emergency_occupation')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('emergency_occupation')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('emergency_address') ? ' is-invalid' : ''); ?>" type="text" name="emergency_address" value="<?php echo e(old('emergency_address')); ?>">

      <label>Emergency's address </label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="emergency_address_feedback" role="alert"></span>
      <?php if($errors->has('emergency_address')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('emergency_address')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
</div><?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/forms/step21.blade.php ENDPATH**/ ?>