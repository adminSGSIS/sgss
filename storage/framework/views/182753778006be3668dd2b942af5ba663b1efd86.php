<?php $__env->startSection('css'); ?>
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
   #table{
       display: none;
   }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>MEDICAL DECLARATION DOMESTIC</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                </div>
            </div>
        </div>
    </section>
    
    <div class="row">
        <div class="col-lg-8 col-md-6 col-sm-6">
            <div class="main-title mt_0_sm mt_0_md">
                <h3 class="mb-30">Export excel</h3>
            </div>
        </div>

        <div class="col-lg-4 text-md-right text-left col-md-6 mb-30-lg col-sm-6 text_sm_right">
            <a href="<?php echo e(url('admin/medical-declaration/domestic/export')); ?>" class="primary-btn small fix-gr-bg">
                <span class="ti-plus pr-2"></span>
                EXPORT </a>
        </div>
    </div>

    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Data List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                            <?php if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                            <div class="alert alert-success">
                                                <?php echo e(session()->get('message-success-delete')); ?>

                                            </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                            <div class="alert alert-danger">
                                                <?php echo e(session()->get('message-danger-delete')); ?>

                                            </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr class="centerTh">
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Nationality</th>
                                <th>Date of birth</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php $__currentLoopData = $all_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="centerTd">
                                        <td><?php echo e($value->full_name); ?></td>
                                        <td><?php echo e($value->email); ?></td>
                                        <td><?php echo e($value->nationality); ?></td>
                                        <td><?php echo e($value->date_of_birth); ?></td>
                                        <td>
                                            <a href="<?php echo e(url('admin/medical-declaration/domestic/details'.'/'.$value->id)); ?>" class="dropdown-item" title="Travel Declaration Details" data-modal-size="modal-md">
                                                View
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/backEnd/communicate/medical_declaration_domestic.blade.php ENDPATH**/ ?>