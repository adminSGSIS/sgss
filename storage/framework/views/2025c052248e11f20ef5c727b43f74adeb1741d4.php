
<?php $__env->startSection('mainContent'); ?>
<div class="mb-3">
    <div class="text-center">
        <h1>Health Monitoring</h1>
    </div>

    <div class="row mb-3">
        <div class="col-lg-4">
            <h3>Creator</h3>
            <div class="white-box">
                Full Name: <?php echo e(Auth::user()->full_name); ?> <br><br>
                Email: <?php echo e(Auth::user()->email); ?> <br><br>
                Phone: <?php echo e(Auth::user()->staff->mobile); ?>

            </div>
        </div>
        <div class="col-lg-8">
            <h3>Student Name</h3>
            <div class="white-box">
                <div class="row mb-3">
                    <div class="col-lg-6">Full Name: <?php echo e($student->full_name); ?></div>
                    <div class="col-lg-6">Age: <?php echo e($student->age); ?></div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Height: <?php echo e($student->height); ?></div>
                    <div class="col-lg-6">Weight: <?php echo e($student->weight); ?></div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Class: <?php echo e($student->className->class_name); ?></div>
                    <div class="col-lg-6">Date of birth: <?php echo e($student->date_of_birth); ?></div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Blood pressure:
                        <?php echo e(!empty($student_health) ? $student_health->blood_pressure : ''); ?></div>
                    <div class="col-lg-6">Heartbeat: <?php echo e(!empty($student_health) ? $student_health->heart_beat : ''); ?>

                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Right eye: <?php echo e(!empty($student_health) ? $student_health->right_eye : ''); ?>

                    </div>
                    <div class="col-lg-6">Left eye: <?php echo e(!empty($student_health) ? $student_health->left_eye: ''); ?></div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">Myopic: <?php echo e(!empty($student_health) && $student_health->myopic); ?></div>
                    <div hidden></div>
                </div>
            </div>
        </div>
    </div>
    <div> <?php if(empty($student_health)): ?>
        <form action="<?php echo e(route('healthMonitoringStore')); ?>">
            <?php echo csrf_field(); ?>
            <?php else: ?>
            <form action="<?php echo e(route('healthMonitoringUpdate',$student_health->student_id)); ?>">
                <?php echo csrf_field(); ?>
                <?php endif; ?>
                <input type="text" value="<?php echo e($student->id); ?>" name="id_student" hidden>
                <input type="text" value="<?php echo e($student->full_name); ?>" name="student_name" hidden>
                <input type="text" value="<?php echo e($student->class_id); ?>" name="class" hidden>
                <div class="white-box mb-3">
                    <div class="row mb-3">
                        <div class="col-lg-12">
                            <div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="input-efect">
                                            <label for="date">Day of birth:</label>
                                            <input type="text" name="date" class="form-control date primary-input"
                                                value="<?php echo e($student->date_of_birth); ?>" readonly>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="input-efect">
                                            <label for="age">Age:</label>
                                            <input type="text" name="age" class="form-control primary-input"
                                                value="<?php echo e($student->age); ?>" readonly>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3>Physical</h3>
                    <div class="row mb-3">
                        <div class="col-lg-6">
                            <div class="form-group input-effect">
                                <label for="date">Height:</label>
                                <input type="text" name="height" class="form-control primary-input"
                                    value="<?php echo e($student->height); ?>" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group input-effect">
                                <label for="date">Weight:</label>
                                <input type="text" name="weight" class="form-control primary-input"
                                    value="<?php echo e($student->weight); ?>" readonly>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group input-effect">
                                <label for="date">Blood pressure:</label>
                                <input type="text" name="blood_pressure" class="form-control primary-input"
                                    value="<?php echo e(!empty($student_health) ? $student_health->blood_pressure : ''); ?>">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group input-effect">
                                <label for="date">Heartbeat:</label>
                                <input type="text" name="heart_beat" class="form-control primary-input"
                                    value="<?php echo e(!empty($student_health) ? $student_health->heart_beat : ''); ?>">
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h3 class="mb-3">Eyes sight</h3>
                        <div class="form-check mb-3">
                            <input type="checkbox" name="myopic" value="1" id="eyesight" class="form-check-input"
                                name="eyesight"
                                <?php echo e(!empty($student_health) && $student_health->myopic == 1 ? 'checked': ''); ?>>
                            <label for="eyesight" class="form-check-label">Myopic</label>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group input-effect">
                                    <label for="date">Right eye:</label>
                                    <input type="text" name="right_eye" class="form-control primary-input"
                                        value="<?php echo e(!empty($student_health) ? $student_health->right_eye : ''); ?>">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group input-effect">
                                    <label for="date">Left eye:</label>
                                    <input type="text" name="left_eye" class="form-control primary-input"
                                        value="<?php echo e(!empty($student_health) ? $student_health->left_eye : ''); ?>">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if(empty($student_health)): ?>
                    <div class="col-lg-12 text-center mt-30">
                        <button class="primary-btn fix-gr-bg">
                            <span class="ti-check"></span>
                            <?php echo app('translator')->get('lang.save'); ?>
                        </button>
                    </div>
                    <?php else: ?>
                    <div class="col-lg-12 text-center mt-30">
                        <button class="primary-btn fix-gr-bg">
                            <span class="ti-check"></span>
                            <?php echo app('translator')->get('lang.update'); ?>
                        </button>
                    </div>
                    <?php endif; ?>
            </form>
    </div>
</div>

</div>


<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3><?php echo app('translator')->get('Patient History'); ?></h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                        <thead>
                            <?php if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != ""): ?>
                            <tr>
                                <td colspan="7">
                                    <?php if(session()->has('message-success-delete')): ?>
                                    <div class="alert alert-success">
                                        <?php echo e(session()->get('message-success-delete')); ?>

                                    </div>
                                    <?php elseif(session()->has('message-danger-delete')): ?>
                                    <div class="alert alert-danger">
                                        <?php echo e(session()->get('message-danger-delete')); ?>

                                    </div>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <th><?php echo app('translator')->get('No'); ?></th>
                                <th><?php echo app('translator')->get('Name'); ?></th>
                                <th><?php echo app('translator')->get('Class'); ?></th>
                                <th><?php echo app('translator')->get('Date'); ?></th>
                                <th><?php echo app('translator')->get('Symtome Type'); ?></th>
                                <th><?php echo app('translator')->get('action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $patients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $patient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($patient->id); ?></td>
                                <td><?php echo e($patient->patient_name); ?></td>
                                <td><?php echo e($patient->symtome_type); ?></td>
                                <td><?php echo e($patient->date); ?></td>
                                <td><?php echo e($patient->symtomeRelation->name); ?></td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            <?php echo app('translator')->get('lang.select'); ?>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a title="Health Summary" class="dropdown-item"
                                                href="<?php echo e(url('medical/healthMonitoring-Summary/'.$patient->id.'/'.$patient->patient_id)); ?>"><?php echo app('translator')->get('Health
                                                Summary'); ?></a>
                                            
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/Medical\Resources/views/healthMonitoringCreate.blade.php ENDPATH**/ ?>