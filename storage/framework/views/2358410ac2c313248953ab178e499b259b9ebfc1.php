
<?php $__env->startSection('css'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/')); ?>/css/croppie.css">
<style>
    .infix_session {
        display: none;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1><?php echo app('translator')->get('lang.student_admission'); ?></h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                <a href="#"><?php echo app('translator')->get('lang.student_information'); ?></a>
                <a href="#"><?php echo app('translator')->get('lang.student_admission'); ?></a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-6 col-sm-6 mb-4">
                <div class="main-title xs_mt_0 mt_0_sm">
                    <h3 class="mb-0">Update <?php echo app('translator')->get('lang.student'); ?></h3>
                </div>
            </div>
        </div>
        <?php if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
        <?php echo e(Form::open(['class' => 'form-horizontal studentadmission', 'files' => true, 'route' => 'student_update', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form'])); ?>

        <?php endif; ?>
        <input type="hidden" name="student_id" value="<?php echo e($student->id); ?>">
        <div class="row">
            <div class="col-lg-12">
                <div class="white-box">
                    <div>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <?php if($errors->any()): ?>
                                <div class="error text-danger "><?php echo e('Something went wrong, please try again'); ?></div>
                                <?php endif; ?>
                            </div>
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head"><?php echo app('translator')->get('lang.personal'); ?> <?php echo app('translator')->get('lang.info'); ?></h4>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="url" value="<?php echo e(URL::to('/')); ?>">
                        <div class="row mb-40 mt-30">
                            <div class="col-lg-3">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('academic_id') ? ' is-invalid' : ''); ?>" 
                                        name="academic_id" id="academic_year">
                                        <option data-display="<?php echo app('translator')->get('lang.academic_year'); ?> *" value=""><?php echo app('translator')->get('lang.academic_year'); ?> *</option>
                                        <?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($session->id); ?>" <?php echo e($student->academic_id == $session->id? 'selected': ''); ?>>
                                                <?php echo e($session->year); ?>[<?php echo e($session->title); ?>]
                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('academic_id')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong><?php echo e($errors->first('session')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <?php
                                $classesAcademic = App\SmClass::where('academic_id', $student->academic_id)->get();
                            ?>
                            <div class="col-lg-3">
                                <div class="input-effect sm2_mb_20 md_mb_20" id="class-div">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('class_id') ? ' is-invalid' : ''); ?>"
                                        name="class_id" id="classSelectStudent">
                                        <option data-display="<?php echo app('translator')->get('lang.class'); ?> *" value=""><?php echo app('translator')->get('lang.class'); ?> *</option>
                                        <?php $__currentLoopData = $classesAcademic; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option data-display="<?php echo e($item ->class_name); ?>" 
                                            value="<?php echo e($item ->id); ?>" <?php if($student->class_id == $item->id): ?> selected <?php endif; ?>>
                                            <?php echo e($item ->class_name); ?>

                                        </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('class_id')): ?>
                                    <span class="invalid-feedback invalid-select" role="alert">
                                        <strong><?php echo e($errors->first('class_id')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <?php
                                $sections = DB::table('sm_class_sections')->where('class_id', '=', $student->class_id)
                                ->join('sm_sections','sm_class_sections.section_id','=','sm_sections.id')
                                ->get();
                            ?>
                            <div class="col-lg-2">
                                <div class="input-effect sm2_mb_20 md_mb_20" id="sectionStudentDiv">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('section_id') ? ' is-invalid' : ''); ?>" 
                                        name="section_id" id="sectionSelectStudent">
                                        <option data-display="<?php echo app('translator')->get('lang.section'); ?> *" value=""><?php echo app('translator')->get('lang.section'); ?> *</option>
                                        <?php $__currentLoopData = $sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($section->id); ?>" <?php if($student->section_id == $section->id): ?> selected <?php endif; ?>>
                                            <?php echo e($section->section_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('section_id')): ?>
                                    <span class="invalid-feedback invalid-select" role="alert">
                                        <strong><?php echo e($errors->first('section_id')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-effect">
                                    <input class="primary-input form-control" 
                                    type="text" name="admission_no" readonly value="<?php echo e($student->admission_no); ?>">
                                    <label><?php echo app('translator')->get('lang.admission'); ?> <?php echo app('translator')->get('lang.number'); ?></label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input read-only-input" type="number" id="roll_no" 
                                    name="roll_no" value="<?php echo e($student->roll_no); ?>" readonly>
                                    <label><?php echo app('translator')->get('lang.roll'); ?> <?php echo app('translator')->get('lang.number'); ?></label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" name="first_name"
                                     value="<?php echo e($student->first_name); ?>" >
                                    <label><?php echo app('translator')->get('lang.first'); ?> <?php echo app('translator')->get('lang.name'); ?> <span>*</span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" name="middle_name"
                                     value="<?php echo e($student->middle_name); ?>">
                                    <label>middle name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" name="last_name"
                                    value="<?php echo e($student->last_name); ?>" >
                                    <label><?php echo app('translator')->get('lang.last'); ?> <?php echo app('translator')->get('lang.name'); ?> *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-6 col-md-6">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input" type="text" id="placeholderPhoto" 
                                            placeholder="<?php echo e($student->student_photo != "" ? $student->student_photo : 'Student photo'); ?>" readonly="">
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('student_photo')): ?>
                                                <span class="invalid-feedback d-block" role="alert">
                                                    <strong><?php echo e(@$errors->first('student_photo')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="photo"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" value="" name="student_img" id="photo" accept="image/*">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="nickname" value="<?php echo e($student->nickname); ?>">
                                    <label>Student’s preferred name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text" 
                                             name="date_of_birth" value="<?php echo e($student->date_of_birth); ?>">
                                            <label><?php echo app('translator')->get('lang.date_of_birth'); ?> *</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control" name="gender_id">
                                        <option data-display="<?php echo app('translator')->get('lang.gender'); ?> *" value=""><?php echo app('translator')->get('lang.gender'); ?> *</option>
                                        <?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>" <?php echo e($student->gender_id == $item->id ? 'selected': ''); ?>><?php echo e($item->base_setup_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="nationality_1" value="<?php echo e($student->nationality_1); ?>">
                                    <label>First Nationality *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                    name="nationality_2" value="<?php echo e($student->nationality_2); ?>">
                                    <label>Second Nationality</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" readonly
                                     name="" value="<?php echo e($student->class->class_name); ?>">
                                    <label>enrol in</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">student interests</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12 mb-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                     name="particular_interests" value="<?php echo e($student->admission->particular_interests ?? ''); ?>">
                                    <label>Student's particular interests</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                     name="talents_or_abilities" value="<?php echo e($student->admission->talents_or_abilities ?? ''); ?>">
                                    <label>talents or abilities </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">language</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="first_language" value="<?php echo e($student->first_language); ?>">
                                    <label>Child's first language *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mb-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="second_language" value="<?php echo e($student->second_language); ?>">
                                    <label>Child's second language</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="other_language" value="<?php echo e($student->other_language); ?>">
                                    <label>Other spoken language</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            
                            <div class="col-12 mt-4">
                                <fieldset>
                                    <p>Other language level *</p>
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <input type="radio" id="other_language_proficient_1" class="common-radio relationButton"
                                            name="other_language_proficient" value="1" <?php if($student->other_language_proficient == 1): ?> checked <?php endif; ?>>
                                            <label for="other_language_proficient_1">Beginner</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="other_language_proficient_2" class="common-radio relationButton" name="other_language_proficient" 
                                            value="2" <?php if($student->other_language_proficient == 2): ?> checked <?php endif; ?>>
                                            <label for="other_language_proficient_2">Gaining confidence</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="other_language_proficient_3" class="common-radio relationButton" name="other_language_proficient" 
                                            value="3" <?php if($student->other_language_proficient == 3): ?> checked <?php endif; ?>>
                                            <label for="other_language_proficient_3">Confidence</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="other_language_proficient_4" class="common-radio relationButton" name="other_language_proficient" 
                                            value="4" <?php if($student->other_language_proficient == 4): ?> checked <?php endif; ?>>
                                            <label for="other_language_proficient_4">Fluent</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="other_language_proficient_5" class="common-radio relationButton" name="other_language_proficient" 
                                            value="5" <?php if($student->other_language_proficient == 5): ?> checked <?php endif; ?>>
                                            <label for="other_language_proficient_5">Native</label>
                                            <br>
                                        </div>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;"><?php echo e($errors->first('other_language_proficient')); ?></small>
                            </div>

                            <div class="col-12 mt-4">
                                <fieldset>
                                    <p>English level *</p>
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <input type="radio" id="english_proficient_1" class="common-radio relationButton" name="english_proficient"
                                            value="1" <?php if($student->english_proficient == 1): ?> checked <?php endif; ?>>
                                            <label for="english_proficient_1">Beginner</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="english_proficient_2" class="common-radio relationButton" name="english_proficient" 
                                            value="2" <?php if($student->english_proficient == 2): ?> checked <?php endif; ?>>
                                            <label for="english_proficient_2">Gaining confidence</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="english_proficient_3" class="common-radio relationButton" name="english_proficient" 
                                            value="3" <?php if($student->english_proficient == 3): ?> checked <?php endif; ?>>
                                            <label for="english_proficient_3">Confidence</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="english_proficient_4" class="common-radio relationButton" name="english_proficient" 
                                            value="4" <?php if($student->english_proficient == 4): ?> checked <?php endif; ?>>
                                            <label for="english_proficient_4">Fluent</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="english_proficient_5" class="common-radio relationButton" name="english_proficient" 
                                            value="5" <?php if($student->english_proficient == 5): ?> checked <?php endif; ?>>
                                            <label for="english_proficient_5">Native</label>
                                            <br>
                                        </div>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;"><?php echo e($errors->first('english_proficient')); ?></small>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">trial date</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-lg-3">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text" 
                                            name="trial_from_date" value="<?php echo e($student->admission->trial_from_date ?? ''); ?>">
                                            <label>From *</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text"  
                                            name="trial_to_date" value="<?php echo e($student->admission->trial_to_date ?? ''); ?>">
                                            <label>To *</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('trial_class_id') ? ' is-invalid' : ''); ?>"
                                         name="trial_class_id">
                                        <option data-display="<?php echo app('translator')->get('lang.class'); ?> *" value=""><?php echo app('translator')->get('lang.class'); ?> *</option>
                                        <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option data-display="<?php echo e($item->class_name); ?>" 
                                                value="<?php echo e($item->id); ?>" <?php if($item->id == ($student->admission->trial_class_id ?? '')): ?> selected <?php endif; ?>>
                                                <?php echo e($item->class_name); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('trial_class_id')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong><?php echo e($errors->first('trial_class_id')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="trial_teacher_id" value="<?php echo e($student->admission->trial_teacher_id ?? ''); ?>">
                                    <label>Teacher *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">school history</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="school_history_name" value="<?php echo e($student->admission->school_history_name ?? ''); ?>">
                                    <label>Current school name *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="school_history_city" value="<?php echo e($student->admission->school_history_city ?? ''); ?>">
                                    <label>City, country *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text"  
                                            name="school_history_from_date" value="<?php echo e($student->admission->school_history_from_date ?? ''); ?>">
                                            <label>Date from *</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect sm2_mb_20 md_mb_20">
                                            <input class="primary-input date form-control" type="text"  
                                            name="school_history_to_date" value="<?php echo e($student->admission->school_history_to_date ?? ''); ?>">
                                            <label>Date to *</label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button type="button">
                                            <i class="ti-calendar" id="start-date-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="school_history_language_instruction" value="<?php echo e($student->admission->school_history_language_instruction ?? ''); ?>">
                                    <label>Language of instruction *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="school_history_withdrawal_reason" value="<?php echo e($student->admission->school_history_withdrawal_reason ?? ''); ?>">
                                    <label>Reason for withdrawal *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">health information</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12 mb-40">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="health_information_1" value="<?php echo e($student->admission->health_information_1 ?? ''); ?>">
                                    <label>Allergies to medications and/or foods</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mb-40">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="health_information_2" value="<?php echo e($student->admission->health_information_2 ?? ''); ?>">
                                    <label>Medications taken on a regular basis</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mb-40">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" 
                                    name="health_information_3" value="<?php echo e($student->admission->health_information_3 ?? ''); ?>">
                                    <label>If your child has any other medical condition which may affect his/her daily routine, please describe it here</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mb-2">
                                <fieldset>
                                    <div>
                                        <input type="checkbox" id="health_information_4" class="common-checkbox relationButton" 
                                        name="health_information_4" value="health_4" <?php if(($student->admission->health_information_4 ?? '') == 'health_4'): ?> checked <?php endif; ?>>
                                        <label for="health_information_4">
                                            We consent to the school nurse administering over-the-counter 
                                            medications for symptom relief of minor illnesses
                                        </label>
                                        <br>
                                    </div>
                                </fieldset>
                            </div>

                            <div class="col-12">
                                <fieldset>
                                    <div>
                                        <input type="checkbox" id="health_information_5" class="common-checkbox relationButton" 
                                        name="health_information_5" value="health_5" <?php if(($student->admission->health_information_5 ?? '') == 'health_5'): ?> checked <?php endif; ?>>
                                        <label for="health_information_5">
                                            We agree to provide any professional reports relevant to my child's education 
                                            history 
                                        </label>
                                        <br>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">additional needs</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="input-effect">
                                    <textarea class="primary-input form-control mt-3" name="additional_needs"
                                    type="text"><?php echo e($student->admission->additional_needs ?? ''); ?></textarea>
                                    <label>
                                        PLease describe any other Special educational need that 
                                        Saigon Star should be aware of in order to make your child's learning is positive experience
                                    </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">School bus service requested</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <?php $__currentLoopData = $bus_service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-12 mb-2">
                                <fieldset>
                                    <div>
                                        <input type="radio" id="bus_service_request_<?php echo e($item->id); ?>" class="common-radio relationButton" 
                                        name="bus_service_request" value="<?php echo e($item->id); ?>" 
                                        <?php if(($student->route_list_id ?? '') == $item->id): ?> checked <?php endif; ?>>
                                        <label for="bus_service_request_<?php echo e($item->id); ?>">
                                            <?php echo e($item->title); ?>

                                        </label>
                                        <br>
                                    </div>
                                </fieldset>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">lunch requested</h4>
                                </div>
                            </div>
                        </div>
                            <div class="col-12 mb-2">
                                <fieldset>
                                    <div>
                                        <input type="radio" name="have_lunch_request" value="1" <?php echo e($student->lunch_request==1 ? 'checked' : ''); ?>>
                                        <label>
                                            Yes
                                        </label>
                                        <br>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-12 mb-2">
                                <fieldset>
                                    <div>
                                        <input type="radio" name="have_lunch_request" value="0" <?php echo e($student->lunch_request==0 ? 'checked' : ''); ?>>
                                        <label>
                                            No
                                        </label>
                                        <br>
                                    </div>
                                </fieldset>
                            </div>
                        
                
    
                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control mt-3" name="lunch_request"
                                    type="text" value="<?php echo e($student->admission->lunch_request ?? ''); ?>">
                                    <label>
                                        Any special request for lunch ?
                                    </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">support</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="eal_support"
                                    type="text" value="<?php echo e($student->admission->eal_support ?? ''); ?>">
                                    <label>EAL support (hours/week)</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="sen_support"
                                    type="text" value="<?php echo e($student->admission->sen_support ?? ''); ?>">
                                    <label>SEN support (hours/week)</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">payment by</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_name" 
                                    type="text" value="<?php echo e($student->admission->payer_name ?? ''); ?>">
                                    <label>Contact Name *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_company"
                                    type="text" value="<?php echo e($student->admission->payer_company ?? ''); ?>">
                                    <label>Company Name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_address" 
                                    type="text" value="<?php echo e($student->admission->payer_address ?? ''); ?>">
                                    <label>Address *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_email" 
                                    type="email" value="<?php echo e($student->admission->payer_email ?? ''); ?>">
                                    <label>Email *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" name="payer_mobile" 
                                    type="text" value="<?php echo e($student->admission->payer_mobile ?? ''); ?>">
                                    <label>Phone number *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-4">
                            <div class="col-12">
                                <div class="main-title">
                                    <h4 class="stu-sub-head">family information</h4>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30">
                            <div class="col-6">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text"
                                    name="fathers_name" value="<?php echo e($student->parent->fathers_name ?? ''); ?>">
                                    <label><?php echo app('translator')->get('lang.father'); ?> <?php echo app('translator')->get('lang.name'); ?><span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                    name="fathers_nationality" value="<?php echo e($student->parent->fathers_nationality ?? ''); ?>">
                                    <label>nationality</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" name="fathers_occupation" value="<?php echo e($student->parent->fathers_occupation ?? ''); ?>">
                                    <label><?php echo app('translator')->get('lang.occupation'); ?></label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" name="fathers_company" value="<?php echo e($student->parent->fathers_company ?? ''); ?>">
                                    <label>Company</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            
                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text"
                                    name="fathers_mobile" value="<?php echo e($student->parent->fathers_mobile ?? ''); ?>">
                                    <label><?php echo app('translator')->get('lang.father'); ?> <?php echo app('translator')->get('lang.phone'); ?> <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                           
                            <div class="col-6 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="email" name="fathers_email"
                                     value="<?php echo e($student->parent->fathers_email ?? ''); ?>">
                                    <label>Father's email</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="fathers_work_address"
                                     value="<?php echo e($student->parent->fathers_work_address ?? ''); ?>">
                                    <label>Father's work address</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="fathers_first_language"
                                     value="<?php echo e($student->parent->fathers_first_language ?? ''); ?>">
                                    <label>Father's first language</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            
                            <div class="col-12 mt-4">
                                <fieldset>
                                    <p>English level</p>
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <input type="radio" id="fathers_language_level_1" class="common-radio relationButton" name="fathers_language_level"
                                            value="1" <?php if(($student->parent->fathers_language_level ?? '') == 1): ?> checked <?php endif; ?>>
                                            <label for="fathers_language_level_1">Beginner</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="fathers_language_level_2" class="common-radio relationButton" name="fathers_language_level" 
                                            value="2" <?php if(($student->parent->fathers_language_level ?? '') == 2): ?> checked <?php endif; ?>>
                                            <label for="fathers_language_level_2">Gaining Confidence</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="fathers_language_level_3" class="common-radio relationButton" name="fathers_language_level" 
                                            value="3" <?php if(($student->parent->fathers_language_level ?? '') == 3): ?> checked <?php endif; ?>>
                                            <label for="fathers_language_level_3">Confidence</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="fathers_language_level_4" class="common-radio relationButton" name="fathers_language_level" 
                                            value="4" <?php if(($student->parent->fathers_language_level ?? '') == 4): ?> checked <?php endif; ?>>
                                            <label for="fathers_language_level_4">Fluent</label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="fathers_language_level_5" class="common-radio relationButton" name="fathers_language_level" 
                                            value="5" <?php if(($student->parent->fathers_language_level ?? '') == 5): ?> checked <?php endif; ?>>
                                            <label for="fathers_language_level_5">Native</label>
                                            <br>
                                        </div>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;"><?php echo e($errors->first('fathers_language_level')); ?></small>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30">
                            <div class="col-6">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" name="mothers_name"
                                     value="<?php echo e($student->parent->mothers_name ?? ''); ?>">
                                    <label><?php echo app('translator')->get('lang.mother'); ?> <?php echo app('translator')->get('lang.name'); ?> <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" 
                                    type="text" name="mothers_nationality" value="<?php echo e($student->parent->mothers_nationality ?? ''); ?>">
                                    <label>nationality</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" name="mothers_occupation" value="<?php echo e($student->parent->mothers_occupation ?? ''); ?>">
                                    <label><?php echo app('translator')->get('lang.occupation'); ?></label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" name="mothers_company" value="<?php echo e($student->parent->mothers_company ?? ''); ?>">
                                    <label>Company</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            
                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" 
                                    type="text" name="mothers_mobile" value="<?php echo e($student->parent->mothers_mobile ?? ''); ?>">
                                    <label><?php echo app('translator')->get('lang.mother'); ?> <?php echo app('translator')->get('lang.phone'); ?> <span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                           
                            <div class="col-6 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="email" name="mothers_email"
                                     value="<?php echo e($student->parent->mothers_email ?? ''); ?>">
                                    <label>Mother's email</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="mothers_work_address"
                                     value="<?php echo e($student->parent->mothers_work_address ?? ''); ?>">
                                    <label>Mother's work address</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="mothers_first_language"
                                     value="<?php echo e($student->parent->mothers_first_language ?? ''); ?>">
                                    <label>Mother's first language</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <fieldset>
                                    <p>English level</p>
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <input type="radio" id="mothers_language_level_1" class="common-radio relationButton" name="mothers_language_level"
                                            value="<?php echo e($proficient[0]); ?>" <?php if(($student->parent->mothers_language_level ?? '') == $proficient[0]): ?> checked <?php endif; ?>>
                                            <label for="mothers_language_level_1"><?php echo e($proficient[0]); ?></label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="mothers_language_level_2" class="common-radio relationButton" name="mothers_language_level" 
                                            value="<?php echo e($proficient[1]); ?>" <?php if(($student->parent->mothers_language_level ?? '') == $proficient[1]): ?> checked <?php endif; ?>>
                                            <label for="mothers_language_level_2"><?php echo e($proficient[1]); ?></label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="mothers_language_level_3" class="common-radio relationButton" name="mothers_language_level" 
                                            value="<?php echo e($proficient[2]); ?>" <?php if(($student->parent->mothers_language_level ?? '') == $proficient[2]): ?> checked <?php endif; ?>>
                                            <label for="mothers_language_level_3"><?php echo e($proficient[2]); ?></label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="mothers_language_level_4" class="common-radio relationButton" name="mothers_language_level" 
                                            value="<?php echo e($proficient[3]); ?>" <?php if(($student->parent->mothers_language_level ?? '') == $proficient[3]): ?> checked <?php endif; ?>>
                                            <label for="mothers_language_level_4"><?php echo e($proficient[3]); ?></label>
                                            <br>
                                        </div>
                                        <div>
                                            <input type="radio" id="mothers_language_level_5" class="common-radio relationButton" name="mothers_language_level" 
                                            value="<?php echo e($proficient[4]); ?>" <?php if(($student->parent->mothers_language_level ?? '') == $proficient[4]): ?> checked <?php endif; ?>>
                                            <label for="mothers_language_level_5"><?php echo e($proficient[4]); ?></label>
                                            <br>
                                        </div>
                                    </div>
                                </fieldset>
                                <small style="color: #DC3545;"><?php echo e($errors->first('mothers_language_level')); ?></small>
                            </div>
                        </div>

                        <div class="row mb-40 mt-30">
                            <div class="col-6">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input form-control" type="text" 
                                    name="emergency_contact_name" value="<?php echo e($student->admission->emergency_contact_name ?? ''); ?>">
                                    <label>emergency name *<span></span> </label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text"
                                    name="emergency_contact_family_name" value="<?php echo e($student->admission->emergency_contact_family_name ?? ''); ?>">
                                    <label>emergency family name</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-12 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text"
                                    name="emergency_relationship" value="<?php echo e($student->admission->emergency_relationship ?? ''); ?>">
                                    <label>Relationship to Child</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" 
                                    name="emergency_contact_mobile_1" value="<?php echo e($student->admission->emergency_contact_mobile_1 ?? ''); ?>">
                                    <label>Emergency phone number 1 *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" 
                                    name="emergency_contact_mobile_2" value="<?php echo e($student->admission->emergency_contact_mobile_2 ?? ''); ?>">
                                    <label>Emergency phone number 2</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-12 mt-4">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <input class="primary-input" type="text" 
                                    name="emergency_contact_address" value="<?php echo e($student->admission->emergency_contact_address ?? ''); ?>">
                                    <label>Emergency address</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40">
                            <div class="col-12 mt-4">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" name="home_adress_in_hcm"
                                    value="<?php echo e($student->admission->home_adress_in_hcm ?? ''); ?>">
                                    <label>Home address in Ho Chi Minh City *</label>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            $tooltip = "";
            if(in_array(65, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                $tooltip = "";
            }else{
                $tooltip = "You have no permission to add";
            }
        ?>
        <div class="row mt-40">
            <div class="col-lg-12 text-center">
                <button class="primary-btn fix-gr-bg" id="_submit_btn_admission" data-toggle="tooltip" title="<?php echo e($tooltip); ?>">
                    <span class="ti-check"></span>
                    <?php echo app('translator')->get('lang.save'); ?> <?php echo app('translator')->get('lang.student'); ?>
                </button>
            </div>
        </div>
        <?php echo e(Form::close()); ?>

    </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('public/backEnd/')); ?>/js/croppie.js"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/js/st_addmision.js"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/backEnd/studentInformation/student_edit.blade.php ENDPATH**/ ?>