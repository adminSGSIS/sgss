
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/css')); ?>/lightbox.css">
<?php $__env->stopPush(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('public/backEnd/js/')); ?>/lightbox-plus-jquery.js"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_content'); ?>
    <div class="banner">
    	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>campus</h3>
            </div>
    	</div>
    	
    </div>
   

    <section class="c-color">
        <div class="container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          
          <div class="carousel-inner">
             <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="carousel-item <?php if($loop->first): ?> active <?php endif; ?>">
                <a href="/public/uploads/category/<?php echo e($image->path); ?>" data-lightbox="1">
                            <img class="img-slider video_1aa w-100 fit-img" src="/public/uploads/category/<?php echo e($image->path); ?>">
                            
                        </a>
              
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick="event.preventDefault();">
                    <span class="fa fa-chevron-left"  aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" onclick="event.preventDefault();">
            <span class="fa fa-chevron-right"  aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    		<!--<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">-->
      <!--          <div class="carousel-inner d-flex justify-content-center">-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item active">-->
      <!--                  <a href="/public/images/campus/01.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/01.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/02.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/02.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/03.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/03.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/04.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/04.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/05.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/05.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/06.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/06.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--               <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                   <a href="/public/images/campus/07.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/07.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/08.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/08.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/09.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/09.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/10.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/10.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/11.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/11.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--              <div class="col-lg-8 col-sm-12 sgsis-journal carousel-item ">-->
      <!--                  <a href="/public/images/campus/12.jpg" data-lightbox="1">-->
      <!--                      <img width="100%" class="img-slider video_1aa" src="/public/images/campus/12.jpg">-->
      <!--                  </a>-->
      <!--              </div>-->
      <!--          </div>-->
      <!--          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">-->
      <!--              <span class="fa fa-chevron-left" style="color:#0c04e0;font-size:25px" aria-hidden="true"></span>-->
      <!--              <span class="sr-only">Previous</span>-->
      <!--          </a>-->
      <!--          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">-->
      <!--              <span class="fa fa-chevron-right"style="color:#0c04e0;font-size:25px" aria-hidden="true"></span>-->
      <!--              <span class="sr-only">Next</span>-->
      <!--          </a>-->
      <!--      </div>-->
    	</div>
                 
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/light_ourSchoolCampus.blade.php ENDPATH**/ ?>