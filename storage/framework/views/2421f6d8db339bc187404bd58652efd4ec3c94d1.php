 
 
 <?php $__env->startSection('css'); ?>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 <style>
    .centerTd td{
        text-align: center;
    }
    .removeOutline:focus {
        box-shadow: none;
    }
    .removeHover:hover{
        background: none;
    }
    .removeHover{
        display: inline;
        margin-left: -25px;
    }
 </style>
 <?php $__env->stopSection(); ?>
 <?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
     <div class="container-fluid">
         <div class="row justify-content-between">
             <h1>Tasks Management</h1>
             <div class="bc-pages">
                 <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
             </div>
         </div>
     </div>
 </section>
 <section class="admin-visitor-area up_st_admin_visitor">
     <div class="container-fluid p-0">
         <?php if(isset($editData)): ?>
         <?php if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
            
         <div class="row">
             <div class="offset-lg-10 col-lg-2 text-right col-md-12 mb-20">
                 <a href="<?php echo e(url('tasks/add')); ?>" class="primary-btn small fix-gr-bg">
                     <span class="ti-plus pr-2"></span>
                     <?php echo app('translator')->get('lang.add'); ?> Task
                 </a>
             </div>
         </div>
         <?php endif; ?>
         <?php endif; ?>
       <div class="row">
             <div class="col-lg-3">
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="main-title">
                             <h3 class="mb-30"><?php if(isset($editData)): ?>
                                     <?php echo app('translator')->get('lang.edit'); ?>
                                 <?php else: ?>
                                     <?php echo app('translator')->get('lang.add'); ?>
                                 <?php endif; ?>
                                 Tasks
                             </h3>
                         </div>
                         <?php if(isset($editData)): ?>
                         <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/'.$editData->id . '/edit', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                         <?php else: ?>
                         <?php if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ): ?>
            
                         <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'tasks/add', 'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                         <?php endif; ?>
                         <?php endif; ?>
                         <div class="white-box">
                             <div class="add-visitor">
                                 <div class="row">
                                     <?php if(session()->has('message-success')): ?>
                                     <div class="alert alert-success mb-20">
                                         <?php echo e(session()->get('message-success')); ?>

                                     </div>
                                     <?php elseif(session()->has('message-danger')): ?>
                                     <div class="alert alert-danger">
                                         <?php echo e(session()->get('message-danger')); ?>

                                     </div>
                                     <?php endif; ?>
                                     <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <input class="primary-input form-control"
                                            type="text" name="name_of_task" autocomplete="off" value="<?php echo e(isset($editData)? $editData->name_of_task : ''); ?><?php echo e(old('name_of_task')); ?>" required>
                                            <label>Name of task<span></span> </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <label>Choose Tags<span></span></label>
                                            <a class="deleteUrl removeHover dropdown-item" data-modal-size="modal-md" title="Choose Tags" href="/tasks/choose-tags" >
                                                <button id="tags" type="button" class="btn removeOutline ml-2" style="font-size: 11px; color: #828bb2;">
                                                    +
                                                </button>
                                            </a>
                                            
                                        </div>
                                     </div>

                                     <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <input class="primary-input form-control<?php echo e($errors->has('tag') ? ' is-invalid' : ''); ?>"
                                            type="text" name="tag" id="tag" autocomplete="off" value=" <?php echo e(isset($editData)? Modules\CRM\Entities\SmTaskType::find($editData->type_id)->title : ''); ?><?php echo e(old('tag')); ?>" readonly>
                                            <label>Tags<span></span> </label>
                                            <span class="focus-border textarea"></span>
                                            <?php if($errors->has('tag')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('tag')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                     <div class="col-lg-12 mb-20">
                                         <div class="input-effect">
                                             <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('priority') ? ' is-invalid' : ''); ?>" name="priority">
                                                 <option data-display="Priority" value="">Priority</option>
                                                 <?php $__currentLoopData = $priority; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                 <option  value="<?php echo e($value); ?>"
                                                 <?php echo e(old('priority')==$value ? 'selected' : ''); ?>

                                                 <?php if(isset($editData)): ?>
                                                 <?php if($editData->priority == $value): ?>
                                                     <?php echo app('translator')->get('lang.selected'); ?>
                                                 <?php endif; ?>
                                                 <?php endif; ?>
                                                 ><?php echo e($value); ?></option>
                                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                             </select>
                                             <span class="focus-border"></span>
                                             <?php if($errors->has('priority')): ?>
                                             <span class="invalid-feedback invalid-select" role="alert">
                                                 <strong><?php echo e($errors->first('priority')); ?></strong>
                                             </span>
                                             <?php endif; ?>
                                         </div>
                                     </div>

                                     <?php if(!isset($editData)): ?>
                                     <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <div class="no-gutters input-right-icon">
                                                <div class="col">
                                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                                        <input class="primary-input date form-control<?php echo e($errors->has('start_date') ? ' is-invalid' : ''); ?>" value="<?php echo e(isset($editData)? $editData->start_date : ''); ?><?php echo e(old('start_date')); ?>" type="text"
                                                            name="start_date" autocomplete="off" required="">
                                                        <label>Start Date</label>
                                                        <span class="focus-border"></span>
                                                        <?php if($errors->has('start_date')): ?>
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong><?php echo e($errors->first('start_date')); ?></strong>
                                                        </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="start-date-icon"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     <?php endif; ?> 

                                    <div class="col-lg-12 mb-20">
                                        <div class="input-effect">
                                            <div class="no-gutters input-right-icon">
                                                <div class="col">
                                                    <div class="input-effect sm2_mb_20 md_mb_20">
                                                        <input class="primary-input date form-control<?php echo e($errors->has('end_date') ? ' is-invalid' : ''); ?>" value="<?php echo e(isset($editData)? $editData->end_date : ''); ?><?php echo e(old('end_date')); ?>" type="text"
                                                            name="end_date" autocomplete="off" required="">
                                                        <label>End Date</label>
                                                        <span class="focus-border"></span>
                                                        <?php if($errors->has('end_date')): ?>
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong><?php echo e($errors->first('end_date')); ?></strong>
                                                        </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <button class="" type="button">
                                                        <i class="ti-calendar" id="start-date-icon"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    
 
                                    <?php if(Auth::user()->role_id == 1 || Auth::user()->role_id == 5): ?>
                                        
                                        <div class="col-lg-12">
                                            <select class="niceSelect w-100 bb form-control mb-3" onchange="addStaffFunc()" name="role_id" id="role_id">
                                                <option data-display="Role" value=""> <?php echo app('translator')->get('lang.select'); ?> </option>
                                                <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="addStaff" onclick="addStaffButton()" >
                                                <img width="20px" src="https://static.thenounproject.com/png/261370-200.png" alt="">
                                                <label style="position: absolute; top: 53px; width: 100px">ADD STAFF</label>
                                            </button>
                                            <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="closeAddStaff" onclick="closeAddStaffButton()">
                                                <img style="margin-top: -3px" width="20px" src="https://static.thenounproject.com/png/261368-200.png" alt="">
                                                <label style="position: absolute; top: 50px; width: 100px">ADD STAFF</label> 
                                            </button>
                                            <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div style="display: none" id="<?php echo e($department->id); ?>">
                                                <?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($staff->department_id == $department->id && $staff->id != App\SmStaff::where('email', Auth::user()->email)->first()->id): ?>
                                                        <div class="mt-2 <?php echo e($errors->has('staffs') ? ' is-invalid' : ''); ?>" style="color: #828bb2;">
                                                            <input onclick="getStaffName()" <?php if(isset($editData)): ?> <?php if(in_array($staff->id, $editData->assigned_users)): ?> checked <?php endif; ?> <?php endif; ?>
                                                            <?php if(old('staffs')): ?> <?php if(in_array($staff->id, old('staffs'))): ?> checked <?php endif; ?> <?php endif; ?> 
                                                            type="checkbox" class="uncheck common-checkbox" name="staffs[]" value="<?php echo e($staff->id); ?>" id="<?php echo e($staff->email); ?>">
                                                            <label id="id<?php echo e($staff->id); ?>" for="<?php echo e($staff->email); ?>"><?php echo e($staff->full_name); ?></label>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-lg-12">
                                            <input type="hidden" value="<?php echo e(App\SmStaff::where('email', Auth::user()->email)->first()->department_id); ?>" name="role_id" id="role_id_2">
                                            <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="addStaff" onclick="addStaffButton()" >
                                                <img width="20px" src="https://static.thenounproject.com/png/261370-200.png" alt="">
                                                <label style="position: absolute; top: 0px; width: 100px">ADD STAFF</label>
                                            </button>
                                            <button style="border: none; background: none; display: none; color: #828bb2; margin-left: -7px" type="button" id="closeAddStaff" onclick="closeAddStaffButton()">
                                                <img style="margin-top: -3px" width="20px" src="https://static.thenounproject.com/png/261368-200.png" alt="">
                                                <label style="position: absolute; top: -3px; width: 100px">ADD STAFF</label> 
                                            </button>
                                            <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $department): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div style="display: none" id="<?php echo e($department->id); ?>">
                                                <?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($staff->department_id == $department->id && $staff->id != App\SmStaff::where('email', Auth::user()->email)->first()->id): ?>
                                                        <div class="mt-2 <?php echo e($errors->has('staffs') ? ' is-invalid' : ''); ?>" style="color: #828bb2;">
                                                            <input onclick="getStaffName()" <?php if(isset($editData)): ?> <?php if(in_array($staff->id, $editData->assigned_users)): ?> checked <?php endif; ?> <?php endif; ?> 
                                                            <?php if(old('staffs')): ?> <?php if(in_array($staff->id, old('staffs'))): ?> checked <?php endif; ?> <?php endif; ?>
                                                            type="checkbox" class="uncheck common-checkbox" name="staffs[]" value="<?php echo e($staff->id); ?>" id="<?php echo e($staff->email); ?>">
                                                            <label id="id<?php echo e($staff->id); ?>" for="<?php echo e($staff->email); ?>"><?php echo e($staff->full_name); ?></label>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    <?php endif; ?>

                                    <div class="col-lg-12 mb-20">
                                        <div id="getStaff" class="input-effect">
                                            <label>Assigned Staffs<span></span></label> </br>
                                        </div>
                                     </div>

                                    
 
                                     <div class="col-lg-12 mb-20 mt-2">
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control" cols="0" rows="5" name="description" id="description"><?php echo e(isset($editData) ? $editData->description : ''); ?><?php echo e(old('description')); ?></textarea>
                                            <label><?php echo app('translator')->get('lang.description'); ?> <span></span> </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                     </div>

                              </div>
                                   <?php 
                                   $tooltip = "";
                                   if(in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1 ){
                                         $tooltip = "";
                                     }else{
                                         $tooltip = "You have no permission to add";
                                     }
                                 ?>
                                 <div class="row mt-40">
                                     <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="<?php echo e($tooltip); ?>">
 
                                             <span class="ti-check"></span>
                                             <?php if(isset($editData)): ?>
                                                 <?php echo app('translator')->get('lang.update'); ?>
                                             <?php else: ?>
                                                 <?php echo app('translator')->get('lang.save'); ?>
                                             <?php endif; ?>
                                         </button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <?php echo e(Form::close()); ?>

                     </div>
                 </div>
             </div>
 
             <div class="col-lg-9">
                
           <div class="row">
             <div class="col-lg-4 no-gutters">
                 <div class="main-title">
                     <h3 class="mb-0">Tasks List</h3>
                 </div>
             </div>
         </div>
 
         <div class="row">
 
             <div class="col-lg-12">
                 <table id="table_id" class="display school-table" cellspacing="0" width="100%">
 
                     <thead>
                         <?php if(session()->has('message-success-delete') != "" ||
                                 session()->get('message-danger-delete') != ""): ?>
                                 <tr>
                                     <td colspan="6">
                                          <?php if(session()->has('message-success-delete')): ?>
                                           <div class="alert alert-success">
                                               <?php echo e(session()->get('message-success-delete')); ?>

                                           </div>
                                         <?php elseif(session()->has('message-danger-delete')): ?>
                                           <div class="alert alert-danger">
                                               <?php echo e(session()->get('message-danger-delete')); ?>

                                           </div>
                                         <?php endif; ?>
                                     </td>
                                 </tr>
                              <?php endif; ?>
                         <tr>
                             <th>Tasks Name</th>
                             <th>Priority</th>
                             <th>Tasks Type</th>
                             <th>Created By</th>
                             <th><?php echo app('translator')->get('lang.action'); ?></th>
                         </tr>
                     </thead>
 
                     <tbody>
                         <?php if(isset($filterTask)): ?>
                         <?php $__currentLoopData = $filterTask; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <tr class="centerTd">
                             <td><?php echo e($value->name_of_task); ?></td>
                             <td <?php if($value->priority == "high"): ?> style="color: red" <?php endif; ?>><?php echo e($value->priority); ?></td>
                             <td>
                                <button class="<?php echo e(Modules\CRM\Entities\SmTaskType::find($value->type_id)->color); ?> removeOutline" style="font-size: 11px;">
                                    <?php echo e(Modules\CRM\Entities\SmTaskType::find($value->type_id)->title); ?>

                                </button>  
                            </td>                              
                             <td><?php echo e(App\SmStaff::find($value->created_by)->full_name); ?></td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                         <?php echo app('translator')->get('lang.select'); ?>
                                     </button>
                                     <div class="dropdown-menu dropdown-menu-right">

                                        <?php if(Auth::user()->role_id != 1 && $value->created_by != App\SmStaff::where('email', Auth::user()->email)->first()->id): ?>
                                            <a class="dropdown-item" href="<?php echo e(url('tasks/'.$value->id.'/planning')); ?>">Planning</a>
                                        <?php endif; ?>
                                        <?php if(Modules\CRM\Entities\SmTaskPlan::where('task_id', $value->id)->where('created_by', App\SmStaff::where('email', Auth::user()->email)->first()->id)->first()): ?>
                                            <a class="deleteUrl dropdown-item" href="<?php echo e(url('tasks/'.$value->id.'/actual')); ?>" title="Task Level">Actual</a>
                                        <?php endif; ?>

                                        <a class="dropdown-item" href="<?php echo e(url('tasks/'.$value->id)); ?>"><?php echo app('translator')->get('lang.view'); ?></a>
                                        
                                        <?php if(Auth::user()->role_id == 1 || $value->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id): ?>
                                            <a class="dropdown-item" href="<?php echo e(url('tasks/'.$value->id.'/edit')); ?>"><?php echo app('translator')->get('lang.edit'); ?></a> 
                                        <?php endif; ?>
                                        <?php if(Auth::user()->role_id == 1 || $value->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id): ?>
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Disable Task" href="<?php echo e(url('tasks/'.$value->id.'/disable-view')); ?>">disable</a>
                                        <?php endif; ?>
                                     </div>
                                 </div>
                             </td>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                         <?php endif; ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>
 </div>
 </section>
 <?php if(Auth::user()->role_id == 1 || Auth::user()->role_id == 5): ?>
 <script>
    var id, oldId;
    var staffsName = [];
    var inputs = document.getElementsByClassName('uncheck'); 
    for(var i = 0; i < inputs.length; i++){
        if(inputs[i].checked){
            if(document.getElementById('getStaff').children.length >= 2){
                if(staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML) == -1){
                    var node = document.createElement("span");
                    node.className = "badge badge-dark";
                    var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                    node.appendChild(textnode);
                    document.getElementById("getStaff").appendChild(node);
                    staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                }
            }
            else{
                staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                var node = document.createElement("span");
                node.className = "badge badge-dark";
                var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                node.appendChild(textnode);
                document.getElementById("getStaff").appendChild(node);
            }
        }
        else{
            var index = staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML);
            if(index != -1){
                staffsName.splice(index, 1);
                document.getElementById('getStaff').children[index + 2].parentNode.removeChild(document.getElementById('getStaff').children[index + 2]); 
            }
        }
    }
    function addStaffFunc(){
        var x = document.getElementById("role_id").value;
        id = x;
        document.getElementById("addStaff").style.display = 'block';
        document.getElementById(oldId || x).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
    }
    function addStaffButton(){
        oldId = id;
        document.getElementById(id).style.display = 'block';
        document.getElementById('closeAddStaff').style.display = 'block';
        document.getElementById('addStaff').style.display = 'none';
    }
    function closeAddStaffButton(){
        document.getElementById(id).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
        document.getElementById('addStaff').style.display = 'block';
    }
    function getStaffName(){
        var inputs = document.getElementsByClassName('uncheck'); 
        for(var i = 0; i < inputs.length; i++){
            if(inputs[i].checked){
                if(document.getElementById('getStaff').children.length >= 2){
                    if(staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML) == -1){
                        var node = document.createElement("span");
                        node.className = "badge badge-dark";
                        var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                        node.appendChild(textnode);
                        document.getElementById("getStaff").appendChild(node);
                        staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                    }
                }
                else{
                    staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                    var node = document.createElement("span");
                    node.className = "badge badge-dark";
                    var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                    node.appendChild(textnode);
                    document.getElementById("getStaff").appendChild(node);
                }
            }
            else{
                var index = staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML);
                if(index != -1){
                    staffsName.splice(index, 1);
                    document.getElementById('getStaff').children[index + 2].parentNode.removeChild(document.getElementById('getStaff').children[index + 2]); 
                }
            }
        }
    }
</script>
<?php else: ?>
<script>
    var x = document.getElementById("role_id_2").value;
    id = x;
    var staffsName = [];
    document.getElementById("addStaff").style.display = 'block';
    document.getElementById(x).style.display = 'none';
    document.getElementById('closeAddStaff').style.display = 'none';
    function addStaffButton(){
        document.getElementById(id).style.display = 'block';
        document.getElementById('closeAddStaff').style.display = 'block';
        document.getElementById('addStaff').style.display = 'none';
    }
    function closeAddStaffButton(){
        document.getElementById(id).style.display = 'none';
        document.getElementById('closeAddStaff').style.display = 'none';
        document.getElementById('addStaff').style.display = 'block';
    }
    function getStaffName(){
        var inputs = document.getElementsByClassName('uncheck'); 
        for(var i = 0; i < inputs.length; i++){
            if(inputs[i].checked){
                if(document.getElementById('getStaff').children.length >= 2){
                    if(staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML) == -1){
                        var node = document.createElement("span");
                        node.className = "badge badge-dark";
                        var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                        node.appendChild(textnode);
                        document.getElementById("getStaff").appendChild(node);
                        staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                    }
                }
                else{
                    staffsName[staffsName.length] = document.getElementById("id" + inputs[i].value).innerHTML;
                    var node = document.createElement("span");
                    node.className = "badge badge-dark";
                    var textnode = document.createTextNode(document.getElementById("id" + inputs[i].value).innerHTML);
                    node.appendChild(textnode);
                    document.getElementById("getStaff").appendChild(node);
                }
            }
            else{
                var index = staffsName.indexOf(document.getElementById("id" + inputs[i].value).innerHTML);
                if(index != -1){
                    staffsName.splice(index, 1);
                    document.getElementById('getStaff').children[index + 2].parentNode.removeChild(document.getElementById('getStaff').children[index + 2]); 
                }
            }
        }
    }
</script>
<?php endif; ?>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/CRM/Resources/views/TasksManagement/newTask.blade.php ENDPATH**/ ?>