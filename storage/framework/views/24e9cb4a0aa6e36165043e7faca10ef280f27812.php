
<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Feedback</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <form action="/feedback-store/<?php echo e($student->id); ?>" method="post">
        
            <?php echo csrf_field(); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                        <div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-center">
                                    <h1 style="font-size:400%">FEEDBACK</h1>
                                </div>
                            </div>
                            <div class="row mb-3" style="padding-top:20px"
                            <?php echo e((Auth::user()->staff->designation_id!=14) && (Auth::user()->staff->designation_id!=13) && Auth::user()->staff->designation_id!=4? 'hidden' : ''); ?>>
                                <div class="col-12">
                                    <div class="input-effect">
                                        <label>EAL Assessments</label>
                                        <textarea class="primary-input form-control  border border-primary" name="eal"
                                            rows="5" type="text"
                                            style="padding:10px;10px;10px;10px"><?php echo e($student->assessment->eal ?? ''); ?></textarea>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3"
                            <?php echo e((Auth::user()->staff->designation_id!=14) && (Auth::user()->staff->designation_id!=13) && Auth::user()->staff->designation_id!=4? 'hidden' : ''); ?>>
                                <div class="col-12">
                                    <div class="input-effect">
                                        <label>EAL Support (HOURS/WEEK)</label>
                                        <input min="0" max="100"
                                            class="primary-input form-control border border-primary" name="eal_support"
                                            type="number" style="padding:10px;10px;10px;10px"
                                            value="<?php echo e($student->assessment->eal_support ?? ''); ?>">
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3" style="padding-top: 65px"
                            <?php echo e((Auth::user()->staff->designation_id!=45) &&(Auth::user()->staff->designation_id!=13) && Auth::user()->staff->designation_id!=4 ? 'hidden' : ''); ?>>
                                <div class="col-12 ">
                                    <div class="input-effect">
                                        <label>SEN Assessments</label>
                                        <textarea class="primary-input form-control  border border-primary" name="sen"
                                            style="padding:10px;10px;10px;10px" rows="5"
                                            type="text"><?php echo e($student->assessment->sen ?? ''); ?></textarea>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                                <div class="col-12" style="padding-top:10px">
                                    <div class="input-effect">
                                        <label>SEN Support (HOURS/WEEK)</label>
                                        <input min="0" max="100"
                                            class="primary-input form-control  border border-primary" name="sen_support"
                                            type="number" style="padding:10px;10px;10px;10px"
                                            value="<?php echo e($student->assessment->sen_support ?? ''); ?>">
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>                            
                            <div class="row mb-3" style="padding-top: 70px"
                            <?php echo e((auth()->user()->staff->id != $student->admission->trial_teacher_id) && (Auth::user()->staff->designation_id!=13) && Auth::user()->staff->designation_id!=4 ? 'hidden' : ''); ?>>
                                <div class="col-12">
                                    <div class="input-effect">
                                        <label>Class Teacher Assessments</label>
                                        <textarea class="primary-input form-control  border border-primary"
                                            name="class_teacher" rows="5" type="text"
                                            style="padding:10px;10px;10px;10px"><?php echo e($student->assessment->class_teacher ?? ''); ?></textarea>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3" style="padding-top: 70px"
                            <?php echo e(Auth::user()->staff->designation_id!=13 && Auth::user()->staff->designation_id!=4 ? 'hidden' : ''); ?>>
                                <div class="col-12">
                                    <div class="input-effect ">
                                        <label>Phase leader/Head teacher's recommendation</label>
                                        <textarea class="primary-input form-control  border border-primary"
                                            name="head_teacher_recommendation" rows="5" type="text"
                                            style="padding:10px;10px;10px;10px"><?php echo e($student->assessment->head_teacher_recommendation ?? ''); ?></textarea>

                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-40">
                        <div class="col-lg-12 text-center">
                            <button class="primary-btn fix-gr-bg" id="_submit_btn_admission">
                                <span class="ti-check"></span>
                                <?php echo app('translator')->get('lang.save'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/backEnd/studentInformation/feedback_v1.blade.php ENDPATH**/ ?>