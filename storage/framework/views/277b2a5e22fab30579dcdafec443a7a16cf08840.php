
<?php $__env->startSection('mainContent'); ?>
<style>
    .pad-top-20 {
        padding-top: 20px;
    }
    .pad-bot-20{
        padding-bottom: 20px;
    }
    .hidden {
        display: none;
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #05B7FF !important;
}
</style>
<div class="row pad-bot-20">
    <div class="col-2">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <a class="nav-link active" id="v-pills-current-tab" data-toggle="pill" href="#v-pills-current" role="tab" aria-selected="true">Current in school</a>
        <a class="nav-link" id="v-pills-passerby-tab" data-toggle="pill" href="#v-pills-passerby" role="tab" aria-selected="false">Visitor</a>
        </div>
    </div>
    <div class="col-10">
      <div class="tab-content" id="v-pills-tabContent">
        <div class="tab-pane  fade show active" id="v-pills-current" role="tabpanel">
            <form method="Get" action="<?php echo e(route('ShowMedical')); ?>">
                <?php echo csrf_field(); ?>
                <div class="white-box">
                    <h2 class="center" style="text-align: center;">Create Medical</h2>
                    <div class="container">
                        <div class="pad-top-20">
                            <h1>Patient Type</h1>
                            <div class="row">
                                <div class="col-lg-4 input-effect">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="1" name="patientType" id="studentRadio"
                                            onclick="checkType()">
                                        <label class="form-check-label" for="studentRadio">
                                            Student
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4 input-effect">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="2" name="patientType" id="teacherRadio"
                                            onclick="checkType()">
                                        <label class="form-check-label" for="teacherRadio">
                                            Teacher
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4 input-effect">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="3" name="patientType" id="staffRadio"
                                            onclick="checkType()">
                                        <label class="form-check-label" for="staffRadio">
                                            Employeer
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="studentBoard" class="hidden">
                            <div class="pad-top-20">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <select name="student_id" id="student_list"
                                            class="niceSelect w-100 bb form-control<?php echo e(@$errors->has('students') ? ' is-invalid' : ''); ?>">
                                            <option data-display="<?php echo app('translator')->get('student name'); ?> *" value=""><?php echo app('translator')->get('student name'); ?> *</option>
                                            <?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(isset($add_income)): ?>
                                            <option value="<?php echo e(@$student->id); ?>"
                                                <?php echo e(@$add_income->student_id == @$student->id? 'selected': ''); ?>>
                                                <?php echo e(@$student->full_name); ?> | <?php echo e(@$student->admission_no); ?></option>
                                            <?php else: ?>
                                            <option label="<?php echo e(@$student->admission_no); ?>" value="<?php echo e(@$student->id); ?>"
                                                <?php echo e(old('student_list') == @$student->id? 'selected' : ''); ?>><?php echo e(@$student->full_name); ?> |
                                                <?php echo e(@$student->admission_no); ?></option>
                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                </div>
                            </div>
            
                        </div>
                        <div id="teacherBoard" class="pad-top-20 hidden">
                            <div class="pad-top-20">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                    <div class="col-lg-6 col-md-6 sm-12">
                                        <select  id="student_list" name="teacher_id"
                                            class="niceSelect w-100 bb form-control<?php echo e(@$errors->has('staffs') ? ' is-invalid' : ''); ?>">
                                            <option data-display="<?php echo app('translator')->get('staff name'); ?> *" value=""><?php echo app('translator')->get('staff name'); ?> *</option>
                                            <?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($teacher->id); ?>"><?php echo e($teacher->full_name); ?> | <?php echo e($teacher->staff_no); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="staffBoard" class="pad-top-20 hidden">
                            <div class="pad-top-20">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                    <div class="col-lg-6 col-md-6 sm-12">
                                        <select  id="student_list" name="staff_id"
                                            class="niceSelect w-100 bb form-control<?php echo e(@$errors->has('staffs') ? ' is-invalid' : ''); ?>">
                                            <option data-display="<?php echo app('translator')->get('staff name'); ?> *" value=""><?php echo app('translator')->get('staff name'); ?> *</option>
                                            <?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($staff->id); ?>"><?php echo e($staff->full_name); ?> | <?php echo e($staff->staff_no); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12"></div>
                        </div>
                        <div class="d-flex justify-content-end" style="padding-top: 35px">
                            <input type="submit" class="primary-btn small fix-gr-bg" value="search">
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="tab-pane fade" id="v-pills-passerby" role="tabpanel">
            <form action="<?php echo e(route('newGuest')); ?>">
                <div class="white-box">
                    <h2 class="center" style="text-align: center;">New Guest</h2>
                    <div class="container">
                        <div class="pad-top-20">
                            <h1>Guest Information</h1>                        
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputName">Name</label>
                                    <input type="text" class="form-control primary-input" id="inputName" name="name" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputEmail">Email (Parent's email)</label>
                                    <input type="text" class="form-control primary-input" id="inputEmail" name="email" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputPhone">Phone (Parent's phone)</label>
                                    <input type="text" class="form-control primary-input" id="inputPhone" name="phone" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputName">Height</label>
                                    <input type="text" class="form-control primary-input" id="inputHeight" name="height" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputEmail">Weight</label>
                                    <input type="text" class="form-control primary-input" id="inputWeight" name="weight" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputBlood">Blood Group</label>
                                    <select name="bloodgroup" id="inputBlood" class="custom-select primary-input">
                                        <?php $__currentLoopData = $blood_groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->base_setup_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputAddress">Address</label>
                                    <input type="text" class="form-control primary-input" id="inputAddress" name="address" required>
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputGender">Gender</label>
                                    <select name="gender" id="inputGender" class="custom-select primary-input">
                                        <option value="" disabled selected hidden>Select</option>
                                        <?php $__currentLoopData = $gender; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($item->id); ?>"><?php echo e($item->base_setup_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>                                    
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div>
                                    <label for="inputBirth">Day of birth</label>
                                    <input type="text" class="form-control primary-input date" id="inputBirth" name="day_of_birth">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <input type="text" name="patientType" value="4" hidden>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end" style="padding-top: 35px">
                            <input type="submit" class="primary-btn small fix-gr-bg" value="continue">
                        </div>
                    </div>
                </div>
            </form>
            
        </div>
      </div>
    </div>
  </div>
  <p></p>
  <section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3><?php echo app('translator')->get('Patient List'); ?></h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                        <thead>
                            <?php if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != ""): ?>
                            <tr>
                                <td colspan="7">
                                    <?php if(session()->has('message-success-delete')): ?>
                                    <div class="alert alert-success">
                                        <?php echo e(session()->get('message-success-delete')); ?>

                                    </div>
                                    <?php elseif(session()->has('message-danger-delete')): ?>
                                    <div class="alert alert-danger">
                                        <?php echo e(session()->get('message-danger-delete')); ?>

                                    </div>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><a href="<?php echo e(url('/medical/patient-PDF')); ?>" class="primary-btn small fix-gr-bg">
                                    <?php echo app('translator')->get('Excel'); ?>
                                    </a></th>
                            </tr>
                            <tr>
                                <th><?php echo app('translator')->get('No'); ?></th>
                                <th><?php echo app('translator')->get('Patient'); ?></th>
                                <th><?php echo app('translator')->get('Symtoms Type'); ?></th>
                                <th><?php echo app('translator')->get('Date'); ?></th>
                                <th><?php echo app('translator')->get('Old Patitent'); ?></th>
                                <th><?php echo app('translator')->get('action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $patients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $patient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($patient->id); ?></td>
                                <td><?php echo e($patient->patient_name); ?></td>
                                <td><?php echo e($patient->symtomeRelation->name); ?></td>
                                <td><?php echo e($patient->date); ?></td>
                                <td>
                                    <?php if(( $patient->old_patient )==1): ?>
                                    Yes
                                    <?php else: ?>
                                    No
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            <?php echo app('translator')->get('lang.select'); ?>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a title="View/Edit Leave Details"
                                                class="dropdown-item" href="<?php echo e(url('medical/patient-edit/'.$patient->id.'/'.$patient->patient_type.'/'.$patient->patient_id)); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                                <a title="Export PDF file" target="_blank"
                                                class="dropdown-item" href="<?php echo e(url('medical/patient-PDF/'.$patient->id)); ?>"><?php echo app('translator')->get('Export PDF'); ?></a>
                                            <a class="dropdown-item" data-toggle="modal"
                                                data-target="#DeletePatient<?php echo e($patient->id); ?>"
                                                href="#"><?php echo app('translator')->get('lang.delete'); ?></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade admin-query" id="DeletePatient<?php echo e($patient->id); ?>">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><?php echo app('translator')->get('lang.delete'); ?> <?php echo app('translator')->get('lang.item'); ?></h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <h4><?php echo app('translator')->get('lang.are_you_sure_to_delete'); ?></h4>
                                            </div>

                                            <div class="mt-40 d-flex justify-content-between">
                                                <button type="button" class="primary-btn tr-bg"
                                                    data-dismiss="modal"><?php echo app('translator')->get('lang.cancel'); ?></button>
                                                <?php echo e(Form::open(['url' => 'medical/patient-delete/'.$patient->id, 'method' => 'DELETE', 'enctype' => 'multipart/form-data'])); ?>

                                                <button class="primary-btn fix-gr-bg"
                                                    type="submit"><?php echo app('translator')->get('lang.delete'); ?></button>
                                                <?php echo e(Form::close()); ?>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script>
    function checkType() {
        if (document.getElementById('studentRadio').checked) {
            document.getElementById("studentBoard").removeAttribute("class", "hidden");
            document.getElementById("teacherBoard").setAttribute("class", "hidden");
            document.getElementById("staffBoard").setAttribute("class", "hidden")
        } else if (document.getElementById('teacherRadio').checked) {
            document.getElementById("studentBoard").setAttribute("class", "hidden");
            document.getElementById("teacherBoard").removeAttribute("class", "hidden");
            document.getElementById("staffBoard").setAttribute("class", "hidden")
        } else if (document.getElementById('staffRadio').checked) {
            document.getElementById("studentBoard").setAttribute("class", "hidden");
            document.getElementById("teacherBoard").setAttribute("class", "hidden");
            document.getElementById("staffBoard").removeAttribute("class", "hidden")
        }
    }
</script>
<script type="text/javascript">
    $('#student_list').on('change', function () {
        var id = $('#student_list option:selected').attr('label');
        $('#admission_no').val(id);
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/Medical\Resources/views/medicalSearch.blade.php ENDPATH**/ ?>