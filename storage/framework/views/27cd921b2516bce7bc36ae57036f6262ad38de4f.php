<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    <!--<link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i' rel='stylesheet'>-->
</head>
<style>
    .content-policy p {
        text-align: justify;
        font-family: 'Roboto';
        font-size: 15px;
        padding-bottom: 0;
    }

    .content-policy h5 {
        color: #191970;
        font-family: 'Roboto';
    }

    .form>div {
        border-bottom-style: dotted;
    }

</style>

<body>
    <div class="main_content">
        <div class="">
            <div class="text-center mt-5">
                <img src="<?php echo e(asset('public/header_letter.png')); ?>" width="100%">
            </div>
            <div class="text-center mt-3 title">
                <h3>Acceptable Use Policy for Parents/Carers</h3>
            </div>
            <div class="content-policy">
                <p>1. I know that my child will be provided with internet access and will use a range of IT
                    systems in order to access the curriculum and be prepared for modern life whilst at Saigon
                    Star International School.
                </p>
                <p>2. I am aware that learners' use of mobile technology, such as mobile phones or smart
                    watches, is not permitted whilst on school premises.
                </p>
                <p>3. I am aware that any internet and technology use using Saigon Star equipment may
                    be monitored for safety and security reasons, to safeguard both my child and the school
                    systems. This monitoring will take place in accordance with data protection (including
                    GDPR) and human rights legislation.
                </p>
                <p>4. I understand that the school will take every reasonable precaution, including
                    monitoring and filtering systems, to ensure that learners are safe when they use the
                    internet and systems. I understand that the school cannot ultimately be held responsible
                    for the nature and content of materials accessed on the internet and using mobile
                    technologies.
                </p>
                <p>5. I understand that my child needs a safe and appropriate place to access remote
                    learning if Saigon Star is closed in response to Covid-19 or for any other reason. I will
                    ensure my child’s access to remote learning is appropriately supervised. I will ensure that
                    remote learning is treated the same way as onsite learning with children adequately
                    prepared, in an appropriate location (e.g. not in bed), suitably dressed and not given food
                    or be distracted otherwise, during the course of any lesson or online activity.
                </p>
                <p>6. I am aware that my child will receive online safety education to help them
                    understand the importance of safe use of technology and the internet, both in and out of
                    Saigon Star but that <a class="font-weight-bold"> raising digitally responsible and safe learners,
                        is the job of both
                        adults at school and at home.</a>
                </p>
                <p>7. I have read and discussed the school’s learner Acceptable Use of Technology Policy
                    (AUP) with my child.
                </p>
                <p>8. I know I can seek support from the school about online safety, such as via the school
                    website or through other recommended resources (such as <a href="https://www.thinkuknow.co.uk"
                        target="_blank" class="text-primary">www.thinkuknow.co.uk</a>) to help
                    keep my child safe online at home.
                </p>
                <p>9. I will support the school’s approach to online safety. I will role model safe and
                    positive online behaviour for my child by sharing images, text, and video online responsibly.
                    I know that some social media platforms and apps have age restrictions for good reasons
                    and understand the risks attached if my child does not follow these guidelines.
                </p>
                <p>10. I, together with my child, will not deliberately upload or add any images, video,
                    sounds or text that could upset, threaten the safety of or offend any member of the school
                    community. If I upload any videos or photos of other pupils from Saigon Star, I will ensure I
                    have obtained permission from the parent/carer beforehand (this does not apply to long
                    shots of live performances/concerts).
                </p>
                <p>11. I understand that if I or my child do not abide by the AUP, appropriate action will be
                    taken. This could include sanctions being applied in line with any policies and if a criminal
                    offence has been committed, it will be processed according to the law.
                </p>
                <p>12. I know that I can speak to the Designated Safeguarding Lead <a class="font-weight-bold">(James
                        Quantrilll)</a>, the
                    Deputy Designated Safeguarding Lead, <a class="font-weight-bold">(Neil Jones)</a>, or my child’s
                    class teacher if I have
                    any concerns about online safety. I understand I will be contacted if Saigon Star has any
                    concerns about either my or my child’s online behaviour.
                </p>
            </div>
            <div class="mt-5 content-policy">
                <h5>I have read, understood and agreed to comply with the Saigon Star Parent/Carer
                    Acceptable Use Policy.
                </h5>
            </div>

            <div class="form">
                <div>
                    <label>Child’s name : <?php echo e($data->child_name); ?></label>
                </div>
                <div>
                    <label>Class : <?php echo e($data->class->class_name); ?></label>
                </div>
                <div>
                    <label>Parent/Carers Name : <?php echo e($data->parent_name); ?></label>
                </div>
                <div style="border: none">
                    <label>Parent/Carers Signature : </label><br><br>
                    <img src="<?php echo e($data->signature); ?>" width="150">
                </div>
            </div>

            <div>
                <img src="<?php echo e(asset('public/footer_letter.png')); ?>" alt="" width="100%">
            </div>
        </div>
    </div>
</body>

</html>
<?php /**PATH /home/sgstared/public_html/resources/views/pdf/acceptable_use_policy_pdf.blade.php ENDPATH**/ ?>