
<?php $__env->startSection('main_content'); ?>
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/css')); ?>/lightbox.css">
<?php $__env->stopPush(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('public/backEnd/js/')); ?>/lightbox-plus-jquery.js"></script>
<?php $__env->stopSection(); ?>
<section class="academics-area mt-20">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 col-md-7">
                        <h3 class="title">Our School Gallery</h3>
                    </div>
                </div>
                <div class="row">
                    <?php if(!count($album)): ?>
                    <div class="col-lg-12 col-md-12">
                        <div class="text-center">
                            <p>Empty</p>
                        </div>
                    </div>
                    <?php else: ?>
                    <?php $__currentLoopData = $album; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="academic-item">
                            <div class="academic-img">
                                <?php $__currentLoopData = $i["gallery"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <a href="public/uploads/category/<?php echo e($img['path']); ?>" data-lightbox="<?php echo e($i['album']['id']); ?>">
                                    <?php if($loop->index == 0): ?>
                                    <img src="public/uploads/category/<?php echo e($img['path']); ?>" alt="<?php echo e($img['created_at']); ?>" style="border-radius: 10px; object-fit: cover">
                                    <?php endif; ?>
                                </a>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/light_category.blade.php ENDPATH**/ ?>