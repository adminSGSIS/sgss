<?php
    use App\SmGeneralSettings; 
    $school_settings= SmGeneralSettings::where('school_id',Auth::user()->school_id)->first();
    if (Auth::user() == "") { header('location:' . url('/login')); exit(); }
    Session::put('permission', App\GlobalVariable::GlobarModuleLinks());
?>
<?php if(SmGeneralSettings::isModule('Saas')== TRUE && Auth::user()->is_administrator=="yes" && Session::get('isSchoolAdmin')==FALSE ): ?>
    <?php echo $__env->make('backEnd/partials/saas_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



<?php else: ?>




<input type="hidden" name="url" id="url" value="<?php echo e(url('/')); ?>">
<nav id="sidebar">
    <div class="sidebar-header update_sidebar">
        <a href="<?php echo e(url('/')); ?>">
          <img  src="<?php echo e(file_exists(@$school_settings->logo) ? asset($school_settings->logo) : asset('public/uploads/settings/logo.png')); ?>" alt="logo">
        </a>
        <a id="close_sidebar" class="d-lg-none">
            <i class="ti-close"></i>
        </a>
    </div>
    
    <ul class="list-unstyled components">
        <?php if(Auth::user()->role_id != 2 && Auth::user()->role_id != 3 ): ?>

            
    
                <li>
                    <a href="<?php echo e(url('/admin-dashboard')); ?>" id="admin-dashboard">
                        <span class="flaticon-speedometer"></span>
                        <?php echo app('translator')->get('lang.dashboard'); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo e(url('/admin-category')); ?>">

                        <span class="flaticon-inventory"></span>
                        categories images
                    </a>
                 </li>

                <li>
                    <a href="<?php echo e(url('/docs')); ?>">
                        <span class="flaticon-book-1"></span>
                        document
                    </a>
                </li>
                
            <?php endif; ?>
            
            
                
            <li>
                <a href="#subMenuDevices" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span>
                        <img style="margin-left: 18px" src="/public/images/device.png" width="17px" alt="">
                    </span>
                    Devices Management
                </a>
                <ul class="collapse list-unstyled" id="subMenuDevices">
                    <li>
                        <a href="#" onclick="qrcheck()">QRCODE Checking</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('devices')); ?>">In Stock</a>
                    </li>
                
                    <?php if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16): ?>
                    <li>
                        <a href="<?php echo e(url('devices/repairing')); ?>">Repairing Devices</a>
                    </li>
                    <?php endif; ?>
               
                
                    <li>
                        <a href="<?php echo e(url('devices/equipments-handover')); ?>">Equipments Handover</a>
                    </li>
                </ul>
            </li>
            
            <li>
                <a href="#subMenuSalary" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-wallet"></span>
                    Payroll
                </a>
                <ul class="collapse list-unstyled" id="subMenuSalary">
                    <li>
                        <a href="<?php echo e(url('salary/me/search')); ?>">My Payroll</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('workday').'/'.Auth::user()->id.'/'.date('Y')); ?>">Work day</a>
                    </li>
                </ul>
            </li>
            

            
            
            <li>
                <a href="#onlineForms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="fa fa-globe" style="padding-left: 20px"></span>
                    Online forms
                </a>
                <ul class="collapse list-unstyled" id="onlineForms">
                    <li>
                        <a href="<?php echo e(url('return-to-work')); ?>">Form return to work</a>
                    </li>

                    <li>
                        <a href="<?php echo e(url('admin/medical-declaration/entry')); ?>">Medical Declaration Entry</a>
                    </li>
                    
                    <li>
                        <a href="<?php echo e(url('admin/medical-declaration/domestic')); ?>">Medical Declaration Domestic</a>
                    </li>
                </ul>
            </li>

            
                <li>
                <a href="#subMenuLeaveManagement" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle">
                    <span class="flaticon-slumber"></span>
                    <?php echo app('translator')->get('lang.leave'); ?>
                </a>
                <ul class="collapse list-unstyled" id="subMenuLeaveManagement">
                   
                        <li>
                            
                            <a href="<?php echo e(url('approve-leave')); ?>"><?php echo app('translator')->get('your approve request'); ?></a>
                            
                            <a href="<?php echo e(url('pending-leave')); ?>"><?php echo app('translator')->get('your pending request'); ?></a>
                            
                        </li>
                    
                   
                        <li>
                            <a href="<?php echo e(url('apply-leave')); ?>"><?php echo app('translator')->get('lang.apply_leave'); ?></a>
                        </li>
                    
                </ul>
            </li>
            

           

            

            <li>
                    <a href="#subMenuCommunicate" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <span class="flaticon-email"></span>
                        <?php echo app('translator')->get('lang.communicate'); ?>
                    </a>
                    <ul class="collapse list-unstyled" id="subMenuCommunicate">
                        
                            <li>
                                <a href="<?php echo e(url('notice-list')); ?>"><?php echo app('translator')->get('lang.notice_board'); ?></a>
                            </li>
                        
                        <?php if(@$config->Saas == 1 && Auth::user()->is_administrator != "yes" ): ?>
                            
                            <li>
                                <a href="<?php echo e(url('administrator-notice')); ?>"><?php echo app('translator')->get('lang.administrator'); ?> <?php echo app('translator')->get('lang.notice'); ?></a>
                            </li>

                        <?php endif; ?>
                        
                        
                            <li>
                                <a href="<?php echo e(url('send-email')); ?>">Send Email</a>
                            </li>
                        
                        
                            <li>
                                <a href="<?php echo e(url('email-sms-log')); ?>"><?php echo app('translator')->get('lang.email_sms_log'); ?></a>
                            </li>
                        
                    </ul>
                </li>

            
            
            <li>
                <a href="#subCustomerFacebook" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <span class="flaticon-consultation"></span>
                    CRM
                </a>
                <ul class="collapse list-unstyled" id="subCustomerFacebook">
                    <?php if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1): ?>
                        <li>
                            <a href="<?php echo e(url('customer-facebook/add')); ?>">add customer facebook</a>
                        </li>
                    <?php endif; ?>
                    <?php if(@in_array(571, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1): ?>
                        <li>
                            <a href="<?php echo e(url('customer-facebook/search-customer')); ?>">search customer facebook</a>
                        </li>
                    <?php endif; ?>
                     <li>
                        <a href="<?php echo e(url('tasks/add')); ?>">Work Flow</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('tasks/disabled')); ?>">Disabled Task</a>
                    </li>
                </ul>
            </li>

            

            

            
                
            
       
        <!-- Student Panel --> 
       

    </ul>
</nav>
<?php endif; ?>
<?php /**PATH D:\WWW\SGS\sgss\resources\views/backEnd/partials/sidebars/staff_sidebar.blade.php ENDPATH**/ ?>