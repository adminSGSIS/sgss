
<?php $__env->startSection('main_content'); ?>
    <?php $__env->startPush('css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
        <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css" />
    <?php $__env->stopPush(); ?>

    <div class="banner">
        <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>BUS SCHEDULE</h3>
            </div>
        </div>
    </div>
    <div class="main_content">
        <div class="c-color pt-30 pb-30 ">
                <a target="_blank" class="d-flex justify-content-center" href="<?php echo e(url('public')); ?>/tailieu/03_SSIS_WEB_PARENT_Bus schedule.pdf" >
                    <div class="early-year col-md-8 col-12">
                        <img width="65%" src="<?php echo e(asset('public/')); ?>/images/Bus-schedule.png" id="EYTimetable">
                    </div>
                </a>         
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\resources\views/frontEnd/home/layout/bus_schedule.blade.php ENDPATH**/ ?>