
<?php $__env->startSection('main_content'); ?>

    <!--PHAN DUOI NAY LA CUA PHU-->
    <!--CSS-->
    <?php $__env->startPush('css'); ?>
        <link href="/public/frontend/css/lifeSchool.css" rel="stylesheet">
        <link href="/public/css/style2.css" rel="stylesheet">
    <?php $__env->stopPush(); ?>
    <!--END CSS-->

    <?php if($cate_post): ?>
        <img width="100%" src="<?php echo e(asset('public/')); ?>/images/banner/banner_03.jpg" alt="">

        <?php if(strcasecmp($cate_name, 'welcome') == 0): ?>
            <div
                style="background-color: rgba(233, 238, 251, 0.746); width: 80%; margin: 50px auto; text-align: center; padding: 10px;">
                <h4 style="color: rgb(16, 114, 114); font-size: 22px">Welcome from the Head of School</h4>
            </div>
            <div class="d-flex justify-content-center">
                <img class="image" src="../../../<?php echo e($cate_post->image); ?>" alt="">
            </div>
        <?php endif; ?>

        <div class="paragraph mb-4">
            <div class="editPostBody container" style="overflow-x:scroll;width: 100%; margin: auto">
                <p><?php echo $cate_post->news_body; ?></p>
            </div>
        </div>
    <?php else: ?>
        <div class="banner">
            <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner/banner_03.jpg" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>admissions process</h3>
                </div>
            </div>
        </div>
        <div class="adm-process">

        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/renderCatePost.blade.php ENDPATH**/ ?>