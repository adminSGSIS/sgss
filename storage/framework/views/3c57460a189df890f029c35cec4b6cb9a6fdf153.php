
<?php $__env->startSection('main_content'); ?>
    <?php $__env->startPush('css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
        <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css" />
    <?php $__env->stopPush(); ?>

    <div class="banner">
        <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>BUS SCHEDULE</h3>
            </div>
        </div>
    </div>
    <div class="main_content">
        <div class="background-content">
            <img class="banner" width="100%" src="<?php echo e(url('public')); ?>//images/BG_03.jpeg" alt="">
            <a target="_blank" href="<?php echo e(url('public')); ?>/tailieu/03_SSIS_WEB_PARENT_Bus schedule.pdf" >
                <div class="early-year">
                    <img width="45%" src="<?php echo e(asset('public/')); ?>/images/Bus-schedule.png" id="EYTimetable">
                </div>
            </a>
            
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/layout/bus_schedule.blade.php ENDPATH**/ ?>