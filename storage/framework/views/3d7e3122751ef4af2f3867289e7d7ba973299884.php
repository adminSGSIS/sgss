
<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 up_breadcrumb white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1><?php echo app('translator')->get('lang.staff_attendance'); ?></h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                <a href="#"><?php echo app('translator')->get('lang.staff_attendance'); ?></a>
                <a href="#"><?php echo app('translator')->get('lang.staff_attendance'); ?> <?php echo app('translator')->get('lang.import'); ?></a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-6">
                <div class="main-title">
                    <h3><?php echo app('translator')->get('lang.select_criteria'); ?></h3>
                </div>
            </div>
            
        </div>
        <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'staff-attendance-store',
                        'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form'])); ?>

        <div class="row">
            <div class="col-lg-12">
                <?php if(session()->has('message-success')): ?>
                  <div class="alert alert-success">
                      <?php echo e(session()->get('message-success')); ?>

                  </div>
                <?php elseif(session()->has('message-danger')): ?>
                  <div class="alert alert-danger">
                      <?php echo e(session()->get('message-danger')); ?>

                  </div>
                <?php endif; ?>
                <div class="white-box">
                    <div class="">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <div class="box-body">      
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>


                        <input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>">
                        <div class="row mb-40 mt-30">
                            <div class="col-lg-6 mt-30-md">
                                    <div class="row no-gutters input-right-icon">
                                        <div class="col">
                                            <div class="input-effect">
                                                <input class="primary-input date form-control read-only-input has-content" id="startDate" type="text" name="attendance_date" autocomplete="off" value="<?php echo e(date('m/d/Y')); ?>">
                                                <label for="startDate"> <?php echo app('translator')->get('Attendance'); ?> <?php echo app('translator')->get('lang.date'); ?> *</label>
                                                <span class="focus-border"></span>
                                             </div>
                                        </div>
                                        <div class="col-auto">
                                            <button class="" type="button">
                                                <i class="ti-calendar" id="start-date-icon"></i>
                                            </button>
                                        </div>
                                    </div>
                                    
                                </div>
                            <div class="col-lg-6">
                                <div class="row no-gutters input-right-icon">
                                    <div class="col">
                                        <div class="input-effect">
                                            <input class="primary-input form-control <?php echo e($errors->has('file') ? ' is-invalid' : ''); ?>" type="text" id="placeholderPhoto" placeholder="Excel file (xlsx, csv) *"
                                                readonly>
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('file')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('file')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <button class="primary-btn-small-input" type="button">
                                            <label class="primary-btn small fix-gr-bg" for="photo"><?php echo app('translator')->get('lang.browse'); ?></label>
                                            <input type="file" class="d-none" name="file" id="photo">
                                        </button>
                                    </div>
                                </div>
                            </div>
                                
                        </div>

                        <div class="row mt-40">
                            <div class="col-lg-12 text-center">
                                <button class="primary-btn fix-gr-bg">
                                    <span class="ti-check"></span>
                                    <?php echo app('translator')->get('lang.import'); ?> <?php echo app('translator')->get('lang.attendance'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo e(Form::close()); ?>

    </div>
    <br><br>
    <table id="table_id" class="display school-table" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Import Date</th>
                <th>Attendance date</th>
                <th>Import by</th>
                <th>Download (.xlsx)</th>
                <th>Option</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $histories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($history->import_date); ?></td>
                <td><?php echo e($history->import_for_date); ?></td>
                <td><?php echo e($history->import_by); ?></td>
                <td><a href="<?php echo e(url('')); ?>/<?php echo e($history->url); ?>" download="" target="_blank">Download</a></td>
                <td>
                    <div class="dropdown">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            <?php echo app('translator')->get('lang.select'); ?>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <?php if(in_array(297, App\GlobalVariable::GlobarModuleLinks()) || Auth::user()->role_id == 1  ): ?>
                              <a class="dropdown-item" href="<?php echo e(url('delete-imported-data/'.$history->id)); ?>">delete</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/PayRoll\Resources/views/staff_attendance_import.blade.php ENDPATH**/ ?>