<script type="text/javascript" src="<?php echo e(asset('public/js/qrcode.js')); ?>"></script>
<?php $__env->startSection('css'); ?>
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                    <?php if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16): ?>
                    <a href="<?php echo e(url('devices/released')); ?>">Released devices</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <?php if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16): ?>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-title">
                                <h3 class="mb-30">
                                    <?php if(isset($editData)): ?>
                                        <?php echo app('translator')->get('lang.update'); ?>
                                    <?php else: ?>
                                        <?php echo app('translator')->get('lang.add'); ?>
                                    <?php endif; ?>
                                    Device
                                </h3>
                            </div>
                        
                            <?php if(isset($editData)): ?>
                            <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices/edit/'.$editData->id,
                            'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                            <?php else: ?>
                                <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'devices',
                                'method' => 'POST', 'enctype' => 'multipart/form-data'])); ?>

                            <?php endif; ?>
                            <input type="hidden" name="school_id" value="<?php echo e(auth()->user()->school_id); ?>">
                            <div class="white-box">
                                <div class="add-visitor">
                                    <div class="row">
                                        <div class="col-md-6">
                                            
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control"
                                                        type="text" name="device_name" autocomplete="off" value="<?php echo e(isset($editData)? $editData->device_name : old('device_name')); ?>" required>
                                                        <label>Device Name<span></span> </label>
                                                        <span class="focus-border textarea"></span>
                                                    </div>
                                                </div>
                                                
                                                <?php if(!isset($editData)): ?>
                                                    <div class="col-lg-12 mt-3">
                                                        <div class="input-effect">
                                                            <input class="primary-input form-control"
                                                            type="number" min="1" name="quantity" autocomplete="off" value="<?php echo e(isset($editData)? $editData->quantity : old('quantity')); ?>" id="quantity" required>
                                                            <label>Quantity<span></span> </label>
                                                            <span class="focus-border textarea"></span>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                                
                                                
                                                
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-6">
                                        
                                            <div class="row no-gutters input-right-icon">
                                                <div class="col">
                                                    <div class="row no-gutters input-right-icon">
                                                        <div class="col">
                                                            <div class="input-effect">
                                                                <input class="primary-input" type="text" id="placeholderFileOneName"
                                                                    placeholder="CHOOSE DEVICE TYPE"
                                                                    readonly
                                                                >
                                                                <span class="focus-border"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <a class="deleteUrl" data-modal-size="modal-md" title="Choose Device Type" href="/devices/type">
                                                                <button class="primary-btn-small-input" type="button">
                                                                    <label class="primary-btn small fix-gr-bg"
                                                                        ><?php echo app('translator')->get('lang.browse'); ?></label>
                                                                    <input type="text" class="d-none">
                                                                </button>
                                                            </a>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-lg-12">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control has-content"
                                                        type="text" name="device_type" id="device_type" autocomplete="off" value="<?php echo e(isset($editData)? Modules\Devices\Entities\SmDeviceType::find($editData->device_type)->title : old('device_type')); ?>  " id="device_type" readonly>
                                                        <label>Device Type<span></span> </label>
                                                        <span class="focus-border textarea"></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="mt-4 mb-5">
                                        <div class="input-effect">
                                            <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('role_id') ? ' is-invalid' : ''); ?>" name="role_id">
                                                <option data-display="Role" value="">Select</option>
                                                <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option data-display="<?php echo e($item->name); ?>" value="<?php echo e($item->id); ?>" <?php if(isset($editData)): ?> <?php if($editData->role_id == $item->id): ?> selected <?php endif; ?> <?php elseif(old('role_id') == $item->id): ?> selected <?php endif; ?>><?php echo e($item->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <span class="focus-border"></span>
                                            <?php if($errors->has('role_id')): ?>
                                            <span class="invalid-feedback invalid-select" role="alert">
                                                <strong><?php echo e($errors->first('role_id')); ?></strong>
                                            </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="input-effect mt-3 mb-3">
                                            <input class="primary-input form-control"
                                            type="text" name="description" autocomplete="off" value="<?php echo e(isset($editData)? $editData->description : old('description')); ?>" id="description">
                                            <label>Description<span></span> </label>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                    <?php 
                                        $tooltip = "";
                                        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16){
                                            $tooltip = "";
                                        }else{
                                            $tooltip = "You have no permission to add";
                                            if(isset($editData)){
                                                $tooltip = "You have no permission to update";
                                            } 
                                        }
                                    ?>
                                    <div class="row mt-40">
                                        <div class="col-lg-12 text-center">
                                            <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="<?php echo e($tooltip); ?>">
                                                <span class="ti-check"></span>
                                                <?php if(isset($editData)): ?>
                                                    <?php echo app('translator')->get('lang.update'); ?>
                                                <?php else: ?>
                                                    <?php echo app('translator')->get('lang.save'); ?>
                                                <?php endif; ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            
                            <?php echo e(Form::close()); ?>

                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Devices List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            <?php if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                            <div class="alert alert-success">
                                                <?php echo e(session()->get('message-success-delete')); ?>

                                            </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                            <div class="alert alert-danger">
                                                <?php echo e(session()->get('message-danger-delete')); ?>

                                            </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr class="centerTh">
                                <th>Device Code</th>
                                <th>Device name</th>
                                <th>Device type</th>
                                <th>role</th>
                                <th>action</th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php $__currentLoopData = $devices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="centerTd">
                                        <td>
                                            <img src="<?php echo e(asset($value->qr_code_path)); ?>" alt="">
                                        </td>
                                        <td><?php echo e($value->device_name); ?></td>
                                        <td><?php echo e($value->deviceType->title); ?></td>
                                        <td><?php echo e($value->role->name); ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <?php echo app('translator')->get('lang.select'); ?>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="<?php echo e(url('devices/borrow/'.$value->id)); ?>" class="dropdown-item">loan</a>
                                                    <a href="<?php echo e(url('devices/view/'.$value->id)); ?>" class="dropdown-item small fix-gr-bg modalLink" title="Device Details" data-modal-size="modal-md">view</a>
                                                    <?php if(Auth::user()->role_id == 1 || Auth::user()->role_id == 16): ?>
                                                        <a class="dropdown-item" href="<?php echo e(url('devices/repair/'.$value->id)); ?>">Repair</a>
                                                        <a class="dropdown-item" href="<?php echo e(url('devices/edit/'.$value->id)); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                                        <a href="<?php echo e(url('devices/release-view/'.$value->id)); ?>" class="dropdown-item small fix-gr-bg modalLink" title="Release device" data-modal-size="modal-md">
                                                            release
                                                        </a>
                                                    <?php endif; ?>
                                                    
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/Devices/Resources/views/index.blade.php ENDPATH**/ ?>