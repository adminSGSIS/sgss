<input type="hidden" name="url" id="url" value="<?php echo e(URL::to('/')); ?>">

<div class="row mt-15 justify-content-start">
  <div class="col">
    More Information, click here <a href="/admissions" target="_blank" rel="noopener noreferrer">Fees & Tuition</a>
  </div>
</div>

<div class="row mt-15 justify-content-start">
  <div class="col-3">
    <div class="input-effect sm2_mb_20 md_mb_20">
      <select class="w-100 bb form-control" name="session" id="academic_year_form">
        <option data-display="<?php echo app('translator')->get('lang.academic_year_form'); ?> *" value=""><?php echo app('translator')->get('lang.academic_year'); ?></option>
        <?php $__currentLoopData = $sessions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $session): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($session->id); ?>" <?php echo e(old('session') == $session->id? 'selected': ''); ?>><?php echo e($session->year); ?>[<?php echo e($session->title); ?>]</option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </select>
      <span class="focus-border"></span>
      <?php if($errors->has('session')): ?>
      <span class="invalid-feedback invalid-select" role="alert">
        <strong><?php echo e($errors->first('session')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

  <div class="col-3">
    <div class="input-effect sm2_mb_20 md_mb_20" id="class-div">
      <select class=" w-100 bb form-control" name="class" id="classSelectStudent_enrolment">
        <option data-display="<?php echo app('translator')->get('lang.class'); ?> *" value=""><?php echo app('translator')->get('lang.class'); ?></option>

      </select>
      <span class="focus-border"></span>
      <?php if($errors->has('class')): ?>
      <span class="invalid-feedback invalid-select" role="alert">
        <strong><?php echo e($errors->first('class')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

  <?php if(!empty(old('class'))): ?>

  <?php
  $old_sections = DB::table('sm_class_sections')->where('class_id', '=', old('class'))
  ->join('sm_sections','sm_class_sections.section_id','=','sm_sections.id')
  ->get();
  ?>

  <div class="col-3">
    <div class="input-effect sm2_mb_20 md_mb_20" id="sectionStudentDiv_enrolment">
      <select class=" w-100 bb form-control" name="section" id="sectionSelectStudent_enrolment">
        <option data-display="<?php echo app('translator')->get('lang.section'); ?> *" value=""><?php echo app('translator')->get('lang.section'); ?></option>
        <?php $__currentLoopData = $old_sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $old_section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($old_section->id); ?>" <?php echo e(old('section')==$old_section->id ? 'selected' : ''); ?>>
          <?php echo e($old_section->section_name); ?></option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </select>
      <span class="focus-border"></span>
      <?php if($errors->has('section')): ?>
      <span class="invalid-feedback invalid-select" role="alert">
        <strong><?php echo e($errors->first('section')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
  <?php else: ?>

  <div class="col-3">
    <div class="input-effect sm2_mb_20 md_mb_20" id="sectionStudentDiv_enrolment">
      <select class=" w-100 bb form-control" name="section" id="sectionSelectStudent_enrolment">
        <option data-display="<?php echo app('translator')->get('lang.section'); ?> *" value=""><?php echo app('translator')->get('lang.section'); ?></option>
      </select>
      <span class="focus-border"></span>
      <?php if($errors->has('section')): ?>
      <span class="invalid-feedback invalid-select" role="alert">
        <strong><?php echo e($errors->first('section')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
  <?php endif; ?>
</div>

<div class="row mt-15 justify-content-start">
  <div class="col-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('price') ? ' is-invalid' : ''); ?>" type="text" name="price" value="<?php echo e(old('price')); ?>">

      <label>Price VND *</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="student_preferred_name_feedback" role="alert"></span>
      <?php if($errors->has('price')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('price')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
  <div class="col-3">
    <div class="row">
      <div class="col">
        <small style="color: #DC3545;"><?php echo e($errors->first('method')); ?></small>
      </div>
      <div class="col">
        <p class="text-uppercase fw-100 mb-10">Option</p>
        <div class="d-flex radio-btn-flex ml-40" style="flex-wrap:wrap">
          <?php $__currentLoopData = $payment_methods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="mt-10">
            <input type="radio" name="method" id="<?php echo e($i->id); ?>" value="<?php echo e($i->method); ?>" class="common-radio relationButton" <?php if(old('method')==$i->method ): ?> checked <?php endif; ?>>
            <label for="<?php echo e($i->id); ?>"><?php echo e($i->method); ?></label>
          </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/forms/step7.blade.php ENDPATH**/ ?>