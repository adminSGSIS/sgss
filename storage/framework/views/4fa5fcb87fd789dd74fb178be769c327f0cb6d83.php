
<?php $__env->startSection('main_content'); ?>
    <?php $__env->startPush('css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
        <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/css')); ?>/lightbox.css">
    <?php $__env->stopPush(); ?>

    <div class="banner">
        <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>quality cycle</h3>
            </div>
        </div>
    </div>
    <div class="main_content" style="background:#fafaf4">
        <div class="container d-flex justify-content-center">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="carousel-item <?php if($loop->first): ?> active <?php endif; ?>">
                  <a href="/public/uploads/category/<?php echo e($image->path); ?>" data-lightbox="2">
                        <img class="img-slider video_1aa w-100 fit-img" src="/public/uploads/category/<?php echo e($image->path); ?>">
                  </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick="event.preventDefault();">
                    <span class="fa fa-chevron-left"  aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" onclick="event.preventDefault();">
            <span class="fa fa-chevron-right"  aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
            
        </div>
        <div class="container">
              <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/CHILDREN-01.png" alt=""> 
        </div>
       
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/layout/events.blade.php ENDPATH**/ ?>