
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
   <div class="banner">
    	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>our vision</h3>
            </div>
    	</div>
    	
    </div>
    <div class="main-ourschool c-color">
        <div class="content-overview d-flex justify-content-center">
            <p class="col-lg-5 col-12">All children are helped to become globally-competent, 
                well-rounded, lifelong learners.
            </p>
        </div>
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>our mission</button>
    </div>
    <div class="main-ourschool c-color">
        <div class="content-overview d-flex justify-content-center">
            <p class="col-lg-5 col-12">We provide motivating and engaging learning experiences,
                within a safe and nurturing environment, that enable every child to experience daily success in learning.
            </p>
        </div>
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    
    <div class="title c-color" style="padding-bottom:5%;">
        <button>philosophy</button>
    </div>
    <section class="py-5 d-flex justify-content-center c-color">
        <img width="55%" src="<?php echo e(url('public/images/our belief and philosophy-01.png')); ?>">
    </section>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>school name</button>
    </div>
    <div class="title c-color">
        <h2 class="mb-0">saigon star </br>international school</h2>
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>acronym</button>
    </div>
    <div class="title c-color">
        <h2 class="mb-0">SGSIS</h2>
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>slogan</button>
    </div>
    <div class="title c-color">
        <h4 class="mb-0">Inspired Learners.<br> Global Thinkers.</h4>
        
    </div>
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>values</button>
    </div>
    <div class="c-color">
        <div class="container d-flex justify-content-center value">
        <img src="/public/images/value.png" alt="">
    </div>
    </div>
    
    <div class="angle-down c-color">
        <i class="fa fa-angle-double-down"></i>
    </div>
    <div class="title c-color">
        <button>History</button>
    </div>
    <div class="c-color">
        <br>
        <div class="container d-flex justify-content-center value ">
        <img src="/public/images/history.png" alt="">
    </div>
    </div>
    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/light_ourSchoolOverview.blade.php ENDPATH**/ ?>