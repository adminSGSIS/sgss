<?php $__env->startSection('css'); ?>
    <style>
        .pdf {
            display: none;
        }
        body {
            -webkit-print-color-adjust: exact !important;
        }
        .box-title {
            background-color: #008fc5 !important;
        }
        .title {
            color: white !important;
        }
        .border {
            align-items: center;
            border: 1px solid #ced4da;
            background: white;
            height: calc(2.875rem + 2px);
            padding: .5rem 1rem;
            border-radius: .3rem;
        }
        .student-photo {
            margin-top: -105px;
        }
        .student-photo img {
            object-fit: cover;
        }
        .pdf-footer {
            border-top: 1px solid #008fc5;
            padding-top: 10px;
        }
        .last-page {
            height: 550px;
        }
        .last-page > div{
            background-image: url('/public/images/pic5.jpg');
            background-repeat: no-repeat;
            background-size: 100% 500px;
            height: 100%;
        }
        .last-page > div > img {
            width: 253px;
            margin-top: -14px;
        }
        .our-school {
            background: #008fc5;
            width: 70%;
            margin: auto;
            border-radius: 17px;
            margin-top: -240px;
            padding: 10px;
        }
        .our-school > h1 {
            color: white;
            font-size: 70px;
            text-align: center;
        }
        @page  { 
            margin-bottom: 0;
            margin-top: 0; 
        }
        @media  print{
            .student-details, .sms-breadcrumb, #message_box, footer, .print {
                display: none !important;
            }
            .box-title{
                display: block !important;
            }
            .pdf {
                display: block;
            }
        }
    </style>
<?php $__env->stopSection(); ?>
<div class="pdf">
    <img src="/public/header_pdf.jpeg" width="100%">
    <div style="height: 1500px; color: #00548a">
        <div class="row justify-content-center align-items-center" style="height: 87%">
            <img src="/public/uploads/settings/f88583d5e43b7ce35be972def7d0219d.png" width="400">
        </div>
        <div class="d-flex align-items-center ml-5 pt-3" style="border-top: 1px solid #008fc5;">
            <i class="fa fa-map-marker ml-3"></i>
            <span class="ml-2">Su Hy Nhan Street, Residential Area No. 5 Thanh My Loi Ward,Thu Duc City</span>
        </div>
        <div class="d-flex align-items-center justify-content-between ml-5">
            <div class="col-3">
                <i class="fa fa-globe" aria-hidden="true"></i>
                <span>www.sgstar.edu.vn</span>
            </div>
            <div class="col-3">
                <i class="fa fa-envelope-square" aria-hidden="true"></i>
                <span>info@sgstar.edu.vn</span>
            </div>
            <div class="col-3">
                <i class="fa fa-headphones" aria-hidden="true"></i>
                <span>028374232222</span>
            </div>
            <div class="col-3">
                <i class="fa fa-phone-square" aria-hidden="true"></i>
                <span>0888006996</span>
            </div>
        </div>
    </div>

    <img src="/public/header_pdf.jpeg" width="100%">

    <div style="height: 1350px">
        <section class="sms-breadcrumb mb-40 white-box box-title">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">STUDENT INFORMATION</h1>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-8 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>First name : </span>
                        <span><?php echo e($student_detail->first_name); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-8 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Middle name : </span>
                        <span><?php echo e($student_detail->middle_name); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-8 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Last name : </span>
                        <span><?php echo e($student_detail->last_name); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="student-photo text-center">
                    <img src="#" width="132" height="171">
                </div>
            </div>

            <div class="col-12 mb-3 mt-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Nickname : </span>
                        <span><?php echo e($student_detail->nickname); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Date of Birth : </span>
                        <span><?php echo e($student_detail->date_of_birth); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="border d-flex justify-content-between">
                    <span>Gender</span>
                    <?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" <?php if($student_detail->gender_id == $item->id): ?> checked <?php endif; ?>>
                        <label class="custom-control-label"><?php echo e($item->base_setup_name); ?></label>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            
            <div class="col-4 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Nationality 1 : </span>
                        <span><?php echo e($student_detail->nationality_1); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-4 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Nationality 2 : </span>
                        <span><?php echo e($student_detail->nationality_2); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-4 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Enrol in : </span>
                        <span><?php echo e($student_detail->enroll_in); ?></span>
                    </div>
                </div>
            </div>
            

            <div class="col-12 mt-4">
                <h3>STUDENT INTERESTS</h3>
            </div>

            <div class="col-12 mt-2">
                <div class="border" style="height: auto">
                    <div class="d-flex">
                        <div class="col-12">
                            <span>Student particular interests? : </span>
                            <span><?php echo e($student_detail->admission->particular_interests ?? ''); ?></span>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <span>What talents or abilities does your child have and wish to develop further? (e.g.music, athletics)</span>
                    </div>
                    <div class="col-12 mt-2">
                        <span><?php echo e($student_detail->admission->talents_or_abilities ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-4">
                <h3>LANGUAGE</h3>
            </div>

            <div class="col-12 mt-2">
                <div class="border" style="height: auto">
                    <div class="d-flex">
                        <div class="col-6">
                            <span>First Language : </span>
                            <span><?php echo e($student_detail->first_language); ?></span>
                        </div>
                        <div class="col-6">
                            <span>Second Language : </span>
                            <span><?php echo e($student_detail->second_language); ?></span>
                        </div>
                    </div>
                    <div class="d-flex mt-3">
                        <div class="col-12">
                            <span>Other spoken language : </span>
                            <span><?php echo e($student_detail->other_language); ?></span>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between col-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->other_language_proficient == 'Beginer'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Beginer</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->other_language_proficient == 'Gaining Confidence'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Gaining Confidence</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->other_language_proficient == 'Confidence'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Confidence</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->other_language_proficient == 'Fluent'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Fluent</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->other_language_proficient == 'Native'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Native</label>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <span>If the student is non native English speaker, please check the student's appropriate level of proficient in English</span>
                    </div>
                    <div class="d-flex justify-content-between col-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->english_proficient == 'Beginer'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Beginer</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->english_proficient == 'Gaining Confidence'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Gaining Confidence</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->english_proficient == 'Confidence'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Confidence</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->english_proficient == 'Fluent'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Fluent</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->english_proficient == 'Native'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Native</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-4">
                <h3>TRIAL DATE</h3>
            </div>

            <div class="col-6">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>From : </span>
                        <span><?php echo e($student_detail->admission->trial_from_date ?? ''); ?></span>
                    </div>
                    <div class="col-6">
                        <span>To : </span>
                        <span><?php echo e($student_detail->admission->trial_to_date ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Class : </span>
                        <span><?php echo e($student_detail->admission->class->class_name ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Teacher : </span>
                        <span><?php echo e($student_detail->admission->teacher->full_name ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-4">
                <h3>SCHOOL HISTORY</h3>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>Current School Name : </span>
                        <span><?php echo e($student_detail->admission->school_history_name ?? ''); ?></span>
                    </div>
                    <div class="col-6">
                        <span>City, Country : </span>
                        <span><?php echo e($student_detail->admission->school_history_city ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Date from : </span>
                        <span><?php echo e($student_detail->admission->school_history_from_date ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Date to : </span>
                        <span><?php echo e($student_detail->admission->school_history_to_date ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Language of instruction : </span>
                        <span><?php echo e($student_detail->admission->school_history_language_instruction ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Reason for withdrawal : </span>
                        <span><?php echo e($student_detail->admission->school_history_withdrawal_reason ?? ''); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pdf-footer d-flex justify-content-start">
        1 &nbsp; | &nbsp; SGSS APPLICATION FORM 
    </div>

    <p style="margin-top: 300px; color: white">space</p>

    <img src="/public/header_pdf.jpeg" width="100%">

    <div style="height: 1350px">
        <section class="sms-breadcrumb mb-40 white-box box-title">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">HEALTH INFORMATION</h1>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 mb-3">
                <div class="border" style="height: auto">
                    <div class="col-12 mt-2">
                        <span>Allergies to medications and/or foods : </span>
                    </div>
                    <div class="col-12 mt-3">
                        <span><?php echo e($student_detail->admission->health_information_1 ?? ''); ?></span>
                    </div>

                    <div class="col-12 mt-4">
                        <span>Medications taken on a regular basis : </span>
                    </div>
                    <div class="col-12 mt-3">
                        <span><?php echo e($student_detail->admission->health_information_2 ?? ''); ?></span>
                    </div>

                    <div class="col-12 mt-4">
                        <span>If your child has any other medical condition which may affect his/her daily routine, please describe it here : </span>
                    </div>
                    <div class="col-12 mt-3">
                        <span><?php echo e($student_detail->admission->health_information_3 ?? ''); ?></span>
                    </div>

                    <div class="col-12 mt-5">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if(($student_detail->admission->health_information_4 ?? '') == 'yes'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">We consent to the school nurse administering over-the-counter 
                                medications for symptom relief of minor illnesses</label>
                        </div>
                    </div>
                    
                    <div class="col-12 mt-3 mb-2">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if(($student_detail->admission->health_information_5 ?? '') == 'yes'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">We agree to provide any professional reports relevant to my child's education 
                                history</label>
                        </div>
                    </div>

                    <div class="col-12">
                        <p class="text-right mb-5">Signature of Parent/Guardian</p>
                    </div>
                </div>
            </div>
        </div>

        <section class="sms-breadcrumb mb-40 white-box box-title mt-5">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">ADDITIONAL NEEDS</h1>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 mb-3">
                <div class="border" style="height: auto">
                    <div class="col-12">
                        <span>PLease describe any other Special educational need that 
                            Saigon Star should be aware of in order to make your child's learning is positive experience : </span>
                    </div>
                    <div class="col-12">
                        <span><?php echo e($student_detail->admission->additional_needs ?? ''); ?></span>
                    </div>
                </div>
            </div>
        </div>

        <section class="sms-breadcrumb mb-40 white-box box-title mt-5">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">SCHOOL BUS REQUESTED</h1>
                </div>
            </div>
        </section>

        <div class="row" style="height: 500px">
            <div class="col-12">
                <div class="border" style="height: auto">
                    <?php $__currentLoopData = $bus_service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-12 mt-3 mb-2">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if(($student_detail->admission->bus_service_request ?? '') == $item->id): ?> checked <?php endif; ?>>
                            <label class="custom-control-label"><?php echo e($item->title); ?></label>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="pdf-footer d-flex justify-content-end">
        SGSS APPLICATION FORM &nbsp; | &nbsp; 2
    </div>

    <p style="margin-top: 330px; color: white">space</p>

    <img src="/public/header_pdf.jpeg" width="100%">

    <div style="height: 1350px">
        <section class="sms-breadcrumb white-box box-title" >
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">LUNCH REQUESTED</h1>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12">
                <div class="border d-flex justify-content-between">
                    <span class="col-8">Any special request for lunch ?</span>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" 
                        <?php if(($student_detail->admission->lunch_request ?? '') != null): ?> checked <?php endif; ?>>
                        <label class="custom-control-label">Yes</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" 
                        <?php if(($student_detail->admission->lunch_request ?? '') == null): ?> checked <?php endif; ?>>
                        <label class="custom-control-label">No</label>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span><?php echo e($student_detail->admission->lunch_request ?? ''); ?></span>
                    </div>
                </div>
            </div>
        </div>

        <section class="sms-breadcrumb mb-40 white-box box-title mt-4">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">SUPPORT</h1>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>EAL support : </span>
                        <span><?php echo e($student_detail->admission->eal_support ?? ''); ?> hours/week</span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>SEN support : </span>
                        <span><?php echo e($student_detail->admission->sen_support ?? ''); ?> hours/week</span>
                    </div>
                </div>
            </div>
        </div>

        <section class="sms-breadcrumb mb-40 white-box box-title mt-4">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">PAYMENT BY</h1>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Contact name : </span>
                        <span><?php echo e($student_detail->admission->payer_name ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Company Name : </span>
                        <span><?php echo e($student_detail->admission->payer_company ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Address : </span>
                        <span><?php echo e($student_detail->admission->payer_company ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>Email : </span>
                        <span><?php echo e($student_detail->admission->payer_email ?? ''); ?></span>
                    </div>
                    <div class="col-6">
                        <span>Phone number : </span>
                        <span><?php echo e($student_detail->admission->payer_mobile ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex justify-content-between">
                    <span class="col-8">Do you require a VAT invoice ?</span>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" 
                        <?php if(($student_detail->admission->payer_require_vat_invoice ?? '') == 'yes'): ?> checked <?php endif; ?>>
                        <label class="custom-control-label">Yes</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" 
                        <?php if(($student_detail->admission->payer_require_vat_invoice ?? '') == 'no'): ?> checked <?php endif; ?>>
                        <label class="custom-control-label">No</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pdf-footer d-flex justify-content-start">
        3 &nbsp; | &nbsp; SGSS APPLICATION FORM 
    </div>

    <p style="margin-top: 330px; color: white">space</p>

    <img src="/public/header_pdf.jpeg" width="100%">

    <div style="height: 1350px">
        <section class="sms-breadcrumb mb-40 white-box box-title mt-4">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">FAMILY INFORMATION</h1>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-12 mb-3">
                <div class="border d-flex justify-content-between">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" checked >
                        <label class="custom-control-label">FATHER</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <label class="custom-control-label">GUARDIAN</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <label class="custom-control-label">PRIMARY CONTACT</label>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Full Name : </span>
                        <span><?php echo e($student_detail->parent->fathers_name ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Nationality : </span>
                        <span><?php echo e($student_detail->parent->fathers_nationality ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Occupation : </span>
                        <span><?php echo e($student_detail->parent->fathers_occupation ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Company : </span>
                        <span><?php echo e($student_detail->parent->fathers_company ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Work address : </span>
                        <span><?php echo e($student_detail->parent->fathers_work_address ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>Personal Email : </span>
                        <span><?php echo e($student_detail->parent->fathers_email ?? ''); ?></span>
                    </div>
                    <div class="col-6">
                        <span>Work Email : </span>
                        <span><?php echo e($student_detail->parent->fathers_email ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>First Language : </span>
                        <span><?php echo e($student_detail->parent->fathers_first_language ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border" style="height: auto">
                    <span class="col-12">English level : </span>
                    <div class="d-flex justify-content-between col-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->fathers_language_level == 'Beginer'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Beginer</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->fathers_language_level == 'Gaining Confidence'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Gaining Confidence</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->fathers_language_level == 'Confidence'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Confidence</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->fathers_language_level == 'Fluent'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Fluent</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->fathers_language_level == 'Native'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Native</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 50px;">
            <div class="col-12 mb-3">
                <div class="border d-flex justify-content-between">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" checked >
                        <label class="custom-control-label">MOTHER</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <label class="custom-control-label">GUARDIAN</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <label class="custom-control-label">PRIMARY CONTACT</label>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Full Name : </span>
                        <span><?php echo e($student_detail->parent->mothers_name ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Nationality : </span>
                        <span><?php echo e($student_detail->parent->mothers_nationality ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Occupation : </span>
                        <span><?php echo e($student_detail->parent->mothers_occupation ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-6 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Company : </span>
                        <span><?php echo e($student_detail->parent->mothers_company ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Work address : </span>
                        <span><?php echo e($student_detail->parent->mothers_work_address ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>Personal Email : </span>
                        <span><?php echo e($student_detail->parent->mothers_email ?? ''); ?></span>
                    </div>
                    <div class="col-6">
                        <span>Work Email : </span>
                        <span><?php echo e($student_detail->parent->mothers_email ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>First Language : </span>
                        <span><?php echo e($student_detail->parent->mothers_first_language ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border" style="height: auto">
                    <span class="col-12">English level : </span>
                    <div class="d-flex justify-content-between col-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->mothers_language_level == 'Beginer'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Beginer</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->mothers_language_level == 'Gaining Confidence'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Gaining Confidence</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->mothers_language_level == 'Confidence'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Confidence</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->mothers_language_level == 'Fluent'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Fluent</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input"
                            <?php if($student_detail->mothers_language_level == 'Native'): ?> checked <?php endif; ?>>
                            <label class="custom-control-label">Native</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="pdf-footer d-flex justify-content-end">
        SGSS APPLICATION FORM &nbsp; | &nbsp; 4
    </div>

    <p style="margin-top: 330px; color: white">space</p>

    <img src="/public/header_pdf.jpeg" width="100%">

    <div style="height: 1350px">
        <div class="col-12 mt-4">
            <h3>EMERGENCY CONTACT</h3>
        </div>

        <div class="row mt-3">
            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>First Name : </span>
                        <span><?php echo e($student_detail->admission->emergency_contact_name ?? ''); ?></span>
                    </div>
                    <div class="col-6">
                        <span>Family Name : </span>
                        <span><?php echo e($student_detail->admission->emergency_contact_family_name ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Relationship to Child : </span>
                        <span><?php echo e($student_detail->admission->emergency_relationship ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>Phone number 1 : </span>
                        <span><?php echo e($student_detail->admission->emergency_contact_mobile_1 ?? ''); ?></span>
                    </div>
                    <div class="col-6">
                        <span>Phone number 2 : </span>
                        <span><?php echo e($student_detail->admission->emergency_contact_mobile_2 ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Contact address : </span>
                        <span><?php echo e($student_detail->admission->emergency_contact_address ?? ''); ?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 mt-4">
            <h3>SIBLINGS</h3>
        </div>

        <div class="row mt-3">
            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>Name of first child : </span>
                        <span><?php echo e($student_detail->admission->name_of_child_1 ?? ''); ?></span>
                    </div>
                    <div class="col-3">
                        <span>Age : </span>
                        <span><?php echo e($student_detail->admission->age_of_child_1 ?? ''); ?></span>
                    </div>
                    <div class="col-3">
                        <span>Grade : </span>
                        <span><?php echo e($student_detail->admission->grade_of_child_1 ?? ''); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>Name of second child : </span>
                        <span><?php echo e($student_detail->admission->name_of_child_2 ?? ''); ?></span>
                    </div>
                    <div class="col-3">
                        <span>Age : </span>
                        <span><?php echo e($student_detail->admission->age_of_child_2 ?? ''); ?></span>
                    </div>
                    <div class="col-3">
                        <span>Grade : </span>
                        <span><?php echo e($student_detail->admission->grade_of_child_2 ?? ''); ?></span>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3" style="height: 750px">
                <div class="border d-flex">
                    <div class="col-6">
                        <span>Name of third child : </span>
                        <span><?php echo e($student_detail->admission->name_of_child_3 ?? ''); ?></span>
                    </div>
                    <div class="col-3">
                        <span>Age : </span>
                        <span><?php echo e($student_detail->admission->age_of_child_3 ?? ''); ?></span>
                    </div>
                    <div class="col-3">
                        <span>Grade : </span>
                        <span><?php echo e($student_detail->admission->grade_of_child_3 ?? ''); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pdf-footer d-flex justify-content-start">
        5 &nbsp; | &nbsp; SGSS APPLICATION FORM 
    </div>

    <p style="margin-top: 300px; color: white">space</p>

    <img src="/public/header_pdf.jpeg" width="100%">

    <div style="height: 1350px">
        <section class="sms-breadcrumb mb-40 white-box box-title mt-4">
            <div class="container-fluid">
                <div class="row">
                    <h1 class="col-12 text-center title">QUOTATION</h1>
                </div>
            </div>
        </section>

        <div class="col-12 mt-4">
            <h3>FEES</h3>
        </div>

        <div class="row mt-3">
            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Application fee : </span>
                        <span><?php echo e($student_detail->admission->application_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Registration fee : </span>
                        <span><?php echo e($student_detail->admission->registration_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Supplement fee : </span>
                        <span><?php echo e($student_detail->admission->supplement_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Lunch fee : </span>
                        <span><?php echo e($student_detail->admission->lunch_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Tuition fee : </span>
                        <span><?php echo e($student_detail->admission->tuition_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>SEN fee : </span>
                        <span><?php echo e($student_detail->admission->sen_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>EAL fee : </span>
                        <span><?php echo e($student_detail->admission->eal_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Bus fee : </span>
                        <span><?php echo e($student_detail->admission->bus_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Total fee : </span>
                        <span><?php echo e($student_detail->admission->total_fee ?? ''); ?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 mt-4">
            <h3>DISCOUNTS</h3>
        </div>

        <div class="row mt-3">
            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Loyalty & sibling discount : </span>
                        <span><?php echo e($student_detail->admission->loyalty_sibling_discount ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Bus discount : </span>
                        <span><?php echo e($student_detail->admission->bus_discount ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Late enrolment discount : </span>
                        <span><?php echo e($student_detail->admission->late_enrol_discount ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Lunch discount : </span>
                        <span><?php echo e($student_detail->admission->lunch_discount ?? ''); ?></span>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div class="border d-flex">
                    <div class="col-12">
                        <span>Total fee after discounts : </span>
                        <span><?php echo e($student_detail->admission->total_fee_after_discounts ?? ''); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pdf-footer d-flex justify-content-end">
        SGSS APPLICATION FORM &nbsp; | &nbsp; 6
    </div>

    <p style="margin-top: 300px; color: white">space</p>

    <img src="/public/header_pdf.jpeg" width="100%">

    

    

    <div class="row mt-5">
        <div class="col-12 mb-3">
            <div class="border d-flex">
                <div class="col-12">
                    <span>NAME OF STUDENT : </span>
                    <span><?php echo e($student_detail->full_name); ?></span>
                </div>
            </div>
        </div>

        <div class="col-12 mb-3">
            <div class="border d-flex">
                <div class="col-12">
                    <span>DATE OF BIRTH : </span>
                    <span><?php echo e($student_detail->date_of_birth); ?></span>
                </div>
            </div>
        </div>

        <div class="col-12 mb-3">
            <div class="border d-flex">
                <div class="col-12">
                    <span>CLASS : </span>
                    <span><?php echo e($student_detail->class->class_name); ?></span>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12 mb-3">
            <div class="border d-flex">
                <div class="col-12">
                    <span>NAME OF FATHER/GUARDIAN : </span>
                    <span><?php echo e($student_detail->parent->fathers_name ?? ''); ?></span>
                </div>
            </div>
        </div>

        <div class="col-12 mb-3">
            <div class="border d-flex">
                <div class="col-12">
                    <span>NAME OF MOTHER/GUARDIAN : </span>
                    <span><?php echo e($student_detail->parent->mothers_name ?? ''); ?></span>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->startSection('script'); ?>
<script>
    function exportPdf() {
        window.print();
    }
</script>
<?php $__env->stopSection(); ?><?php /**PATH D:\WWW\SGS\sgss\resources\views/backEnd/studentInformation/student_view_pdf.blade.php ENDPATH**/ ?>