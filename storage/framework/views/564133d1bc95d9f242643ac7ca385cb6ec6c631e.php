<?php $__env->startSection('css'); ?>
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/css')); ?>/purchase-proposal.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Documents</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                    <a href="<?php echo e(url('docs')); ?>">New docs</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30">
                                <?php if(isset($editData)): ?>
                                    <?php echo app('translator')->get('lang.update'); ?>
                                <?php else: ?>
                                    <?php echo app('translator')->get('lang.add'); ?>
                                <?php endif; ?>
                                Document
                            </h3>
                        </div>
                       
                        <?php if(isset($editData)): ?>
                            <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'docs/edit/'.$editData->id,
                            'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'add-income-update'])); ?>

                        <?php else: ?>
                            <?php echo e(Form::open(['class' => 'form-horizontal', 'files' => true, 'url' => 'docs',
                            'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'add-income'])); ?>

                        <?php endif; ?>
                        <input type="hidden" name="created_by" value="<?php echo e(auth()->id()); ?>">
                        <input type="hidden" name="updated_by" value="<?php echo e(auth()->id()); ?>">
                        <input type="hidden" name="school_id" value="<?php echo e(auth()->user()->school_id); ?>">
                        <input type="hidden" name="approve_status" value="0">
                        <input type="hidden" name="pin_to_top" value="0">
                        <div class="white-box">
                            <div class="add-visitor">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control"
                                                    type="text" name="title" autocomplete="off" value="<?php echo e(isset($editData)? $editData->title : old('title')); ?>" required>
                                                    <label>Title<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12 mt-3">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('stf_or_std') ? ' is-invalid' : ''); ?>" name="stf_or_std" id="stf_std">
                                                        <option data-display="Docs for staffs or students ?" value="">Select</option>
                                                        <option data-display="Staffs" value="stf" <?php if(old('stf_or_std') == 'stf'): ?> selected <?php endif; ?> <?php if(isset($editData)): ?> <?php if($editData->stf_or_std == "stf"): ?> selected <?php endif; ?> <?php endif; ?>>Staffs</option>
                                                        <option data-display="Students" value="std" <?php if(old('stf_or_std') == 'std'): ?> selected <?php endif; ?> <?php if(isset($editData)): ?> <?php if($editData->stf_or_std == "std"): ?> selected <?php endif; ?> <?php endif; ?>>Students</option>
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    <?php if($errors->has('stf_or_std')): ?>
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong><?php echo e($errors->first('stf_or_std')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            
                                        

                                            <div style="display: none" id="public" class="col-lg-12 mt-3">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('public') ? ' is-invalid' : ''); ?>" name="public">
                                                        <option data-display="Do you want to public ?" value="">Select</option>
                                                        <option data-display="Yes" value="1" <?php if(old('public') == '1'): ?> selected <?php endif; ?> <?php if(isset($editData)): ?> <?php if($editData->public == "1"): ?> selected <?php endif; ?> <?php endif; ?>>Yes</option>
                                                        <option data-display="No" value="0" <?php if(old('public') == '0'): ?> selected <?php endif; ?> <?php if(isset($editData)): ?> <?php if($editData->public == "0"): ?> selected <?php endif; ?> <?php endif; ?>>No</option>
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    <?php if($errors->has('public')): ?>
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong><?php echo e($errors->first('public')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>

                                            <div style="display: none" id="class" class="col-lg-12 mt-3">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('class') ? ' is-invalid' : ''); ?>">
                                                        <option data-display="Class" value="">Select</option>
                                                        <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option  value="<?php echo e($value->id); ?>"
                                                        <?php echo e(old('class')==$value ? 'selected' : ''); ?>

                                                        ><?php echo e($value->class_name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    <?php if($errors->has('class')): ?>
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong><?php echo e($errors->first('class')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>

                                            
                                        </div>
                                       
                                    </div>
                                    <div class="col-md-6">
                                       

                                        <div class="row no-gutters input-right-icon">
                                            <div class="col">
                                                <div class="row no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="input-effect">
                                                            <input class="primary-input" type="text" id="placeholderFileOneName" placeholder="File" readonly="">
                                                            <span class="focus-border"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="primary-btn-small-input" type="button">
                                                            <label class="primary-btn small fix-gr-bg"
                                                                   for="document_file_1"><?php echo app('translator')->get('lang.browse'); ?></label>
                                                            <input type="file" class="d-none" name="file" id="document_file_1">
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-gutters input-right-icon mt-2">
                                            <div class="col">
                                                <div class="row no-gutters input-right-icon">
                                                    <div class="col">
                                                        <div class="input-effect">
                                                            <input class="primary-input" type="text" id="placeholderFileOneName"
                                                                   placeholder="CHOOSE DOCS TYPE"
                                                                   readonly
                                                            >
                                                            <span class="focus-border"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <a class="deleteUrl" data-modal-size="modal-md" title="Choose Docs Type" href="/docs/type">
                                                            <button class="primary-btn-small-input" type="button">
                                                                <label class="primary-btn small fix-gr-bg"
                                                                       ><?php echo app('translator')->get('lang.browse'); ?></label>
                                                                <input type="text" class="d-none">
                                                            </button>
                                                        </a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mt-2">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control has-content"
                                                    type="text" name="docs_type" autocomplete="off" value="<?php echo e(isset($editData)? Modules\Docs\Entities\DocsType::find($editData->docs_type_id)->title : old('docs_type')); ?>" id="docs_type" readonly>
                                                    <label>Docs Type<span></span> </label>
                                                    <span class="focus-border textarea"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div style="display: none" id="student" class="row mt-3">
                                            <div class="col-lg-12">
                                                <div class="input-effect">
                                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('class') ? ' is-invalid' : ''); ?>" name="private_stu_id">
                                                        <option  data-display="<?php echo e(isset($editData->private_stu_id) ? App\SmStudent::find($editData->private_stu_id)->full_name : "Student"); ?>" value="">Select</option>
                                                        
                                                    </select>
                                                    <span class="focus-border"></span>
                                                    <?php if($errors->has('student')): ?>
                                                    <span class="invalid-feedback invalid-select" role="alert">
                                                        <strong><?php echo e($errors->first('student')); ?></strong>
                                                    </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div> 
                                        </div>

                                    </div>

                                </div>
                                <div class="row mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <textarea class="primary-input form-control" cols="0" rows="4" id="summernote" name="content">
                                                <?php echo e(!empty($editData)? $editData->content : old('content')); ?>

                                            </textarea>
                                            <span class="focus-border textarea"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg">
                                            <span class="ti-check"></span>
                                            <?php if(isset($editData)): ?>
                                                <?php echo app('translator')->get('lang.update'); ?>
                                            <?php else: ?>
                                                <?php echo app('translator')->get('lang.save'); ?>
                                            <?php endif; ?>
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                         
                        <?php echo e(Form::close()); ?>

                    </div>
                </div>
            </div>

            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Documents List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            <?php if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                            <div class="alert alert-success">
                                                <?php echo e(session()->get('message-success-delete')); ?>

                                            </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                            <div class="alert alert-danger">
                                                <?php echo e(session()->get('message-danger-delete')); ?>

                                            </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr class="centerTh">
                                <th><?php echo app('translator')->get('lang.title'); ?></th>
                                <th>docs for</th>
                                <th>docs type</th>
                                <th>public</th>
                                <th>created at</th>
                                <th>action</th>
                                <?php if(Auth::user()->role_id == 1 || Auth::user()->role_id == 5): ?>
                                <th>approve</th>
                                <?php endif; ?>
                            </tr>
                            </thead>

                            <tbody>
                                <?php $__currentLoopData = $docs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="centerTd">
                                        <td><?php echo e($value->title); ?></td>
                                        <td><?php echo e($value->stf_or_std == "stf" ? 'Staff' : 'Student'); ?></td>
                                        <td><?php echo e(Modules\Docs\Entities\DocsType::find($value->docs_type_id)->title); ?></td>
                                        <td><?php echo e($value->public == "0" ? 'No' : 'Yes'); ?></td>
                                        <td><?php echo e($value->created_at); ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <?php echo app('translator')->get('lang.select'); ?>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <?php if($value->pin_to_top == 0): ?>
                                                        <a href="<?php echo e(url('docs/pinToTop/'.$value->id)); ?>" class="dropdown-item" title="Docs Details" data-modal-size="modal-md">Pin to top</a>
                                                    <?php else: ?>
                                                        <a href="<?php echo e(url('docs/unpinFromTop/'.$value->id)); ?>" class="dropdown-item" title="Docs Details" data-modal-size="modal-md">UnPin from top</a>
                                                    <?php endif; ?>
                                                    <a href="<?php echo e(url('docs/view/'.$value->id)); ?>" class="dropdown-item small fix-gr-bg modalLink" title="Docs Details" data-modal-size="modal-md">view</a>
                                                    <a class="dropdown-item" href="<?php echo e(url('docs/edit/'.$value->id)); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                                    <a href="<?php echo e(url('docs/delete-view/'.$value->id)); ?>" class="dropdown-item small fix-gr-bg modalLink" title="Delete Docs" data-modal-size="modal-md">
                                                        <?php echo app('translator')->get('lang.delete'); ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        <?php if(Auth::user()->role_id == 1 || Auth::user()->role_id == 5): ?>
                                            <td>
                                                <div class="cbx">
                                                    <input <?php if($value->approve_status != 0): ?> checked <?php endif; ?> value="<?php echo e($value->id); ?>" id="cbx" class="changeApproveStatus" type="checkbox"/>
                                                    <label for="cbx"></label>
                                                    <svg width="15" height="14" viewbox="0 0 15 14" fill="none">
                                                    <path d="M2 8.36364L6.23077 12L13 2"></path>
                                                    </svg>
                                                </div>
                                            </td>
                                        <?php elseif(Auth::user()->role_id == 1 || Auth::user()->role_id == 5): ?>
                                            <td></td>
                                        <?php endif; ?>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    $('#summernote').summernote({

       toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['fontname', ['fontname']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'video']],
    ['view', ['fullscreen', 'codeview', 'help']],
    ['height', ['height']]
],
});
</script>

<script type="text/javascript">

    $(document).ready(function(){
        var value=$('#stf_std').val();
        var public_value = $('#public select').val();
        if(value == "std"){
            $('#public').css("display", "inline");
            if(public_value == "0"){
                $('#class').css("display", "inline");
                $('#student').css("display", "block");
            }
            else{
                $('#class').css("display", "none");
                $('#student').css("display", "none");
                $('#student select').val("");
            }
        }
        else if(value == "stf"){
            $('#public').css("display", "none");
            
        }
    });

    $(document).ready(function(){
        $('#stf_std').change(function(e){
            var value=$(this).val();
            if(value == "std"){
                $('#public').css("display", "inline");
            }
            else if(value == "stf"){
                $('#student select').val("");
                $('#public select').val("0");
                $('#public div div span').text("NO")
                $('#public').css("display", "none");
                $('#class').css("display", "none");
                $('#student').css("display", "none");
            }
        });
    });

    $(document).ready(function(){
        $('#public select').change(function(e){
            var value=$(this).val();
            if(value == "0"){
                $('#class').css("display", "inline");
                $('#student').css("display", "block");
            }
            else if(value == "1"){
                $('#student select').val("");
                $('#class').css("display", "none");
                $('#student').css("display", "none");
                $('#student select').val("");
            }
        });
    });

    $(document).ready(function() {
        $("#class select").on("change", function() {
            var url = $("#url").val();

            var formData = {
                id: $(this).val(),
            };
            $.ajax({
                type: "GET",
                data: formData,
                dataType: "json",
                url: url + "/" + "docs/getStudent",
                success: function(data) {
                    var a = "";
                    $.each(data, function(i, item) {
                        if (item.length) {
                            $("#student select").find("option").not(":first").remove();
                            $("#student ul").find("li").not(":first").remove();

                            $.each(item, function(i, student) {
                                $("#student select").append(
                                    $("<option>", {
                                        value: student.id,
                                        text: student.full_name,
                                    })
                                );

                                $("#student ul").append(
                                    "<li data-value='" +
                                    student.id +
                                    "' class='option'>" +
                                    student.full_name +
                                    "</li>"
                                );
                            });
                        } else {
                            $("#student .current").html("Student");
                            $("#student select").find("option").not(":first").remove();
                            $("#student ul").find("li").not(":first").remove();
                        }
                    });
                },
                error: function(data) {
                    console.log('Error:', data);
                },
            });
        });
    });

    $(document).ready(function(){
        $(".changeApproveStatus").click(function(){
            var url = $("#url").val();

            var formData = {
                id: $(this).val(),
            };
            $.ajax({
                type: "GET",
                data: formData,
                dataType: "json",
                url: url + "/" + "docs/changeApproveStatus",
                success: function (data) {
                    console.log(data);
                }
            });
        });
    });
    
    
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/Docs\Resources/views/index.blade.php ENDPATH**/ ?>