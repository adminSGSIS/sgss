
<?php $__env->startSection('main_content'); ?>


<img width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an.jpeg">
<div class="container mt-4">
        <h3 class="titleNews mb-3" style="text-align: center"><?php echo e($name_subcate->sub_category_name); ?></h3>
        <div class="col">
            <div class="row" style="width: 90%; margin: auto">
                <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lastest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                 
                    <div class="col-lg-4 col-sm-6 col-12 item">
                        <a href="<?php echo e(url('news-details/'.$lastest->id)); ?>" title="View Details">
                            <div style="height: 500px; "  class="card news-card" >
                                <div class="zoom img-hover-zoom">
                                <img class="card-img-top" src="<?php echo e(asset($lastest->image)); ?>" height="auto" width="auto" alt="Card image cap">
                                </div>
                                <div class="card-body">
                                    <div class="card-title">
                                        <h5><?php echo e(App\SmGeneralSettings::DateConvater($lastest->publish_date)); ?></h5>
                                        <h4><?php echo e(\Illuminate\Support\Str::limit($lastest->news_title,70)); ?></h4>
                                    </div>      
                                    <br>
                                    <div class="card-description">
                                        <?php if(strlen($lastest->description) > 10): ?>
                                            <p class="card-text"><?php echo e(\Illuminate\Support\Str::limit($lastest->description,150)); ?></p>
                                        <?php else: ?>
                                            <a href="<?php echo e(url('news-details/'.$lastest->id)); ?>">Read More</a>
                                        <?php endif; ?>
                                    </div>    
                                </div>
                            </div>
                        </a>
                    </div>                                      
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/category_items.blade.php ENDPATH**/ ?>