
<?php $__env->startSection('mainContent'); ?>
<style>
    .pad-top-20 {
        padding-top: 20px;
    }
    .pad-bot-20{
        padding-bottom: 20px;
    }
    .hidden {
        display: none;
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff;
    background-color: #05B7FF !important;
}
</style>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Supply Medicine To Patient</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
       
        <div class="col-12">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane  fade show active" id="v-pills-current" role="tabpanel">
                    <form method="Get" action="#">
                        <?php echo csrf_field(); ?>
                        <div class="white-box">
                            <h2 class="center" style="text-align: center;">Supply Medicine To Patient</h2>
                            <div class="container">
                                <div class="row mt-30">
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="inputName">Staff Name</label>
                                            <select name="bloodgroup" id="inputBlood" class="custom-select primary-input">
                                                <?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($staff->id); ?>"><?php echo e($staff->full_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="inputEmail">Type of Medicine</label>
                                            <select name="bloodgroup" id="inputBlood" class="custom-select primary-input">
                                                <?php $__currentLoopData = $medicine_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value=""><?php echo e($value->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="inputBlood">Date</label>
                                            <input type="text" class="form-control primary-input date" id="inputBirth" name="day_of_birth">
                                            <span class="focus-border"></span>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-10">
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="inputName">Name of Medicine</label>
                                            <select name="bloodgroup" id="inputBlood" class="custom-select primary-input">
                                                <?php $__currentLoopData = $medicines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value=""><?php echo e($value->medicine_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div>
                                            <label for="inputName">Amount</label>
                                            <input type="text" class="form-control primary-input" id="inputAddress" name="address" required>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class=" mt-10">
                                        
                                    <div class="pad-top-20">
                                        <h1>Patient Name</h1>
                                        <div class="row">
                                            <div class="col-lg-4 input-effect">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="1" name="patientType" id="studentRadio"
                                                        onclick="checkType()">
                                                    <label class="form-check-label" for="studentRadio">
                                                        Student
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 input-effect">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="2" name="patientType" id="teacherRadio"
                                                        onclick="checkType()">
                                                    <label class="form-check-label" for="teacherRadio">
                                                        Teacher
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 input-effect">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="3" name="patientType" id="staffRadio"
                                                        onclick="checkType()">
                                                    <label class="form-check-label" for="staffRadio">
                                                        Employeer
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="studentBoard" class="hidden">
                                        <div class="pad-top-20">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <select name="student_id" id="student_list"
                                                        class="niceSelect w-100 bb form-control<?php echo e(@$errors->has('students') ? ' is-invalid' : ''); ?>">
                                                        <option data-display="<?php echo app('translator')->get('student name'); ?> *" value=""><?php echo app('translator')->get('student name'); ?> *</option>
                                                        <?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if(isset($add_income)): ?>
                                                        <option value="<?php echo e(@$student->id); ?>"
                                                            <?php echo e(@$add_income->student_id == @$student->id? 'selected': ''); ?>>
                                                            <?php echo e(@$student->full_name); ?> | <?php echo e(@$student->class->class_name); ?></option>
                                                        <?php else: ?>
                                                        <option label="<?php echo e(@$student->admission_no); ?>" value="<?php echo e(@$student->id); ?>"
                                                            <?php echo e(old('student_list') == @$student->id? 'selected' : ''); ?>><?php echo e(@$student->full_name); ?> |
                                                            <?php echo e(@$student->class->class_name); ?></option>
                                                        <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                            </div>
                                        </div>
                        
                                    </div>
                                    <div id="teacherBoard" class="pad-top-20 hidden">
                                        <div class="pad-top-20">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                                <div class="col-lg-6 col-md-6 sm-12">
                                                    <select   name="teacher_id"
                                                        class="niceSelect w-100 bb form-control<?php echo e(@$errors->has('staffs') ? ' is-invalid' : ''); ?>">
                                                        <option data-display="<?php echo app('translator')->get('staff name'); ?> *" value=""><?php echo app('translator')->get('staff name'); ?> *</option>
                                                        <?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($teacher->id); ?>"><?php echo e($teacher->full_name); ?> | <?php echo e($teacher->staff_no); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="staffBoard" class="pad-top-20 hidden">
                                        <div class="pad-top-20">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                                <div class="col-lg-6 col-md-6 sm-12">
                                                    <select   name="staff_id"
                                                        class="niceSelect w-100 bb form-control<?php echo e(@$errors->has('staffs') ? ' is-invalid' : ''); ?>">
                                                        <option data-display="<?php echo app('translator')->get('staff name'); ?> *" value=""><?php echo app('translator')->get('staff name'); ?> *</option>
                                                        <?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($staff->id); ?>"><?php echo e($staff->full_name); ?> | <?php echo e($staff->staff_no); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-3">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                                <div class="mt-30 text-center">
                                    <button type="button" class="btn btn-success">Save</button>
                                </div>  
                            </div>
                                  
                        </div>
                    </form>
                </div>

            </div>
        </div>
     
    </div>
</section>
<section class="admin-visitor-area up_st_admin_visitor mt-30">
    <div class="container-fluid p-0">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-4 no-gutters">
                    <div class="main-title">
                        <h3><?php echo app('translator')->get('Patient List'); ?></h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                        <thead>
                            <?php if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != ""): ?>
                            <tr>
                                <td colspan="7">
                                    <?php if(session()->has('message-success-delete')): ?>
                                    <div class="alert alert-success">
                                        <?php echo e(session()->get('message-success-delete')); ?>

                                    </div>
                                    <?php elseif(session()->has('message-danger-delete')): ?>
                                    <div class="alert alert-danger">
                                        <?php echo e(session()->get('message-danger-delete')); ?>

                                    </div>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><a href="<?php echo e(url('/medical/patient-PDF')); ?>" class="primary-btn small fix-gr-bg">
                                    <?php echo app('translator')->get('Excel'); ?>
                                    </a></th>
                            </tr>
                            <tr>
                                <th><?php echo app('translator')->get('No'); ?></th>
                                <th><?php echo app('translator')->get('Staff Name'); ?></th>
                                <th><?php echo app('translator')->get('Patient Name'); ?></th>
                                <th><?php echo app('translator')->get('Type of Medicine'); ?></th>
                                <th><?php echo app('translator')->get('Name of Medicine'); ?></th>
                                <th><?php echo app('translator')->get('action'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <?php if(2>1): ?>
                                    Yes
                                    <?php else: ?>
                                    No
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                            <?php echo app('translator')->get('lang.select'); ?>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a title="View/Edit Leave Details"
                                                class="dropdown-item" href="<?php echo e(url('medical/patient-edit/'.'/')); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                                <a title="Export PDF file" target="_blank"
                                                class="dropdown-item" href="<?php echo e(url('medical/patient-PDF/')); ?>"><?php echo app('translator')->get('Export PDF'); ?></a>
                                            <a class="dropdown-item" data-toggle="modal"
                                                data-target="#DeletePatient"
                                                href="#"><?php echo app('translator')->get('lang.delete'); ?></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <div class="modal fade admin-query" id="DeletePatient">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title"><?php echo app('translator')->get('lang.delete'); ?> <?php echo app('translator')->get('lang.item'); ?></h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <h4><?php echo app('translator')->get('lang.are_you_sure_to_delete'); ?></h4>
                                            </div>

                                            <div class="mt-40 d-flex justify-content-between">
                                                <button type="button" class="primary-btn tr-bg"
                                                    data-dismiss="modal"><?php echo app('translator')->get('lang.cancel'); ?></button>
                                                <?php echo e(Form::open(['url' => 'medical/patient-delete/', 'method' => 'DELETE', 'enctype' => 'multipart/form-data'])); ?>

                                                <button class="primary-btn fix-gr-bg"
                                                    type="submit"><?php echo app('translator')->get('lang.delete'); ?></button>
                                                <?php echo e(Form::close()); ?>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function checkType() {
        if (document.getElementById('studentRadio').checked) {
            document.getElementById("studentBoard").removeAttribute("class", "hidden");
            document.getElementById("teacherBoard").setAttribute("class", "hidden");
            document.getElementById("staffBoard").setAttribute("class", "hidden")
        } else if (document.getElementById('teacherRadio').checked) {
            document.getElementById("studentBoard").setAttribute("class", "hidden");
            document.getElementById("teacherBoard").removeAttribute("class", "hidden");
            document.getElementById("staffBoard").setAttribute("class", "hidden")
        } else if (document.getElementById('staffRadio').checked) {
            document.getElementById("studentBoard").setAttribute("class", "hidden");
            document.getElementById("teacherBoard").setAttribute("class", "hidden");
            document.getElementById("staffBoard").removeAttribute("class", "hidden")
        }
    }
</script>
<script type="text/javascript">
    $('#student_list').on('change', function () {
        var id = $('#student_list option:selected').attr('label');
        $('#admission_no').val(id);
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/Medical\Resources/views/supply-medicine-to-patient.blade.php ENDPATH**/ ?>