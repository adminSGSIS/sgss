
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
<?php $__env->stopPush(); ?>
<style>
#sgsis-team-mobile{ display: none}
@media  screen and (max-width: 540px) {
    #sgsis-team-web{ display: none}   
    #sgsis-team-mobile{ display: block}
    .title-mobile{
        background: #7dd3f7;
        color: #fff !important;
        font-size: 20px;
        font-family: 'roboto';
        padding: 0.5rem 1.5rem !important;
        border-radius: 25px;
        font-weight: 600;
        margin-bottom: 7px;
    }
    .sgsis-team {
        height: auto !important;
    }
    .btn-profile{ font-size: 20px !important}
}
</style>
<?php $__env->startSection('main_content'); ?>
    <div class="banner">
        <img class="banner" width="100%" src="<?php echo e(asset('public/images/banner/banner_03.jpg')); ?>" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>sgsis news team</h3>
            </div>
        </div>
    </div>
    <div class="c-color">

        <div class="container" id="sgsis-team-web">

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner sgsis-team">
                    <div class="carousel-item active">
                        <div class="d-flex justify-content-center">
                            <img src="/public/images/01.png" alt="" style="width:50%">
                        </div>
                        <div class="d-flex justify-content-center">
                            <img src="/public/images/02.png" alt="" style="width:33%">
                        </div>
                    </div>

                    <div class="carousel-item ">
                        <p class="title-sgsis">Journalist</p>
                        <div class="student d-flex justify-content-center">
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-01.png" alt=""></br>
                                <button type="button" class="btn-profile">Duyen Huynh T.Kim</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-02.png" alt=""></br>
                                <button type="button" class="btn-profile">Tom Thai</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-03.png" alt=""></br>
                                <button type="button" class="btn-profile">Nurlykhan Nurkhojayev</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-04.png" alt=""></br>
                                <button type="button" class="btn-profile">Henry Nguyen</button>
                            </div>

                        </div>
                    </div>

                    <div class="carousel-item">
                        <p class="title-sgsis">Journalist & Ad Manager</p>
                        <div class="student d-flex justify-content-center">
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-05.png" alt=""></br>
                                <button type="button" class="btn-profile">Isaiah Mehrudeen</button>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item ">
                        <p class="title-sgsis">Journalist & News Anchor</p>
                        <div class="student d-flex justify-content-center">
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-06.png" alt=""></br>
                                <button type="button" class="btn-profile">Vu Anh Tran</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-07.png" alt=""></br>
                                <button type="button" class="btn-profile">Thao Anh Tran</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-08.png" alt=""></br>
                                <button type="button" class="btn-profile">Timofey Mednonogov</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-09.png" alt=""></br>
                                <button type="button" class="btn-profile">Peter Ivanov</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-10.png" alt=""></br>
                                <button type="button" class="btn-profile">Seohyun Lee</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-11.png" alt=""></br>
                                <button type="button" class="btn-profile">Mathis D'Hertoge</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-12.png" alt=""></br>
                                <button type="button" class="btn-profile">Ky Anh Nguyen Huu</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-13.png" alt=""></br>
                                <button type="button" class="btn-profile">Hieu Liem Phung</button>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <p class="title-sgsis">Journalist & Photographer</p>
                        <div class="student d-flex justify-content-center">
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-22.png" alt=""></br>
                                <button type="button" class="btn-profile">Ori Mintz</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-23.png" alt=""></br>
                                <button type="button" class="btn-profile">Mia Vu</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-24.png" alt=""></br>
                                <button type="button" class="btn-profile">Jake Park</button>
                            </div>

                        </div>
                    </div>

                    <div class="carousel-item ">
                        <p class="title-sgsis">Journalist & News Reporter</p>

                        <div class="student d-flex justify-content-center">
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-14.png" alt=""></br>
                                <button type="button" class="btn-profile">Rei Soh</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-15.png" alt=""></br>
                                <button type="button" class="btn-profile">Naama Mintz</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-16.png" alt=""></br>
                                <button type="button" class="btn-profile">Min Hee Lee</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-17.png" alt=""></br>
                                <button type="button" class="btn-profile">Johnson Bui</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-18.png" alt=""></br>
                                <button type="button" class="btn-profile">Aitana Bernabeu</button>
                            </div>
                        </div>

                    </div>

                    <div class="carousel-item">
                        <p class="title-sgsis">Journalist & Editor</p>
                        <div class="student d-flex justify-content-center">
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-19.png" alt=""></br>
                                <button type="button" class="btn-profile">Oliver Bindzus</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-20.png" alt=""></br>
                                <button type="button" class="btn-profile">Khanh Vy Ninh</button>
                            </div>
                            <div class="profile collumn">
                                <img src="/public/images/ID/ID-21.png" alt=""></br>
                                <button type="button" class="btn-profile">Alexis Chan</button>
                            </div>

                        </div>
                    </div>

                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick="e.preventDefault();">
                    <span class="fa fa-chevron-left" style="color:#149bd7;font-size:25px" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" onclick="e.preventDefault();">
                    <span class="fa fa-chevron-right" style="color:#149bd7;font-size:25px" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
            </div>

        </div>

        <div class="container pt-30" id="sgsis-team-mobile">

            <div id="demo" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner sgsis-team text-center">
                    <div class="carousel-item active">
                        <a type="button" class="title-mobile">Logo</a>
                        <div class="d-flex justify-content-center">
                            <img src="/public/images/01.png" alt="" style="width:90%">
                        </div>
                        <div class="d-flex justify-content-center">
                            <img src="/public/images/02.png" alt="" style="width:80%">
                        </div>
                    </div>

                    <div class="carousel-item ">
                        <a type="button" class="title-mobile">Journalist</a>
                        <div class="text-center">
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-01.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Duyen Huynh T.Kim</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-02.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Tom Thai</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-03.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Nurlykhan Nurkhojayev</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-04.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Henry Nguyen</button>
                            </div>

                        </div>
                    </div>

                    <div class="carousel-item">
                        <a type="button" class="title-mobile">Journalist & Ad Manager</a>
                        <div class="">
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-05.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Isaiah Mehrudeen</button>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item ">
                        <a type="button" class="title-mobile">Journalist & News Anchor</a>
                        <div class="">
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-06.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Vu Anh Tran</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-07.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Thao Anh Tran</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-08.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Timofey Mednonogov</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-09.png" alt=""  width="80%"></br>
                                <button type="button" class="btn-profile">Peter Ivanov</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-10.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Seohyun Lee</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-11.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Mathis D'Hertoge</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-12.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Ky Anh Nguyen Huu</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-13.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Hieu Liem Phung</button>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <a type="button" class="title-mobile">Journalist & Photographer</a>
                        <div class="">
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-22.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Ori Mintz</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-23.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Mia Vu</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-24.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Jake Park</button>
                            </div>

                        </div>
                    </div>

                    <div class="carousel-item ">
                        <a type="button" class="title-mobile">Journalist & News Reporter</a>

                        <div class="">
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-14.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Rei Soh</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-15.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Naama Mintz</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-16.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Min Hee Lee</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-17.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Johnson Bui</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-18.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Aitana Bernabeu</button>
                            </div>
                        </div>

                    </div>

                    <div class="carousel-item">
                        <a type="button" class="title-mobile">Journalist & Editor</a>
                        <div class="">
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-19.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Oliver Bindzus</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-20.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Khanh Vy Ninh</button>
                            </div>
                            <div class="col-12 text-center pb-20">
                                <img src="/public/images/ID/ID-21.png" alt="" width="80%"></br>
                                <button type="button" class="btn-profile">Alexis Chan</button>
                            </div>

                        </div>
                    </div> 

                </div>
                <a class="carousel-control-prev" href="#demo" role="button" data-slide="prev" onclick="e.preventDefault();">
                    <span class="fa fa-chevron-left" style="color:#149bd7;font-size:25px" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#demo" role="button" data-slide="next" onclick="e.preventDefault();">
                    <span class="fa fa-chevron-right" style="color:#149bd7;font-size:25px" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
            </div>

        </div>
    </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\resources\views/frontEnd/home/sgsisTeam.blade.php ENDPATH**/ ?>