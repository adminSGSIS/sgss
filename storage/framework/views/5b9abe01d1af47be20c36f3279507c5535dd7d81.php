<div class="row mb-40 mt-30">
  <div class="col-lg-10">
    <p>If your child has any medical condition or chronic disease which requires medication, or which mayaffect his/her daily routine, please describe it here *</p>
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('answer1') ? ' is-invalid' : ''); ?>" type="text" name="answer1" value="<?php echo e(old('answer1')); ?>">

      <label>Answer</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="answer1_feedback" role="alert"></span>
      <?php if($errors->has('answer1')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('answer1')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
</div>
<div class="row mb-40 mt-30">
  <div class="col-lg-10">
    <p>Allergies to medications and/or foods *</p>
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('answer2') ? ' is-invalid' : ''); ?>" type="text" name="answer2" value="<?php echo e(old('answer2')); ?>">

      <label>Answer</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="answer2_feedback" role="alert"></span>
      <?php if($errors->has('answer2')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('answer2')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
</div>

<div class="row mb-40 mt-30">
  <div class="col-lg-10">
    <p>Medications taken on a regular basis *</p>
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('answer3') ? ' is-invalid' : ''); ?>" type="text" name="answer3" value="<?php echo e(old('answer3')); ?>">

      <label>Answer</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="answer3_feedback" role="alert"></span>
      <?php if($errors->has('answer3')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('answer3')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
</div><?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/forms/step4.blade.php ENDPATH**/ ?>