
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/frontend/css/new_style.css" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
<section class="container mt-4">
    <div class="row mb-4">
        <div class="contentNews">
            <div class="post-title">
                <h3><?php echo e($news->news_title); ?></h3>
            </div>
            <h5 style="text-transform: uppercase;"><?php echo e(App\SmGeneralSettings::DateConvater($news->publish_date)); ?></h5>
            <div class="news-body">
                <p><?php echo $news->news_body; ?></p>
            </div>            
        </div>
    </div>
    <div class="relatedPost">
        <h4>Related Posts</h4>
        <div class="row">
            <?php $__currentLoopData = App\SmNews::where('sub_category_id', $news->sub_category_id)->where('id', '!=', $news->id)->orderBy('updated_at', 'desc')->take(3)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lastest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                 
                <div class="col-lg-4 col-sm-6 col-12 item">
                    <a href="<?php echo e(url('news-details/'.$lastest->id)); ?>" title="View Details">
                        <div style="height: 500px; " class="card news-card">
                            <div class="zoom img-hover-zoom">
                            <img class="card-img-top" src="<?php echo e(asset($lastest->image)); ?>" height="auto" width="auto" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <div class="card-title">
                                    <h5><?php echo e(App\SmGeneralSettings::DateConvater($lastest->publish_date)); ?></h5>
                                    <h4><?php echo e(\Illuminate\Support\Str::limit($lastest->news_title,70)); ?></h4>
                                </div>      
                                <div class="card-description">
                                    <?php if(strlen($lastest->description) > 10): ?>
                                        <p class="card-text"><?php echo e(\Illuminate\Support\Str::limit($lastest->description,150)); ?></p>
                                    <?php else: ?>
                                        <a href="<?php echo e(url('news-details/'.$lastest->id)); ?>">Read More</a>
                                    <?php endif; ?>
                                </div>    
                            </div>
                        </div>
                    </a>
                </div>                                      
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/light_news_details.blade.php ENDPATH**/ ?>