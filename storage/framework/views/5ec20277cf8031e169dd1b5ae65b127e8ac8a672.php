<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Fee Group</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                <a href="<?php echo e(url('fee-group')); ?>">All group</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <form action="<?php echo e(url('fee-group')); ?>" method="POST">
            <?php echo csrf_field(); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-title">
                                        <h4>Create Fee Group</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-30 mt-3">
                                <div class="col-12">
                                    <div class="input-effect">
                                        <input class="primary-input form-control" type="text" required 
                                        name="group_name" value="<?php echo e(old('group_name')); ?>" placeholder="Group Name *">
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="row mt-40">
                        <div class="col-lg-12 text-center">
                            <button class="primary-btn fix-gr-bg">
                                <span class="ti-check"></span>
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/fee_group/create.blade.php ENDPATH**/ ?>