<?php $__env->startSection('css'); ?>
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>FEE</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">List</h3>
                        </div>
                    </div>
                </div>
                <?php if(isset($EditData)): ?>
                <form action="<?php echo e(url('fee/update')); ?>" method="POST">
                    <input type="hidden" name="edit_id" value="<?php echo e($EditData->id); ?>">
                <?php else: ?>
                <form action="<?php echo e(url('fee')); ?>" method="POST">
                <?php endif; ?>
                <?php echo csrf_field(); ?>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 ">
                        <div class="white-box mt-30">
                             
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h4>Create Fee</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-30 mt-3">
                            <div class="col-12">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" required 
                                    name="fee_name" value="<?php echo e(isset($EditData) ? $EditData->fee_name : ''); ?>" placeholder="Fee Name *">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        <br>
                        <div class="col-12 mt-30">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="number" required 
                                    name="amount" value="<?php echo e(isset($EditData) ? $EditData->amount : ''); ?>" placeholder="Fee Amount *">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-3">
                            <div class="col-12">
                                
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select id="select_group" class="niceSelect w-100 bb form-control<?php echo e($errors->has('id_group') ? ' is-invalid' : ''); ?>" 
                                        name="id_group">
                                        <option data-display="Fee Group *" value="">Fee Group *</option>
                                        <?php $__currentLoopData = $groupFee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->id); ?>" <?php echo e(isset($EditData) && $EditData->groupfee->id == $value->id ? 'selected' : ''); ?>>
                                                <?php echo e($value->group_name); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('id_group')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong><?php echo e($errors->first('id_group')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                       
                        <div id="class_select" class="row mb-40 mt-3" style="display:none;">
                            <div class="col-12">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select id="select_class" class="niceSelect w-100 bb form-control<?php echo e($errors->has('id_group') ? ' is-invalid' : ''); ?>" 
                                        name="class_id">
                                        <option data-display="Class *" value="">Class *</option>
                                        <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->id); ?>" <?php echo e(isset($EditData) && $EditData->id_group == 2 && $EditData->class->id == $value->id ? 'selected' : ''); ?>>
                                                <?php echo e($value->class_name); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row mt-40">
                        <div class="col-lg-12 text-center">
                            <button class="primary-btn fix-gr-bg">
                                <span class="ti-check"></span>
                                Save
                            </button>
                        </div>
                    </div>
                    </div>
                
                </form>        
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>Fee name</th>
                                    <th>Amount</th>
                                    <th>Group name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $fees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="centerTd">
                                        <td><?php echo e($value->fee_name); ?></td>
                                        <td><?php echo e(number_format($value->amount)); ?></td>
                                        <td><?php echo e($value->groupfee->group_name); ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <?php echo app('translator')->get('lang.select'); ?>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="/fee/edit/<?php echo e($value->id); ?>">edit</a>
                                                    <a class="dropdown-item" href="/fee/delete/<?php echo e($value->id); ?>">delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script>
    $( document ).ready(function() {
        if($('#select_group').val() == 2)
        {
            $('#class_select').show();
            $('#select_class').attr('required','required');
        }
        else{
            $('#class_select').hide();
            $('#select_class').removeAttr('required');
            $('#select_class option:selected').val('');
        }
    });
    $('#select_group').on('change',function(){
        if($(this).val() == 2)
        {
            $('#class_select').show();
            $('#select_class').attr('required','required');
        }
        else{
            $('#class_select').hide();
            $('#select_class').removeAttr('required');
            $('#select_class option:selected').val('');
        }
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/fee/index.blade.php ENDPATH**/ ?>