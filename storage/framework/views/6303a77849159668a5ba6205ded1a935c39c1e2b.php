<footer id="footer" >
      <div class="footer-top">
        <div class="container">
          <div class="row">
         
          <div class="grid-footer col-lg-12 row" id="main-footer">
              <div class="col-lg-4 col-sm-12  last-footer">
                <div class="col-sm-12">
                  <img src="/public/frontEnd/assets/img/logo_bottom_burned.png" alt="img">
                  <p class="title-logo">Inspired Learners. Global Thinkers.</p>
                  <ul class="social">
                      <li>
                        <a href="">
                            <img src="/public/images/icon-bottom/ICON-INSTA.png" alt="">
                        </a>
                      </li>
                      <li>
                        <a href="https://www.facebook.com/saigonstarschool/" >
                            <img src="/public/images/icon-bottom/ICON-FB.png" alt="">
                        </a>
                      </li>
                      <li>
                        <a href="https://twitter.com/StarSaigon" >
                            <img src="/public/images/icon-bottom/ICON-TW.png" alt="">
                        </a>
                      </li>
                     
                      <li>
                        <a href="https://www.youtube.com/channel/UCcKlFmxYK5C6piLNe18hhRg" >
                            <img src="/public/images/icon-bottom/ICON-YOUTUBE.png" alt="">
                        </a>
                      </li>
                  </ul>
                  <b class="footer_address">Su Hy Nhan Street, Residential Area No. 5 </br>
                    Thanh My Loi Ward,Thu Duc City </br>
                    <div class="row">
                        <div class="col-6 address-text-sm">www.sgstar.edu.vn</div>
                        <div class="col-6 address-text-sm">info@sgstar.edu.vn</div>
                    </div>
                     <div class="row">
                        <div class="col-6 address-text-sm">028 3742 32222</div>
                        <div class="col-6 address-text-sm">08 8800 6996</div>
                    </div>
                    
                  </b>
                  </div>
              </div>  
              <div class="col-lg-8 col-sm-12">
                <div class="row">
                    <div class="col-sm-12 col-lg-3">
                      <h1>OUR SCHOOL</h1>
                      <ul>
                        <li>Welcome message</li>
                        <li>Overview</li>
                        <li>Teachers</li>
                        <li>Campus</li>
                      </ul>
                    </div>
                    <div class=" col-sm-12 col-lg-3">
                      <h1>CURRICULUM</h1>
                      <ul>
                        <li>Overview</li>
                        <li>Early Years</li>
                        <li>Primary Years</li>
                      </ul>
                    </div>
                    <div class=" col-sm-12 col-lg-3">
                      <h1>ADMISSIONS</h1>
                      <ul>
                        <li>Why SGSIS</li>
                        <li>Admissions process</li>
                        <li>Fees and policy</li>
                        <li>Age and class</li>
                        <li>Scholarship</li>
                        <li>Book a tour</li>
                        <li>Admissions team</li>
                      </ul>
                    </div>
                    <div class=" col-sm-12 col-lg-3">
                      <h1>OUR PARENTS</h1>
                      <ul>
                        <li>School Calender</li>
                        <li>Timetable</li>
                        <li>Menu</li>
                        <li>Bus Schedule</li>
                        <li>Workshop</li>
                        <li>Parent essentials</li>
                        
                        <li>Parent Teacher Group</li>
                      </ul>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col-sm-12 col-lg-3">
                      <h1>OUR STUDENTS</h1>
                      <ul>
                        <li>Student Council</li>
                        <li>SGS News team</li>
                        <li>SGS TV team</li>
                        <li>Winter Camp</li>
                        <li>Summer Camp</li>
                        <li>Gallery</li>
                        <li>Community success</li>
                            
                      </ul>
                    </div>
                    <div class=" col-sm-12 col-lg-3">
                      <h1>NEWS & EVENTS</h1>
                      <ul>
                        <li>Quality cycle</li>
                        <li>Gallery of events</li>
                        <li>The SGSIS Journal</li>
                        <li>The SGSIS TV</li>
                      </ul>
                    </div>
                    <div class=" col-sm-12 col-lg-3">
                      <h1>OUR STAFF</h1>
                      <ul>
                        <li>Meet our team</li>
                        <li>Workshop</li>
                        <li>Join Us</li>
                      </ul>
                    </div>
                    <div class=" col-sm-12 col-lg-3">
                      <h1 style="width:190px">INTERNATIONAL PARTNER</h1>
                      <h1 style="width:215px;border-bottom: none">
                      <img  style="margin-left:-7%"src="/public/images/LOGO_FIELDWORK.png" alt="img">
                      </h1>
                      
                    </div>
                    
                  </div>
              </div>
              <div class="col-lg-12">
            <p class="footer_outro">
               © 2020 The President and Fellows of SaiGon Star School
            </p>
          </div>
          </div>

          <div class="col-12 footer-mb">
                <img src="/public/frontEnd/assets/img/logo_bottom_burned.png" alt="img">
                <p class="title-logo-mb" >Inspired Learners. Global Thinkers.</p>
                <ul class="social">
                      <li>
                        <a href="">
                            <img src="/public/images/icon-bottom/ICON-INSTA.png" alt="">
                        </a>
                      </li>
                      <li>
                        <a href="https://www.facebook.com/saigonstarschool/" >
                            <img src="/public/images/icon-bottom/ICON-FB.png" alt="">
                        </a>
                      </li>
                      <li>
                        <a href="https://twitter.com/StarSaigon">
                            <img src="/public/images/icon-bottom/ICON-TW.png" alt="">
                        </a>
                      </li>
                     
                      <li>
                        <a href="https://www.youtube.com/channel/UCcKlFmxYK5C6piLNe18hhRg">
                            <img src="/public/images/icon-bottom/ICON-YOUTUBE.png" alt="">
                        </a>
                      </li>
                  </ul>
                  <p class="footer_address">Su Hy Nhan Street, Residential Area No. 5 </br>
                    Thanh My Loi Ward,Thu Duc City</br>
                    www.sgstar.edu.vn </br>  info@sgstar.edu.vn </br>
                    028 3742 32222 </br> 08 8800 6996
                  </p>
                  <h3>INTERNATIONAL PARTNER</h3>
                  <img width="85%" src="/public/images/LOGO_FIELDWORK.png" alt="img">
          </div>
          <div class="col-12 footer-mb-title">
              <h1>OUR SCHOOL</h1>
              <h1>CURRICULUM</h1> 
              <h1>ADMISSIONS</h1>
              <h1>OUR PARENTS</h1>
              <h1>OUR STUDENTS</h1>
              <h1>NEWS & EVENTS</h1>
              <h1>OUR STAFF</h1>
          </div>
          
          </div><!-- row -->
        </div><!-- container -->
      </div><!-- footer top -->
      <div class="footer_outro_mb">
          <p >
               © 2020 The President and Fellows of SaiGon Star School
          </p>
      </div>
    </footer>
    <!-- <a class="page-scroll scroll-top" href="#scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a> -->
    <!-- Footer End here -->
   
    <!--<div id="zalo_icon">-->
    <!--    <a href="https://zalo.me/0888944455" target="_blank"><img width="64" src="/public/icon-zalo.png"></a>-->
    <!--</div>-->
    
    <div class="action_footer">
        <a id="phone_icon" href="#" onclick="event.preventDefault();showsidebar();" ><img src="https://freeiconshop.com/wp-content/uploads/edd/plus-flat.png"></a>
    </div>
    <!--https://kingbag.com.vn/icon/phone.png-->
    <!-- Footer End here -->
	<!-- include ending file tags and JavaScript inclusions -->
  </div> <!-- end #page -->

        <!--

      JavaScripts

    -->
        <script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/jquery-3.2.1.min.js"></script> 
        <script src="<?php echo e(url('public')); ?>/backEnd/js/lightbox-plus-jquery.js"></script>

        <script src="<?php echo e(url('public/frontEnd')); ?>/assets/js/script.js?v=20200311"></script>
        <script src="<?php echo e(url('public/frontEnd')); ?>/assets/slick/slick.js"></script>
    	  
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        
    	<script src="<?php echo e(url('public/frontEnd')); ?>/assets/gallery_slider/js/jquery.flexisel.js"></script>
        
        
        <script src="<?php echo e(url('public')); ?>/backEnd/vendors/js/popper.js"></script>
        <!-- jquery -->
        
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
        <!-- Bootstrap -->
        <!--<script src="<?php echo e(url('public')); ?>/js/bootstrap.min.js"></script>-->
        <!-- Isotope -->
        <script src="<?php echo e(url('public')); ?>/js/isotope.min.js"></script>
      
        <!-- lightcase -->
        <script src="/public/js/lightcase.js"></script>
      
        <!-- counterup -->
        <script src="/public/js/jquery.counterup.min.js"></script>
      
        <!-- Swiper -->
        <script src="/public/js/swiper.jquery.min.js"></script>

        <!--progress-->
        <script src="/public/js/circle-progress.min.js"></script>

        <!--nstSlider-->
        <script src="/public/js/jquery.nstSlider.js"></script>

        <!--flexslider-->
        <script src="/public/js/flexslider-min.js"></script>

        <!-- custom -->
        <script src="/public/js/custom.js"></script>

        <!-- 31-07-2020 -->
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        
        <!--<script src="<?php echo e(url('public')); ?>/backEnd/vendors/js/bootstrap.min.js"></script>-->
        <script src="<?php echo e(url('public')); ?>/backEnd/vendors/js/moment.min.js"></script>
        <script src="<?php echo e(url('public')); ?>/backEnd/vendors/js/print/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php echo e(url('public')); ?>/backEnd/vendors/js/bootstrap-datepicker.min.js"></script>
        
        <script src="<?php echo e(url('public')); ?>/js/historys.js"></script>
        <!--<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDs3mrTgrYd6_hJS50x4Sha1lPtS2T-_JA"></script>-->
        <?php echo $__env->yieldPushContent('script'); ?>
        <?php echo $__env->yieldContent('script'); ?>
        <?php echo $__env->yieldContent('script_map'); ?>
        <script>
          function myFunction(x) {
            x.classList.toggle("change");
            $('.tablet-menu').toggle();
          }
           function showsidebar()
          {
            $('.main-menu').toggle();
            $("#phone_icon").toggleClass('rotated');
          }
          
        </script>

        <script type="text/javascript">
          $( document ).ready(function() {
                $('.has-drop-nav').mouseenter(function(){ 
                    // alert($(window).width());
                    count = $('> .list--drop-nav > li', this).length;
                    if($(window).width() <= 1350 && $(window).width() >= 1320){
                        $('> .list--drop-nav', this).css('height',count*30);
                    }
                    if($(window).width() <= 1320){
                        $('> .list--drop-nav', this).css('height',count*39);
                    }
                    if($(window).width() > 1351){
                        $('> .list--drop-nav', this).css('height',count*39);
                    }
                    
                    $('> .list--drop-nav', this).css('visibility','visible');
                    $('> .list--drop-nav', this).css('opacity','1');
                    $('> .list--drop-nav', this).css('transition','all .5s ease-in-out');
                    
                });
                $('.has-sub-menu').mouseenter(function(){ 
                    $(this).parent().css('overflow','visible');
                    count = $('> .list--drop-nav > li', this).length;
                    if($(window).width() <= 1350 && $(window).width() >= 1320){
                        $('> .list--drop-nav', this).css('height',count*30);
                    }
                    if($(window).width() <= 1320){
                        $('> .list--drop-nav', this).css('height',count*39);
                    }
                    if($(window).width() > 1351){
                        $('> .list--drop-nav', this).css('height',count*39);
                    }
                    
                    $('> .list--drop-nav', this).css('visibility','visible');
                    $('> .list--drop-nav', this).css('opacity','1');
                    $('> .list--drop-nav', this).css('transition','all .5s ease-in-out');
                    
                });
                $('.has-drop-nav , .has-sub-menu').mouseleave(function(){    
                    $(this).parent().css('overflow','');
                    $('> .list--drop-nav', this).css('height','0px');
                    $('> .list--drop-nav', this).css('visibility','hidden');
                    $('> .list--drop-nav', this).css('opacity','0');
                    $('> .list--drop-nav', this).css('transition','all .5s ease-in-out');
                });
                
                if($(window).width() > 600){
                        showsidebar();
                    }
                
                
                $("#visitday").datepicker({ format: "dd-mm-yyyy" }).val();
                $("#visitday_modal").datepicker({ format: "dd-mm-yyyy" }).val();
                
                $('#telephone1,#telephone2').keypress(function (event) {
                  if (event.which !== 8 && event.which !== 0 && event.which < 48 || event.which > 57) {
                    event.preventDefault();
                  }
                });
            });
        </script>
		
        <script src="<?php echo e(asset('/public/build/js/intlTelInput.min.js')); ?>"></script>
        <script src="<?php echo e(asset('/public/build/js/intlTelInput-jquery.min.js')); ?>"></script>
        
        <script>
            $("#telephone1").intlTelInput({
                preferredCountries: ["vn","gb","us","uk","cn","jp","kr","au","fr" ],
                separateDialCode:true,
            }).on('countrychange', function (e, countryData) {
            $("#dial1").val(($("#telephone1").intlTelInput("getSelectedCountryData").dialCode));
            $("#country1").val(($("#telephone1").intlTelInput("getSelectedCountryData").name));
            });

            $("#telephone2").intlTelInput({
                preferredCountries: ["vn","gb","us","uk","cn","jp","kr","au","fr" ],
                separateDialCode:true,
            }).on('countrychange', function (e, countryData) {
            $("#dial2").val(($("#telephone2").intlTelInput("getSelectedCountryData").dialCode));
            $("#country2").val(($("#telephone2").intlTelInput("getSelectedCountryData").name));
            });
            $('.fa-caret-down').on('click',function(e){
                 e.preventDefault();
            });
        </script>
        <script>
            var acc = document.getElementsByClassName("accordion");
            var i;
            
            for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                panel.style.display = "none";
                } else {
                panel.style.display = "block";
                }
            });
            } 
        </script>
        <script type="text/javascript">
            jQuery(function($){  
            /*$("#bt_close").on("click", function() {
                $(".social_group").addClass("hidden");
                $("#bt_open").show();
            });
            $("#bt_open").on("click", function() {
                $(this).hide();
                $(".social_group").show();
                $(".social_group").removeClass("hidden");
            });*/

            $(".social_group").hover(            
                    function() {
                        $(this).toggleClass('open');        
                    },
                    function() {
                        $(this).toggleClass('open');       
                    }
                );
            });  

            $( document ).ready(function() {    
                $("#bs-example-navbar-collapse-1 .dropdown-menu li.active").parent().parent().addClass('active');
            $('#menutop li .i_mobile_ex').click(function(){
                console.log('ok');
            });
            $("#dial1").val(($("#telephone1").intlTelInput("getSelectedCountryData").dialCode));
            $("#country1").val(($("#telephone1").intlTelInput("getSelectedCountryData").name));
            $("#dial2").val(($("#telephone2").intlTelInput("getSelectedCountryData").dialCode));
            $("#country2").val(($("#telephone2").intlTelInput("getSelectedCountryData").name));
            });
        </script>
        
        <script>
        
        
        $('.grid--3a.carousel').slick({
            dots: false,
            infinite: false,
            arrows: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        $('.grid--full.carousel').slick({
            dots: false,
            infinite: false,
            arrows: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
        });
        $('.grid--full-w-view.carousel .view .view-content').slick({
            dots: false,
            infinite: false,
            arrows: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
        });
        
        $(document).ready(function() {
            $("#flexiselDemo3").flexisel({
                visibleItems: 3,
                itemsToScroll: 1,
                autoPlay: {
                    enable: false,
                    interval: 5000,
                    pauseOnHover: true
                }
            });
            
            
        }(jQuery));
        $('.video_0').click(function(){
            $(".video_0").remove();
           $('.video-home').append('<iframe src="https://www.youtube.com/embed/Rp_toY4SqOg?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        });
        
        $(".video_1").click(function() {
            $(".video_1").remove();
            $(".wrapper_1").append('<iframe src="https://www.youtube.com/embed/fZrFZhrLZMQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        });
        
        $(".video_2").click(function() {
            $(".video_2").remove();
            $(".wrapper_2").append('<iframe src="https://www.youtube.com/embed/uY3Inbu-fQM"  frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        });
        
        $(".video_3").click(function() {
            $(".video_3").remove();
            $(".wrapper_3").append('<iframe src="https://www.youtube.com/embed/q6PTMsk3u9g"  frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        });

    </script>
<script>
/* Check the location of each element */
$('.content').each( function(i){
  
  var bottom_of_object= $(this).offset().top + $(this).outerHeight();
  var bottom_of_window = $(window).height();
  
  if( bottom_of_object > bottom_of_window){
    $(this).addClass('hidden');
  }
});


// $(window).scroll( function(){
//     /* Check the location of each element hidden */
//     $('.hidden').each( function(i){
      
//         var bottom_of_object = $(this).offset().top + $(this).outerHeight();
//         var bottom_of_window = $(window).scrollTop() + $(window).height();
      
//         /* If the object is completely visible in the window, fadeIn it */
//         if( bottom_of_window > bottom_of_object ){
//           $(this).animate({'opacity':'1'},700);
//         }
//     });
// });

<?php /**PATH D:\WWW\SGS\sgss\resources\views/frontEnd/home/layout/footer.blade.php ENDPATH**/ ?>