<div class="top-header">
	<h3 class="center text-header">PAYMENT</h3>
</div>

<div class="row">
	<div class="col-12">
		<div class="enrol-input">
			<input type="text" class="form-control" name="payment_name" placeholder="Contact name">
		</div>
	</div>
	<div class="col-12">
		<div class="enrol-input">
			<input type="text" class="form-control" name="payment_company" placeholder="Company name">
		</div>
	</div>
	<div class="col-12">
		<div class="enrol-input">
			<input type="text" class="form-control" name="payment_address" placeholder="Address">
		</div>
	</div>
	<div class="col-12">
		<div class="enrol-input">
			<input type="text" class="form-control" name="payment_email" placeholder="Email">
		</div>
	</div>
	<div class="col-12">
		<div class="enrol-input">
			<input type="text" class="form-control" name="payment_phone" placeholder="Phone number">
		</div>
	</div>
	<div class="col-12">
		<div class="gender-container">
			<div class="sm-content">Payment method</div>
			<div class="row checkbox-group">
				<div class="col-lg-3 col-md-3 col-sm-6">
					<input type="radio" name="payment_method" value="Cash" checked="checked"> Cash
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<input type="radio" name="payment_method" value="Checkque"> Cheque
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<input type="radio" name="payment_method" value="Bank transfer"> Bank transfer
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6">
					<input type="radio" name="payment_method" value="Bank transfer"> Credit card
				</div>
			</div>	
			<div class="sm-content">Do you require  a VAT invoice ? </div>
			<div class="row checkbox-group">
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="vat_invoice" value="yes_vat" > Yes
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<input type="radio" name="vat_invoice" value="no_vat" checked="checked"> No
				</div>
			</div>	
		</div>
	</div>
</div>
<?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/steps/step-5.blade.php ENDPATH**/ ?>