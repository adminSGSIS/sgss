
<?php $__env->startSection('main_content'); ?>

<div class="banner">
    	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>Working at Saigon Star </h3>
            </div>
    	</div>
    	
    </div>
<div class="c-color" >
<div class="container ">
        
	   <br>
        
        <br>

		<div class="col">
		   
            <div class="row">
            	<?php $__currentLoopData = $careers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $career): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-12 col-md-6 col-lg-4 item" >
                	<a href="view-career/<?php echo e($career->id); ?>" title="View Details">
                    <div class="card news-card" >
                    	<div class="ItemImage">
                        <img class="card-img-top news-card-img" src="<?php echo e(url('')); ?>/<?php echo e($career->image); ?>" alt="Card image cap">
                        </div>
                        <div class="card-body news-card-body">
                            <h5 class="card-title news-card-title"><?php echo e(Illuminate\Support\Str::limit($career->name,50)); ?></h5>
                           
                        </div>
                    </div>
                    </a>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>    
		</div>
</div>
<div class="container">
              <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/CHILDREN-01.png" alt=""> 
        </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/careers.blade.php ENDPATH**/ ?>