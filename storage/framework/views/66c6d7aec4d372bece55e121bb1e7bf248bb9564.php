
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
    <div class="banner">
    	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>curriculum</h3>
            </div>
    	</div>
    	
    </div>
    <div class="c-color">
        <div class="curriculum-page row">
            <div class="col-lg-3 col-md-12 col-sm-12 sub-menu">          
                <a href="/curriculum">OVERVIEW</a></br>
                <a onclick="event.preventDefault();" href="#">ACADEMIC PROGRAMS</a></br>
                <a class="pl-2 active" href="/early-year">EARLY YEARS</a></br>
                <a class="pl-2" href="/primary-year">PRIMARY YEARS</a>
            </div> 
            <div class="col-lg-9 col-md-12 col-sm-12">
                <h3>Early Years</h3>
                <p class="new-text-color">
                    The IEYC uses international best practices, holistic enquiry and play-based approaches
                    that cover all curriculum areas including personal, social and emotional development. </br></br>
                    The IEYC was designed in response to demand from schools/Early Years settings that were using the International
                    Primary Curriculum (IPC) and wanted to incorporate a self-sustained international curriculum for learners in the Early
                    Years age range of 2-5 years old which includes Nursery, Pre-school and Reception. </br></br>
                    At Saigon Star, we fully appreciate (and cannot overemphasise enough) the critical 
                    importance of the Early Years phase of development in a young child's life. </br></br>
                    These initial steps for children aged 2-5 years old are the 'building blocks' of their
                    educational career as a whole and we work creatively as a collaborative team, 
                    consistently reflecting on our ability to facilitate those engaging, motivating and memorable 
                    learning experiences that help support the initial, early growth.
                    We supplement the 'Development Matters' framework (developed by Early Education with support 
                    from the British Department for Education) with the International Early Years Curriculum (IEYC), 
                    which we strong believe provides our learners with a holistic approach, full of opportunities to 
                    stretch wonder and imagination within a nurturing, safe and caring learning environment. 
                    Our belief is that each child is motivated by different interests and by offering the IEYC, 
                    the play-based approaches ensure children remain motivated to learn and supported by trained,
                    experienced teachers when interventions are needed. </br></br>
                    Our teachers support our learners' progress by using the pedagogy of Phonics from the
                    UK and building this, and the practical application of Maths into their lesson planning, 
                    identifying and actioning next steps for the learners to demonstrate. We appreciate the value 
                    of the creative arts in ensuring a rich and balanced curriculum so PE, 
                    Swimming, Music, Dance and Computing are key.
                
                </p>
                <img width="80%" src="/public/images/pic4.png"></img>
                
                <div style="padding-bottom:10%" class="curriculum-link">
                    <br>
                    <a target="_blank" href="/public/tailieu/02_SSIS_WEB_CURRICULUM_EY.pdf">Download the 2020-2021 Early Year Program Overview Kit</a>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/early-year.blade.php ENDPATH**/ ?>