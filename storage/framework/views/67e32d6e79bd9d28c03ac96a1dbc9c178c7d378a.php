
<?php $__env->startSection('main_content'); ?>
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
    <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css"/> 
<?php $__env->stopPush(); ?>

<div class="banner">
	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>SGSIS TV</h3>
        </div>
	</div>
	
	
</div>
<div class="main_content" style="background:#fafaf4">
	<div class="container">
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner d-flex justify-content-center">
                <div class="col-lg-10 col-sm-12 carousel-item active">
                    <iframe  width="100%" class="sgsis-tv" src="https://www.youtube.com/embed/aeNCEH5dO0w"
                    frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; 
                    gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </div>
                
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" onclick="event.preventDefault();">
                    <span class="fa fa-chevron-left"  aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" onclick="event.preventDefault();">
                <span class="fa fa-chevron-right"  aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
        </div>
	</div>
	
	<div class="container">
		<img  width="100%" src="<?php echo e(asset('public/')); ?>/images/CHILDREN-01.png" alt="">
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/layout/sgstv.blade.php ENDPATH**/ ?>