<?php $__currentLoopData = $teacher_info; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teacher_info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="modal fade" style="background: rgb(0 0 0 / 60%);" id="exampleModal<?php echo e($teacher_info->user_id); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"><?php echo e($teacher_info->full_name); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="modal-images">    
       <img src="<?php echo e(asset($teacher_info->staff_photo)); ?>" alt="Mountains"> 
        </div>
        <div class="title-modal"><?php echo e($teacher_info->designations->title); ?></div>
        <div class="modal-content"> 
        <?php echo e($teacher_info->experience); ?>

       </div>            
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/partials/info-teachers.blade.php ENDPATH**/ ?>