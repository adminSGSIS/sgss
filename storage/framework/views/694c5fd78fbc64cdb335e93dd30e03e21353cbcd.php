
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/teacher.css" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>

    <!-- CSS Library-->
    <br><br><br><br><br><br>
    <!-- Style -->
    <div class="main-teacher">

        <h3 style="color:#08436b; text-align:center; padding-bottom:20px" >Our Teachers</h3>

        <div id="myBtnContainer" style="text-align:center">
            <button class="btn md-txt active" onclick="filterSelection('leadership')"> Leadership Team</button>
            <button class="btn md-txt" onclick="filterSelection('Early')"> Early years</button>
            <button class="btn md-txt" onclick="filterSelection('primary')"> Primary years</button>
            <!--<button class="btn md-txt" onclick="filterSelection('Middle')"> Middle years</button>-->
            <button class="btn md-txt" onclick="filterSelection('Specialist')"> Specialists</button>
        </div>

        <!-- Portfolio Gallery Grid -->
        <div class="row-people clearfix d-flex justify-content-center">
                        <?php $__currentLoopData = $leaderships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leadership): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($leadership->designation_id == 13): ?>
                        <div class="column-teacher leadership">
                            <div class="content-teacher">
                                <img src="<?php echo e(asset($leadership->staff_photo)); ?>" alt="Lights" style="width:100%">
                                <h4><?php echo e($leadership->full_name); ?></h4>
                                <p><?php echo e($leadership->designations->title); ?></p> 
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($leadership->user_id); ?>">
                                    Read more
                                </button>

                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $leaderships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leadership): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <?php if($leadership->designation_id == 7): ?>
                        <div class="column-teacher leadership">
                            <div class="content-teacher">
                                <img src="<?php echo e(asset($leadership->staff_photo)); ?>" alt="Lights" style="width:100%">
                                <h4><?php echo e($leadership->full_name); ?></h4>
                                <p><?php echo e($leadership->designations->title); ?></p> 
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($leadership->user_id); ?>">
                                    Read more
                                </button>

                            </div>
                        </div>
                        <?php endif; ?>
                        
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $leaderships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leadership): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($leadership->designation_id == 6): ?>
                        <div class="column-teacher leadership">
                            <div class="content-teacher">
                                <img src="<?php echo e(asset($leadership->staff_photo)); ?>" alt="Lights" style="width:100%">
                                <h4><?php echo e($leadership->full_name); ?></h4>
                                <p><?php echo e($leadership->designations->title); ?></p> 
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($leadership->user_id); ?>">
                                    Read more
                                </button>

                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                         <?php $__currentLoopData = $leaderships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leadership): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($leadership->designation_id == 5): ?>
                        <div class="column-teacher leadership">
                            <div class="content-teacher">
                                <img src="<?php echo e(asset($leadership->staff_photo)); ?>" alt="Lights" style="width:100%">
                                <h4><?php echo e($leadership->full_name); ?></h4>
                                <p><?php echo e($leadership->designations->title); ?></p> 
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($leadership->user_id); ?>">
                                    Read more
                                </button>

                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $primary_years_teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$primary_years_teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="column-teacher primary">
                            <div class="content-teacher">
                            <img src="<?php echo e(asset($primary_years_teacher->staff_photo)); ?>" alt="Car" style="width:100%">
                            <h4><?php echo e($primary_years_teacher->full_name); ?></h4>
                            <p><?php echo e($primary_years_teacher->designations->title); ?></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($primary_years_teacher->user_id); ?>">
                                Read more
                            </button>  
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   
                        <?php $__currentLoopData = $early_years_teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$early_years_teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key == 1): ?>
                        <div class="column-teacher Early">
                            <div class="content-teacher">
                            <img src="<?php echo e(asset($early_years_teacher->staff_photo)); ?>" alt="Car" style="width:100%">
                            <h4><?php echo e($early_years_teacher->full_name); ?></h4>
                            <p><?php echo e($early_years_teacher->designations->title); ?></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($early_years_teacher->user_id); ?>">
                                Read more
                            </button>  
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $early_years_teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$early_years_teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key == 2): ?>
                        <div class="column-teacher Early">
                            <div class="content-teacher">
                            <img src="<?php echo e(asset($early_years_teacher->staff_photo)); ?>" alt="Car" style="width:100%">
                            <h4><?php echo e($early_years_teacher->full_name); ?></h4>
                            <p><?php echo e($early_years_teacher->designations->title); ?></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($early_years_teacher->user_id); ?>">
                                Read more
                            </button>  
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $early_years_teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$early_years_teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key ==3): ?>
                        <div class="column-teacher Early">
                            <div class="content-teacher">
                            <img src="<?php echo e(asset($early_years_teacher->staff_photo)); ?>" alt="Car" style="width:100%">
                            <h4><?php echo e($early_years_teacher->full_name); ?></h4>
                            <p><?php echo e($early_years_teacher->designations->title); ?></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($early_years_teacher->user_id); ?>">
                                Read more
                            </button>  
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php $__currentLoopData = $early_years_teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$early_years_teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key == 0): ?>
                        <div class="column-teacher Early">
                            <div class="content-teacher">
                            <img src="<?php echo e(asset($early_years_teacher->staff_photo)); ?>" alt="Car" style="width:100%">
                            <h4><?php echo e($early_years_teacher->full_name); ?></h4>
                            <p><?php echo e($early_years_teacher->designations->title); ?></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($early_years_teacher->user_id); ?>">
                                Read more
                            </button>  
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                        <!--<?php $__currentLoopData = $middle_years_teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $middle_years_teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                        <!--<div class="column-teacher Middle">-->
                        <!--    <div class="content-teacher">-->
                        <!--    <img src="<?php echo e(asset($middle_years_teacher->staff_photo)); ?>" alt="Car" style="width:100%">-->
                        <!--    <h4><?php echo e($middle_years_teacher->full_name); ?></h4>-->
                        <!--    <p><?php echo e($middle_years_teacher->designations->title); ?></p>-->
                        <!--    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($middle_years_teacher->user_id); ?>">-->
                        <!--        Read more-->
                        <!--    </button>  -->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                        
                        <?php $__currentLoopData = $specialist_teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $specialist_teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="column-teacher Specialist">
                            <div class="content-teacher">
                            <img src="<?php echo e(asset($specialist_teacher->staff_photo)); ?>" alt="Car" style="width:100%">
                            <h4><?php echo e($specialist_teacher->full_name); ?></h4>
                            <p><?php echo e($specialist_teacher->designations->title); ?></p>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?php echo e($specialist_teacher->user_id); ?>">
                                Read more
                            </button>  
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <!-- END GRID -->
        
        </div>

    <?php echo $__env->make('frontEnd.home.partials.info-teachers', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        filterSelection("leadership")
        function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("column-teacher");
        
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
        }

        function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
        }
        }

        function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);     
            }
        }
        element.className = arr1.join(" ");
        }


        // Add active class to the current button (highlight it)
        var btnContainer = document.getElementById("myBtnContainer");
        var btns = btnContainer.getElementsByClassName("btn");
        for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function(){
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
        }
    </script>

    <script type="text/javascript">
        jQuery(function($){  
        $("#bt_close").on("click", function() {
            $(".social_group").addClass("hidden");
            $("#bt_open").show();
        });
        $("#bt_open").on("click", function() {
            $(this).hide();
            $(".social_group").show();
            $(".social_group").removeClass("hidden");
        });

        $(".social_group").hover(            
                function() {
                    $(this).toggleClass('open');        
                },
                function() {
                    $(this).toggleClass('open');       
                }
            );
        });  

        $( document ).ready(function() {    
            $("#bs-example-navbar-collapse-1 .dropdown-menu li.active").parent().parent().addClass('active');
        $('#menutop li .i_mobile_ex').click(function(){
            console.log('ok');
        })
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/teachers.blade.php ENDPATH**/ ?>