
<?php $__env->startSection('mainContent'); ?>
<div class="col-lg-12">
        <form method="POST" action="<?php echo e(url('feescollection/list-invoice-search')); ?>">
            <?php echo csrf_field(); ?>
                <div class="white-box">
                    <div class="row">
                        <div class="col-lg-4 sm_mb_20 sm2_mb_20 md_mb_20">
                            <select class="niceSelect w-100 bb" name="status"> 
                                <option value="" data-display="Select Status">Select Status</option>
                                <option value="0">Unpaid</option>
                                <option value="1">Paid</option>
                                <option value="2">Deposit</option>
                            </select>
                    </div>
                        <div class="col-lg-4">
                            <div class="input-effect sm_mb_20 sm2_mb_20 md_mb_20">
                                <select class="niceSelect w-100 bb" name="academic">
                                <option value="" data-display="Select Academic Year">Select Academic Year</option>
                                <?php $__currentLoopData = $academics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $academic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($academic->id); ?>"><?php echo e($academic->year); ?>[<?php echo e($academic->title); ?>]</option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 mt-20 text-right">
                            <button type="submit" class="primary-btn small fix-gr-bg" id="btnsubmit">
                                <span class="ti-search pr-2"></span>
                                Search
                            </button>
                        </div>
                    </div>
                    </div>
        </form>
                </div>
        
<br><br>
<table id="table_id" class="display school-table dataTable no-footer dtr-inline">
	<thead>
		<tr>
			<th>Student name</th>
			<th>amount(vnd)</th>
			<th>discount(vnd)</th>
			<th>total(vnd)</th>
			<th>payment status</th>
			<th>Next due date</th>
			<th>action</th>
		</tr>
	</thead>
	<tbody>
		<?php $__currentLoopData = $slips; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr role="row">
			<td><?php echo e($slip->student->full_name); ?></td>
			<td><?php echo e(number_format($slip->amount)); ?></td>
			<td><?php echo e(number_format($slip->discount)); ?></td>
			<td><?php echo e(number_format($slip->amount_applied_discount)); ?></td>
			<td>
				<?php $__currentLoopData = $slip->payment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($payment->status == 1): ?>
				<button class="btn btn-success">Paid</button>
				<?php elseif($payment->status == 0): ?>
				<button class="btn btn-danger" style="font-size: 13px;">Unpaid</button>
				<?php elseif($payment->status == 2): ?>
				<button class="btn btn-warning" style="font-size: 13px;">Deposit</button>
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</td>
			<td>

				<?php $__currentLoopData = $slip->payment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($payment->status == 0 || $payment->status == 2): ?>
					<?php 
					$date = Carbon\Carbon::parse($payment->due_date);
					$now  = Carbon\Carbon::now();
					
					?>
					<?php if($date->diffInDays($now) >= 21): ?>
					    <?php if($date->gte($now) == true): ?>
					    <button class="btn btn-success" style="font-size: 13px;"><?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')); ?></button>
					    <?php else: ?>
					    <button class="btn btn-warning" style="font-size: 13px;"><?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')); ?></button>
					    <?php endif; ?>
					<?php elseif($date->diffInDays($now) >= 7 && $date->diffInDays($now) <=21 ): ?>
					    <?php if($date->gte($now) == true): ?>
					    <button class="btn btn-warning" style="font-size: 13px;"><?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')); ?></button>
					    <?php else: ?>
					    <button class="btn btn-warning" style="font-size: 13px;"><?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')); ?></button>
					    <?php endif; ?>
					
					<?php elseif(($date->diffInDays($now) <= 7)): ?>
					    <?php if($date->gte($now) == true): ?>
					    <button class="btn btn-danger" style="font-size: 13px;"><?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')); ?></button>
					    <?php else: ?>
					    <button class="btn btn-warning" style="font-size: 13px;"><?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')); ?></button>
					    <?php endif; ?>
					<?php endif; ?>
					<?php break; ?>
				<?php elseif($payment->status == 1): ?>
					<?php continue; ?>
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</td>
			<td>
				<div class="dropdown">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                        <?php echo app('translator')->get('lang.select'); ?>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="<?php echo e(url('feescollection/view-slip')); ?>/<?php echo e($slip->id); ?>"><?php echo app('translator')->get('lang.view'); ?></a>
                        <a class="dropdown-item" href="<?php echo e(url('feescollection/send-notifications')); ?>/<?php echo e($slip->id); ?>"><?php echo app('translator')->get('send notice'); ?></a>
                    </div>
                </div>
			</td>
		</tr>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</tbody>
</table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/list-slip.blade.php ENDPATH**/ ?>