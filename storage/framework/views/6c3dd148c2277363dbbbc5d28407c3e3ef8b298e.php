
<?php $__env->startSection('mainContent'); ?>
<div class="mb-5">
    <form method="Get" action="<?php echo e(route('healthMonitoringCreate')); ?>">
        <?php echo csrf_field(); ?>
        <div class="white-box">
            <h2 class="center" style="text-align: center;">Health Monitoring</h2>
            <div class="container">
                <div id="studentBoard">
                    <div class="pad-top-20">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <select name="student_id" id="student_list"
                                    class="niceSelect w-100 bb form-control<?php echo e(@$errors->has('students') ? ' is-invalid' : ''); ?>">
                                    <option data-display="<?php echo app('translator')->get('student name'); ?> *" value=""><?php echo app('translator')->get('student name'); ?> *</option>
                                    <?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if(isset($add_income)): ?>
                                    <option value="<?php echo e(@$student->id); ?>"
                                        <?php echo e(@$add_income->student_id == @$student->id? 'selected': ''); ?>>
                                        <?php echo e(@$student->full_name); ?> | <?php echo e(@$student->admission_no); ?></option>
                                    <?php else: ?>
                                    <option label="<?php echo e(@$student->admission_no); ?>" value="<?php echo e(@$student->id); ?>"
                                        <?php echo e(old('student_list') == @$student->id? 'selected' : ''); ?>><?php echo e(@$student->full_name); ?> |
                                        <?php echo e(@$student->admission_no); ?></option>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12"></div>
                </div>
                <div class="d-flex justify-content-end" style="padding-top: 35px">
                    <input type="submit" class="primary-btn small fix-gr-bg" value="search">
                </div>
            </div>
        </div>
    </form>
</div>

<div>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="container-fluid p-0">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3><?php echo app('translator')->get('Health Monitoring'); ?></h3>
                        </div>
                    </div>
                </div>
    
                <div class="row">
                    <div class="col-lg-12">
    
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
    
                            <thead>
                                <?php if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(session()->get('message-success-delete')); ?>

                                        </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                        <div class="alert alert-danger">
                                            <?php echo e(session()->get('message-danger-delete')); ?>

                                        </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <th><?php echo app('translator')->get('No'); ?></th>
                                    <th><?php echo app('translator')->get('Name'); ?></th>
                                    <th><?php echo app('translator')->get('Class'); ?></th>
                                    <th><?php echo app('translator')->get('Update time'); ?></th>
                                    <th><?php echo app('translator')->get('action'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $healthMonitorings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $healthMonitoring): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($healthMonitoring->id); ?></td>
                                    <td><?php echo e($healthMonitoring->student_name); ?></td>
                                    <td><?php echo e($healthMonitoring->className->class_name); ?></td>
                                    <td>
                                        <?php
                                            $healthUpdate=$healthMonitoring->updated_at;
                                            $trialExpires = $healthUpdate->addMonth(6);
                                            $now=Carbon::now();
                                        ?>
                                        <?php if($trialExpires->lt($now)): ?>
                                        <button class="btn btn-danger" style="font-size: 13px;">Updated Now</button>
                                        <?php else: ?>
                                        <button class="btn btn-success" style="font-size: 13px;"><?php echo e(date('d-m-Y', strtotime($trialExpires))); ?></button>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                <?php echo app('translator')->get('lang.select'); ?>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a title="View/Edit Leave Details"
                                                    class="dropdown-item" href="<?php echo e(url('medical/healthMonitoring-edit/'.$healthMonitoring->student_id)); ?>"><?php echo app('translator')->get('Detail'); ?></a>
                                                    <a title="View/Edit Leave Details" target="_blank"
                                                    class="dropdown-item" href="<?php echo e(url('medical/healthMonitoring-PDF/'.$healthMonitoring->id.'/'.$healthMonitoring->student_id)); ?>"><?php echo app('translator')->get('Export PDF'); ?></a>
                                                <a class="dropdown-item" data-toggle="modal"
                                                    data-target="#DeleteHealthMonitoring<?php echo e($healthMonitoring->id); ?>"
                                                    href="#"><?php echo app('translator')->get('lang.delete'); ?></a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade admin-query" id="DeleteHealthMonitoring<?php echo e($healthMonitoring->id); ?>">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title"><?php echo app('translator')->get('lang.delete'); ?> <?php echo app('translator')->get('lang.item'); ?></h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <h4><?php echo app('translator')->get('lang.are_you_sure_to_delete'); ?></h4>
                                                </div>
    
                                                <div class="mt-40 d-flex justify-content-between">
                                                    <button type="button" class="primary-btn tr-bg"
                                                        data-dismiss="modal"><?php echo app('translator')->get('lang.cancel'); ?></button>
                                                    <?php echo e(Form::open(['url' => 'medical/healthMonitoring-delete/'.$healthMonitoring->student_id, 'method' => 'DELETE', 'enctype' => 'multipart/form-data'])); ?>

                                                    <button class="primary-btn fix-gr-bg"
                                                        type="submit"><?php echo app('translator')->get('lang.delete'); ?></button>
                                                    <?php echo e(Form::close()); ?>

                                                </div>
                                            </div>
    
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/Medical\Resources/views/health-monitoring.blade.php ENDPATH**/ ?>