<style type="text/css">
    body {
  background-color: #74EBD5;
  background-image: linear-gradient(90deg, #74EBD5 0%, #9FACE6 100%);

  min-height: 100vh;
}

::-webkit-scrollbar {
  width: 5px;
}

::-webkit-scrollbar-track {
  width: 5px;
  background: #f5f5f5;
}

::-webkit-scrollbar-thumb {
  width: 1em;
  background-color: #ddd;
  outline: 1px solid slategrey;
  border-radius: 1rem;
}

.text-small {
  font-size: 0.9rem;
}

.messages-box2{
    height: 555px;
    overflow-y: scroll;

}
.chat-box2 {
  
  height: 503px;
  overflow-y: scroll;
}

.rounded-lg {
  border-radius: 0.5rem;
}

input::placeholder {
  font-size: 0.9rem;
  color: #999;
}
.minibox{
    height: 600px;
    width: 800px;
    display: none;
    position: fixed;
    bottom: 30px;
    right: 100px;
    z-index: 9999;
}
.loading-message{
    width: 100%;
    height: 510px;
    background-color: #e8e8e8;
    position: absolute;
    z-index: 9999;
}


</style>
<a target="_blank" href="<?php echo e(url('/chat')); ?>" id="message_box" style="position: fixed; bottom: 10px;right: 10px; z-index : 99999; " ><img id="message-icon" src="<?php echo e(url('Modules/ChatBox/public/message.jpg')); ?>" width="60px"></a>

<?php /**PATH D:\WWW\SGS\sgss\Modules/ChatBox\Resources/views/minibox.blade.php ENDPATH**/ ?>