
<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('public/frontend/css/course.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_content'); ?>
<img width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an.jpeg" alt="">

<section class="container news-lastest">
    
    

<!-- Modal -->

    <h4 class="mt-4 titleNews"><a href="<?php echo e(url('public/uploads/THE SAIGON JOURNAL_10112020.pdf')); ?>"target="_blank">THE SAIGON JOURNAL (click to open)</a></h4>
    
    <br>
    <div style="margin-left: 20px" class="col nen">
        <div class="row">
            <?php if($hotnews): ?>
                <div class="col-lg-5 item">
                    <a href="<?php echo e(url('news-details/'.$hotnews->id)); ?>" title="View Details">
                    <div class="card news-card h-100" >
                        <div class="zoom img-hover-zoom">
                            <img class="card-img-top" src="<?php echo e(asset($hotnews->image)); ?>"  alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h5><?php echo e(App\SmGeneralSettings::DateConvater($hotnews->publish_date)); ?></h5>
                                <h4><?php echo e(\Illuminate\Support\Str::limit($hotnews->news_title,70)); ?></h4>
                            </div>
                            <br>
                            <div class="card-description">
                                <?php if(strlen($hotnews->description) > 10): ?>
                                    <p class="card-text"><?php echo e(\Illuminate\Support\Str::limit($hotnews->description,150)); ?></p>
                                <?php else: ?>
                                    <a href="<?php echo e(url('news-details/'.$hotnews->id)); ?>">Read More</a>
                                <?php endif; ?>
                            </div>                
                        </div>
                    </div>
                    </a>
                </div>

                <div class="col-lg-7">
                    <div class="row">
                        <?php $__currentLoopData = $newslist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-6 item" >
                            <a href="<?php echo e(url('news-details/'.$news->id)); ?>" title="View Details">
                            <div class="card news-card h-100" >
                                <div class="zoom img-hover-zoom">
                                <img class="card-img-top" src="<?php echo e(asset($news->image)); ?>" height="200px" width="200px" alt="Card image cap">
                                </div>
                                <div class="card-body">
                                    <div class="card-title">
                                        <h5><?php echo e(App\SmGeneralSettings::DateConvater($news->publish_date)); ?></h5>
                                        <h4><?php echo e(\Illuminate\Support\Str::limit($news->news_title,30)); ?></h4>
                                    </div>                      
                                </div>
                                
                            </div>
                            </a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <br>
    
</section>
<section class="student-details section-gap-top mt-4">
    <div class="d-flex justify-content-center">
        <div style="display: none;"  class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown button
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" onclick="removeActive()"  href="#all" role="tab" data-toggle="tab">All</a>
                <?php $__currentLoopData = $list_cate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a class="dropdown-item" onclick="removeActive()" href="#id<?php echo e($cate->id); ?>" role="tab" data-toggle="tab"><?php echo e($cate->sub_category_name); ?></a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <ul class="nav nav-tabs mb-3" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#all" role="tab" data-toggle="tab">All</a>
            </li>
            <?php $__currentLoopData = $list_cate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="nav-item">
                    <a class="nav-link" href="#id<?php echo e($cate->id); ?>" <?php if($cate->id == 38): ?> id="bullentin" <?php endif; ?> role="tab" data-toggle="tab"><?php echo e($cate->sub_category_name); ?></a>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>

    <div class="tab-content">
        <!-- Start Overview Tab -->
        <div role="tabpanel" class="tab-pane fade show active" id="all">
            <?php $__currentLoopData = $list_cate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php
                    $latest_news = App\SmNews::where('sub_category_id', $cate->id)->orderBy('updated_at', 'desc')->take(3)->get();
                ?>
                <?php if(count($latest_news) > 0): ?>
                    <h1 style="text-align:center; color: cadetblue;"><?php echo e($cate->sub_category_name); ?></h1>
                <?php endif; ?>
                <div class="row" style="width: 90%; margin: auto">
                    <?php $__currentLoopData = $latest_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$lastest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                 
                        <div class="col-lg-4 col-sm-6 col-12 item">
                            <a href="<?php echo e(url('news-details/'.$lastest->id)); ?>" title="View Details">
                                <div style="height: 500px; "  class="card news-card" >
                                    <div class="zoom img-hover-zoom">
                                    <img class="card-img-top" src="<?php echo e(asset($lastest->image)); ?>" height="auto" width="auto" alt="Card image cap">
                                    </div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h5><?php echo e(App\SmGeneralSettings::DateConvater($lastest->publish_date)); ?></h5>
                                            <h4><?php echo e(\Illuminate\Support\Str::limit($lastest->news_title,70)); ?></h4>
                                        </div>      
                                        <br>
                                        <div class="card-description">
                                            <?php if(strlen($lastest->description) > 10): ?>
                                                <p class="card-text"><?php echo e(\Illuminate\Support\Str::limit($lastest->description,150)); ?></p>
                                            <?php else: ?>
                                                <a href="<?php echo e(url('news-details/'.$lastest->id)); ?>">Read More</a>
                                            <?php endif; ?>
                                        </div>    
                                    </div>
                                </div>
                            </a>
                        </div>                                      
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>

        <?php $__currentLoopData = $list_cate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div role="tabpanel" class="tab-pane fade" id="id<?php echo e($cate->id); ?>">
                <div class="row" style="width: 90%; margin: auto">
                    <?php $__currentLoopData = App\SmNews::where('sub_category_id', $cate->id)->orderBy('updated_at', 'desc')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lastest): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                 
                        <div class="col-lg-4 col-sm-6 col-12 item">
                            <a href="<?php echo e(url('news-details/'.$lastest->id)); ?>" title="View Details">
                                <div style="height: 500px; "  class="card news-card" >
                                    <div class="zoom img-hover-zoom">
                                    <img class="card-img-top" src="<?php echo e(asset($lastest->image)); ?>" height="auto" width="auto" alt="Card image cap">
                                    </div>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h5><?php echo e(App\SmGeneralSettings::DateConvater($lastest->publish_date)); ?></h5>
                                            <h4><?php echo e(\Illuminate\Support\Str::limit($lastest->news_title,70)); ?></h4>
                                        </div>      
                                        <br>
                                        <div class="card-description">
                                            <?php if(strlen($lastest->description) > 10): ?>
                                                <p class="card-text"><?php echo e(\Illuminate\Support\Str::limit($lastest->description,150)); ?></p>
                                            <?php else: ?>
                                                <a href="<?php echo e(url('news-details/'.$lastest->id)); ?>">Read More</a>
                                            <?php endif; ?>
                                        </div>    
                                    </div>
                                </div>
                            </a>
                        </div>                                      
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </div>
</section>

<script>
    if(window.location.href.indexOf('/bulletin') != -1){
        for(var i = 0; i <  document.getElementsByClassName('nav-link').length; i++){
            document.getElementsByClassName('nav-link')[i].className = document.getElementsByClassName('nav-link')[i].className.replace(" active", "");
        }
        for(var i = 0; i <  document.getElementsByClassName('tab-pane').length; i++){
            document.getElementsByClassName('tab-pane')[i].className = document.getElementsByClassName('tab-pane')[i].className.replace(" show active", "");
        }
        document.getElementById('bullentin').className += " active";
        document.getElementById('id38').className += " show active";
    }
    function removeActive(){
        for(var i = 0; i <  document.getElementsByClassName('dropdown-item').length; i++){
            document.getElementsByClassName('dropdown-item')[i].className = "dropdown-item";
        }
    }   
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/light_news_category.blade.php ENDPATH**/ ?>