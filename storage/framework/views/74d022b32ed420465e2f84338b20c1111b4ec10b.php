 
 
 <?php $__env->startSection('css'); ?>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 <style>
    .centerTd td, .centerTh th{
        text-align: center;
    }
    .removeOutline:focus {
        box-shadow: none;
    }
    .removeHover:hover{
        background: none;
    }
    .removeHover{
        display: inline;
        margin-left: -25px;
    }
 </style>
 <?php $__env->stopSection(); ?>
 <?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
     <div class="container-fluid">
         <div class="row justify-content-between">
             <h1>Tasks Management</h1>
             <div class="bc-pages">
                 <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
             </div>
         </div>
     </div>
 </section>
 <section class="admin-visitor-area up_st_admin_visitor">
     <div class="container-fluid p-0">
         <div class="row">
             <div class="col-lg-12">
                 <table id="table_id" class="display school-table" cellspacing="0" width="100%">
 
                     <thead>
                         <?php if(session()->has('message-success-delete') != "" ||
                                 session()->get('message-danger-delete') != ""): ?>
                                 <tr>
                                     <td colspan="6">
                                          <?php if(session()->has('message-success-delete')): ?>
                                           <div class="alert alert-success">
                                               <?php echo e(session()->get('message-success-delete')); ?>

                                           </div>
                                         <?php elseif(session()->has('message-danger-delete')): ?>
                                           <div class="alert alert-danger">
                                               <?php echo e(session()->get('message-danger-delete')); ?>

                                           </div>
                                         <?php endif; ?>
                                     </td>
                                 </tr>
                              <?php endif; ?>
                         <tr class="centerTh">
                             <th>Tasks Name</th>
                             <th>Priority</th>
                             <th>Tasks Type</th>
                             <th>Created By</th>
                             <th><?php echo app('translator')->get('lang.action'); ?></th>
                         </tr>
                     </thead>
 
                     <tbody>
                         <?php if(isset($filterTask)): ?>
                         <?php $__currentLoopData = $filterTask; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <tr class="centerTd">
                             <td><?php echo e($value->name_of_task); ?></td>
                             <td <?php if($value->priority == "high"): ?> style="color: red" <?php endif; ?>><?php echo e($value->priority); ?></td>
                             <td>
                                <button class="<?php echo e(Modules\CRM\Entities\SmTaskType::find($value->type_id)->color); ?> removeOutline" style="font-size: 11px;">
                                    <?php echo e(Modules\CRM\Entities\SmTaskType::find($value->type_id)->title); ?>

                                </button>  
                            </td>                              
                             <td><?php echo e(App\SmStaff::find($value->created_by)->full_name); ?></td>
                             <td>
                                 <div class="dropdown">
                                     <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                         <?php echo app('translator')->get('lang.select'); ?>
                                     </button>
                                     <div class="dropdown-menu dropdown-menu-right">

                                        <a class="dropdown-item" href="<?php echo e(url('tasks/'.$value->id)); ?>"><?php echo app('translator')->get('lang.view'); ?></a>
                                    
                                        <?php if(Auth::user()->role_id == 1 || $value->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id): ?>
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Unable Task" href="<?php echo e(url('tasks/'.$value->id.'/unable-view')); ?>">unable</a>
                                        <?php endif; ?>
                                        
                                        <?php if(Auth::user()->role_id == 1 || $value->created_by == App\SmStaff::where('email', Auth::user()->email)->first()->id): ?>
                                            <a class="deleteUrl dropdown-item" data-modal-size="modal-md" title="Delete Task" href="<?php echo e(url('tasks/'.$value->id.'/delete-view')); ?>">delete</a>
                                        <?php endif; ?>
                                     </div>
                                 </div>
                             </td>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                         <?php endif; ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>
 </div>
 </section>
 
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/CRM/Resources/views/TasksManagement/disableTask.blade.php ENDPATH**/ ?>