<?php
$checked_0 = false;
$checked_1 = false;
?>
<?php if(old('health_permissions')): ?>
<?php $__currentLoopData = old('health_permissions'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($i == "0"): ?>
<?php
$checked_0 = true
?>
<?php endif; ?>

<?php if($i == "1"): ?>
<?php
$checked_1 = true
?>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<div class="row mt-30">
  <div class="col-lg-12">
    <fieldset>
      <small style="color: #DC3545;"><?php echo e($errors->first('health_permissions')); ?></small>
      <div>
        <input type="checkbox" id="health1" name="health_permissions[]" value="0" <?php if($checked_0): ?> checked <?php endif; ?>>
        <label for="health1">I DO NOT consent to the school nurse administering over-the-counter medications for symptom relief of minor illnesses</label>
      </div>
      <div>
        <input type="checkbox" id="health2" name="health_permissions[]" value="1" <?php if($checked_1): ?> checked <?php endif; ?>>
        <label for="health2">I DO NOT consent to emergency hospital treatment for my child</label>
      </div>
    </fieldset>
  </div>
</div>

<?php
$checked_0 = false;
$checked_1 = false;
?>
<?php if(old('other_permissions')): ?>
<?php $__currentLoopData = old('other_permissions'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<?php if($i == "0"): ?>
<?php
$checked_0 = true
?>
<?php endif; ?>
<?php if($i == "1"): ?>
<?php
$checked_1 = true
?>
<?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<div class="row">
  <div class="col-lg-12">
    <fieldset>
      <small style="color: #DC3545;"><?php echo e($errors->first('other_permissions')); ?></small>
      <div>
        <input type="checkbox" id="permissions0" name="other_permissions[]" value="0" <?php if($checked_0): ?> checked <?php endif; ?>>
        <label for="permissions0">Please tick if you DO NOT consent to group photographs of your child being used for school advertising and marketing purposes</label>
      </div>
      <div>
        <input type="checkbox" id="permissions1" name="other_permissions[]" value="1" <?php if($checked_1): ?> checked <?php endif; ?>>
        <label for="permissions1">Please tick if you DO NOT consent to joining a class parents What'sApp group (to learn about birthday parties etc)</label>
      </div>
    </fieldset>
  </div>
</div>
<?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/forms/step5.blade.php ENDPATH**/ ?>