<?php $page_title="All about Infix School management system; School management software"; ?>

<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public')); ?>/frontEnd/css/new_style.css"/>
    
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>

    <img width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an.jpeg" alt="">
    <!--================ End Facts Area =================-->

    <!--================ Tran Thanh Phu start =================-->
    <div class="header">
        <h4>Home / About us</h4>
    </div>
    <div class="wrapper">
        <h3 class="mission">1. Our Mission</h3>
        <div class="missionContent">
            <i class="fa fa-quote-left mr-5" aria-hidden="true"></i>
            <h3>
                <span style="font-family: Arial, Helvetica, sans-serif;">
                    <span style="color: rgb(22, 160, 133);">"</span>
                </span>
                To provide a safe and supportive learning environment, and every opportunity for children to be successful
                both as learners and as contributors in a changing world
                <span style="font-family: Arial, Helvetica, sans-serif;">
                    <span style="color: rgb(22, 160, 133);">"</span>
                </span>
            </h3>
        </div>
        <h3 class="mission mt-3">2. Our Shared Vision & Mission Explained</h3>
        <div class="ourShared">
            <h4>Responsible global citizens</h4>
            <div class="image">
                <img src="public/images/vision-150x150.jpg" alt="">
            </div>
            <p><?php echo $cate_post->news_body; ?></p>
        </div>
    </div>
    <!--================ Tran Thanh Phu end =================-->
    
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/light_about.blade.php ENDPATH**/ ?>