<!--domestic-->

<?php $__env->startSection('main_content'); ?>
    <br><br><br><br>
    <section class="container mt-5">
        <div class="text-center">
            <h3>MEDICAL DECLARATION INFORMATION</h3>
            <h4><?php echo e($domestic->full_name); ?></h4>
            <div style="color: green;" class="mb-2">Declared information was created at <?php echo e($domestic->created_at->format('d-m-Y')); ?></div>
            <p class="mb-0">Please take a screenshot for proof</p>
            <div>
                <?php echo e(SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)
                    ->generate(url('admin/medical-declaration/domestic/details') . '/'. $domestic->id)); ?>

            </div>
            <a href="/">
                <button class="btn btn-primary mt-3">GO HOME</button>
            </a>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/declaration_success_domestic.blade.php ENDPATH**/ ?>