<?php $__env->startPush('css'); ?>
    <style>
        input {
            border: none;
            width: 88%;
        }

        #sig-canvas {
            border: 2px dotted #cccccc;
            border-radius: 15px;
            cursor: pointer;
            touch-action: none;
        }
        
        .box-title{
        	border-radius: 15px;
        	background: #7dd3f7;
        }
        
        .box-title h2{
        	color: #191970;
        	margin: 0;
            padding: 0.5rem;
        }
        
        .content-policy p{
        	text-align: justify;
        	padding-right: 2.5rem;
        	padding-left: 2.5rem;
        	font-family: 'Roboto';
            font-size: 1.2em;
            padding-bottom: 0;
        }
        
        
        .content-policy h3{
        	color: #191970;
            font-family: 'Roboto';
            font-size: 1.6em;
        }

    </style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
    <br>
    <div class="banner">
        <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
    </div>
    <div class="main_content c-color">
        <div class="container">
            <div class="d-flex justify-content-center">
            <img  width="100%" src="<?php echo e(asset('public/header_letter.png')); ?>" alt="">
            </div>
            <div class="d-flex justify-content-center mt-5 box-title mb-5">
                <h2>Acceptable Use Policy for Parents/Carers</h2>
            </div>
            <div class="content-policy">
                <p>1. I know that my child will be provided with internet access and will use a range of IT
                    systems in order to access the curriculum and be prepared for modern life whilst at Saigon
                    Star International School.
                </p>
                <p>2. I am aware that learners' use of mobile technology, such as mobile phones or smart
                    watches, is not permitted whilst on school premises.
                </p>
                <p>3. I am aware that any internet and technology use using Saigon Star equipment may
                    be monitored for safety and security reasons, to safeguard both my child and the school
                    systems. This monitoring will take place in accordance with data protection (including
                    GDPR) and human rights legislation.
                </p>
                <p>4. I understand that the school will take every reasonable precaution, including
                    monitoring and filtering systems, to ensure that learners are safe when they use the
                    internet and systems. I understand that the school cannot ultimately be held responsible
                    for the nature and content of materials accessed on the internet and using mobile
                    technologies.
                </p>
                <p>5. I understand that my child needs a safe and appropriate place to access remote
                    learning if Saigon Star is closed in response to Covid-19 or for any other reason. I will
                    ensure my child’s access to remote learning is appropriately supervised. I will ensure that
                    remote learning is treated the same way as onsite learning with children adequately
                    prepared, in an appropriate location (e.g. not in bed), suitably dressed and not given food
                    or be distracted otherwise, during the course of any lesson or online activity.
                </p>
                <p>6. I am aware that my child will receive online safety education to help them
                    understand the importance of safe use of technology and the internet, both in and out of
                    Saigon Star but that <a class="font-weight-bold"> raising digitally responsible and safe learners, is
                        the job of both
                        adults at school and at home.</a>
                </p>
                <p>7. I have read and discussed the school’s learner Acceptable Use of Technology Policy
                    (AUP) with my child.
                </p>
                <p>8. I know I can seek support from the school about online safety, such as via the school
                    website or through other recommended resources (such as <a href="https://www.thinkuknow.co.uk"
                        target="_blank" class="text-primary">www.thinkuknow.co.uk</a>) to help
                    keep my child safe online at home.
                </p>
                <p>9. I will support the school’s approach to online safety. I will role model safe and
                    positive online behaviour for my child by sharing images, text, and video online responsibly.
                    I know that some social media platforms and apps have age restrictions for good reasons
                    and understand the risks attached if my child does not follow these guidelines.
                </p>
                <p>10. I, together with my child, will not deliberately upload or add any images, video,
                    sounds or text that could upset, threaten the safety of or offend any member of the school
                    community. If I upload any videos or photos of other pupils from Saigon Star, I will ensure I
                    have obtained permission from the parent/carer beforehand (this does not apply to long
                    shots of live performances/concerts).
                </p>
                <p>11. I understand that if I or my child do not abide by the AUP, appropriate action will be
                    taken. This could include sanctions being applied in line with any policies and if a criminal
                    offence has been committed, it will be processed according to the law.
                </p>
                <p>12. I know that I can speak to the Designated Safeguarding Lead <a class="font-weight-bold">(James
                        Quantrilll)</a>, the
                    Deputy Designated Safeguarding Lead, <a class="font-weight-bold">(Neil Jones)</a>, or my child’s class
                    teacher if I have
                    any concerns about online safety. I understand I will be contacted if Saigon Star has any
                    concerns about either my or my child’s online behaviour.
                </p>
            </div>
            <div class="mt-5 content-policy">
                <h3>I have read, understood and agreed to comply with the Saigon Star Parent/Carer
                    Acceptable Use Policy.
                </h3>
            </div>

            <form action="<?php echo e(url('acceptable-use-policy-for-parents')); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <div class="row p-3">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="child_name">Child’s name</label>
                            <input type="text" class="form-control" id="child_name" required name="child_name"
                                value="<?php echo e(old('child_name')); ?>">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Class</label>
                            <select class="custom-select" name="class_id">
                                <option selected value="">Choose Class</option>
                                <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($item->id); ?>"
                                        <?php echo e(old('class_id') == $item->id ? 'checked' : ''); ?>>
                                        <?php echo e($item->class_name); ?>

                                    </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="parent_name">Parent/Carers Name</label>
                            <input type="text" class="form-control" id="parent_name" name="parent_name" required
                                value="<?php echo e(old('parent_name')); ?>">
                        </div>
                    </div>

                    <div class="col-12 mt-4">
                        <span>Parent/Carers Signature</span>
                        <div class="row">
                            <div class="col-md-8">
                                <canvas id="sig-canvas" class="mt-2" width="620" height="160">

                                </canvas>
                            </div>
                            <div class="col-md-4">
                                <img id="sig_img" src="" alt="">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-outline-danger btn-sm" id="sig-clearBtn">Reset</button>
                                <button type="button" class="btn btn-outline-success btn-sm"
                                    id="sig-submitBtn">Accept</button>
                            </div>
                        </div>
                        <br />
                        <input required name="signature" id="sig-dataUrl" class="d-none">
                    </div>

                    <div class="col-12 text-center mt-4">
                        <button class="primary-btn fix-gr-bg">
                            <span class="ti-check"></span>
                            Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset('public/js/travel_declaration.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/acceptable_use_policy.blade.php ENDPATH**/ ?>