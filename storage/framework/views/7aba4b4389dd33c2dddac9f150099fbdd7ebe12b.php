
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
<br>

<div>
    <img style="width:100%" src="<?php echo e(url('public')); ?>/images/banner/banner_03.jpg" alt="">
</div>

<!-- Start of Early year -->
<section id="admissions_page">
    <div class="container policy">
    	<button>Early </br>Years</button>
    	<button>Primary </br>Years</button>
    </div>
    
    <div class="container">
    	<div class="admission-link">
    	    <div class="link">
    	        <a target="_blank" href="<?php echo e(url('public/tailieu/SCHEDULE OF FEE 2021- 2022 ACADEMIC YEAR.pdf')); ?>">schedule of fee 2021-2022 academic year</a>
    	    </div>
    	    <div class="link">
    	        <a target="_blank" href="<?php echo e(url('public/tailieu/_SCHOOL BUS SERVICE 2021-2022.pdf')); ?>">school bus service 2021-2022</a>
    	    </div>
    	    <div class="link">
    	        <a target="_blank" href="<?php echo e(url('public/tailieu/ADMISSIONS POLICY_01032021.pdf')); ?>">admissions policy</a>
    	    </div>
    	    
    	</div>
    </div>
</section>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/admissions.blade.php ENDPATH**/ ?>