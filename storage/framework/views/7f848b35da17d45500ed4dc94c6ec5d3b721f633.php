<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('frontEnd.home.layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</head>
<body>
	 <?php echo $__env->yieldContent('main_content'); ?>
	
</body>
	<?php echo $__env->make('frontEnd.home.layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	
</html><?php /**PATH D:\WWW\SGS\sgss\resources\views/frontEnd/home/layout/front_master.blade.php ENDPATH**/ ?>