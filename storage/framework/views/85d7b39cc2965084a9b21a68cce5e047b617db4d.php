
<?php $__env->startSection('main_content'); ?>
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
    <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css"/> 
<?php $__env->stopPush(); ?>

<div class="banner">
	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>SUMMER CAMP</h3>
        </div>
	</div>
	
	
</div>
<div class="main_content" style="background:#fafaf4">
    <div class="container winter-camp">
        <div class="col-lg-4 col-md-12 col-sm-12">
            <img src="<?php echo e(asset('public/')); ?>/images/summer-camp.jpg" alt="">
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12">
            <p class="new-text-color" style="margin-bottom:0px;line-height:1.3em">
                Get ready for a whole summer packed with excitement, discovery, and adventure. At SGSIS Summer Camp, your child will explore themes through hands-on projects and daily activities: a mix of sports, team-building, STEAM projects, life skills, dance, camping and field trips. <br><br>
                Our five-week summer camp is divided into weekly themes, which promotes learning experiences and personal learning journey. <br><br>
                Our all-day camp features healthy meals and snacks. Plus, campers receive a T-shirt, and baseball cap for summer adventures. Especially, kids from 8 years old spend one sleepaway night at school at the end of the camp. <br><br>
                <span style="color:#7dd3f7;font-weight: bold;">Open to students from all schools (age 2-11).</span><br> 
                Week 1: 5 – 9/7/2021<br>
                Week 2: 12 – 16/7/2021<br>
                Week 3: 19 – 23/7/2021<br>
                Week 4: 26 – 30/7/2021<br>
                Week 5: 2 – 6/8/2021<br><br>
                 
                <span style="color:#7dd3f7;font-weight: bold;">Knowledge, Skills and Attitudes</span><br>
                To be resilient, to be adaptable, to be a collaborator, to be empathetic, to be a thinker<br><br>
                Creativity, confidence, physical development <br><br>
                <span style="color:#7dd3f7;font-weight: bold;">Field trips: </span><br>
                Camping, Flying kites, Movies (activities are subject to change depending on the weather and age group)<br><br>
                <span style="color:#7dd3f7;font-weight: bold;">Student grouping by age </span><br>
                - Group 1: 2-5 years old<br>
                - Group 2: 6-8 years old<br>
                - Group 3: 9+ years old<br><br>
                <span style="color:#7dd3f7;font-weight: bold;">Sample timetable </span><br>
                <img width="100%" src="<?php echo e(url('public/images/summer_camp.png')); ?>">
            </p>
            
            <a target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSc_5-_HLc7cNsQwNv-nYxmIBsdKEWTzY2a2Hd5ANIWRItOL0A/viewform" type="button" style="
                background: #de0084;
                padding: 7px 15px 7px 15px;
                color: #fff;
                font-weight: bold;
                font-size: 20px;
                border-radius: 50px;
                ">REGISTER ONLINE
            </a>
            
        </div>
    </div>  

    <div class="container">
        <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/CHILDREN-01.png" alt="">
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/summer-camp.blade.php ENDPATH**/ ?>