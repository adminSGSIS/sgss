
<?php $__env->startSection('main_content'); ?>
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
    <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css"/> 
<?php $__env->stopPush(); ?>

<div class="banner">
	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner/banner_03.jpg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>STUDENT COUNCIL</h3>
        </div>
	</div>
</div>

<div class="main_content" style="background:#fafaf4">
	<div class="container">
		<div class="col-12 student-council">
		    <div class="col-lg-6 col-sm-12">
		        <h1>year 3</h1>
		        <div class="group1">
    		        <div class="avatar">
    		            <img src="<?php echo e(asset('public/')); ?>/images/student-council/TOM.png" alt="">
    		            <h3>Tom</h3>
    		        </div>
    		        <div class="avatar">
    		            <img src="<?php echo e(asset('public/')); ?>/images/student-council/PEPSI.png" alt="">
    		            <h3>Pepsi</h3>
    		        </div>
    		     </div>
		    </div>
		    <div class="col-lg-6 col-sm-12">
		        <h1>year 4</h1>
		        <div class="group1">
    		        <div class="avatar">
    		            <img src="<?php echo e(asset('public/')); ?>/images/student-council/ADI.png" alt="">
    		            <h3>Adi</h3>
    		        </div>
    		        <div class="avatar">
    		            <img src="<?php echo e(asset('public/')); ?>/images/student-council/LIAM.png" alt="">
    		            <h3>Liam</h3>
    		        </div>
    		     </div>
		    </div>
		</div>
		<div class="col-12 student-council">
		    <div class="col-lg-6 col-sm-12">
		        <h1>year 5</h1>
		        <div class="group1">
    		        <div class="avatar">
    		            <img src="<?php echo e(asset('public/')); ?>/images/student-council/KIRIN.png" alt="">
    		            <h3>Kirin</h3>
    		        </div>
    		        <div class="avatar">
    		            <img src="<?php echo e(asset('public/')); ?>/images/student-council/TIMOFEY.png" alt="">
    		            <h3>Timofey</h3>
    		        </div>
    		     </div>
		    </div>
		    <div class="col-lg-6 col-sm-12">
		        <h1>year 6</h1>
		        <div class="group1">
    		        <div class="avatar">
    		            <img src="<?php echo e(asset('public/')); ?>/images/student-council/KY-ANH.png" alt="">
    		            <h3>Ky Anh</h3>
    		        </div>
    		        <div class="avatar">
    		            <img src="<?php echo e(asset('public/')); ?>/images/student-council/ORI.png" alt="">
    		            <h3>Ori</h3>
    		        </div>
    		     </div>
		    </div>
		</div>
		
		
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/student-council.blade.php ENDPATH**/ ?>