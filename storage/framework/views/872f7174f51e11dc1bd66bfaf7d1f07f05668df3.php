
<?php $__env->startSection('main_content'); ?>

<body>
	<div class="banner">
    	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
    </div>
    <div class="c-color">
	<header class="site-header" id="header" style="text-align : center;">
		<h1 class="site-header__title" data-lead-id="site-header-title" style="color : #1ac754;">THANK YOU!</h1>
	</header>

	<div class="main-content ">
		
		<div class="container center">
		    <p class="main-content__body" data-lead-id="main-content-body">Your submission is received and we will contact you soon.</p>
		</div>
	</div>

	
	</div>
</body>
</html>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/success.blade.php ENDPATH**/ ?>