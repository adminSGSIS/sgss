<div class="row mb-40 mt-30">
  <div class="col-lg-10">
    <p>If your child has a vision or hearing impairment, please describe it here *</p>
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('answer4') ? ' is-invalid' : ''); ?>" type="text" name="answer4" value="<?php echo e(old('answer4')); ?>">

      <label>Answer</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="answer4_feedback" role="alert"></span>
      <?php if($errors->has('answer4')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('answer4')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
</div>
<div class="row mb-40 mt-30">
  <div class="col-lg-10">
    <p>Please describe any other Special Educational Need *</p>
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('answer5') ? ' is-invalid' : ''); ?>" type="text" name="answer5" value="<?php echo e(old('answer5')); ?>">

      <label>Answer</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="answer5_feedback" role="alert"></span>
      <?php if($errors->has('answer5')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('answer5')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
</div><?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/forms/step51.blade.php ENDPATH**/ ?>