<div id="std-info">
<div class="top-header">
	<h3 class="center text-header">STUDENT INFORMATION</h3>
</div>
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-12">
		<div class="enrol-input">
			<input type="text" value="<?php echo e(isset($student)? $student->first_name : ''); ?>" class="form-control" id="f_name" name="f_name" placeholder="First name (Given name) *" required="required">
			<div class="validate" id="f_name_validate">*this field cannot be empty*</div>
		</div>
		<div class="enrol-input">
			<input type="text" value="<?php echo e(isset($student)? $student->middle_name : ''); ?>" class="form-control" name="m_name" placeholder="Middle name(s) (if any)">
		</div>
		<div class="enrol-input">
			<input type="text" value="<?php echo e(isset($student)? $student->last_name : ''); ?>" class="form-control" id="l_name" name="l_name" placeholder="Last name (Family name) *" required="required">
			<div class="validate" id="l_name_validate">*this field cannot be empty*</div>
		</div>
		<p>* please capitalize the LAST NAME / FAMILY NAME</p>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 center">
		<div class="std-img-picker">
			<img src="<?php echo e(url('/')); ?>/<?php echo e(isset($student)? $student->student_photo: ''); ?>" id="imgPreview" name="std_photo">
		</div>
		<a  id="std_photo_btn" class="btn btn-primary" style="margin-top: 10px;background: #7dd3f7;color:#fff">Photo</a>
		<input type="file" name="std_photo" id="std_photo_btn_real" class="hidden" >
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="enrol-input">
			<input type="text" value="<?php echo e(isset($student)? $student->nickname:""); ?>" class="form-control" name="p_name" placeholder="Preferred name (or nick name)">
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="enrol-input fontcalender" >
			<input type="text" value="<?php echo e(isset($student)? $student->date_of_birth: ''); ?>" class="form-control date-input" name="date_of_birth" id="date_of_birth" placeholder="Date of Birth (dd/mm/yyyy) *" required="required" autocomplete="off">
			<div class="validate" id="date_of_birth_validate">this field cannot be empty</div>
			<label for="date_of_birth"><i class="fa fa-calendar fa-lg" aria-hidden="true"></i></label>			
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="gender-container">
			<div class="row">
				<div class="col-3">Gender</div>
				<div class="col-4"><input type="radio" <?php echo e(isset($student) && $student->gender_id == 1? 'checked': 'checked'); ?> name="gender" value="1"> Male</div>
				<div class="col-5"><input type="radio" <?php echo e(isset($student) && $student->gender_id == 2? 'checked': ''); ?> name="gender" value="2"> Female</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="enrol-input">
			<input type="text" value="<?php echo e(isset($student)? $student->nationality_1: ''); ?>" class="form-control" name="nationality_1" id="nationality_1" placeholder="Nationality 1  *" required="required">
			<div class="validate" id="nationality_1_validate">*this field cannot be empty*</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="enrol-input">
			<input type="text" value="<?php echo e(isset($student)? $student->nationality_2: ''); ?>" class="form-control" name="nationality_2" placeholder="Nationality 2 (if any)">
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-6">
		<div class="enrol-input">
			<select class="form-control" name="enrol_in" id="enrol_in" required="required">
				<option value="">Enrol in  *</option>
				<?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($class->id != 26): ?>
				<option value="<?php echo e($class->id); ?>" <?php echo e(isset($student)&& $student->class_id == $class->id ? 'selected' : ''); ?>><?php echo e($class->class_name); ?></option>
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php if($class->id == 26): ?>
				<option value="<?php echo e($class->id); ?>" <?php echo e(isset($student)&& $student->class_id == $class->id ? 'selected' : ''); ?>><?php echo e($class->class_name); ?></option>
				<?php endif; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</select>
			<div class="validate" id="enrol_in_validate">*this field cannot be empty*</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-12">
		<div class="enrol-input">
			<select class="form-control" id="applicant_select" name="applicant">
			<option value="1">Father</option>
			<option value="2">Mother</option>
		</select>
		</div>
		
	</div>
	
</div>
</div>
<div class="top-header">
	<h3 class="center text-header">FAMILY INFORMATION</h3>
</div>

<br>
<div id="family-select">
	<select class="niceSelect w-100 form-control" id="parent-select" >
	<option value="0" id="first-select">Select family</option>
	<?php $__currentLoopData = $parents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<option <?php echo e(isset($parent_find) && $parent_find->id == $parent->id ? 'selected' : ''); ?>  value="<?php echo e($parent->id); ?>">Father : <?php echo e($parent->fathers_name != "" ? $parent->fathers_name : ""); ?> / Mother : <?php echo e($parent->mothers_name != "" ? $parent->mothers_name : ""); ?>

		</option>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</select>
	
</div>
<br>
<br><br>
<div id="family">
	<div class="main-family" id="father-1"> 
	<div class="row">
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="family_1_radio" value="father" checked="checked" id="father_radio"> FATHER
		 </div>
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="family_1_radio" value="guardian" id="guardian_radio"> GUARDIAN
		 </div>
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="primary_contact_radio" value="father" id="guardian_radio" checked=""> PRIMARY CONTACT	
		 </div>
	</div>
	
	<div class="father-info family-info">
		<h4 class="text-sub-header">FATHER INFORMATION </h4>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->fathers_name : ''); ?>" name="father_name" placeholder="Full name" id="father_name" >
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->fathers_nationality : ''); ?>" name="father_nationality" placeholder="Nationality" id="father_nationality">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->fathers_occupation : ''); ?>" name="father_occupation" placeholder="Occupation" id="father_occupation">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->fathers_company : ''); ?>"  name="father_copany" placeholder="Company" id="father_company">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->fathers_work_address : ''); ?>" name="father_work_address" placeholder="Work address" id="father_work_address">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->fathers_mobile : ''); ?>" name="father_phone" placeholder="Phone" id="father_work_email">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->fathers_email : ''); ?>" name="father_email" placeholder="Personal email" id="father_email">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->fathers_first_language : ''); ?>" name="father_language" placeholder="First language" id="father_language">
				</div>
			</div>
		</div>
		<div class="gender-container">
			<div>English level: please check a box</div>
			<div class="row checkbox-group">
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="father_english1" value="1" <?php echo e(isset($parent_find) && $parent_find->fathers_language_level == 1 ? 'checked' : ''); ?>> Beginner
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<input type="radio" name="father_english1" value="2" <?php echo e(isset($parent_find) && $parent_find->fathers_language_level == 2 ? 'checked' : ''); ?>> Gaining confidence
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="father_english1" value="3" <?php echo e(isset($parent_find) && $parent_find->fathers_language_level == 3 ? 'checked' : ''); ?>> Confidence
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="father_english1" value="4" <?php echo e(isset($parent_find) && $parent_find->fathers_language_level == 4 ? 'checked' : ''); ?>> Fluent
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="father_english1" value="5" <?php echo e(isset($parent_find) && $parent_find->fathers_language_level == 5 ? 'checked' : ''); ?>> Native
				</div>
			</div>	
		</div>
	
	</div>
</div>
<div class="main-family" id="mother-1">
	<div class="row">
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="family_2_radio" value="mother" checked="checked" id="mother_radio"> MOTHER
		 </div>
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="family_2_radio" value="guardian" id="mother_radio"> GUARDIAN
		 </div>
		 <div class="col-sm-4">
		 	<input type="radio" class="family-radio" name="primary_contact_radio" value="mother" id="guardian_radio" > PRIMARY CONTACT	
		 </div>
	</div>
	<div class="mother-info family-info">
		<h4 class="text-sub-header">MOTHER INFORMATION </h4>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->mothers_name : ''); ?>" name="mother_name" placeholder="Full name" id="mother_name">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->mothers_nationality : ''); ?>" name="mother_nationality" placeholder="Nationality">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->mothers_occupation : ''); ?>" name="mother_occupation" placeholder="Occupation">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->mothers_company : ''); ?>" name="mother_copany" placeholder="Company">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->mothers_work_address : ''); ?>" name="mother_work_address" placeholder="Work address">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->mothers_mobile : ''); ?>" name="mother_phone" placeholder="Phone">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->mothers_email : ''); ?>" name="mother_email" placeholder="Personal email">
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="enrol-input">
					<input type="text" class="form-control" value="<?php echo e(isset($parent_find) ? $parent_find->mothers_first_language : ''); ?>" name="mother_language" placeholder="First language">
				</div>
			</div>
		</div>
		<div class="gender-container">
			<div>English level: please check a box</div>
			<div class="row checkbox-group">
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="mother_english1" value="1" <?php echo e(isset($parent_find) && $parent_find->mothers_language_level == 1 ? 'checked' : ''); ?>> Beginner
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<input type="radio" name="mother_english1" value="2" <?php echo e(isset($parent_find) && $parent_find->mothers_language_level == 2 ? 'checked' : ''); ?>> Gaining confidence
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="mother_english1" value="3" <?php echo e(isset($parent_find) && $parent_find->mothers_language_level == 3 ? 'checked' : ''); ?>> Confidence
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="mother_english1" value="4" <?php echo e(isset($parent_find) && $parent_find->mothers_language_level == 4 ? 'checked' : ''); ?>> Fluent
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6">
					<input type="radio" name="mother_english1" value="5" <?php echo e(isset($parent_find) && $parent_find->mothers_language_level == 5 ? 'checked' : ''); ?>> Native
				</div>
			</div>	
		</div>
	</div>
</div>
</div><h4 class="text-sub-header">Home address in Ho CHi Minh City</h4>
	<div class="gender-container">
			<div class="sm-content b-text">Home address in Ho Chi Minh City*</div>
			<input type="text" value="<?php echo e(isset($enrolment) ? $enrolment->home_adress_in_hcm : ''); ?>" name="hcmc_address" class="b-bottom-input form-control" id="hcmc_address">
	</div>
	<?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/steps/step-1.blade.php ENDPATH**/ ?>