
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
    <div >
        <div class="banner">
            <img class="banner" width="100%" src="<?php echo e(asset('public/images/banner/banner_03.jpg')); ?>" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>academic team</h3>
                </div>
            </div>
        </div>
        
        <div class="staff d-flex justify-content-center c-color">
            <?php $__currentLoopData = $earlys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $early): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="profile2 collumn">
                    <img src="<?php echo e(file_exists(@$early->staff_photo) ? asset($early->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal<?php echo e($early->user_id); ?>">
                        <?php echo e($early->first_name); ?>

                        <?php echo e(substr($early->last_name, strrpos($early->last_name, ' '))); ?>


                    </a><br>
                    <p class="staff-title"><?php echo $early->designations->title; ?></p>
                </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $primarys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $primary): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="profile2 collumn">
                    <img src="<?php echo e(file_exists(@$primary->staff_photo) ? asset($primary->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal<?php echo e($primary->user_id); ?>">
                        <?php echo e($primary->first_name); ?>

                        <?php echo e(substr($primary->last_name, strrpos($primary->last_name, ' '))); ?>


                    </a><br>
                    <p class="staff-title"><?php echo $primary->designations->title; ?></p>
                </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
            <?php $__currentLoopData = $middles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $middle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="profile2 collumn">
                    <img src="<?php echo e(file_exists(@$middle->staff_photo) ? asset($middle->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal<?php echo e($primary->user_id); ?>">
                        <?php echo e($middle->first_name); ?>

                        <?php echo e(substr($middle->last_name, strrpos($middle->last_name, ' '))); ?>


                    </a><br>
                    <p class="staff-title"><?php echo $middle->designations->title; ?></p>
                </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
            <?php $__currentLoopData = $specialists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $specialist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="profile2 collumn">
                    <img src="<?php echo e(file_exists(@$specialist->staff_photo) ? asset($specialist->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal<?php echo e($specialist->user_id); ?>">
                        <?php echo e($specialist->first_name); ?>

                        <?php echo e(substr($specialist->last_name, strrpos($specialist->last_name, ' '))); ?>


                    </a><br>
                    <p class="staff-title"><?php echo $specialist->designations->title; ?></p>
                </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    
    <div class="c-color">
        <div class="container">
              <img width="100%" src="<?php echo e(url('public')); ?>/images/CHILDREN-01.png" alt="">
        </div>
    </div>
    <?php echo $__env->make('frontEnd.home.partials.info-teachers', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/staff/academic.blade.php ENDPATH**/ ?>