
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
    <div class="banner">
    	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>curriculum</h3>
            </div>
    	</div>
    </div>
    <div class="main_content c-color">
        <div class="curriculum-page row">
            <div class="col-lg-3 col-md-12 col-sm-12 sub-menu">          
                <a href="/curriculum" class="active font-weight-bold">OVERVIEW <i class="fa fa-caret-down"></i></a></br>
                <a class="pl-2" href="#1">INTRODUCTION </a></br>
                <a class="pl-2" href="#2">OUR BELIEF AND PHILOSOPHY</a><br>
                <a  id="academic" class="font-weight-bold">ACADEMIC PROGRAMS <i class="fa fa-caret-down"></i></a> </br>
                <a class="pl-2 academic" href="/early-year">EARLY YEARS</a></br>
                <a class="pl-2 academic" href="/primary-year">PRIMARY YEARS</a>
            </div>  
            <div class="col-lg-9 col-sm-12 col-md-12">
                <h3>Overview</h3>
                <h4 id="1">Introduction</h4>
                <p class="new-text-color">
                    Saigon Star International School is a boutique educational setting with lush and green
                    facilities in a quiet area of District 2. We specialise in, and deliver, the National 
                    Curriculum for England and Wales which is supplemented by the holistic and progressive 
                    International Curriculum (IEYC and IPC). We were the first school in Vietnam to be
                    accredited with this and attain categories of 'Mastering' in 2018.</br></br>

                    Our amazing, dedicated and caring teachers are fully qualified/licensed, mainly from the UK,
                    and carry at least three years experience from their home countries. Supported by our 
                    professional development opportunities, they model the attributes of lifelong learning and our
                    reflective practices to our student body and in doing so, help us try to develop those 'ways of thinking'
                    (Academic and International goals) and 'ways of being' (our Personal Learning Goals).</br></br>

                    Our approach to the IPC curriculum, the associated Personal Learning Goals and the overarching 'umbrella'
                    of International Mindedness goes beyond just words on our walls and we aim to provide meaningful opportunities,
                    for our learners to improve and showcase their Knowledge, Skills and
                    Understanding in a safe, purposeful and nurturing environment. We aim to ensure that whilst we help to
                    foster an intrinsic motivation for learning, we also equip our students with the ability to collaborate 
                    and cooperate, think cognitively and gain strong resilience for life's challenges.</br></br>

                    Aside from the academic expectations of our school, we focus strongly on providing creative opportunities 
                    with the Sciences, Arts and Physical Education all core to our provision. Our learning opportunities for 
                    these include STEM, World Languages, Music and Dance, Art and many more that foster passion,
                    creativity and enrich young lives.</br></br>

                    By supplementing our high-quality education with various community events throughout the academic year, 
                    our hope is that we celebrate the rich tapestry of our families, their backgrounds and beliefs, and offer 
                    motivating learning opportunities no matter the age.
                    <div>
                        <img src="<?php echo e(url('public')); ?>/images/curriculum_teacher.jpg" alt="">
                    </div>
                </p>

                <div class="curriculum-overview">
                    <h4 id="2">Our belief and philosophy</h4>
                    <div class="center">
                    <img width="60%" class="center" src="<?php echo e(url('public')); ?>/images/our belief and philosophy-01.png"></img>
                    </div>
                    <div>
                        <a href="#" id="4" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Personal Learning Goals</a>
                        <div id="accordion_admission">
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_admission">
                                <p class="new-text-color">In addition to the key knowledge, skills and understandings learned within each subject domain, 
                                children are helped to develop 8 personal characteristics and attitudes that will help them to be successful both as 
                                learners and as contributors in our rapidly-changing global society, both now and in the future. 
                                <div>
                                    <img src="<?php echo e(url('public')); ?>/images/personal-learning.png" alt="">
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a href="#" id="5" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">Subject Learning Goals</a>
                        <div id="accordion_admission1">
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion_admission1">
                                <p class="new-text-color"> Subject goals cover the knowledge, skills and understanding of children relating to the subjects they are learning.
                                There are subject learning goals for Language Arts, Mathematics, Science, ICT & Computing, Technology, History, Geography, 
                                Music, Physical Education, Art and Society.</br>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a href="#" id="6" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">International Learning Goals</a>
                        <div id="accordion_admission2">
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion_admission2">
                                <p class="new-text-color"> International learning goals are unique to our curriculum and help young children begin the move towards 
                                an increasingly sophisticated national, international and intercultural perspective. Each thematic IPC unit includes 
                                an international aspect, to help develop a sense of ‘international mindedness’.
                                </p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                </div>
            </div>
        </div>
    </div>  
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script>
        let check = true;
        $("#academic").on("click", function(e) {
            $(".academic").css("display", "none");
            if (check == true) {
			    $(".academic").css("display", "inline");
            }
            check = !check;
		});
		$("a").on('click', function(e) {
			$("HTML, BODY").animate({ scrollTop: $($(this).attr('href')).offset().top - 80}, 1000);
		});
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\resources\views/frontEnd/home/curriculum-overview.blade.php ENDPATH**/ ?>