
<?php $__env->startSection('mainContent'); ?>
<style>
td{
    font-size: 15px!important;
}
</style>
<div class="row print">
    <div class="col-lg-8 col-md-6">
        <div class="main-title">
            <h3 class="mb-30">EXPORT</h3>
        </div>
    </div>
    <div class="col-lg-4 text-right">
        <a href="#" onclick="exportPdf()" class="primary-btn small fix-gr-bg">
            <span class="ti-plus pr-2"></span>
            EXPORT TO PDF
        </a>
    </div>
</div>
<?php echo $__env->make('feescollection::view-slip-pdf', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('feescollection::letter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div id="view-slip">
    <div class="white-box">
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <div class="single-meta mt-20">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Student's Name
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                <?php echo e($slip->student->full_name); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-meta">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Father’s Name
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                <?php echo e($slip->student->parents->fathers_name); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-meta">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Mobile
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                <?php echo e($slip->student->parents->fathers_mobile); ?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="offset-lg-2 col-lg-5 col-md-6">
                <div class="single-meta mt-20">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Class
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                <?php echo e($slip->student->className->class_name); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-meta">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Student id
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                <?php echo e($slip->student->admission_no); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-meta">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="value text-left">
                                Student number
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="name">
                                <?php echo e($slip->student->roll_no); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <table class="dataTable">
        <thead>
            <tr>
                <th>Type of fee</th>
                <th>Amount (vnd)</th>
                <th>Discount (%)</th>
                <th>Discount reason</th>
                <th>Total amount (vnd)</th>
            </tr>
        </thead>
        <tbody>
            <?php $__currentLoopData = $fees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($fee->feegroup->group_name); ?></td>
                <td><?php echo e(number_format($fee->fee->amount)); ?></td>
                <td><?php echo e($fee->discount); ?> %</td>
                <td><?php echo e($fee->discount_reason); ?></td>
                <td><?php echo e(number_format($fee->fee->amount / 100 * (100 - $fee->discount))); ?></td>
            </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><b>Total</b></td>
                <td></td>
                <td></td>
                <td></td>

                <td><b><?php echo e(number_format($slip->amount_applied_discount)); ?></b></td>
            </tr>
        </tbody>
    </table>
    <br>
    <div class="row" style="padding:15px">
        <div class="col-lg-9 col-md-9 col-sm-0"></div>
        <div class="col-lg-3 col-md-3 col-sm-12 white-box">
            <div class="row">
                <div class="col-6">
                    Total:
                </div>
                <div class="col-6">
                    <?php echo e(number_format($slip->amount_applied_discount)); ?> vnd
                </div>
            </div>
            <?php if($slip->deposit != 0): ?>
            <div class="row">
                <div class="col-6">
                    Deposit:
                </div>
                <div class="col-6">
                    <?php echo e($slip->deposit); ?> %
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Deposit amount:
                </div>
                <div class="col-6">
                    <?php echo e(number_format($slip->amount_applied_discount / 100 * $slip->deposit)); ?> vnd
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-6">
                    Paid:
                </div>
                <div class="col-6">
                    <?php echo e(number_format($slip->paid)); ?>

                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-6">
                    <b>Balance: </b>
                </div>
                <div class="col-6">
                    <b><?php echo e(number_format($slip->amount_applied_discount - $slip->paid)); ?> vnd</b>
                </div>
            </div>
        </div>
    </div>
    <br>
    <table class="dataTable" >
			<thead>
				<tr>
					<th>Amount(vnd)</th>
					<th>Due date</th>
					<th>Paid(vnd)</th>
					<th>Balance(vnd)</th>
					<th>Payment status</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<?php $__currentLoopData = $payments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(number_format($payment->amount_payment)); ?></td>
    <td><?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')); ?></td>
    <td><?php echo e(number_format($payment->paid)); ?></td>
    <td><?php echo e(number_format($payment->amount_payment-$payment->paid)); ?></td>
    <td>
        <?php if($payment->status == 0): ?>
        <button class="btn btn-danger btn-sm" style="border-radius: 20px;">unpaid</button>
        <?php elseif($payment->status == 1): ?>
        <button class="btn btn-success" style="border-radius: 20px;">paid</button>
        <?php elseif($payment->status == 2): ?>
        <button class="btn btn-warning btn-sm" style="border-radius: 20px;">deposit</button>
        <?php endif; ?>
    </td>
    <td>
        <div class="dropdown">
            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                <?php echo app('translator')->get('lang.select'); ?>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <?php if($payment->status == 0): ?>
                <a class="dropdown-item" data-toggle="modal" data-target="#ProceedPayment<?php echo e($payment->id); ?>">Pay</a>
                <?php elseif($payment->status == 1): ?>
                <a class="dropdown-item" data-toggle="modal" data-target="#ProceedPayment<?php echo e($payment->id); ?>">Paid</a>
                <?php elseif($payment->status == 2): ?>
                <a class="dropdown-item" data-toggle="modal" data-target="#ProceedPayment<?php echo e($payment->id); ?>">Deposit</a>
                <?php endif; ?>
                <a class="dropdown-item" data-toggle="modal" data-target="#ExtendDueDate<?php echo e($payment->id); ?>">Extend</a>
            </div>
        </div>
    </td>
    </tr>
    </tbody>
    <div class="modal fade admin-query" id="ProceedPayment<?php echo e($payment->id); ?>">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo e(url('feescollection/proceed-payment')); ?>"
                        enctype="multipart/form-data">
                        <?php echo csrf_field(); ?>
                        Amount : <?php echo e(number_format($payment->amount_payment)); ?> vnđ<br><br>
                        Paid : <?php echo e(number_format($payment->paid)); ?> vnđ<br><br>
                        Balance : <?php echo e(number_format($payment->amount_payment-$payment->paid)); ?> <br><br>
                        Amount payment (vnđ) <input type="text" min="0"
                            max="<?php echo e($payment->amount_payment - $payment->paid); ?>"
                            class="form-control primary-input number amount_payment" name="amount_payment" required=""
                            value="0" <?php echo e($payment->status == 1 ? 'readonly' : ''); ?> required="">
                        <input type="hidden" name="id_payment" value="<?php echo e($payment->id); ?>">
                        <input type="hidden" name="id_slip" value="<?php echo e($slip->id); ?>">
                        <br>
                        <h4>Payment method</h4>
                        <div class="row">
                            <div class="col-3">
                                <input type="radio"name="payment_method"value="cash" checked="">&nbsp;<label>Cash</label></div>
                            <div class="col-3">
                                <input type="radio"name="payment_method"value="cheque">&nbsp;<label>Cheque</label></div>
                            <div class="col-3">
                                <input type="radio"name="payment_method"value="bank transfer">&nbsp;<label>Bank tranfer</label></div>
                            <div class="col-3">
                                <input type="radio"name="payment_method"value="credit card">&nbsp;<label>Credit card</label></div>
                        </div>
                        <br>
                        <h4>Select proof of payment</h4>
                        <input type="file" name="proof_of_payment" required=""><br><br>

                        <?php if(count($payment->proof) > 0): ?>
                        <h4>Payment history</h4>
                        <div class="row">
                            <div class="col-3">
                                Date
                            </div>
                            <div class="col-3">
                                Amount
                            </div>
                            <div class="col-3">
                                Payment method
                            </div>
                            <div class="col-3">
                                Proof
                            </div>
                        </div>
                        
                        <?php $__currentLoopData = $payment->proof; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proof): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="row">
                                <div class="col-3">
                                    <?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $proof->payment_date) , 'd-m-Y')); ?>

                                </div>
                                <div class="col-3">
                                    <?php echo e(number_format($proof->amount)); ?>vnd 
                                </div>
                                <div class="col-3">
                                    <?php echo e($proof->payment_method); ?> 
                                </div>
                                <div class="col-3">
                                    <a href="<?php echo e(url('')); ?>/<?php echo e($proof->url_of_proof); ?>" download>view proof</a><br>
                                </div>
                            </div>
                             
                            
                            
                                
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        <?php if($payment->amount_payment - $payment->paid > 0): ?>
                        <br>
                        <button class="btn btn-primary">Save</button>
                        <?php endif; ?>
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="modal fade admin-query" id="ExtendDueDate<?php echo e($payment->id); ?>">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Extend due date</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <form method="POST" action="<?php echo e(url('feescollection/extend-duedate')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="text-center">
                            <input type="hidden" name="payment_id" value="<?php echo e($payment->id); ?>">
                            <h4>Old due date :
                                <?php echo e(date_format(DateTime::createFromFormat('Y-m-d', $payment->due_date) , 'd-m-Y')); ?></h4>
                            <h4>New due date :</h4><input type="text" id="duedate" class="form-control" name="due_date"
                                placeholder="dd-mm-yyyy" required="" autocomplete="off">
                        </div>
                        <div class="mt-40 d-flex justify-content-between">
                            <button type="button" class="primary-btn tr-bg"
                                data-dismiss="modal"><?php echo app('translator')->get('Cancel'); ?></button>
                            <input type="hidden" name="id" value="" id="ncome_id">
                            <button class="primary-btn fix-gr-bg" type="submit"><?php echo app('translator')->get('Submit'); ?></button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(url('Modules/Form/public/js/jquery.number.min.js')); ?>"></script>
<script type="text/javascript">
    function exportPdf() {
        window.print();
    }
    $(function () {
        $("#duedate").datepicker({
            format: "dd-mm-yyyy"
        }).val();
    });
    $('.number').number(true);
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/view-slip.blade.php ENDPATH**/ ?>