
<?php $__env->startSection('mainContent'); ?>
<style>
    td{
        font-size: 15px!important;
    }
    </style>
    <div class="row print">
        <div class="col-lg-8 col-md-6">
            <div class="main-title">
                <h3 class="mb-30">EXPORT</h3>
            </div>
        </div>
        <div class="col-lg-4 text-right">
            <a href="#" onclick="exportPdf()" class="primary-btn small fix-gr-bg">
                <span class="ti-plus pr-2"></span>
                EXPORT TO PDF
            </a>
        </div>
    </div>
    <?php echo $__env->make('feescollection::view-slip-pdf', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make('feescollection::letter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div id="view-slip">
        <div class="white-box">
            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <div class="single-meta mt-20">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="value text-left">
                                    Student's Name
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name">
                                    <?php echo e($slip->student->full_name); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-meta">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="value text-left">
                                    Father’s Name
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name">
                                    <?php echo e($slip->student->parents->fathers_name); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-meta">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="value text-left">
                                    Mobile
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name">
                                    <?php echo e($slip->student->parents->fathers_mobile); ?>

                                </div>
                            </div>
                        </div>
                    </div>
    
                </div>
                <div class="offset-lg-2 col-lg-5 col-md-6">
                    <div class="single-meta mt-20">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="value text-left">
                                    Class
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name">
                                    <?php echo e($slip->student->className->class_name); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-meta">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="value text-left">
                                    Student id
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name">
                                    <?php echo e($slip->student->admission_no); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-meta">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="value text-left">
                                    Student number
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name">
                                    <?php echo e($slip->student->roll_no); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <br>
        <table class="dataTable">
            <thead>
                <tr>
                    <th>Types of fee</th>
                    <th>Amount (vnd)</th>
                    <th>Discount (%)</th>
                    <th>Discount reason</th>
                    <th>Total amount (vnd)</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $fees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($fee->feegroup->group_name); ?></td>
                    <td><?php echo e(number_format($fee->fee->amount)); ?></td>
                    <td><?php echo e($fee->discount); ?> %</td>
                    <td><?php echo e($fee->discount_reason); ?></td>
                    <td><?php echo e(number_format($fee->fee->amount / 100 * (100 - $fee->discount))); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><b>Total</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
    
                    <td><b><?php echo e(number_format($slip->amount_applied_discount)); ?></b></td>
                </tr>
            </tbody>
        </table>
        <br>
        <div class="row" style="padding:15px">
            <div class="col-lg-9 col-md-9 col-sm-0"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 white-box">
                <div class="row">
                    <div class="col-6">
                        Total:
                    </div>
                    <div class="col-6">
                        <?php echo e(number_format($slip->amount_applied_discount)); ?> vnd
                    </div>
                </div>
                <?php if($slip->deposit != 0): ?>
                <div class="row">
                    <div class="col-6">
                        Deposit:
                    </div>
                    <div class="col-6">
                        <?php echo e($slip->deposit); ?> %
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        Deposit amount:
                    </div>
                    <div class="col-6">
                        <?php echo e(number_format($slip->amount_applied_discount / 100 * $slip->deposit)); ?> vnd
                    </div>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-6">
                        Paid:
                    </div>
                    <div class="col-6">
                        <?php echo e(number_format($slip->paid)); ?>

                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-6">
                        <b>Balance: </b>
                    </div>
                    <div class="col-6">
                        <b><?php echo e(number_format($slip->amount_applied_discount - $slip->paid)); ?> vnd</b>
                    </div>
                </div>
            </div>
        </div>
        <br>
</div>
<script>
    window.onload = function () {
        setTimeout(function () {
            var conf = confirm('Do you want to save PDF file?');
            if (conf) {
                window.print();
            }
        }, 2500);
    }
</script>
<script>
    function exportPdf() {
        var conf = confirm('Do you want to save PDF file?');
        if (conf) {
            window.print();
        }
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/payment-detail.blade.php ENDPATH**/ ?>