<?php $__env->startSection('css'); ?>
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/css')); ?>/purchase-proposal.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Trial List</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <tr class="centerTh">
                                    <th>Student name</th>
                                    <th>Class</th>
                                    <th>Date of birth</th>
                                    <th>Gender</th>
                                    <th>Is Trial</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="centerTd">
                                        <td><?php echo e($value->full_name); ?></td>
                                        <td><?php echo e($value->admission->class->class_name ?? ''); ?></td>
                                        <td><?php echo e($value->date_of_birth); ?></td>
                                        <td><?php echo e($value->gender->base_setup_name); ?></td>
                                        <td>
                                            <div class="cbx">
                                                <input <?php if($value->is_trial == 1): ?> checked <?php endif; ?> value="<?php echo e($value->id); ?>" id="cbx" class="isTrialStatus" type="checkbox"/>
                                                <label for="cbx"></label>
                                                <svg width="15" height="14" viewbox="0 0 15 14" fill="none">
                                                <path d="M2 8.36364L6.23077 12L13 2"></path>
                                                </svg>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <?php echo app('translator')->get('lang.select'); ?>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="/feedback/<?php echo e($value->id); ?>">view</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script>
        $(".isTrialStatus").click(function(){
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "/is-trial-status/" + $(this).val(),
                success: function (data) {
                    console.log(data);
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\resources\views/backEnd/studentInformation/trial-list.blade.php ENDPATH**/ ?>