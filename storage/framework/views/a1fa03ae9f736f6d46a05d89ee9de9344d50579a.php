<?php $__env->startSection('mainContent'); ?>

<h1 style="font-size: 60px" class="mb-3">Additional Files</h1>

<?php if(count($publicFiles) > 0): ?>
    <h1 style="font-size: 40px" class="mb-3">Public Files</h1>
<?php endif; ?>
<?php $__currentLoopData = $publicFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <h1># <?php echo e($item->title); ?></h1>
    <p class="mt-3">Additional file : <a href="<?php echo e(asset($item->file)); ?>" download><?php echo e($item->file_name); ?></a></p>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php if($privateFiles != null): ?>

        <?php if(count($privateFiles) > 0): ?>
            <h1 style="font-size: 40px" class="mb-3">Private Files</h1>
        <?php endif; ?>

        <?php $__currentLoopData = $privateFiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <h1># <?php echo e($item->title); ?></h1>
            <p class="mt-3">Additional file : <a href="<?php echo e(asset($item->file)); ?>" download><?php echo e($item->file_name); ?></a></p>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('docs::layouts.document_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/Docs/Resources/views/additionalFiles.blade.php ENDPATH**/ ?>