
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
    <div class="banner">
    	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
    	<div class="button-container">
    		<div class="title d-flex justify-content-center">
                <h3>training</h3>
            </div>
    	</div>
    	
    </div>
   

    <section style="background:#fafaf4">
        <div class="container">
           
            <ul id="flexiselDemo3" style="background:#fafaf4">
                <li class="wrapper_1">
                    <img class="img-slider " src="/public/images/staff_workshop/01.jpg" />
                </li>
                <li class="wrapper_2">
                    <img class="img-slider " style="max-height:253px;width:100%" src="/public/images/staff_workshop/02.jpg" />
                </li>
                <li class="wrapper_3">
                    <img class="img-slider " src="/public/images/staff_workshop/03.jpg" />
                </li>
            </ul>
        </div>
       <div class="container">
            <img  width="100%" src="<?php echo e(asset('public/')); ?>/images/CHILDREN-01.png" alt=""> 
       </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/staff-workshop.blade.php ENDPATH**/ ?>