

<?php $__env->startSection('main_content'); ?>
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/css')); ?>/lightbox.css">
<?php $__env->stopPush(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('public/backEnd/js/')); ?>/lightbox-plus-jquery.js"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('css'); ?>
    <link href="/public/frontend/css/lifeSchool.css" rel="stylesheet">
<?php $__env->stopPush(); ?>
<section class="academics-area mt-20">
   <img width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an.jpeg" alt="">
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                 <div class="topnav">
                    <div class="d-flex justify-content-center">
                        <a href="">Our School Gallery</a>
                    </div>
                </div>
                <div class="row">
                    <?php if(!count($album)): ?>
                    <div class="col-lg-12 col-md-12">
                        <div class="text-center">
                            <p>Empty</p>
                        </div>
                    </div>
                    <?php else: ?>
                    <?php for($i = 0; $i < count($album); $i++): ?>
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="academic-item">
                                <div class="academic-img img-hover-zoom">
                                    <?php $__currentLoopData = $album[$i]["gallery"]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $img): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a href="/life-school/<?php echo e(str_replace(" ","-", $album[$i]['album']->title)); ?>">
                                        <div style="opacity: 0" onmouseover="hoverIn(this)" onmouseout="hoverOut(this)" class="overlayCategory">
                                            <div class="nameOfCategory"><?php echo e($album[$i]['album']->title); ?>(<?php echo e(count($album[$i]["gallery"])); ?>)</div>
                                        </div>
                                        <?php if($loop->index == 0): ?>
                                            <img src="public/uploads/category/<?php echo e($img['path']); ?>" alt="<?php echo e($img['created_at']); ?>" style="border-radius: 10px; object-fit: cover">
                                        <?php endif; ?>
                                    </a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/light_lifeSchool.blade.php ENDPATH**/ ?>