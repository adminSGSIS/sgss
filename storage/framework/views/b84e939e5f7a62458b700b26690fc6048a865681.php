
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
    <div>
        <div class="banner">
            <img class="banner" width="100%" src="<?php echo e(asset('public/images/banner/banner_03.jpg')); ?>" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>leadership team</h3>
                </div>
            </div>
        </div>
        
        <div class="staff d-flex justify-content-center c-color">
            <?php $__currentLoopData = $leaderships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leadership): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($leadership->designation_id == 13): ?>
                <div class="profile collumn">
                    <img src="<?php echo e(file_exists(@$leadership->staff_photo) ? asset($leadership->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal<?php echo e($leadership->user_id); ?>">
                        <?php echo e($leadership->full_name); ?>

                    </a>
                    <p class="staff-title"><?php echo e($leadership->designations->title); ?></p>
                </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $leaderships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leadership): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($leadership->designation_id == 7): ?>
                <div class="profile collumn">
                    <img src="<?php echo e(file_exists(@$leadership->staff_photo) ? asset($leadership->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal<?php echo e($leadership->user_id); ?>">
                        <?php echo e($leadership->full_name); ?>

                    </a>
                    <p class="staff-title"><?php echo e($leadership->designations->title); ?></p>
                </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $leaderships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leadership): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($leadership->designation_id == 6): ?>
                <div class="profile collumn">
                    <img src="<?php echo e(file_exists(@$leadership->staff_photo) ? asset($leadership->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal<?php echo e($leadership->user_id); ?>">
                        <?php echo e($leadership->full_name); ?>

                    </a>
                    <p class="staff-title"><?php echo e($leadership->designations->title); ?></p>
                </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $leaderships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$leadership): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($leadership->designation_id == 5): ?>
                <div class="profile collumn">
                    <img src="<?php echo e(file_exists(@$leadership->staff_photo) ? asset($leadership->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    
                    <a href="#" onclick="event.preventDefault();" class="btn-profile" data-toggle="modal"
                        data-target="#exampleModal<?php echo e($leadership->user_id); ?>">
                        <?php echo e($leadership->full_name); ?>

                    </a>
                    <p class="staff-title"><?php echo e($leadership->designations->title); ?></p>
                </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="c-color">
        <div class="container">
              <img width="100%" src="<?php echo e(url('public')); ?>/images/CHILDREN-01.png" alt="">
        </div>
    </div>
            <?php echo $__env->make('frontEnd.home.partials.info-teachers', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/staff/leadership.blade.php ENDPATH**/ ?>