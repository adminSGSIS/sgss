<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>FEE</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                <a href="<?php echo e(url('fee/create')); ?>">New fee</a>
            </div>
        </div>
    </div>
</section>
<section class="admin-visitor-area up_admin_visitor">
    <div class="container-fluid p-0">
        <form action="<?php echo e(url('fee/update').'/'.$fee->id); ?>" method="POST">
            <?php echo csrf_field(); ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="main-title">
                                    <h4>Update Fee</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-30 mt-3">
                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="text" required 
                                    name="fee_name" value="<?php echo e($fee->fee_name); ?>" placeholder="Fee Name *">
                                    <span class="focus-border"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="input-effect">
                                    <input class="primary-input form-control" type="number" required 
                                    name="amount" value="<?php echo e($fee->amount); ?>" placeholder="Fee Amount *">
                                    <span class="focus-border"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-40 mt-3">
                            <div class="col-12">
                                <div class="input-effect sm2_mb_20 md_mb_20">
                                    <select class="niceSelect w-100 bb form-control<?php echo e($errors->has('id_group') ? ' is-invalid' : ''); ?>" 
                                        name="id_group">
                                        <option data-display="Fee Group *" value="">Fee Group *</option>
                                        <?php $__currentLoopData = $groupFee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->id); ?>" <?php echo e($fee->id_group == $value->id ? 'selected': ''); ?>>
                                                <?php echo e($value->group_name); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span class="focus-border"></span>
                                    <?php if($errors->has('id_group')): ?>
                                        <span class="invalid-feedback invalid-select" role="alert">
                                            <strong><?php echo e($errors->first('id_group')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-40">
                        <div class="col-lg-12 text-center">
                            <button class="primary-btn fix-gr-bg">
                                <span class="ti-check"></span>
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/fee/edit.blade.php ENDPATH**/ ?>