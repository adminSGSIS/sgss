<div class="row mb-40 mt-30">
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('school_name') ? ' is-invalid' : ''); ?>" type="text" name="school_name" value="<?php echo e(old('school_name')); ?>">

      <label>School Name</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="school_name_feedback" role="alert"></span>
      <?php if($errors->has('school_name')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('school_name')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('location_or_country') ? ' is-invalid' : ''); ?>" type="text" name="location_or_country" value="<?php echo e(old('location_or_country')); ?>">

      <label>Location/Country</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="location_or_country_feedback" role="alert"></span>
      <?php if($errors->has('location_or_country')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('location_or_country')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('language_of_instruction') ? ' is-invalid' : ''); ?>" type="text" name="language_of_instruction" value="<?php echo e(old('language_of_instruction')); ?>">

      <label>Language of Instruction</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="language_of_instruction_feedback" role="alert"></span>
      <?php if($errors->has('language_of_instruction')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('language_of_instruction')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

</div>

<div class="row mb-40 mt-30">
  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('from') ? ' is-invalid' : ''); ?>" type="text" name="from" value="<?php echo e(old('from')); ?>">

      <label>From</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="from_feedback" role="alert"></span>
      <?php if($errors->has('from')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('from')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="input-effect">
      <input class="primary-input form-control<?php echo e($errors->has('to') ? ' is-invalid' : ''); ?>" type="text" name="to" value="<?php echo e(old('to')); ?>">

      <label>To</label>
      <span class="focus-border"></span>
      <span class="invalid-feedback" id="to_feedback" role="alert"></span>
      <?php if($errors->has('to')): ?>
      <span class="invalid-feedback" role="alert">
        <strong><?php echo e($errors->first('to')); ?></strong>
      </span>
      <?php endif; ?>
    </div>
  </div>
</div><?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/forms/step3.blade.php ENDPATH**/ ?>