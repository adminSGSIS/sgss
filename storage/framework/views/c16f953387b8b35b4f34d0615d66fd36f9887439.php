
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('Modules/Form/public')); ?>/css/enrolment.css" media='all' />
<link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/nice-select.css" media='all'/>
<link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/toastr.min.css"/>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
	<div class="banner">
	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>HOW TO APPLY</h3>
            <div id="scroll"></div>

        </div>      
	</div>
	
</div>
<div class="c-color">
	
	<div class="container enrol-form">

		<form method="post" action="<?php echo e(url('form/student-enrolment')); ?>" enctype="multipart/form-data">
			<input type="hidden" name="student_id" value="<?php echo e(isset($student) ? $student->id : ''); ?>">
			<input type="hidden" name="parent_selected" id="parent_selected" value="<?php echo e(isset($parent_find) ? $parent_find->id : ''); ?>">
			<input type="hidden" name="enrolment_id" id="enrolment_id" value="<?php echo e(isset($enrolment) ? $enrolment->id : ''); ?>">
			<?php echo csrf_field(); ?>
			<div id="step-1" class="collapse show">
				<?php echo $__env->make('form::steps.step-1', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			</div>
			
		<div class="btn-save">
			<button class="btn btn-primary" id="enrol-save" type="submit">Save</button>
		</div>
		</form>
	</div>
	<br><br>

</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(url('Modules/Form/public/js/printThis.js')); ?>"></script>
<script src="<?php echo e(url('Modules/Form/public/js/jquery.number.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/nice-select.min.js"></script>
<script src="<?php echo e(url('Modules/Form/public/js/enrolment.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/toastr.min.js"></script>
<?php echo Toastr::message(); ?>


<script type="text/javascript">
$('#parent-select').on('change', function(){
	if($('#parent-select option:selected').val()==0)
	{
		$('#parent_selected').val("0");
		// $('.main-family input').removeAttr('disabled');
		// $('.main-family input').removeAttr('readonly');
		$('.main-family input').val('');
	}
	var parents = 	<?php echo json_encode($parents); ?>;
	$.each(parents, function (index, value) {
	  if(value.id == $('#parent-select').val())
	  {
	  	$('#family').show();
	  	$('#parent_selected').val(value.id);

	  	$('#father_name').val(value.fathers_name);
	  	$('#father_nationality').val(value.fathers_nationality);
	  	$('#father_occupation').val(value.fathers_occupation);
	  	$('#father_company').val(value.fathers_company);
	  	$('#father_work_address').val(value.fathers_work_address);
	  	$('#father_work_email').val(value.father_name);
	  	$('#father_email').val(value.fathers_email);
	  	$('#father_language').val(value.fathers_first_language);

	  	$('#father_name2').val(value.fathers_name);
	  	$('#father_nationality2').val(value.fathers_nationality);
	  	$('#father_occupation2').val(value.fathers_occupation);
	  	$('#father_company2').val(value.fathers_company);
	  	$('#father_work_address2').val(value.fathers_work_address);
	  	$('#father_work_email2').val(value.father_name);
	  	$('#father_email2').val(value.fathers_email);
	  	$('#father_language2').val(value.fathers_first_language);

	  	$('#mother_name').val(value.mothers_name);
	  	$('#mother_nationality').val(value.mothers_nationality);
	  	$('#mother_occupation').val(value.mothers_occupation);
	  	$('#mother_company').val(value.mothers_company);
	  	$('#mother_work_address').val(value.mothers_work_address);
	  	$('#mother_work_email').val(value.mothers_email);
	  	$('#mother_email').val(value.mothers_email);
	  	$('#mother_language').val(value.mothers_first_language);

	  	$('#mother_name2').val(value.mothers_name);
	  	$('#mother_nationality2').val(value.mothers_nationality);
	  	$('#mother_occupation2').val(value.mothers_occupation);
	  	$('#mother_company2').val(value.mothers_company);
	  	$('#mother_work_address2').val(value.mothers_work_address);
	  	$('#mother_work_email2').val(value.mothers_email);
	  	$('#mother_email2').val(value.mothers_email);
	  	$('#mother_language2').val(value.mothers_first_language);

	  	$('#home_address').val(value.hcmc_address);
	  	$('#emer_name_1').val(value.emergency_name_1);
	  	$('#emer_name_2').val(value.emergency_name_2);
	  	$('#relationship_to_child').val(value.relationship_to_child);
	  	$('#emer_phone_1').val(value.emergency_phone_1);
	  	$('#emer_phone_2').val(value.emergency_phone_2);
	  	$('#emer_address').val(value.emergency_address);

	  }
	});
});
$('#parent-select').on('change', function(){
	if($('#parent-select option:selected').val()!=0)
	{
		var id = $('#parent-select option:selected').val();

		$.ajax({
                url: '/form/get-siblings/'+(id)+'',
                type: 'GET',
                dataType: 'html',
                
            }).done(function(siblings) {
                $.each(JSON.parse(siblings),function(index , value){
                	$('#list-sibling').append("<div class='row'><div class='col-md-5 col-sm-12 row'><div class='col-md-4 col-sm-12'><div class='sm-content'>child's name</div></div><div class='col-md-8 col-sm-12'><input readonly='' type='text' name='1st_child_name' class='b-bottom-input form-control' value='"+(value.full_name)+"'></div></div><div class='col-md-4 col-sm-6 row'><div class='col-6'><div class='sm-content'>Admissions date</div></div><div class='col-6'><input readonly='' type='text' name='1st_child_age' class='b-bottom-input form-control' value='"+(value.admission_date)+"'></div></div><div class='col-md-3 col-sm-6 row'><div class='col-6'><div class='sm-content'>Grade</div></div><div class='col-6'><input type='text' readonly='' name='1st_child_grade' class='b-bottom-input form-control' value='"+(value.class)+"'></div></div></div>"
                	);
                });
         
            });
	}
});
$('#search_enrol').click(function(){
	admission_no = $('#admission_no').val();
	url = "/form/enrolment-form/"+admission_no+""
	window.location.href =url;
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/enrolment-form.blade.php ENDPATH**/ ?>