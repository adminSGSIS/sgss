
<?php $__env->startSection('main_content'); ?>
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css"/>
    <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css"/> 
<?php $__env->stopPush(); ?>
<div class="banner">
	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3><?php echo e(strtoupper($dayOfWeek)); ?> MENU</h3>
        </div>
	</div>
</div>
<div class="main">
		<div class="week-first-half content menu-food">
			<div class="container" style="padding-bottom: 30px;text-align:center">
				<a href="<?php echo e(url('parents/menu/monday')); ?>" type="button" class="btn-menu  btn-sm <?php if(strtolower($dayOfWeek) == 'monday'): ?> select <?php endif; ?>">Monday</a>
				<a href="<?php echo e(url('parents/menu/tuesday')); ?>" type="button" class="btn-menu   btn-sm <?php if(strtolower($dayOfWeek) == 'tuesday'): ?> select <?php endif; ?>">Tuesday</a>
				<a href="<?php echo e(url('parents/menu/wednesday')); ?>" type="button" class="btn-menu   btn-sm <?php if(strtolower($dayOfWeek) == 'wednesday'): ?> select <?php endif; ?>">Wednesday</a>
				<a href="<?php echo e(url('parents/menu/thursday')); ?>" type="button" class="btn-menu   btn-sm <?php if(strtolower($dayOfWeek) == 'thursday'): ?> select <?php endif; ?>">Thursday</a>
				<a href="<?php echo e(url('parents/menu/friday')); ?>" type="button" class="btn-menu   btn-sm <?php if(strtolower($dayOfWeek) == 'friday'): ?> select <?php endif; ?>">Friday</a>
			</div>
			<div class="table-responsive">
			<table class="table container table-bordered">
			  <thead class="table-primary">
			    <tr>
			      <td scope="row"><h5>Hot Dishes / Các món nóng</h5></td>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hotdishes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  	<?php if($hotdishes->food->id == 1): ?>
			    <tr>
			      <th scope="row"><?php echo e($hotdishes->food->name); ?></th>
			    </tr>
			    <?php endif; ?>
			    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			  </tbody>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Vegetarian / Các món chay </h5></td>
			    </tr>
			  </thead>
			  <tbody>
			    <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hotdishes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  	<?php if($hotdishes->food->id == 2): ?>
			    <tr>
			      <th scope="row"><?php echo e($hotdishes->food->name); ?></th>
			    </tr>
			    <?php endif; ?>
			    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Salad & vegetables / Salad và rau củ </h5></td>
			    </tr>
			  </thead>
			  <tbody>
			    <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hotdishes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  	<?php if($hotdishes->food->id == 3): ?>
			    <tr>
			      <th scope="row"><?php echo e($hotdishes->food->name); ?></th>
			    </tr>
			    <?php endif; ?>
			    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			  </tbody>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Hams / Các loại thịt nguội</h5></td>
			    </tr>
			  </thead>
			  <tbody>
			    <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hotdishes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			  	<?php if($hotdishes->food->id == 4): ?>
			    <tr>
			      <th scope="row"><?php echo e($hotdishes->food->name); ?></th>
			    </tr>
			    <?php endif; ?>
			    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			  </tbody>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Dessert / Tráng miệng</h5></td>
			    </tr>
			  </thead>
			  <tbody>
			  	<tr>
			  		<th class="table-primary" scope="row">FRUITS / TRÁI CÂY - FRUIT JUICE / NƯỚC ÉP - PASTRIES / BÁNH</th>
			  	</tr>
			  </tbody>
			  <thead class="table-primary">
			    <tr>
			      <td><h5>Snack / Ăn nhẹ buổi chiều</h5></td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <?php $__currentLoopData = $menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hotdishes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				  	<?php if($hotdishes->food->id == 5): ?>
				    <tr>
				      <th scope="row"><?php echo e($hotdishes->food->name); ?></th>
				    </tr>
				    <?php endif; ?>
				  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			    </tr>
			  </tbody>
			  
			
			</table>
		</div>
		</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\resources\views/frontEnd/home/layout/menu.blade.php ENDPATH**/ ?>