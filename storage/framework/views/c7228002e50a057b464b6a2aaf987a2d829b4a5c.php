
<?php $__env->startSection('mainContent'); ?>
    <form method="Get" action="<?php echo e(route('ShowPaySlip')); ?>">
<?php echo csrf_field(); ?>
<div class="white-box">
	<h2 class="center" style="text-align: center;">Create Quotation</h2>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<select required="" name="student_id" id="student_list"  class="niceSelect w-100 bb form-control<?php echo e(@$errors->has('students') ? ' is-invalid' : ''); ?>" >
					<option data-display="<?php echo app('translator')->get('student name'); ?> *" value="" ><?php echo app('translator')->get('student name'); ?> *</option>
					<?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					    <?php if(isset($add_income)): ?>
					    <option value="<?php echo e(@$student->id); ?>"
					        <?php echo e(@$add_income->student_id == @$student->id? 'selected': ''); ?>><?php echo e(@$student->full_name); ?>  |  <?php echo e(@$student->admission_no); ?></option>
					    <?php else: ?>
					    <option label="<?php echo e(@$student->admission_no); ?>" value="<?php echo e(@$student->id); ?>" <?php echo e(old('student_list') == @$student->id? 'selected' : ''); ?>><?php echo e(@$student->full_name); ?>  |  <?php echo e(@$student->admission_no); ?></option>
					    <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</select>
			</div>
			<div class="col-lg-3 col-md-3">
 				
			</div>
            
		</div>
        <div class="d-flex justify-content-end" style="padding-top: 35px">
            <input type="submit" class="primary-btn small fix-gr-bg" value="search">
        </div>
	</div>
</div>

<br>
</div>
</form>



<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
	$('#student_list').on('change',function(){
		var id = $('#student_list option:selected').attr('label');
		$('#admission_no').val(id);
	});	
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/payslip-search.blade.php ENDPATH**/ ?>