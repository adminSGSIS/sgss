
<?php $__env->startSection('mainContent'); ?>
<section class="sms-breadcrumb mb-40 white-box">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <h1>Medicine</h1>
            <div class="bc-pages">
                <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>

            </div>
        </div>
    </div>
</section>

<section class="admin-visitor-area up_st_admin_visitor">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main-title">
                            <h3 class="mb-30"><?php if(isset($editData)): ?>
                                <?php echo app('translator')->get('lang.edit'); ?>
                                <?php else: ?>
                                <?php echo app('translator')->get('lang.add'); ?>
                                <?php endif; ?>
                                Medicine
                            </h3>
                        </div>
                        <?php if(isset($editData)): ?>
                        <form action="<?php echo e(route('medicine-update',$editData->id)); ?>" method="post">
                            <!--Update-->
                            <?php echo csrf_field(); ?>
                            <?php else: ?>
                            <form action="<?php echo e(route('medicine-store')); ?>" method="POST">
                                <!--Create-->
                                <?php echo csrf_field(); ?>
                                <?php endif; ?>
                                <div class="white-box">
                                    <div class="add-visitor">
                                        <div class="row  mt-25">
                                            <div class="col-lg-12">
                                                <select name="medicinetype" id="symtomsType"
                                                    class="form-control w-100 niceSelect bb">
                                                    <option selected disabled><label for="symtomsType">Medicine
                                                            Type</label></option>
                                                    <?php $__currentLoopData = $medicine_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $medicine_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($medicine_type->id); ?>"
                                                        >
                                                        <?php echo e($medicine_type->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row  mt-25">
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text"
                                                        name="medicinecode" required
                                                        value="<?php echo e(!empty($editData)? $editData->medicine_code: ''); ?>">
                                                    <label><?php echo app('translator')->get('Medicine code'); ?> <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="name"
                                                        value="<?php echo e(!empty($editData)? $editData->medicine_name: ''); ?>"
                                                        required>
                                                    <label><?php echo app('translator')->get('lang.name'); ?> <span>*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row  mt-25">
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="unit"
                                                        value="<?php echo e(!empty($editData)? $editData->unit: ''); ?>" required>
                                                    <label><?php echo app('translator')->get('Unit'); ?> <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="price"
                                                        value="<?php echo e(!empty($editData)? $editData->price: ''); ?>" required>
                                                    <label><?php echo app('translator')->get('Price'); ?> <span>*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row  mt-25">
                                            <div class="col-lg-6">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control date" type="text"
                                                            name="manufacturingdate" id="apply_date" required
                                                            value="<?php echo e(!empty($editData)? $editData->manufacturing_date: ''); ?>">
                                                        <label><?php echo app('translator')->get('Manufacturing Date'); ?> <span>*</span></label>
                                                        <span class="focus-border"></span>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="" type="button">
                                                            <i class="ti-calendar" id="apply_date_icon"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="no-gutters input-right-icon">
                                                    <div class="input-effect">
                                                        <input class="primary-input form-control date" type="text"
                                                            name="expirydate" id="expiry_date" required
                                                            value="<?php echo e(!empty($editData)? $editData->expiry_date: ''); ?>">
                                                        <label><?php echo app('translator')->get('Expiry Date'); ?> <span>*</span></label>
                                                        <span class="focus-border"></span>
                                                    </div>
                                                    <div class="col-auto">
                                                        <button class="" type="button">
                                                            <label for="expiry_date"><i class="ti-calendar"
                                                                    id="expiry_date_icon"></i></label>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row  mt-25">
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text"
                                                        name="producer"
                                                        value="<?php echo e(!empty($editData)? $editData->producer: ''); ?>" required>
                                                    <label><?php echo app('translator')->get('Producer'); ?> <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="input-effect">
                                                    <input class="primary-input form-control" type="text" name="stock"
                                                        value="<?php echo e(!empty($editData)? $editData->stock: ''); ?>" required>
                                                    <label><?php echo app('translator')->get('Stock'); ?> <span>*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="<?php echo e(!empty($editData)? $editData->id: ''); ?>">
                                        <div class="row mt-40">
                                            <div class="col-lg-12 text-center">
                                                <button class="primary-btn fix-gr-bg" data-toggle="tooltip"
                                                    title="<?php echo e(@$tooltip); ?>">
                                                    <span class="ti-check"></span>
                                                    <?php echo e(!isset($editData)? "save":"update"); ?>

                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Medicine <?php echo app('translator')->get('lang.list'); ?></h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                                <?php if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(session()->get('message-success-delete')); ?>

                                        </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                        <div class="alert alert-danger">
                                            <?php echo e(session()->get('message-danger-delete')); ?>

                                        </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <th>No.</th>
                                    <th><?php echo app('translator')->get('Medicine Type'); ?></th>
                                    <th><?php echo app('translator')->get('Medicine code'); ?></th>
                                    <th><?php echo app('translator')->get('Medicine name'); ?></th>
                                    <th><?php echo app('translator')->get('Unit'); ?></th>
                                    <th><?php echo app('translator')->get('Price'); ?></th>
                                    <th><?php echo app('translator')->get('Manufacturing date'); ?></th>
                                    <th><?php echo app('translator')->get('Expiry Date'); ?></th>
                                    <th><?php echo app('translator')->get('Producer'); ?></th>
                                    <th><?php echo app('translator')->get('Stock'); ?></th>
                                    <th><?php echo app('translator')->get('lang.action'); ?></th>
                                </tr>
                            </thead>
                            <?php $count=1; ?>
                            <tbody>
                                <?php $__currentLoopData = $medicines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <th><?php echo e($count++); ?></th>
                                    <th><?php echo e($value->medicineTypeRelation->name); ?></th>
                                    <th><?php echo e($value->medicine_code); ?></th>
                                    <th><?php echo e($value->medicine_name); ?></th>
                                    <th><?php echo e($value->unit); ?></th>
                                    <th><?php echo e($value->price); ?></th>
                                    <th><?php echo e($value->manufacturing_date); ?></th>
                                    <th><?php echo e($value->expiry_date); ?></th>
                                    <th><?php echo e($value->producer); ?></th>
                                    <th><?php echo e($value->stock); ?></th>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                <?php echo app('translator')->get('lang.select'); ?>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">

                                                <a class="dropdown-item"
                                                    href="<?php echo e(url('medical/medicine-edit/'.$value->id)); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                                <a href="<?php echo e(url('medical/medicine-delete/'.$value->id)); ?>"
                                                    class="dropdown-item"><?php echo app('translator')->get('lang.delete'); ?></a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/Medical\Resources/views/Medicine/medicine.blade.php ENDPATH**/ ?>