
<?php $__env->startSection('mail-content'); ?>
  <!--Content-->
    <div style="text-align:center">
        <h1 style="text-align:center">FEEDBACK FORM</h1>
        <h2>Teacher's Name: <?php echo e($data1['teacher-name']); ?></h2>
    </div>
    <table class="table" style="text-align: center">
        <tbody>
            <tr>
                <td colspan="6">
                    Student's name: <b><?php echo e($data1['studentName']); ?></b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Father's name: <b><?php echo e($data1['studentFather']); ?></b>
                </td>
                <td colspan="3">
                    Father's phone: <b><?php echo e($data1['fatherPhone']); ?></b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Mother's name: <b><?php echo e($data1['studentMother']); ?></b>
                </td>
                <td colspan="3">
                    Mother's phone: <b><?php echo e($data1['motherPhone']); ?></b>
                </td>
            </tr>
            <tr>
                <td>
                    Listening: <b><?php echo e($data1['listen']); ?></b>.
                </td>
                <td>
                   Speaking:  <b><?php echo e($data1['speaking']); ?></b>.
                </td>
                <td>
                    Reading: <b><?php echo e($data1['reading']); ?></b>.
                </td>
                <td colspan="3">
                    Writing: <b><?php echo e($data1['writing']); ?></b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    EAL Assessments:  <b><?php echo e($data1['ela-assessments']); ?></b>
                 </td>
                <td colspan="3">
                    EAL Support: <b><?php echo e($data1['eal-support']); ?></b> (hour/week)
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    SEN Assessments: <b><?php echo e($data1['sen']); ?></b>
                </td>
                <td colspan="3">
                    SEN Support: <b><?php echo e($data1['sen-support']); ?></b> (hour/week)
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    CAT4 Assessment: <b><?php echo e($data1['cat4-assessment']); ?></b>
                </td>
                <td>
                    Quantitative: <b><?php echo e($data1['quantitative']); ?></b>
                </td>
                <td>
                    Verbal: <b><?php echo e($data1['verbal']); ?></b>
                </td>
                <td>
                    Non-verbal: <b><?php echo e($data1['non-verbal']); ?></b>
                </td>
                <td>
                    Spatial: <b><?php echo e($data1['spatia_l']); ?></b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Class teacher: <b><?php echo e($data1['class-teacher']); ?></b>
                </td>
                <td colspan="3">
                    Head teacher recommendation: <b><?php echo e($data1['head-teacher']); ?></b>
                </td>
            </tr>
        </tbody>
    </table>
    <!--Content End-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd/mails/mail_template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/mails/feedback_mail.blade.php ENDPATH**/ ?>