
<?php $__env->startSection('mainContent'); ?>
<h2>MENU SETTING</h2>
	<div class="row">
        
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="<?php echo e(url('lunch-menu/monday')); ?>" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Monday</h3>
                            <p class="mb-0">Monday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                        
                                                    </h1>
                    </div>
                </div>
            </a>
        </div>
                
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="<?php echo e(url('lunch-menu/tuesday')); ?>" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Tuesday</h3>
                            <p class="mb-0">Tuesday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                        
                        </h1>
                    </div>
                </div>
            </a>
        </div>
                        
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="<?php echo e(url('lunch-menu/wednesday')); ?>" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Wednesday</h3>
                            <p class="mb-0">Wednesday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                        
                        </h1>
                    </div>
                </div>
            </a>
        </div>
                
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="<?php echo e(url('lunch-menu/thursday')); ?>" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Thursday</h3>
                            <p class="mb-0">Thursday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                      
                                                  </h1>
                   </div>
               </div>
           </a>
       </div>
       <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="<?php echo e(url('lunch-menu/friday')); ?>" class="d-block">
                <div class="white-box single-summery">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h3>Friday</h3>
                            <p class="mb-0">Friday menu</p>
                        </div>
                        <h1 class="gradient-color2">
                                                      
                                                  </h1>
                   </div>
               </div>
           </a>
       </div>
          </div>
<br><br>
<h2>FOOD SETTING</h2>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="white-box mt-30">
                        <form action="<?php echo e(url('add-food-2')); ?>" method="POST">
                            <?php echo csrf_field(); ?>
                        
                            <div class="add-visitor">
                                <div class="row  mt-25">
                                    <div class="col-lg-12">
                                        <div class="input-effect">
                                            <input class="primary-input form-control" type="text" name="name" value="" maxlength="100">
                                            <label>Food name<span>*</span></label>
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row  mt-25">
                                    <div class="col-lg-12">
                                        <select class="niceSelect w-100 bb form-control" name="type" style="display: none;">
                                            <option data-display="Select Food type*" value="">Food type</option>
                                            <?php $__currentLoopData = $foodtypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $foodtype): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option data-display="<?php echo e($foodtype->name); ?>" value="<?php echo e($foodtype->id); ?>"><?php echo e($foodtype->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                     </div>  
                                </div>
                                <div class="row mt-40">
                                    <div class="col-lg-12 text-center">
                                        <button class="primary-btn fix-gr-bg" data-toggle="tooltip" title="" data-original-title="">
                                            <span class="ti-check"></span>
                                            Save                                        </button>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <div class="white-box mt-30">
                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">
                            <thead>
                                <?php if(session()->has('message-success-delete') != "" ||
                                session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(session()->get('message-success-delete')); ?>

                                        </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                        <div class="alert alert-danger">
                                            <?php echo e(session()->get('message-danger-delete')); ?>

                                        </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <th width="40%"><?php echo app('translator')->get('lang.name'); ?></th>
                                    <th><?php echo app('translator')->get('lang.type'); ?></th>
                                    <th><?php echo app('translator')->get('lang.action'); ?></th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $__currentLoopData = $foods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $food): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($food->name); ?></td>
                                    <td><?php echo e($food->food_type->name); ?></td>
                                    
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                <?php echo app('translator')->get('lang.select'); ?>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a onclick="return confirm('Delete food?')" class="dropdown-item deleteAddIncomeModal" href="/delete-food/<?php echo e($food->id); ?>" ><?php echo app('translator')->get('lang.delete'); ?></a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/backEnd/kitchen/lunchmenu.blade.php ENDPATH**/ ?>