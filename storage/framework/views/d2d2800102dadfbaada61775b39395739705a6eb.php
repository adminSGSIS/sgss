
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('Modules/Form/public')); ?>/css/enrolment.css" media='all' />
<link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/nice-select.css" media='all'/>
<link rel="stylesheet" href="<?php echo e(asset('public/backEnd/')); ?>/vendors/css/toastr.min.css"/>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
	<div class="banner">
	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>How to apply?</h3>
            <div id="scroll"></div>

        </div>      
	</div>
	
</div>
<div class="c-color">
	<div class="container enrol-form">

		<form method="post" action="<?php echo e(url('form/application-parent')); ?>" enctype="multipart/form-data">
			<input type="hidden" name="student_id" value="<?php echo e(isset($student) ? $student->id : ''); ?>">
			<input type="hidden" name="parent_selected" id="parent_selected" value="<?php echo e(isset($parent_find) ? $parent_find->id : ''); ?>">
			<input type="hidden" name="enrolment_id" id="enrolment_id" value="<?php echo e(isset($enrolment) ? $enrolment->id : ''); ?>">
			<input type="hidden" name="admission_no" id="enrolment_id" value="<?php echo e(isset($admission_no) ? $admission_no : ''); ?>">
			
			<?php echo csrf_field(); ?>
			<div id="step-2" class="collapse show">
				<?php echo $__env->make('form::steps.step-2', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			</div>
			<div id="step-3" class="collapse show">
				<?php echo $__env->make('form::steps.step-3', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
			</div>
			
		<div class="btn-save">
			<button class="btn btn-primary" id="enrol-save" type="submit">Save</button>
		</div>
		</form>
	<br><br>

</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?php echo e(url('Modules/Form/public/js/printThis.js')); ?>"></script>
<script src="<?php echo e(url('Modules/Form/public/js/jquery.number.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/nice-select.min.js"></script>
<script src="<?php echo e(url('Modules/Form/public/js/enrolment.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/backEnd/')); ?>/vendors/js/toastr.min.js"></script>
<?php echo Toastr::message(); ?>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/application.blade.php ENDPATH**/ ?>