

<?php if($cate_post): ?>
    <title><?php echo e($cate_post->news_title); ?></title>
<?php endif; ?>
<?php $__env->startSection('main_content'); ?>
    <?php $__env->startPush('css'); ?>
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/css')); ?>/lightbox.css">
        <link href="<?php echo e(asset('public')); ?>/frontEnd/css/lifeSchool.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
        <link rel="stylesheet" href="<?php echo e(asset('public/frontEnd/css/newcss.css')); ?>" />
    <?php $__env->stopPush(); ?>


    <?php if($imagesPhoto): ?>
        <div class="banner">
            <img class="banner" width="100%" src="<?php echo e(asset('public/images/banner/banner_03.jpg')); ?>" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3><?php echo e($imagesAlbum->title); ?></h3>
                </div>
            </div>
        </div>
        <?php if($cate_post): ?>
            <div class="catePost c-color">
                <div class="paragraph">
                    <p><?php echo $cate_post->news_body; ?></p>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <div class="banner">
            <img class="banner" width="100%" src="<?php echo e(asset('public/images/banner/banner_03.jpg')); ?>" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3><?php echo e($cate_post->news_title); ?></h3>
                </div>
            </div>
        </div>
        <div class="catePost c-color">

            <div class="paragraph">
                <p><?php echo $cate_post->news_body; ?></p>
            </div>
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/light_lifeSchoolDetail.blade.php ENDPATH**/ ?>