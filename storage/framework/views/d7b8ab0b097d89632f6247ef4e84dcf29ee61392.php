<?php $__env->startSection('css'); ?>
<style>
   .centerTd td, .centerTh th{
       text-align: center;
   }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('mainContent'); ?>
    <section class="sms-breadcrumb mb-40 white-box">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <h1>Device Management</h1>
                <div class="bc-pages">
                    <a href="<?php echo e(url('dashboard')); ?>"><?php echo app('translator')->get('lang.dashboard'); ?></a>
                    <a href="<?php echo e(url('devices/repaired')); ?>">Repaired Devices</a>
                </div>
            </div>
        </div>
    </section>
    <section class="admin-visitor-area up_admin_visitor">
        <div class="row">
            <div class="col-lg-12 mt-5">
                <div class="row">
                    <div class="col-lg-4 no-gutters">
                        <div class="main-title">
                            <h3 class="mb-0">Repairing Devices List</h3>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <table id="table_id" class="display school-table" cellspacing="0" width="100%">

                            <thead>
                            <?php if(session()->has('message-success-delete') != "" ||
                            session()->get('message-danger-delete') != ""): ?>
                                <tr>
                                    <td colspan="7">
                                        <?php if(session()->has('message-success-delete')): ?>
                                            <div class="alert alert-success">
                                                <?php echo e(session()->get('message-success-delete')); ?>

                                            </div>
                                        <?php elseif(session()->has('message-danger-delete')): ?>
                                            <div class="alert alert-danger">
                                                <?php echo e(session()->get('message-danger-delete')); ?>

                                            </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr class="centerTh">
                                <th>Device Code</th>
                                <th>Device name</th>
                                <th>Staff Name</th>
                                <th>Notes</th>
                                <th>action</th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php $__currentLoopData = $repairDevice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="centerTd">
                                        <?php
                                            $device = Modules\Devices\Entities\SmDevice::find($value->device);
                                        ?>
                                        <td>
                                            <img src="<?php echo e(asset($device->qr_code_path)); ?>" alt="">
                                        </td>
                                        <td><?php echo e($device->device_name); ?></td>
                                        <td><?php echo e(App\SmStaff::find($value->staff_id)->full_name); ?></td>
                                        <td><?php echo e($device->notes); ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                                    <?php echo app('translator')->get('lang.select'); ?>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a href="<?php echo e(url('devices/view/'.$device->id)); ?>" class="dropdown-item small fix-gr-bg modalLink" title="Device Details" data-modal-size="modal-md">view</a>
                                                    <a class="dropdown-item" href="<?php echo e(url('devices/repair/edit/'.$value->id)); ?>"><?php echo app('translator')->get('lang.edit'); ?></a>
                                                    <a class="dropdown-item" href="<?php echo e(url('devices/repair/done/'.$value->id)); ?>">Repair Done</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/Devices/Resources/views/repairingDevice.blade.php ENDPATH**/ ?>