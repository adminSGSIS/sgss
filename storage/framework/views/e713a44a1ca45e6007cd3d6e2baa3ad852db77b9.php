
<style type="text/css">
	.weekend{
		color: rgb(255, 238, 137);
		
	}
	.absent,.late,.halfday{
		color :#3234a8;
	}
	.leave{
		
		color: red;
	}
	.present{
		
		color: green;
	}
	.holiday{
		
		color: blue;
	}
	.square{
		width: 20px;
		height: 20px;
	}
	.hl{
		background-color: blue;
	}
	.wk{
		background-color: rgb(255, 238, 137);
	}
	.lv{
		background-color: red;
	}
	.as{
		background-color: #3234a8;
	}
	.pr{
		background-color: green;
	}
	.hd{
		width: 100px;
	}
	.width-40{
		width: 25%;
	}
	.width-60{
		width: 75%;
	}
	table{
		background: darkgrey;
	}
	tr{
		background-color: #fff;
	}
</style>
<?php $__env->startSection('mainContent'); ?>

<?php
	function checkweekend($day , $month , $year)
	{
		$date = $year.'-'.$month.'-'.$day;
        $MyGivenDateIn = strtotime($date);
        $ConverDate = date("l", $MyGivenDateIn);
        $ConverDateTomatch = strtolower($ConverDate);
        
        if(($ConverDateTomatch == "saturday" )|| ($ConverDateTomatch == "sunday")){
            return true;
        } else {
            return false;
        }
	}

	function checkLeaveDate($day , $month , $year , $leaveFrom , $leaveTo)
	{
		$date = strtotime($day.'-'.$month.'-'.$year);
		$from = strtotime($leaveFrom);
		$to = strtotime($leaveTo);

		if($date >= $from && $date <= $to)
			{
				return true;
			}
		else
			{
				return false;
			}
	}

	function checkHoliday($day , $month , $year , $holidayFrom , $holidayTo)
	{
		$date = strtotime($day.'-'.$month.'-'.$year);
		$from = strtotime($holidayFrom);
		$to = strtotime($holidayTo);
		if($date >= $from && $date <= $to)
			{
				return true;
			}
		else
			{
				return false;
			}
	}

	function checkAttendance($day , $month , $year , $attend_date , $attend_type)
	{
		$date = strtotime($day.'-'.$month.'-'.$year);
		$date_attend = strtotime($attend_date);
		if($date == $date_attend && $attend_type == 'L')
			{return 'L';}
		if($date == $date_attend && $attend_type == 'A')
			{return 'A';}
		if($date == $date_attend && $attend_type == 'F')
			{return 'F';}
		if($date == $date_attend && $attend_type == 'P')
			{return 'P';}
	}
?>

	<h2>Workday <?php echo e($user->full_name); ?></h2>
	<div class="white-box">
		<div class="row">
			<div class="col-lg-4">
				<?php if(isset($employees)): ?>
					<select class="niceSelect w-100 bb" name="status" id="selectEmployee">
						<option value="0"><?php echo e(Auth::user()->staff->departments->name); ?> Department</option>
						<?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<option value="<?php echo e($employee->staff_user->id); ?>" <?php echo e($employee->staff_user->id == $id? 'selected' : ''); ?>><?php echo e($employee->full_name); ?></option>
                    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                	</select>
				<?php endif; ?>
			</div>
			<div class="col-lg-4">
				<select class="niceSelect w-100 bb" name="status" id="selectYear">
					<?php $thisyear = date('Y') ?>
					<?php for($i = 8 ; $i >= 1 ; $i--): ?>
					<option value="<?php echo e($thisyear-$i); ?>" <?php echo e($thisyear-$i == $year ? 'selected' : ''); ?>><?php echo e($thisyear-$i); ?></option>
					<?php endfor; ?> 
					<option value="<?php echo e($thisyear); ?>" <?php echo e($thisyear == $year ? 'selected' : ''); ?>><?php echo e($thisyear); ?></option>
                    
                </select>
			</div>
			<div class="col-lg-4">
				Total leave: <?php echo e($totalLeaves); ?> / 12
			</div>

		</div>
		<?php if(isset($employees)): ?>
		<div class="col-lg-12 mt-20 text-right">
            <button type="submit" class="primary-btn small fix-gr-bg" id="btnsubmit">
                <span class="ti-search pr-2"></span>
                Search
            </button>
        </div>
        <?php endif; ?>
	</div>
	<br>
	<div class="white-box">
		<table class="table-bordered table table-responsive">
			<thead>
				<tr>
					<td>Month name</td>
					<?php for($i = 1 ; $i <= 31 ; $i++): ?>
					<td><?php echo e($i); ?></td>
					<?php endfor; ?>
				</tr>
				<?php $__currentLoopData = $date; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr>
						<td><?php echo e(date("F", mktime(0, 0, 0, $key, 10))); ?></td>
						<?php for($i = 1 ; $i<= $value ; $i++): ?>
							<td class=
							"
								<?php if(checkweekend($i , $key , $year) == true): ?>
									<?php echo e('weekend'); ?>

								<?php else: ?>
									<?php $check = 0 ?>
									<?php $__currentLoopData = $attendances; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attendance): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php switch(checkAttendance($i , $key , $year , $attendance->attendence_date , $attendance->attendence_type)):
											case ('A'): ?>
												<?php echo e('absent'); ?> 
												<?php echo e($check = 1); ?>

												<?php break; ?>
											<?php case ('L'): ?>
												<?php echo e('late'); ?>

												<?php echo e($check = 1); ?>

												<?php break; ?>
											<?php case ('F'): ?>
												<?php echo e('halfday'); ?>

												<?php echo e($check = 1); ?>

												<?php break; ?>
											<?php case ('P'): ?>
												<?php echo e('present'); ?>

												<?php echo e($check = 1); ?>

												<?php break; ?>
											<?php default: ?>
												<?php echo e(''); ?>

												<?php echo e($check = 0); ?> 
												<?php break; ?>
										<?php endswitch; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php if($check == 0): ?>
										<?php $checkHoliday=false ?>
										<?php $__currentLoopData = $holidays; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $holiday): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<?php echo e(checkHoliday($i , $key , $year , $holiday->from_date , $holiday->to_date) == true ? $checkHoliday = true : ''); ?>

										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										<?php if($checkHoliday == true): ?>
											<?php echo e('holiday'); ?>

										<?php else: ?>
											<?php $__currentLoopData = $leaveDates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $leaveDate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php echo e(checkLeaveDate($i , $key , $year , $leaveDate->leave_from , $leaveDate->leave_to) == true ? 'leave' : ''); ?>

											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										<?php endif; ?>
									<?php endif; ?>
								<?php endif; ?>
							"
							></td>
						<?php endfor; ?>
					</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</thead>
		</table>
		<br>
		<div class="row">
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square wk">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Weekend
						</span>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square lv">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Leave
						</span>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square hl">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Holiday
						</span>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square as">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Absent / Late / Halfday
						</span>
					</div>
				</div>
			</div>
			<div class="col-2">
				<div class="row">
					<div class="col-2">
						<div class="square pr">
				
						</div>
					</div>
					<div class="col-8">
						<span >
							Present
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="id_user" value="<?php echo e($id); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
	$( document ).ready(function() {
		$('.holiday').text('H');
    	$('.leave').text('L');
    	$('.weekend').text('W');
    	$('.absent').text('A');
    	$('.late').text('L');
    	$('.halfday').text('F');
    	$('.present').text('P');
    	
	});

 	$('#btnsubmit').click(function(){
 		var id = $('#selectEmployee').val();
 		var year = $('#selectYear').val();
 		var url = '<?php echo e(url('workday')); ?>'+'/'+(id)+'/'+(year);
 		if(id == 0)
 		{
 			return false;
 		}
 		if (url) { // require a URL
              window.location = url; // redirect
          }
        return false;
 	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backEnd.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\WWW\SGS\sgss\Modules/PayRoll\Resources/views/workday.blade.php ENDPATH**/ ?>