
<?php $__env->startSection('main_content'); ?>
    <?php $__env->startPush('css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
        <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css" />
    <?php $__env->stopPush(); ?>

    <div class="banner">
        <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>Parent teacher group</h3>
            </div>
        </div>
    </div>
    <div class="main_content" style="background:#fafaf4">
        <div class="container d-flex justify-content-center">
            <div class="col-lg-10 col-sm-12">
                <img width="100%" style="border-radius:15px" src="<?php echo e(asset('public/')); ?>/images/parent-teacher-group.jpg" alt="">
            </div>
        </div>
        <div class="container">
              <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/CHILDREN-01.png" alt=""> 
        </div>
       
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/layout/parent_teacher_group.blade.php ENDPATH**/ ?>