
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>

    <body id="body" class="homepage">
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KFKSVRT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!-- Google Tag Manager -->
    <!--<script>-->
    <!--    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':-->
    <!--    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],-->
    <!--    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=-->
    <!--    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);-->
    <!--    })(window,document,'script','dataLayer','GTM-TJQF6T3');-->
    <!--</script>-->
    <!-- End Google Tag Manager -->
        <!-- Google Tag Manager (noscript) -->

        <!-- End Google Tag Manager (noscript) -->

        <div id="page" class="page">

            <!-- include keyboard jump links -->
            <ul class="jump-links">
                <li>
                    <a href="#mainArea" id="jumpToMain">
                        Jump to main content
                    </a>
                </li>
                <li>
                    <a href="#searchArea" id="jumpToSearch">
                        Jump to search
                    </a>
                </li>
                <li>
                    <a href="#footerArea">
                        Jump to the footer
                    </a>
                </li>
            </ul>


            <!--

        Header Area

      -->


            <!-- end Header Area -->
            <!--

        Main Content Area

      -->

            <main role="main" id="mainArea" class="main-base" style="padding-top: 0px;">

                <div
                    class="view view-annoucement view-id-annoucement view-display-id-block_for_homepage view-dom-id-5c93caff7f6c5a82c29dd3c1806fce81">
                </div>

                <section class="card card--video-bg card-callout--reverse txt-center stitle-home-primary-feature"
                    data-moduletitle="home-primary-feature">
                    <?php echo $__env->make('frontEnd.home.partials.banner', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </section>

                <section class="container">
                    <br>
                    <header>
                        <div class="title-homepage">
                            <h2>Welcome To Saigon Star International School</h2>
                        </div>
                    </header>
                    <div class="row">
                        <div class="col-12 video-home">
                            <img class="d-block w-100 video_0" src="<?php echo e(url('public')); ?>/images/main-thumbnail.jpg" width="100%">
                            <i class="fa fa-play-circle video_0" aria-hidden="true"></i>
                        </div>
                    </div>
                </section>

                  <section class="container" id="advantage">
                    <div class="title-homepage">
                        <h2>What makes SGSIS different?</h2>
                    </div>
                    <div class="row">
                        <div class="grid-container text-light">
                            <div class="item1 d-flex align-items-center">
                                <div class="content-wrap">
                                    <h3>a balance between academic achievement & personal development
                                    </h3>     
                                </div>
                                <div class="item-wrap">
                                    
                                </div>
                            </div>
                            <div class="item2 d-flex align-items-center d-flex justify-content-center">
                                    <div class="content-wrap">
                                        <h3>a quiet, spacious, peaceful </br>and green evironment with </br>
                                            more than 7,500m²
                                        </h3>
                                    </div>
                                <div class="item-wrap">
                                    
                                </div>
                            </div>
                            <div class="item3 d-flex align-items-center d-flex justify-content-center">
                                <div class="content-wrap">
                                    <h3>the only ipc-accredited school in vietnam
                                    </h3>
                                </div>
                                <div class="item-wrap">
                                    
                                </div>
                            </div>
                            <div class="item4 d-flex align-items-center d-flex justify-content-center">
                                <div class="content-wrap">
                                    <h3>a warm community among </br>parents - teachers - students
                                    </h3>
                                </div>
                                <div class="item-wrap">
                                    
                                </div>
                            </div>
                            <div class="item5 d-flex align-items-center d-flex justify-content-center">
                                <div class="content-wrap">
                                    <h3>small class size</h3>
                                </div>
                                <div class="item-wrap">
                                    
                                </div>
                            </div>
                            <div class="item6 d-flex align-items-center d-flex justify-content-center">
                                <div class="content-wrap">
                                    <h3>a truly international </br>learning</br> evironment
                                    </h3>
                                </div>
                                <div class="item-wrap">
                                   
                                </div>
                            </div> 
                        </div>
                    </div>
                </section>

                <section class="section-contain section-inforgraphic" id="advantage-mobile">
                    <div class="container">
                        <div class="title-homepage">
                            <h2>advantages</h2>
                        </div>
                        <div class="row">
                            <div class="">
                                <div class="row adv-mobile">
                                    <div class="left">
                                        <h3>a balance between academic achievement & personal development
                                        </h3>
                                    </div>
                                    <div class="right">
                                        <img src="<?php echo e(url('public')); ?>/images/advantage/mobile/01.png" alt="">
                                    </div>

                                    <div class="left">
                                        <img src="<?php echo e(url('public')); ?>/images/advantage/mobile/02.png" alt="">
                                    </div>
                                    <div class="right">
                                        <h3>a quiet, spacious, peaceful and green evironment with
                                                </br>more than 7,500 m²
                                        </h3>
                                    </div>

                                    <div class="left">
                                        <h3>
                                            small class </br>sizes (typically 12-16 </br>children  per </br>class, max 20)
                                        </h3>
                                    </div>
                                    <div class="right">
                                        <img src="<?php echo e(url('public')); ?>/images/advantage/mobile/03.png" alt="">      
                                    </div>

                                    <div class="left">
                                        <img src="<?php echo e(url('public')); ?>/images/advantage/mobile/04.png" alt="">
                                    </div>
                                    <div class="right">
                                        <h3>
                                        the only </br>ipc-accredited </br>school </br>in vietnam
                                       
                                        </h3>     
                                    </div>

                                    <div class="left">
                                        <h3> a warm community among </br> parents - teachers - students
                                        </h3>
                                    </div>
                                    <div class="right">
                                        <img src="<?php echo e(url('public')); ?>/images/advantage/mobile/05.png" alt="">    
                                    </div>

                                    <div class="left">
                                        <img src="<?php echo e(url('public')); ?>/images/advantage/mobile/06.png" alt="">    
                                    </div>
                                    <div class="right">
                                        <h3>a truly international</br> learning </br> evironment
                                        </h3>   
                                    </div>

                                </div>
                             
                            </div>
                        </div>
                    </div>
                </section>


                <section class="col-12" id="gallery-homepage">
                    <div class="gallery-homepage">
                        <div class="left ">
                            <a href="/early-year"><img src="/public/images/pic4.png" alt=""><a></a>
                        </div>
                        <div class="right">
                            <a href="/early-year"><p><i class="fa fa-circle "></i> early years</p></a>
                            <div class="content-gallery">
                             <a class="new-text-color">The program was designed in response to the demands of incorporating
                             a self-sustained international curriculum for learners in the Early Years 
                             age range of 2-5 years old which includes Pre-school, Nursery and Reception.
                             </a>    
                             <br>
                             <a href="/early-year">
                                    <i class="icon-link icon-right-arrow"></i>
                                </a>
                             </div>
                        </div>
                    </div>
                    <div class="gallery-homepage d-flex justify-content-end" style="margin-top:-5%">
                        <div class="left">
                            <a href="/primary-year"><p> primary years <i class="fa fa-circle "></i></p></a>
                            <div class="content-gallery-left">
                                 <a class="new-text-color">The IPC is our International Primary Curriculum for children aged 5 - 11 years old. 
                                 It is used by over 1000 schools in over 90 countries worldwide. It is a comprehensive, 
                                 thematic, creative curriculum, with a clear process of learning and specific learning goals
                                 for every subject. It also develops international mindedness and encourages personal learning.
                                </a> 
                                <br>
                                <a href="/primary-year">
                                    <i class="icon-link icon-left-arrow"></i>
                                </a>
                            </div>
                        </div>
                        <div class="right">
                            <a href="/primary-year"><img src="/public/images/pic5.jpg" alt=""></a>
                        </div>
                    </div>

                </section>
        
                <section class="col-12" id="gallery-home-mb">
                    <div class="gallery-home-mb">
                        <a href="/early-year"><img src="/public/images/pic4.png" alt=""></a>
                        <a href="/early-year"><h3>early years</h3></a>   
                        <p class="new-text-color">The program was designed in response to the demands to incorporate
                             a self-sustained international curriculum for learners in the Early Years 
                             age range of 2-5 years old which includes Nursery, Pre-school and Reception.
                        </p>    
                    </div> 
                    <div class="gallery-home-mb">
                        <a href="/primary-year"><img src="/public/images/pic5.jpg" alt=""></a>
                        <a href="/primary-year"><h3 style="color:#ffda05">primary years</h3></a>     
                        <p class="new-text-color">The IPC is our International Primary Curriculum for children aged 5 - 11 years old. 
                                 It is used by over 1000 schools in over 90 countries worldwide. It is a comprehensive, 
                                 thematic, creative curriculum, with a clear process of learning and specific learning goals
                                 for every subject. It also develops international mindedness and encourages personal learning.
                        </p>    
                    </div> 
                </section>      


                <section id="testimonials">
                    <div class="container">
                        <h2 id="slider-title">TESTIMONIALS</h2>
                        <ul id="flexiselDemo3">
                            <li class="wrapper_1">
                                <img class="img-slider video_1" src="/public/images/testimonial/ep1.jpg" />
                                <i class="fa fa-play-circle video_1" aria-hidden="true"></i>
                            </li>
                            <li class="wrapper_2">
                                <img class="img-slider video_2" src="/public/images/testimonial/ep2.jpg" />
                                <i class="fa fa-play-circle video_2" aria-hidden="true"></i>
                            </li>
                            <li class="wrapper_3">
                                <img class="img-slider video_3" src="/public/images/testimonial/ep3.jpg" />
                                <i class="fa fa-play-circle video_3" aria-hidden="true"></i>
                            </li>
                        </ul>
                    </div>
                    <div class="clearout"></div>
                </section>
                <section class="container contact pt-60">
                    <div class="contact-image-background">
                        <img src="/public/images/contact.png" width="100%">
                        <div class="booktour_mobile">
                            <a href="#" data-toggle="modal" data-target="#booktournew" data-whatever="@mdo">
                                <img src="<?php echo e(url('public/frontEnd')); ?>/assets/img/logo/booktour_icon.gif">
                            </a>
                        </div>
                    </div>


                    <form id="contact-form" method="POST" action="<?php echo e(url('/book-tour')); ?>">
                        <?php echo csrf_field(); ?>
                        
                        <div class="booktour_row">
                            <input type="text" id="parent" class="form-control" name="name" placeholder="*Parent's name "
                                required>
                            <input type="text" id="address" class="form-control" name="address" placeholder="*Address "
                                required>
                        </div>
                        <!--<div class="booktour_row">-->
                        <!--    <input type="text" id="purpose" class="form-control" name="purpose" placeholder="*Purpose "-->
                        <!--        required>-->
                        <!--    <input type="number" id="dateofbirth" class="form-control" name="no_of_person" required-->
                        <!--        placeholder="*No. of person">-->
                        <!--</div>-->
                        <div class="booktour_row">
                            <input type="text" id="email" class="form-control" name="email" placeholder="*Email" required>
                            <input type="text" id="telephone1" class="form-control b-t-phone" name="phone" placeholder="*Phone" required>
                        </div>
                        <div class="booktour_row fontcalender" >
                            <input type="text" id="visitday" class="form-control" name="date"
                                placeholder="dd/mm/yyyy" required autocomplete="off">
                            <label for="visitday"><i class="fa fa-calendar fa-lg calendar" aria-hidden="true" ></i></label>
                        </div>
                        <div class="booktour_row">
                            <textarea rows="4" cols="50" name="description" placeholder="*Message to school..."></textarea>
                        </div>

                        <button class="btn_submit">SUBMIT</button>
                    </form>


                </section>

            </main>

    </body>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/layout/body.blade.php ENDPATH**/ ?>