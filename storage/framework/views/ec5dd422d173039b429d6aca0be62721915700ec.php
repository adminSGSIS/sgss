<?php $__env->startSection('css'); ?>
<style>
    body {
        -webkit-print-color-adjust: exact !important;
        height: 100%;
        width: 100%;
    }
    .logan{
        background-color: #31b6f0;
        width:100%;
        height:125px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 0px 0px 30px 30px;
        font-size: 80px;
    }
    .logan h1{
        font-size: 70px;
        color: #fff;
        font-weight: bold;
    }
    .left-icon{
        position: absolute;
        left: 85px;
    }
    .right-icon{
        position: absolute;
        right: -135px;
    }
    .letter{
        padding-left: 70px!important;
        padding-right:70px!important;
        font-size:22px;
        padding:8px;
        line-height:1.3;
    }
    .ft200 {
        font-size: 200% !important;
    }

    .p-2>h1 {
        font-size: 100px;
        color: #fff;
    }
    div > h1{

    }
    .footer{
        bottom: 0;position: absolute;width: 100%;
    }
    @page  {
        margin-bottom: 0;
        margin-top: 0;
    }
    
    @media  print {
        #message_box,
        footer,
        .print {
            display: none !important;
        }

        .pdf {
            display: block;
        }

        #view-slip {
            display: none;
        }

        #pay-slip {
            display: none;
        }
    }
    .pdf {
        display: none;
        font-family: 'Roboto', sans-serif;
        
    }
    
    .header{
        padding:30px;
    }
    
    
    
    .pdf-footer {
        border-top: 1px solid #008fc5;
        padding-top: 10px;
    }

    .tablePDF {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        
        .tdPDF,
        .thPDF {
            text-align:  left;
            padding: 8px;
            font-size: 150%!important;
        }
    .info{
        font-size: 170%!important;
    }
    .ft200{
        font-size: 130%!important;
    }
    .p-2>h1{
        font-size: 100px;
    }
    @page  {
        margin-bottom: 0;
        margin-top: 0;
    }

    @media  print {
        .table thead tr td,.table tbody tr td{
            border-width: 1px !important;
            border-style: solid !important;
            border-color: #7dd3f7 !important;
            font-size: 18px !important;
            /*background-color: red;*/
            /*padding:0px;*/
            -webkit-print-color-adjust:exact ;
        }
        
        
    
       
        .table td, .table th { 
            background-color: transparent !important;
            -webkit-print-color-adjust:exact ;
        } 
        .student-details,
        .sms-breadcrumb,
        #message_box,
        footer,
        .print {
            display: none !important;
        }

        
        .pdf {
            display: block;
        }

        #view-slip {
            display: none;
        }
        
    
        
    }
    .bold{
        font-weight:bold;
    }
    .ft300{
        font-size:18px;
        color: rgba(66, 64, 64, 0.822);
    }
    
    .fz25{
        font-size:22px;
        line-height:1.3;
    }
   
    .bg-header{
            background-color: #e6f2fa !important;
            
            -webkit-print-color-adjust:exact ;
            
        }
    
</style>
<?php $__env->stopSection(); ?>
<div class="pdf">
    <img src="<?php echo e(asset('public/header_letter.png')); ?>" width="100%">
    <div style="height: 1350px;">
        <div class=" justify-content-center align-items-center" style="height: 87%">
            <br>
            <div style="position:relative;">
                <div>
                    <img src="<?php echo e(asset('public/quotation.png')); ?>" width="100%">
                </div>
                <br>
                <div class="row fz25" style="position:absolute; top:20px;left:550px;">
                    
                        Student's name: <?php echo e($slip->student->full_name); ?><br>
                        Parent's name: <?php echo e($slip->student->full_name); ?><br>
                        Class: <?php echo e($slip->student->class->class_name); ?>&nbsp;&nbsp;
                        Year: <?php echo e(date('Y')); ?>

                    
                </div>
            </div>
            <br>
            
            <div>
               
                <table class="table table-bordered " style="text-align: center;color:#191970">
                    <thead>
                        <tr class="bg-header">
                            <td width="25%" class="ft200 bold">TYPE OF FEE</td>
                            <td width="24%" class="ft200 bold">AMOUNT (VND)</td>
                            <td width="26%" class="ft200 bold">DISCOUNT (%)</td>
                            <td width="25%" class="ft200 bold">TOTAL AMOUNT (VND)</td>
                        </tr>
                        <?php $__currentLoopData = $fees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td width="25%" class="ft200 "><?php echo e($fee->feegroup->group_name); ?></td>
                            <td width="24%" class="ft200 bold"><?php echo e(number_format($fee->fee->amount)); ?></td>
                            <td width="26%" class="ft200 bold"><?php echo e($fee->discount); ?> %</td>
                            <td width="25%" class="ft200 bold"><?php echo e(number_format($fee->fee->amount / 100 * (100 - $fee->discount))); ?></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </thead>
                    
                </table>
                <div class="row" style="margin-top : -17px;margin-bottom : 20px;padding-left:10px">
                    <div class="col-lg-6 col-md-6">
                    </div>
                    <div class="col-lg-6 col-md-6 row bg-header" style="border:1px solid #7dd3f7;padding:15px;color:#191970">
                        <div class="col-6 ">
                            <div class="bold" style="font-size:18px;line-height:1.5">
                                TOTAL: <br>
                                <?php if($slip->deposit != 0): ?>
                                DEPOSIT: <br>
                                DEPOSIT AMOUNT: <br>
                                <?php endif; ?>
                                PAID: <br>
                                BALANCE: 
                            </div>
                        </div>
                        <div class="col-6" >
                            <div class="bold" style="font-size:18px;line-height:1.5">
                                <?php echo e(number_format($slip->amount_applied_discount)); ?> vnd<br>
                                <?php if($slip->deposit != 0): ?>
                                <?php echo e($slip->deposit); ?> %<br>
                                <?php echo e(number_format($slip->amount_applied_discount / 100 * $slip->deposit)); ?> vnd<br>
                                <?php endif; ?>
                                <?php echo e(number_format($slip->paid)); ?> vnd<br>
                                <?php echo e(number_format($slip->amount_applied_discount-$slip->paid)); ?> vnd
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            <img src="<?php echo e(asset('Modules/FeesCollection/public/quotation_banner.png')); ?>" width="100%">
            <div class="row" style="margin-top:20px;">
                <div class="col-12" style="font-size:18px;line-height:1.4">
                    Payment by bank transfer or cash <br><br>
                </div>
                
                <div class="col-12" style="font-size:18px;line-height:1.4">
                    Account Name: <b>SAIGON STAR INTERNATIONAL LIABILITY LIMITED COMPANY </b><br>
                    Account Number (USD): <b>060 245 314011 </b><br>
                    Bank Name: <b>SAIGON THUONG TIN COMMERCIAL JOINT STOCK BANK (SACOMBANK) </b><br>
                    Bank Address: <b>50 TRAN NAO STR, BINH AN WARD, THU DUC CITY, HCM CITY, VIETNAM</b><br>
                    SWIFT CODE: <b>SGTTVNVX</b><br>
                </div>
                <div class="col-12">
                    <img src="<?php echo e(asset('Modules/FeesCollection/public/quotation_hr.png')); ?>" width="100%">
                </div>
                
                
                <div class="col-12" style="font-size:18px;line-height:1.4">
                    Tên Tài Khoản: <b>CÔNG TY TNHH QUỐC TẾ NGÔI SAO SÀI GÒN </b><br>
                    Số Tài khoản (VND): <b>060 245 300551 </b><br>
                    Tên Ngân Hàng: <b>NGÂN HÀNG THƯƠNG MẠI CỔ PHẦN SÀI GÒN THƯƠNG TÍN - SACOMBANK CHI NHÁNH QUẬN 2 </b><br>
                    Địa chỉ: <b>50 TRẦN NÃO, PHƯỜNG BÌNH AN, THÀNH PHỐ THỦ ĐỨC, THÀNH PHỐ HỒ CHÍ MINH</b><br>
                    Mã SWIFT: <b>SGTTVNVX</b><br>
                </div>
            </div>
        </div>
        <div >
            <img src="<?php echo e(asset('public/footer_letter.png')); ?>" alt="" width="100%">
        </div>
    
   
</div>
    
</div><?php /**PATH /home/sgstared/public_html/Modules/FeesCollection/Resources/views/view-slip-pdf.blade.php ENDPATH**/ ?>