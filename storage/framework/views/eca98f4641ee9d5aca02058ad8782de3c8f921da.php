<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    <style>
        .thank {
            border: 1px solid rgb(130, 130, 130);
            padding: 10px;
            text-align: center;
            font-weight: 700;
        }

        .header {
            width: 100%;
        }

        .logo {

            width: 20%;
        }

        .logo-group {
            display: flex;
        }
        a{
            text-decoration: none;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        
        td,
        th {
            border: 1px solid #dddddd;
            text-align:  left;
            padding: 8px;
            font-size: 25px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .map-mb {
            width: 5%;
        }

        .map-span {
            padding-left: 1%;
            font-size: 24px;
        }
        .logo-text{
            padding-left:5%;
            font-size:24px;
            display:flex;
        }
        @media  only screen and (max-width: 420px) {
            .header {
                width: 100%;
            }

            .map-mb {
                width: 15%;
                height: 15%;
            }

            .logo {
                width: 15%;
            }

            .logo-group {
                display: block;
            }

            .map-span {
                padding-left: 5%;
                font-size: 12px;
                display: flex;
            }
            .logo-text{
            padding-left:5%;
            font-size:12px;
        }
        }
    </style>
    <script src="https://use.fontawesome.com/987dcc5886.js"></script>
    <link rel="stylesheet" href="<?php echo e(asset('public/css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/bootstrap/bootstrap.min.css')); ?>">
    <script src="<?php echo e(asset('public/bootstrap/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/bootstrap/popper.min.js')); ?>"></script>
</head>

<body>
    <div class="container">
    <div style="height: 60px;background-color: #0f53bf;color: #fff;text-align: center;"></div>
    <div style="text-align: center">
        <img src="https://i.imgur.com/acppyR7.png" class="header" alt="">
        <!--<div class="thank"><h2>SAIGON STAR INTERNATIONAL SCHOOL</h2></div>-->
    </div>
    
    <?php echo $__env->yieldContent('mail-content'); ?>

    <br><br>

    <div class="" style="border-top: 3px solid #008fc5;width:100%"></div>
    <img src="https://i.imgur.com/ZpWFOKR.png" alt="" width="100%">
    <!--<div class="logo-group">-->
    <!--            <img src="https://sgstar.asia/public/uploads/category/sgstar_20/phone.png" alt="" class="logo">-->
               
    <!--        </span></div> -->
    <!--</div>-->
    </div>
</body>

</html><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/mails/mail_template.blade.php ENDPATH**/ ?>