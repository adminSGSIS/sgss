
<?php $__env->startSection('main_content'); ?>
    <?php $__env->startPush('css'); ?>
        <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
        <link rel="stylesheet" href="<?php echo e(url('public')); ?>/frontEnd/css/newcss.css" />
    <?php $__env->stopPush(); ?>

    <div class="banner">
        <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
        <div class="button-container">
            <div class="title d-flex justify-content-center">
                <h3>community success</h3>
            </div>
        </div>
    </div>
    <div class="main_content" style="background:#fafaf4">
        <div class="container d-flex justify-content-center">
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="event-march pd-5 col-12">
                        <div class="right center">
                            <img class="circle-img" src="<?php echo e(asset('public/')); ?>/images/student-council/THAOANH.png" alt=""> 
                            <a href="#" onclick="event.preventDefault();">Thao Anh</a>
                        </div>
                        <div class="left">
                            <p style="padding-top:0">We are delighted to announce that a student of Saigon Star International School has won a
                                                     Bronze medal in a prestigious international Robotics competition! Tran Thao Anh, who 
                                                     currently learns in our IMYC programme, won the third prize together with her team in 
                                                     the International Young Robotics Competition (IYRC), held online in Korea in September 2020.
                            </p>
                        </div>
                    </div>
                </div>
                   
              </div>
            </div>
            
        </div>
        <div class="container">
              <img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/CHILDREN-01.png" alt=""> 
        </div>
       
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/student-of-the-month.blade.php ENDPATH**/ ?>