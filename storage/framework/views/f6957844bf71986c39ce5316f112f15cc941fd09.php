
<?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/css/style2.css" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('main_content'); ?>
    <div>
        <div class="banner">
            <img class="banner" width="100%" src="<?php echo e(asset('public/images/banner/banner_03.jpg')); ?>" alt="">
            <div class="button-container">
                <div class="title d-flex justify-content-center">
                    <h3>admissions team</h3>
                </div>
            </div>
        </div>
        <div class="staff d-flex justify-content-center c-color">
            <?php $__currentLoopData = $admissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $admission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="profile collumn">
                    <img src="<?php echo e(file_exists(@$admission->staff_photo) ? asset($admission->staff_photo) : asset('public/uploads/staff/demo/staff.jpg')); ?>"
                        alt=""></br>
                    <p class="btn-profile">
                        <?php echo e($admission->full_name); ?>

                    </p>
                    
                    <p class="staff-title"><?php echo e($admission->designations->title); ?></p>
                </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="c-color">
        <div class="container">
              <img width="100%" src="<?php echo e(url('public')); ?>/images/CHILDREN-01.png" alt="">
        </div>
    </div>
    <!--<div class="rainbow">-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--    <p></p>-->
    <!--</div>-->
    <?php echo $__env->make('frontEnd.home.partials.info-teachers', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/resources/views/frontEnd/home/staff/admission.blade.php ENDPATH**/ ?>