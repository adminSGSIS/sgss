<?php $__env->startPush('css'); ?>
<link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/frontEnd/css/new_style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/backEnd/')); ?>/css/croppie.css">
<link rel="stylesheet" href="<?php echo e(asset('public/')); ?>/frontEnd/css/smart_wizard_all.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<?php $__env->stopPush(); ?>

<?php
$err_steps = array();
?>

<?php if($errors->has('student_full_name')
   | $errors->has('date_of_birth')
   | $errors->has('gender')
   | $errors->has('first_nationality')
   | $errors->has('residential_address_in_vietnam')
   | $errors->has('student_lives_with')
   | $errors->has('childs_first_language')
   | $errors->has('level_of_E_language_experience')
): ?>
<?php array_push($err_steps, 0); ?>
<?php endif; ?>

<?php if($errors->has('fathers_full_name')
   | $errors->has('fathers_nationality')
   | $errors->has('fathers_contact_phone_number')
   | $errors->has('fathers_email_address')
   | $errors->has('mothers_full_name')
   | $errors->has('mothers_nationality')
   | $errors->has('mothers_contact_phone_number')
   | $errors->has('mothers_email_address')
   | $errors->has('emergency_full_name')
   | $errors->has('emergency_relationship_to_parents')
   | $errors->has('emergency_contact_phone_number')
): ?>
<?php array_push($err_steps, 1); ?>
<?php endif; ?>

<?php if($errors->has('school_name')
   | $errors->has('location_or_country')
   | $errors->has('language_of_instruction')
   | $errors->has('from')
   | $errors->has('to')
): ?>
<?php array_push($err_steps, 2); ?>
<?php endif; ?>

<?php if($errors->has('answer1')
   | $errors->has('answer2')
   | $errors->has('answer3')
): ?>
<?php array_push($err_steps, 3); ?>
<?php endif; ?>

<?php if($errors->has('other_permissions')
   | $errors->has('answer4')
   | $errors->has('answer5')
): ?>
<?php array_push($err_steps, 4); ?>
<?php endif; ?>

<?php if($errors->has('lunch_requested')
   | $errors->has('school_bus')
   | $errors->has('payment_by')
   | $errors->has('VAT')
): ?>
<?php array_push($err_steps, 5); ?>
<?php endif; ?>

<?php if($errors->has('session')
   | $errors->has('class')
   | $errors->has('section')
): ?>
<?php array_push($err_steps, 6); ?>
<?php endif; ?>

<?php $__env->startSection('main_content'); ?>

<div class="banner">
	<img class="banner" width="100%" src="<?php echo e(asset('public/')); ?>/images/banner-du-an1.jpeg" alt="">
	<div class="button-container">
		<div class="title d-flex justify-content-center">
            <h3>APPLICATION FORM</h3>
        </div>
	</div>
</div>
<div class="c-color pd-50">
    <div class="container">
        <?php echo e(Form::open(['class' => 'form-horizontal studentadmission', 'files' => true, 'url' => 'form', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'id' => 'student_form'])); ?>

        
    
        <div id="smartwizard" style="z-index:1">
            <?php echo $__env->make('form::nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->make('form::content', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
        <button type="submit" id="submit_form" class="btn btn-primary" style="text-align: center;margin-left: 50%; transform:translate(-50%, 50%); background: #17a2b8">Submit</button>
        <?php echo e(Form::close()); ?>

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script src="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/js/jquery.smartWizard.min.js"></script>
<script>
    $(document).ready(function() {
        const step = 6;

        <?php if($errors->any()): ?>
        $('#smartwizard').smartWizard({
            theme: 'dots',
            keyboardSettings: {
                keyNavigation: false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
            },
            errorSteps: [<?php echo e(implode(', ', $err_steps)); ?>],
            selected: step
        });

        $('#smartwizard').smartWizard("goToStep", <?php echo e($err_steps[0]); ?>);
        <?php else: ?>
        $('#smartwizard').smartWizard({
            theme: 'dots',
            keyboardSettings: {
                keyNavigation: false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
            },
        });
        <?php endif; ?>

        $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
            if (nextStepIndex === step) {
                $("#submit_form").show();
            } else {
                $("#submit_form").hide();
            }
        });

        var stepIndex = $('#smartwizard').smartWizard("getStepIndex");
        if (stepIndex != step) {
            $("#submit_form").hide();
        }

        // academic year
        $("#academic_year_form  , form#infix_form #academic_year").on('change', function() {
            var url = $('#url').val();


            var formData = {
                id: $(this).val()
            };
            // get section for student
            $.ajax({
                type: "GET",
                data: formData,
                dataType: 'json',
                url: url + '/' + 'academic-year-get-class',
                success: function(data) {

                    console.log(data);


                    var a = '';
                    // $.each(data[0], function (i, item) {

                    $("#sectionSelectStudent_enrolment").find("option").not(":first").remove();
                    $("#sectionStudentDiv ul").find("li").not(":first").remove();

                    if (data[0].length) {

                        $('#classSelectStudent_enrolment').find('option').not(':first').remove();
                        $('#class-div ul').find('li').not(':first').remove();

                        $.each(data[0], function(i, className) {


                            $('#classSelectStudent_enrolment').append($('<option>', {
                                value: className.id,
                                text: className.class_name
                            }));

                            $("#class-div ul").append("<li data-value='" + className.id + "' class='option'>" + className.class_name + "</li>");
                        });
                    } else {
                        $('#class-div .current').html('SELECT CLASS *');
                        $('#classSelectStudent_enrolment').find('option').not(':first').remove();
                        $('#class-div ul').find('li').not(':first').remove();
                    }


                },
                error: function(data) {
                    // console.log('Error:', data);
                }
            });
        });

        // class
        $("#classSelectStudent_enrolment").on("change", function() {
            var url = $("#url").val();
            console.log(url);

            var formData = {
                id: $(this).val(),
            };
            // get section for student
            $.ajax({
                type: "GET",
                data: formData,
                dataType: "json",
                url: url + "/" + "ajaxSectionStudent",
                success: function(data) {
                    // console.log(data);
                    var a = "";
                    $.each(data, function(i, item) {
                        if (item.length) {
                            $("#sectionSelectStudent_enrolment").find("option").not(":first").remove();
                            $("#sectionStudentDiv ul").find("li").not(":first").remove();

                            $.each(item, function(i, section) {
                                $("#sectionSelectStudent_enrolment").append(
                                    $("<option>", {
                                        value: section.id,
                                        text: section.section_name,
                                    })
                                );

                                $("#sectionStudentDiv ul").append(
                                    "<li data-value='" +
                                    section.id +
                                    "' class='option'>" +
                                    section.section_name +
                                    "</li>"
                                );
                            });
                        } else {
                            $("#sectionStudentDiv .current").html("SECTION *");
                            $("#sectionSelectStudent_enrolment").find("option").not(":first").remove();
                            $("#sectionStudentDiv ul").find("li").not(":first").remove();
                        }
                    });
                    console.log(a);
                },
                error: function(data) {
                    console.log('Error:', data);
                },
            });
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontEnd.home.layout.front_master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/sgstared/public_html/Modules/Form/Resources/views/layout.blade.php ENDPATH**/ ?>