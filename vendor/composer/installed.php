<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'laravel/laravel',
  ),
  'versions' => 
  array (
    'africastalking/africastalking' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f14094a1e1677ae36bac1a2203083f6eb981aa90',
    ),
    'anhskohbo/no-captcha' => 
    array (
      'pretty_version' => '3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f654a4093bd2e9ddbdfad3297bb7aa142a55e611',
    ),
    'asm89/stack-cors' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9c31def6a83f84b4d4a40d35996d375755f0e08',
    ),
    'bacon/bacon-qr-code' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3e9d791b67d0a2912922b7b7c7312f4b37af41e4',
    ),
    'barryvdh/laravel-dompdf' => 
    array (
      'pretty_version' => 'v0.8.7',
      'version' => '0.8.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '30310e0a675462bf2aa9d448c8dcbf57fbcc517d',
    ),
    'botman/botman' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '127f7dc0aef5bc0561f8a2a8a3b246fe951b63e7',
    ),
    'botman/driver-web' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7399fd2994ee4c6c14c0a69268269e81fd2cd7f6',
    ),
    'brian2694/laravel-toastr' => 
    array (
      'pretty_version' => '5.54',
      'version' => '5.54.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e543a8a6953be6b25706da4c7ae48600666a70dc',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.9.1',
      'version' => '0.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '283a40c901101e66de7061bd359252c013dcc43c',
    ),
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'dasprid/enum' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5abf82f213618696dda8e3bf6f64dd042d8542b2',
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'defuse/php-encryption' => 
    array (
      'pretty_version' => 'v2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f407c43b953d571421e0020ba92082ed5fb7620',
    ),
    'dnoegel/php-xdg-base-dir' => 
    array (
      'pretty_version' => 'v0.1.1',
      'version' => '0.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8f8a6e48c5ecb0f991c2fdcf5f154a47d85f9ffd',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9cf661f4eb38f7c881cac67c75ea9b00bf97b210',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f350df0268e904597e3bd9c4685c53e0e333feea',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'dompdf/dompdf' => 
    array (
      'pretty_version' => 'v0.8.6',
      'version' => '0.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db91d81866c69a42dad1d2926f61515a1e3f42c5',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '72b6fbf76adb3cf5bc0db68559b33d41219aba27',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '2.1.22',
      'version' => '2.1.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '68e418ec08fbfc6f58f6fd2eea70ca8efc8cc7d5',
    ),
    'erusev/parsedown' => 
    array (
      'pretty_version' => '1.7.4',
      'version' => '1.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
    ),
    'evenement/evenement' => 
    array (
      'pretty_version' => 'v3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '531bfb9d15f8aa57454f5f0285b18bec903b8fb7',
    ),
    'facade/flare-client-php' => 
    array (
      'pretty_version' => '1.3.6',
      'version' => '1.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '451fadf38e9f635e7f8e1f5b3cf5c9eb82f11799',
    ),
    'facade/ignition' => 
    array (
      'pretty_version' => '2.3.7',
      'version' => '2.3.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b364db8860a63c1fb58b72b9718863c21df08762',
    ),
    'facade/ignition-contracts' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aeab1ce8b68b188a43e81758e750151ad7da796b',
    ),
    'fideloper/proxy' => 
    array (
      'pretty_version' => '4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9beebf48a1c344ed67c1d36bb1b8709db7c3c1a8',
    ),
    'filp/whoops' => 
    array (
      'pretty_version' => '2.7.3',
      'version' => '2.7.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d5fe9bb3d656b514d455645b3addc5f7ba7714d',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.2.0',
      'version' => '5.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'feb0e820b8436873675fd3aca04f3728eb2185cb',
    ),
    'fruitcake/laravel-cors' => 
    array (
      'pretty_version' => 'v1.0.6',
      'version' => '1.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d127dbec313e2e227d65e0c483765d8d7559bf6',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc10d778e4b84d5bd315dad194661e091d307c6f',
    ),
    'google/apiclient' => 
    array (
      'pretty_version' => 'v2.8.2',
      'version' => '2.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '24f3ec0c79567a08c31f8271b61d79b050b25b0a',
    ),
    'google/apiclient-services' => 
    array (
      'pretty_version' => 'v0.153',
      'version' => '0.153.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db62bb19f32f81c9551f223c72be0682eb58ebd8',
    ),
    'google/auth' => 
    array (
      'pretty_version' => 'v1.14.3',
      'version' => '1.14.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1503299c779af0cbc99b43788f75930988852cf',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '239400de7a173fe9901b9ac7c06497751f00727a',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c3d0a3f6af734494ad8f6fbbee0ba92422859f3',
    ),
    'illuminate/auth' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/broadcasting' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/bus' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/cache' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/config' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/console' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/container' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/cookie' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/database' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/encryption' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/events' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/hashing' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/http' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/log' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/mail' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/notifications' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/pagination' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/pipeline' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/queue' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/redis' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/routing' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/session' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/support' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/testing' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/translation' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/validation' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'illuminate/view' => 
    array (
      'replaced' => 
      array (
        0 => 'v7.28.3',
      ),
    ),
    'intervention/image' => 
    array (
      'pretty_version' => '2.5.1',
      'version' => '2.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'abbf18d5ab8367f96b3205ca3c89fb2fa598c69e',
    ),
    'jaybizzle/crawler-detect' => 
    array (
      'pretty_version' => 'v1.2.99',
      'version' => '1.2.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ffea34489b258a2709bfe93a9553e1efa5d1904',
    ),
    'jenssegers/agent' => 
    array (
      'pretty_version' => 'v2.6.4',
      'version' => '2.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'daa11c43729510b3700bc34d414664966b03bffe',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'laminas/laminas-diactoros' => 
    array (
      'pretty_version' => '2.4.1',
      'version' => '2.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '36ef09b73e884135d2059cc498c938e90821bb57',
    ),
    'laminas/laminas-zendframework-bridge' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ede70583e101030bcace4dcddd648f760ddf642',
    ),
    'laravel/framework' => 
    array (
      'pretty_version' => 'v7.28.3',
      'version' => '7.28.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b0942c391975972b1a54b2dc983e33a239f169a9',
    ),
    'laravel/helpers' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b4b8d6e84a6306fc88870f61a244d8c537779f2d',
    ),
    'laravel/laravel' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'laravel/passport' => 
    array (
      'pretty_version' => 'v9.3.2',
      'version' => '9.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '192fe387c1c173c12f82784e2a1b51be8bd1bf45',
    ),
    'laravel/socialite' => 
    array (
      'pretty_version' => 'v5.0.1',
      'version' => '5.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '61ad2b5f98e28f8e4e479851f8744b43ebbb9b46',
    ),
    'laravel/tinker' => 
    array (
      'pretty_version' => 'v2.4.2',
      'version' => '2.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '58424c24e8aec31c3a3ac54eb3adb15e8a0a067b',
    ),
    'laravel/ui' => 
    array (
      'pretty_version' => 'v2.4.1',
      'version' => '2.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c69ae3e8b52fe6c9eaf83b43c6dd8ef5c3f9e2c',
    ),
    'laravelcollective/html' => 
    array (
      'pretty_version' => 'v6.2.0',
      'version' => '6.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3bb99be7502feb2129b375cd026ccb0fa4b66628',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '3.3.3',
      'version' => '3.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1123697f6a2ec29162b82f170dd4a491f524773',
    ),
    'league/commonmark' => 
    array (
      'pretty_version' => '1.5.5',
      'version' => '1.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '45832dfed6007b984c0d40addfac48d403dc6432',
    ),
    'league/event' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2cc124cf9a3fab2bb4ff963307f60361ce4d119',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9be3b16c877d477357c015cec057548cf9b2a14a',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ea2fbfc988bade315acd5967e6d02274086d0f28',
    ),
    'league/oauth1-client' => 
    array (
      'pretty_version' => 'v1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '159c3d2bf27568f9af87d6c3f4bb616a251eb12b',
    ),
    'league/oauth2-client' => 
    array (
      'pretty_version' => '2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9f2a1e000dc14eb3c02e15d15759385ec7ff0fb',
    ),
    'league/oauth2-server' => 
    array (
      'pretty_version' => '8.1.1',
      'version' => '8.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '09f22e8121fa1832962dba18213b80d4267ef8a3',
    ),
    'league/oauth2server' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'lncd/oauth2' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'maatwebsite/excel' => 
    array (
      'pretty_version' => '3.1.22',
      'version' => '3.1.22.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ba7152670257ba9f47bb6ff7cee025f8fc9da608',
    ),
    'macsidigital/laravel-api-client' => 
    array (
      'pretty_version' => '3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f12b734c2e1460b50afc942943897276c43500ee',
    ),
    'macsidigital/laravel-oauth2-client' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a01b22cef100a83b039eed5879f22bf4a862761',
    ),
    'macsidigital/laravel-zoom' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b0af5617a1ad632f13c1a652391abb993606c3fb',
    ),
    'macsidigital/zoom' => 
    array (
      'pretty_version' => '0.0.5',
      'version' => '0.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '49f68aa9ee47360df7c622eee940e8ca160e39ed',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3131244e29c08d44fefb49e0dd35021e9e39dd2',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '182d44c3b2e3b063468f7481ae3ef71c69dc1409',
    ),
    'mnapoli/front-yaml' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7602e0ffe84ca07b68865960d255d3ae9502e5d4',
    ),
    'mnapoli/silly' => 
    array (
      'pretty_version' => '1.7.2',
      'version' => '1.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '66807f87abd2ab8e5708754d70b4b601f5614c32',
    ),
    'mobiledetect/mobiledetectlib' => 
    array (
      'pretty_version' => '2.8.34',
      'version' => '2.8.34.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f8113f57a508494ca36acbcfa2dc2d923c7ed5b',
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => '1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '20cab678faed06fac225193be281ea0fddb43b93',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f9eee5cec93dfb313a38b6b288741e84e53f02d5',
    ),
    'mpociot/documentarian' => 
    array (
      'pretty_version' => '0.4.0',
      'version' => '0.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c7dc266b6646a49f82b304a3451e41161cc4b1c3',
    ),
    'mpociot/laravel-apidoc-generator' => 
    array (
      'pretty_version' => '4.8.2',
      'version' => '4.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9015d39020dc1d7e80b0cd012bc1e999fd2542ca',
    ),
    'mpociot/pipeline' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3584db4a0de68067b2b074edfadf6a48b5603a6b',
    ),
    'mpociot/reflection-docblock' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c8b2e2b1f5cebbb06e2b5ccbf2958f2198867587',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '969b211f9a51aa1f6c01d1d2aef56d3bd91598e5',
      'replaced' => 
      array (
        0 => '1.10.1',
      ),
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.7.6',
      'version' => '1.7.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f36467c7a87e20fbdc51e524fd8f9d1de80187c',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.40.1',
      'version' => '2.40.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9a76d8b7eb0f97cf3a82529393245212f40ba3b',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.2',
      'version' => '4.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '658f1be311a230e0907f5dfe0213742aff0596de',
    ),
    'nunomaduro/collision' => 
    array (
      'pretty_version' => 'v4.2.0',
      'version' => '4.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd50490417eded97be300a92cd7df7badc37a9018',
    ),
    'nwidart/laravel-modules' => 
    array (
      'pretty_version' => '7.2.0',
      'version' => '7.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8980876d63096951e067bcaf8649d591d4c7f0da',
    ),
    'nyholm/psr7' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '21b71a31eab5c0c2caf967b9c0fd97020254ed75',
    ),
    'opis/closure' => 
    array (
      'pretty_version' => '3.5.7',
      'version' => '3.5.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '4531e53afe2fc660403e76fb7644e95998bff7bf',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.99',
      'version' => '9.99.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
    ),
    'paragonie/sodium_compat' => 
    array (
      'pretty_version' => 'v1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbade402cbe84c69b718120911506a3aa2bae653',
    ),
    'paypal/rest-api-sdk-php' => 
    array (
      'pretty_version' => '1.14.0',
      'version' => '1.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '72e2f2466975bf128a31e02b15110180f059fc04',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
    ),
    'phenx/php-font-lib' => 
    array (
      'pretty_version' => '0.5.2',
      'version' => '0.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6ad461f032145fff5971b5985e5af9e7fa88d8',
    ),
    'phenx/php-svg-lib' => 
    array (
      'pretty_version' => 'v0.3.3',
      'version' => '0.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa61b65e612ce1ae15f69b3d223cb14ecc60e32',
    ),
    'php-di/invoker' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a6f8f276d2680e77d06294b9fd67b4881b1f82d',
    ),
    'php-http/message-factory' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a478cb11f66a6ac48d8954216cfed9aa06a501a1',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.1.7',
      'version' => '6.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c2370ba3df7034f9eb7b8f387c97b52b2ba5ad0',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.14.1',
      'version' => '1.14.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2383aad5689778470491581442aab38cec41bf1d',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.7.5',
      'version' => '1.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '994ecccd8f3283ecf5ac33254543eb0ac946d525',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '2.0.29',
      'version' => '2.0.29.0',
      'aliases' => 
      array (
      ),
      'reference' => '497856a8d997f640b4a516062f84228a772a48a8',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.12.0',
      'version' => '1.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '765cd5d5d237525f8bbadaec5dc161c83a369119',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '7.0.10',
      'version' => '7.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1884187926fbb755a9aaf0b3836ad3165b478bf',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '050bedf145a257b1ff02746c31894800e5122946',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1038454804406b0b5f5f520358e78c1c2f71501e',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '995192df77f63a59e47f025390d2d1fdf8f425ff',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '8.5.8',
      'version' => '8.5.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '34c18baa6a44f1d1fbf0338907139e9dce95b997',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
        1 => '1.0.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.10.4',
      'version' => '0.10.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8aec1b2981ab66882a01cce36a49b6317dc3560',
    ),
    'pusher/pusher-php-server' => 
    array (
      'pretty_version' => 'v4.1.4',
      'version' => '4.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e75e5715e3b651ec20dee5844095aadefab81acb',
    ),
    'rahulreghunath/textlocal' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5ec529463616517a5d26d396f9192b214b3994c4',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'ramsey/collection' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '24d93aefb2cd786b7edd9f45b554aea20b28b9b1',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '4.1.1',
      'version' => '4.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd4032040a750077205918c86049aa0f43d22947',
    ),
    'react/cache' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '44a568925556b0bd8cacc7b49fb0f1cf0d706a0c',
    ),
    'react/dns' => 
    array (
      'pretty_version' => 'v1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '665260757171e2ab17485b44e7ffffa7acb6ca1f',
    ),
    'react/event-loop' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6d24de090cd59cfc830263cfba965be77b563c13',
    ),
    'react/promise' => 
    array (
      'pretty_version' => 'v2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3cff96a19736714524ca0dd1d4130de73dbbbc4',
    ),
    'react/promise-timer' => 
    array (
      'pretty_version' => 'v1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'daee9baf6ef30c43ea4c86399f828bb5f558f6e6',
    ),
    'react/socket' => 
    array (
      'pretty_version' => 'v1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2b96b23a13ca9b41ab343268dbce3f8ef4d524a',
    ),
    'react/stream' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c02b510ee3f582c810aeccd3a197b9c2f52ff1a',
    ),
    'renatomarinho/laravel-page-speed' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7639014cde1bbb8dbe3e22be6ff92bee00671625',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '4.1.1',
      ),
    ),
    'robincsamuel/laravel-msg91' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'd31404733af403f9d1ff76a84eb1ac8bc1c1284a',
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.3.1',
      'version' => '8.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd217848e1396ef962fb1997cf3e2421acba7f796',
    ),
    'scrivo/highlight.php' => 
    array (
      'pretty_version' => 'v9.18.1.2',
      'version' => '9.18.1.2',
      'aliases' => 
      array (
      ),
      'reference' => 'efb6e445494a9458aa59b0af5edfa4bdcc6809d9',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de4fc177adf9bce8df98d8d141a7559d7ccf6da',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '720fcc7e9b5cf384ea68d9d930d480907a0c1a29',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.3',
      'version' => '4.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '464c90d7bdf5ad4e8a6aea15c091fec0603d4368',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '68609e1261d215ea5b21b7987539cbfbe156ec3e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edf8a461cf1d4005f19fb0b6b8b95a9f7fa0adc4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cfd9e65d11ffb5af41198476395774d4c8a84c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '773f97c67f28de00d397be301821b06708fca0be',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7a795d35b889bf80a0cc04e08d77cedfa917a9',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3aaaa15fa71d27650d62a948be022fe3b48541a3',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'simplesoftwareio/simple-qrcode' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b721089cc541f6c37f0a41b90377529af977948',
    ),
    'spatie/laravel-google-calendar' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2e62238fbeed348b8a57d9a8b6f3a1adce30ecc1',
    ),
    'spatie/macroable' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '74b0d189ce75142f1706aad834d5a428dfc7c3c3',
    ),
    'stripe/stripe-php' => 
    array (
      'pretty_version' => 'v7.56.0',
      'version' => '7.56.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '987596ad7ca747ce498e13bc1e3d1a57fbeb94cc',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.2.3',
      'version' => '6.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '149cfdf118b169f7840bbe3ef0d4bc795d1780c9',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '04c3a31fe8ea94b42c9e2d1acc93d19782133b00',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e544e24472d4c97b2d11ade7caacd446727c6bf9',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa56b4074d1ae755beb55617ddafe6f5d78f665',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2f1d4996d5499f1261164d10080e4120001f041',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd5de97d6af175a9e8131c546db054ca32842dd0f',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ba7d54483095a198fa51781bc608d17e84dffa2',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c3ba7ad6884e6c4451ce2340e2dc23f6fa3e0d8',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3a5d0fe7908daaa23e3dbf4cee3ba4bfbb19fdd3',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '6cca6b2e4b69fc5bace160d14cf1ee5f71483db4',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '17227644c3c66dcf32bdfeceff4364d090cd6756',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '4404d6545125863561721514ad9388db2661eec5',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c302646f6efc070cd46856e600e5e0684d6b454',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6c2f78eb8f5ab8eaea98f6d414a5915f2e0fce36',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b740103edbdcc39602239ee8860f0f45a8eb9aa5',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5dcab1bc7146cf8c1beaa4502a3d9be344334251',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '37078a8dd4a2a1e9ab0231af7c6cb671b2ed5a7e',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6977d63bf9a0ad4c65cd352709e230876f9904a',
    ),
    'symfony/polyfill-php70' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0dd93f2c578bdc9c72697eaa5f1dd25644e618d3',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '639447d008615574653fb3bc60d1986d7172eaae',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fffa1a52a023e782cdcc221d781fe1ec8f87fcca',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd87d5766cbf48d72388a9f6b85f280c8ad51f981',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd3a2e64866169586502f0cd9cab69135ad12cee9',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e44f249afab496b4e8c0f7461fb8140eaa4b24d2',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd36e06eb02a55522a8eed070c1cbc3dc3c389876',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd15da7ba4957ffb8f1747218be9e1a121fd298a1',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '4a9afe9d07bac506f75bcee8ed3ce76da5a9343e',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3cdd5119b1b5bf0698c351b8ee20fb5a4ea248b',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '77ce1c3627c9f39643acd9af086631f842c50c4d',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c976c115a0d788808f7e71834c8eb0844f678d02',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b858508e49beb257fd635104c3d449a8113e8fe',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e147a68cb66a8b510f4b7481fe4da5b2ab65ec6a',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'tightenco/collect' => 
    array (
      'pretty_version' => 'v8.0.4',
      'version' => '8.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d261a13c36fe964449d0240744eb4094ac3cd12',
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b43b05cf43c1b6d849478965062b6ef73e223bb5',
    ),
    'twilio/sdk' => 
    array (
      'pretty_version' => '5.42.2',
      'version' => '5.42.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cfcb871b18a9c427dd9e8f0ed7458d43009b48a',
    ),
    'unicodeveloper/laravel-paystack' => 
    array (
      'pretty_version' => '1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6e8c790b16a947e5d2369ad77d2082e892c326b',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v4.1.8',
      'version' => '4.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '572af79d913627a9d70374d27a6f5d689a35de32',
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '1.5.3',
      'version' => '1.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '25bcbf01678930251fd572891447d9e318a6e2b8',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bafc69caeb4d49c39fd0779086c03a3738cbb389',
    ),
    'windwalker/renderer' => 
    array (
      'pretty_version' => '3.5.21',
      'version' => '3.5.21.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b157f2832dac02209db032cb61e21b8264ee4499',
    ),
    'windwalker/structure' => 
    array (
      'pretty_version' => '3.5.21',
      'version' => '3.5.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '59466adb846685d60463f9c1403df2832d2fcf90',
    ),
    'zendframework/zend-diactoros' => 
    array (
      'replaced' => 
      array (
        0 => '^2.2.1',
      ),
    ),
  ),
);
